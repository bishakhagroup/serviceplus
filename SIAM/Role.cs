using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serialiable object used to contain a Role definition.
	/// </summary>
	[Serializable]
	public class Role
	{
		private string _name;
		private int _roleid;

		/// <summary>
		/// An array of <see cref="AccessRight"/> objects.
		/// </summary>
		public AccessRight[] AccessRights;
		/// <summary>
		/// An integer value containing the Role id.
		/// </summary>
		public int RoleId
		{
			get
			{
				return _roleid;
			}
			set
			{
				_roleid = value;
			}
		}

		/// <summary>
		/// The Name of the role.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// An array of <see cref="Role"/> objects of Child roles for this role.
		/// </summary>
		public Role[] Children;
	}
}
