using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define a SIAM resource.
	/// </summary>
	[Serializable]
	public class Resource:IComparable
	{
		private string _name;
		private int _id;

		/// <summary>
		/// IComparable: CompareTo implementation
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int CompareTo(object obj)
		{
			if (obj is Resource)
			{
				Resource theObj = (Resource)obj;
				return this.Name.CompareTo(theObj.Name);
			}
			else
			{
				throw new ArgumentException("IComparable:Object is not of type sony.us.siam.User");
			}
		}

		/// <summary>
		/// The name of the resource.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
		/// <summary>
		/// The Id of the resource.
		/// </summary>
		public int Id
		{
			get
			{
				return _id;
			}
			set
			{
				_id = value;
			}
		}
	}
}
