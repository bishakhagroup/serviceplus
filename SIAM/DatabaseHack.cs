using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using Sony.US.SIAMUtilities;

namespace Sony.US.siam {
    /// <summary>
    /// Summary description for DatabaseHack.
    /// </summary>
    public class DatabaseHack {
        private OracleConnection cn;
        /// <summary>
        /// will add later
        /// </summary>
        public DatabaseHack() {
            cn = new OracleConnection();

            //Sony.US.siam.SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}

            cn.ConnectionString = ConfigurationData.Environment.DataBase.ConnectionString;
        }

        /// <summary>
        /// This method didn't fit well into the OracleAdministrator nor it's interface.
        /// This is a back door for the front end system administration tool.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string GetResults(string sql) {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            string output = "";

            cmd.CommandText = "executeQuery";
            cmd.Parameters.Add("xSql", OracleDbType.Varchar2, 255);
            cmd.Parameters["xSql"].Value = sql;

            OracleParameter outparam = new OracleParameter("retVal", OracleDbType.Clob);
            outparam.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(outparam);

            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                cmd.Connection = this.cn;

                cmd.ExecuteNonQuery();
                OracleClob ReturnValue = cmd.Parameters["RetVal"].Value as OracleClob;
                if (ReturnValue != null) {
                    ReturnValue.Seek(0, System.IO.SeekOrigin.Begin);
                    byte[] lobBuffer = new byte[ReturnValue.Length];
                    ReturnValue.Read(lobBuffer, 0, (int)ReturnValue.Length);
                    byte[] newBuffer;
                    newBuffer = System.Text.UTF8Encoding.Convert(System.Text.Encoding.Unicode, System.Text.Encoding.UTF8, lobBuffer, 0, lobBuffer.Length);
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    for (int x = 0; x <= newBuffer.GetUpperBound(0); x++) {
                        sb.Append((char)newBuffer[x]);
                    }
                    output = sb.ToString();
                }
            } catch (Exception ex) {
                output = ex.Message;
            }

            cn.Close();
            return output;
        }
        /// <summary>
        /// add later
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string ExecuteLiteral(string sql) {
            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;
            string output = "";

            try {
                if (cn.State != System.Data.ConnectionState.Open) {
                    cn.Open();
                }
                cmd.Connection = this.cn;
                cmd.ExecuteNonQuery();
                output = "Success";
            } catch (Exception ex) {
                output = ex.Message;
            }

            cn.Close();

            return output;
        }
    }
}
