using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable response container for the Update Credential Method call in SIAM.
	/// Note: Devalued. May not be included in the Final API.
	/// </summary>
	[Serializable]
	public class UpdateCredentialsResponse
	{
	}
}
