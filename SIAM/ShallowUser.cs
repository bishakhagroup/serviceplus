using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam 
{
	/// <summary>
	/// A serializable container object used to hold information regarding a user when a fully hydrated
	/// user object is not required.
	/// </summary>
	[Serializable]
	public class ShallowUser: System.IComparable
	{


		/// <summary>
		/// IComparable: CompareTo implementation
		/// </summary>
		/// <param name="user"></param>
		/// <returns></returns>
		public int CompareTo(object user)
		{
			if (user is ShallowUser)
			{
				ShallowUser theUser = (ShallowUser)user;
				return this.Name.CompareTo(theUser.Name);
			}
			else
			{
				throw new ArgumentException("IComparable:Object is not of type sony.us.siam.User");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private string _identity;

		/// <summary>
		/// 
		/// </summary>
		private string _name;
		/// <summary>
		/// 
		/// </summary>
		private string _emailaddress;
		/// <summary>
		/// 
		/// </summary>
		private string _company;

        //Added by chandra on 10-july-07
        /// <summary>
        /// 
        /// </summary>
        private string _username;
        /// <summary>
        /// 
        /// </summary>
        ///
        private string _alternateusername;
        private string _LdapId;//6650


        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

		/// <summary>
		/// 
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _emailaddress;
			}
			set
			{
				_emailaddress = value;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public string Company
		{
			get
			{
				return _company;
			}
			set
			{
				_company = value;
			}
		}
        //6650 starts
        public string LdapId
        {
            get
            {
                return _LdapId;
            }
            set
            {
                _LdapId = value;
            }
        }
        //6650 ends

		/// <summary>
		/// The Name of the User.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
		/// <summary>
		/// The users's identity.
		/// </summary>
		public string Identity
		{
			get
			{
				return _identity;
			}
			set
			{
				_identity = value;
			}
		}
        // The user's LDAP ID.
        public string AlternateUserName
        {
            get
            {
                return _alternateusername;
            }
            set
            {
                _alternateusername = value;
            }
        }
		/// <summary>
		/// 
		/// </summary>
		public class EmailComparer: System.Collections.IComparer
		{
			/// <summary>
			/// 
			/// </summary>
			/// <param name="firstObj"></param>
			/// <param name="secondObj"></param>
			/// <returns></returns>
			public int Compare(object firstObj, object secondObj)
			{
				if((firstObj is ShallowUser) && (secondObj is ShallowUser))
				{
					ShallowUser firstUser = (ShallowUser)firstObj;
					ShallowUser secondUser = (ShallowUser)secondObj;
					return firstUser.EmailAddress.CompareTo(secondUser.EmailAddress);
				}
				else
				{
					throw new ArgumentException("objects not of type sony.us.siam.shallowUser");
				}
			}
		}


		/// <summary>
		/// 
		/// </summary>
		public class CompanyComparer: System.Collections.IComparer
		{
			/// <summary>
			/// 
			/// </summary>
			/// <param name="firstObj"></param>
			/// <param name="secondObj"></param>
			/// <returns></returns>
			public int Compare(object firstObj, object secondObj)
			{
				if((firstObj is ShallowUser) && (secondObj is ShallowUser))
				{
					ShallowUser firstUser = (ShallowUser)firstObj;
					ShallowUser secondUser = (ShallowUser)secondObj;
					return firstUser.Company.CompareTo(secondUser.Company);
				}
				else
				{
					throw new ArgumentException("objects not of type sony.us.siam.shallowUser");
				}
			}
		}
        //6650 starts
        /// <summary>
        /// 
        /// </summary>
        public class LdapIdComparer : System.Collections.IComparer
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="firstObj"></param>
            /// <param name="secondObj"></param>
            /// <returns></returns>
            public int Compare(object firstObj, object secondObj)
            {
                if ((firstObj is ShallowUser) && (secondObj is ShallowUser))
                {
                    ShallowUser firstUser = (ShallowUser)firstObj;
                    ShallowUser secondUser = (ShallowUser)secondObj;
                    return firstUser.LdapId.CompareTo(secondUser.LdapId);
                 }
                else
                {
                    throw new ArgumentException("objects not of type sony.us.siam.shallowUser");
                }
            }
        }
        //6650 ends
	}
}
