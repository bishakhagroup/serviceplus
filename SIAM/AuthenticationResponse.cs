using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define an AuthenticationResult. Used by SIAM to return Authentication results
	/// to the caller.
	/// </summary>
	[Serializable]
	public class AuthenticationResult : SiamResponse
	{
		/// <summary>
		/// The <see cref="User"/> object obtained when a successfull authentication takes place.
		/// </summary>
		public User user;
		/// <summary>
		/// A boolean value indicating success or failure.
		/// </summary>
		public bool Result = false;
		/// <summary>
		/// The credentials to be used during the authentication process.
		/// </summary>
		public Credential credential;
	}
}
