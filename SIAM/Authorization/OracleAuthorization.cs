using System;
using Oracle.ManagedDataAccess.Client;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting;


namespace Sony.US.siam.Authorization
{

	internal class OracleAuthorization:IAuthorization
	{
		private const string ClassName = "OracleAuthorization";

		internal OracleAuthorization()
		{

		}
		
		public AuthorizationResult Authorize(User user,string application,string resource,string Action)
		{

			return new AuthorizationResult();
		}

		public AccessRight[] GetAccessRights(User user,string Application)
		{
			return new AccessRight[1];
		}
	}
}
