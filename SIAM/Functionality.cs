using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
    /// A serialiable object used to contain a Functionality definition.
	/// </summary>
	[Serializable]
    public class Functionality
	{
        private string _name;
        private string _mappedURL;
        private int _Functionalityid;
        private string _GroupName;
        private int _status = 0;
        ///// <summary>
        ///// An array of <see cref="AccessRight"/> objects.
        ///// </summary>
        //public AccessRight[] AccessRights;
		/// <summary>
        /// An integer value containing the Functionality id.
		/// </summary>
        public int FunctionalityId
		{
			get
			{
                return _Functionalityid;
			}
			set
			{
                _Functionalityid = value;
			}
		}

		/// <summary>
        /// The Name of the Functionality.
		/// </summary>
        public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}
        public string GroupName
        {
            get
            {
                return _GroupName;
            }
            set
            {
                _GroupName = value;
            }
        }
        public string URLMapped
        {
            get
            {
                return _mappedURL;
            }
            set
            {
                _mappedURL = value;
            }
        }

        /// <summary>
        /// Set as 1 for Selected and 0 for Available for a selected user. THis is only for getting selected and available record in one fetch
        /// </summary>
        public int AvailableStatus
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

		/// <summary>
        /// An array of <see cref="Role"/> objects of Child Functionalites for this Functionality.
		/// </summary>
        //public Functionality[] Children;
	}
}
