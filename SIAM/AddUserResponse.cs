using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable objet used to define an AddUserResponse in SIAM.
	/// </summary>
	[Serializable]
	public class AddUserResponse : SiamResponse
	{
		/// <summary>
		/// An instance of <see cref="User"/> that results from a successfull call to AddUser.
		/// </summary>
		public User User;
	}
}
