using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// The UpdateUserResponse is used as the return value for an update user method call in SIAM.
	/// </summary>
	[Serializable]
	public class UpdateUserResponse : SiamResponse
	{
		/// <summary>
		/// An instance of <see cref="User"/>
		/// </summary>
		public User User;
	}
}
