using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to encapsulate the return value of GetUser calls.
	/// </summary>
	[Serializable]
	public class FetchUserResponse : SiamResponse
	{
		/// <summary>
		/// An instance of <see cref="User"/> will contain the User object upon return
		/// from a successfull GetUser call.
		/// </summary>
		public User TheUser;
	}
}
