using System;
using Oracle.ManagedDataAccess.Client;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Remoting;

namespace Sony.US.siam.Authentication
{
	/// <summary>
	/// The OracleAuthenticator class implements those "authentication" related functions that apply
	/// to the Oracle back end. Like administration, Authentication mechanisms can differ for each user type
	/// defined in SIAM. 
	/// </summary>
	internal class OracleAuthenticator   //:Sony.US.siam.Interfaces.IAuthentication
	{

		#region Class Variables

		/// <summary>
		/// 
		/// </summary>
		//private SonyLogger Logger = new SonyLogger();

		/// <summary>
		/// A constant value used by logging.
		/// </summary>
		private const string ClassName = "OracleAuthenticator";
		
		//private sony.us.siam.SIAMConfig config;

		#endregion

		#region Constructor

		/// <summary>
		/// No outside instantial is avaiable for this class. Consumed excluseively by SIAM's class factories.
		/// Notice also that unlike the OracleAdministrator class this class does NOT require constructor Args.
		/// Although it could define these in configuration, no connection string is required by this class.
		/// This class simply uses the current DAL to do it's work. (less coding).
		/// </summary>
		internal OracleAuthenticator()
		{

		}

		#endregion

		#region Public Implementatoin of IAuthentication.


		/// <summary>
		/// The Authenticate method takes an instance of sony.us.siam.AuthenticationResult and acts
		/// much like a "stamp" in that it validates the Result and stamps it with a result code
		/// defined in sony.us.siam.Messages. 
		/// </summary>
		/// <param name="results">An instance of sony.us.siam.AuthenticationResult</param>
		/// <returns>An instance of sony.us.siam.AuthenticationResult.</returns>
        public void Authenticate(AuthenticationResult results)
        {
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    if (results.user.Password != results.credential.Password)
        //    {
        //        results.Result = false;
        //        results.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADPASSWORD;
        //        results.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString();
        //        //SIAMConfig config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
        //        if (new UtilityMethods().CheckPasswordAttempts(results.user,results.credential))
        //        {
        //            new SecurityAdministrator().LockAccount(results.user);
        //            results.Result = false;
        //            results.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
        //            results.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
        //        }
        //    }
        //    else
        //    {
        //        if (results.user.LockStatus == 1)
        //        {
        //            results.Result = false;
        //            results.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED;
        //            results.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString();
        //        }
        //        else
        //        {
        //            //if (System.DateTime.Now > System.DateTime.Parse(results.user.PasswordExpirationDate))
        //            //{
        //            //	results.Result = false;
        //            //	results.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_EXPIREDPASSWORD;
        //            //	results.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_EXPIREDPASSWORD.ToString();
        //            //}
        //            //else
        //            //{
        //            results.Result = true;
        //            results.user = results.user;
        //            results.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
        //            results.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
        //            //}
        //        }
        //    }
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,results.SiamResponseMessage,TraceLevel.Info);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return;
        }

        public void GetSecurityPIN(AuthenticationResult result)
        {
            //Do nothing
        }


#endregion
	}
}
