using System;
using System.Xml.Serialization;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable base container for all SIAM response values that are publicly exposed. This class
	/// is used throughout SIAM as a return value. SIAM will make every effort to avoid throwing exceptions.
	/// Instead return values defined in <see cref="Messages"/> will be returned using this class.
	/// In addition this class defines a SiamPayLoad member. This member is generically typed as <see cref="System.Object"/>
	/// and is used to contain any return values sent back by the method call. For example, one might call 
	/// GetUser(). If successfull a SiamResponse object will be sent back with a successfull response code
	/// and Response Message. The SiamPayLoad member will contain the instance of <see cref="User"/> that
	/// is expected as the return value of GetUser.
	/// </summary>
	[Serializable]
	public class SiamResponse
	{
		/// <summary>
		/// The generically typed member that contains return values for all method calls in SIAM.
		/// </summary>
		public object SiamPayLoad;
		/// <summary>
		/// The response code as defined by <see cref="Messages"/>
		/// </summary>
		public int SiamResponseCode;
		/// <summary>
		/// The response message as defined by <see cref="Messages"/>
		/// </summary>
		public string SiamResponseMessage;
		/// <summary>
		/// The inner response message <see cref="Messages"/>
		/// </summary>
		public string SiamInnerResponseMessage = "";
	}
}
