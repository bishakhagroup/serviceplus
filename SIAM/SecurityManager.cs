using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;
using System.Diagnostics;
using Oracle.ManagedDataAccess.Client;
using System.Reflection;
using System.Runtime.Remoting;

namespace Sony.US.siam
{
	/// <summary>
	/// 
	/// </summary>
	public class SecurityManager
	{

		#region Private Class Variables

		/// <summary>
		/// 
		/// </summary>
		//private SonyLogger Logger = new SonyLogger();

		/// <summary>
		/// A constant value used by Logging.
		/// </summary>
		private const string ClassName = "SecurityManager";
		/// <summary>
		/// An instance of <see cref="ISecurityAdministrator"/> that is defined
		/// by configuration and represents the current data access layer in user by SIAM.
		/// </summary>
		private Interfaces.ISecurityAdministrator _dal = null;
		/// <summary>
		/// An enumeration of Delegate types used for SIAM Authentication.
		/// </summary>
		private enum DelegateTypes {AuthorizationDelegates,AuthenticationDelegates};
		/// <summary>
		/// An instance of <see cref="SIAMConfig"/> that wraps SIAM configuration.
		/// </summary>
        /// Commented this line due to merge of both Configuration files. Used ConfigurationData.Environment instead
        //private SIAMConfig config = null;
		#endregion

		#region Constructor


		/// <summary>
		/// Security Manager constructor requires no arguments.
		/// </summary>
		public SecurityManager()
		{
            //config = CachingServices.Cache["SiamConfiguration"] as SIAMConfig;
            //if (config == null)
            //{
            //    config = new SIAMConfig();
            //    CachingServices.Cache["SiamConfiguration"] = config;
            //}
            _dal = new DelegateHandler().GetCurrentDal();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Authorize takes an instance of sony.us.saim.User, an application name, a resource name,
		/// and an action name and returns an Authorization result that indicates whether that user
		/// has the AccessRight requested. An instance of sony.us.siam.User will have a an array of
		/// Roles that contain access rights as well as an Array of Effective Access Rights that
		/// represents the aggregate Access Rights for all that users associated roles. It is not
		/// necessary or required to call this method to evaluate whether a user has an access right.
		/// The code consuming SIAM security services may iterate the Roles / Access Rights on a given
		/// user object to obtain to the same result. The benifit to calling this method is that prior
		/// to issueing an Authorization result Authorize will obtain the latest Roles and Access Rights
		/// that have been associated with that user.
		/// NOTE: For the Alpha and possibly Beta releases of SIAM no authorization delegate is used.
		/// The siam backend is the only system with authorization capabilities so we will go directy
		/// to the dal to get updated access rights and make an authorization decision here.
		/// </summary>
		/// <param name="user">An instance of sony.us.siam.User</param>
		/// <param name="Application">The name of the application for the access right being evaluated</param>
		/// <param name="Resource">The name of the resource for the access right being evaluated</param>
		/// <param name="Action">The name of the action for the access right being evaluated</param>
		/// <returns>An instance of sony.us.siam.AuthorizationResult</returns>
		public AuthorizationResult Authorize(User user,string Application,string Resource,string Action)
		{
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			string ErrorString = "";
			AuthorizationResult result = new AuthorizationResult();
			AccessRight[] rights = _dal.GetEffectiveAccessRights(user.Roles,out ErrorString);	
			if (rights == null)
			{
				// dont grant the right
				result.SiamResponseCode = (int)Messages.ACCESSRIGHT_NOT_GRANTED;
				result.SiamResponseMessage = Messages.ACCESSRIGHT_NOT_GRANTED.ToString();
			}
			else
			{
				for (int x = 0; x <= rights.GetUpperBound(0);x++)
				{
					if (rights[x].Application.Name == Application && rights[x].Resource.Name == Resource && rights[x].Action.Name == Action)
					{
						// Grant the right
						result.SiamResponseCode = (int)Messages.ACCESSRIGHT_GRANTED;
						result.SiamResponseMessage = Messages.ACCESSRIGHT_GRANTED.ToString();
						break;
					}
					else
					{
						// don't grant the right
						result.SiamResponseCode = (int)Messages.ACCESSRIGHT_NOT_GRANTED;
						result.SiamResponseMessage = Messages.ACCESSRIGHT_NOT_GRANTED.ToString();
					}
				}
			}
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
			//Logger.RaiseTrace(ClassName,result.SiamResponseMessage,TraceLevel.Info);
			return result;
		}

		/// <summary>
		/// Authenticate takes an instance of <see cref="Credential"/> containing a user name
		/// and password values  and attempts to authenticate them against the currently defined Authentication
		/// delegate class. This class will be defined in SIAM. This Allows SIAM to authenticate different user
		/// types in different ways. For example, some user types might be authenticated against SIAM's backend
		/// whether that be oracle or Sql. Other user types, may be authenticated against an LDAP directory server.
		/// </summary>
		/// <param name="credentials">An instance of <see cref="Credential"/></param>
		/// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
		/// of the call.</returns>
        public AuthenticationResult Authenticate(Credential credentials)
        {
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
        //    AuthenticationResult authenticationResult = new AuthenticationResult();
        //    string ErrorString;
        //    authenticationResult.credential = credentials;
        //    _dal.GetUser(authenticationResult,out ErrorString);
        //    if (authenticationResult.SiamResponseCode == (int)Messages.AUTHENTICATE_SUCCESS)
        //    {
        //        if (authenticationResult.user == null)
        //        {
        //            authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADUSERID;
        //            authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADUSERID.ToString();
        //            if (ErrorString != "")
        //                authenticationResult.SiamResponseMessage += " " + ErrorString;
        //            authenticationResult.Result = false;
        //            //Logger.RaiseTrace(ClassName,authenticationResult.SiamResponseMessage,TraceLevel.Error);
        //        }
        //        else
        //        {
        //            if (authenticationResult.credential.IsInternalUser == true)
        //            {
        //                authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
        //                authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
        //            }
        //            else
        //            {
        //                IAuthentication _authenticator = new DelegateHandler().GetAuthenticationAuthority(authenticationResult.user.UserType);
        //                if (_authenticator != null)
        //                {
        //                    _authenticator.Authenticate(authenticationResult);
        //                    if (authenticationResult.Result == true)
        //                    {
        //                        authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
        //                        authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
        //                        if (authenticationResult.user.UserType != "NonAccount")
        //                        {
        //                            _dal.UpdateNextQuestionIndex(authenticationResult.user.Identity,GetNextValue(authenticationResult.user),out ErrorString);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    authenticationResult.Result = false;
        //                    authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_NODELEGATE;
        //                    authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_NODELEGATE.ToString();
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_BADUSERID;
        //        //authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_BADUSERID.ToString();
        //        if (ErrorString != "")
        //            authenticationResult.SiamResponseMessage += " " + ErrorString;
        //        authenticationResult.Result = false;
        //        //Logger.RaiseTrace(ClassName, authenticationResult.SiamResponseMessage, TraceLevel.Error);
        //    }
			
        //    //Logger.RaiseTrace(ClassName,authenticationResult.SiamResponseMessage,TraceLevel.Info);
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            return null;
        }

        /// <summary>
        /// Returns security answer from LDAP
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        //public void GetUserPIN(ref AuthenticationResult authenticationResult)
        //{
        //    TimeSpan start = TimeKeeper.GetCurrentTime();
        //    //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);
        //    string ErrorString = string.Empty;
        //    {
        //        if (authenticationResult.credential.IsInternalUser == true)
        //        {
        //            authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
        //            authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
        //        }
        //        else
        //        {
        //            IAuthentication _authenticator = new DelegateHandler().GetAuthenticationAuthority(authenticationResult.user.UserType.ToString());
        //            if (_authenticator != null)
        //            {
        //                _authenticator.GetSecurityPIN(authenticationResult);
        //                if (authenticationResult.Result == true)
        //                {
        //                    authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_SUCCESS;
        //                    authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_SUCCESS.ToString();
        //                }
        //            }
        //            else
        //            {
        //                authenticationResult.Result = false;
        //                authenticationResult.SiamResponseCode = (int)Messages.AUTHENTICATE_FAILURE_NODELEGATE;
        //                authenticationResult.SiamResponseMessage = Messages.AUTHENTICATE_FAILURE_NODELEGATE.ToString();
        //            }
        //        }
        //    }
        //    //Logger.RaiseTrace(ClassName, authenticationResult.SiamResponseMessage, TraceLevel.Info);
        //    TimeSpan end = TimeKeeper.GetCurrentTime();
        //    TimeSpan exectime = start.Subtract(end);
        //    //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);
        //}


#endregion

		#region Helper Methods

		/// <summary>
		/// Gets the next value for the Security Question.
		/// </summary>
		/// <param name="user">An instance of <see cref="User"/></param>
		/// <returns>An instance of <see cref="SiamResponse"/> that will contain the results
		/// of the call.</returns>
        //internal int GetNextValue(User user)
        //{
        //    int newValue = 0;
        //    //for (int x = 0; x <= config.UserTypes.GetUpperBound(0); x++)
        //    for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
        //    {
        //        //if (config.UserTypes[x].Name == user.UserType)
        //        if (ConfigurationData.Environment.UserTypes[x].Name == user.UserType)
        //        {
        //            //if (config.UserTypes[x].LoginPolicy.SecurityQuestions != null)
        //            if (ConfigurationData.Environment.UserTypes[x].LoginPolicy.SecurityQuestions != null)
        //            {
        //                //if (user.ThirdID.NextQuestionIndex < config.UserTypes[x].LoginPolicy.SecurityQuestions.Length - 1)
        //                if (user.ThirdID.NextQuestionIndex < ConfigurationData.Environment.UserTypes[x].LoginPolicy.SecurityQuestions.Length - 1)
        //                {
        //                    newValue = user.ThirdID.NextQuestionIndex + 1;
        //                    break;
        //                }
        //                else
        //                {
        //                    newValue = 0;
        //                    break;
        //                }

        //            }
        //        }
        //    }
        //    return newValue;
        //}

		#endregion
	}
}
