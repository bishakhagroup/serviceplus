using System;
using Sony.US.siam;
using Sony.US.SIAMUtilities;
using Sony.US.siam.Interfaces;
using Sony.US.siam.Administration;
using Sony.US.siam.Authentication;
using Sony.US.siam.Authorization;

namespace Sony.US.siam
{
	/// <summary>
	/// A serializable object used to define an AuthriizationResult.
	/// Note: Devalued. May not be included in the final API
	/// </summary>
	[Serializable]
	public class AuthorizationResult : SiamResponse
	{
	}
}
