using System;

namespace SFTP.jsch
{
	

	public interface MAC
	{
		String getName();
		int getBlockSize(); 
		void init(byte[] key);
		void update(byte[] foo, int start, int len);
		void update(int foo);
		byte[] doFinal();
	}

}
