using System;

namespace SFTP.jsch
{
	

	internal interface Identity
	{
		bool setPassphrase(String passphrase);
		byte[] getPublicKeyBlob();
		byte[] getSignature(Session session, byte[] data);
		bool decrypt();
		String getAlgName();
		String getName();
		bool isEncrypted();
	}
}
