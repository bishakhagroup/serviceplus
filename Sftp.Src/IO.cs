using System;
using System.IO;

namespace SFTP.jsch
{
	

	public class IO
	{
		internal Stream ins;
		internal Stream outs;
		internal Stream out_ext;

		internal void setOutputStream(Stream outs)
		{
			this.outs=outs;
		}
		internal void setExtOutputStream(Stream outs)
		{
			this.out_ext=outs;
		}
		internal void setInputStream(Stream ins)
		{
			this.ins=ins;
		}
		public void put(Packet p)
		{
			outs.Write(p.buffer.buffer, 0, p.buffer.index);
			outs.Flush();
		}
		internal void put(byte[] array, int begin, int length)
		{
			outs.Write(array, begin, length);
			outs.Flush();
		}
		internal void put_ext(byte[] array, int begin, int length)
		{
			out_ext.Write(array, begin, length);
			out_ext.Flush();
		}

		internal int getByte()
		{
			int res = ins.ReadByte()&0xff;
			return res; 
		}

		internal void getByte(byte[] array)
		{
			getByte(array, 0, array.Length);
		}

		internal void getByte(byte[] array, int begin, int length)
		{
			do
			{
				int completed = ins.Read(array, begin, length);
				if(completed<=0)
				{
					throw new IOException("");
				}
				begin+=completed;
				length-=completed;
			}
			while (length>0);
		}

		public void finalize()
		{
			try
			{
				if(ins!=null) ins.Close();
			}
			catch{}
			try
			{
				if(outs!=null) outs.Close();
			}
			catch{}
		}
	}

}
