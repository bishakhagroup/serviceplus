using System;
using System.Threading;

namespace SFTP.jsch
{
	

	public class ChannelShell : ChannelSession
	{
		internal bool xforwading=false;
		/*
		ChannelShell(){
		  super();
		  type="session".getBytes();
		  io=new IO();
		}
		*/
		public override void setXForwarding(bool foo)
		{
			xforwading=true;
		}
		public override void start()
		{
			try
			{
				Request request;
				if(xforwading)
				{
					request=new RequestX11();
					request.request(session, this);
				}
				request=new RequestPtyReq();
				request.request(session, this);
				request=new RequestShell();
				request.request(session, this);
			}
			catch//(Exception e)
			{
			}
			thread=new Thread(new ThreadStart(this.run));
			thread.Start();
		}
		public override void finalize() 
		{
			if(thread!=null)
			{
				thread.Interrupt();
				thread=null;
			}
			base.finalize();
		}
		public override void init()
		{
			io.setInputStream(session.ins);
			io.setOutputStream(session.outs);
		}
		public override void run()
		{
			//    thread=Thread.currentThread();
			Buffer buf=new Buffer();
			Packet packet=new Packet(buf);
			int i=0;
			try
			{
				while(isConnected() &&
					thread!=null && 
					io!=null && 
					io.ins!=null)
							   {
								   i=io.ins.Read(buf.buffer, 14, buf.buffer.Length-14);
								   if(i==0)continue;
								   if(i==-1)
								   {
									   eof();
									   break;
								   }
								   if(_close)break;
								   packet.reset();
								   buf.putByte((byte)Session.SSH_MSG_CHANNEL_DATA);
								   buf.putInt(recipient);
								   buf.putInt(i);
								   buf.skip(i);
								   session.write(packet, this, i);
							   }
			}
			catch//(Exception e)
			{
				//System.out.println("ChannelShell.run: "+e);
			}
			thread=null;
		}

		public void setPtySize(int row, int col, int wp, int hp)
		{
			//if(thread==null) return;
			try
			{
				RequestWindowChange request=new RequestWindowChange();
				request.setSize(row, col, wp, hp);
				request.request(session, this);
			}
			catch(Exception e)
			{
				Console.WriteLine("ChannelShell.setPtySize: "+e);
			}
		}
	}

}
