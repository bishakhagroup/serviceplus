using System;
using System.IO;

namespace SFTP.jsch
{
	
	public interface Proxy
	{
		void connect(Session session, String host, int port);
		Stream getInputStream();
		Stream getOutputStream();
		void close();
	}
}
