using System;

namespace SFTP.jsch
{
	
	public interface KeyPairGenRSA
	{
		void init(int key_size);
		byte[] getD();
		byte[] getE();
		byte[] getN();

		byte[] getC();
		byte[] getEP();
		byte[] getEQ();
		byte[] getP();
		byte[] getQ();
	}

}
