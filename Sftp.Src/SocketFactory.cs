using System;
using System.Net.Sockets;
using System.IO;

namespace SFTP.jsch
{
	
	public interface SocketFactory
	{
		Socket createSocket(String host, int port);
		Stream getInputStream(Socket socket);
		Stream getOutputStream(Socket socket);
	}
}
