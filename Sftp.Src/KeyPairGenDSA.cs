using System;

namespace SFTP.jsch
{

	public interface KeyPairGenDSA
	{
		void init(int key_size);
		byte[] getX();
		byte[] getY();
		byte[] getP();
		byte[] getQ();
		byte[] getG();
	}

}
