Public Class ShoppingCartItem
    Inherits CoreObject

    Public Enum ItemOriginationType
        Search
        Manual_Entry
        My_Catalog
    End Enum

    Public Enum DeliveryMethod
        Ship = 0
        Download = 1
        DownloadAndShip = 2
        PrintOnDemand = 3
        ClassRoomTraining = 4
        OnlineTraining = 5
        Certification = 6
    End Enum
    Public Shared PurchaseLimit As Integer = 50000

    Private m_cart As ShoppingCart
    Private m_line_number As String
	Private m_product As Product
	Private m_original_partnumber As String		' Added for Shopping Cart enhancement to keep track of the part that was replaced.
	Private m_quantity As Integer
	Private m_your_price As Double
	Private m_haskit As Boolean
	Private m_kititem As KitItem()
	Private m_num_parts_available As Integer
	Private m_origin As ItemOriginationType
	Private m_comment As String
	Private m_delivery_channel As DeliveryMethod

    Public Sub New(ByRef product As Product, ByVal number_of_parts_available As Integer, ByVal your_price As Double, ByRef cart As ShoppingCart)
        m_product = product
        m_num_parts_available = number_of_parts_available
        m_your_price = your_price
        m_quantity = 1
        m_delivery_channel = DeliveryMethod.Ship
        m_cart = cart
    End Sub

    Public Property haskit() As Boolean
        Get
            Return m_haskit
        End Get
        Set(ByVal Value As Boolean)
            m_haskit = Value
        End Set
	End Property

    Public Property kit() As KitItem()
        Get
            Return m_kititem
        End Get
        Set(ByVal Value As KitItem())
            m_kititem = Value
        End Set
	End Property

    Public Property ShoppingCart() As ShoppingCart
        Get
            Return m_cart
        End Get
        Set(ByVal Value As ShoppingCart)
            m_cart = Value
        End Set
    End Property

    Public Property LineNumber() As String
        Get
            Return m_line_number
        End Get
        Set(ByVal Value As String)
            m_line_number = Value
        End Set
    End Property

    Public Property Product() As Product
        Get
            Return m_product
        End Get
        Set(ByVal Value As Product)
            m_product = Value
        End Set
	End Property

	Public Property OriginalPartNumber() As String
		Get
			Return IIf(String.IsNullOrEmpty(m_original_partnumber), Nothing, m_original_partnumber)
		End Get
		Set(ByVal Value As String)
			m_original_partnumber = Value
		End Set
	End Property

    Public Property DeliveryChannel() As DeliveryMethod
        Get
            Return m_delivery_channel
        End Get
        Set(ByVal Value As DeliveryMethod)
			m_delivery_channel = Value
        End Set
    End Property

    Public Property Comment() As String
        Get
            Return m_comment
        End Get
        Set(ByVal Value As String)
            m_comment = Value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Return m_quantity
        End Get
        Set(ByVal Value As Integer)
			m_quantity = Value
        End Set
    End Property

    Public Property YourPrice() As Double
        Get
            Return m_your_price
        End Get
        Set(ByVal Value As Double)
            m_your_price = Value
        End Set
    End Property

    Public ReadOnly Property TotalLineItemPrice() As Double
        Get
            Return m_your_price * m_quantity
        End Get
    End Property

    Public Property NumberOfPartsAvailable() As Integer
        Get
            Return m_num_parts_available
        End Get
        Set(ByVal Value As Integer)
            m_num_parts_available = Value
        End Set
    End Property

    Public Property Origin() As ItemOriginationType
        Get
            Return m_origin
        End Get
        Set(ByVal Value As ItemOriginationType)
            m_origin = Value
        End Set
    End Property

End Class
