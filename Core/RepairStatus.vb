Public Class RepairStatus
    Private m_MODELNUMBER As String = String.Empty
    Private m_SERIALNUMBER As String = String.Empty
    Private m_CUSTOMERPHONE As String = String.Empty
    Private m_NOTIFICATIONNUMBER As String = String.Empty
    Private m_STATUS As String = String.Empty
    Private m_CARRIERCODE As String = String.Empty
    Private m_TRACKINGNUMBER As String = String.Empty
    Private m_CUSTOMERSHIPTO As String = String.Empty
    Private m_PLANTNUMBER As String = String.Empty
    Private m_PLANTPHONE As String = String.Empty
    Private m_PLANTEMAILADDRESS As String = String.Empty
    Private m_TRACKINGURL As String = String.Empty
    'Added by Sneha for bug 9014
    Private m_oldmodelnumber As String = String.Empty


    Public Property MODELNUMBER() As String
        Get
            Return m_MODELNUMBER
        End Get
        Set(ByVal value As String)
            m_MODELNUMBER = value
        End Set
    End Property
    'Added by Sneha for Bug 9014
    Public Property OLDMODELNUMBER() As String
        Get
            Return m_oldmodelnumber
        End Get
        Set(ByVal value As String)
            m_oldmodelnumber = value
        End Set
    End Property
    Public Property SERIALNUMBER() As String
        Get
            Return m_SERIALNUMBER
        End Get
        Set(ByVal value As String)
            m_SERIALNUMBER = value
        End Set
    End Property
    Public Property CUSTOMERPHONE() As String
        Get
            Return m_CUSTOMERPHONE
        End Get
        Set(ByVal value As String)
            m_CUSTOMERPHONE = value
        End Set
    End Property
    Public Property NOTIFICATIONNUMBER() As String
        Get
            Return m_NOTIFICATIONNUMBER
        End Get
        Set(ByVal value As String)
            m_NOTIFICATIONNUMBER = value
        End Set
    End Property
    Public Property STATUS() As String
        Get
            Return m_STATUS
        End Get
        Set(ByVal value As String)
            m_STATUS = value
        End Set
    End Property
    Public Property CARRIERCODE() As String
        Get
            Return m_CARRIERCODE
        End Get
        Set(ByVal value As String)
            m_CARRIERCODE = value
        End Set
    End Property
    Public Property TRACKINGNUMBER() As String
        Get
            Return m_TRACKINGNUMBER
        End Get
        Set(ByVal value As String)
            m_TRACKINGNUMBER = value
        End Set
    End Property
    Public Property CUSTOMERSHIPTO() As String
        Get
            Return m_CUSTOMERSHIPTO
        End Get
        Set(ByVal value As String)
            m_CUSTOMERSHIPTO = value
        End Set
    End Property
    Public Property PLANTNUMBER() As String
        Get
            Return m_PLANTNUMBER
        End Get
        Set(ByVal value As String)
            m_PLANTNUMBER = value
        End Set
    End Property
    Public Property PLANTPHONE() As String
        Get
            Return m_PLANTPHONE
        End Get
        Set(ByVal value As String)
            m_PLANTPHONE = value
        End Set
    End Property
    Public Property PLANTEMAILADDRESS() As String
        Get
            Return m_PLANTEMAILADDRESS
        End Get
        Set(ByVal value As String)
            m_PLANTEMAILADDRESS = value
        End Set
    End Property
    Public Property TRACKINGURL() As String
        Get
            Return m_TRACKINGURL
        End Get
        Set(ByVal value As String)
            m_TRACKINGURL = value
        End Set
    End Property

End Class
