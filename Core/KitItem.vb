Public Class KitItem

    Private m_product As product
    Private m_quantity As Integer
    Private m_num_parts_available As Integer
    Private m_your_price As Double

    Public Sub New()
        m_product = New Part
    End Sub

    Public Property Product() As Product
        Get
            Return m_product
        End Get
        Set(ByVal Value As Product)
            m_product = Value
        End Set
    End Property

    Public Property NumberOfPartsAvailable() As Integer
        Get
            Return m_num_parts_available
        End Get
        Set(ByVal Value As Integer)
            m_num_parts_available = Value
        End Set
    End Property

    Public Property YourPrice() As Double
        Get
            Return m_your_price
        End Get
        Set(ByVal Value As Double)
            m_your_price = Value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Return m_quantity 'have to pay attention to kit quantity and multiply the two together.
        End Get
        Set(ByVal Value As Integer)
            m_quantity = Value
        End Set
    End Property

    Public ReadOnly Property TotalKitItemPrice() As Double
        Get
            Return m_your_price * m_quantity
        End Get
    End Property

End Class
