Public Class CreditCardException
    Inherits CoreObject

    Private m_credit_card_exception_id As String
    Private m_sequence_number As String
    Private m_order As Order
    Private m_invoice As Invoice
    Private m_tax As Double
    Private m_shipping As Double
    Private m_total_amount As Double
    Private m_bad_credit_card As CreditCard
    Private m_new_credit_card As CreditCard
    Private m_comments As String
    Private m_complete As Boolean
    Private m_completed_by As String
    Private m_completed_date As Date
    Private m_credit_card_action As CreditCard.CreditCardAction
    Private m_reprocess_date As Date
    Private m_verisign_txn_code As String
    Private m_verisign_auth_code As String
    Private m_fullyLoaded As Boolean = False

    ' added new for getting other exception details based on processed flag
    ' added by Deepa V Mar 8,2006
    Private m_processedflag As String
    Private m_exp_order_number As String
    Private m_exp_invoice_number As String
    'ADDED BY CHANDRA ON 01/24/2008 TO GET CUSTOMER ID FROM GETCREDITCARDEXCEPTION STORED PROCEDURE
    Private m_customerid As String = String.Empty


    Public Sub New()
    End Sub

    'ADDED BY CHANDRA ON 01/24/2008 TO GET CUSTOMER ID FROM GETCREDITCARDEXCEPTION STORED PROCEDURE
    Public Property CustomerID() As String
        Get
            Return m_customerid
        End Get
        Set(ByVal Value As String)
            SetProperty(m_customerid, Value)
        End Set
    End Property

    ' property added new for processed flag
    ' added by Deepa V Mar 8,2006
    Public Property Processedflag() As String
        Get
            Return m_processedflag
        End Get
        Set(ByVal Value As String)
            SetProperty(m_processedflag, Value)
        End Set
    End Property

    Public Property ExpOrderNumber() As String
        Get
            Return m_exp_order_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_exp_order_number, Value)
        End Set
    End Property

    Public Property ExpInvoiceNumber() As String
        Get
            Return m_exp_invoice_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_exp_invoice_number, Value)
        End Set
    End Property
    'end of addition

    Public Property CreditCardExceptionID() As String
        Get
            Return m_credit_card_exception_id
        End Get
        Set(ByVal Value As String)
            m_credit_card_exception_id = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    Public Property Order() As Order
        Get
            Return m_order
        End Get
        Set(ByVal Value As Order)
            m_order = Value
        End Set
    End Property
    Public ReadOnly Property OrderNumber() As String
        Get
            If Not m_order Is Nothing Then
                Return m_order.OrderNumber
            Else
                Return ""
            End If
        End Get
    End Property

    Public Property Invoice() As Invoice
        Get
            Return m_invoice
        End Get
        Set(ByVal Value As Invoice)
            m_invoice = Value
        End Set
    End Property

    Public ReadOnly Property InvoiceNumber() As String
        Get
            If Not m_invoice Is Nothing Then
                Return m_invoice.InvoiceNumber
            Else
                Return ""
            End If
        End Get
    End Property

    Public Property Tax() As Double
        Get
            Return m_tax
        End Get
        Set(ByVal Value As Double)
            m_tax = Value
        End Set
    End Property

    Public Property Shipping() As Double
        Get
            Return m_shipping
        End Get
        Set(ByVal Value As Double)
            m_shipping = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return m_total_amount
        End Get
        Set(ByVal Value As Double)
            m_total_amount = Value
        End Set
    End Property

    Public Property BadCreditCard() As CreditCard
        Get
            Return m_bad_credit_card
        End Get
        Set(ByVal Value As CreditCard)
            m_bad_credit_card = Value
        End Set
    End Property

    Public Property NewCreditCard() As CreditCard
        Get
            Return m_new_credit_card
        End Get
        Set(ByVal Value As CreditCard)
            m_new_credit_card = Value
        End Set
    End Property

    Public Property Comments() As String
        Get
            Return m_comments
        End Get
        Set(ByVal Value As String)
            SetProperty(m_comments, Value)
        End Set
    End Property

    Public Property Complete() As Boolean
        Get
            Return m_complete
        End Get
        Set(ByVal Value As Boolean)
            SetProperty(m_complete, Value)
        End Set
    End Property

    Public Property CompletedBy() As String
        Get
            Return m_completed_by
        End Get
        Set(ByVal Value As String)
            SetProperty(m_completed_by, Value)
        End Set
    End Property

    Public Property CompletedDate() As Date
        Get
            Return m_completed_date
        End Get
        Set(ByVal Value As Date)
            SetProperty(m_completed_date, Value)
        End Set
    End Property

    Public Property CreditCardAction() As CreditCard.CreditCardAction
        Get
            Return m_credit_card_action
        End Get
        Set(ByVal Value As CreditCard.CreditCardAction)
            m_credit_card_action = Value
        End Set
    End Property

    Public Property ReprocessDate() As Date
        Get
            Return m_reprocess_date
        End Get
        Set(ByVal Value As Date)
            SetProperty(m_reprocess_date, Value)
        End Set
    End Property

    Public Property VerisignTransactionCode() As String
        Get
            Return m_verisign_txn_code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_verisign_txn_code, Value)
        End Set
    End Property

    Public Property VerisignAuthCode() As String
        Get
            Return m_verisign_auth_code
        End Get
        Set(ByVal Value As String)
            m_verisign_auth_code = Value
        End Set
    End Property

    Public Property FullyLoaded() As Boolean
        Get
            Return m_fullyLoaded
        End Get
        Set(ByVal Value As Boolean)
            m_fullyLoaded = Value
        End Set
    End Property
End Class
