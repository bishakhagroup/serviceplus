Public Class Software
    Inherits Product
    Implements IComparable(Of Software)

    'Public Enum FreeOfChargeType
    '    NotFree = 0
    '    Free = 1
    '    FreeAndTrack = 2
    'End Enum

    Private m_product_category As String
    Private m_model_number As String
    Private m_model_description As String
    Private m_version As String
    Private m_kit_description As String
    Private m_release_notes_file_name As String
    Private m_download_file_name As String
    Private m_freeofcharge As Integer
    Private m_tracking As Integer
    Public Shared m_SortOrder As Integer
    Private m_required_key As String 'swplus change

    Public Enum enumSortOrder
        ProductCategoryASC = 0
        ProductCategoryDESC = 1
        ModelNumberASC = 2
        ModelNumberDESC = 3
    End Enum


    Public Sub New()
        m_type = Product.ProductType.Software
        m_download_file_name = ""
    End Sub

    Public Property ProductCategory() As String
        Get
            Return m_product_category
        End Get
        Set(ByVal Value As String)
            m_product_category = Value
        End Set
    End Property
  
    Public Property ModelNumber() As String
        Get
            Return m_model_number
        End Get
        Set(ByVal Value As String)
            m_model_number = Value
        End Set
    End Property
    Public Property FreeOfCharge() As Boolean
        Get
            Return m_freeofcharge
        End Get
        Set(ByVal Value As Boolean)
            m_freeofcharge = Value
        End Set
    End Property
    Public Property Tracking() As Boolean
        Get
            Return m_tracking
        End Get
        Set(ByVal value As Boolean)
            m_tracking = value
        End Set
    End Property

    Public Property ModelDescription() As String
        Get
            Return m_model_description
        End Get
        Set(ByVal Value As String)
            m_model_description = Value
        End Set
    End Property
    'SWPLUS starts
    Public Property RequiredKey() As String
        Get
            Return m_required_key
        End Get
        Set(ByVal Value As String)
            m_required_key = Value
        End Set
    End Property
    'SWPLUS ends
    Public Property Version() As String
        Get
            Return m_version
        End Get
        Set(ByVal Value As String)
            m_version = Value
        End Set
    End Property

    Public Property KitPartNumber() As String
        Get
            Return PartNumber
        End Get
        Set(ByVal Value As String)
            PartNumber = Value
        End Set
    End Property

    Public Property KitDescription() As String
        Get
            Return m_kit_description
        End Get
        Set(ByVal Value As String)
            m_kit_description = Value
        End Set
    End Property

    Public Property ReleaseNotesFileName() As String
        Get
            Return m_release_notes_file_name
        End Get
        Set(ByVal Value As String)
            m_release_notes_file_name = Value
        End Set
    End Property

    Public Property DownloadFileName() As String
        Get
            Return m_download_file_name
        End Get
        Set(ByVal Value As String)
            m_download_file_name = Value
        End Set
    End Property

    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property

    Public Function CompareTo(ByVal obj As Software) As Integer Implements System.IComparable(Of Software).CompareTo
        Select Case (m_SortOrder)
            Case 0
                CompareTo = (ProductCategory < obj.ProductCategory) Or
                            (ProductCategory <= obj.ProductCategory And ModelNumber < obj.ModelNumber) Or
                            (ProductCategory <= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version > obj.Version) Or
                            (ProductCategory <= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (ProductCategory <= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)

            Case 1
                CompareTo = (ProductCategory > obj.ProductCategory) Or
                            (ProductCategory >= obj.ProductCategory And ModelNumber < obj.ModelNumber) Or
                            (ProductCategory >= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version > obj.Version) Or
                            (ProductCategory >= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (ProductCategory >= obj.ProductCategory And ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)

            Case 2
                CompareTo = ModelNumber < obj.ModelNumber Or
                            (ModelNumber <= obj.ModelNumber And Version > obj.Version) Or
                            (ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (ModelNumber <= obj.ModelNumber And Version >= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)
            Case 3
                CompareTo = ModelNumber > obj.ModelNumber Or
                            (ModelNumber >= obj.ModelNumber And Version > obj.Version) Or
                            (ModelNumber >= obj.ModelNumber And Version >= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (ModelNumber >= obj.ModelNumber And Version >= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)


            Case 6
                CompareTo = (Version < obj.Version) Or
                            (Version <= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (Version <= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)
            Case 7
                CompareTo = (Version > obj.Version) Or
                            (Version >= obj.Version And KitPartNumber > obj.KitPartNumber) Or
                            (Version >= obj.Version And KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)


            Case 4
                CompareTo = KitPartNumber < obj.KitPartNumber Or
                            (KitPartNumber <= obj.KitPartNumber And KitDescription < obj.KitDescription)
            Case 5
                CompareTo = KitPartNumber > obj.KitPartNumber Or
                            (KitPartNumber >= obj.KitPartNumber And KitDescription < obj.KitDescription)
        End Select
    End Function
End Class
