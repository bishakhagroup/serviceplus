Public Class UserType
    Inherits CoreObject
    Private m_statusid As String
    Private m_status As String
    Public Property StatusID() As String
        Get
            Return m_statusid
        End Get
        Set(ByVal value As String)
            m_statusid = value
        End Set
    End Property
    Public Property StatusName() As String
        Get
            Return (m_status)
        End Get
        Set(ByVal value As String)
            m_status = value
        End Set
    End Property
End Class
