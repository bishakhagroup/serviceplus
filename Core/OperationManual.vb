Public Class OperationManual
    Private m_MAINCAT As String = String.Empty
    Private m_SUBCAT As String = String.Empty
    Private m_LANG As String = String.Empty
    Private m_Model As String = String.Empty
    Private m_PN_LONG As String = String.Empty
    Private m_DESCRIPTION As String = String.Empty
    Private m_Version As String = String.Empty
    Private m_ACTIVE_FLAG As String = String.Empty
    Private m_FILENAME As String = String.Empty
    Private m_FILESIZE As String = String.Empty
    Private m_VERSION_DATE As Date = Nothing


    Public Property MAINCAT() As String
        Get
            Return m_MAINCAT
        End Get
        Set(ByVal value As String)
            m_MAINCAT = value
        End Set
    End Property
    Public Property SUBCAT() As String
        Get
            Return m_SUBCAT
        End Get
        Set(ByVal value As String)
            m_SUBCAT = value
        End Set
    End Property
    Public Property LANG() As String
        Get
            Return m_LANG
        End Get
        Set(ByVal value As String)
            m_LANG = value
        End Set
    End Property
    Public Property Model() As String
        Get
            Return m_Model
        End Get
        Set(ByVal value As String)
            m_Model = value
        End Set
    End Property
    Public Property PN_LONG() As String
        Get
            Return m_PN_LONG
        End Get
        Set(ByVal value As String)
            m_PN_LONG = value
        End Set
    End Property
    Public Property DESCRIPTION() As String
        Get
            Return m_DESCRIPTION
        End Get
        Set(ByVal value As String)
            m_DESCRIPTION = value
        End Set
    End Property
    Public Property Version() As String
        Get
            Return m_Version
        End Get
        Set(ByVal value As String)
            m_Version = value
        End Set
    End Property
    Public Property ACTIVE_FLAG() As String
        Get
            Return m_ACTIVE_FLAG
        End Get
        Set(ByVal value As String)
            m_ACTIVE_FLAG = value
        End Set
    End Property
    Public Property FILENAME() As String
        Get
            Return m_FILENAME
        End Get
        Set(ByVal value As String)
            m_FILENAME = value
        End Set
    End Property
    Public Property FILESIZE() As String
        Get
            Return m_FILESIZE
        End Get
        Set(ByVal value As String)
            m_FILESIZE = value
        End Set
    End Property
    Public Property VERSION_DATE() As Date
        Get
            Return m_VERSION_DATE
        End Get
        Set(ByVal value As Date)
            m_VERSION_DATE = value
        End Set
    End Property

End Class
