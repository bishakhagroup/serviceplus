
Imports System.Collections

Public Class ShoppingCart
    Inherits CoreObject
    Implements IComparable

    Private m_expiration_date As Date
    Private m_date_created As Date
    Private m_shopping_cart_number As String
    Private m_sequence_number As String
    Private m_comment As String
    Private m_po As String
    Private m_hasKitInfo As Boolean
    Private m_KitItem As KitItem()

    Private m_type As enumCartType
    'Private m_contains_downloadable_items As Boolean

    Private m_shopping_cart_items As ArrayList
    Private m_customer As Customer
    Public Shared m_SortOrder As Integer

    Public Enum enumSortOrder
        PurchaseOrderASC = 0
        PurchaseOrderDESC = 1
        DateSavedASC = 2
        DateSavedDESC = 3
        ExpirationDateASC = 4
        ExpirationDateDESC = 5        
    End Enum

    Public Enum enumCartType
        Other = 0
        Download = 1
        PrintOnDemand = 2
    End Enum

    Public Sub New()
        m_shopping_cart_items = New ArrayList
        m_po = ""
        'm_contains_downloadable_items = False
    End Sub

    Public Sub New(ByRef customer As Customer)
        m_customer = customer
        m_shopping_cart_items = New ArrayList
        m_po = ""
        'm_contains_downloadable_items = False
    End Sub

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property
    Public Property Type() As enumCartType
        Get
            Return m_type
        End Get
        Set(ByVal Value As enumCartType)
            m_type = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    Public Property ExpirationDate() As Date
        Get
            Return m_expiration_date
        End Get
        Set(ByVal Value As Date)
            m_expiration_date = Value
        End Set
    End Property

    Public Property Comment() As String
        Get
            Return m_comment
        End Get
        Set(ByVal Value As String)
            m_comment = Value
        End Set
    End Property

    Public Property PurchaseOrderNumber() As String
        Get
            Return m_po
        End Get
        Set(ByVal Value As String)
            SetProperty(m_po, Value)
        End Set
    End Property

    Public Property DateCreated() As Date
        Get
            Return m_date_created
        End Get
        Set(ByVal Value As Date)
            m_date_created = Value
        End Set
    End Property

    Public ReadOnly Property ReferenceName() As String
        Get
            Return DateCreated.ToShortDateString + " : " + SequenceNumber
        End Get
    End Property

    Public ReadOnly Property NumberOfItems() As Integer
        Get
            Return m_shopping_cart_items.Count
        End Get
    End Property

    Public ReadOnly Property TotalYourPrice() As Double
        Get
            Dim price As Double = 0
            For Each item As ShoppingCartItem In m_shopping_cart_items
                price = price + item.YourPrice
            Next
            Return price
        End Get
    End Property

    Public ReadOnly Property TotalListPrice() As Double
        Get
            Dim price As Double = 0
            For Each item As ShoppingCartItem In m_shopping_cart_items
                price = price + item.Product.ListPrice
            Next
            Return price
        End Get
    End Property

    Public ReadOnly Property ShoppingCartTotalPrice() As Double
        Get
            Dim price As Double = 0
            For Each item As ShoppingCartItem In ShoppingCartItems
                price = price + item.TotalLineItemPrice
            Next
            Return price
        End Get
    End Property



    Public Property ShoppingCartItems() As ShoppingCartItem()
        Get
            If ReturnDeletedObjects Then
                Dim items(m_shopping_cart_items.Count - 1) As ShoppingCartItem
                m_shopping_cart_items.CopyTo(items)

                Return items
            Else
                Dim iter As IEnumerator = m_shopping_cart_items.GetEnumerator
                Dim num_of_deletes As Integer = 0

                While iter.MoveNext
                    If CType(iter.Current, ShoppingCartItem).Action = CoreObject.ActionType.DELETE Then
                        num_of_deletes = num_of_deletes + 1
                    End If
                End While

                Dim j As Integer = 0
                Dim items(m_shopping_cart_items.Count - 1 - num_of_deletes) As ShoppingCartItem

                iter.Reset()

                While iter.MoveNext
                    If CType(iter.Current, ShoppingCartItem).Action <> CoreObject.ActionType.DELETE Then
                        items.SetValue(iter.Current, j)
                        j = j + 1
                    End If
                End While
                Return items
            End If

        End Get
        Set(ByVal Value As ShoppingCartItem())

        End Set
    End Property

    Public Sub AddShoppingCartItem(ByVal item As ShoppingCartItem)
        item.ShoppingCart = Me

        m_shopping_cart_items.Add(item)
    End Sub

    Public ReadOnly Property ContainsDownloadOnlyItems() As Boolean
        Get
            Dim has_download_items As Boolean = False

            For Each item As ShoppingCartItem In ShoppingCartItems
                If item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Download Then
                    has_download_items = True
                Else
                    'Else added by chandra on 20 Aug 2007
                    has_download_items = False
                    Exit For
                End If
            Next
            Return has_download_items
        End Get
    End Property
    Public ReadOnly Property ContainsDownloadAndShippingItems() As Boolean
        Get
            Dim has_downloadandshipping_items As Boolean = False

            For Each item As ShoppingCartItem In ShoppingCartItems
                If item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.DownloadAndShip Then
                    has_downloadandshipping_items = True
                    Exit For
                End If
            Next

            Return has_downloadandshipping_items
        End Get
    End Property
    Public ReadOnly Property ContainsPrintOnDemandItems() As Boolean
        Get
            Dim has_pod_items As Boolean = False

            For Each item As ShoppingCartItem In ShoppingCartItems
                If item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.PrintOnDemand Then
                    has_pod_items = True
                End If
            Next

            Return has_pod_items
        End Get
    End Property

    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property

    Public Function FindItem(ByVal part_number As String) As ShoppingCartItem
        Dim item As ShoppingCartItem
        For Each i As ShoppingCartItem In ShoppingCartItems
            If i.Product.PartNumber = part_number Then
                item = i
                Exit For
            End If
        Next
        Return item
    End Function

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Select Case (m_SortOrder)
            Case 0
                CompareTo = PurchaseOrderNumber < CType(obj, ShoppingCart).PurchaseOrderNumber
            Case 1
                CompareTo = PurchaseOrderNumber > CType(obj, ShoppingCart).PurchaseOrderNumber
            Case 2
                CompareTo = DateCreated < CType(obj, ShoppingCart).DateCreated
            Case 3
                CompareTo = DateCreated > CType(obj, ShoppingCart).DateCreated
            Case 4
                CompareTo = ExpirationDate < CType(obj, ShoppingCart).ExpirationDate
            Case 5
                CompareTo = ExpirationDate > CType(obj, ShoppingCart).ExpirationDate
        End Select
    End Function

End Class
