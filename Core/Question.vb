Public Class Question

    Public Enum AnswerDisplayType
        RadioButtons
        Textbox
    End Enum

    Private m_question_text As String
    Private m_answer_display_type As AnswerDisplayType
    Private m_question_id As String

    Public Sub New()

    End Sub

    Public Property Text() As String
        Get
            Return m_question_text
        End Get
        Set(ByVal Value As String)
            m_question_text = Value
        End Set
    End Property

    Public Property AnswerType() As AnswerDisplayType
        Get
            Return m_answer_display_type
        End Get
        Set(ByVal Value As AnswerDisplayType)
            m_answer_display_type = Value
        End Set
    End Property

    Public Property QuestionID() As String
        Get
            Return m_question_id
        End Get
        Set(ByVal Value As String)
            m_question_id = Value
        End Set
    End Property

End Class
