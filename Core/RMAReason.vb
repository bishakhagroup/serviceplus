Public Class RMAReason

    Private m_reason_id As String
    Private m_description As String

    Public Sub New()

    End Sub

    Public Property ReasonID() As String
        Get
            Return m_reason_id
        End Get
        Set(ByVal Value As String)
            m_reason_id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

End Class
