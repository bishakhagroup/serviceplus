Public Class CourseParticipant
    Inherits CoreObject

    Private m_ReservationNumber As String
    Private m_FirstName As String
    Private m_LastName As String
    Private m_EMail As String
    Private m_Phone As String
    Private m_Status As Char
    Private m_DateReserved As Date
    Private m_DateCanceled As Date
    Private m_DateWaited As Date
    Private m_Extension As String
    Private m_Fax As String
    Private m_Notes As String
    Private m_Addr1 As String
    Private m_Addr2 As String
    Private m_Addr3 As String
    Private m_City As String
    Private m_State As String
    Private m_Zip As String
    Private m_ISOCountryCode As String
    Private m_Country As String
    Private m_ConfirmationMailSent As String
    Private m_SequenceNumber As Integer
    Private m_UpdatedSIAMID As String
    Private m_UpdateDate As Date
    Private m_Company As String

    Public Property ReservationNumber() As String
        Get
            Return m_ReservationNumber
        End Get
        Set(ByVal Value As String)
            m_ReservationNumber = Value
        End Set
    End Property
    Public Property Company() As String
        Get
            Return m_Company
        End Get
        Set(ByVal Value As String)
            m_Company = Value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(ByVal Value As String)
            m_FirstName = Value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(ByVal Value As String)
            m_LastName = Value
        End Set
    End Property

    Public Property EMail() As String
        Get
            Return m_EMail
        End Get
        Set(ByVal Value As String)
            m_EMail = Value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return m_Phone
        End Get
        Set(ByVal Value As String)
            m_Phone = Value
        End Set
    End Property

    Public Property Status() As Char
        Get
            Return m_Status
        End Get
        Set(ByVal Value As Char)
            m_Status = Value
        End Set
    End Property
    Public ReadOnly Property StatusDisplay() As String
        Get
            If m_Status = "C" Then
                Return "Cancel"
            End If
            If m_Status = "W" Then
                Return "Waiting List"
            End If
            If m_Status = "R" Then
                Return "Reserved"
            End If
        End Get
    End Property

    Public Property DateReserved() As Date
        Get
            Return m_DateReserved
        End Get
        Set(ByVal Value As Date)
            m_DateReserved = Value
        End Set
    End Property
    Public ReadOnly Property DateReservedDisplay() As String
        Get
            Return Format(m_DateReserved)
        End Get
    End Property

    Public Property DateCanceled() As Date
        Get
            Return m_DateCanceled
        End Get
        Set(ByVal Value As Date)
            m_DateCanceled = Value
        End Set
    End Property

    Public ReadOnly Property DateCanceledDisplay() As String
        Get
            Return Format(m_DateCanceled)
        End Get
    End Property

    Public Property DateWaited() As Date
        Get
            Return m_DateWaited
        End Get
        Set(ByVal Value As Date)
            m_DateWaited = Value
        End Set
    End Property
    Public ReadOnly Property DateWaitedDisplay() As String
        Get
            Return Format(m_DateWaited)
        End Get
    End Property

    Public Property Extension() As String
        Get
            Return m_Extension
        End Get
        Set(ByVal Value As String)
            m_Extension = Value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return m_Fax
        End Get
        Set(ByVal Value As String)
            m_Fax = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return m_Addr1
        End Get
        Set(ByVal Value As String)
            m_Addr1 = Value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return m_Addr2
        End Get
        Set(ByVal Value As String)
            m_Addr2 = Value
        End Set
    End Property
    Public Property Address3() As String
        Get
            Return m_Addr3
        End Get
        Set(ByVal Value As String)
            m_Addr3 = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(ByVal Value As String)
            m_City = Value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_State
        End Get
        Set(ByVal Value As String)
            m_State = Value
        End Set
    End Property

    Public Property Zip() As String
        Get
            Return m_Zip
        End Get
        Set(ByVal Value As String)
            m_Zip = Value
        End Set
    End Property

    Public Property ISOCountryCode() As String
        Get
            Return m_ISOCountryCode
        End Get
        Set(ByVal Value As String)
            m_ISOCountryCode = Value
        End Set
    End Property
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set(ByVal Value As String)
            m_Country = Value
        End Set
    End Property

    Public Property UpdatedSIAMID() As String
        Get
            Return m_UpdatedSIAMID
        End Get
        Set(ByVal Value As String)
            m_UpdatedSIAMID = Value
        End Set
    End Property

    Public Property ConfirmationMailSent() As String
        Get
            Return m_ConfirmationMailSent
        End Get
        Set(ByVal Value As String)
            m_ConfirmationMailSent = Value
        End Set
    End Property

End Class
