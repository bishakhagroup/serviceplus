Public Class SISAccount
    Inherits Account

    ' ASleight - This is already in the base Account class
    'Public Enum AddressOverrideType
    '    ShipTo
    '    ShipToAndBillTo
    '    NotAllowed
    'End Enum

    Private m_address_override As String
    Private m_override_description As String
    Private m_credit_hold As Boolean
    Private m_admin_hold As Boolean
    Private m_tax_exempt As Boolean
    Private m_multi_payer As Boolean
    Private m_sap_account As Boolean

    Private m_customer_class As String
    Private m_internal_account As Boolean
    Private m_ship_to_address As Address
    Private m_CACalifornia As String

    Public Sub New()
        m_ship_to_address = New Address
    End Sub

    Public Property CACalifornia() As String
        Get
            Return m_CACalifornia
        End Get
        Set(ByVal Value As String)
            m_CACalifornia = Value
        End Set
    End Property

    ' ASleight - This property already exists in base class Account
    'Public Property AddressOverride() As AddressOverrideType
    '    Get
    '        Return m_address_override
    '    End Get
    '    Set(ByVal Value As AddressOverrideType)
    '        m_address_override = Value
    '    End Set
    'End Property

    Public Property OverrideDescription() As String
        Get
            Return m_override_description
        End Get
        Set(ByVal Value As String)
            m_override_description = Value
        End Set
    End Property

    Public Property CreditHold() As Boolean
        Get
            Return m_credit_hold
        End Get
        Set(ByVal Value As Boolean)
            m_credit_hold = Value
        End Set
    End Property

    Public Property AdminHold() As Boolean
        Get
            Return m_admin_hold
        End Get
        Set(ByVal Value As Boolean)
            m_admin_hold = Value
        End Set
    End Property

    Public Property TaxExempt() As Boolean
        Get
            Return m_tax_exempt
        End Get
        Set(ByVal Value As Boolean)
            m_tax_exempt = Value
        End Set
    End Property

    Public Property MultiPayer() As Boolean
        Get
            Return m_multi_payer
        End Get
        Set(ByVal Value As Boolean)
            m_multi_payer = Value
        End Set
    End Property

    Public Property SAPAccount() As Boolean
        Get
            Return m_sap_account
        End Get
        Set(ByVal Value As Boolean)
            m_sap_account = Value
        End Set
    End Property

    Public Property CustomerClass() As String
        Get
            Return m_customer_class
        End Get
        Set(ByVal Value As String)
            m_customer_class = Value
        End Set
    End Property

    Public Property InternalAccount() As Boolean
        Get
            Return m_internal_account
        End Get
        Set(ByVal Value As Boolean)
            m_internal_account = Value
        End Set
    End Property

    Public Property BillTo() As Address
        Get
            Return Address
        End Get
        Set(ByVal Value As Address)
            Address = Value
        End Set
    End Property

    Public Property ShipTo() As Address
        Get
            Return m_ship_to_address
        End Get
        Set(ByVal Value As Address)
            m_ship_to_address = Value
        End Set
    End Property

End Class
