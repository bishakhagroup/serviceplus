' User Role - Class file
Public Class UserRole

    Private m_roleid As String
    Private m_rolename As String
    Private m_roledeleted As String


    Public Property RoleID() As String
        Get
            Return m_roleid
        End Get
        Set(ByVal value As String)
            m_roleid = value
        End Set
    End Property

    Public Property RoleName() As String
        Get
            Return (m_rolename)
        End Get
        Set(ByVal value As String)
            m_rolename = value
        End Set
    End Property

    Public Property IsDeleted() As String
        Get
            Return m_roledeleted
        End Get
        Set(ByVal value As String)
            m_roledeleted = value
        End Set
    End Property

End Class
