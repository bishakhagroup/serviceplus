
Imports System.Globalization

Public Class CustomerCatalogItem
    Inherits CoreObject
    Implements IComparable

    Private m_customer As Customer
    Private m_product As Product
    Private m_sequence_number As Integer
    Private m_model_number As String
    Private m_date_added As Date
    Private m_reminder_start_date As Date
    Private m_recurrence_pattern As RecurrencePattern
    Private m_recurrence_day As RecurrenceDay

    Private Shared m_SortOrder As Integer

    Public Enum enumSortOrder
        ModelASC = 0
        ModelDESC = 1
        PartNumberASC = 2
        PartNumberDESC = 3
        DateAddedASC = 4
        DateAddedDESC = 5
    End Enum

    Public Enum RecurrencePattern
        OneTime = 0
        Weekly = 1
        Monthly = 2
    End Enum

    Public Enum RecurrenceDay
        Monday = 0
        Tuesday = 1
        Wednesday = 2
        Thursday = 3
        Friday = 4
        Saturday = 5
        Sunday = 6
        FirstOfMonth = 7
        MiddleOfMonth = 8
        LastOfMonth = 9
    End Enum

    Public Sub New()
        m_reminder_start_date = New DateTime(1900, 1, 1)
        m_model_number = ""
    End Sub

    Public Sub New(ByVal customer As Customer)
        m_customer = customer
        m_reminder_start_date = New DateTime(1900, 1, 1)
        m_model_number = ""
    End Sub

    Public Property Customer() As Customer
        Get
            Return m_customer
        End Get
        Set(ByVal Value As Customer)
            m_customer = Value
        End Set
    End Property

    Public Property Product() As Product
        Get
            Return m_product
        End Get
        Set(ByVal Value As Product)
            m_product = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As Integer)
            m_sequence_number = Value
        End Set
    End Property

    Public Property ModelNumber() As String
        Get
            Return m_model_number
        End Get
        Set(ByVal Value As String)
            m_model_number = Value
        End Set
    End Property

    Public ReadOnly Property ReminderToday() As Boolean
        Get
            Dim rc As Boolean = False
            Dim default_date As New DateTime(1900, 1, 1)
            Dim calender As New GregorianCalendar

            If ReminderStartDate = default_date Then
                rc = False
            Else
                If ReminderStartDate = Date.Today And TheRecurrencePattern = RecurrencePattern.OneTime Then 'one time reminder
                    rc = True
                Else
                    Dim today As DayOfWeek = calender.GetDayOfWeek(Date.Today)
                    If TheRecurrencePattern = RecurrencePattern.Weekly Then 'weekly reminder
                        If today.ToString() = m_recurrence_day.ToString() And Date.Today >= ReminderStartDate Then
                            rc = True
                        End If
                    Else
                        If TheRecurrencePattern = RecurrencePattern.Monthly And Date.Today >= ReminderStartDate Then 'monthly reminder
                            Dim day_of_month As Integer = calender.GetDayOfMonth(Date.Today)

                            If day_of_month = 1 And m_recurrence_day = RecurrenceDay.FirstOfMonth Then
                                rc = True
                            End If

                            If day_of_month = 15 And m_recurrence_day = RecurrenceDay.MiddleOfMonth Then
                                rc = True
                            End If

                            If day_of_month = calender.GetDaysInMonth(Date.Today.Year, Date.Today.Month) And m_recurrence_day = RecurrenceDay.LastOfMonth Then
                                rc = True
                            End If

                        End If
                    End If
                End If
            End If

            Return rc
        End Get
    End Property

    Public Property ReminderStartDate() As Date
        Get
            Return m_reminder_start_date
        End Get
        Set(ByVal Value As Date)
            m_reminder_start_date = Value
        End Set
    End Property

    Public Property TheRecurrencePattern() As RecurrencePattern
        Get
            Return m_recurrence_pattern
        End Get
        Set(ByVal Value As RecurrencePattern)
            m_recurrence_pattern = Value
        End Set
    End Property

    Public Property TheRecurrenceDay() As RecurrenceDay
        Get
            Return m_recurrence_day
        End Get
        Set(ByVal Value As RecurrenceDay)
            m_recurrence_day = Value
        End Set
    End Property

    Public Function SetOneTimeReminder(ByVal reminder_date As Date)
        SetProperty(m_reminder_start_date, reminder_date)
    End Function

    Public Function RemoveReminder()
        Dirty = True
        SetProperty(m_reminder_start_date, "01/01/1900")
        m_recurrence_pattern = Nothing
        m_recurrence_day = Nothing
    End Function

    Public Function SetWeeklyReminder(ByVal start_date As Date, ByVal day As RecurrenceDay)

        Select Case (day)
            Case RecurrenceDay.FirstOfMonth
                Throw New Exception("Can't set weekly reminder using the 'FirstOfMonth' option.")
            Case RecurrenceDay.MiddleOfMonth
                Throw New Exception("Can't set weekly reminder using the 'MiddleOfMonth' option.")
            Case RecurrenceDay.LastOfMonth
                Throw New Exception("Can't set weekly reminder using the 'LastOfMonth' option.")
        End Select

        SetProperty(m_reminder_start_date, start_date)
        m_recurrence_pattern = RecurrencePattern.Weekly
        m_recurrence_day = day
        Dirty = True
    End Function

    Public Function SetMonthlyReminder(ByVal start_date As Date, ByVal day As RecurrenceDay)

        Select Case (day)
            Case RecurrenceDay.FirstOfMonth
            Case RecurrenceDay.MiddleOfMonth
            Case RecurrenceDay.LastOfMonth
            Case Else
                Throw New Exception("When setting a monthly reminder, please use 'FirstOfMonth', 'MiddleOfMonth' or 'LastOfMonth'")
        End Select

        SetProperty(m_reminder_start_date, start_date)
        m_recurrence_pattern = RecurrencePattern.Monthly
        m_recurrence_day = day
        Dirty = True
    End Function

    Public Property DateAdded() As Date
        Get
            Return m_date_added
        End Get
        Set(ByVal Value As Date)
            m_date_added = Value
        End Set
    End Property

    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Select Case (m_SortOrder)
            Case 0
                CompareTo = ModelNumber < CType(obj, CustomerCatalogItem).ModelNumber
            Case 1
                CompareTo = ModelNumber > CType(obj, CustomerCatalogItem).ModelNumber
            Case 2
                CompareTo = Product.PartNumber < CType(obj, CustomerCatalogItem).ModelNumber
            Case 3
                CompareTo = Product.PartNumber > CType(obj, CustomerCatalogItem).ModelNumber
            Case 4
                CompareTo = DateAdded < CType(obj, CustomerCatalogItem).DateAdded
            Case 5
                CompareTo = DateAdded > CType(obj, CustomerCatalogItem).DateAdded

        End Select
    End Function

End Class
