Public Class Promotion

    Private m_promotion_id As String
    Private m_promotion_text As String
    Private m_image_file_name As String
    Private m_effective_date As Date
    Private m_expiration_date As Date
    Private m_display_priority As Integer
    Private m_product_type_id As Integer

    Public Sub New()

    End Sub

    Public Property PromotionText() As String
        Get
            Return m_promotion_text
        End Get
        Set(ByVal Value As String)
            m_promotion_text = Value
        End Set
    End Property

    Public Property ImageFileName() As String
        Get
            Return m_image_file_name
        End Get
        Set(ByVal Value As String)
            m_image_file_name = Value
        End Set
    End Property

    Public Property EffectiveDate() As Date
        Get
            Return m_effective_date
        End Get
        Set(ByVal Value As Date)
            m_effective_date = Value
        End Set
    End Property

    Public Property ExpirationDate() As Date
        Get
            Return m_expiration_date
        End Get
        Set(ByVal Value As Date)
            m_expiration_date = Value
        End Set
    End Property

    Public Property DisplayPriority() As Integer
        Get
            Return m_display_priority
        End Get
        Set(ByVal Value As Integer)
            m_display_priority = Value
        End Set
    End Property

End Class
Public Class PromotionPart

    Private m_part_number As String
    Private m_model_number As String
    Private m_list_price As Double
    Private m_internet_price As Double
    Private m_discount_percentage As Double
    Private m_description As String
   

    Public Sub New()

    End Sub

    Public Property PartNumber() As String
        Get
            Return m_part_number
        End Get
        Set(ByVal Value As String)
            m_part_number = Value
        End Set
    End Property

    Public Property ModelNumber() As String
        Get
            Return m_model_number
        End Get
        Set(ByVal Value As String)
            m_model_number = Value
        End Set
    End Property

    Public Property ListPrice() As Double
        Get
            Return m_list_price
        End Get
        Set(ByVal Value As Double)
            m_list_price = Value
        End Set
    End Property
    Public Property InternetPrice() As Double
        Get
            Return m_internet_price
        End Get
        Set(ByVal Value As Double)
            m_internet_price = Value
        End Set
    End Property

    Public Property DiscountPercentage() As Double
        Get
            Return m_discount_percentage
        End Get
        Set(ByVal Value As Double)
            m_discount_percentage = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property


End Class

Public Class PromotionKit

    Private m_part_number As String
    Private m_discount_percentage As Double

    Public Sub New()

    End Sub

    Public Property PartNumber() As String
        Get
            Return m_part_number
        End Get
        Set(ByVal Value As String)
            m_part_number = Value
        End Set
    End Property
    Public Property DiscountPercentage() As Double
        Get
            Return m_discount_percentage
        End Get
        Set(ByVal Value As Double)
            m_discount_percentage = Value
        End Set
    End Property



End Class