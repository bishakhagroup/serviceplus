Public Class Response
    Inherits CoreObject

    Public Enum FixedRadioButtonResponseType
        NotSpecified                    '0
        DoesNotMeetAnyExpectations      '1
        DoesNotMeetManyExpectations1    '2    
        DoesNotMeetManyExpectations2    '3
        MeetsMostExpectations1          '4
        MeetsMostExpectations2          '5
        MeetsExpectations               '6
        ExceedsExpectations             '7
        DoesNotApply                    '8
    End Enum

    Private m_survey As Survey
    Private m_question As Question
    Private m_sequence_number As String
    Private m_text_response As String
    Private m_radio_button_response As FixedRadioButtonResponseType
    Private m_respondent As Customer

    Public Sub New()
        m_radio_button_response = FixedRadioButtonResponseType.NotSpecified
        m_text_response = Nothing
    End Sub

    Public Property TextResponse() As String
        Get
            Return m_text_response
        End Get
        Set(ByVal Value As String)
            SetProperty(m_text_response, Value)
        End Set
    End Property

    Public Property RadioButtonResponse() As FixedRadioButtonResponseType
        Get
            Return m_radio_button_response
        End Get
        Set(ByVal Value As FixedRadioButtonResponseType)
            SetProperty(m_radio_button_response, Value)
        End Set
    End Property

    Public Property Respondent() As Customer
        Get
            Return m_respondent
        End Get
        Set(ByVal Value As Customer)
            m_respondent = Value
        End Set
    End Property

    Public Property Survey() As Survey
        Get
            Return m_survey
        End Get
        Set(ByVal Value As Survey)
            m_survey = Value
        End Set
    End Property

    Public Property Question() As Question
        Get
            Return m_question
        End Get
        Set(ByVal Value As Question)
            m_question = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

End Class
