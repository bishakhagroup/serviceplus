Public Class RMA

    Private m_customer As Customer
    Private m_sequence_number As String
    Private m_invoice_number As String
    Private m_order_number As String
    Private m_price As Double
    Private m_serial_number As String
    Private m_warranty As Boolean
    Private m_part_number As String
    Private m_ra_reason As RMAReason
    Private m_comments As String

    Public Sub New(ByVal customer As Customer)
        m_customer = customer
    End Sub

    Public ReadOnly Property Customer() As Customer
        Get
            Return m_customer
        End Get
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_invoice_number
        End Get
        Set(ByVal Value As String)
            m_invoice_number = Value
        End Set
    End Property

    Public Property OrderNumber() As String
        Get
            Return m_order_number
        End Get
        Set(ByVal Value As String)
            m_order_number = Value
        End Set
    End Property

    Public Property Price() As Double
        Get
            Return m_price
        End Get
        Set(ByVal Value As Double)
            m_price = Value
        End Set
    End Property

    Public Property SerialNumber() As String
        Get
            Return m_serial_number
        End Get
        Set(ByVal Value As String)
            m_serial_number = Value
        End Set
    End Property

    Public Property Warranty() As Boolean
        Get
            Return m_warranty
        End Get
        Set(ByVal Value As Boolean)
            m_warranty = Value
        End Set
    End Property

    Public Property PartNumber() As String
        Get
            Return m_part_number
        End Get
        Set(ByVal Value As String)
            m_part_number = Value
        End Set
    End Property

    Public Property Reason() As RMAReason
        Get
            Return m_ra_reason
        End Get
        Set(ByVal Value As RMAReason)
            m_ra_reason = Value
        End Set
    End Property

    Public Property Comments() As String
        Get
            Return m_comments
        End Get
        Set(ByVal Value As String)
            m_comments = Value
        End Set
    End Property

End Class
