Public Class TechBulletin
    Inherits CoreObject

    Private m_productline_no As String
    Private m_productline_desc As String
    Private m_subscription_id As String
    Private m_zipcode As String
    Private m_bulletin_no As String = String.Empty
    Private m_model As String
    Private m_subject As String
    Private m_print_date As String
    Private m_startrange As Long
    Private m_endrange As Long
    Private m_Type As Short
    'Added By Sneha
    Private m_old_model As String

    Public Class ProductLines
        Public productLineNo As String
        Public productLineDesc As String
    End Class

    Public Property Type() As Short
        Get
            Return m_Type
        End Get
        Set(ByVal Value As Short)
            SetProperty(m_Type, Value)
        End Set
    End Property

    Public Property ProductLineNo() As String
        Get
            Return m_productline_no
        End Get
        Set(ByVal Value As String)
            SetProperty(m_productline_no, Value)
        End Set
    End Property

    Public Property ProductLineDesc() As String
        Get
            Return m_productline_desc
        End Get
        Set(ByVal Value As String)
            SetProperty(m_productline_desc, Value)
        End Set
    End Property

    Public Property SubscriptionID() As String
        Get
            Return m_subscription_id
        End Get
        Set(ByVal Value As String)
            SetProperty(m_subscription_id, Value)
        End Set
    End Property

    Public Property ZipCode() As String
        Get
            Return m_zipcode
        End Get
        Set(ByVal Value As String)
            SetProperty(m_zipcode, Value)
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_model
        End Get
        Set(ByVal Value As String)
            SetProperty(m_model, Value)
        End Set
    End Property

    Public Property Subject() As String
        Get
            Return m_subject
        End Get
        Set(ByVal Value As String)
            SetProperty(m_subject, Value)
        End Set
    End Property

    Public Property PrintDate() As String
        Get
            Return m_print_date
        End Get
        Set(ByVal Value As String)
            SetProperty(m_print_date, Value)
        End Set
    End Property

    Public Property BulletinNo() As String
        Get
            Return m_bulletin_no
        End Get
        Set(ByVal Value As String)
            SetProperty(m_bulletin_no, Value)
        End Set
    End Property

    Public Property StartRange() As Long
        Get
            Return m_startrange
        End Get
        Set(ByVal Value As Long)
            SetProperty(m_startrange, Value)
        End Set
    End Property
    Public Property EndRange() As Long
        Get
            Return m_endrange
        End Get
        Set(ByVal Value As Long)
            SetProperty(m_endrange, Value)
        End Set
    End Property
    'Added By Sneha
    Public Property OldModel() As String
        Get
            Return m_old_model
        End Get
        Set(ByVal Value As String)
            SetProperty(m_old_model, Value)
        End Set
    End Property

End Class
