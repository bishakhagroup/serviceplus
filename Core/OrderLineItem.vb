Public Class OrderLineItem
    Inherits CoreObject

    Public Enum Status
        Allocated
        Backordered
        Confirmed
        Pick
        InProcess
        Unallocated
        CommentLine
        ErrorStatus
        Cancelled
        Deleted
    End Enum

    Private m_part_confirmedqty As String

    Private m_line_number As String
    Private m_Sequencenumber As Integer
    Private m_order As Order
    Private m_invoice As Invoice
    Private m_line_status As Status
    Private m_line_status_date As Date
    Private m_product As Product
    Private m_quantity As Integer
    Private m_software_download_filename As String
    Private m_downloadable As Boolean
    Private m_SOFTWAREDELIVERYMETHOD As String = String.Empty
    Private m_OrderQty As Integer
    Private m_invoice_number As String '5325
    Private m_Item_Price As Double '8346
    Private m_TOTAL_LINE_ITEM_PRICE As Double '8346
    Private M_DESCRIPTION As String '8346
    Private M_LISTPRICE As Double '8346
    Private m_partnumber As String '8346
    Private m_CERTIFICATIONACTIVATIONCODE As String '8346




    Public Property OrderQty() As Integer
        Get
            Return m_OrderQty
        End Get
        Set(ByVal Value As Integer)
            m_OrderQty = Value
        End Set
    End Property

    Public Property ConfirmedQty() As Integer
        Get
            Return m_part_confirmedqty
        End Get
        Set(ByVal Value As Integer)
            m_part_confirmedqty = Value
        End Set
    End Property
    Public Sub New(ByVal order As Order)
        m_order = order
    End Sub

    Public Property Order() As Order
        Get
            Return m_order
        End Get
        Set(ByVal Value As Order)
            m_order = Value
        End Set
    End Property

    Public Property Invoice() As Invoice
        Get
            Return m_invoice
        End Get
        Set(ByVal Value As Invoice)
            m_invoice = Value
        End Set
    End Property
    Public Property SequenceNumber() As Integer
        Get
            Return m_Sequencenumber
        End Get
        Set(ByVal Value As Integer)
            m_Sequencenumber = Value
        End Set
    End Property
    Public Property LineNumber() As String
        Get
            Return m_line_number
        End Get
        Set(ByVal Value As String)
            m_line_number = Value
        End Set
    End Property

    Public Property SoftwareDownloadFileName() As String
        Get
            Return m_software_download_filename
        End Get
        Set(ByVal Value As String)
            m_software_download_filename = Value
        End Set
    End Property
    
    Public Property Downloadable() As Boolean
        Get
            Return m_downloadable
        End Get
        Set(ByVal Value As Boolean)
            'm_downloadable = True 'Modified for fixing Bug# 321
            m_downloadable = Value
        End Set
    End Property
    Public Property Deliverymethod() As String
        Get
            Return m_SOFTWAREDELIVERYMETHOD
        End Get
        Set(ByVal Value As String)
            m_SOFTWAREDELIVERYMETHOD = Value
        End Set
    End Property
    Public Property LineStatus() As Status
        Get
            Return m_line_status
        End Get
        Set(ByVal Value As Status)
            m_line_status = Value
        End Set
    End Property

    Public Property LineStatusDate() As Date
        Get
            Return m_line_status_date
        End Get
        Set(ByVal Value As Date)
            m_line_status_date = Value
        End Set
    End Property

    Public Property Quantity() As Integer
        Get
            Return m_quantity
        End Get
        Set(ByVal Value As Integer)
            m_quantity = Value
        End Set
    End Property


    Public Property Item_Price() As Double '8346
        Get
            Return m_Item_Price

        End Get
        Set(ByVal Value As Double)
            m_Item_Price = Value
        End Set
    End Property

    Public Property Total_Line_Item_Price() As Double '8346
        Get
            Return m_TOTAL_LINE_ITEM_PRICE

        End Get
        Set(ByVal Value As Double)
            m_TOTAL_LINE_ITEM_PRICE = Value
        End Set
    End Property
    Public Property LISTPRICE() As Double '8346
        Get
            Return M_LISTPRICE

        End Get
        Set(ByVal Value As Double)
            M_LISTPRICE = Value
        End Set
    End Property
    Public Property DESCRIPTION() As String '8346
        Get
            Return M_DESCRIPTION

        End Get
        Set(ByVal Value As String)
            M_DESCRIPTION = Value
        End Set
    End Property
    Public Property CERTIFICATIONACTIVATIONCODE() As String '8346
        Get
            Return m_CERTIFICATIONACTIVATIONCODE

        End Get
        Set(ByVal Value As String)
            m_CERTIFICATIONACTIVATIONCODE = Value
        End Set
    End Property

    Public Property Product() As Product
        Get
            Return m_product
        End Get
        Set(ByVal Value As Product)
            m_product = Value
        End Set
    End Property

    '5325 starts
    Public Property InvoiceNumber() As String
        Get
            Return m_invoice_number
        End Get
        Set(ByVal Value As String)
            m_invoice_number = Value
        End Set
    End Property
    '5325 ends
    Public Property partnumber() As String
        Get
            Return m_partnumber
        End Get
        Set(ByVal Value As String)
            m_partnumber = Value
        End Set
    End Property
End Class
