Public Class CourseCountry
    Inherits CoreObject

    Private m_Code As String
    Private m_Country As String
    Private m_TerritoryName As String
    Private m_Description As String

    Public Sub New(Optional ByVal territoryName As String = "")
        If Not String.IsNullOrWhiteSpace(territoryName) Then m_TerritoryName = territoryName
    End Sub

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal Value As String)
            m_Code = Value
        End Set
    End Property

    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set(ByVal Value As String)
            m_Country = Value
        End Set
    End Property

    Public Property TerritoryName() As String
        Get
            Return m_TerritoryName
        End Get
        Set(ByVal Value As String)
            m_TerritoryName = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal Value As String)
            m_Description = Value
        End Set
    End Property
End Class
Public Class CourseLocation
    Inherits CoreObject

    Private m_Code As String
    Private m_Name As String
    Private m_Addr1 As String
    Private m_Addr2 As String
    Private m_Addr3 As String
    Private m_City As String
    Private m_State As String
    Private m_Zip As String
    Private m_Phone As String
    Private m_Fax As String
    Private m_Extension As String
    Private m_SequenceNumber As Integer
    Private m_StatusCode As Integer
    Private m_UpdateDate As Date
    Private m_Notes As String
    Private m_Direction As String
    Private m_Accomodations As String
    Private m_Country As CourseCountry

    Public Property Country() As CourseCountry
        Get
            If m_Country Is Nothing Then
                m_Country = New CourseCountry
            End If
            Return m_Country
        End Get
        Set(ByVal Value As CourseCountry)
            m_Country = Value
        End Set
    End Property

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal Value As String)
            m_Code = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(ByVal Value As String)
            m_Name = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return m_Addr1
        End Get
        Set(ByVal Value As String)
            m_Addr1 = Value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return m_Addr2
        End Get
        Set(ByVal Value As String)
            m_Addr2 = Value
        End Set
    End Property
    Public Property Address3() As String
        Get
            Return m_Addr3
        End Get
        Set(ByVal Value As String)
            m_Addr3 = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(ByVal Value As String)
            m_City = Value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_State
        End Get
        Set(ByVal Value As String)
            m_State = Value
        End Set
    End Property

    Public Property Zip() As String
        Get
            Return m_Zip
        End Get
        Set(ByVal Value As String)
            m_Zip = Value
        End Set
    End Property
    Public ReadOnly Property CityStateZip() As String
        Get
            Return m_City + ", " + m_State + " " + m_Zip
        End Get
    End Property

    Public Property Phone() As String
        Get
            Return m_Phone
        End Get
        Set(ByVal Value As String)
            m_Phone = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return m_Fax
        End Get
        Set(ByVal Value As String)
            m_Fax = Value
        End Set
    End Property
    Public Property Extension() As String
        Get
            Return m_Extension
        End Get
        Set(ByVal Value As String)
            m_Extension = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property

    Public Property StatusCode() As Integer
        Get
            Return m_StatusCode
        End Get
        Set(ByVal Value As Integer)
            m_StatusCode = Value
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property Direction() As String
        Get
            Return m_Direction
        End Get
        Set(ByVal Value As String)
            m_Direction = Value
        End Set
    End Property

    Public Property Accomodations() As String
        Get
            Return m_Accomodations
        End Get
        Set(ByVal Value As String)
            m_Accomodations = Value
        End Set
    End Property

End Class
