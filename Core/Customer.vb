Imports System.Collections
'Imports Sony.US.ServicesPLUS.Data

Public Class Customer
    Inherits CoreObject

#Region "[Private Variables]"
    Private m_customer_id As String
    Private m_sequence_number As String
    Private m_company_name As String = ""
    Private m_first_name As String = ""
    Private m_last_name As String = ""
    Private m_email_address As String = ""
    Private m_siam_identity As String = ""
    Private m_tax_exempt As Boolean
    Private m_address As New Address
    Private m_email_ok As Boolean
    'Private m_CaliforniaPartialTax As Boolean
    Private m_enabledocumentidnumber As String = ""
    Private m_ETdocumentidnumber As String = ""
    Private m_phone_number As String
    Private m_phone_extension As String = ""
    Private m_fax_number As String = ""
    Private m_sap_bill_to_accounts As ArrayList
    Private m_legacy_bill_to_accounts As ArrayList
    Private m_legacy_ship_to_accounts As ArrayList
    Private m_credit_cards As ArrayList
    Private m_marketing_interests As ArrayList
    Private m_isActive As Boolean
    'Private m_isLeaderTechFirslLogin As Boolean = False'6668
    Private m_IsValidTBSubscription As Boolean = False
    Private m_subscriptionid As String
    Private m_subscriptionactive As Boolean = False
    Private m_user_name As String = String.Empty
    Private m_user_type As String = String.Empty
	'Private m_user_status As String = String.Empty
    Private m_PasswordExpirationDate As String = String.Empty
    Private m_Password As String = String.Empty
    Private m_LockStatus As Short
    Private m_pin_1 As String = String.Empty
    Private m_pin_3 As String = String.Empty
    Private m_pin_2 As String = String.Empty
    'Private m_status_id As Int16'6668
    'Private m_AlternateUserName As String = String.Empty
	Private m_status_code As Short
    Private m_ldapID As String = String.Empty
    Private myupdatedate As Date

    Private m_Country_Name As String = String.Empty
    Private m_Country_Code As String = String.Empty


    '***************Changes done for Bug# 1212 Starts here***************
    '***************Kannapiran S*****************
    Private m_UserLocation As Integer = 0
    '***************Changes done for Bug# 1212 Ends here***************

    Private m_IsPunchOutUser As Boolean = False
    Private m_PunchOutUserName As String = String.Empty
    Private m_LastLoginTime As String = String.Empty
#End Region

    Public Sub New()
        m_sap_bill_to_accounts = New ArrayList
        m_legacy_bill_to_accounts = New ArrayList
        m_legacy_ship_to_accounts = New ArrayList
        m_credit_cards = New ArrayList
        m_marketing_interests = New ArrayList
        m_address = New Address
    End Sub

#Region "[Properties]"
    Public Property UserName() As String
        Get
            Return m_user_name
        End Get
        Set(ByVal Value As String)
            m_user_name = Value
        End Set
    End Property
    

    Public Property UserType() As String
        Get
            Return m_user_type.ToUpper()
        End Get
        Set(ByVal Value As String)
            m_user_type = Value.ToUpper()
        End Set
    End Property

	'  Public Property UserStatus() As String
	'      Get
	'	Return m_status_code.ToString
	'      End Get
	'Set(ByVal Value As String)
	'	m_status_code = Convert.ToInt16(Value)
	'End Set
	'  End Property

    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(ByVal Value As String)
            m_Password = Value
        End Set
    End Property

    Public Property PasswordExpirationDate() As String
        Get
            Return m_PasswordExpirationDate
        End Get
        Set(ByVal Value As String)
            m_PasswordExpirationDate = Value
        End Set
    End Property

    Public Property LockStatus() As Short
        Get
            Return m_LockStatus
        End Get
        Set(ByVal Value As Short)
            m_LockStatus = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return m_customer_id
        End Get
        Set(ByVal Value As String)
            m_customer_id = Value
        End Set
    End Property

    Public Property SequenceNumber() As String
        Get
            Return m_sequence_number
        End Get
        Set(ByVal Value As String)
            m_sequence_number = Value
        End Set
    End Property

    Public Property SIAMIdentity() As String
        Get
            Return m_siam_identity
        End Get
        Set(ByVal Value As String)
            m_siam_identity = Value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return m_isActive
        End Get
        Set(ByVal Value As Boolean)
            m_isActive = Value
        End Set
    End Property
    '6668 starts
    'Public Property LEADERTECHFIRSTLOGIN() As Boolean
    '    Get
    '        Return m_isLeaderTechFirslLogin
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        m_isLeaderTechFirslLogin = Value
    '    End Set
    'End Property
    '6668 ends
    Public Property CompanyName() As String
        Get
            Return m_company_name
        End Get
        Set(ByVal Value As String)
            SetProperty(m_company_name, Value)
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal Value As String)
            SetProperty(m_first_name, Value)
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal Value As String)
            SetProperty(m_last_name, Value)
        End Set
    End Property

    Public Property SubscriptionID() As String
        Get
            Return m_subscriptionid
        End Get
        Set(ByVal Value As String)
            SetProperty(m_subscriptionid, Value)
        End Set
    End Property

    Public Property IsSubscriptionActive() As Boolean
        Get
            Return m_subscriptionactive
        End Get
        Set(ByVal Value As Boolean)
            m_subscriptionactive = Value
        End Set
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return m_first_name + " " + m_last_name
        End Get
    End Property

    Public Property EmailAddress() As String
        Get
            Return m_email_address
        End Get
        Set(ByVal Value As String)
            SetProperty(m_email_address, Value)
        End Set
    End Property

    Public Property Address() As Address
        Get
            Return m_address
        End Get
        Set(ByVal Value As Address)
            m_address = Value
        End Set
    End Property

    Public Property EmailOk() As Boolean
        Get
            Return m_email_ok
        End Get
        Set(ByVal Value As Boolean)
            m_email_ok = Value
        End Set
    End Property
    Public Property CountryName() As String
        Get
            Return m_Country_Name
        End Get
        Set(ByVal value As String)
            SetProperty(m_Country_Name, value)
        End Set
    End Property
    Public Property CountryCode() As String
        Get
            Return m_Country_Code
        End Get
        Set(ByVal value As String)
            SetProperty(m_Country_Code, value)
        End Set
    End Property

    'Public Property CaliforniaPartialTax() As Boolean
    '    Get
    '        Return m_CaliforniaPartialTax
    '    End Get
    '    Set(ByVal Value As Boolean)
    '        SetProperty(m_CaliforniaPartialTax, Value)
    '    End Set
    'End Property


    Public Property Enabledocumentidnumber() As String
        Get
            Return m_enabledocumentidnumber
        End Get
        Set(ByVal Value As String)
            SetProperty(m_enabledocumentidnumber, Value)
        End Set
    End Property


    Public Property ETDocumentidnumber() As String
        Get
            Return m_ETdocumentidnumber
        End Get
        Set(ByVal Value As String)
            SetProperty(m_ETdocumentidnumber, Value)
        End Set
    End Property


    Public Property PhoneNumber() As String
        Get
            Return m_phone_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_phone_number, Value)
        End Set
    End Property

    Public Property PhoneExtension() As String
        Get
            Return m_phone_extension
        End Get
        Set(ByVal Value As String)
            SetProperty(m_phone_extension, Value)
        End Set
    End Property

    Public Property FaxNumber() As String
        Get
            Return m_fax_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_fax_number, Value)
        End Set
	End Property

    Public Property StatusCode() As Short
        Get
            Return m_status_code
        End Get
        Set(ByVal Value As Short)
            m_status_code = Value
        End Set
    End Property

    Public Property PIN1() As String
        Get
            Return m_pin_1
        End Get
        Set(ByVal Value As String)
            SetProperty(m_pin_1, Value)
        End Set
    End Property

    Public Property PIN2() As String
        Get
            Return m_pin_2
        End Get
        Set(ByVal Value As String)
            SetProperty(m_pin_2, Value)
        End Set
    End Property

    Public Property PIN3() As String
        Get
            Return m_pin_3
        End Get
        Set(ByVal Value As String)
            SetProperty(m_pin_3, Value)
        End Set
    End Property
    Public Property LdapID() As String
        Get
            Return m_ldapID
        End Get
        Set(ByVal Value As String)
            SetProperty(m_ldapID, Value)
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return myupdatedate
        End Get
        Set(ByVal Value As Date)
            myupdatedate = Value
        End Set
    End Property


    'Public Property AlternateUserName() As String
    '    Get
    '        Return m_AlternateUserName
    '    End Get
    '    Set(ByVal Value As String)
    '        SetProperty(m_AlternateUserName, Value)
    '    End Set
    'End Property

    Public Property SAPBillToAccounts() As Account()
        Get
            If ReturnDeletedObjects Then
                Dim accounts(m_sap_bill_to_accounts.Count - 1) As Account
                m_sap_bill_to_accounts.CopyTo(accounts)
                Return accounts
            Else
                Dim iter As IEnumerator = m_sap_bill_to_accounts.GetEnumerator
                Dim num_of_deletes As Integer = 0

                While iter.MoveNext
                    If CType(iter.Current, Account).Action = CoreObject.ActionType.DELETE Then
                        num_of_deletes = num_of_deletes + 1
                    End If
                End While

                Dim j As Integer = 0
                Dim accounts(m_sap_bill_to_accounts.Count - 1 - num_of_deletes) As Account

                iter.Reset()

                While iter.MoveNext
                    If CType(iter.Current, Account).Action <> CoreObject.ActionType.DELETE Then
                        accounts.SetValue(iter.Current, j)
                        j = j + 1
                    End If
                End While
                Return accounts
            End If
        End Get
        Set(ByVal Value As Account())
            m_sap_bill_to_accounts.Clear()
            For Each acc As Account In Value
                m_sap_bill_to_accounts.Add(acc)
            Next
        End Set
    End Property

    Public ReadOnly Property SISLegacyShipToAccounts() As Account()
        Get
            Dim accounts(m_legacy_ship_to_accounts.Count - 1) As Account
            m_legacy_ship_to_accounts.CopyTo(accounts)
            Return accounts
        End Get
    End Property

    Public Property SISLegacyBillToAccounts() As Account()
        Get
            If ReturnDeletedObjects Then
                Dim accounts(m_legacy_bill_to_accounts.Count - 1) As Account
                m_legacy_bill_to_accounts.CopyTo(accounts)
                Return accounts
            Else
                Dim iter As IEnumerator = m_legacy_bill_to_accounts.GetEnumerator
                Dim num_of_deletes As Integer = 0

                While iter.MoveNext
                    If CType(iter.Current, Account).Action = CoreObject.ActionType.DELETE Then
                        num_of_deletes = num_of_deletes + 1
                    End If
                End While

                Dim j As Integer = 0
                Dim accounts(m_legacy_bill_to_accounts.Count - 1 - num_of_deletes) As Account

                iter.Reset()

                While iter.MoveNext
                    If CType(iter.Current, Account).Action <> CoreObject.ActionType.DELETE Then
                        accounts.SetValue(iter.Current, j)
                        j = j + 1
                    End If
                End While
                Return accounts
            End If
        End Get
        Set(ByVal Value As Account())

        End Set
    End Property

    Public Property Tax_Exempt() As Boolean
        Get
            Return m_tax_exempt
        End Get
        Set(ByVal Value As Boolean)
            m_tax_exempt = Value
        End Set
    End Property

    Public Property IsValidTBSubscription() As Boolean
        Get
            Return m_IsValidTBSubscription
        End Get
        Set(ByVal Value As Boolean)
            m_IsValidTBSubscription = Value
        End Set
    End Property

    Public Property MarketingInterests() As MarketingInterest()
        Get
            If ReturnDeletedObjects Then
                Dim interests(m_marketing_interests.Count - 1) As MarketingInterest
                m_marketing_interests.CopyTo(interests)
                Return interests
            Else
                Dim iter As IEnumerator = m_marketing_interests.GetEnumerator
                Dim num_of_deletes As Integer = 0

                While iter.MoveNext
                    If CType(iter.Current, MarketingInterest).Action = CoreObject.ActionType.DELETE Then
                        num_of_deletes = num_of_deletes + 1
                    End If
                End While

                Dim j As Integer = 0
                Dim interests(m_marketing_interests.Count - 1 - num_of_deletes) As MarketingInterest

                iter.Reset()

                While iter.MoveNext
                    If CType(iter.Current, MarketingInterest).Action <> CoreObject.ActionType.DELETE Then
                        interests.SetValue(iter.Current, j)
                        j = j + 1
                    End If
                End While
                Return interests
            End If
        End Get
        Set(ByVal Value As MarketingInterest())

        End Set
    End Property

    Public Property CreditCards() As CreditCard()
        Get
            If ReturnDeletedObjects Then
                Dim cards(m_credit_cards.Count - 1) As CreditCard
                m_credit_cards.CopyTo(cards)
                Return cards
            Else
                Dim iter As IEnumerator = m_credit_cards.GetEnumerator
                Dim num_of_deletes As Integer = 0

                While iter.MoveNext
                    If CType(iter.Current, CreditCard).Action = CoreObject.ActionType.DELETE Then
                        num_of_deletes = num_of_deletes + 1
                    End If
                End While

                Dim j As Integer = 0
                Dim cards(m_credit_cards.Count - 1 - num_of_deletes) As CreditCard

                iter.Reset()

                While iter.MoveNext
                    If CType(iter.Current, CreditCard).Action <> CoreObject.ActionType.DELETE Then
                        cards.SetValue(iter.Current, j)
                        j = j + 1
                    End If
                End While
                Return cards
            End If
        End Get
        Set(ByVal Value As CreditCard())

        End Set
    End Property

    '***************Changes done for Bug# 1212 Starts here***************
    '***************Kannapiran S*****************
    Public Property UserLocation() As Integer
        Get
            Return (m_UserLocation)
        End Get
        Set(ByVal Value As Integer)
            m_UserLocation = Value
        End Set
    End Property '***************Changes done for Bug# 1212 Ends here***************

    Public Property IsPunchOutUser() As Boolean
        Get
            Return m_IsPunchOutUser
        End Get
        Set(ByVal Value As Boolean)
            m_IsPunchOutUser = Value
        End Set
    End Property

    Public Property PunchOutUserName() As String
        Get
            Return m_PunchOutUserName
        End Get
        Set(ByVal Value As String)
            m_PunchOutUserName = Value
        End Set
    End Property
    Public Property LastLoginTime() As String
        Get
            Return m_LastLoginTime
        End Get
        Set(ByVal Value As String)
            m_LastLoginTime = Value
        End Set
    End Property
#End Region

#Region "   Methods   "
    Public Sub AcceptMarketingInterestChange()
        For Each mIntst As MarketingInterest In MarketingInterests
            If mIntst.Action = ActionType.DELETE Then
                m_marketing_interests.Remove(mIntst)
            End If
        Next
    End Sub

    Public Function GetCreditCard(ByVal sequence_number As String) As CreditCard
        Dim card As CreditCard

        For Each c As CreditCard In CreditCards
            If c.SequenceNumber = sequence_number Then
                card = c
            End If
        Next

        Return card
    End Function

    Public Function AddSAPBillToAccount(ByVal account_number As String) As Account
        Dim acct As New Account With {
            .AccountNumber = account_number,
            .Customer = Me
        }
        'acct.AccountType = Account.Type.SAP_BILL_TO_ACCOUNT
        m_sap_bill_to_accounts.Add(acct)

        Return acct
    End Function

    Public Sub ClearSAPBillToAccounts()
        m_sap_bill_to_accounts.Clear()
    End Sub

    Public Sub AddSISLegacyBillToAccount(ByVal account_number As String)
        Dim acct As New Account With {
            .AccountNumber = account_number,
            .Customer = Me
        }
        'acct.AccountType = Account.Type.SIS_LEGACY_BILL_TO_ACCOUNT
        m_legacy_bill_to_accounts.Add(acct)
    End Sub

    Public Sub AddAccount(ByVal account As Account)
        m_sap_bill_to_accounts.Add(account)
    End Sub

    Public Sub RemoveAccount(ByRef account As Account)
        account.Action = CoreObject.ActionType.DELETE
    End Sub

    Public Function AddCreditCard(ByVal credit_card_number As String, ByVal expiration_date As Date, ByVal credit_card_type As CreditCardType, ByVal csc As String, ByVal nickname As String) As CreditCard
        Dim card As New CreditCard(credit_card_type)

        For Each cc As CreditCard In CreditCards
            If cc.HideCardFlag = False And cc.NickName <> "" And cc.NickName = nickname Then
                Throw New Exception("Please select unique nickname")
            End If
        Next

        card.CreditCardNumber = credit_card_number
        card.ExpirationDate = expiration_date.ToString("MM/dd/yyyy")
        card.CSCCode = csc
        card.NickName = nickname
        card.Customer = Me

        m_credit_cards.Add(card)
        Return card
    End Function

    Public Sub RemoveCreditCard(ByRef card As CreditCard)
        card.Action = CoreObject.ActionType.DELETE
    End Sub

    Public Sub AddCreditCard(ByVal credit_card As CreditCard)
        m_credit_cards.Add(credit_card)
    End Sub

    Public Sub AddMarketingInterest(ByVal marketing_interest As MarketingInterest)
        marketing_interest.Customer = Me
        m_marketing_interests.Add(marketing_interest)
    End Sub

    Public Sub RemoveMarketingInterest(ByRef marketing_interest As MarketingInterest)
        marketing_interest.Action = CoreObject.ActionType.DELETE
    End Sub

    Public Sub ClearAllSAPAccounts()
        m_sap_bill_to_accounts.Clear()
    End Sub

#End Region

    Public Overrides Function ToString() As String
        Return $"CustomerID: {CustomerID}, Sequence: {SequenceNumber}, Ldap ID: {LdapID}, First Name: {FirstName}, Last Name: {LastName}, Email: {EmailAddress}"
    End Function

End Class
