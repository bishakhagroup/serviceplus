Public Class CourseSchedule
    Inherits CoreObject
    Implements IComparable

    Private m_Course As Course
    Private m_StartDate As Date
    Private m_EndDate As Date
    Private m_Time As String = String.Empty
    Private m_Location As CourseLocation
    Private m_Instructor As CourseInstructor
    Private m_Capacity As Integer
    Private m_Vacancy As Integer = -1 'initialize to -1 since it is not retrieved when it is first loaded from DB
    Private m_ReminderDaysBefore As Integer
    Private m_Notes As String = String.Empty
    Private m_PartNumber As String = String.Empty
    Public Shared m_SortOrder As Integer
    Private m_SequenceNumber As Integer
    Private m_StatusCode As Integer
    Private m_UpdateDate As Date
    Private m_isDefaultLocation As Boolean
    Private m_modellist As String = String.Empty
    Private m_Hide As Integer
    Private m_OldClass As Integer
    Private m_TypeCode As Integer = 0

    Public Enum enumSortOrder
        ProductCategoryASC = 0
        ProductCategoryDESC = 1
        ModuleNumberASC = 2
        ModuleNumberDESC = 3
        CourseTitleASC = 4
        CourseTitleDESC = 5
        CourseLocationASC = 6
        CourseLocationDESC = 7
    End Enum
    Public Property TypeCode() As Integer
        Get
            Return m_TypeCode
        End Get
        Set(ByVal Value As Integer)
            m_TypeCode = Value
        End Set
    End Property

    Public Property OldClass() As Integer
        Get
            Return m_OldClass
        End Get
        Set(ByVal Value As Integer)
            m_OldClass = Value
        End Set
    End Property

    Public Property Hide() As Integer
        Get
            Return m_Hide
        End Get
        Set(ByVal Value As Integer)
            m_Hide = Value
        End Set
    End Property
    Public Property IsDefaulLocation() As Boolean
        Get
            Return m_isDefaultLocation
        End Get
        Set(ByVal Value As Boolean)
            m_isDefaultLocation = Value
        End Set
    End Property

    Public Property Course() As Course
        Get
            If m_Course Is Nothing Then m_Course = New Course
            Return m_Course
        End Get
        Set(ByVal Value As Course)
            m_Course = Value
        End Set
    End Property
    Public ReadOnly Property StartDateDisplay() As String
        Get
            Dim strDate = m_StartDate.ToString("MM/dd/yyyy")
            For Each str As String In NullDateTime
                If strDate = str Then
                    strDate = "TBD"
                End If
            Next
            Return strDate
        End Get
    End Property
    Public Property StartDate() As Date
        Get
            Return m_StartDate
        End Get
        Set(ByVal Value As Date)
            m_StartDate = Value
        End Set
    End Property
    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property
    Public ReadOnly Property EndDateDisplay() As String
        Get
            Dim strDate As String = m_EndDate.ToString("MM/dd/yyyy")
            For Each str As String In NullDateTime
                If strDate = str Then
                    strDate = "TBD"
                End If
            Next
            Return strDate
        End Get
    End Property

    Public Property EndDate() As Date
        Get
            Return m_EndDate
        End Get
        Set(ByVal Value As Date)
            m_EndDate = Value
        End Set
    End Property
    Public Property Time() As String
        Get
            Return m_Time
        End Get
        Set(ByVal Value As String)
            m_Time = Value
        End Set
    End Property

    Public Property Location() As CourseLocation
        Get
            If m_Location Is Nothing Then
                m_Location = New CourseLocation
            End If
            Return m_Location
        End Get
        Set(ByVal Value As CourseLocation)
            m_Location = Value
        End Set
    End Property

    Public Property Instructor() As CourseInstructor
        Get
            If m_Instructor Is Nothing Then
                m_Instructor = New CourseInstructor
            End If
            Return m_Instructor
        End Get
        Set(ByVal Value As CourseInstructor)
            m_Instructor = Value
        End Set
    End Property

    Public Property Capacity() As Integer
        Get
            Return m_Capacity
        End Get
        Set(ByVal Value As Integer)
            m_Capacity = Value
        End Set
    End Property

    Public Property ReminderDaysBefore() As Integer
        Get
            Return m_ReminderDaysBefore
        End Get
        Set(ByVal Value As Integer)
            m_ReminderDaysBefore = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property PartNumber() As String
        Get
            Return m_PartNumber
        End Get
        Set(ByVal Value As String)
            m_PartNumber = Value
        End Set
    End Property

    Public Property Vacancy() As Integer
        Get
            Return m_Vacancy
        End Get
        Set(ByVal Value As Integer)
            m_Vacancy = Value
        End Set
    End Property
    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property
    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property

    Public Property StatusCode() As Integer
        Get
            Return m_StatusCode
        End Get
        Set(ByVal Value As Integer)
            m_StatusCode = Value
        End Set
    End Property

    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Try
            Dim cs As CourseSchedule = CType(obj, CourseSchedule)
            Select Case (m_SortOrder)
                Case 0
                    CompareTo = Course.MajorCategory.Description.CompareTo(cs.Course.MajorCategory.Description)
                    If CompareTo = 0 Then
                        Return CompareTitleDateModelLocation(cs)
                    End If
                Case 1
                    CompareTo = -Course.MajorCategory.Description.CompareTo(cs.Course.MajorCategory.Description)
                    If CompareTo = 0 Then
                        Return CompareTitleDateModelLocation(cs)
                    End If
                Case 2
                    CompareTo = Course.Model.CompareTo(cs.Course.Model)
                    If CompareTo = 0 Then
                        Return CompareTitleDateLocation(cs)
                    End If
                Case 3
                    CompareTo = -Course.Model.CompareTo(cs.Course.Model)
                    If CompareTo = 0 Then
                        Return CompareTitleDateLocation(cs)
                    End If
                Case 4
                    CompareTo = Course.Title.CompareTo(cs.Course.Title)
                    If CompareTo = 0 Then
                        Return CompareDateModelLocation(cs)
                    End If
                Case 5
                    CompareTo = -Course.Title.CompareTo(cs.Course.Title)
                    If CompareTo = 0 Then
                        Return CompareDateModelLocation(cs)
                    End If
                Case 6
                    CompareTo = Location.State.CompareTo(cs.Location.State)
                    If CompareTo = 0 Then
                        Return CompareTitleDateModel(cs)
                    End If
                Case 7
                    CompareTo = -Location.State.CompareTo(cs.Location.State)
                    If CompareTo = 0 Then
                        Return CompareTitleDateModel(cs)
                    End If
            End Select
        Catch ex As Exception
            CompareTo = 0 'some exception happens, not comparable, just treat them as equal
        End Try
    End Function

    Private Function CompareDateModelLocation(ByRef cs As CourseSchedule)
        Dim result As Integer = StartDate.CompareTo(cs.StartDate)
        If result <> 0 Then
            Return result
        Else
            result = Course.Model.CompareTo(cs.Course.Model)
            If result <> 0 Then
                Return result
            Else
                result = Location.State.CompareTo(cs.Location.State)
                Return result
            End If
        End If
    End Function

    Private Function CompareTitleDateModel(ByRef cs As CourseSchedule)
        Dim result As Integer = Course.Title.CompareTo(cs.Course.Title)
        If result <> 0 Then
            Return result
        Else
            result = StartDate.CompareTo(cs.StartDate)
            If result <> 0 Then
                Return result
            Else
                result = Course.Model.CompareTo(cs.Course.Model)
                Return result
            End If
        End If
    End Function
    Private Function CompareTitleDateModelLocation(ByRef cs As CourseSchedule)
        Dim result As Integer = Course.Title.CompareTo(cs.Course.Title)
        If result <> 0 Then
            Return result
        Else
            result = StartDate.CompareTo(cs.StartDate)
            If result <> 0 Then
                Return result
            Else
                result = Course.Model.CompareTo(cs.Course.Model)
                If result <> 0 Then
                    Return result
                Else
                    result = Location.State.CompareTo(cs.Location.State)
                    Return result
                End If
            End If
        End If
    End Function

    Private Function CompareTitleDateLocation(ByRef cs As CourseSchedule)
        Dim result As Integer = Course.Title.CompareTo(cs.Course.Title)
        If result <> 0 Then
            Return result
        Else
            result = StartDate.CompareTo(cs.StartDate)
            If result <> 0 Then
                Return result
            Else
                result = Location.State.CompareTo(cs.Location.State)
                Return result
            End If
        End If
    End Function

    Public Property ModelList() As String
        Get
            Return m_modellist
        End Get
        Set(ByVal Value As String)
            m_modellist = Value
        End Set
    End Property

End Class
