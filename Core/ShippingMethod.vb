Public Class ShippingMethod

    Private m_shipping_method_code As String
    Private m_description As String
    Private m_tracking_url As String
    Private m_sis_pick_group_code As String
    Private m_sis_carrier_code As String
    Private m_sis_backorder_override As Boolean
    Private m_sis_bo_pick_group_code As String
    Private m_sis_bo_carrier_code As String
    Private m_ISBackOrderSameShippingMethod As Boolean = False

    Public Sub New()

    End Sub

    Public Property ShippingMethodCode() As String
        Get
            Return m_shipping_method_code
        End Get
        Set(ByVal Value As String)
            m_shipping_method_code = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property TrackingURL() As String
        Get
            Return m_tracking_url
        End Get
        Set(ByVal Value As String)
            m_tracking_url = Value
        End Set
    End Property

    Public Property SISPickGroupCode() As String
        Get
            Return m_sis_pick_group_code
        End Get
        Set(ByVal Value As String)
            m_sis_pick_group_code = Value
        End Set
    End Property

    Public Property SISCarrierCode() As String
        Get
            Return m_sis_carrier_code
        End Get
        Set(ByVal Value As String)
            m_sis_carrier_code = Value
        End Set
    End Property

    Public Property SISBackOrderOverride() As Boolean
        Get
            Return m_sis_backorder_override
        End Get
        Set(ByVal Value As Boolean)
            m_sis_backorder_override = Value
        End Set
    End Property

    Public Property SISBackOrderPickGroupCode() As String
        Get
            Return m_sis_bo_pick_group_code
        End Get
        Set(ByVal Value As String)
            m_sis_bo_pick_group_code = Value
        End Set
    End Property

    Public Property SISBackOrderCarrierCode() As String
        Get
            Return m_sis_bo_carrier_code
        End Get
        Set(ByVal Value As String)
            m_sis_bo_carrier_code = Value
        End Set
    End Property

    Public Property ISBackOrderSameShippingMethod() As Boolean
        Get
            Return m_ISBackOrderSameShippingMethod
        End Get
        Set(ByVal Value As Boolean)
            m_ISBackOrderSameShippingMethod = Value
        End Set
    End Property

End Class
