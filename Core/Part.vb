Public Class Part
    Inherits Product
    Implements IComparable

    Public Enum PartsFinderRefineSearchType
        ByDescription
        ByComponentReference
    End Enum

    Private m_module_name As String
    Private m_module_id As String
    Private m_has_exploded_view As Boolean
    Private m_part_reference_id As String
    Private m_part_usage As String
    Private m_model_group As String
    Private m_model As String
    Private m_dealer_price As Double
    Private m_distributor_price As Double
    Private m_canadian_price As Double
    Private m_BillingOnly As String
    Private m_SalesText As String
    Private Shared m_SortOrder As Integer

    Public Enum enumSortOrder
        ModuleASC = 0
        ModuleDESC = 1
        PartNumberASC = 2
        PartNumberDESC = 3
    End Enum

    Public Sub New()
        m_type = Product.ProductType.Part
        m_has_exploded_view = False
    End Sub

    Public Property ModuleName() As String
        Get
            Return m_module_name
        End Get
        Set(ByVal Value As String)
            m_module_name = Value
        End Set
    End Property

    Public Property ModuleID() As String
        Get
            Return m_module_id
        End Get
        Set(ByVal Value As String)
            m_module_id = Value
        End Set
    End Property

    Public Property HasExplodedView() As Boolean
        Get
            Return m_has_exploded_view
        End Get
        Set(ByVal Value As Boolean)
            m_has_exploded_view = Value
        End Set
    End Property

    Public Property PartReferenceID() As String
        Get
            Return m_part_reference_id
        End Get
        Set(ByVal Value As String)
            m_part_reference_id = Value
        End Set
    End Property

    Public Property PartUsage() As String
        Get
            Return m_part_usage
        End Get
        Set(ByVal Value As String)
            m_part_usage = Value
        End Set
    End Property

    Public Property ModelGroup() As String
        Get
            Return m_model_group
        End Get
        Set(ByVal Value As String)
            m_model_group = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_model
        End Get
        Set(ByVal Value As String)
            m_model = Value
        End Set
    End Property

    Public Property DealerPrice() As Double
        Get
            Return m_dealer_price
        End Get
        Set(ByVal Value As Double)
            m_dealer_price = Value
        End Set
    End Property

    Public Property DistributorPrice() As Double
        Get
            Return m_distributor_price
        End Get
        Set(ByVal Value As Double)
            m_distributor_price = Value
        End Set
    End Property

    Public Property CanadianPrice() As Double
        Get
            Return m_canadian_price
        End Get
        Set(ByVal Value As Double)
            m_canadian_price = Value
        End Set
    End Property

    Public Property BillingOnly() As String
        Get
            Return m_BillingOnly
        End Get
        Set(ByVal Value As String)
            m_BillingOnly = Value
        End Set
    End Property


    Public Property SalesText() As String
        Get
            Return m_SalesText
        End Get
        Set(ByVal Value As String)
            m_SalesText = Value
        End Set
    End Property

    Public Shared Property SortOrder() As enumSortOrder
        Get
            SortOrder = m_SortOrder
        End Get
        Set(ByVal Value As enumSortOrder)
            m_SortOrder = Value
        End Set
    End Property
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
        Select Case (m_SortOrder)
            Case 0
                CompareTo = ModuleName < CType(obj, Part).ModuleName
            Case 1
                CompareTo = ModuleName > CType(obj, Part).ModuleName
            Case 2
                CompareTo = PartNumber < CType(obj, Part).PartNumber
            Case 3
                CompareTo = PartNumber > CType(obj, Part).PartNumber
        End Select
    End Function

End Class
