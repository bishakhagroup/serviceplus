Public Class Functionality
    Inherits CoreObject


    Private m_FunctionalityID As Integer
    Private m_FunctionalityName As String
    Private m_GroupID As String
    Private m_URLMapped As String
    Private m_isDeleted As String
    Private m_SITESTATUS As String

    Public Sub New()
    End Sub

    Public Property FunctionalityId() As Integer
        Get
            Return m_FunctionalityID
        End Get
        Set(ByVal value As Integer)
            m_FunctionalityID = value
        End Set
    End Property

    Public Property FunctionalityName() As String
        Get
            Return m_FunctionalityName
        End Get
        Set(ByVal value As String)
            m_FunctionalityName = value
        End Set
    End Property
    Public Property GroupID() As String
        Get
            Return m_GroupID
        End Get
        Set(ByVal value As String)
            m_GroupID = value
        End Set
    End Property
    Public Property URLMapped() As String
        Get
            Return m_URLMapped
        End Get
        Set(ByVal value As String)
            m_URLMapped = value
        End Set
    End Property

    Public Property IsDeleted() As String
        Get
            Return m_isDeleted
        End Get
        Set(ByVal value As String)
            m_isDeleted = value
        End Set
    End Property
    Public Property SITESTATUS() As String
        Get
            Return m_SITESTATUS
        End Get
        Set(ByVal value As String)
            m_SITESTATUS = value
        End Set
    End Property

End Class