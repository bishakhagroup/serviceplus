Public Class CourseReservation
    Inherits CoreObject

    Private m_CourseSchedule As CourseSchedule
    Private m_ReservationNumber As String = String.Empty
    Private m_SequenceNumber As Integer
    Private m_StatusCode As Integer
    Private m_UpdateDate As Date
    Private m_CustomerSIAMID As String = String.Empty
    Private m_CustomerFirstName As String = String.Empty
    Private m_CustomerLastName As String = String.Empty
    Private m_Notes As String = String.Empty
    Private m_sentinvoiceorquote As Integer
    Private m_AdminSIAMID As String = String.Empty
    Private m_Participants() As CourseParticipant

    Public Property CourseSchedule() As CourseSchedule
        Get
            If m_CourseSchedule Is Nothing Then
                m_CourseSchedule = New CourseSchedule
            End If
            Return m_CourseSchedule
        End Get
        Set(ByVal Value As CourseSchedule)
            m_CourseSchedule = Value
        End Set
    End Property

    Public Property CustomerSIAMID() As String
        Get
            Return m_CustomerSIAMID
        End Get
        Set(ByVal Value As String)
            m_CustomerSIAMID = Value
        End Set
    End Property
    Public Property CustomerFirstName() As String
        Get
            Return m_CustomerFirstName
        End Get
        Set(ByVal Value As String)
            m_CustomerFirstName = Value
        End Set
    End Property
    Public Property CustomerLastName() As String
        Get
            Return m_CustomerLastName
        End Get
        Set(ByVal Value As String)
            m_CustomerLastName = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property
    Public Property Participants() As CourseParticipant()
        Get
            Return m_Participants
        End Get
        Set(ByVal Value As CourseParticipant())
            m_Participants = Value
        End Set
    End Property
    Public Property Sentinvoiceorquote() As Integer
        Get
            Return m_sentinvoiceorquote
        End Get
        Set(ByVal Value As Integer)
            m_sentinvoiceorquote = Value
        End Set
    End Property

    Public Property StatusCode() As Integer
        Get
            Return m_StatusCode
        End Get
        Set(ByVal Value As Integer)
            m_StatusCode = Value
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property

    Public Property ReservationNumber() As String
        Get
            Return m_ReservationNumber
        End Get
        Set(ByVal Value As String)
            m_ReservationNumber = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property AdminSIAMID() As String
        Get
            Return m_AdminSIAMID
        End Get
        Set(ByVal Value As String)
            m_AdminSIAMID = Value
        End Set
    End Property

    Public Sub AddParticipant(ByRef cp As CourseParticipant)
        Dim number As Integer = 0
        If Not m_Participants Is Nothing Then
            number = m_Participants.Length
        End If

        ReDim Preserve m_Participants(number)
        m_Participants(number) = cp
    End Sub

    Public Function ParticipantsStatus() As Byte
        Dim status As Byte = 0

        For Each cp As CourseParticipant In m_Participants
            If (cp.Status = "R") Then
                status = status Or 1
            End If
            If (cp.Status = "W") Then
                status = status Or 2
            End If
            If (cp.Status = "C") Then
                status = status Or 4
            End If
        Next

        Return status
    End Function
End Class
