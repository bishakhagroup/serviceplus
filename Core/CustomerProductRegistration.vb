Imports System.Collections
'Imports Sony.US.ServicesPLUS.Data

Public Class CustomerProductRegistration
    Inherits CoreObject

#Region "[Private m_Variables]"

    Private m_CustomerID As String = String.Empty

    Private m_SUFFIX As String = String.Empty
    Private m_FIRST_NAME As String = String.Empty
    Private m_LAST_NAME As String = String.Empty
    Private m_EMAIL As String = String.Empty
    Private m_TITLE As String = String.Empty
    Private m_COMPANY_NAME As String = String.Empty
    Private m_COMPANY_TYPE As String = String.Empty
    Private m_LINE_OF_WORK As String = String.Empty
    Private m_ADDRESS1 As String = String.Empty
    Private m_ADDRESS2 As String = String.Empty
    Private m_ADDRESS3 As String = String.Empty
    Private m_CITY As String = String.Empty
    Private m_STATE As String = String.Empty
    Private m_ZIPCODE As String = String.Empty
    Private m_COUNTRY As String = String.Empty
    Private m_PR_PHONE As String = String.Empty
    Private m_SEC_PHONE As String = String.Empty
    Private m_EXT_NO As String = String.Empty
    Private m_FAX As String = String.Empty
    Private m_NOTES As String = String.Empty
    Private m_OPTOUT As String = String.Empty
    Private m_CODES As String = String.Empty
    Private m_BOOTH_BULLETIN_NUM As String = String.Empty
    Private m_MODEL_INTERESTED As String = String.Empty
    Private m_PROMO_DESC As String = String.Empty
    Private m_FOLLOW_UP_ACTION As String = String.Empty
    Private m_PURCHASE_TIME_FRAME As String = String.Empty
    Private m_SHOW_VISIT_DT As String = String.Empty
    Private m_STATION_ID As String = String.Empty
    Private m_LITERATURE As String = String.Empty
    Private m_DEALER As String = String.Empty
    Private m_QUESTION1 As String = String.Empty
    Private m_ANSWER1 As String = String.Empty
    Private m_QUESTION2 As String = String.Empty
    Private m_ANSWER2 As String = String.Empty
    Private m_QUESTION3 As String = String.Empty
    Private m_ANSWER3 As String = String.Empty
    Private m_QUESTION4 As String = String.Empty
    Private m_ANSWER4 As String = String.Empty
    Private m_QUESTION5 As String = String.Empty
    Private m_ANSWER5 As String = String.Empty
    Private m_QUESTION6 As String = String.Empty
    Private m_ANSWER6 As String = String.Empty
    Private m_QUESTION7 As String = String.Empty
    Private m_ANSWER7 As String = String.Empty
    Private m_QUESTION8 As String = String.Empty
    Private m_ANSWER8 As String = String.Empty
    Private m_QUESTION9 As String = String.Empty
    Private m_ANSWER9 As String = String.Empty
    Private m_QUESTION10 As String = String.Empty
    Private m_ANSWER10 As String = String.Empty
    Private m_CONTACTEVENTLEVEL As String = String.Empty
    Private m_SUCCESS_MESSAGE As String = String.Empty
#End Region


#Region "[Properties]"



    Public Property CustomerID() As String
        Get
            Return m_CustomerID
        End Get
        Set(ByVal Value As String)
            m_CustomerID = Value
        End Set
    End Property

    Public Property SUFFIX() As String
        Get
            Return m_SUFFIX
        End Get
        Set(ByVal Value As String)
            m_SUFFIX = Value
        End Set
    End Property

    Public Property FIRST_NAME() As String
        Get
            Return m_FIRST_NAME
        End Get
        Set(ByVal Value As String)
            m_FIRST_NAME = Value
        End Set
    End Property
    Public Property LAST_NAME() As String
        Get
            Return m_LAST_NAME
        End Get
        Set(ByVal Value As String)
            m_LAST_NAME = Value
        End Set
    End Property
    Public Property EMAIL() As String
        Get
            Return m_EMAIL
        End Get
        Set(ByVal Value As String)
            m_EMAIL = Value
        End Set
    End Property
    Public Property TITLE() As String
        Get
            Return m_TITLE
        End Get
        Set(ByVal Value As String)
            m_TITLE = Value
        End Set
    End Property
    Public Property COMPANY_NAME() As String
        Get
            Return m_COMPANY_NAME
        End Get
        Set(ByVal Value As String)
            m_COMPANY_NAME = Value
        End Set
    End Property
    Public Property COMPANY_TYPE() As String
        Get
            Return m_COMPANY_TYPE
        End Get
        Set(ByVal Value As String)
            m_COMPANY_TYPE = Value
        End Set
    End Property
    Public Property LINE_OF_WORK() As String
        Get
            Return m_LINE_OF_WORK
        End Get
        Set(ByVal Value As String)
            m_LINE_OF_WORK = Value
        End Set
    End Property
    Public Property ADDRESS1() As String
        Get
            Return m_ADDRESS1
        End Get
        Set(ByVal Value As String)
            m_ADDRESS1 = Value
        End Set
    End Property
    Public Property ADDRESS2() As String
        Get
            Return m_ADDRESS2
        End Get
        Set(ByVal Value As String)
            m_ADDRESS2 = Value
        End Set
    End Property
    Public Property ADDRESS3() As String
        Get
            Return m_ADDRESS3
        End Get
        Set(ByVal Value As String)
            m_ADDRESS3 = Value
        End Set
    End Property
    Public Property CITY() As String
        Get
            Return m_CITY
        End Get
        Set(ByVal Value As String)
            m_CITY = Value
        End Set
    End Property
    Public Property STATE() As String
        Get
            Return m_STATE
        End Get
        Set(ByVal Value As String)
            m_STATE = Value
        End Set
    End Property
    Public Property ZIPCODE() As String
        Get
            Return m_ZIPCODE
        End Get
        Set(ByVal Value As String)
            m_ZIPCODE = Value
        End Set
    End Property
    Public Property COUNTRY() As String
        Get
            Return m_COUNTRY
        End Get
        Set(ByVal Value As String)
            m_COUNTRY = Value
        End Set
    End Property
    Public Property PR_PHONE() As String
        Get
            Return m_PR_PHONE
        End Get
        Set(ByVal Value As String)
            m_PR_PHONE = Value
        End Set
    End Property
    Public Property SEC_PHONE() As String
        Get
            Return m_SEC_PHONE
        End Get
        Set(ByVal Value As String)
            m_SEC_PHONE = Value
        End Set
    End Property
    Public Property EXT_NO() As String
        Get
            Return m_EXT_NO
        End Get
        Set(ByVal Value As String)
            m_EXT_NO = Value
        End Set
    End Property
    Public Property FAX() As String
        Get
            Return m_FAX
        End Get
        Set(ByVal Value As String)
            m_FAX = Value
        End Set
    End Property
    Public Property NOTES() As String
        Get
            Return m_NOTES
        End Get
        Set(ByVal Value As String)
            m_NOTES = Value
        End Set
    End Property
    Public Property OPTOUT() As String
        Get
            Return m_OPTOUT
        End Get
        Set(ByVal Value As String)
            m_OPTOUT = Value
        End Set
    End Property
    Public Property CODES() As String
        Get
            Return m_CODES
        End Get
        Set(ByVal Value As String)
            m_CODES = Value
        End Set
    End Property
    Public Property BOOTH_BULLETIN_NUM() As String
        Get
            Return m_BOOTH_BULLETIN_NUM
        End Get
        Set(ByVal Value As String)
            m_BOOTH_BULLETIN_NUM = Value
        End Set
    End Property
    Public Property MODEL_INTERESTED() As String
        Get
            Return m_MODEL_INTERESTED
        End Get
        Set(ByVal Value As String)
            m_MODEL_INTERESTED = Value
        End Set
    End Property
    Public Property PROMO_DESC() As String
        Get
            Return m_PROMO_DESC
        End Get
        Set(ByVal Value As String)
            m_PROMO_DESC = Value
        End Set
    End Property
    Public Property FOLLOW_UP_ACTION() As String
        Get
            Return m_FOLLOW_UP_ACTION
        End Get
        Set(ByVal Value As String)
            m_FOLLOW_UP_ACTION = Value
        End Set
    End Property
    Public Property PURCHASE_TIME_FRAME() As String
        Get
            Return m_PURCHASE_TIME_FRAME
        End Get
        Set(ByVal Value As String)
            m_PURCHASE_TIME_FRAME = Value
        End Set
    End Property
    Public Property SHOW_VISIT_DT() As String
        Get
            Return m_SHOW_VISIT_DT
        End Get
        Set(ByVal Value As String)
            m_SHOW_VISIT_DT = Value
        End Set
    End Property
    Public Property STATION_ID() As String
        Get
            Return m_STATION_ID
        End Get
        Set(ByVal Value As String)
            m_STATION_ID = Value
        End Set
    End Property
    Public Property LITERATURE() As String
        Get
            Return m_LITERATURE
        End Get
        Set(ByVal Value As String)
            m_LITERATURE = Value
        End Set
    End Property
    Public Property DEALER() As String
        Get
            Return m_DEALER
        End Get
        Set(ByVal Value As String)
            m_DEALER = Value
        End Set
    End Property
    Public Property QUESTION1() As String
        Get
            Return m_QUESTION1
        End Get
        Set(ByVal Value As String)
            m_QUESTION1 = Value
        End Set
    End Property
    Public Property ANSWER1() As String
        Get
            Return m_ANSWER1
        End Get
        Set(ByVal Value As String)
            m_ANSWER1 = Value
        End Set
    End Property
    Public Property QUESTION2() As String
        Get
            Return m_QUESTION2
        End Get
        Set(ByVal Value As String)
            m_QUESTION2 = Value
        End Set
    End Property
    Public Property ANSWER2() As String
        Get
            Return m_ANSWER2
        End Get
        Set(ByVal Value As String)
            m_ANSWER2 = Value
        End Set
    End Property
    Public Property QUESTION3() As String
        Get
            Return m_QUESTION3
        End Get
        Set(ByVal Value As String)
            m_QUESTION3 = Value
        End Set
    End Property
    Public Property ANSWER3() As String
        Get
            Return m_ANSWER3
        End Get
        Set(ByVal Value As String)
            m_ANSWER3 = Value
        End Set
    End Property
    Public Property QUESTION4() As String
        Get
            Return m_QUESTION4
        End Get
        Set(ByVal Value As String)
            m_QUESTION4 = Value
        End Set
    End Property
    Public Property ANSWER4() As String
        Get
            Return m_ANSWER4
        End Get
        Set(ByVal Value As String)
            m_ANSWER4 = Value
        End Set
    End Property
    Public Property QUESTION5() As String
        Get
            Return m_QUESTION5
        End Get
        Set(ByVal Value As String)
            m_QUESTION5 = Value
        End Set
    End Property
    Public Property ANSWER5() As String
        Get
            Return m_ANSWER5
        End Get
        Set(ByVal Value As String)
            m_ANSWER5 = Value
        End Set
    End Property
    Public Property QUESTION6() As String
        Get
            Return m_QUESTION6
        End Get
        Set(ByVal Value As String)
            m_QUESTION6 = Value
        End Set
    End Property
    Public Property ANSWER6() As String
        Get
            Return m_ANSWER6
        End Get
        Set(ByVal Value As String)
            m_ANSWER6 = Value
        End Set
    End Property
    Public Property QUESTION7() As String
        Get
            Return m_QUESTION7
        End Get
        Set(ByVal Value As String)
            m_QUESTION7 = Value
        End Set
    End Property
    Public Property ANSWER7() As String
        Get
            Return m_ANSWER7
        End Get
        Set(ByVal Value As String)
            m_ANSWER7 = Value
        End Set
    End Property
    Public Property QUESTION8() As String
        Get
            Return m_QUESTION8
        End Get
        Set(ByVal Value As String)
            m_QUESTION8 = Value
        End Set
    End Property
    Public Property ANSWER8() As String
        Get
            Return m_ANSWER8
        End Get
        Set(ByVal Value As String)
            m_ANSWER8 = Value
        End Set
    End Property
    Public Property QUESTION9() As String
        Get
            Return m_QUESTION9
        End Get
        Set(ByVal Value As String)
            m_QUESTION9 = Value
        End Set
    End Property
    Public Property ANSWER9() As String
        Get
            Return m_ANSWER9
        End Get
        Set(ByVal Value As String)
            m_ANSWER9 = Value
        End Set
    End Property
    Public Property QUESTION10() As String
        Get
            Return m_QUESTION10
        End Get
        Set(ByVal Value As String)
            m_QUESTION10 = Value
        End Set
    End Property
    Public Property ANSWER10() As String
        Get
            Return m_ANSWER10
        End Get
        Set(ByVal Value As String)
            m_ANSWER10 = Value
        End Set
    End Property
    Public Property CONTACTEVENTLEVEL() As String
        Get
            Return m_CONTACTEVENTLEVEL
        End Get
        Set(ByVal Value As String)
            m_CONTACTEVENTLEVEL = Value
        End Set
    End Property
    'Added by prasad for promotional code CR
    Public Property SUCCESS_MESSAGE() As String
        Get
            Return m_SUCCESS_MESSAGE
        End Get
        Set(ByVal Value As String)
            m_SUCCESS_MESSAGE = Value
        End Set
    End Property

#End Region


End Class
