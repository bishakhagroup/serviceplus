Public Class InvoiceException
    Inherits CoreObject

    Private m_exp_order_number As String
    Private m_exp_invoice_number As String
    Private m_processedflag As String
    Private m_creditorder_flag As String

    Private m_tax As Double
    Private m_shipping As Double
    Private m_parts_subtotal As Double
    Private m_restock_amount As Double
    Private m_recyclefee_amount As Double
    Private m_invoice_total As Double

    Private m_cc_authorization_code As String
    Private m_tracking_number As String
    Private m_original_invoice_number As String
    Private m_carrier_code As String
    Private m_update_date As Date

    Public Sub New()
    End Sub

    Public Property OrderNumber() As String
        Get
            Return m_exp_order_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_exp_order_number, Value)
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_exp_invoice_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_exp_invoice_number, Value)
        End Set
    End Property

    Public Property Processedflag() As String
        Get
            Return m_processedflag
        End Get
        Set(ByVal Value As String)
            SetProperty(m_processedflag, Value)
        End Set
    End Property

    Public Property CreditOrderFlag() As String
        Get
            Return m_creditorder_flag
        End Get
        Set(ByVal Value As String)
            SetProperty(m_creditorder_flag, Value)
        End Set
    End Property

    Public Property Tax() As Double
        Get
            Return m_tax
        End Get
        Set(ByVal Value As Double)
            m_tax = Value
        End Set
    End Property

    Public Property ShippingAmount() As Double
        Get
            Return m_shipping
        End Get
        Set(ByVal Value As Double)
            m_shipping = Value
        End Set
    End Property

    Public Property PartsSubTotal() As Double
        Get
            Return m_parts_subtotal
        End Get
        Set(ByVal Value As Double)
            m_parts_subtotal = Value
        End Set
    End Property

    Public Property RestockAmount() As Double
        Get
            Return m_restock_amount
        End Get
        Set(ByVal Value As Double)
            m_restock_amount = Value
        End Set
    End Property

    Public Property RecycleFeeAmount() As Double
        Get
            Return m_recyclefee_amount
        End Get
        Set(ByVal Value As Double)
            m_recyclefee_amount = Value
        End Set
    End Property

    Public Property InvoiceTotal() As Double
        Get
            Return m_invoice_total
        End Get
        Set(ByVal Value As Double)
            m_invoice_total = Value
        End Set
    End Property


    Public Property CCAuthorizationCode() As String
        Get
            Return m_cc_authorization_code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_cc_authorization_code, Value)
        End Set
    End Property
    Public Property TrackingNumber() As String
        Get
            Return m_tracking_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_tracking_number, Value)
        End Set
    End Property
    Public Property OriginalInvoiceNumber() As String
        Get
            Return m_original_invoice_number
        End Get
        Set(ByVal Value As String)
            SetProperty(m_original_invoice_number, Value)
        End Set
    End Property

    Public Property CarrierCode() As String
        Get
            Return m_carrier_code
        End Get
        Set(ByVal Value As String)
            SetProperty(m_carrier_code, Value)
        End Set
    End Property
    Public Property UpdateDate() As Date
        Get
            Return m_update_date
        End Get
        Set(ByVal Value As Date)
            SetProperty(m_update_date, Value)
        End Set
    End Property

End Class
