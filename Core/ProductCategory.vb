Public Class ProductCategory
    Private m_category_code As String
    Private m_description As String

    Public Sub New(ByVal category_code As String)
        m_category_code = category_code
    End Sub

    Public Property CategoryCode() As String
        Get
            Return m_category_code
        End Get
        Set(ByVal Value As String)
            m_category_code = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

End Class
