
Public Class ServiceItemInfo
    Inherits CoreObject

    Private m_serialno As String = String.Empty
    Private m_accessories As String = String.Empty
    Private m_serviceContractNo As String = String.Empty
    Private m_serviceContract As Boolean
    Private m_warrantyCode As Char 'p-prodyc warranty, s-service warranty o-out of warranty
    Private m_billtoaddr As CustomerAddr
    Private m_shiptoaddr As CustomerAddr
    Private m_ponumber As String = String.Empty
    Private m_approval As Boolean
    Private m_preapprovalamt As Double
    Private m_taxexempt As Boolean
    Private m_paymenttype As String = String.Empty

    Private m_isIntermittentProb As Boolean
    Private m_isPreventiveMint As Boolean
    Private m_probDescr As String = String.Empty
    Private m_depot_center_addr As String = String.Empty
    Private m_depot_center_addr_id As Integer
    'Start Modification after Issue E793 reopen
    Private m_contact_emailaddress As String = String.Empty
    Private m_depot_emailaddress As String = String.Empty
    'End Modification after Issue E793 reopen
    Private m_state_abbr As String = String.Empty

    Private sPayerAccount As String = String.Empty
    Private sShipToAccount As String = String.Empty
    Private i_CustomerID As Long
    Private m_Modelno As String = String.Empty
    Private b_IsAccountHolder As Boolean = False
    Private b_IsProduct_Ship_onHold As Boolean = False
    Private int_ServicesRequestID As Long
    Private m_ContactPersonName As String = String.Empty

    Public Property ContactPersonName() As String
        Get
            Return m_ContactPersonName
        End Get
        Set(ByVal value As String)
            m_ContactPersonName = value
        End Set
    End Property
    Public Property DepotEmail() As String
        Get
            Return m_depot_emailaddress
        End Get
        Set(ByVal value As String)
            m_depot_emailaddress = value
        End Set
    End Property
    Public Property ServicesRequestID() As Long
        Get
            Return int_ServicesRequestID
        End Get
        Set(ByVal value As Long)
            int_ServicesRequestID = value
        End Set
    End Property

    Public Property PayerAccount() As String
        Get
            Return sPayerAccount
        End Get
        Set(ByVal value As String)
            sPayerAccount = value
        End Set
    End Property

    Public Property CustomerID() As Long
        Get
            Return i_CustomerID
        End Get
        Set(ByVal value As Long)
            i_CustomerID = value
        End Set
    End Property

    Public Property IsAccountHolder() As Boolean
        Get
            Return b_IsAccountHolder
        End Get
        Set(ByVal value As Boolean)
            b_IsAccountHolder = value
        End Set
    End Property

    Public Property ShipToAccount() As String
        Get
            Return sShipToAccount
        End Get
        Set(ByVal value As String)
            sShipToAccount = value
        End Set
    End Property

    Public Property ShipStateAbbr() As String
        Get
            Return m_state_abbr
        End Get
        Set(ByVal value As String)
            m_state_abbr = value
        End Set
    End Property

    Public Property DepotCenterAddress() As String
        Get
            Return m_depot_center_addr
        End Get
        Set(ByVal value As String)
            m_depot_center_addr = value
        End Set
    End Property

    Public Property DepotCenterAddressID() As Integer
        Get
            Return m_depot_center_addr_id
        End Get
        Set(ByVal value As Integer)
            m_depot_center_addr_id = value
        End Set
    End Property

    Public Property isIntermittentProblem() As Boolean
        Get
            Return m_isIntermittentProb
        End Get
        Set(ByVal value As Boolean)
            m_isIntermittentProb = value
        End Set
    End Property

    Public Property isPreventiveMaintenance() As Boolean
        Get
            Return m_isPreventiveMint
        End Get
        Set(ByVal value As Boolean)
            m_isPreventiveMint = value
        End Set
    End Property

    Public Property IsProductShipOnHold() As Boolean
        Get
            Return b_IsProduct_Ship_onHold
        End Get
        Set(ByVal value As Boolean)
            b_IsProduct_Ship_onHold = value
        End Set
    End Property

    Public Property ProblemDescription() As String
        Get
            Return m_probDescr
        End Get
        Set(ByVal value As String)
            m_probDescr = value
        End Set
    End Property

    Public Property PaymentType() As String
        Get
            Return m_paymenttype
        End Get
        Set(ByVal value As String)
            m_paymenttype = value
        End Set
    End Property

    Public Property isTaxExempt() As Boolean
        Get
            Return m_taxexempt
        End Get
        Set(ByVal value As Boolean)
            m_taxexempt = value
        End Set
    End Property

    Public Property PreApproveAmount() As Double
        Get
            Return m_preapprovalamt
        End Get
        Set(ByVal value As Double)
            m_preapprovalamt = value
        End Set
    End Property

    Public Property isApproval() As Boolean
        Get
            Return m_approval
        End Get
        Set(ByVal value As Boolean)
            m_approval = value
        End Set
    End Property

    Public Property PONumber() As String
        Get
            Return m_ponumber
        End Get
        Set(ByVal value As String)
            m_ponumber = value
        End Set
    End Property

    Public Property ShipToAddr() As CustomerAddr
        Get
            Return m_shiptoaddr
        End Get
        Set(ByVal value As CustomerAddr)
            m_shiptoaddr = value
        End Set
    End Property

    Public Property BillToAddr() As CustomerAddr
        Get
            Return m_billtoaddr
        End Get
        Set(ByVal value As CustomerAddr)
            m_billtoaddr = value
        End Set
    End Property

    Public Property ModelNumber() As String
        Get
            Return m_Modelno
        End Get
        Set(ByVal value As String)
            m_Modelno = value
        End Set
    End Property

    Public Property SerialNumber() As String
        Get
            Return m_serialno
        End Get
        Set(ByVal value As String)
            m_serialno = value
        End Set
    End Property

    Public Property Accessories() As String
        Get
            Return m_accessories
        End Get
        Set(ByVal value As String)
            m_accessories = value
        End Set
    End Property

    Public Property ContractNumber() As String
        Get
            Return m_serviceContractNo
        End Get
        Set(ByVal value As String)
            m_serviceContractNo = value
        End Set
    End Property

    Public Property WarrantyType() As Char
        Get
            Return m_warrantyCode
        End Get
        Set(ByVal value As Char)
            m_warrantyCode = value
        End Set
    End Property

    'Start Modification after Issue E793 reopen
    Public Property ContactEmailAddress() As String
        Get
            Return m_contact_emailaddress
        End Get
        Set(ByVal value As String)
            m_contact_emailaddress = value
        End Set
    End Property
    'End Modification after Issue E793 reopen

End Class
