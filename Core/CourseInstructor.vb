Public Class CourseInstructor
    Inherits CoreObject

    Private m_Code As Integer
    Private m_FirstName As String
    Private m_LastName As String
    Private m_Address1 As String
    Private m_Address2 As String
    Private m_Address3 As String
    Private m_City As String
    Private m_State As String
    Private m_Zip As String
    Private m_ISOCountryCode As String
    Private m_EMail As String
    Private m_Phone As String
    Private m_Extension As String
    Private m_Fax As String
    Private m_LaborRate As String
    Private m_Notes As String
    Private m_UpdateDate As Date
    Private m_SequenceNumber As Integer
    Private m_StatusCode As Integer

    Public Sub New(Optional ByVal instructorCode As String = "")
        If Not String.IsNullOrWhiteSpace(instructorCode) Then m_Code = instructorCode
    End Sub

    Public Property Code() As Integer
        Get
            Return m_Code
        End Get
        Set(ByVal Value As Integer)
            m_Code = Value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return m_FirstName
        End Get
        Set(ByVal Value As String)
            m_FirstName = Value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_LastName
        End Get
        Set(ByVal Value As String)
            m_LastName = Value
        End Set
    End Property
    Public ReadOnly Property Name() As String
        Get
            Return m_FirstName + " " + m_LastName
        End Get
    End Property

    Public Property Address1() As String
        Get
            Return m_Address1
        End Get
        Set(ByVal Value As String)
            m_Address1 = Value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return m_Address2
        End Get
        Set(ByVal Value As String)
            m_Address2 = Value
        End Set
    End Property
    Public Property Address3() As String
        Get
            Return m_Address3
        End Get
        Set(ByVal Value As String)
            m_Address3 = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(ByVal Value As String)
            m_City = Value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_State
        End Get
        Set(ByVal Value As String)
            m_State = Value
        End Set
    End Property

    Public Property Zip() As String
        Get
            Return m_Zip
        End Get
        Set(ByVal Value As String)
            m_Zip = Value
        End Set
    End Property

    Public Property ISOCountryCode() As String
        Get
            Return m_ISOCountryCode
        End Get
        Set(ByVal Value As String)
            m_ISOCountryCode = Value
        End Set
    End Property

    Public Property EMail() As String
        Get
            Return m_EMail
        End Get
        Set(ByVal Value As String)
            m_EMail = Value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return m_Phone
        End Get
        Set(ByVal Value As String)
            m_Phone = Value
        End Set
    End Property

    Public Property Extension() As String
        Get
            Return m_Extension
        End Get
        Set(ByVal Value As String)
            m_Extension = Value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return m_Fax
        End Get
        Set(ByVal Value As String)
            m_Fax = Value
        End Set
    End Property

    Public Property LaborRate() As String
        Get
            Return m_LaborRate
        End Get
        Set(ByVal Value As String)
            m_LaborRate = Value
        End Set
    End Property

    Public Property Notes() As String
        Get
            Return m_Notes
        End Get
        Set(ByVal Value As String)
            m_Notes = Value
        End Set
    End Property

    Public Property UpdateDate() As Date
        Get
            Return m_UpdateDate
        End Get
        Set(ByVal Value As Date)
            m_UpdateDate = Value
        End Set
    End Property

    Public Property SequenceNumber() As Integer
        Get
            Return m_SequenceNumber
        End Get
        Set(ByVal Value As Integer)
            m_SequenceNumber = Value
        End Set
    End Property

    Public Property StatusCode() As Integer
        Get
            Return m_StatusCode
        End Get
        Set(ByVal Value As Integer)
            m_StatusCode = Value
        End Set
    End Property

End Class
