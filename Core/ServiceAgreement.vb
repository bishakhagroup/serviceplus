﻿Public Class ServiceAgreement
    Inherits CoreObject

    Public Property ID As Integer
    Public Property AgreementNumber As String
    Public Property FirstName As String
    Public Property LastName As String
    Public Property Email As String
    Public Property Company As String
    Public Property Address1 As String
    Public Property Address2 As String
    Public Property Address3 As String
    Public Property City As String
    Public Property State As String
    Public Property ZipCode As String
    Public Property Phone As String
    Public Property Extension As String
    Public Property Fax As String

    Public Property ResellerCompany As String
    Public Property ResellerName As String
    Public Property ResellerPhone As String
    Public Property ResellerExtension As String
    Public Property EquipmentList As List(Of ServiceAgreementItem)

    Public Function HasReseller() As Boolean
        Dim hasValue As Boolean = False
        If Not String.IsNullOrWhiteSpace(ResellerCompany) Then hasValue = True
        If Not String.IsNullOrWhiteSpace(ResellerName) Then hasValue = True
        If Not String.IsNullOrWhiteSpace(ResellerPhone) Then hasValue = True
        Return hasValue
    End Function
End Class

Public Class ServiceAgreementItem
    Public Property Model As String

    Public Property Serial As String

    Private _datePurchased As Date
    Public Property DatePurchased As Date
        Get
            Return _datePurchased.Date
        End Get
        Set(value As Date)
            _datePurchased = value
        End Set
    End Property

    Public Sub New(_model As String, _serial As String, _purchased As Date)
        Model = _model
        Serial = _serial
        DatePurchased = _purchased
    End Sub
End Class
