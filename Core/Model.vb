Public Class Model

    Private m_model_id As String
    Private m_description As String
    Private m_country_id As String
    Private m_destination As String
    Private m_serial_range As String
    Private m_start_serial_number As String
    Private m_end_serial_number As String
    Private m_manual_id As String
    Private m_add_in As String
    Private m_model_group As String
    Private m_categories As ArrayList
    Private m_sub_models As ArrayList

    Public Sub New()
        m_categories = New ArrayList
        m_sub_models = New ArrayList
    End Sub

    Public Property ModelID() As String
        Get
            Return m_model_id
        End Get
        Set(ByVal Value As String)
            m_model_id = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_description
        End Get
        Set(ByVal Value As String)
            m_description = Value
        End Set
    End Property

    Public Property Categories() As Category()
        Get
            Dim cats(m_categories.Count - 1) As Category
            m_categories.CopyTo(cats)
            Return cats
        End Get
        Set(ByVal Value As Category())

        End Set
    End Property

    Public Property SubModels() As Model()
        Get
            Dim subs(m_sub_models.Count - 1) As Model
            m_sub_models.CopyTo(subs)
            Return subs
        End Get
        Set(ByVal Value As Model())

        End Set
    End Property

    Public Sub AddSubModel(ByVal model As Model)
        m_sub_models.Add(model)
    End Sub

    Public Sub AddCategory(ByVal category As Category)
        m_categories.Add(category)
    End Sub

    Public Property CountryID() As String
        Get
            Return m_country_id
        End Get
        Set(ByVal Value As String)
            m_country_id = Value
        End Set
    End Property

    Public Property Destination() As String
        Get
            Return m_destination
        End Get
        Set(ByVal Value As String)
            m_destination = Value
        End Set
    End Property

    Public Property SerialRange() As String
        Get
            Return m_serial_range
        End Get
        Set(ByVal Value As String)
            m_serial_range = Value
        End Set
    End Property

    Public Property StartSerialNumber() As String
        Get
            Return m_start_serial_number
        End Get
        Set(ByVal Value As String)
            m_start_serial_number = Value
        End Set
    End Property

    Public Property EndSerialNumber() As String
        Get
            Return m_end_serial_number
        End Get
        Set(ByVal Value As String)
            m_end_serial_number = Value
        End Set
    End Property

    Public Property ManualID() As String
        Get
            Return m_manual_id
        End Get
        Set(ByVal Value As String)
            m_manual_id = Value
        End Set
    End Property

    Public Property AddIn() As String
        Get
            Return m_add_in
        End Get
        Set(ByVal Value As String)
            m_add_in = Value
        End Set
    End Property

    Public Property ModelGroup() As String
        Get
            Return m_model_group
        End Get
        Set(ByVal Value As String)
            m_model_group = Value
        End Set
    End Property

End Class
