Imports System.Collections

Public Class Kit
    Inherits Product

    Public Enum KitType
        Overhaul
        Major
        Minor
    End Enum

    Private m_model As String
    Private m_items As ArrayList
    Private m_kit_type As KitType
    Private m_discount_code_description As String
    Private m_discount_code As String
    Private m_discount_percentage As Double
    Private m_part_count As Integer
    Private m_your_price As Double

    Public Sub New()
        m_type = Product.ProductType.Kit
        m_items = New ArrayList
    End Sub

    Public Property Items() As KitItem()
        Get
            Dim parts_list(m_items.Count - 1) As KitItem
            m_items.CopyTo(parts_list)
            Return parts_list
        End Get
        Set(ByVal Value As KitItem())
        End Set
    End Property

    Public Sub AddKitItem(ByVal item As KitItem)
        m_items.Add(item)
    End Sub

    Public Property TheKitType() As KitType
        Get
            Return m_kit_type
        End Get
        Set(ByVal Value As KitType)
            m_kit_type = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_model
        End Get
        Set(ByVal Value As String)
            m_model = Value
        End Set
    End Property

    Private Property YourPrice() As Double
        Get
            Return m_your_price
        End Get
        Set(ByVal Value As Double)
            m_your_price = Value
        End Set
    End Property

    Public Property PartCount() As Integer
        Get
            Return m_part_count
        End Get
        Set(ByVal Value As Integer)
            m_part_count = Value
        End Set
    End Property

    Public Property DiscountPercentage() As Double
        Get
            Return m_discount_percentage
        End Get
        Set(ByVal Value As Double)
            m_discount_percentage = Value
        End Set
    End Property

    Public Property DiscountCode() As String
        Get
            Return m_discount_code
        End Get
        Set(ByVal Value As String)
            m_discount_code = Value
        End Set
    End Property
    Public Property DiscountCodeDescription() As String
        Get
            Return m_discount_code_description
        End Get
        Set(ByVal Value As String)
            m_discount_code_description = Value
        End Set
    End Property

End Class
