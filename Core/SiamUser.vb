Imports System.Collections
'Imports Sony.US.ServicesPLUS.Data

Public Class SiamUser
    Inherits CoreObject

    Private m_user_id As String
    Private m_user_status As New UserType
    Private m_user_type As String
    Private m_isdeleted As Integer
    Private m_address_line2 As String
    Private m_phone As String
    Private m_fax As String
    Private m_address_line1 As String
    Private m_first_name As String
    Private m_zip As String
    Private m_company_name As String
    Private m_last_name As String
    Private m_email_address As String
    Private m_state As String
    Private m_city As String
    Private m_user_name As String
    Private m_alternate_user_name As String
    Private m_password As String
    Private m_password_expiration_date As String
    Private m_lock_status As String
    Private m_role_name As String


    Public Sub New()
    End Sub

    Public Property UserID() As String
        Get
            Return m_user_id
        End Get
        Set(ByVal Value As String)
            m_user_id = Value
        End Set
    End Property

    Public Property Status() As UserType
        Get
            Return m_user_status
        End Get
        Set(ByVal Value As UserType)
            m_user_status = Value
        End Set
    End Property

    Public Property UserType() As String
        Get
            Return m_user_type
        End Get
        Set(ByVal Value As String)
            m_user_type = Value
        End Set
    End Property

    Public Property IsDeleted() As Integer
        Get
            Return m_isdeleted
        End Get
        Set(ByVal Value As Integer)
            m_isdeleted = Value
        End Set
    End Property
    Public Property AddressLine2() As String
        Get
            Return m_address_line2
        End Get
        Set(ByVal Value As String)
            m_address_line2 = Value
        End Set
    End Property

    Public Property Phone() As String
        Get
            Return m_phone
        End Get
        Set(ByVal Value As String)
            m_phone = Value
        End Set
    End Property

    Public Property Fax() As String
        Get
            Return m_fax
        End Get
        Set(ByVal Value As String)
            m_fax = Value
        End Set
    End Property

    Public Property AddressLine1() As String
        Get
            Return m_address_line1
        End Get
        Set(ByVal Value As String)
            m_address_line1 = Value
        End Set
    End Property

    Public Property FirstName() As String
        Get
            Return m_first_name
        End Get
        Set(ByVal Value As String)
            m_first_name = Value
        End Set
    End Property

    Public Property Zip() As String
        Get
            Return m_zip
        End Get
        Set(ByVal Value As String)
            m_zip = Value
        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return m_company_name
        End Get
        Set(ByVal Value As String)
            m_company_name = Value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_last_name
        End Get
        Set(ByVal Value As String)
            m_last_name = Value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return m_email_address
        End Get
        Set(ByVal Value As String)
            m_email_address = Value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_state
        End Get
        Set(ByVal Value As String)
            m_state = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return m_city
        End Get
        Set(ByVal Value As String)
            m_city = Value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return m_user_name
        End Get
        Set(ByVal Value As String)
            m_user_name = Value
        End Set
    End Property

    'Public Property AlternateUserName() As String
    '    Get
    '        Return m_alternate_user_name
    '    End Get
    '    Set(ByVal Value As String)
    '        m_alternate_user_name = Value
    '    End Set
    'End Property

    Public Property Password() As String
        Get
            Return m_password
        End Get
        Set(ByVal Value As String)
            m_password = Value
        End Set
    End Property

    Public Property PasswordExpirationDate() As String
        Get
            Return m_password_expiration_date
        End Get
        Set(ByVal Value As String)
            m_password_expiration_date = Value
        End Set
    End Property

    Public Property LockStatus() As String
        Get
            Return m_lock_status
        End Get
        Set(ByVal Value As String)
            m_lock_status = Value
        End Set
    End Property

    Public Property RoleName() As String
        Get
            Return m_role_name
        End Get
        Set(ByVal Value As String)
            m_role_name = Value
        End Set
    End Property

End Class

