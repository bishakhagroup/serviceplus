﻿Public Class SAPKitInquiryMapping

    Private errorNumberField As Long

    Private errorMessageField As String

    Private KitsField() As SAPProxyKit

End Class
Public Class SAPProxyKit

    '   <xsd:element name="ItemNumber" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="StatusFlag" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="StatusMessage" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="ItemQuantity" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="Description" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="Availability" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="ItemListPrice" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="ItemYourPrice" nillable="true" type="xsd:string" minOccurs="0" /> 
    '<xsd:element name="Category" nillable="true" type="xsd:string" minOccurs="0" /> 


    Private ItemnumberField As String

    Private StatusFlagField As String

    Private StatusMessageField As String

    Private ItemQuantityField As Double

    Private DescriptionField As String

    Private AvailabilityField As Long

    Private ItemListPriceField As Double

    Private ItemYourPriceField As Double

    Private CategoryField As String


    '''<remarks/>
    Public Property Number() As String
        Get
            Return ItemnumberField
        End Get
        Set(ByVal value As String)
            ItemnumberField = value
        End Set
    End Property
    '''<remarks/>
    Public Property StatusFlag() As String
        Get
            Return StatusFlagField
        End Get
        Set(ByVal value As String)
            StatusFlagField = value
        End Set
    End Property

    '''<remarks/>
    Public Property StatusMessage() As String
        Get
            Return StatusMessageField
        End Get
        Set(ByVal value As String)
            StatusMessageField = value
        End Set
    End Property
    '''<remarks/>
    Public Property Quantity() As Double
        Get
            Return ItemQuantityField
        End Get
        Set(ByVal value As Double)
            ItemQuantityField = value
        End Set
    End Property

    '''<remarks/>
    Public Property Description() As String
        Get
            Return descriptionField
        End Get
        Set(ByVal value As String)
            descriptionField = value
        End Set
    End Property
    '''<remarks/>
    Public Property Availability() As Long
        Get
            Return AvailabilityField
        End Get
        Set(ByVal value As Long)
            AvailabilityField = value
        End Set
    End Property

    '''<remarks/>
    Public Property ListPrice() As Double
        Get
            Return ItemListPriceField
        End Get
        Set(ByVal value As Double)
            ItemListPriceField = value
        End Set
    End Property

    '''<remarks/>
    Public Property YourPrice() As Double
        Get
            Return ItemYourPriceField
        End Get
        Set(ByVal value As Double)
            ItemYourPriceField = value
        End Set
    End Property

    '''<remarks/>
    Public Property Category() As String
        Get
            Return CategoryField
        End Get
        Set(ByVal value As String)
            CategoryField = value
        End Set
    End Property

End Class


