Public Class Advertisement
    Private m_ImageFile As String
    Private m_URL As String
    Private m_altText As String
    Private m_popup As Boolean
    Private m_Type As Short

    Public Sub New(ByRef imageFile As String, ByRef url As String, ByRef altText As String, ByVal popup As Boolean, ByVal Type As Short)
        m_ImageFile = imageFile
        m_URL = url
        m_altText = altText
        m_popup = popup
        m_Type = Type
    End Sub

    Public ReadOnly Property ImageFile() As String
        Get
            Return m_ImageFile
        End Get
    End Property
    Public ReadOnly Property AltText() As String
        Get
            Return m_altText
        End Get
    End Property
    Public ReadOnly Property Popup() As Boolean
        Get
            Return m_popup
        End Get
    End Property

    Public ReadOnly Property URL() As String
        Get
            Return m_URL
        End Get
    End Property
    Public ReadOnly Property Type() As Short
        Get
            Return m_Type
        End Get
    End Property
End Class
