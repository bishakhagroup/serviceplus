Public Class TrainingCourseModel
    Inherits CoreObject

    Private m_CourseModels As ArrayList

    Public Sub New()
        m_CourseModels = New ArrayList
    End Sub

    Public ReadOnly Property CourseModels() As CourseModel()
        Get
            Dim lm_CourseModels(m_CourseModels.Count - 1) As CourseModel
            m_CourseModels.CopyTo(lm_CourseModels)
            Return lm_CourseModels
        End Get
    End Property


#Region "Method"
    Public Function AddCourseModel(ByVal regCourseModels As CourseModel) As Boolean
        If m_CourseModels.Count = 1 Then
            If regCourseModels.IsDefault = True Then
                Return False
            Else
                If CType(m_CourseModels(0), CourseModel).IsDefault = True Then
                    m_CourseModels.Remove(m_CourseModels(0))
                End If
            End If
        End If
        regCourseModels.ItemID = m_CourseModels.Count + 1
        m_CourseModels.Add(regCourseModels)
        Return True
    End Function

    Public Function AddCourseModel(ByVal ModelNum As String, ByVal CourseNumber As String, ByVal IsDefault As Boolean) As Boolean
        If m_CourseModels.Count = 1 Then
            If IsDefault = True Then
                Return False
            Else
                If CType(m_CourseModels(0), CourseModel).IsDefault = True Then
                    m_CourseModels.Remove(m_CourseModels(0))
                End If
            End If
        End If
        m_CourseModels.Add(New CourseModel(ModelNum, CourseNumber, m_CourseModels.Count + 1))
        Return True
    End Function

    Public Sub RemoveModel(ByVal ModelToRemove As CourseModel)
        m_CourseModels.Remove(ModelToRemove)
    End Sub
#End Region

End Class
Public Class CourseModel
    Inherits CoreObject

    Private m_Model As String
    Private m_CourseNumber As String
    Private m_ModelId As Integer
    Private m_itemNumber As Short
    Private m_isdefault As Boolean = False
    Public Sub New()
    End Sub

    Public Sub New(ByVal ModelNum As String, ByVal CourseNumber As String, ByVal ItemNumber As Integer)
        m_Model = ModelNum
        m_CourseNumber = CourseNumber
        m_itemNumber = ItemNumber
        m_ModelId = ItemNumber
        If (ModelNum = String.Empty Or CourseNumber = "") Then
            m_isdefault = True
        End If
    End Sub

    Public ReadOnly Property IsDefault() As Boolean
        Get
            Return m_isdefault
        End Get
    End Property

    Public Property ItemID() As Integer
        Get
            Return m_itemNumber
        End Get
        Set(ByVal Value As Integer)
            m_itemNumber = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_Model
        End Get
        Set(ByVal Value As String)
            m_Model = Value
        End Set
    End Property

    Public Property CourseNumber() As String
        Get
            Return m_CourseNumber
        End Get
        Set(ByVal Value As String)
            m_CourseNumber = Value
        End Set
    End Property
    Public Property ModelID() As Integer
        Get
            Return m_ModelId
        End Get
        Set(ByVal Value As Integer)
            m_ModelId = Value
        End Set
    End Property

End Class
