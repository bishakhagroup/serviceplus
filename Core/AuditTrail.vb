Public Class AuditTrail
    Inherits CoreObject

    Private m_UserID As String
    Private m_EventOrgin As String
    Private m_EventType As String
    Private m_Identity As String
    Private m_Status As String
    Private m_UpdateDate As String

    Public Sub New()
    End Sub

    Public Property UserId() As String
        Get
            Return m_UserID
        End Get
        Set(ByVal value As String)
            m_UserID = value
        End Set
    End Property

    Public Property EventOrgin() As String
        Get
            Return m_EventOrgin
        End Get
        Set(ByVal value As String)
            m_EventOrgin = value
        End Set
    End Property

    Public Property EventType() As String
        Get
            Return m_EventType
        End Get
        Set(ByVal value As String)
            m_EventType = value
        End Set
    End Property

    Public Property Identity() As String
        Get
            Return m_Identity
        End Get
        Set(ByVal value As String)
            m_Identity = value
        End Set
    End Property

    Public Property StatusCode() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value
        End Set
    End Property


    Public Property UpdateDate() As String
        Get
            Return (m_UpdateDate)
        End Get
        Set(ByVal value As String)
            m_UpdateDate = value
        End Set
    End Property

End Class
