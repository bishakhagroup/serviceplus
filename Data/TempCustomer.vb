﻿
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.SIAMUtilities
Imports System.Collections.Generic

Public Class TempCustomer
    Inherits DataManager

    Public Function AddCustomer(ByRef data As Object, Optional ByVal update As Boolean = False) As Integer
        Dim temp_cust As TempCustomerValues = CType(data, TempCustomerValues)
        Dim returnval As Integer
        Dim cmd As OracleCommand = New OracleCommand
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            If update Then
                cmd.CommandText = "UPDATE_TEMP_USER"
                cmd.Parameters.Add("xFIRSTNAME", OracleDbType.Varchar2, 50)
                cmd.Parameters("xFIRSTNAME").Value = temp_cust.FirstName
                cmd.Parameters.Add("xLASTNAME", OracleDbType.Varchar2, 50)
                cmd.Parameters("xLASTNAME").Value = temp_cust.LastName
                cmd.Parameters.Add("xEMAILADDRESS", OracleDbType.Varchar2, 100)
                cmd.Parameters("xEMAILADDRESS").Value = temp_cust.EmailAddress
                cmd.Parameters.Add("xVERIFICATIONCODE", OracleDbType.Varchar2, 50)
                cmd.Parameters("xVERIFICATIONCODE").Value = temp_cust.VerificationCode
            Else
                cmd.CommandText = "INSERT_TEMP_USER"
                cmd.Parameters.Add("xFIRSTNAME", OracleDbType.Varchar2, 50)
                cmd.Parameters("xFIRSTNAME").Value = temp_cust.FirstName
                cmd.Parameters.Add("xLASTNAME", OracleDbType.Varchar2, 50)
                cmd.Parameters("xLASTNAME").Value = temp_cust.LastName
                cmd.Parameters.Add("xEMAILADDRESS", OracleDbType.Varchar2, 100)
                cmd.Parameters("xEMAILADDRESS").Value = temp_cust.EmailAddress
                cmd.Parameters.Add("xVERIFICATIONCODE", OracleDbType.Varchar2, 50)
                cmd.Parameters("xVERIFICATIONCODE").Value = temp_cust.VerificationCode
                Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
                outparam.Direction = ParameterDirection.Output
                cmd.Parameters.Add(outparam)
            End If

            txn_context = data_store.BeginTransaction
            returnval = ExecuteCMD(cmd, Context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            If txn_context IsNot Nothing Then
                data_store.RollbackTransaction(txn_context)
            End If
            Throw ex
        End Try

        Return returnval
    End Function

    Public Function GetCustomerDetails(ByRef data As Object) As DataSet
        Dim temp_cust As TempCustomerValues = CType(data, TempCustomerValues)
        Dim returnval As DataSet = New DataSet()
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            Dim cmd = New OracleCommand With {
                .CommandText = "Select * from TEMP_USER_VALIDATION where EMAILADDRESS='" + temp_cust.EmailAddress + "'",
                .CommandType = CommandType.Text,
                .Connection = dbConnection
            }

            txn_context = data_store.BeginTransaction
            Dim myAdaptor As New OracleDataAdapter(cmd)
            myAdaptor.Fill(returnval)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            If txn_context IsNot Nothing Then
                data_store.RollbackTransaction(txn_context)
            End If
            Throw ex
        End Try

        Return returnval
    End Function

    Public Function GetCustomerIsverified(ByVal emailaddress As String) As String
        Dim returnval As New DataSet()
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim cmd As New OracleCommand

        Try
            cmd.CommandText = "Select ISVERIFIED from Customer where EMAILADDRESS='" + emailaddress + "'"
            cmd.CommandType = CommandType.Text
            cmd.Connection = dbConnection
            txn_context = data_store.BeginTransaction
            Dim myAdaptor As New OracleDataAdapter(cmd)
            myAdaptor.Fill(returnval)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            If txn_context IsNot Nothing Then
                data_store.RollbackTransaction(txn_context)
            End If
            Throw ex
        End Try
        'If (returnval IsNot Nothing) AndAlso (returnval.Tables.Count > 0) Then
        '    Return returnval.Tables(0).Rows(0).Item(0).ToString()
        'Else
        '    Return Nothing
        'End If
        Return returnval?.Tables(0)?.Rows(0)?.Item(0)?.ToString()
    End Function

    Public Function UpdateCustomer(ByRef data As Object) As Integer
        Dim temp_cust As TempCustomerValues = CType(data, TempCustomerValues)
        Dim returnval As Integer
        Dim cmd As OracleCommand = New OracleCommand
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            cmd.CommandText = "UPDATE_CUS_ISVEIRFIED"
            cmd.Parameters.Add("xEMAILADDRESS", OracleDbType.Varchar2, 100)
            cmd.Parameters("xEMAILADDRESS").Value = temp_cust.EmailAddress
            cmd.Parameters.Add("xISVERIFIED", OracleDbType.Int32)
            cmd.Parameters("xISVERIFIED").Value = temp_cust.IsEmailVerified
            txn_context = data_store.BeginTransaction
            returnval = ExecuteCMD(cmd, Context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            If txn_context IsNot Nothing Then
                data_store.RollbackTransaction(txn_context)
            End If
            Throw ex
        End Try

        Return returnval
    End Function
End Class

Public Class TempCustomerValues
    Private _firstName As String
    Private _lastName As String
    Private _emailAddress As String
    Private _verificationCode As String
    Private _createdDate As Date
    Private _isEmailVerified As Integer

    Public Property FirstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return _emailAddress
        End Get
        Set(ByVal value As String)
            _emailAddress = value
        End Set
    End Property

    Public Property VerificationCode() As String
        Get
            Return _verificationCode
        End Get
        Set(ByVal value As String)
            _verificationCode = value
        End Set
    End Property

    Public Property CreatedDate() As Date
        Get
            Return _createdDate
        End Get
        Set(ByVal value As Date)
            _createdDate = value
        End Set
    End Property

    Public Property IsEmailVerified() As Integer
        Get
            Return _isEmailVerified
        End Get
        Set(ByVal value As Integer)
            _isEmailVerified = value
        End Set
    End Property
End Class
