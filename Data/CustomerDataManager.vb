Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities

Public Class CustomerDataManager
    Inherits DataManager

    Public Sub UpdateCustomerSubscription(ByRef data As Object, ByRef context As TransactionContext)
        Dim cust As Customer = CType(data, Customer)
        Dim cmd As New OracleCommand("updateCustomer")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 16)
        If Not String.IsNullOrEmpty(cust.SIAMIdentity) Then
            cmd.Parameters("xUserId").Value = cust.SIAMIdentity
        Else
            cmd.Parameters("xUserId").Value = String.Empty
        End If

        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.FirstName) Then
            cmd.Parameters("xFirstName").Value = cust.FirstName
        Else
            cmd.Parameters("xFirstName").Value = String.Empty
        End If

        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.LastName) Then
            cmd.Parameters("xLastName").Value = cust.LastName
        Else
            cmd.Parameters("xLastName").Value = String.Empty
        End If

        cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 100)
        If Not String.IsNullOrEmpty(cust.CompanyName) Then
            cmd.Parameters("xCompanyName").Value = cust.CompanyName
        Else
            cmd.Parameters("xCompanyName").Value = String.Empty
        End If

        cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.Address.Address1) Then
            cmd.Parameters("xAddress1").Value = cust.Address.Address1
        Else
            cmd.Parameters("xAddress1").Value = String.Empty
        End If

        cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.Address.Address2) Then
            cmd.Parameters("xAddress2").Value = cust.Address.Address2
        Else
            cmd.Parameters("xAddress2").Value = String.Empty
        End If

        cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 35)
        If Not String.IsNullOrEmpty(cust.Address.Address3) Then
            cmd.Parameters("xAddress3").Value = cust.Address.Address3
        Else
            cmd.Parameters("xAddress3").Value = String.Empty
        End If

        cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.Address.City) Then
            cmd.Parameters("xCity").Value = cust.Address.City
        Else
            cmd.Parameters("xCity").Value = String.Empty
        End If

        cmd.Parameters.Add("xState", OracleDbType.Char, 2)
        If Not String.IsNullOrEmpty(cust.Address.State) Then
            cmd.Parameters("xState").Value = cust.Address.State
        Else
            cmd.Parameters("xState").Value = String.Empty
        End If

        cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10)
        If Not String.IsNullOrEmpty(cust.Address.Zip) Then
            cmd.Parameters("xZip").Value = cust.Address.Zip
        Else
            cmd.Parameters("xZip").Value = String.Empty
        End If

        cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20)
        If Not String.IsNullOrEmpty(cust.FaxNumber) Then
            cmd.Parameters("xFax").Value = cust.FaxNumber
        Else
            cmd.Parameters("xFax").Value = String.Empty
        End If

        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20)
        If Not String.IsNullOrEmpty(cust.PhoneNumber) Then
            cmd.Parameters("xPhone").Value = cust.PhoneNumber
        Else
            cmd.Parameters("xPhone").Value = String.Empty
        End If

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 100)
        If Not String.IsNullOrEmpty(cust.EmailAddress) Then
            cmd.Parameters("xEmailAddress").Value = cust.EmailAddress
        Else
            cmd.Parameters("xEmailAddress").Value = String.Empty
        End If

        cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrEmpty(cust.UserName) Then
            cmd.Parameters("xUserName").Value = cust.UserName
        Else
            cmd.Parameters("xUserName").Value = String.Empty
        End If

        cmd.Parameters.Add("xUserType", OracleDbType.Char, 1)
        If Not String.IsNullOrEmpty(cust.UserType) Then
            cmd.Parameters("xUserType").Value = cust.UserType
        Else
            cmd.Parameters("xUserType").Value = String.Empty
        End If

        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32, 38)
        If Not String.IsNullOrWhiteSpace(cust.SequenceNumber) Then
            cmd.Parameters("xSequenceNumber").Value = Convert.ToInt32(cust.SequenceNumber)
        Else
            cmd.Parameters("xSequenceNumber").Value = 0

        End If

        cmd.Parameters.Add("xEmailOK", OracleDbType.Int32, 38)
        If cust.EmailOk = False Then
            cmd.Parameters("xEmailOK").Value = 0
        Else
            cmd.Parameters("xEmailOK").Value = 1
        End If

        cmd.Parameters.Add("xENABLEDOCUMENTIDNUMBER", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Enabledocumentidnumber) Then
            cmd.Parameters("xENABLEDOCUMENTIDNUMBER").Value = cust.Enabledocumentidnumber
        Else
            cmd.Parameters("xENABLEDOCUMENTIDNUMBER").Value = String.Empty
        End If

        cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 6)
        If Not String.IsNullOrEmpty(cust.PhoneExtension) Then
            cmd.Parameters("xPhoneExtension").Value = cust.PhoneExtension
        Else
            cmd.Parameters("xPhoneExtension").Value = String.Empty
        End If

        cmd.Parameters.Add("xSubscriptionID", OracleDbType.Varchar2, 10)
        If Not String.IsNullOrEmpty(cust.SubscriptionID) Then
            cmd.Parameters("xSubscriptionID").Value = cust.SubscriptionID
        Else
            cmd.Parameters("xSubscriptionID").Value = String.Empty
        End If

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32, 38)
        If Not String.IsNullOrWhiteSpace(cust.CustomerID) Then
            cmd.Parameters("xCustomerID").Value = Convert.ToInt32(cust.CustomerID.Trim)
        Else
            cmd.Parameters("xCustomerID").Value = 0
        End If

        cmd.Parameters.Add("xStatusCode", OracleDbType.Int32, 22)
        cmd.Parameters("xStatusCode").Value = cust.StatusCode

        cmd.Parameters.Add("xCountryCode", OracleDbType.Varchar2, 2)
        If String.IsNullOrEmpty(cust.CountryCode) Then
            cmd.Parameters("xCountryCode").Value = "US"
        Else
            cmd.Parameters("xCountryCode").Value = cust.CountryCode
        End If

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        ExecuteCMD(cmd, context)
    End Sub

    Public Sub UpdateCustomerSubscriptionUserType(ByRef data As Object, ByRef context As TransactionContext)
        Dim cust As Customer = CType(data, Customer)
        Dim cmd As New OracleCommand("updateCustomer")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        If Not String.IsNullOrWhiteSpace(cust.SIAMIdentity) Then
            cmd.Parameters("xUserId").Value = cust.SIAMIdentity
        Else
            cmd.Parameters("xUserId").Value = String.Empty
        End If

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32, 20)
        If Not String.IsNullOrWhiteSpace(cust.CustomerID) Then
            cmd.Parameters("xCustomerID").Value = Convert.ToInt32(cust.CustomerID)
        Else
            cmd.Parameters("xCustomerID").Value = 0
        End If

        cmd.Parameters.Add("xStatusCode", OracleDbType.Int32, 22)
        cmd.Parameters("xStatusCode").Value = cust.StatusCode '6775

        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.FirstName) Then
            cmd.Parameters("xFirstName").Value = cust.FirstName
        Else
            cmd.Parameters("xFirstName").Value = String.Empty
        End If

        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.LastName) Then
            cmd.Parameters("xLastName").Value = cust.LastName
        Else
            cmd.Parameters("xLastName").Value = String.Empty
        End If

        cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.CompanyName) Then
            cmd.Parameters("xCompanyName").Value = cust.CompanyName
        Else
            cmd.Parameters("xCompanyName").Value = String.Empty
        End If


        cmd.Parameters.Add("xAddress1", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Address?.Address1) Then
            cmd.Parameters("xAddress1").Value = cust.Address.Address1
        Else
            cmd.Parameters("xAddress1").Value = String.Empty
        End If

        cmd.Parameters.Add("xAddress2", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Address?.Address2) Then
            cmd.Parameters("xAddress2").Value = cust.Address.Address2
        Else
            cmd.Parameters("xAddress2").Value = String.Empty
        End If

        cmd.Parameters.Add("xAddress3", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Address?.Address3) Then
            cmd.Parameters("xAddress3").Value = cust.Address.Address3
        Else
            cmd.Parameters("xAddress3").Value = String.Empty
        End If

        cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Address?.City) Then
            cmd.Parameters("xCity").Value = cust.Address.City
        Else
            cmd.Parameters("xCity").Value = String.Empty
        End If

        cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Address?.State) Then
            cmd.Parameters("xState").Value = cust.Address.State
        Else
            cmd.Parameters("xState").Value = String.Empty
        End If

        cmd.Parameters.Add("xZip", OracleDbType.Varchar2, 10)
        If Not String.IsNullOrWhiteSpace(cust.Address?.Zip) Then
            cmd.Parameters("xZip").Value = cust.Address.Zip
        Else
            cmd.Parameters("xZip").Value = String.Empty
        End If

        cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20)
        If Not String.IsNullOrWhiteSpace(cust.FaxNumber) Then
            cmd.Parameters("xFax").Value = cust.FaxNumber
        Else
            cmd.Parameters("xFax").Value = String.Empty
        End If

        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20)
        If Not String.IsNullOrWhiteSpace(cust.PhoneNumber) Then
            cmd.Parameters("xPhone").Value = cust.PhoneNumber
        Else
            cmd.Parameters("xPhone").Value = String.Empty
        End If

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.EmailAddress) Then
            cmd.Parameters("xEmailAddress").Value = cust.EmailAddress
        Else
            cmd.Parameters("xEmailAddress").Value = String.Empty
        End If

        cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.UserName) Then
            cmd.Parameters("xUserName").Value = cust.UserName
        Else
            cmd.Parameters("xUserName").Value = String.Empty
        End If

        cmd.Parameters.Add("xUserType", OracleDbType.Char)
        If Not String.IsNullOrWhiteSpace(cust.UserType) Then
            cmd.Parameters("xUserType").Value = "A"
        Else
            cmd.Parameters("xUserType").Value = String.Empty
        End If

        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32, 22)
        If Not String.IsNullOrWhiteSpace(cust.SequenceNumber) Then
            cmd.Parameters("xSequenceNumber").Value = Convert.ToInt32(cust.SequenceNumber)
        Else
            cmd.Parameters("xSequenceNumber").Value = 0
        End If

        cmd.Parameters.Add("xEmailOK", OracleDbType.Int32, 22)
        cmd.Parameters("xEmailOK").Value = IIf(cust.EmailOk, 1, 0)  ' 1 = True, 0 = False

        cmd.Parameters.Add("xENABLEDOCUMENTIDNUMBER", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.Enabledocumentidnumber) Then
            cmd.Parameters("xENABLEDOCUMENTIDNUMBER").Value = cust.Enabledocumentidnumber
        Else
            cmd.Parameters("xENABLEDOCUMENTIDNUMBER").Value = String.Empty
        End If

        cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 10)
        If Not String.IsNullOrWhiteSpace(cust.PhoneExtension) Then
            cmd.Parameters("xPhoneExtension").Value = cust.PhoneExtension
        Else
            cmd.Parameters("xPhoneExtension").Value = String.Empty
        End If

        cmd.Parameters.Add("xSubscriptionID", OracleDbType.Varchar2, 50)
        If Not String.IsNullOrWhiteSpace(cust.SubscriptionID) Then
            cmd.Parameters("xSubscriptionID").Value = cust.SubscriptionID
        Else
            cmd.Parameters("xSubscriptionID").Value = String.Empty
        End If

        If Not String.IsNullOrWhiteSpace(cust.CountryCode) Then
            cmd.Parameters.Add("xCountryCode", OracleDbType.Varchar2, 2)
            cmd.Parameters("xCountryCode").Value = cust.CountryCode
        End If

        cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)

        ExecuteCMD(cmd, context)
    End Sub

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim cust As Customer = CType(data, Customer)
        Dim cmd As New OracleCommand
        Dim tempStr As String = ""
        Dim debugMsg = $"CustomerDM.Store - START at {Date.Now.ToShortTimeString()}.{Environment.NewLine}"

        Try     ' FOR DEBUG ONLY
            cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)

            If (Not update) Or (update Or (cust.Dirty Or cust.Address.Dirty)) Then
                If update Then
                    If cust.Action = CoreObject.ActionType.DELETE Then
                        cmd.CommandText = "deleteCustomer"
                        'We can find a unique active account with the SIAM Indentity and statuscode = 1
                        'While deleting from SIAM we do not have information other than SIAMIdentity
                        cmd.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 16)
                        cmd.Parameters("xSiamIdentity").Value = cust.SIAMIdentity
                    Else
                        cmd.CommandText = "updateCustomer"
                        'Delete User do not have these parameter, so have shifted in the updateCustomer block
                        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                        cmd.Parameters("xCustomerID").Value = cust.CustomerID
                        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                        cmd.Parameters("xSequenceNumber").Value = cust.SequenceNumber
                    End If
                Else
                    ' New Customer
                    cmd.CommandText = "GetNextUserId"
                    ExecuteCMD(cmd, context)
                    'If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    If Not IsDBNull(cmd.Parameters("retVal").Value) Then
                        'Dim seedValue As Double = Convert.ToDouble(cmd.Parameters("retVal").Value.ToString())
                        'cust.SIAMIdentity = GUID.CreateGUID("10", seedValue.ToString())
                        cust.SIAMIdentity = GUID.CreateGUID("10", cmd.Parameters("retVal").Value.ToString())
                        cmd = New OracleCommand("addCustomer")
                        cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)
                        ' If String.IsNullOrWhiteSpace(cust.SequenceNumber) Then cust.SequenceNumber = "1"  ' ASleight - I fixed the logic of this, but it broke more logic. I'm guessing it was never really supposed to work.

                        cmd.Parameters.Add("xUserType", OracleDbType.Char)
                        cmd.Parameters("xUserType").Value = cust.UserType

                        cmd.Parameters.Add("xUserName", OracleDbType.Varchar2, 50)
                        cmd.Parameters("xUserName").Value = cust.UserName
                    Else
                        Utilities.LogMessages($"CustomerDataManager.Store - ERROR while reading return value from 'GetNextUserId'. Error detail is below.")
                        Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
                    End If
                End If

                If cust.Action <> CoreObject.ActionType.DELETE Then
                    ' These fields are only required for non-Delete actions.
                    cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xFirstName").Value = cust.FirstName
                    cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xLastName").Value = cust.LastName
                    cmd.Parameters.Add("xEmailOK", OracleDbType.Int32)
                    cmd.Parameters("xEmailOK").Value = Convert.ToInt32(cust.EmailOk)
                    cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 100)
                    cmd.Parameters("xEmailAddress").Value = cust.EmailAddress
                    cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 20)
                    cmd.Parameters("xFax").Value = cust.FaxNumber
                    cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 20)
                    cmd.Parameters("xPhone").Value = cust.PhoneNumber
                    cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 6)
                    cmd.Parameters("xPhoneExtension").Value = cust.PhoneExtension
                    cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 100)
                    cmd.Parameters("xCompanyName").Value = cust.CompanyName
                    cmd.Parameters.Add("xAddressLine1", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddressLine1").Value = cust.Address.Line1
                    cmd.Parameters.Add("xAddressLine2", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddressLine2").Value = cust.Address.Line2
                    cmd.Parameters.Add("xAddressLine3", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xAddressLine3").Value = cust.Address.Line3
                    cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xCity").Value = cust.Address.City
                    cmd.Parameters.Add("xState", OracleDbType.Varchar2, 2)
                    cmd.Parameters("xState").Value = cust.Address.State
                    cmd.Parameters.Add("xPostalCode", OracleDbType.Varchar2, 10)
                    cmd.Parameters("xPostalCode").Value = cust.Address.PostalCode
                    cmd.Parameters.Add("xSiamIdentity", OracleDbType.Varchar2, 16)
                    cmd.Parameters("xSiamIdentity").Value = cust.SIAMIdentity
                    cmd.Parameters.Add("xSubscriptionID", OracleDbType.Varchar2, 10)
                    cmd.Parameters("xSubscriptionID").Value = cust.SubscriptionID
                    cmd.Parameters.Add("xUserLocation", OracleDbType.Int16)
                    cmd.Parameters("xUserLocation").Value = cust.UserLocation
                    cmd.Parameters.Add("xLdapId", OracleDbType.Varchar2, 50)
                    cmd.Parameters("xLdapId").Value = cust.LdapID
                    cmd.Parameters.Add("xCountryCode", OracleDbType.Varchar2, 2)
                    cmd.Parameters("xCountryCode").Value = cust.CountryCode
                End If

                ' DEBUG Section only
                debugMsg &= $"- Params: "
                For Each param As OracleParameter In cmd.Parameters
                    debugMsg &= $"({param.ParameterName} = {param.Value}), "
                Next

                ExecuteCMD(cmd, context)
                debugMsg &= $"{Environment.NewLine}- addCustomer finished.{Environment.NewLine}"

                debugMsg &= $"- retVal = {If(cmd.Parameters("retVal")?.Value, "VB null")}{Environment.NewLine}"
                If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    debugMsg &= $"- retVal isn't null.{Environment.NewLine}"
                    tempStr = cmd.Parameters("retVal").Value.ToString()
                    If update Then
                        cust.SequenceNumber = tempStr
                    Else
                        cust.CustomerID = tempStr
                        cust.SequenceNumber = tempStr
                    End If
                    debugMsg &= $"- Customer Sequence Number: {cust.SequenceNumber}.{Environment.NewLine}"
                End If
            End If

            If cust.Action <> CoreObject.ActionType.DELETE Then 'Added by chandra on  9th Aug. In case of delete nothing have been changed
                cust.Action = CoreObject.ActionType.RESTORE

                'store related and dependent objects
                cust.ReturnDeletedObjects = True

                'store sap accounts
                'If update = False Then
                debugMsg &= $"- Storing SAP accounts.{Environment.NewLine}"
                Dim adm As New AccountDataManager
                For Each act As Account In cust.SAPBillToAccounts
                    If act.Action = Account.ActionType.ADD Then
                        adm.Store(act, context)
                    Else
                        adm.Store(act, context, True)
                    End If
                Next

                'store legacy accounts (that were not obtained from SIS)
                debugMsg &= $"- Storing Legacy accounts.{Environment.NewLine}"
                For Each act As Account In cust.SISLegacyBillToAccounts
                    If act.Action = Account.ActionType.ADD Then
                        adm.Store(act, context)
                    Else
                        adm.Store(act, context, True)
                    End If
                Next
                'End If

                'store credit cards
                debugMsg &= $"- Storing Credit Cards.{Environment.NewLine}"
                If update Then
                    Dim pdm As New PaymentDataManager
                    For Each card As CreditCard In cust.CreditCards
                        If card.Action = CoreObject.ActionType.ADD Then
                            pdm.Store(card, context)
                        Else
                            pdm.Store(card, context, True)
                        End If
                    Next
                End If

                'store marketing interests
                debugMsg &= $"- Storing Marketing Interests{Environment.NewLine}"
                If update = False Then
                    Dim mdm As New MarketingInterestDataManager
                    For Each interest As MarketingInterest In cust.MarketingInterests
                        If interest.Action = CoreObject.ActionType.ADD Then
                            mdm.Store(interest, context)
                        Else
                            mdm.Store(interest, context, True)
                        End If
                    Next

                End If
            End If
            cust.ReturnDeletedObjects = False
        Catch ex As Exception   ' FOR DEBUG ONLY
            Utilities.LogDebug(debugMsg)
            Utilities.WrapExceptionforUI(ex)
        End Try
    End Sub

    Public Sub AddCustomerAccount(ByRef data As Object, ByRef context As TransactionContext)
        Dim cust As Customer = CType(data, Customer)
        Dim adm As New AccountDataManager
        For Each act As Account In cust.SAPBillToAccounts
            If act.Action = Account.ActionType.ADD Then
                adm.Store(act, context)
            Else
                adm.Store(act, context, True)
            End If
        Next

        'store legacy accounts (that were not obtained from SIS)
        For Each act As Account In cust.SISLegacyBillToAccounts
            If act.Action = Account.ActionType.ADD Then
                adm.Store(act, context)
            Else
                adm.Store(act, context, True)
            End If
        Next
    End Sub

    Public Function DeleteCustomerRecord(ByVal customerId As String) As Boolean
        Dim cmd As New OracleCommand("DELETECUSTOMER") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("xCustomerID", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCustomerID").Value = customerId
        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        cmd.ExecuteNonQuery()
        CloseConnection()

        If (cmd.Parameters("retVal")?.Value?.ToString() = "1") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub StoreBillToAccounts(ByRef cust As Customer, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim adm As New AccountDataManager
        For Each act As Account In cust.SAPBillToAccounts
            If act.Customer Is Nothing Then
                Utilities.LogDebug("CustomerDM.StoreBillToAccounts - Account has a null Customer.")
                act.Customer = cust
            End If
            If act.Action = Account.ActionType.ADD Then
                adm.Store(act, context)
            Else
                adm.Store(act, context, True)
            End If
        Next
    End Sub

    Public Sub StoreMarketingInterest(ByRef cust As Customer, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim mdm As New MarketingInterestDataManager
        For Each interest As MarketingInterest In cust.MarketingInterests
            If interest.Action = CoreObject.ActionType.ADD Then
                mdm.Store(interest, context)
            Else
                mdm.Store(interest, context, True)
            End If
        Next
    End Sub

    Public Sub StoreCreditCard(ByRef cust As Customer, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim pdm As New PaymentDataManager
        For Each card As CreditCard In cust.CreditCards
            If card.Action = CoreObject.ActionType.ADD Then
                pdm.Store(card, context)
            Else
                pdm.Store(card, context, True)
            End If
        Next
    End Sub

    Public Function GetCustomerDateReg(ByRef context As TransactionContext, ByVal identity As String) As String
        Dim cmd As New OracleCommand("GetDateRegistered")
        Dim strDateReg As String = ""
        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = identity
        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            strDateReg = cmd.Parameters("retVal").Value.ToString()
        End If

        Return strDateReg
    End Function

    Public Function GetCustomerSubscriptionID(ByRef context As TransactionContext, ByVal identity As String) As String
        Dim cmd As New OracleCommand("GetCustomerSubscriptionID")
        Dim strSubscription As String = ""
        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = identity
        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            strSubscription = cmd.Parameters("retVal").Value.ToString()
        End If

        Return strSubscription
    End Function

    ''' Returns 3 on errors or no data, or the number of entries in the Customer table. (Usually 1)
    Public Function GetValidateCustomerData(ByRef context As TransactionContext, ByVal EmailAddress As String) As Short
        Dim cmd As New OracleCommand("VALIDATECUSTOMERDATA")
        Dim resultVALIDATECUSTOMERDATA As Short = 0
        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2)
        cmd.Parameters("xEmailAddress").Value = EmailAddress
        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            resultVALIDATECUSTOMERDATA = CType(cmd.Parameters("retVal").Value.ToString(), Short)
        End If

        Return resultVALIDATECUSTOMERDATA
    End Function

    Public Function GetInactiveCustomerData(ByRef context As TransactionContext, ByVal EmailAddress As String) As Long
        Dim customerId As Long = 0
        Dim cmd As New OracleCommand("INACTIVECUSTOMERDATA")

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2)
        cmd.Parameters("xEmailAddress").Value = EmailAddress
        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            customerId = CType(cmd.Parameters("retVal").Value.ToString(), Long)
        End If

        Return customerId
    End Function

    Public Function GetCustomerPhoneExtension(ByRef context As TransactionContext, ByVal identity As String) As String
        Dim strPhoneExtension As String = ""
        Dim cmd As New OracleCommand("GetCustomerPhoneExt")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50)
        cmd.Parameters("xUserId").Value = identity

        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 100)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            strPhoneExtension = cmd.Parameters("retVal").Value.ToString()
        End If

        Return strPhoneExtension
    End Function

    Public Function GetSiamIdByLdapId(ByRef context As TransactionContext, ByVal xLDAPID As String) As String
        Dim strSiamID As String = ""
        Dim cmd As New OracleCommand("GETSIAMIDBYLDAPID")

        cmd.Parameters.Add("xLDAPID", OracleDbType.Varchar2, 10)
        cmd.Parameters("xLDAPID").Value = xLDAPID

        Dim outparam As New OracleParameter("xSIAMId", OracleDbType.Varchar2, 16)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("xSIAMId")?.Value, INullable).IsNull Then
            strSiamID = cmd.Parameters("xSIAMId").Value.ToString()
        End If

        Return strSiamID
    End Function

    Public Function IsValidSubscription(ByRef context As TransactionContext, ByVal customerID As String, ByVal subscriptionID As String) As Short
        Dim resultSubscription As Short = 0
        Dim cmd As New OracleCommand("CheckSubscriptionPreValidity")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = customerID

        cmd.Parameters.Add("xSubscriptionId", OracleDbType.Varchar2, 10)
        cmd.Parameters("xSubscriptionId").Value = subscriptionID

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            resultSubscription = CType(cmd.Parameters("retVal").Value.ToString(), Integer)
        End If

        Return resultSubscription
    End Function

    Public Function IsValidSubscription(ByRef context As TransactionContext, ByVal customerID As String, ByVal password As String, ByVal subscriptionID As String) As Boolean
        Dim boolSubscription As Boolean = False
        Dim cmd As New OracleCommand("CheckSubscription")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = customerID

        cmd.Parameters.Add("xSubscriptionId", OracleDbType.Varchar2, 10)
        cmd.Parameters("xSubscriptionId").Value = subscriptionID

        cmd.Parameters.Add("xPassword", OracleDbType.Varchar2, 10)
        cmd.Parameters("xPassword").Value = password

        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            boolSubscription = CType(cmd.Parameters("retVal").Value.ToString(), Boolean)
        End If

        Return boolSubscription
    End Function

    Public Function IsValidSubscription(ByRef context As TransactionContext, ByVal customerID As String) As Boolean
        Dim boolSubscription As Boolean = False
        Dim cmd As New OracleCommand("CheckExistingSubscription")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = customerID

        Dim outparam As New OracleParameter("retVal", OracleDbType.Varchar2, 10)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        OpenConnection()
        cmd.Connection = dbConnection

        Try
            ExecuteCMD(cmd, context)
            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                boolSubscription = CType(cmd.Parameters("retVal").Value.ToString(), Boolean)
            End If
        Catch ex As Exception
            'Throw New Exception(ex.Message())
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
        End Try

        Return boolSubscription
    End Function

    Public Function GetUserlist(ByVal userType As String) As DataSet
        Dim cmd As New OracleCommand("CUSTOMERDETAILS.GETCUSTOMERSLIST") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("xUserType", OracleDbType.Char)
        cmd.Parameters("xUserType").Value = userType

        cmd.Parameters.Add(New OracleParameter("xCustomerList", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        Dim myAdaptor As New OracleDataAdapter(cmd)
        Dim dataset As New DataSet
        Try
            myAdaptor.Fill(dataset)
            Return dataset
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try
    End Function

    Public Overrides Function Restore(ByVal siamId As String) As Object
        Dim customer As Customer = Nothing
        Dim account_manager As New AccountDataManager
        Dim payment_manager As New PaymentDataManager
        Dim marketing_manager As New MarketingInterestDataManager
        Dim accounts() As Account
        Dim cards() As CreditCard
        Dim interests() As MarketingInterest
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getCustomer")

        cmd.Parameters.Add("xUserId", OracleDbType.Char)
        cmd.Parameters("xUserId").Value = siamId

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        For Each node In nodes 'just hard code to 0 and throw exception if count > 1
            customer = New Customer With {
                .CompanyName = If(node("COMPANYNAME")?.InnerText, ""),
                .CountryCode = If(node("COUNTRYCODE")?.InnerText, ""),
                .CustomerID = If(node("CUSTOMERID")?.InnerText, ""),
                .EmailAddress = If(node("EMAILADDRESS")?.InnerText, ""),
                .EmailOk = IIf(node("EMAILOK")?.InnerText = "1", True, False),
                .Enabledocumentidnumber = If(node("ENABLEDOCUMENTIDNUMBER")?.InnerText, ""),
                .FaxNumber = If(node("FAX")?.InnerText, ""),
                .FirstName = If(node("FIRSTNAME")?.InnerText, ""),
                .IsActive = IIf(node("ISActive")?.InnerText = "1", True, False),
                .IsSubscriptionActive = IIf(node("ISSUBSCRIPTIONACTIVE")?.InnerText = "1", True, False),
                .LastLoginTime = If(node("LASTLOGINTIME")?.InnerText, ""),
                .LastName = If(node("LASTNAME")?.InnerText, ""),
                .LdapID = If(node("LDAPID")?.InnerText, ""),
                .PhoneExtension = If(node("PHONEEXTENSION")?.InnerText, ""),
                .PhoneNumber = If(node("PHONE")?.InnerText, ""),
                .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, ""),
                .SIAMIdentity = If(node("SIAMIDENTITY")?.InnerText, ""),
                .StatusCode = Convert.ToInt16(If(node("STATUSCODE")?.InnerText, "1")),
                .SubscriptionID = IIf(node("SUBSCRIPTIONID")?.InnerText = "0", " ", node("SUBSCRIPTIONID")?.InnerText),
                .UserLocation = Convert.ToInt16(If(node("USERLOCATION")?.InnerText, "1")),
                .UserName = If(node("USERNAME")?.InnerText, ""),
                .UserType = If(node("USERTYPE")?.InnerText, ""),
                .Action = CoreObject.ActionType.RESTORE,
                .Address = New Address With {
                    .Line1 = If(node("ADDRESS1")?.InnerText, ""),
                    .Line2 = If(node("ADDRESS2")?.InnerText, ""),
                    .Line3 = If(node("ADDRESS3")?.InnerText, ""),
                    .City = If(node("CITY")?.InnerText, ""),
                    .State = If(node("STATE")?.InnerText, ""),
                    .PostalCode = If(node("POSTALCODE")?.InnerText, ""),
                    .Country = If(node("COUNTRYCODE")?.InnerText, ""),
                    .Action = CoreObject.ActionType.RESTORE
                }
            }
        Next

        If (customer Is Nothing OrElse nodes Is Nothing OrElse nodes?.Count = 0) Then Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)

        ' Get SAP Buyer Accounts
        accounts = account_manager.GetCustomerAccounts(customer)
        For Each act As Account In accounts
            customer.AddAccount(act)
        Next

        ' Get Credit Cards
        cards = payment_manager.GetCustomerCreditCards(customer)
        For Each card As CreditCard In cards
            customer.AddCreditCard(card)
        Next

        ' Get Marketing Interests
        interests = marketing_manager.GetCustomerMarketingInterests(customer)
        For Each interest As MarketingInterest In interests
            customer.AddMarketingInterest(interest)
        Next

        Return customer
    End Function

    Public Function GetAccountData(ByVal userID As String) As ArrayList
        Dim ds As DataSet
        Dim ls As New ArrayList
        Dim cmd As New OracleCommand("CustomerDetails.GetAccountData")

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50)
        cmd.Parameters("xUserId").Value = userID

        cmd.Parameters.Add(New OracleParameter("xCustomerAccount", OracleDbType.RefCursor, ParameterDirection.Output))

        ds = GetResults(cmd, True)
        If ds.Tables(0)?.Rows?.Count > 0 Then
            For Each row As DataRow In ds.Tables(0).Rows
                If row("ACCOUNTNUMBER") IsNot Nothing Then ls.Add(row("ACCOUNTNUMBER").ToString())
            Next
        End If

        Return ls
    End Function

    Public Overloads Function Restore(ByVal userID As String, ByVal email As String, ByVal firstName As String, ByVal lastName As String) As Object
        Dim customer As Customer = Nothing
        Dim ds As DataSet
        Dim drow As DataRow
        Dim account As Account
        Dim credit_card As CreditCard
        Dim type As CreditCardType
        Dim marketing_interest As MarketingInterest
        Dim cmd As New OracleCommand("CustomerDetails.GetCustomerData")
        'Dim debugString As String = $"CustomerDM.Restore - BEGIN at {Date.Now.ToShortTimeString}"

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 50)
        cmd.Parameters("xUserId").Value = userID
        cmd.Parameters.Add("xEmail", OracleDbType.Varchar2, 100)
        cmd.Parameters("xEmail").Value = email
        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xFirstName").Value = firstName
        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xLastName").Value = lastName

        cmd.Parameters.Add("xCustomerInfo", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("xCustomerAccount", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("xCustomerCreditCard", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("xCustomerMarketingInterest", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32, ParameterDirection.Output)
        cmd.Parameters.Add("xVariable", OracleDbType.Int32, ParameterDirection.Output)

        ds = GetResults(cmd, True)

        If ds.Tables(0)?.Rows?.Count > 0 Then
            drow = ds.Tables(0).Rows(0)     ' Only one row should ever be returned by this search.

            customer = New Customer With {
                .CompanyName = If(drow("COMPANYNAME")?.ToString(), ""),
                .CountryCode = If(drow("COUNTRYCODE")?.ToString(), ""),
                .CountryName = If(drow("COUNTRYNAME")?.ToString(), ""),
                .CustomerID = If(drow("CUSTOMERID")?.ToString(), ""),
                .EmailAddress = drow("EMAILADDRESS").ToString(),
                .EmailOk = drow("EMAILOK").ToString(),  ' TODO: Make this a true Boolean, instead of relying on VB magic to parse the string value.
                .Enabledocumentidnumber = If(drow("ENABLEDOCUMENTIDNUMBER")?.ToString(), ""),
                .ETDocumentidnumber = If(drow("ETDOCUMENTIDNUMBER")?.ToString(), ""),
                .FaxNumber = If(drow("FAX")?.ToString(), ""),
                .FirstName = drow("FIRSTNAME").ToString(),
                .IsActive = drow("ISActive")?.ToString() = "1",
                .IsSubscriptionActive = drow("ISSUBSCRIPTIONACTIVE")?.ToString() = "1",
                .LastName = drow("LASTNAME").ToString(),
                .LdapID = If(drow("LDAPID")?.ToString(), ""),
                .PhoneExtension = If(drow("PHONEEXTENSION")?.ToString(), ""),
                .PhoneNumber = If(drow("PHONE")?.ToString(), ""),
                .SequenceNumber = If(drow("SEQUENCENUMBER")?.ToString(), ""),
                .SIAMIdentity = If(drow("SIAMIDENTITY")?.ToString(), ""),
                .StatusCode = If(drow("STATUSCODE")?.ToString(), ""),
                .SubscriptionID = If(drow("SUBSCRIPTIONID")?.ToString(), ""),
                .Tax_Exempt = If(drow("TAXEXEMPT")?.ToString(), False),  ' TODO: Make this a true Boolean, instead of relying on VB magic to parse the string value.
                .UpdateDate = If(drow("UPDATEDATE")?.ToString(), ""),
                .UserLocation = Convert.ToInt16(If(drow("USERLOCATION")?.ToString(), "1")),
                .UserName = drow("USERNAME").ToString(),
                .UserType = If(drow("USERTYPE")?.ToString(), ""),
                .Action = CoreObject.ActionType.RESTORE,
                .Address = New Address With {
                    .Line1 = If(drow("ADDRESS1")?.ToString(), ""),
                    .Line2 = If(drow("ADDRESS2")?.ToString(), ""),
                    .Line3 = If(drow("ADDRESS3")?.ToString(), ""),
                    .City = If(drow("CITY")?.ToString(), ""),
                    .State = If(drow("STATE")?.ToString(), ""),
                    .PostalCode = If(drow("POSTALCODE")?.ToString(), ""),
                    .Action = CoreObject.ActionType.RESTORE
                }
            }

            If Not CType(cmd.Parameters("xVariable")?.Value, INullable).IsNull Then
                customer.IsValidTBSubscription = (cmd.Parameters("xVariable").Value.ToString() = "1")
            End If
        Else
            Return Nothing  ' If the main Customer record can't be found, no need to go through the rest.
        End If

        ' Get SAP Accounts
        If ds.Tables(1)?.Rows?.Count > 0 Then
            For Each row As DataRow In ds.Tables(1).Rows
                account = New Account With {
                    .AccountNumber = row("ACCOUNTNUMBER").ToString().Trim(),
                    .SequenceNumber = row("SEQUENCENUMBER").ToString().Trim(),
                    .Customer = customer,
                    .Action = CoreObject.ActionType.RESTORE
                }
                customer.AddAccount(account)
            Next
            'debugString &= $"- SAP Accounts found. Count: {customer.SAPBillToAccounts.Count}.{Environment.NewLine}"
        End If

        'get credit cards
        If ds.Tables(2)?.Rows?.Count > 0 Then
            For Each row As DataRow In ds.Tables(2).Rows
                type = New CreditCardType With {
                    .CreditCardTypeCode = row("CREDITCARDTYPEID").ToString().Trim(),
                    .Description = row("DESCRIPTION").ToString().Trim(),
                    .CardType = row("CARDTYPE").ToString().Trim()
                }
                credit_card = New CreditCard(type) With {
                    .CreditCardNumber = row("CREDITCARDNUMBER").ToString().Trim(), ' Encryption.DeCrypt(drow("CREDITCARDNUMBER").ToString().Trim(), Nothing, Nothing)
                    .SequenceNumber = row("SEQUENCENUMBER").ToString().Trim(),
                    .CSCCode = If(row("CSC")?.ToString().Trim(), ""),
                    .NickName = If(row("NICKNAME")?.ToString().Trim(), ""),
                    .HideCardFlag = (row("HIDECARDFLAG")?.ToString().Trim() = "Y"),
                    .ExpirationDate = Convert.ToString(row("EXPIRATIONDATE"), New Globalization.CultureInfo("en-US")),
                    .Customer = customer,
                    .Action = Account.ActionType.RESTORE
                }
                'debugString &= $"- Card Sequence: {credit_card.SequenceNumber}, Name: {credit_card.NickName}, Hidden: {credit_card.HideCardFlag}, Raw Expiry: {row("EXPIRATIONDATE")}{Environment.NewLine}"
                customer.AddCreditCard(credit_card)
            Next
            'debugString &= $"- Credit Cards found. Count: {customer.CreditCards.Count}.{Environment.NewLine}"
        End If

        'get marketing interests
        If ds.Tables(3)?.Rows?.Count > 0 Then
            For Each row As DataRow In ds.Tables(3).Rows
                marketing_interest = New MarketingInterest With {
                    .MarketingInterestID = If(row("MARKETINGINTERESTID")?.ToString().Trim(), ""),
                    .Description = If(row("TOPIC")?.ToString().Trim(), ""),
                    .SequenceNumber = If(row("SEQUENCENUMBER")?.ToString().Trim(), ""),
                    .Action = Account.ActionType.RESTORE
                }
                customer.AddMarketingInterest(marketing_interest)
            Next
        End If
        'Utilities.LogDebug(debugString)     ' For debugging issues with this function. Comment this line as needed.

        Return customer
    End Function

    Public Function RestoreByID(ByVal Cust_Id As Long) As Object
        Dim customer As Customer
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCustomerDetailByID")
        cmd.Parameters.Add("xUserId", OracleDbType.Char)
        cmd.Parameters("xUserId").Value = Cust_Id

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        Dim j As Integer
        Dim elem As XmlElement

        customer = New Customer
        For j = 0 To nodes.Count - 1 'just hard code to 0 and throw exception if count > 1
            customer.FirstName = nodes(j)("FIRSTNAME").InnerText
            customer.LastName = nodes(j)("LASTNAME").InnerText
            customer.EmailOk = nodes(j)("EMAILOK").InnerText
            customer.EmailAddress = nodes(j)("EMAILADDRESS").InnerText

            elem = nodes(j)("SIAMIDENTITY")
            If elem IsNot Nothing Then customer.SIAMIdentity = nodes(j)("SIAMIDENTITY").InnerText

            elem = nodes(j)("LDAPID")
            If elem IsNot Nothing Then customer.LdapID = nodes(j)("LDAPID").InnerText

            elem = nodes(j)("FAX")
            If elem IsNot Nothing Then customer.FaxNumber = nodes(j)("FAX").InnerText

            elem = nodes(j)("PHONE")
            If elem IsNot Nothing Then customer.PhoneNumber = nodes(j)("PHONE").InnerText

            elem = nodes(j)("PHONEEXTENSION")
            If elem IsNot Nothing Then customer.PhoneExtension = nodes(j)("PHONEEXTENSION").InnerText

            elem = nodes(j)("COMPANYNAME")
            If elem IsNot Nothing Then customer.CompanyName = nodes(j)("COMPANYNAME").InnerText

            elem = nodes(j)("USERLOCATION")
            If elem IsNot Nothing Then customer.UserLocation = Convert.ToInt16(nodes(j)("USERLOCATION").InnerText)

            customer.Address.Line1 = nodes(j)("ADDRESS1").InnerText

            elem = nodes(j)("ADDRESS2")
            If elem IsNot Nothing Then customer.Address.Line2 = nodes(j)("ADDRESS2").InnerText

            elem = nodes(j)("ADDRESS3")
            If elem IsNot Nothing Then customer.Address.Line3 = nodes(j)("ADDRESS3").InnerText

            customer.Address.City = nodes(j)("CITY").InnerText
            customer.Address.State = nodes(j)("STATE").InnerText
            customer.Address.PostalCode = nodes(j)("POSTALCODE").InnerText
            customer.CustomerID = nodes(j)("CUSTOMERID").InnerText
            customer.SequenceNumber = nodes(j)("SEQUENCENUMBER").InnerText
            customer.Action = CoreObject.ActionType.RESTORE
            customer.Address.Action = CoreObject.ActionType.RESTORE
        Next

        If nodes.Count = 0 Then
            Return Nothing 'or thrown not found exception?
        End If

        'get accounts
        Dim accounts() As Account
        Dim account_manager As New AccountDataManager
        accounts = account_manager.GetCustomerAccounts(customer)

        For Each act As Account In accounts
            customer.AddAccount(act)
        Next

        'get credit cards
        Dim cards() As CreditCard
        Dim payment_manager As New PaymentDataManager
        cards = payment_manager.GetCustomerCreditCards(customer)

        For Each card As CreditCard In cards
            customer.AddCreditCard(card)
        Next

        'get marketing interests
        Dim interests() As MarketingInterest
        Dim marketing_manager As New MarketingInterestDataManager
        interests = marketing_manager.GetCustomerMarketingInterests(customer)

        For Each interest As MarketingInterest In interests
            customer.AddMarketingInterest(interest)
        Next

        Return customer
    End Function

    Public Function GetNonValidatedAccountHolders() As Customer()
        Dim customer_list As New ArrayList
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getNonValidatedCustomers")

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        If nodes.Count = 0 Then Return Nothing  ' 2018-06-14 ASleight - Moved up from below to prevent NRE.

        For Each node As XmlNode In nodes
            customer_list.Add(New Customer With {
                .CustomerID = If(node("CUSTOMERID").InnerText, ""),
                .FirstName = If(node("FIRSTNAME")?.InnerText, ""),
                .LastName = If(node("LASTNAME")?.InnerText, ""),
                .EmailOk = If(node("EMAILOK")?.InnerText, False),
                .EmailAddress = If(node("EMAILADDRESS")?.InnerText, ""),
                .FaxNumber = If(node("FAX")?.InnerText, ""),
                .PhoneNumber = If(node("PHONE")?.InnerText, ""),
                .PhoneExtension = If(node("PHONEEXTENSION")?.InnerText, ""),
                .CompanyName = If(node("COMPANYNAME")?.InnerText, ""),
                .SIAMIdentity = If(node("SIAMIDENTITY")?.InnerText, ""),
                .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, ""),
                .UpdateDate = If(node("UPDATEDATE")?.InnerText, "2001-01-01"),
                .LdapID = If(node("LDAPID")?.InnerText, ""),
                .Action = CoreObject.ActionType.RESTORE,
                .Address = New Address() With {
                    .Line1 = If(node("ADDRESS1")?.InnerText, ""),
                    .Line2 = If(node("ADDRESS2")?.InnerText, ""),
                    .Line3 = If(node("ADDRESS3")?.InnerText, ""),
                    .City = If(node("CITY")?.InnerText, ""),
                    .State = If(node("STATE")?.InnerText, ""),
                    .PostalCode = If(node("POSTALCODE")?.InnerText, "")
                }
            })
        Next

        'get accounts
        'Dim accounts() As Account
        'Dim account_manager As New AccountDataManager

        'For Each c As Customer In customer_list
        '    accounts = account_manager.GetCustomerAccounts(c)
        '    For Each act As Account In accounts
        '        c.AddAccount(act)
        '    Next
        'Next

        Dim customers(customer_list.Count - 1) As Customer
        customer_list.CopyTo(customers)

        Return customers
    End Function

    Public Function GetAllMarketingInterests(ByRef Language_id As String) As MarketingInterest()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getMarketingInterests")
        Dim marketing_interest_list As New ArrayList
        Dim j As Integer

        cmd.Parameters.Add("p_language_id", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_language_id").Value = Language_id

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes

        For j = 0 To nodes.Count - 1
            marketing_interest_list.Add(New MarketingInterest With {
                .MarketingInterestID = nodes(j)("MARKETINGINTERESTID").InnerText,
                .Description = nodes(j)("TOPIC").InnerText
            })
        Next

        Dim interest_types(marketing_interest_list.Count - 1) As MarketingInterest
        marketing_interest_list.CopyTo(interest_types)
        Return interest_types
    End Function

    Public Function GetCustomersWithLDAPID(ByVal LDAPID As String) As List(Of Customer)
        Dim cmd As New OracleCommand("CustomerDetailsByLDAPID.GetCustomersByLDAPID")
        Dim customer As Customer
        Dim dataset As New DataSet
        Dim customer_list As New List(Of Customer)

        cmd.Parameters.Add("xLDAPID", OracleDbType.Char) '6668
        cmd.Parameters("xLDAPID").Value = LDAPID

        cmd.Parameters.Add(New OracleParameter("xCustomerDetails", OracleDbType.RefCursor, ParameterDirection.Output))

        Try
            dataset = GetResults(cmd, True)

            If dataset.Tables(0).Rows.Count > 1 Then
                For Each drow As DataRow In dataset.Tables(0).Rows
                    customer = New Customer With {
                        .CustomerID = drow("CustomerId").ToString().Trim(),
                        .EmailAddress = drow("EmailAddress").ToString().Trim(),
                        .StatusCode = drow("StatusCode").ToString().Trim()
                    }
                    customer_list.Add(customer)
                Next
            End If
        Catch ex As Exception
            Throw ex
        Finally
            CloseConnection()
        End Try

        Return customer_list
    End Function

    Public Function GetCustomerID(ByVal siamID As String, ByRef context As TransactionContext) As Integer
        Dim custID As Integer
        Dim cmd As New OracleCommand("getCustomerID")

        cmd.Parameters.Add("xSIAMId", OracleDbType.Varchar2)
        cmd.Parameters("xSIAMId").Value = siamID

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            custID = Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())
        End If

        Return custID
    End Function

    Public Function GetCustomerIDWithEmailAddress(ByVal emailAddress As String, ByRef context As TransactionContext) As Integer
        Dim custID As Integer
        Dim cmd As New OracleCommand("getCustomerIDWITHEMAILADDRESS")

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2)
        cmd.Parameters("xEmailAddress").Value = emailAddress

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            custID = Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())
        End If

        Return custID
    End Function

    Public Function GetActiveCustomerEmailAddress(ByVal ldapId As String, ByRef context As TransactionContext) As String
        Dim emailAddress As String = Nothing
        Dim cmd As New OracleCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "GETACTIVECUSTOMEREMAILADDRESS"

        cmd.Parameters.Add("xLDAPID", OracleDbType.Varchar2)
        cmd.Parameters("xLDAPID").Value = ldapId

        Dim outparam1 As New OracleParameter("xEmailAddress", OracleDbType.Varchar2, 100)
        outparam1.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam1)

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("xEmailAddress")?.Value, INullable).IsNull Then
            emailAddress = cmd.Parameters("xEmailAddress").Value.ToString()
        End If

        Return emailAddress
    End Function

    Public Sub UpdateLoginTime(ByVal customerID As Integer, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "UpdateLoginTime"

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = customerID

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)
    End Sub

    Public Sub UpdateLdapid(ByVal customerID As Integer, ByVal LDAPID As String, ByRef context As TransactionContext)
        Dim cmd As New OracleCommand
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "UPDATELDAPDID"

        cmd.Parameters.Add("xcustomerid", OracleDbType.Int32)
        cmd.Parameters("xcustomerid").Value = customerID

        cmd.Parameters.Add("xLDAPID", OracleDbType.Varchar2)
        cmd.Parameters("xLDAPID").Value = LDAPID

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, context)
    End Sub

    Public Sub SaveServiceAgreementData(ByRef registrationData As ProductRegistration, ByRef context As TransactionContext)
        Dim int_AgreementID As Long
        Dim intShipToID As Long
        Dim certStatus As Short
        Dim cmd As New OracleCommand

        Try
            ' 1 - Store Customer Data
            CreateServiceAgreementCustomerCommand(cmd, registrationData)     ' CommandText = "ADDSERVICEAGREEMENTCUSTOMER"
            cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
            ExecuteCMD(cmd, context)
            If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            int_AgreementID = Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())

            ' 2 - Store Ship To Data
            CreateServiceAgreementShipToCommand(cmd, registrationData, int_AgreementID)  ' CommandText = "ADDSERVICEAGREEMENTSHIPTO"
            cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
            ExecuteCMD(cmd, context)
            If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            intShipToID = Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())

            ' 3 - Loop and Store Product Data
            For Each objProduct As ProductRegistration.Product In registrationData.RegisteredProducts
                CreateServiceAgreementProductCommand(cmd, objProduct, registrationData.CustomerID, int_AgreementID, intShipToID, registrationData.Dealer) ' CommandText = "ADDServiceAgreementProduct"
                cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
                ExecuteCMD(cmd, context)
                If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
                certStatus = Convert.ToInt16(cmd.Parameters("retVal").Value.ToString())

                If certStatus < 0 Then
                    If certStatus = -1 Then
                        objProduct.CertificateStatus = CertificateUseType.UsedBySameCustomer
                    ElseIf certStatus = -2 Then
                        objProduct.CertificateStatus = CertificateUseType.UsedByAnotherCustomer
                    ElseIf certStatus = -3 Then
                        objProduct.CertificateStatus = CertificateUseType.NotAValidSerialNumber
                    End If
                    Throw New ApplicationException("DUPCERT")
                End If
            Next
        Catch ex As Exception
            '  Throw ex
            If (ex.Message.Contains("DUPCERT")) Then
                Throw New ApplicationException("DUPCERT")
            Else
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End If
        End Try
    End Sub

    Private Sub CreateServiceAgreementCustomerCommand(ByRef cmd As OracleCommand, ByRef ProductRegistrationData As ProductRegistration)
        cmd = New OracleCommand("ADDSERVICEAGREEMENTCUSTOMER") With {
            .CommandType = CommandType.StoredProcedure
        }

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = ProductRegistrationData.CustomerID

        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xFirstName").Value = ProductRegistrationData.CustomerAddress.FirstName

        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xLastName").Value = ProductRegistrationData.CustomerAddress.LastName

        'cmd.Parameters.Add("xEmailOK", OracleDbType.Int32)
        'cmd.Parameters("xEmailOK").Value = IIf(ProductRegistrationData.OptIN, 1, 0)

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50)
        cmd.Parameters("xEmailAddress").Value = ProductRegistrationData.CustomerAddress.EmailAddress

        cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 50)
        cmd.Parameters("xFax").Value = ProductRegistrationData.CustomerAddress.Fax

        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPhone").Value = ProductRegistrationData.CustomerAddress.Phone

        cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPhoneExtension").Value = ProductRegistrationData.CustomerAddress.PhoneExtn

        cmd.Parameters.Add("xAddressLine1", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine1").Value = ProductRegistrationData.CustomerAddress.Address1

        cmd.Parameters.Add("xAddressLine2", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine2").Value = ProductRegistrationData.CustomerAddress.Address2

        cmd.Parameters.Add("xAddressLine3", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine3").Value = ProductRegistrationData.CustomerAddress.Address3

        cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCity").Value = ProductRegistrationData.CustomerAddress.City

        cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50)
        cmd.Parameters("xState").Value = ProductRegistrationData.CustomerAddress.State

        cmd.Parameters.Add("xPostalCode", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPostalCode").Value = ProductRegistrationData.CustomerAddress.ZipCode

        cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCompanyName").Value = ProductRegistrationData.CustomerAddress.Company

        'cmd.Parameters.Add("xIsAccountHolder", OracleDbType.Varchar2, 50)
        'cmd.Parameters("xIsAccountHolder").Value = IIf(ProductRegistrationData.IsAccountHolder, 1, 0)

        cmd.Parameters.Add("xhttp_x_forwarded_for", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_x_forwarded_for").Value = ProductRegistrationData.HTTP_X_Forward_For

        cmd.Parameters.Add("xremote_addr", OracleDbType.Varchar2)
        cmd.Parameters("xremote_addr").Value = ProductRegistrationData.RemoteADDR

        cmd.Parameters.Add("xhttp_referer", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_referer").Value = ProductRegistrationData.HTTPReferer

        cmd.Parameters.Add("xhttp_url", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_url").Value = ProductRegistrationData.HTTPURL

        cmd.Parameters.Add("xhttp_user_agent", OracleDbType.Varchar2)
        cmd.Parameters("xhttp_user_agent").Value = ProductRegistrationData.HTTPUserAgent

        cmd.Parameters.Add("xcustomersequencenumber", OracleDbType.Int32)
        cmd.Parameters("xcustomersequencenumber").Value = ProductRegistrationData.CustomerSequenceNumber
    End Sub

    Private Sub CreateServiceAgreementShipToCommand(ByRef cmd As OracleCommand, ByRef ProductRegistrationData As ProductRegistration, ByVal AgreementID As Long)
        cmd = New OracleCommand("ADDSERVICEAGREEMENTSHIPTO") With {
            .CommandType = CommandType.StoredProcedure
        }

        cmd.Parameters.Add("xAGREEMENTID", OracleDbType.Int32)
        cmd.Parameters("xAGREEMENTID").Value = AgreementID

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = ProductRegistrationData.CustomerID

        cmd.Parameters.Add("xFirstName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xFirstName").Value = ProductRegistrationData.LocationAddress.FirstName

        cmd.Parameters.Add("xLastName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xLastName").Value = ProductRegistrationData.LocationAddress.LastName

        cmd.Parameters.Add("xEmailAddress", OracleDbType.Varchar2, 50)
        cmd.Parameters("xEmailAddress").Value = ProductRegistrationData.LocationAddress.EmailAddress

        cmd.Parameters.Add("xFax", OracleDbType.Varchar2, 50)
        cmd.Parameters("xFax").Value = ProductRegistrationData.LocationAddress.Fax

        cmd.Parameters.Add("xPhone", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPhone").Value = ProductRegistrationData.LocationAddress.Phone

        cmd.Parameters.Add("xPhoneExtension", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPhoneExtension").Value = ProductRegistrationData.LocationAddress.PhoneExtn

        cmd.Parameters.Add("xAddressLine1", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine1").Value = ProductRegistrationData.LocationAddress.Address1

        cmd.Parameters.Add("xAddressLine2", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine2").Value = ProductRegistrationData.LocationAddress.Address2

        cmd.Parameters.Add("xAddressLine3", OracleDbType.Varchar2, 50)
        cmd.Parameters("xAddressLine3").Value = ProductRegistrationData.LocationAddress.Address3

        cmd.Parameters.Add("xCity", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCity").Value = ProductRegistrationData.LocationAddress.City

        cmd.Parameters.Add("xState", OracleDbType.Varchar2, 50)
        cmd.Parameters("xState").Value = ProductRegistrationData.LocationAddress.State

        cmd.Parameters.Add("xPostalCode", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPostalCode").Value = ProductRegistrationData.LocationAddress.ZipCode

        cmd.Parameters.Add("xCompanyName", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCompanyName").Value = ProductRegistrationData.LocationAddress.Company
    End Sub

    Private Sub CreateServiceAgreementProductCommand(ByRef cmd As OracleCommand, ByRef RegisteredProductData As ProductRegistration.Product, ByVal CustomerID As Long, ByVal AgreementID As Long, ByVal ShiptoID As Long, ByVal Dealer As String)
        cmd = New OracleCommand("ADDServiceAgreementProduct")

        cmd.Parameters.Add("xAGREEMENTSHIPTOID", OracleDbType.Int32)
        cmd.Parameters("xAGREEMENTSHIPTOID").Value = ShiptoID

        cmd.Parameters.Add("xAGREEMENTID", OracleDbType.Int32)
        cmd.Parameters("xAGREEMENTID").Value = AgreementID

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = CustomerID

        cmd.Parameters.Add("xPRODUCTCODE", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPRODUCTCODE").Value = RegisteredProductData.ProductCode

        cmd.Parameters.Add("xMODELNUMBER", OracleDbType.Varchar2, 50)
        cmd.Parameters("xMODELNUMBER").Value = RegisteredProductData.ModelNumber

        cmd.Parameters.Add("xPURCHASEDATE", OracleDbType.Varchar2, 10)
        cmd.Parameters("xPURCHASEDATE").Value = RegisteredProductData.PurchaseDate

        cmd.Parameters.Add("xCERTIFICATENUMBER", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCERTIFICATENUMBER").Value = RegisteredProductData.CertificateNumber

        cmd.Parameters.Add("xSERIALNUMBER", OracleDbType.Varchar2, 50)
        cmd.Parameters("xSERIALNUMBER").Value = RegisteredProductData.SerialNumber

        cmd.Parameters.Add("xDealer", OracleDbType.Varchar2, 50)
        cmd.Parameters("xDealer").Value = Dealer

        cmd.Parameters.Add("xPRODUCTCATAGORY", OracleDbType.Varchar2, 50)
        cmd.Parameters("xPRODUCTCATAGORY").Value = RegisteredProductData.ProductCatagory
    End Sub

    Public Function SaveServiceRequestData(ByRef ServiceRequestData As ServiceItemInfo, ByRef context As TransactionContext) As Long
        Dim cmd As New OracleCommand("ADDSERVICEREQUESTDATA")
        cmd.CommandType = CommandType.StoredProcedure

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output)

        Try
            cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
            cmd.Parameters("xCUSTOMERID").Value = ServiceRequestData.CustomerID

            cmd.Parameters.Add("xISACCOUNTHOLDER", OracleDbType.Int16)
            cmd.Parameters("xISACCOUNTHOLDER").Value = IIf(ServiceRequestData.IsAccountHolder, 1, 0)

            cmd.Parameters.Add("xPAYMENTTYPE", OracleDbType.Varchar2, 20)
            cmd.Parameters("xPAYMENTTYPE").Value = ""

            If ServiceRequestData.WarrantyType.ToString().ToUpper() = "O" Then
                If ServiceRequestData.PaymentType = "1" Then
                    cmd.Parameters("xPAYMENTTYPE").Value = "SONY ACCOUNT"
                ElseIf ServiceRequestData.PaymentType = "2" Then
                    cmd.Parameters("xPAYMENTTYPE").Value = "CREDIT CARD"
                Else
                    cmd.Parameters("xPAYMENTTYPE").Value = "CHECK"
                End If
            End If

            cmd.Parameters.Add("xPAYERACCOUNT", OracleDbType.Varchar2, 250)
            cmd.Parameters("xPAYERACCOUNT").Value = ServiceRequestData.PayerAccount

            cmd.Parameters.Add("xBILLTOATTENTION", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOATTENTION").Value = ServiceRequestData.BillToAddr.AttnName

            cmd.Parameters.Add("xBILLTOCOMPANY", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOCOMPANY").Value = ServiceRequestData.BillToAddr.Company

            cmd.Parameters.Add("xBILLTOADDRESS1", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOADDRESS1").Value = ServiceRequestData.BillToAddr.Address1

            cmd.Parameters.Add("xBILLTOADDRESS2", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOADDRESS2").Value = ServiceRequestData.BillToAddr.Address2

            cmd.Parameters.Add("xBILLTOADDRESS3", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOADDRESS3").Value = ServiceRequestData.BillToAddr.Address3

            cmd.Parameters.Add("xBILLTOADDRESS4", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOADDRESS4").Value = ServiceRequestData.BillToAddr.Address4

            cmd.Parameters.Add("xBILLTOCITY", OracleDbType.Varchar2, 50)
            cmd.Parameters("xBILLTOCITY").Value = ServiceRequestData.BillToAddr.City

            cmd.Parameters.Add("xBILLTOSTATE", OracleDbType.Varchar2, 20)
            cmd.Parameters("xBILLTOSTATE").Value = ServiceRequestData.BillToAddr.State

            cmd.Parameters.Add("xBILLTOPOSTALCODE", OracleDbType.Varchar2, 10)
            cmd.Parameters("xBILLTOPOSTALCODE").Value = ServiceRequestData.BillToAddr.ZipCode

            cmd.Parameters.Add("xBILLTOPHONE", OracleDbType.Varchar2, 20)
            cmd.Parameters("xBILLTOPHONE").Value = ServiceRequestData.BillToAddr.Phone

            cmd.Parameters.Add("xBILLTOPHONEEXTENSION", OracleDbType.Varchar2, 6)
            cmd.Parameters("xBILLTOPHONEEXTENSION").Value = ServiceRequestData.BillToAddr.PhoneExtn

            cmd.Parameters.Add("xBILLTOFAX", OracleDbType.Varchar2, 20)
            cmd.Parameters("xBILLTOFAX").Value = ServiceRequestData.BillToAddr.Fax

            'cmd.Parameters.Add("xSHIPTOACCOUNT", OracleDbType.Varchar2, 250)
            'cmd.Parameters("xSHIPTOACCOUNT").Value = ServiceRequestData.ShipToAccount

            cmd.Parameters.Add("xSHIPTOATTENTION", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOATTENTION").Value = ServiceRequestData.ShipToAddr.AttnName

            cmd.Parameters.Add("xSHIPTOCOMPANY", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOCOMPANY").Value = ServiceRequestData.ShipToAddr.Company

            cmd.Parameters.Add("xSHIPTOADDRESS1", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOADDRESS1").Value = ServiceRequestData.ShipToAddr.Address1

            cmd.Parameters.Add("xSHIPTOADDRESS2", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOADDRESS2").Value = ServiceRequestData.ShipToAddr.Address2

            cmd.Parameters.Add("xSHIPTOADDRESS3", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOADDRESS3").Value = ServiceRequestData.ShipToAddr.Address3

            cmd.Parameters.Add("xSHIPTOADDRESS4", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOADDRESS4").Value = ServiceRequestData.ShipToAddr.Address4

            cmd.Parameters.Add("xSHIPTOCITY", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSHIPTOCITY").Value = ServiceRequestData.ShipToAddr.City

            cmd.Parameters.Add("xSHIPTOSTATE", OracleDbType.Varchar2, 20)
            cmd.Parameters("xSHIPTOSTATE").Value = ServiceRequestData.ShipToAddr.State

            cmd.Parameters.Add("xSHIPTOPOSTALCODE", OracleDbType.Varchar2, 10)
            cmd.Parameters("xSHIPTOPOSTALCODE").Value = ServiceRequestData.ShipToAddr.ZipCode

            cmd.Parameters.Add("xSHIPTOPHONE", OracleDbType.Varchar2, 20)
            cmd.Parameters("xSHIPTOPHONE").Value = ServiceRequestData.ShipToAddr.Phone

            cmd.Parameters.Add("xSHIPTOPHONEEXTENSION", OracleDbType.Varchar2, 6)
            cmd.Parameters("xSHIPTOPHONEEXTENSION").Value = ServiceRequestData.ShipToAddr.PhoneExtn

            cmd.Parameters.Add("xSHIPTOFAX", OracleDbType.Varchar2, 20)
            cmd.Parameters("xSHIPTOFAX").Value = ServiceRequestData.ShipToAddr.Fax

            cmd.Parameters.Add("xMODELNUMBER", OracleDbType.Varchar2, 50)
            cmd.Parameters("xMODELNUMBER").Value = ServiceRequestData.ModelNumber

            cmd.Parameters.Add("xSERIALNUMBER", OracleDbType.Varchar2, 50)
            cmd.Parameters("xSERIALNUMBER").Value = ServiceRequestData.SerialNumber

            cmd.Parameters.Add("xACCESSORIES", OracleDbType.Varchar2, 250)
            cmd.Parameters("xACCESSORIES").Value = ServiceRequestData.Accessories

            cmd.Parameters.Add("xPONUMBER", OracleDbType.Varchar2, 20)
            cmd.Parameters("xPONUMBER").Value = ServiceRequestData.PONumber

            'cmd.Parameters.Add("xFIXEDAPPROVALAMOUNT", OracleDbType.Int16)
            'cmd.Parameters("xFIXEDAPPROVALAMOUNT").Value = IIf(ServiceRequestData.isApproval, 1, 0)

            cmd.Parameters.Add("xPREAPPROVALAMOUNT", OracleDbType.Decimal)
            cmd.Parameters("xPREAPPROVALAMOUNT").Value = ServiceRequestData.PreApproveAmount

            cmd.Parameters.Add("xCONTACTNAME", OracleDbType.Varchar2, 100)
            cmd.Parameters("xCONTACTNAME").Value = ServiceRequestData.ContactPersonName

            cmd.Parameters.Add("xCONTACTEMAILADDRESS", OracleDbType.Varchar2, 100)
            cmd.Parameters("xCONTACTEMAILADDRESS").Value = ServiceRequestData.ContactEmailAddress

            cmd.Parameters.Add("xINTERMITTENTPROBLEM", OracleDbType.Int16)
            cmd.Parameters("xINTERMITTENTPROBLEM").Value = IIf(ServiceRequestData.isIntermittentProblem, 1, 0)

            cmd.Parameters.Add("xPREVENTIVEMAINTENANCE", OracleDbType.Int16)
            cmd.Parameters("xPREVENTIVEMAINTENANCE").Value = IIf(ServiceRequestData.isPreventiveMaintenance, 1, 0)

            cmd.Parameters.Add("xPROBLEMDESCRIPTION", OracleDbType.Varchar2, 250)
            cmd.Parameters("xPROBLEMDESCRIPTION").Value = ServiceRequestData.ProblemDescription

            cmd.Parameters.Add("xCONTRACTNUMBER", OracleDbType.Varchar2, 50)
            cmd.Parameters("xCONTRACTNUMBER").Value = ServiceRequestData.ContractNumber

            cmd.Parameters.Add("xWARRANTYTYPE", OracleDbType.Char, 1)
            cmd.Parameters("xWARRANTYTYPE").Value = UCase(ServiceRequestData.WarrantyType)

            cmd.Parameters.Add("xTaxExempt", OracleDbType.Int16)
            cmd.Parameters("xTaxExempt").Value = IIf(ServiceRequestData.isTaxExempt, 1, 0)

            cmd.Parameters.Add("xISShipOnHold", OracleDbType.Int16)
            cmd.Parameters("xISShipOnHold").Value = IIf(ServiceRequestData.IsProductShipOnHold, 1, 0)

            cmd.Parameters.Add("xSERVICECENTERADDRESSID", OracleDbType.Int32)
            cmd.Parameters("xSERVICECENTERADDRESSID").Value = ServiceRequestData.DepotCenterAddressID

            cmd.Parameters.Add(outparam)

            ExecuteCMD(cmd, context)
            If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                'Throw New ApplicationException("Service Request Save Error")
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            Else
                Return Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())
            End If
        Catch ex As Exception
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function
End Class
