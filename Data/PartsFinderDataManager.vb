
Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core

Public Class PartsFinderDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)

    End Sub

    ' ASleight - This appears to be an orphaned function.
    'Public Function CheckParts(ByVal PartNumber As String) As Boolean
    '    Dim rc As Boolean = False
    '    Dim doc As XmlDocument
    '    Dim cmd As New OracleCommand("CheckParts")

    '    cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 20, PartNumber, ParameterDirection.Input)

    '    doc = GetResults(cmd)

    '    Dim root As XmlElement = doc.DocumentElement
    '    Dim nodes As XmlNodeList = root.ChildNodes

    '    If nodes.Count > 0 Then

    '        Dim sReplacementpartnubmer As String = ""
    '        Dim sLISTPRICE As String = ""
    '        Dim sPARTCATEGORY As String = ""
    '        Dim sdescription As String = ""
    '        Dim j As Integer
    '        For j = 1 To nodes.Count - 1
    '            sReplacementpartnubmer = nodes(j)("REPLACEMENTPARTNUMBER").InnerText()
    '            sLISTPRICE = nodes(j)("LISTPRICE").InnerText()
    '            sPARTCATEGORY = nodes(j)("PARTCATEGORY").InnerText()
    '            sdescription = nodes(j)("Description").InnerText()
    '        Next
    '        If (sReplacementpartnubmer.Equals(PartNumber) Or String.IsNullOrEmpty(sReplacementpartnubmer)) Then
    '            If sReplacementpartnubmer.StartsWith("990199") Then
    '                Throw New ServicesPlusBusinessException("")
    '            End If
    '            If sPARTCATEGORY.ToUpper().StartsWith("HOL") Or sPARTCATEGORY.ToUpper().StartsWith("INT") Then

    '            End If
    '            If String.IsNullOrEmpty(sLISTPRICE) Then
    '            ElseIf Convert.ToInt64(sLISTPRICE) >= 0 Then
    '            End If
    '        Else
    '            Return CheckParts(sReplacementpartnubmer)
    '        End If
    '        rc = True
    '    Else
    '        Throw New ServicesPlusBusinessException("Error adding " + PartNumber + " to cart.")
    '    End If

    '    Return rc
    'End Function
    'Added/Modified by Umesh and Sneha

    Public Function GetModels(ByVal model_number As String, ByRef objGlobalData As GlobalData, Optional ByRef lstConflicts As List(Of String) = Nothing) As Model()
        Dim model_list As New ArrayList
        Dim doc As New XmlDocument
        Dim nodes As XmlNodeList
        Dim j As Integer
        Dim model As Model

        Dim cmd As New OracleCommand("pfind.getnewmodels") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_modelid", OracleDbType.Varchar2, 20, model_number, ParameterDirection.Input)
        cmd.Parameters.Add("p_Sales_org", OracleDbType.Varchar2, 20, objGlobalData.SalesOrganization, ParameterDirection.Input)
        cmd.Parameters.Add("p_language_id", OracleDbType.Varchar2, 20, objGlobalData.Language, ParameterDirection.Input)

        cmd.Parameters.Add("rc_confmodels", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("rc_rowcount", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("rc_newmodels", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection

        Dim myAdaptor As New OracleDataAdapter(cmd)
        Dim dataset As New DataSet
        Dim xml_data As String

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        CloseConnection()

        'internalize model information

        Try
            doc.LoadXml(xml_data)
            nodes = doc.DocumentElement.ChildNodes

            lstConflicts = New List(Of String)
            For i As Integer = 0 To doc.GetElementsByTagName("OLD_MATERIAL").Count - 1
                If nodes(i).Item("OLD_MATERIAL") IsNot Nothing Then
                    lstConflicts.Add(nodes(i).Item("OLD_MATERIAL").InnerText + ":" + nodes(i).Item("NEW_MATERIAL").InnerText)
                End If
            Next

            nodes = doc.GetElementsByTagName("Table2")
            For j = 0 To doc.GetElementsByTagName("Table2").Count - 1
                model = New Model With {
                    .ModelID = If(nodes(j).Item("MODEL_ID")?.InnerText, ""),
                    .Description = If(nodes(j).Item("DESCRIPTION")?.InnerText, ""),
                    .AddIn = If(nodes(j).Item("ADD_IN")?.InnerText, ""),
                    .CountryID = If(nodes(j).Item("COUNTRY_ID")?.InnerText, ""),
                    .EndSerialNumber = If(nodes(j).Item("END_SERIAL_NUMBER")?.InnerText, ""),
                    .ManualID = If(nodes(j).Item("MANUAL_ID")?.InnerText, ""),
                    .ModelGroup = If(nodes(j).Item("MODEL_GROUP")?.InnerText, ""),
                    .StartSerialNumber = If(nodes(j).Item("START_SERIAL_NUMBER")?.InnerText, ""),
                    .Destination = If(nodes(j).Item("DESTINATION")?.InnerText, ""),
                    .SerialRange = If(nodes(j).Item("SERIAL_RANGE")?.InnerText, "")
                }
                model_list.Add(model)
            Next
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Dim models(model_list.Count - 1) As Model
        model_list.CopyTo(models)

        Return models
    End Function

    Public Sub GetSubModels(ByRef model As Model)
        Dim doc As New XmlDocument
        Dim dataset As New DataSet
        Dim xml_data As String

        Dim cmd As New OracleCommand("pfind.getsubmodels") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_modelgroup", OracleDbType.Varchar2, 20, model.ModelGroup, ParameterDirection.Input)
        cmd.Parameters.Add("rc_submodels", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection
        Dim myAdaptor As New OracleDataAdapter(cmd)
        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        dbConnection.Close()

        Try
            doc.LoadXml(xml_data)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim sub_model As Model

            For Each node As XmlNode In nodes
                sub_model = New Model With {
                    .StartSerialNumber = "-",
                    .EndSerialNumber = "-",
                    .ModelID = If(node("MODEL_ID")?.InnerText, ""),
                    .Description = If(node("DESCRIPTION")?.InnerText, ""),
                    .CountryID = If(node("COUNTRY_ID")?.InnerText, ""),
                    .SerialRange = If(node("SERIAL_RANGE")?.InnerText, ""),
                    .ManualID = If(node("MANUAL_ID")?.InnerText, ""),
                    .AddIn = If(node("ADD_IN")?.InnerText, ""),
                    .ModelGroup = If(node("MODEL_GROUP")?.InnerText, "")
                }
                model.AddSubModel(sub_model)
            Next

        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub GetModelCategories(ByRef model As Model)
        Dim doc As New XmlDocument
        Dim nodes As XmlNodeList
        Dim dataset As New DataSet
        Dim xml_data As String
        Dim cmd As New OracleCommand("pfind.getcategories") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("p_modelgroup", OracleDbType.Varchar2, 50, model.ModelGroup, ParameterDirection.Input)
        cmd.Parameters.Add("rc_categories", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection
        Dim myAdaptor As New OracleDataAdapter(cmd)
        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Console.WriteLine("troubles calling partsfinder: " + ex.Message)
            Throw ex
        End Try
        CloseConnection()

        Try
            doc.LoadXml(xml_data)

            nodes = doc.DocumentElement.ChildNodes
            Dim j As Integer
            Dim category As Category

            For j = 0 To nodes.Count - 1
                category = New Category With {
                    .CategoryID = If(nodes(j)("CATEGORY_ID")?.InnerText, ""),
                    .Description = If(nodes(j)("CATEGORY_DESCRIPTION")?.InnerText, "")
                }
                model.AddCategory(category)
            Next
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Function GetModules(ByVal model As Model, ByVal category As Category) As _Module()
        Dim dataset As New DataSet
        Dim doc As New XmlDocument
        Dim xml_data As String
        Dim module_list As New ArrayList
        Dim cmd As New OracleCommand("pfind.getmodules") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_modelgroup", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_modelgroup").Value = model.ModelGroup
        cmd.Parameters.Add("p_categoryid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_categoryid").Value = category.CategoryID
        cmd.Parameters.Add("p_startSerialNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_startSerialNumber").Value = model.StartSerialNumber
        cmd.Parameters.Add("p_endSerialNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_endSerialNumber").Value = model.EndSerialNumber
        cmd.Parameters.Add("p_countryID", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_countryID").Value = model.CountryID

        cmd.Parameters.Add("rc_modules", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection
        Dim myAdaptor As New OracleDataAdapter(cmd)
        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        CloseConnection()

        Try
            doc.LoadXml(xml_data)

            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            'Dim elem As XmlElement
            Dim j As Integer
            Dim m As _Module

            For j = 0 To nodes.Count - 1
                m = New _Module With {
                    .Name = nodes(j)("MODULE_NAME").InnerText,
                    .ModuleID = nodes(j)("MODULE_ID").InnerText,
                    .HasDiagram = (nodes(j)("DIAGRAM_EXISTS")?.InnerText = "Y")
                }
                module_list.Add(m)
            Next
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Dim modules(module_list.Count - 1) As _Module
        module_list.CopyTo(modules)

        Return modules
    End Function

    Public Function GetParts(ByVal model As Model, ByVal category As Category, ByVal _module As _Module, ByVal refine_search As String, ByRef objGlobalData As GlobalData, Optional ByVal refine_type As Part.PartsFinderRefineSearchType = Part.PartsFinderRefineSearchType.ByDescription) As Part()
        Dim part_list As New ArrayList
        Dim dataset As New DataSet
        Dim part As Part
        Dim debugMessage As String = $"PartsFinderDM.GetParts - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"
        Dim debugLoop As String = ""    ' Holds the last loop's debug statement when an exception is hit. (Reset at loop start to prevent spamming.)
        Dim debugCount As Integer = 1

        Dim cmd As New OracleCommand("pfind.getparts") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_modelid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_modelid").Value = model.ModelID
        cmd.Parameters.Add("p_modelgroup", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_modelgroup").Value = model.ModelGroup
        cmd.Parameters.Add("p_countryid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_countryid").Value = model.CountryID

        cmd.Parameters.Add("p_categoryid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_categoryid").Value = If(category?.CategoryID, "%")

        cmd.Parameters.Add("p_moduleid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_moduleid").Value = If(_module?.ModuleID, "%")

        cmd.Parameters.Add("p_keyword", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_keyword").Value = "%" + refine_search

        cmd.Parameters.Add("p_keywordtype", OracleDbType.Char)
        cmd.Parameters("p_keywordtype").Value = IIf(refine_type = Part.PartsFinderRefineSearchType.ByDescription, "D", "X")

        cmd.Parameters.Add("p_Sales_Org", OracleDbType.Char)
        cmd.Parameters("p_Sales_Org").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("p_Language_Id", OracleDbType.Char)
        cmd.Parameters("p_Language_Id").Value = objGlobalData.Language

        cmd.Parameters.Add("rc_parts", OracleDbType.RefCursor, ParameterDirection.Output)
        debugMessage &= $"- Parameters added.{Environment.NewLine}"
        OpenConnection()
        cmd.Connection = dbConnection
        Dim myAdaptor As New OracleDataAdapter(cmd)
        'Dim xml_data As String

        Try
            debugMessage &= $"- Calling OracleDataAdapter.Fill.{Environment.NewLine}"
            myAdaptor.Fill(dataset)
            For Each row As DataRow In dataset?.Tables(0)?.Rows
                debugLoop = $"- Loop {debugCount} start.{Environment.NewLine}"
                debugCount += 1
                ' TODO: Fix Double parsing below.
                'part = New Part With {
                '    .PartNumber = row("PART_NUMBER").ToString(),
                '    .ReplacementPartNumber = If(row("REPLACEMENTPARTNUMBER")?.ToString(), ""),
                '    .Model = If(row("MODELNUMBER")?.ToString(), ""),
                '    .Description = row("PART_DESCRIPTION")?.ToString(),
                '    .ModuleName = row("MODULE_NAME")?.ToString(),
                '    .ModuleID = row("MODULE_ID")?.ToString(),
                '    .PartReferenceID = row("PART_REFERENCE_ID")?.ToString(),
                '    .PartUsage = row("PART_USAGE")?.ToString(),
                '    .ModelGroup = row("MODEL_GROUP")?.ToString(),
                '    .ListPrice = IIf(CType(row("LISTPRICE")?.Value, INullable).IsNull, 0.0, Convert.ToDouble(row("LISTPRICE").ToString())),
                '    .ProgramCode = If(row("PROGRAMCODE")?.ToString(), ""),
                '    .CoreCharge = IIf(CType(row("FRBCORECHARGE")?.Value, INullable).IsNull, 0.0, Convert.ToDouble(row("FRBCORECHARGE").ToString())),
                '    .HasExplodedView = (row("DIAGRAM_EXISTS")?.ToString() = "Y")
                '}
                part = New Part()
                part.PartNumber = row("PART_NUMBER").ToString()
                debugLoop &= $"--- Properties as they're parsed: Part Number = {part.PartNumber}"
                part.ReplacementPartNumber = If(row("REPLACEMENTPARTNUMBER")?.ToString(), "")
                debugLoop &= $", Replacement = {part.ReplacementPartNumber}"
                part.Model = If(row("MODELNUMBER")?.ToString(), "")
                debugLoop &= $", Model = {part.Model}"
                part.Description = row("PART_DESCRIPTION")?.ToString()
                debugLoop &= $", Desc = {part.Description}"
                part.ModuleName = row("MODULE_NAME")?.ToString()
                debugLoop &= $", Module = {part.ModuleName}"
                part.ModuleID = row("MODULE_ID")?.ToString()
                debugLoop &= $", Module ID = {part.ModuleID}"
                part.PartReferenceID = row("PART_REFERENCE_ID")?.ToString()
                debugLoop &= $", Part Ref = {part.PartReferenceID}"
                part.PartUsage = row("PART_USAGE")?.ToString()
                debugLoop &= $", Part Use = {part.PartUsage}"
                part.ModelGroup = row("MODEL_GROUP")?.ToString()
                debugLoop &= $", Model Group = {part.ModelGroup}"
                part.ProgramCode = If(row("PROGRAMCODE")?.ToString(), "")
                debugLoop &= $", Prog Code = {part.ProgramCode}"
                part.HasExplodedView = (row("DIAGRAM_EXISTS")?.ToString() = "Y")
                debugLoop &= $", Has Exploded = {part.HasExplodedView} {Environment.NewLine}"
                debugLoop &= $"--- Parsing ListPrice value: {row("LISTPRICE")} {Environment.NewLine}"
                part.ListPrice = IIf(IsDBNull(row("LISTPRICE")), 0.0, row("LISTPRICE")) 'Convert.ToDouble(row("LISTPRICE").ToString()))
                debugLoop &= $"--- Parsing FrbCoreCharge value: {row("FRBCORECHARGE")} {Environment.NewLine}"
                part.CoreCharge = IIf(IsDBNull(row("FRBCORECHARGE")), 0.0, row("FRBCORECHARGE")) 'Convert.ToDouble(row("FRBCORECHARGE").ToString()))

                part_list.Add(part)
            Next
        Catch ex As Exception
            Utilities.LogMessages(debugMessage & debugLoop)
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Dim parts(part_list.Count - 1) As Part
        part_list.CopyTo(parts)

        'Utilities.LogDebug(debugMessage & debugLoop)
        'Utilities.LogDebug($"Parts List length = {If(part_list?.Count, "null")}")
        Return parts
    End Function

    Public Function GetExplodedView(ByVal part As Part) As Byte()
        Dim fake_pdf() As Byte = Nothing
        Dim myReader As OracleDataReader
        Dim pdf_length As Long

        Dim cmd As New OracleCommand("pfind.getDiagram") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_moduleid", OracleDbType.Varchar2, 20, part.ModuleID, ParameterDirection.Input)
        cmd.Parameters.Add("rc_diagram", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection
        Try
            myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            myReader.Read()
            pdf_length = myReader.GetBytes(0, 0, fake_pdf, 0, 8000)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Dim pdf(pdf_length) As Byte
        Try
            pdf_length = myReader.GetBytes(0, 0, pdf, 0, pdf_length)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        myReader.Close()

        Return pdf
    End Function

    Public Function GetExplodedViewPath(ByVal part As Part) As String
        Dim myReader As OracleDataReader
        Dim pdf_path As String = ""
        Dim cmd As New OracleCommand("pfind.getDiagramPath") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("p_moduleid", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_moduleid").Value = part.ModuleID
        cmd.Parameters.Add("rc_diagrampath", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection
        Try
            myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
            myReader.Read()
            If Not myReader.IsDBNull(0) Then
                pdf_path = myReader.GetString(0)
            End If
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        myReader.Close()

        Return pdf_path
    End Function

End Class
