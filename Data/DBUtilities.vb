﻿Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports ServicesPlusException

Public Class DBUtilities
    Inherits DataManager

    Public Function GetSPSErrorList() As DataSet
        Dim dsSPSErrorList As DataSet = Nothing
        Dim cmd As OracleCommand = New OracleCommand
        cmd.CommandText = "SPSERRORMESSAGE.GETMESSAGELIST"
        Dim outparam As New OracleParameter("xERRORLIST", OracleDbType.RefCursor)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        Try
            dsSPSErrorList = GetResults(cmd, True)
        Catch ex As Exception
            'No need to wrap it because do not have the custom message list yet
            ' Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        Return dsSPSErrorList
    End Function

End Class
