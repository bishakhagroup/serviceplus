﻿Imports Oracle.ManagedDataAccess.Client
Imports ServicesPlusException
Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Types

Public Class TaxExemptionDataManager
    Inherits DataManager

    ''' <summary>
    ''' Fetches all of the Tax Exemptions for a given Siam ID
    ''' </summary>
    ''' <param name="siamID">The SIAM ID for a login account.</param>
    ''' <returns>An ArrayList of TaxExemptions</returns>
    Public Function GetTaxExemptions(ByVal siamID As String) As ArrayList
        'Dim outparam As OracleParameter
        Dim myAdaptor As OracleDataAdapter
        Dim data As New DataSet
        Dim taxList As New ArrayList
        Dim cmd As New OracleCommand With {
            .BindByName = True,
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "TaxExempt.GetTaxExemptions"
        }
        cmd.Parameters.Add("xCustomerSiamId", OracleDbType.Varchar2).Value = siamID
        cmd.Parameters.Add("xExemptions", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection

        ' Run the Procedure and fill the DataSet with the returned data
        myAdaptor = New OracleDataAdapter(cmd)
        myAdaptor.Fill(data)

        ' Parse each row into a TaxExemption, adding it to the ArrayList.
        For Each row As DataRow In data.Tables(0).Rows
            taxList.Add(ConvertRow(row))
        Next

        Return taxList
    End Function

    Public Function UpdateTaxExemption(ByVal item As TaxExemption) As Boolean
        Dim cmd As New OracleCommand
        Dim success As Boolean = False

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "TaxExempt.UpdateTaxExemption"

        'Utilities.LogMessages("TAX DM: Update Row - INFO - Row_ID: " & item.RowID & ", Name: " & item.Name & ", City: " & item.City & ", State: " & item.State)
        'Utilities.LogMessages("TAX DM: Update Row - INFO - Doc State: " & item.DocumentState & ", Doc Status: " & item.DocumentStatus & ", Doc Date: " & item.DocumentDate)
        'Utilities.LogMessages("TAX DM: Update Row - INFO - Doc Expiry: " & item.ExpirationDate & ", Doc ID: " & item.DocumentIDNumber)

        ' -- Input Parameters
        cmd.Parameters.Add("xRowId", OracleDbType.Varchar2).Value = item.RowID
        cmd.Parameters.Add("xCustomerName", OracleDbType.Varchar2).Value = item.Name
        cmd.Parameters.Add("xCity", OracleDbType.Varchar2).Value = item.City
        cmd.Parameters.Add("xState", OracleDbType.Varchar2).Value = item.State
        cmd.Parameters.Add("xDocumentState", OracleDbType.Varchar2).Value = item.DocumentState
        cmd.Parameters.Add("xDocumentStatus", OracleDbType.Varchar2).Value = item.DocumentStatus
        cmd.Parameters.Add("xDocumentDate", OracleDbType.Date).Value = item.DocumentDate            '.ToString("dd-MMM-yyyy")
        cmd.Parameters.Add("xExpirationDate", OracleDbType.Date).Value = item.ExpirationDate        '.ToString("dd-MMM-yyyy")
        cmd.Parameters.Add("xDocumentIdNumber", OracleDbType.Varchar2).Value = item.DocumentIDNumber

        ' -- Output Parameter
        cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection

        'Utilities.LogMessages("TAX DM: Update Row - EXECUTING SQL")
        ExecuteCMD(cmd, Context)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull OrElse (cmd.Parameters("retVal")?.Value > 0) Then
            'Utilities.LogMessages("TAX DM: EXCEPTION calling database.")
            Throw New Exception("There was a database error while trying to Update your Tax Exemption.")
        Else
            success = True
        End If

        Return success
    End Function

    Public Function AddTaxExemption(ByVal item As TaxExemption) As Boolean
        Dim rowsAffected As Integer = 0
        Dim cmd As New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "TaxExempt.AddTaxExemption"
        }

        'Utilities.LogMessages("TAX DM: Add Row - INFO - SIAM: " & item.SiamID & ", Name: " & item.Name & ", City: " & item.City & ", State: " & item.State)
        'Utilities.LogMessages("TAX DM: Add Row - INFO - Doc State: " & item.DocumentState & ", Doc Status: " & item.DocumentStatus & ", Doc Date: " & item.DocumentDate)
        'Utilities.LogMessages("TAX DM: Add Row - INFO - Doc Expiry: " & item.ExpirationDate & ", Doc ID: " & item.DocumentIDNumber)

        ' -- Input Parameters
        cmd.Parameters.Add("xCustomerSiamId", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.SiamID), String.Empty, item.SiamID)
        cmd.Parameters.Add("xCustomerName", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.Name), String.Empty, item.Name)
        cmd.Parameters.Add("xCity", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.City), String.Empty, item.City)
        cmd.Parameters.Add("xState", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.State), String.Empty, item.State)
        cmd.Parameters.Add("xDocumentState", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.DocumentState), String.Empty, item.DocumentState)
        cmd.Parameters.Add("xDocumentStatus", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.DocumentStatus), String.Empty, item.DocumentStatus)
        cmd.Parameters.Add("xDocumentDate", OracleDbType.Date).Value = item.DocumentDate        '.ToString("dd-MMM-yyyy")
        cmd.Parameters.Add("xExpirationDate", OracleDbType.Date).Value = item.ExpirationDate    '.ToString("dd-MMM-yyyy")
        cmd.Parameters.Add("xDocumentIdNumber", OracleDbType.Varchar2).Value = IIf(String.IsNullOrEmpty(item.DocumentIDNumber), String.Empty, item.DocumentIDNumber)

        OpenConnection()
        cmd.Connection = dbConnection

        'Utilities.LogMessages("TAX DM: Add Row - EXECUTING SQL")
        rowsAffected = ExecuteCMD(cmd, Context)   ' Returns an Integer for the number of rows affected

        Return (rowsAffected = 1)
    End Function

    Public Sub DeleteTaxExemption(ByVal rowID As String)
        Dim cmd As New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .CommandText = "TaxExempt.DeleteTaxExemption"
        }
        cmd.Parameters.Add("xRowId", OracleDbType.Varchar2).Value = rowID

        OpenConnection()
        cmd.Connection = dbConnection

        ExecuteCMD(cmd, Context)
    End Sub

    ''' <summary>
    ''' Takes a DataRow from the Database and transforms it into a TaxExemption object.
    ''' </summary>
    ''' <param name="row">A DataRow as pulled from Oracle Table EnableTaxExemptStatus.</param>
    ''' <returns>A TaxExemption object with the data filled in.</returns>
    Public Function ConvertRow(ByVal row As DataRow) As TaxExemption
        Dim retVal As New TaxExemption With {
            .SiamID = row("CUSTOMERSIAMIDENTITY").ToString(),
            .Name = row("CUSTOMERNAME").ToString(),
            .City = row("CITY").ToString(),
            .State = row("STATE").ToString(),
            .DocumentState = row("DOCUMENTSTATE").ToString(),
            .DocumentStatus = row("DOCUMENTSTATUS").ToString(),
            .DocumentDate = row("DOCUMENTDATE").ToString(),
            .ExpirationDate = row("EXPIRATIONDATE").ToString(),
            .UpdateDate = row("UPDATEDATE").ToString(),
            .DocumentIDNumber = row("DOCUMENTIDNUMBER").ToString(),
            .RowID = row("Row_ID").ToString()
        }
        Return retVal
    End Function
End Class
