Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.SIAMUtilities

Public Class PCICCAuditDataManager
    Inherits DataManager

    Public Sub New()
        MyBase.New()
    End Sub

    Public Function LogMessageToDB() As Boolean
        Dim cmd As New OracleCommand
        cmd.CommandText = "ADDPCICCAUDITLOG"

        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, 20)
        cmd.Parameters("xUserId").Value = ""

        Dim outparam As New OracleParameter("xIsFirstLogin", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output

        cmd.Parameters.Add(outparam)

        Dim intUpdatedRow As Integer = ExecuteCMD(cmd, Nothing)

        Return False
    End Function

End Class
