Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

' ASleight - Do not trust "Find All References." This class is used in the /e/sw/ (SoftwarePlus Admin) portion of the site.
Public Class UserDataManager
    Protected cn As OracleConnection
    Protected connString As String
    Protected connection_string As String
    Private oContext As TransactionContext

    Public Sub New()
        cn = New OracleConnection
        connString = ConfigurationData.Environment.DataBase.ConnectionString
        cn.ConnectionString = connString
    End Sub

    Public Sub OpenConnection()
        If cn.State = ConnectionState.Closed Then
            cn.Open()
        End If
    End Sub

    Public Sub CloseConnection()
        If cn.State = ConnectionState.Open Then
            cn.Close()
        End If
    End Sub

    Public Sub ExecuteCMD(ByRef command As OracleCommand, ByRef context As TransactionContext, Optional ByVal auto_close_connection As Boolean = True)
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True

        'if no transactional context then manage the connection here with no transaction.
        If context Is Nothing Then
            OpenConnection()
            command.Connection = cn
        Else
            command.Connection = context.NativeConnection
            command.Transaction = context.NativeTransaction
        End If

        Try
            command.ExecuteNonQuery()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        If context Is Nothing And auto_close_connection Then
            CloseConnection()
        End If
    End Sub

    Public Function GetNotificationUsers() As XmlDocument
        Dim return_value As OracleClob
        Dim doc As New XmlDocument
        Dim error_node As XmlNode
        Dim cmd As New OracleCommand("GETCHANGEPWDNOTIFICATIONUSER")

        cmd.Parameters.Add("xDaysAdvance", OracleDbType.Int32)
        cmd.Parameters("xDaysAdvance").Value = 10
        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Clob, ParameterDirection.Output))

        ExecuteCMD(cmd, Nothing, False)

        If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Dim error_message = "Requested data not found."
            If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
                'error condition was set in procedure
                error_message = cmd.Parameters("errorCondition").Value.ToString()
            End If
            Throw New Exception(error_message)
        Else
            return_value = cmd.Parameters("retVal").Value
            return_value.Seek(0, IO.SeekOrigin.Begin)

            Try
                doc.Load(return_value)
            Catch ex As Exception
                Dim excp As String
                excp = ex.Message()
                excp = ex.StackTrace()  ' What's the point of these three lines?
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
            End Try

            CloseConnection()

            error_node = doc.SelectSingleNode("//ERROR")

            If error_node IsNot Nothing Then
                ' Throw New Exception(error_node.InnerText)
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)   ' Overwriting errors is always useful.
            End If

        End If
        Return doc
    End Function

    Public Function BeginTransaction() As TransactionContext
        Dim oracle_transaction As OracleTransaction

        OpenConnection()
        oracle_transaction = cn.BeginTransaction
        Dim context As New TransactionContext(cn, oracle_transaction)

        Return context
    End Function

    Public Sub CommitTransaction(ByRef context As TransactionContext)
        context.CommitTransaction()
    End Sub

    Public Sub RollbackTransaction(ByRef context As TransactionContext)
        context.RollbackTransaction()
    End Sub

    Public ReadOnly Property Context() As TransactionContext
        Get
            Return oContext
        End Get
    End Property
End Class
