Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class ShoppingCartDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim debugMessage As String = $"ShoppingCartDataManager.Store - START at {Date.Now.ToShortTimeString}.{Environment.NewLine}"     ' TODO: Remove debug statements.
        Try
            If (data Is Nothing) Then ServicesPlusException.Utilities.LogDebug("ShoppingCartDataManager.Store - data parameter is null.")
            Dim cart As ShoppingCart = CType(data, ShoppingCart)
            Dim cmd As New OracleCommand

            If (Not update) Or (update And cart.Dirty) Then
                If update Then
                    If cart.Action = CoreObject.ActionType.DELETE Then
                        cmd.CommandText = "deleteShoppingCart"
                    Else
                        cmd.CommandText = "updateShoppingCart"
                    End If
                    cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                    cmd.Parameters("xSequenceNumber").Value = cart.SequenceNumber
                Else
                    cmd.CommandText = "addShoppingCart"
                End If
                debugMessage &= $"- CommandText = {cmd.CommandText}, Sequence = {cart.SequenceNumber}.{Environment.NewLine}"

                cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
                cmd.Parameters("xCustomerID").Value = cart.Customer.CustomerID
                cmd.Parameters.Add("xExpirationDate", OracleDbType.Date)
                cmd.Parameters("xExpirationDate").Value = cart.ExpirationDate
                cmd.Parameters.Add("xDateCreated", OracleDbType.Date)
                cmd.Parameters("xDateCreated").Value = cart.DateCreated
                cmd.Parameters.Add("xPONumber", OracleDbType.Varchar2, 50)
                cmd.Parameters("xPONumber").Value = cart.PurchaseOrderNumber
                debugMessage &= $"- Customer ID = {cart.Customer.CustomerID}, Cart Sequence = {cart.SequenceNumber}, Cart PO = {cart.PurchaseOrderNumber}.{Environment.NewLine}"

                If cart IsNot Nothing Then
                    cmd.Parameters.Add("xCartType", OracleDbType.Varchar2, 20)
                    If cart.Type = ShoppingCart.enumCartType.Other Then
                        cmd.Parameters("xCartType").Value = "Ship"
                    Else
                        cmd.Parameters("xCartType").Value = "Download"
                    End If
                End If

                cmd.Parameters.Add("retVal", OracleDbType.Int64, ParameterDirection.Output)

                debugMessage &= $"- Calling ExecuteCMD.{Environment.NewLine}"
                ExecuteCMD(cmd, context)

                cart.Action = CoreObject.ActionType.RESTORE

                If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    cart.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
                    debugMessage &= $"- New Cart Sequence = {cart.SequenceNumber}.{Environment.NewLine}"
                End If
            End If

            'store cartitems
            cart.ReturnDeletedObjects = True

            debugMessage &= "- Storing Shopping Cart Line Items."
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                StoreShoppingCartItem(item, context)
            Next

            cart.ReturnDeletedObjects = False
        Catch ex As Exception
            ServicesPlusException.Utilities.LogMessages(debugMessage)
            Throw ex
        End Try
    End Sub

    Private Sub StoreShoppingCartItem(ByVal item As ShoppingCartItem, ByVal context As TransactionContext)
        Dim cmd As New OracleCommand
        Dim update As Boolean = False
        Dim debugMessage As String = $"ShoppingCartDataManager.StoreShoppingCartItem START {Environment.NewLine}"

        Try
            Select Case item.Action
                Case CoreObject.ActionType.ADD
                    cmd.CommandText = "addShoppingCartItem"
                Case CoreObject.ActionType.RESTORE
                    If Not item.Dirty Then Return
                    cmd.CommandText = "updateShoppingCartItem"
                    update = True
                Case CoreObject.ActionType.DELETE
                    cmd.CommandText = "deleteShoppingCartItem"
                    update = True
            End Select

            debugMessage &= $"- CommandText = {cmd.CommandText}, Customer = {item.ShoppingCart?.Customer?.CustomerID}, Sequence = {item.ShoppingCart?.SequenceNumber}.{Environment.NewLine}"
            cmd.Parameters.Add("xCustomerID", OracleDbType.Int32, item.ShoppingCart.Customer.CustomerID, ParameterDirection.Input)
            cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32, item.ShoppingCart.SequenceNumber, ParameterDirection.Input)

            If update Then
                debugMessage &= $"- Line Number = {item.LineNumber}.{Environment.NewLine}"
                cmd.Parameters.Add("xLineNumber", OracleDbType.Int32, item.LineNumber, ParameterDirection.Input)
            Else
                debugMessage &= $"- PartNumber = {item.Product?.PartNumber}, ItemOriginID = {item.Origin}.{Environment.NewLine}"
                cmd.Parameters.Add("PartNumber", OracleDbType.Varchar2, 50, item.Product.PartNumber, ParameterDirection.Input)
                cmd.Parameters.Add("PartDescription", OracleDbType.Varchar2, 50, item.Product.Description, ParameterDirection.Input)
                cmd.Parameters.Add("ListPrice", OracleDbType.Double, item.Product.ListPrice, ParameterDirection.Input)
                cmd.Parameters.Add("ItemOriginID", OracleDbType.Int32, item.Origin, ParameterDirection.Input)
                cmd.Parameters.Add("xProductTypeID", OracleDbType.Int32)

                Select Case item.Product.Type
                    Case Product.ProductType.Part
                        cmd.Parameters("xProductTypeID").Value = Product.ProductType.Part
                        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Part).Model, ParameterDirection.Input)
                    Case Product.ProductType.Kit
                        cmd.Parameters("xProductTypeID").Value = Product.ProductType.Kit
                        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Kit).Model, ParameterDirection.Input)
                    Case Product.ProductType.ExtendedWarranty
                        cmd.Parameters("xProductTypeID").Value = Product.ProductType.ExtendedWarranty
                        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                        cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)

                        If item.Action = CoreObject.ActionType.ADD Then
                            cmd.Parameters.Add("xEWModelName", OracleDbType.Varchar2, 30)
                            cmd.Parameters("xEWModelName").Value = CType(item.Product.SoftwareItem, ExtendedWarrantyModel).EWModelNumber

                            cmd.Parameters.Add("xEWSerialNumber", OracleDbType.Varchar2, 30)
                            cmd.Parameters("xEWSerialNumber").Value = CType(item.Product.SoftwareItem, ExtendedWarrantyModel).EWSerialNumber

                            cmd.Parameters.Add("xEWPurchaseDate", OracleDbType.Date)
                            cmd.Parameters("xEWPurchaseDate").Value = CType(item.Product.SoftwareItem, ExtendedWarrantyModel).EWPurchaseDate
                        End If
                    Case Else
                        Select Case item.Product.SoftwareItem.Type
                            Case Product.ProductType.Certificate
                                cmd.Parameters("xProductTypeID").Value = Product.ProductType.Certificate
                                cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                                cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)
                            Case Product.ProductType.ClassRoomTraining
                                cmd.Parameters("xProductTypeID").Value = Product.ProductType.ClassRoomTraining
                                cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                                cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)
                            Case Product.ProductType.ComputerBasedTraining
                                cmd.Parameters("xProductTypeID").Value = Product.ProductType.ComputerBasedTraining
                                cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                                cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)
                            Case Product.ProductType.OnlineTraining
                                cmd.Parameters("xProductTypeID").Value = Product.ProductType.OnlineTraining
                                cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                                cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)
                            Case Product.ProductType.Software
                                cmd.Parameters("xProductTypeID").Value = Product.ProductType.Software
                                cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 20, CType(item.Product, Software).ModelNumber, ParameterDirection.Input)
                                cmd.Parameters.Add("xSoftwareDownloadFileName", OracleDbType.Varchar2, 50, CType(item.Product, Software).DownloadFileName, ParameterDirection.Input)
                        End Select
                End Select
            End If

            debugMessage &= $"- Quantity = {item.Quantity}, NumberOfPartsAvailable = {item.NumberOfPartsAvailable}.{Environment.NewLine}"
            cmd.Parameters.Add("xQuantity", OracleDbType.Int32, item.Quantity, ParameterDirection.Input)
            cmd.Parameters.Add("xYourPrice", OracleDbType.Double, item.YourPrice, ParameterDirection.Input)
            cmd.Parameters.Add("xNumberOfPartsAvailable", OracleDbType.Int32, item.NumberOfPartsAvailable, ParameterDirection.Input)
            'cmd.Parameters.Add("xDownloadItem", OracleDbType.Int32, item.DeliveryChannel, ParameterDirection.Input)

            If Not update Then
                cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)
            End If

            ExecuteCMD(cmd, context)

            If Not update Then
                If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                    item.LineNumber = cmd.Parameters("retVal").Value.ToString()
                End If

                item.Action = CoreObject.ActionType.RESTORE
            End If
        Catch ex As Exception
            ServicesPlusException.Utilities.LogMessages(debugMessage)
            Throw ex
        End Try
    End Sub

    Public Function GetCustomerSavedCarts(ByVal customerid As Long) As DataSet
        Dim cmd As New OracleCommand("SHOPPINGCARTMANAGER.GETCUSTOMERSAVEDCARTS")

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32, customerid, ParameterDirection.Input)
        cmd.Parameters.Add("xCUSTOMERSAVEDCARTS", OracleDbType.RefCursor, ParameterDirection.Output)

        Return GetResults(cmd, True)
    End Function


    Public Function GetCustomerShoppingCarts(ByVal customer As Customer, Optional ByVal Sequencenumber As Integer = 0) As ShoppingCart()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cart As ShoppingCart
        Dim cart_list As New ArrayList
        Dim cmd As New OracleCommand

        If Sequencenumber = 0 Then
            cmd.CommandText = "getCustomerShoppingCarts"
            cmd.Parameters.Add("xUserId", OracleDbType.Char)
            cmd.Parameters("xUserId").Value = customer.CustomerID
        Else
            cmd.CommandText = "getCustomerShoppingCart"
            cmd.Parameters.Add("xUserId", OracleDbType.Char)
            cmd.Parameters("xUserId").Value = customer.CustomerID
            cmd.Parameters.Add("xSequencenumber", OracleDbType.Int32)
            cmd.Parameters("xSequencenumber").Value = Sequencenumber
        End If

        doc = GetResults(cmd)
        nodes = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            cart = New ShoppingCart(customer) With {
                .SequenceNumber = node("SEQUENCENUMBER").InnerText,
                .ExpirationDate = node("EXPIRATIONDATE").InnerText,
                .DateCreated = node("DATECREATED").InnerText,
                .PurchaseOrderNumber = If(node("PONUMBER")?.InnerText, ""),
                .Action = Account.ActionType.RESTORE
            }
            cart_list.Add(cart)
        Next

        Dim carts(cart_list.Count - 1) As ShoppingCart
        cart_list.CopyTo(carts)

        'restore items for each cart
        For Each c As ShoppingCart In carts
            GetCustomerShoppingCartItems(c)
        Next

        Return carts
    End Function

    Public Sub GetCustomerShoppingCartItems(ByRef cart As ShoppingCart)
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCustomerShoppingCartItems")

        cmd.Parameters.Add("xCustomerId", OracleDbType.Varchar2, 50, cart.Customer.CustomerID, ParameterDirection.Input)
        cmd.Parameters.Add("xSequenceNumber", OracleDbType.Varchar2, 50, cart.SequenceNumber, ParameterDirection.Input)

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim item As ShoppingCartItem
        Dim product As Product
        Dim cart_list As New ArrayList
        Dim j As Integer
        Dim number_of_items_available As Integer
        Dim your_price As Double

        For j = 0 To nodes.Count - 1
            product = New Part()        ' 2018-10-04 ASleight - Added to remove null reference warning. Object should always be recast below, though.
            Dim product_type As String
            product_type = nodes(j)("PRODUCTTYPEID").InnerText

            elem = nodes(j)("MODELNUMBER")

            Select Case product_type
                Case "0"
                    product = New Part
                    If elem IsNot Nothing Then
                        CType(product, Part).Model = elem.InnerText
                    End If
                Case "1", "5", "3", "4", "6"
                    product = New Software
                    If elem IsNot Nothing Then
                        CType(product, Software).ModelNumber = elem.InnerText
                    End If

                    elem = nodes(j)("PRODUCTTYPEID")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, Software).Type = Convert.ToInt16(elem.InnerText)
                    End If

                    elem = nodes(j)("SOFTWAREDOWNLOADFILENAME")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, Software).DownloadFileName = elem.InnerText
                    End If

                    elem = nodes(j)("URL")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, Software).TrainingURL = elem.InnerText
                    End If
                Case "8"
                    product = New ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
                    If elem IsNot Nothing Then
                        CType(product, Software).ModelNumber = elem.InnerText
                    End If

                    elem = nodes(j)("SOFTWAREDOWNLOADFILENAME")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, Software).DownloadFileName = elem.InnerText
                    End If

                    elem = nodes(j)("EWDATEPURCHASED")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, ExtendedWarrantyModel).EWPurchaseDate = Convert.ToDateTime(elem.InnerText)
                    End If

                    elem = nodes(j)("EWMODELPURCHASED")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, ExtendedWarrantyModel).EWModelNumber = elem.InnerText
                    End If

                    elem = nodes(j)("EWSERIALNUMBER")
                    If elem IsNot Nothing Then
                        'cart.ContainsDownloadableItems = True
                        CType(product, ExtendedWarrantyModel).EWSerialNumber = elem.InnerText
                    End If

                Case "2"
                    product = New Kit
                    If elem IsNot Nothing Then
                        CType(product, Kit).Model = elem.InnerText
                    End If
            End Select

            product.PartNumber = nodes(j)("PARTNUMBER").InnerText
            product.Description = nodes(j)("PARTDESCRIPTION").InnerText
            product.ListPrice = nodes(j)("LISTPRICE").InnerText
            number_of_items_available = nodes(j)("NUMBEROFPARTSAVAILABLE").InnerText
            your_price = nodes(j)("YOURPRICE").InnerText

            item = New ShoppingCartItem(product, number_of_items_available, your_price, cart)

            elem = nodes(j)("CARTTYPE") 'should rename to DeliveryChannel
            If elem IsNot Nothing Then
                If elem.InnerText = "Ship" Then
                    item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Ship
                ElseIf elem.InnerText = "Download" Then
                    item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Download
                Else
                    item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.DownloadAndShip
                End If
            End If

            'rely on local information
            item.LineNumber = nodes(j)("LINENUMBER").InnerText
            item.Quantity = nodes(j)("QUANTITY").InnerText
            item.Origin = nodes(j)("ITEMORIGINID").InnerText

            item.Action = CoreObject.ActionType.RESTORE
            cart.AddShoppingCartItem(item)
        Next
    End Sub

    Public Function GetConflictParts(ByVal sParts As String, Optional ByRef objGlobalData As GlobalData = Nothing) As DataSet
        Dim cmd As New OracleCommand("pfind.getnewparts")

        cmd.Parameters.Add("p_partids", OracleDbType.Varchar2, 500, sParts, ParameterDirection.Input)
        If objGlobalData IsNot Nothing Then
            cmd.Parameters.Add("p_salesorg", OracleDbType.Varchar2, 10, objGlobalData.SalesOrganization, ParameterDirection.Input)
        End If
        cmd.Parameters.Add("rc_newparts", OracleDbType.RefCursor, ParameterDirection.Output)

        Return GetResults(cmd, True)
    End Function
End Class
