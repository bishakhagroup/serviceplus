
Imports System.Xml
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.ServicesPLUS.Core
Imports ServicesPlusException
Imports Oracle.ManagedDataAccess.Types

Public Class SISDataManager
    Inherits DataManager

    Public Function GetKits(ByRef model_number As String, ByRef part_number As String, ByRef description As String) As Kit()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim kit As New Kit
        Dim kit_model As String = ""
        Dim kit_list As New ArrayList
        Dim kit_item As KitItem
        Dim cmd As New OracleCommand("searchSISKits")
        Dim debugMsg As String = $"SISDataManager.GetKits - START at {Date.Now.ToShortTimeString()}.{Environment.NewLine}"

        Try     ' FOR DEBUG ONLY
            model_number = model_number.Replace("-", "").Replace("/", "")
            part_number = part_number.Replace("-", "").Replace("/", "")
            description = description.Replace("-", "").Replace("/", "")

            cmd.Parameters.Add("XModelNumber", OracleDbType.Char, model_number.ToUpper() + "%", ParameterDirection.Input)
            cmd.Parameters.Add("XPartNumber", OracleDbType.Char, part_number.ToUpper() + "%", ParameterDirection.Input)
            cmd.Parameters.Add("XModelDescription", OracleDbType.Char, description.ToUpper() + "%", ParameterDirection.Input)
            debugMsg &= $"- Parameters: Prefix = {cmd.Parameters("XModelNumber").Value}, Part = {cmd.Parameters("XPartNumber").Value}, Desc = {cmd.Parameters("XModelDescription").Value}{Environment.NewLine}"

            doc = GetResults(cmd)
            nodes = doc.DocumentElement.ChildNodes
            debugMsg &= $"- GetResults completed. Node count = {nodes.Count}{Environment.NewLine}"

            For Each node As XmlNode In nodes
                If kit_model = node("KITMODELNAME").InnerText Then  ' It's a "child" item of the parent Kit. Attach to the parent.
                    debugMsg &= $"- Child item found for Kit {kit_model}. Part Number: {node("PARTNUMBER")?.InnerText}, Part Category: {node("PARTCATEGORY")?.InnerText}{Environment.NewLine}"
                    kit_item = New KitItem With {
                        .Quantity = If(node("QUANTITY")?.InnerText, "1"),
                        .Product = New Part With {
                            .PartNumber = If(node("PARTNUMBER")?.InnerText, ""),
                            .Model = If(node("MODELNUMBER")?.InnerText, ""),
                            .Description = If(node("PARTDESCRIPTION")?.InnerText, ""),
                            .Category = IIf(node("PARTCATEGORY")?.InnerText Is Nothing, Nothing, New ProductCategory(node("PARTCATEGORY")?.InnerText)), ' This can be a null value in the DB.
                            .ProgramCode = If(node("PROGRAMCODE")?.InnerText, ""),
                            .CoreCharge = IIf(String.IsNullOrWhiteSpace(node("FRBCORECHARGE")?.InnerText), 0.0, Convert.ToDouble(node("FRBCORECHARGE")?.InnerText)),
                            .ReplacementPartNumber = If(node("REPLACEMENTPARTNUMBER")?.InnerText, ""),
                            .ListPrice = IIf(String.IsNullOrWhiteSpace(node("LISTPRICE")?.InnerText), 0.0, Convert.ToDouble(node("LISTPRICE")?.InnerText)),
                            .BillingOnly = If(node("BILLINGONLY")?.InnerText, ""),
                            .SalesText = If(node("SALESTEXT")?.InnerText, "")
                        }
                    }
                    debugMsg &= $"--- Child item populated. Part = {kit_item.Product.PartNumber}, Quantity = {kit_item.Quantity}{Environment.NewLine}"

                    If kit_item.Quantity > 1 Then
                        kit_item.Product.ListPrice = kit_item.Product.ListPrice * kit_item.Quantity
                    End If

                    kit.AddKitItem(kit_item)
                Else
                    debugMsg &= $"- New Kit found.{Environment.NewLine}"
                    kit = New Kit With {
                        .Model = node("KITMODELNAME").InnerText,
                        .PartNumber = node("KITMODELNAME").InnerText,
                        .DiscountPercentage = If(node("DISCOUNTPERCENT")?.InnerText, ""),
                        .Description = If(node("DESCRIPTION")?.InnerText, ""),
                        .TheKitType = IIf((node("SISKITTYPEID")?.InnerText = "MJ"), Kit.KitType.Major, Kit.KitType.Minor)
                    }
                    debugMsg &= $"--- Kit Model = {kit.Model}, Part = {kit.PartNumber}{Environment.NewLine}"

                    kit_item = New KitItem With {
                        .Quantity = If(node("QUANTITY")?.InnerText, ""),
                        .Product = New Part With {
                            .PartNumber = If(node("PARTNUMBER")?.InnerText, ""),
                            .Model = If(node("MODELNUMBER")?.InnerText, ""),
                            .Description = If(node("PARTDESCRIPTION")?.InnerText, ""),
                            .Category = IIf(node("PARTCATEGORY")?.InnerText Is Nothing, Nothing, New ProductCategory(node("PARTCATEGORY")?.InnerText)), ' This can be a null value in the DB.
                            .ProgramCode = If(node("PROGRAMCODE")?.InnerText, ""),
                            .CoreCharge = IIf(String.IsNullOrWhiteSpace(node("FRBCORECHARGE")?.InnerText), 0.0, Convert.ToDouble(node("FRBCORECHARGE")?.InnerText)),
                            .ReplacementPartNumber = If(node("REPLACEMENTPARTNUMBER")?.InnerText, ""),
                            .ListPrice = IIf(String.IsNullOrWhiteSpace(node("LISTPRICE")?.InnerText), 0.0, Convert.ToDouble(node("LISTPRICE")?.InnerText)),
                            .BillingOnly = If(node("BILLINGONLY")?.InnerText, ""),
                            .SalesText = If(node("SALESTEXT")?.InnerText, "")
                        }
                    }
                    debugMsg &= $"- KitItem Part = {kit_item.Product.PartNumber} {Environment.NewLine}"

                    If kit_item.Quantity > 1 Then
                        kit_item.Product.ListPrice = kit_item.Product.ListPrice * kit_item.Quantity
                    End If

                    kit.AddKitItem(kit_item)
                    kit_list.Add(kit)
                    kit_model = kit.Model   ' Store the model so we can find child elements in future iterations.
                End If
            Next

            debugMsg &= $"- Parsing complete. Copying to array and returning.{Environment.NewLine}"
            Dim kits(kit_list.Count - 1) As Kit
            kit_list.CopyTo(kits)

            Return kits
        Catch ex As Exception
            Utilities.LogDebug(debugMsg)
            Utilities.WrapExceptionforUI(ex)
            Return Nothing
        End Try
    End Function

    Public Function SearchSISMaster(ByVal model_number As String, ByVal part_number As String, ByVal description As String, ByRef sMaxiumValue As String, ByRef objGlobalData As GlobalData) As Part()
        Dim part_list As New ArrayList
        Dim part As Part
        Dim objAdapter As OracleDataAdapter
        Dim objDataset As New DataSet()

        Dim cmd As New OracleCommand("search_part_pkg.SEARCHPARTS") With {
            .CommandType = CommandType.StoredProcedure,
            .Connection = dbConnection,
            .BindByName = True
        }

        model_number = model_number.Replace("-", "").Replace("/", "")
        part_number = part_number.Replace("-", "%").Replace("/", "%")
        description = description.Replace("-", "%").Replace("/", "%")

        cmd.Parameters.Add("xModelNumber", OracleDbType.Char)
        cmd.Parameters("xModelNumber").Value = model_number.ToUpper()

        cmd.Parameters.Add("xPartNumber", OracleDbType.Char)
        cmd.Parameters("xPartNumber").Value = part_number.ToUpper()

        '2016-04-20 ASleight Adding missing parameter to resolve frequent errors
        cmd.Parameters.Add("xSearchTraining", OracleDbType.Int32)
        cmd.Parameters("xSearchTraining").Value = 0

        cmd.Parameters.Add("xDescription", OracleDbType.Char)
        cmd.Parameters("xDescription").Value = "%" + description.ToUpper()

        cmd.Parameters.Add("p_Sales_Org", OracleDbType.Char)
        cmd.Parameters("p_Sales_Org").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("p_Language_Id", OracleDbType.Char)
        cmd.Parameters("p_Language_Id").Value = objGlobalData.Language

        Dim outparam As New OracleParameter("errorCondition", OracleDbType.Varchar2, 100)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)

        cmd.Parameters.Add("search_refcur", OracleDbType.RefCursor, ParameterDirection.Output)

        objAdapter = New OracleDataAdapter(cmd)
        objAdapter.Fill(objDataset)
        ' 2018-03-29 - ASleight - The new Oracle API returns null string parameters as the string "null". Comparing OracleString to DbNull.Value is no longer valid.
        If Not CType(cmd.Parameters("errorCondition")?.Value, INullable).IsNull Then
            'error condition was set in procedure
            Dim error_message As String = cmd.Parameters("errorCondition").Value.ToString()
            ' Throw New Exception(error_message)
            '' Throw New ServicesPlusBusinessException(error_message)
            sMaxiumValue = error_message
        End If

        For Each row As DataRow In objDataset.Tables(0).Rows
            part = New Part With {
                .PartNumber = row("PARTNUMBER")?.ToString(),
                .ReplacementPartNumber = row("REPLACEMENTPARTNUMBER")?.ToString(),
                .Model = row("MODELNUMBER")?.ToString(),
                .Description = row("DESCRIPTION")?.ToString(),
                .ProgramCode = row("PROGRAMCODE")?.ToString(),
                .SalesText = row("SALESTEXT")?.ToString(),
                .Category = New ProductCategory(row("PARTCATEGORY").ToString()),
                .BillingOnly = (row("BILLINGONLY")?.ToString().Contains("Y"))
            }
            If Not String.IsNullOrEmpty(row("FRBCORECHARGE").ToString()) Then
                part.CoreCharge = Convert.ToDouble(row("FRBCORECHARGE").ToString())
            End If
            If Not String.IsNullOrEmpty(row("LISTPRICE").ToString()) Then
                part.ListPrice = Convert.ToDouble(row("LISTPRICE")?.ToString())
            End If
            part_list.Add(part)
        Next

        Dim parts(part_list.Count - 1) As Part
        part_list.CopyTo(parts)

        Return parts
    End Function

    Public Function getSISPartsInRange(ByVal groupid As Long, ByVal startIndex As Integer, ByVal length As Integer, ByRef totalCount As Integer) As Part()
        Dim myAdaptor As New OracleDataAdapter
        Dim objDataset As New DataSet
        Dim part_list As New ArrayList
        Dim part As Part
        Dim cmd As New OracleCommand("pfind.GetSISPartsInRange1") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        cmd.Parameters.Add("start_index", OracleDbType.Int32)
        cmd.Parameters("start_index").Value = startIndex

        cmd.Parameters.Add("length_index", OracleDbType.Int32)
        cmd.Parameters("length_index").Value = length

        cmd.Parameters.Add("xGroupID", OracleDbType.Int32)
        cmd.Parameters("xGroupID").Value = groupid

        cmd.Parameters.Add("p_count", OracleDbType.Int32, ParameterDirection.Output)
        cmd.Parameters.Add("rc_parts", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection

        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(objDataset)
            For Each row As DataRow In objDataset.Tables(0)?.Rows
                part = New Part With {
                    .Model = row("modelnumber").ToString(),
                    .PartNumber = row("PARTREQUESTED").ToString(),
                    .ReplacementPartNumber = row("PARTSUPPLIED").ToString(),
                    .Description = row("DESCRIPTION").ToString(),
                    .ListPrice = Convert.ToDouble(If(row("LISTPRICE")?.ToString(), "0.0"))
                }
                part_list.Add(part)
            Next
            Dim parts(part_list.Count - 1) As Part
            part_list.CopyTo(parts)

            Return parts
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try
    End Function

    Public Function GetSISPartsInRange(ByVal groupid As Long) As Part()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim part_list As New ArrayList
        Dim part As Part
        Dim cmd As New OracleCommand("GetSISPartsInRange")
        cmd.Parameters.Add("xGroupID", OracleDbType.Int32, groupid, ParameterDirection.Input)

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            part = New Part With {
                .PartNumber = node("PARTNUMBER")?.ToString(),
                .ReplacementPartNumber = node("REPLACEMENTPARTNUMBER")?.ToString(),
                .Model = node("MODELNUMBER")?.ToString(),
                .Description = node("DESCRIPTION")?.ToString(),
                .ProgramCode = node("PROGRAMCODE")?.ToString(),
                .SalesText = node("SALESTEXT")?.ToString(),
                .Category = New ProductCategory(node("PARTCATEGORY").ToString()),
                .CoreCharge = Convert.ToDouble(If(node("FRBCORECHARGE")?.ToString(), "0.0")),
                .ListPrice = Convert.ToDouble(If(node("LISTPRICE")?.ToString(), "0.0")),
                .BillingOnly = (node("BILLINGONLY")?.ToString().Contains("Y"))
            }
            part_list.Add(part)
        Next

        Dim parts(part_list.Count - 1) As Part
        part_list.CopyTo(parts)

        Return parts
    End Function

    Public Function GetStateBySalesOrg(ByRef objGlobalData As GlobalData) As StatesDetail()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("USERS.GetStates")

        ' Add Country Code as a paramter 
        cmd.Parameters.Add("xSales_Org", OracleDbType.Varchar2) '2016-04-20 ASleight removed ", 50" to fix mismatched parameter types
        cmd.Parameters("xSales_Org").Value = objGlobalData.SalesOrganization

        ''Dim xStateDetails As New OracleParameter("xStateDetails", OracleDbType.RefCursor)
        ''xStateDetails.Direction = ParameterDirection.Output
        ''cmd.Parameters.Add(xStateDetails)

        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim statesdetails_list As New ArrayList
        Dim states As StatesDetail

        For Each node As XmlNode In nodes
            states = New StatesDetail With {
                .StateName = If(node("STATE")?.InnerText, ""),
                .StateAbbr = If(node("ABBREVIATION")?.InnerText, "")
            }
            statesdetails_list.Add(states)
        Next

        Dim allstates(statesdetails_list.Count - 1) As StatesDetail
        statesdetails_list.CopyTo(allstates)

        Return allstates
    End Function

    Public Function GetStateDetails() As StatesDetail()
        Dim doc As XmlDocument
        Dim statesdetails_list As New ArrayList
        Dim states As StatesDetail
        Dim cmd As New OracleCommand("GETSTATES")

        ' Add Country Code as a paramter 
        cmd.Parameters.Add("country", OracleDbType.Varchar2)
        cmd.Parameters("country").Value = "USA"

        doc = GetResults(cmd)
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            states = New StatesDetail With {
                .StateName = If(node("STATE")?.InnerText, ""),
                .StateAbbr = If(node("ABBREVIATION")?.InnerText, "")
            }
            statesdetails_list.Add(states)
        Next

        Dim allstates(statesdetails_list.Count - 1) As StatesDetail
        statesdetails_list.CopyTo(allstates)

        Return allstates
    End Function

    Public Function GetStateDetailsByCountry(ByVal Country As String) As DataSet
        Dim ds As DataSet
        Dim cmd As New OracleCommand("USERS.GetStateData")

        cmd.Parameters.Add("xCountryID", OracleDbType.Varchar2, 50, Country, ParameterDirection.Input)
        cmd.Parameters.Add("xStateDetails", OracleDbType.RefCursor, ParameterDirection.Output)

        ds = GetResults(cmd, True)

        Return ds
    End Function

    Public Function GetCountryDetails() As DataSet
        Dim ds As DataSet
        Dim cmd As New OracleCommand("USERS.GetCountrydata")

        cmd.Parameters.Add("xCountryDetails", OracleDbType.RefCursor, ParameterDirection.Output)

        ds = GetResults(cmd, True)

        Return ds
    End Function

    Public Function GetCountryNameByCode(ByRef objGlobalData As GlobalData, Optional ByRef CountryCode As String = "") As String
        Dim doc As XmlDocument
        Dim elem As XmlElement
        Dim partgroups_list As New ArrayList
        Dim strCountryName As String = String.Empty
        Dim j As Integer
        Dim cmd As New OracleCommand("USERS.GetCountry")

        cmd.Parameters.Add("xSales_Org", OracleDbType.Varchar2, 50)
        cmd.Parameters("xSales_Org").Value = objGlobalData.SalesOrganization

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        For Each node As XmlNode In nodes
            elem = nodes(j)("COUNTRYNAME")
            If Not elem Is Nothing Then
                strCountryName = elem.InnerText
            End If
            elem = nodes(j)("COUNTRYCODE")
            If Not elem Is Nothing Then
                CountryCode = elem.InnerText
            End If
        Next

        Return strCountryName
    End Function

    Public Function GetPartsGroupByPageNumber(ByVal PageNo As Integer) As PartsCatalogGroup()
        Dim doc As XmlDocument
        Dim partgroups_list As New ArrayList
        Dim partgroups As PartsCatalogGroup
        Dim cmd As New OracleCommand("GetPartsGroupByPgNumber")
        cmd.Parameters.Add("xPageNumber", OracleDbType.Int32, PageNo, ParameterDirection.Input)

        doc = GetResults(cmd)
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            partgroups = New PartsCatalogGroup With {
                .GroupID = node("GROUPID").InnerText,
                .RangeStart = node("RANGESTART").InnerText,
                .RangeEnd = node("RANGEEND").InnerText
            }

            If partgroups.RangeStart IsNot Nothing And partgroups.RangeEnd IsNot Nothing Then
                partgroups.RangeStart = partgroups.RangeStart + " to " + partgroups.RangeEnd
            End If

            partgroups_list.Add(partgroups)
        Next

        Dim allpartgroupsinpage(partgroups_list.Count - 1) As PartsCatalogGroup
        partgroups_list.CopyTo(allpartgroupsinpage)

        Return allpartgroupsinpage
    End Function

    ' 2018-10-18 ASleight - No changes, but this returns all pages up to the current one.
    '   Should be fixed at the DB side eventually, but doesn't cause any true issues.
    Public Function GetModelsGroupByPageNumber(ByVal PageNo As Integer) As ModelsCatalogGroup()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetServiceModelsByPageNo")
        cmd.Parameters.Add("xPageNumber", OracleDbType.Int32)
        cmd.Parameters("xPageNumber").Value = PageNo

        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim modelgroups_list As New ArrayList
        Dim modelgroups As ModelsCatalogGroup
        Dim j As Integer

        For j = 0 To nodes.Count - 1 Step 2
            modelgroups = New ModelsCatalogGroup

            elem = nodes(j)("RNGNUMBER")
            If elem IsNot Nothing Then
                modelgroups.GroupID = elem.InnerText
            End If

            elem = nodes(j)("MODELNAME")
            If elem IsNot Nothing Then
                modelgroups.Range = elem.InnerText
            End If

            If (j + 1) <= nodes.Count - 1 Then
                elem = nodes(j + 1)("MODELNAME")
                If elem IsNot Nothing Then
                    modelgroups.Range = modelgroups.Range + " to " + elem.InnerText
                End If
            End If

            modelgroups_list.Add(modelgroups)
        Next

        Dim allmodelgroupsinpage(modelgroups_list.Count - 1) As ModelsCatalogGroup
        'Dim allmodelgroupsinpage(iTotMod) As ModelsCatalogGroup
        modelgroups_list.CopyTo(allmodelgroupsinpage)

        Return allmodelgroupsinpage
    End Function

    Public Function GetModelsDetailsBySearch(ByVal p_ModelName As String, ByRef objGlobalData As GlobalData, ByRef lstConflicts As List(Of String)) As ModelDetails()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("SEARCHSERVICEMODELS")
        cmd.Parameters.Add("p_SearchModel", OracleDbType.Varchar2)
        cmd.Parameters("p_SearchModel").Value = p_ModelName

        cmd.Parameters.Add("P_Sales_Org", OracleDbType.Varchar2) '2016-04-20 ASleight updated from .Char type to reflect expected parameters
        cmd.Parameters("P_Sales_Org").Value = objGlobalData.SalesOrganization

        cmd.Parameters.Add("P_Language_Id", OracleDbType.Varchar2) '2016-04-20 ASleight updated from .Char type to reflect expected parameters
        cmd.Parameters("P_Language_Id").Value = objGlobalData.Language

        doc = GetResults_SearchServiceModels(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim modelgroups_list As New ArrayList
        Dim modelgroups As ModelDetails
        Dim j As Integer
        'Added by Sneha on 15/11/2013
        lstConflicts = New List(Of String)
        For i As Integer = 0 To doc.GetElementsByTagName("OLD_MATERIAL").Count - 1
            If nodes(i).Item("OLD_MATERIAL") IsNot Nothing Then
                lstConflicts.Add(nodes(i).Item("OLD_MATERIAL").InnerText + ":" + nodes(i).Item("NEW_MATERIAL").InnerText)
            End If
        Next
        'Fixed bug by Sneha on 6th Feb 2014
        For j = 0 To doc.GetElementsByTagName("Table1").Count - 1
            modelgroups = New ModelDetails
            nodes = doc.GetElementsByTagName("Table1")

            elem = nodes(j).Item("MODEL")
            If Not elem Is Nothing Then
                modelgroups.ModelName = elem.InnerText
            End If

            elem = nodes(j).Item("DESCRIPTION")
            If Not elem Is Nothing Then
                modelgroups.ModelDescription = elem.InnerText
            End If

            elem = nodes(j).Item("DISCONT")
            If Not elem Is Nothing Then
                modelgroups.DiscontinuedFlag = elem.InnerText
            End If

            modelgroups_list.Add(modelgroups)
        Next

        Dim allmodelgroupsinpage(modelgroups_list.Count - 1) As ModelDetails
        modelgroups_list.CopyTo(allmodelgroupsinpage)

        Return allmodelgroupsinpage
    End Function

    Public Function ModelDiscCode(ByVal psModelName As String) As String
        Dim doc As XmlDocument
        Dim sDiscCode As String = "NIL"
        Dim cmd As New OracleCommand("GetModelStatus")

        cmd.Parameters.Add("xModelName", OracleDbType.Varchar2)
        cmd.Parameters("xModelName").Value = psModelName

        doc = GetResults(cmd)
        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
        If nodes?.Count > 0 Then
            sDiscCode = nodes(0).ChildNodes(0).InnerText + "," + nodes(0).ChildNodes(1).InnerText
        End If

        Return sDiscCode
    End Function

    Public Function ServiceCenterDetails(ByVal psStateAbbr As String) As String
        Dim doc As XmlDocument
        Dim sResult As String = ""
        Dim cmd As New OracleCommand("GETSERVICECENTER")

        cmd.Parameters.Add("xStateAbbreiation", OracleDbType.Varchar2)
        cmd.Parameters("xStateAbbreiation").Value = psStateAbbr

        doc = GetResults(cmd)

        Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes

        If nodes.Count >= 1 Then
            sResult = nodes(0)("SERVICECENTERNAME").InnerText + "!" + nodes(0)("STATE").InnerText + "!" +
                nodes(0)("PHONE").InnerText + "!" + nodes(0)("REQUESTSTATE").InnerText
        Else
            sResult = "NIL"
        End If

        Return sResult
    End Function
    '7395 Starts
    '----------------------------------------------------------------------------'
    ' function to get service cetner details
    ' @input: State abbreviation code
    ' @output: address like Service ceter name, state and phone joined with !
    '-----------------------------------------------------------------------------'

    'Public Function GetStateAbbrByZip(ByVal inzipcode As String, ByRef context As TransactionContext) As String


    '    Dim cmd1 = New OracleCommand

    '    cmd1.CommandType = CommandType.StoredProcedure
    '    cmd1.CommandText = "GETSTATEABBRBYZIP"
    '    cmd1.Parameters.Add("xzip", OracleDbType.Varchar2, 5)
    '    cmd1.Parameters("xzip").Value = inzipcode

    '    Dim outparam1 As New OracleParameter("xstatecode", OracleDbType.Varchar2, 2)
    '    outparam1.Direction = ParameterDirection.Output
    '    cmd1.Parameters.Add(outparam1)

    '    If cn.State = ConnectionState.Closed Then
    '        cn.Open()
    '        cmd1.Connection = cn
    '    End If

    '    ExecuteCMD(cmd1, context)

    '    Dim outStateAbbr As String
    '    outStateAbbr = Nothing
    '    If Not (cmd1.Parameters("xstatecode").Value Is System.DBNull.Value) Then
    '        outStateAbbr = Convert.ToString(cmd1.Parameters("xstatecode").Value)
    '    End If
    '    Return outStateAbbr

    'End Function
    '7395 Ends
    '--------------------------------------'
    '----------------------------------------------------------------------------'
    ' function to get depot service cetner details
    ' @input: State abbreviation code
    ' @output: address like Service ceter name, state and phone joined with !
    '-----------------------------------------------------------------------------'

    Public Function DepotServiceCenterDetails(ByVal psStateAbbr As String, ByVal pModelName As String) As String
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim elem As XmlElement
        Dim sResult As String = ""
        Dim cmd As New OracleCommand("GETDEPOTSERVICECENTER")

        cmd.Parameters.Add("xStateAbbreiation", OracleDbType.Varchar2)
        cmd.Parameters("xStateAbbreiation").Value = psStateAbbr

        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2)
        cmd.Parameters("xModelNumber").Value = pModelName

        Try
            doc = GetResults(cmd)
            nodes = doc.DocumentElement.ChildNodes

            If nodes?.Count > 0 Then
                elem = nodes(0)("ERRORMSG")
                If Not elem Is Nothing Then
                    Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
                End If

                elem = nodes(0)("STREET")
                If Not elem Is Nothing Then
                    sResult = elem.InnerText
                End If

                elem = nodes(0)("STREET2")
                If Not elem Is Nothing Then
                    sResult = sResult + "!" + elem.InnerText
                End If

                elem = nodes(0)("STREET3")
                If Not elem Is Nothing Then
                    sResult = sResult + "!" + elem.InnerText
                End If

                elem = nodes(0)("CITY")
                If Not elem Is Nothing Then
                    sResult = sResult + "!" + elem.InnerText
                End If

                elem = nodes(0)("STATE")
                If Not elem Is Nothing Then
                    sResult = sResult + ", " + elem.InnerText.ToUpper()
                End If

                elem = nodes(0)("ZIP_CODE")
                If Not elem Is Nothing Then
                    sResult = sResult + " " + elem.InnerText
                End If

                elem = nodes(0)("PHONE")
                If Not elem Is Nothing Then
                    ' ASleight - Updated the below so it's more flexible. It used to butcher any number with more than 10 digits.
                    '            Though honestly, it's silly for the data to be stored formatted, then un-formatted by the DB SP, then formatted again here.
                    Dim phonenumber As String = elem.InnerText                              ' "12345678901" - Stored Proc always returns numbers only.
                    If (phonenumber.Length > 10) Then
                        phonenumber = phonenumber.Insert((phonenumber.Length - 10), " ")     ' "1 2345678901" - Adding space before area code.
                    End If
                    phonenumber = phonenumber.Insert((phonenumber.Length - 7), "-")         ' "1 234-5678901" - Adding dash before prefix.
                    phonenumber = phonenumber.Insert((phonenumber.Length - 4), "-")         ' "1 234-567-8901" - Adding dash before line number.

                    sResult = sResult + "!!Phone: " + phonenumber   'elem.InnerText
                End If

                elem = nodes(0)("ADDRESSID")
                If Not elem Is Nothing Then
                    sResult = sResult + "#" + elem.InnerText
                End If
                elem = nodes(0)("EMAIL")
                If Not elem Is Nothing Then
                    sResult = sResult + "#" + elem.InnerText
                End If
            Else
                sResult = "Address not available."
            End If
        Catch ex As Exception
            If ex.Message = "Requested data not found." Then
                sResult = ""
            Else
                Throw New ServicesPLUSOracleException(ERRORCODE.ora06550)
            End If
        End Try

        Return sResult
    End Function

    Private Function ChangeCase(ByVal pString As String) As String
        Dim str As String = pString.ToLower()
        Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
    End Function

    Public Function GetModelsDetailsByPgNo(ByVal PageNo As Integer) As ModelDetails()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetServiceModelGroup")

        cmd.Parameters.Add("xPageNumber", OracleDbType.Int32)
        cmd.Parameters("xPageNumber").Value = PageNo

        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim modelgroups_list As New ArrayList
        Dim modelgroups As ModelDetails
        Dim j As Integer

        For j = 0 To nodes.Count - 1

            modelgroups = New ModelDetails

            elem = nodes(j)("MODEL")
            If Not elem Is Nothing Then
                modelgroups.ModelName = elem.InnerText
            End If

            elem = nodes(j)("DESCRIPTION")
            If Not elem Is Nothing Then
                modelgroups.ModelDescription = elem.InnerText
            End If

            modelgroups_list.Add(modelgroups)
        Next

        Dim allmodelgroupsinpage(modelgroups_list.Count - 1) As ModelDetails
        modelgroups_list.CopyTo(allmodelgroupsinpage)

        Return allmodelgroupsinpage
    End Function

    Public Function GetPrePartsRange() As PartsCatalogGroup()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetPrePartsRange")

        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim partgroups_list As New ArrayList
        Dim partgroups As PartsCatalogGroup
        Dim j As Integer

        For j = 0 To nodes.Count - 1

            partgroups = New PartsCatalogGroup

            elem = nodes(j)("GROUPID")
            If Not elem Is Nothing Then
                partgroups.GroupID = elem.InnerText
            End If

            elem = nodes(j)("RANGESTART")
            If Not elem Is Nothing Then
                partgroups.RangeStart = elem.InnerText
            End If

            elem = nodes(j)("RANGEEND")
            If Not elem Is Nothing Then
                partgroups.RangeEnd = elem.InnerText
            End If

            ' start of E672
            If Not (partgroups.RangeStart) Is Nothing And Not (partgroups.RangeEnd) Is Nothing Then
                partgroups.RangeStart = partgroups.RangeStart + " to " + partgroups.RangeEnd
            End If
            ' end of E672

            partgroups_list.Add(partgroups)
        Next

        Dim allpartgroupsinpage(partgroups_list.Count - 1) As PartsCatalogGroup
        partgroups_list.CopyTo(allpartgroupsinpage)

        Return allpartgroupsinpage
    End Function

    Public Function GetPartsGroups() As PartsCatalogGroup()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetAllPartsGroup")
        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim partgroups_list As New ArrayList
        Dim partgroups As PartsCatalogGroup
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            partgroups = New PartsCatalogGroup

            elem = nodes(j)("GROUPID")
            If Not elem Is Nothing Then
                partgroups.GroupID = elem.InnerText
            End If

            elem = nodes(j)("RANGESTART")
            If Not elem Is Nothing Then
                partgroups.RangeStart = elem.InnerText
            End If

            elem = nodes(j)("RANGEEND")
            If Not elem Is Nothing Then
                partgroups.RangeEnd = elem.InnerText
            End If

            partgroups_list.Add(partgroups)
        Next

        Dim allpartgroups(partgroups_list.Count - 1) As PartsCatalogGroup
        partgroups_list.CopyTo(allpartgroups)

        Return allpartgroups
    End Function

    Public Function GetModelsGroups() As ModelsCatalogGroup()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetAllPartsGroup")
        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim partgroups_list As New ArrayList
        Dim partgroups As ModelsCatalogGroup
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            partgroups = New ModelsCatalogGroup

            elem = nodes(j)("GROUPID")
            If Not elem Is Nothing Then
                partgroups.GroupID = elem.InnerText
            End If

            elem = nodes(j)("RANGESTART")
            If Not elem Is Nothing Then
                'partgroups.RangeStart = elem.InnerText
            End If

            elem = nodes(j)("RANGEEND")
            If Not elem Is Nothing Then
                'partgroups.RangeEnd = elem.InnerText
            End If

            partgroups_list.Add(partgroups)
        Next

        Dim allpartgroups(partgroups_list.Count - 1) As ModelsCatalogGroup
        partgroups_list.CopyTo(allpartgroups)

        Return allpartgroups
    End Function

    Public Function GetMaxGroupID() As Long
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetMaxGroupID")
        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim partgroupid As Long = 1
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            elem = nodes(j)("GROUPID")
            If Not elem Is Nothing Then
                partgroupid = elem.InnerText
            End If
        Next

        Return partgroupid
    End Function

    Public Function GetModelMaxGroupID() As Long
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("GetMaxServiceModel")

        ' refer to GetMaxGroupID stored proc and create a new one with model catalog related tables, hint: create the column names similar to parts catalog related table
        doc = GetResults(cmd)

        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim elem As XmlElement
        Dim modelgroupid As Long = 1
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            elem = nodes(j)("MAXMODEL")
            If Not elem Is Nothing Then
                modelgroupid = elem.InnerText
            End If
        Next

        Return modelgroupid
    End Function

    Public Function GetKits_PartsFindSearch(ByRef model_number As String, ByRef lstConflicts As List(Of String)) As Kit()
        Dim doc As XmlDocument
        Dim dataset As New DataSet
        Dim xml_data As String
        Dim cmd As New OracleCommand("searchSISKits_new") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        model_number = model_number.Replace("-", "").Replace("/", "").ToUpper()
        cmd.Parameters.Add("XModelNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("XModelNumber").Value = model_number

        cmd.Parameters.Add("rc_confmodels", OracleDbType.RefCursor, ParameterDirection.Output)
        cmd.Parameters.Add("rc_newmodels", OracleDbType.RefCursor, ParameterDirection.Output)

        OpenConnection()
        cmd.Connection = dbConnection

        Dim myAdaptor As New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
        CloseConnection()

        Try
            doc = New XmlDocument
            doc.LoadXml(xml_data)
            'internalize
            Dim nodes As XmlNodeList = doc.DocumentElement.ChildNodes
            Dim elem As XmlElement
            Dim kit As New Kit()
            Dim kit_model As String = ""
            Dim kit_list As New ArrayList
            Dim kit_type As String
            Dim kit_item As KitItem
            Dim part As Part
            Dim category As ProductCategory
            Dim j As Integer

            lstConflicts = New List(Of String)
            For i As Integer = 0 To doc.GetElementsByTagName("OLD_MATERIAL").Count - 1
                If nodes(i).Item("OLD_MATERIAL") IsNot Nothing Then
                    lstConflicts.Add(nodes(i).Item("OLD_MATERIAL").InnerText + ":" + nodes(i).Item("NEW_MATERIAL").InnerText)
                End If
            Next

            For j = 0 To doc.GetElementsByTagName("Table1").Count - 1
                nodes = doc.GetElementsByTagName("Table1")
                If kit_model = nodes(j).Item("KITMODELNAME").InnerText Then
                    kit_item = New KitItem
                    part = New Part
                    elem = nodes(j).Item("PARTNUMBER")
                    If Not elem Is Nothing Then
                        part.PartNumber = elem.InnerText
                    End If

                    elem = nodes(j).Item("MODELNUMBER")
                    If Not elem Is Nothing Then
                        part.Model = elem.InnerText
                    End If

                    elem = nodes(j).Item("PARTDESCRIPTION")
                    If Not elem Is Nothing Then
                        part.Description = elem.InnerText
                    End If
                    elem = nodes(j).Item("PARTCATEGORY")
                    If Not elem Is Nothing Then
                        category = New ProductCategory(nodes(j).Item("PARTCATEGORY").InnerText)
                        part.Category = category
                    End If


                    elem = nodes(j).Item("PROGRAMCODE")
                    If elem Is Nothing Then
                    Else
                        part.ProgramCode = elem.InnerText
                    End If

                    elem = nodes(j).Item("FRBCORECHARGE")
                    If elem Is Nothing Then
                    Else
                        part.CoreCharge = elem.InnerText
                    End If

                    elem = nodes(j).Item("REPLACEMENTPARTNUMBER")
                    If elem Is Nothing Then
                    Else
                        part.ReplacementPartNumber = elem.InnerText
                    End If

                    elem = nodes(j).Item("LISTPRICE")
                    If elem Is Nothing Then
                    Else
                        part.ListPrice = elem.InnerText
                    End If

                    elem = nodes(j).Item("BILLINGONLY")
                    If elem Is Nothing Then
                    Else
                        part.BillingOnly = elem.InnerText
                    End If

                    elem = nodes(j).Item("SALESTEXT")
                    If elem Is Nothing Then
                    Else
                        part.SalesText = elem.InnerText
                    End If

                    kit_item.Product = part

                    elem = nodes(j).Item("QUANTITY")
                    If Not elem Is Nothing Then
                        kit_item.Quantity = elem.InnerText
                        'MZN: Added 4/06/04 to mutiply quantity * listprice if quantity is > 1 before adding kit_item to kit
                        If kit_item.Quantity > 1 Then
                            part.ListPrice = part.ListPrice * kit_item.Quantity
                        End If
                    End If
                    kit.AddKitItem(kit_item)
                Else
                    kit = New Kit
                    kit.Model = nodes(j).Item("KITMODELNAME").InnerText
                    kit.PartNumber = kit.Model
                    kit_model = kit.Model

                    elem = nodes(j).Item("DISCOUNTPERCENT")
                    If Not elem Is Nothing Then
                        kit.DiscountPercentage = elem.InnerText
                    End If

                    elem = nodes(j).Item("DESCRIPTION")
                    If Not elem Is Nothing Then
                        kit.Description = elem.InnerText
                    End If

                    elem = nodes(j).Item("SISKITTYPEID")
                    If Not elem Is Nothing Then
                        kit_type = elem.InnerText

                        If kit_type = "MJ" Then
                            kit.TheKitType = Kit.KitType.Major
                        Else
                            kit.TheKitType = Kit.KitType.Minor
                        End If
                    End If

                    kit_item = New KitItem
                    part = New Part

                    elem = nodes(j).Item("PARTNUMBER")
                    If Not elem Is Nothing Then
                        part.PartNumber = elem.InnerText
                    End If

                    elem = nodes(j).Item("MODELNUMBER")
                    If Not elem Is Nothing Then
                        part.Model = elem.InnerText
                    End If

                    elem = nodes(j).Item("PARTDESCRIPTION")
                    If Not elem Is Nothing Then
                        part.Description = elem.InnerText
                    End If

                    elem = nodes(j).Item("PARTCATEGORY")
                    If Not elem Is Nothing Then
                        category = New ProductCategory(nodes(j).Item("PARTCATEGORY").InnerText)
                        part.Category = category
                    End If


                    elem = nodes(j).Item("PROGRAMCODE")
                    If elem Is Nothing Then
                    Else
                        part.ProgramCode = elem.InnerText
                    End If

                    elem = nodes(j).Item("FRBCORECHARGE")
                    If elem Is Nothing Then
                    Else
                        part.CoreCharge = elem.InnerText
                    End If

                    elem = nodes(j).Item("REPLACEMENTPARTNUMBER")
                    If elem Is Nothing Then
                    Else
                        part.ReplacementPartNumber = elem.InnerText
                    End If

                    elem = nodes(j).Item("LISTPRICE")
                    If elem Is Nothing Then
                    Else
                        part.ListPrice = elem.InnerText
                    End If

                    elem = nodes(j).Item("BILLINGONLY")
                    If elem Is Nothing Then
                    Else
                        part.BillingOnly = elem.InnerText
                    End If

                    elem = nodes(j).Item("SALESTEXT")
                    If elem Is Nothing Then
                    Else
                        part.SalesText = elem.InnerText
                    End If


                    kit_item.Product = part

                    elem = nodes(j).Item("QUANTITY")
                    If Not elem Is Nothing Then
                        kit_item.Quantity = elem.InnerText
                        'MZN: Added 4/06/04 to mutiply quantity * listprice if quantity is > 1 before adding kit_item to kit
                        If kit_item.Quantity > 1 Then
                            part.ListPrice = part.ListPrice * kit_item.Quantity
                        End If
                    End If
                    kit.AddKitItem(kit_item)

                    kit_list.Add(kit)
                End If
            Next

            Dim kits(kit_list.Count - 1) As Kit
            kit_list.CopyTo(kits)
            Return kits
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Function
End Class
