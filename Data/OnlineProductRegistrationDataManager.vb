﻿Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core

Public Class OnlineProductRegistrationDataManager
    Inherits DataManager
    Dim ewmodel As DataSet

    Public Function GetModelPrefix() As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetPRModelPrefix")

        cmd.Parameters.Add("rc_PRModelPrefix", OracleDbType.RefCursor, ParameterDirection.Output)

        Try
            ewmodel = GetPRResults(cmd, True)
        Catch ex As Exception
            'Console.WriteLine("troubles calling GetPRModelPrefix: " + ex.Message)
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetIndustry() As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetPRIndustry")

        cmd.Parameters.Add("rc_PRIndustry", OracleDbType.RefCursor, ParameterDirection.Output)

        Try
            ewmodel = GetPRResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetTitle() As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetTitle")

        cmd.Parameters.Add("rc_PRTitle", OracleDbType.RefCursor, ParameterDirection.Output)

        Try
            ewmodel = GetPRResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetPRModels(ByVal ModelPrefix As String) As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetPRModelNumberList")

        cmd.Parameters.Add("xMODELPREFIX", OracleDbType.Varchar2, 20, ModelPrefix, ParameterDirection.Input)
        cmd.Parameters.Add("rc_PRModelNumberList", OracleDbType.RefCursor, ParameterDirection.Output)

        Try
            ewmodel = GetPRResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    Public Function GetReseller() As DataSet
        Dim cmd As New OracleCommand("ExtendedWarranty.GetResellerList")

        cmd.Parameters.Add("rc_PRResellerPrefix", OracleDbType.RefCursor, ParameterDirection.Output)

        Try
            ewmodel = GetPRResults(cmd, True)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Return ewmodel
    End Function

    ''' <summary>
    ''' Checks if the User is registered as a Reseller by comparing their linked SAP
    ''' Accounts to the PR_Reseller table. When comparing, the SP will strip any
    ''' leading zeros, so 0000123 = 123
    ''' </summary>
    ''' <returns>1 = Reseller, 0 = Not Reseller, -1 = Database-level error.</returns>
    ''' <remarks>Added May 2018 for enhancement request</remarks>
    Public Function IsReseller(userId As String, context As TransactionContext) As Boolean
        Dim dbFlag As Decimal = -1      ' Default value = Error
        Dim cmd As New OracleCommand("IsReseller")
        cmd.Parameters.Add("xUserId", OracleDbType.Varchar2, userId, ParameterDirection.Input)
        cmd.Parameters.Add("retVal", OracleDbType.Int32, ParameterDirection.Output)

        Try
            ExecuteCMD(cmd, context)

            If CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then Throw New ServicesPLUSOracleException("An error occurred while checking IsReseller.")

            dbFlag = CType(cmd.Parameters("retVal").Value.ToString(), Integer)
            If dbFlag < 0 Then Throw New ServicesPLUSOracleException("IsReseller returned an error code of: " & dbFlag)

            Return (dbFlag = 1)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ValidatePromotionalCode(ByVal PROMO_CODE_NUMBER As String, ByVal PurchaseDate As Date, ByVal INDUSTRY As String, ByVal ModelNumber As String, ByVal ResellerName As String) As String
        Dim cmd As New OracleCommand("ValidatePromotionalCode")

        cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, 10, PROMO_CODE_NUMBER, ParameterDirection.Input)
        cmd.Parameters.Add("xPurchaseDate", OracleDbType.Date, PurchaseDate, ParameterDirection.Input)
        cmd.Parameters.Add("xINDUSTRY", OracleDbType.Varchar2, 100, INDUSTRY, ParameterDirection.Input)
        cmd.Parameters.Add("xModelNumber", OracleDbType.Varchar2, 18, ModelNumber, ParameterDirection.Input)
        cmd.Parameters.Add("xResellerName", OracleDbType.Varchar2, 36, ResellerName, ParameterDirection.Input)

        Dim outparam2 As New OracleParameter("xSUCCESS_MESSAGE", OracleDbType.Varchar2, 255)
        outparam2.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam2)
        Dim outparam3 As New OracleParameter("XTemp", OracleDbType.Varchar2, 250)
        outparam3.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam3)

        CheckPromtionalCode(cmd)

        If CType(cmd.Parameters("xSUCCESS_MESSAGE")?.Value, INullable).IsNull Then
            Return "Error"
        Else
            Dim strTemp As String
            Dim strSuccess As String
            strTemp = cmd.Parameters("XTemp")?.Value?.ToString()
            strSuccess = cmd.Parameters("xSUCCESS_MESSAGE").Value.ToString()
            If strTemp = "TTT" And strSuccess <> "Error" Then
                Return cmd.Parameters("xSUCCESS_MESSAGE").Value.ToString()
            Else
                Return "Error"
            End If
        End If
    End Function

    Public Function DuplicatePromotionalCode(ByVal PROMO_CODE_NUMBER As String) As Boolean
        Dim strTemp As String = ""
        Dim cmd As New OracleCommand("checkPromotionalCode")

        cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, 10)
        cmd.Parameters("xPROMO_CODE_NUMBER").Value = PROMO_CODE_NUMBER

        Dim outparam2 As New OracleParameter("xret", OracleDbType.Varchar2, 10)
        outparam2.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam2)
        CheckPromtionalCode(cmd)
        strTemp = cmd.Parameters("xret").Value.ToString()

        Return (strTemp = "Duplicate")
    End Function

    Private Sub CheckPromtionalCode(ByRef command As OracleCommand)
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Try
            OpenConnection()
            command.Connection = dbConnection
            command.ExecuteNonQuery()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try
    End Sub

    Public Function SaveProductRegistration(ByRef ProductRegistrationData As CustomerProductRegistration, ByRef context As TransactionContext) As Long
        Dim cmd As New OracleCommand("ADDPRODUCTREGISTRATION")

        cmd.Parameters.Add("xCUSTOMERID", OracleDbType.Int32)
        cmd.Parameters("xCUSTOMERID").Value = Convert.ToInt32(ProductRegistrationData.CustomerID)

        cmd.Parameters.Add("xSUFFIX", OracleDbType.Varchar2, 20)
        cmd.Parameters("xSUFFIX").Value = ""

        cmd.Parameters.Add("xFIRST_NAME", OracleDbType.Varchar2, 20)
        cmd.Parameters("xFIRST_NAME").Value = ProductRegistrationData.FIRST_NAME

        cmd.Parameters.Add("xLAST_NAME", OracleDbType.Varchar2, 20)
        cmd.Parameters("xLAST_NAME").Value = ProductRegistrationData.LAST_NAME

        cmd.Parameters.Add("xEMAIL", OracleDbType.Varchar2, 100)
        cmd.Parameters("xEMAIL").Value = ProductRegistrationData.EMAIL

        cmd.Parameters.Add("xTITLE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xTITLE").Value = ProductRegistrationData.TITLE

        cmd.Parameters.Add("xCOMPANY_NAME", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCOMPANY_NAME").Value = ProductRegistrationData.COMPANY_NAME

        cmd.Parameters.Add("xCOMPANY_TYPE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCOMPANY_TYPE").Value = ProductRegistrationData.COMPANY_TYPE

        cmd.Parameters.Add("xLINE_OF_WORK", OracleDbType.Varchar2, 20)
        cmd.Parameters("xLINE_OF_WORK").Value = ProductRegistrationData.LINE_OF_WORK

        cmd.Parameters.Add("xADDRESS1", OracleDbType.Varchar2, 20)
        cmd.Parameters("xADDRESS1").Value = ProductRegistrationData.ADDRESS1

        cmd.Parameters.Add("xADDRESS2", OracleDbType.Varchar2, 20)
        cmd.Parameters("xADDRESS2").Value = ProductRegistrationData.ADDRESS2

        cmd.Parameters.Add("xADDRESS3", OracleDbType.Varchar2, 20)
        cmd.Parameters("xADDRESS3").Value = ProductRegistrationData.ADDRESS3

        cmd.Parameters.Add("xCITY", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCITY").Value = ProductRegistrationData.CITY

        cmd.Parameters.Add("xSTATE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xSTATE").Value = ProductRegistrationData.STATE

        cmd.Parameters.Add("xZIPCODE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xZIPCODE").Value = ProductRegistrationData.ZIPCODE

        cmd.Parameters.Add("xCOUNTRY", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCOUNTRY").Value = ProductRegistrationData.COUNTRY

        cmd.Parameters.Add("xPR_PHONE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xPR_PHONE").Value = ProductRegistrationData.PR_PHONE

        cmd.Parameters.Add("xSEC_PHONE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xSEC_PHONE").Value = ProductRegistrationData.SEC_PHONE

        cmd.Parameters.Add("xEXT_NO", OracleDbType.Varchar2, 20)
        cmd.Parameters("xEXT_NO").Value = ProductRegistrationData.EXT_NO

        cmd.Parameters.Add("xFAX", OracleDbType.Varchar2, 20)
        cmd.Parameters("xFAX").Value = ProductRegistrationData.FAX

        cmd.Parameters.Add("xNOTES", OracleDbType.Varchar2, 20)
        cmd.Parameters("xNOTES").Value = ProductRegistrationData.NOTES

        cmd.Parameters.Add("xOPTOUT", OracleDbType.Varchar2, 20)
        cmd.Parameters("xOPTOUT").Value = ProductRegistrationData.OPTOUT

        cmd.Parameters.Add("xCODES", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCODES").Value = ProductRegistrationData.CODES

        cmd.Parameters.Add("xBOOTH_BULLETIN_NUM", OracleDbType.Varchar2, 20)
        cmd.Parameters("xBOOTH_BULLETIN_NUM").Value = ProductRegistrationData.BOOTH_BULLETIN_NUM

        cmd.Parameters.Add("xMODEL_INTERESTED", OracleDbType.Varchar2, 20)
        cmd.Parameters("xMODEL_INTERESTED").Value = ProductRegistrationData.MODEL_INTERESTED

        cmd.Parameters.Add("xPROMO_DESC", OracleDbType.Varchar2, 20)
        cmd.Parameters("xPROMO_DESC").Value = ProductRegistrationData.PROMO_DESC

        cmd.Parameters.Add("xFOLLOW_UP_ACTION", OracleDbType.Varchar2, 20)
        cmd.Parameters("xFOLLOW_UP_ACTION").Value = ProductRegistrationData.FOLLOW_UP_ACTION

        cmd.Parameters.Add("xPURCHASE_TIME_FRAME", OracleDbType.Varchar2, 20)
        cmd.Parameters("xPURCHASE_TIME_FRAME").Value = ProductRegistrationData.PURCHASE_TIME_FRAME

        cmd.Parameters.Add("xSHOW_VISIT_DT", OracleDbType.Varchar2, 20)
        cmd.Parameters("xSHOW_VISIT_DT").Value = ProductRegistrationData.SHOW_VISIT_DT

        cmd.Parameters.Add("xSTATION_ID", OracleDbType.Varchar2, 20)
        cmd.Parameters("xSTATION_ID").Value = ProductRegistrationData.STATION_ID

        cmd.Parameters.Add("xLITERATURE", OracleDbType.Varchar2, 20)
        cmd.Parameters("xLITERATURE").Value = ProductRegistrationData.LITERATURE

        cmd.Parameters.Add("xDEALER", OracleDbType.Varchar2, 20)
        cmd.Parameters("xDEALER").Value = ProductRegistrationData.DEALER

        cmd.Parameters.Add("xQUESTION1", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION1").Value = ProductRegistrationData.QUESTION1
        cmd.Parameters.Add("xANSWER1", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER1").Value = ProductRegistrationData.ANSWER1

        cmd.Parameters.Add("xQUESTION2", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION2").Value = ProductRegistrationData.QUESTION2
        cmd.Parameters.Add("xANSWER2", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER2").Value = ProductRegistrationData.ANSWER2

        cmd.Parameters.Add("xQUESTION3", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION3").Value = ProductRegistrationData.QUESTION3
        cmd.Parameters.Add("xANSWER3", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER3").Value = ProductRegistrationData.ANSWER3

        cmd.Parameters.Add("xQUESTION4", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION4").Value = ProductRegistrationData.QUESTION4
        cmd.Parameters.Add("xANSWER4", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER4").Value = ProductRegistrationData.ANSWER4

        cmd.Parameters.Add("xQUESTION5", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION5").Value = ProductRegistrationData.QUESTION5
        cmd.Parameters.Add("xANSWER5", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER5").Value = ProductRegistrationData.ANSWER5

        cmd.Parameters.Add("xQUESTION6", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION6").Value = ProductRegistrationData.QUESTION6
        cmd.Parameters.Add("xANSWER6", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER6").Value = ProductRegistrationData.ANSWER6

        cmd.Parameters.Add("xQUESTION7", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION7").Value = ProductRegistrationData.QUESTION7
        cmd.Parameters.Add("xANSWER7", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER7").Value = ProductRegistrationData.ANSWER7

        cmd.Parameters.Add("xQUESTION8", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION8").Value = ProductRegistrationData.QUESTION8
        cmd.Parameters.Add("xANSWER8", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER8").Value = ProductRegistrationData.ANSWER8

        cmd.Parameters.Add("xQUESTION9", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION9").Value = ProductRegistrationData.QUESTION9
        cmd.Parameters.Add("xANSWER9", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER9").Value = ProductRegistrationData.ANSWER9

        cmd.Parameters.Add("xQUESTION10", OracleDbType.Varchar2, 20)
        cmd.Parameters("xQUESTION10").Value = ProductRegistrationData.QUESTION10
        cmd.Parameters.Add("xANSWER10", OracleDbType.Varchar2, 20)
        cmd.Parameters("xANSWER10").Value = ProductRegistrationData.ANSWER10

        cmd.Parameters.Add("xCONTACTEVENTLEVEL", OracleDbType.Varchar2, 20)
        cmd.Parameters("xCONTACTEVENTLEVEL").Value = ProductRegistrationData.CONTACTEVENTLEVEL

        cmd.Parameters.Add("retVal", OracleDbType.Int16, ParameterDirection.Output)

        ExecuteCMD(cmd, context)

        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            Return Convert.ToInt64(cmd.Parameters("retVal").Value.ToString())
        Else
            Return Nothing
        End If
    End Function

    Public Sub SavePromotionalCode(ByVal strPROMO_CODE_NUMBER As String, ByVal strSUCCESS_MESSAGE As String, ByVal PROMO_START_DATE As Date, ByVal PROMO_END_DATE As Date, ByVal PROD_REG_END_DATE As Date, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("InsertPromotionalCode")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)
            cmd.Parameters.Add("xSUCCESS_MESSAGE", OracleDbType.Varchar2, strSUCCESS_MESSAGE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROMO_START_DATE", OracleDbType.Date, PROMO_START_DATE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROMO_END_DATE", OracleDbType.Date, PROMO_END_DATE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROD_REG_END_DATE", OracleDbType.Date, PROD_REG_END_DATE, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    'Added by Prasad for promotional code changes
    Public Sub ModifyPromotionalCode(ByVal strPROMO_CODE_NUMBER As String, ByVal strSUCCESS_MESSAGE As String, ByVal PROMO_START_DATE As Date, ByVal PROMO_END_DATE As Date, ByVal PROD_REG_END_DATE As Date, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("UpdatePromotionalCode")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)
            cmd.Parameters.Add("xSUCCESS_MESSAGE", OracleDbType.Varchar2, strSUCCESS_MESSAGE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROMO_START_DATE", OracleDbType.Date, PROMO_START_DATE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROMO_END_DATE", OracleDbType.Date, PROMO_END_DATE, ParameterDirection.Input)
            cmd.Parameters.Add("xPROD_REG_END_DATE", OracleDbType.Date, PROD_REG_END_DATE, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub SavePromoIndustry(ByVal strPROMO_CODE_NUMBER As String, ByVal strInd As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("InsertPROIndustry")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)
            cmd.Parameters.Add("xINDUSTRYCODE", OracleDbType.Varchar2, strInd, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub SavePromoReseller(ByVal strPROMO_CODE_NUMBER As String, ByVal strReseller As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("InsertPROReseller")

            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)
            cmd.Parameters.Add("xRESELLERCODE", OracleDbType.Varchar2, strReseller, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub DeletePROReseller(ByVal strPROMO_CODE_NUMBER As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("DeletePROReseller")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub DeletePROMODEL(ByVal strPROMO_CODE_NUMBER As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("DeletePROMODEL")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub DeletePROIndustry(ByVal strPROMO_CODE_NUMBER As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("DeletePROIndustry")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub DeletePROMOTIONCODE(ByVal strPROMO_CODE_NUMBER As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("DeletePROMOTIONCODE")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Sub SavePromoModel(ByVal strPROMO_CODE_NUMBER As String, ByVal strModel As String, ByRef context As TransactionContext)
        Try
            Dim cmd As New OracleCommand("InsertPROMODEL")
            cmd.Parameters.Add("xPROMO_CODE_NUMBER", OracleDbType.Varchar2, strPROMO_CODE_NUMBER, ParameterDirection.Input)
            cmd.Parameters.Add("xMODELNUMBER", OracleDbType.Varchar2, strModel, ParameterDirection.Input)

            ExecuteCMD(cmd, context)
        Catch ex As Exception
            'Throw ex
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try
    End Sub

    Public Function GetPRResults(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean) As DataSet
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            OpenConnection()
            command.Connection = dbConnection
            da.Fill(ds)
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        If auto_close_connection Then CloseConnection()

        Return ds
    End Function

    Public Function GetPromotionalCodeNumbers(ByVal whereClause As String) As DataSet
        Dim ErrorString As String = ""
        Dim ds As DataSet = Nothing
        Dim cmd As New OracleCommand("GetPromotionalCodeNumbers") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        Try
            OpenConnection()
            cmd.Connection = dbConnection
            cmd.Parameters.Add("whereClause", OracleDbType.Varchar2, whereClause, ParameterDirection.Input)
            cmd.Parameters.Add("RetVal", OracleDbType.Clob, ParameterDirection.Output)

            cmd.ExecuteNonQuery()

            Dim ReturnValue As OracleClob = TryCast(cmd.Parameters("RetVal").Value, OracleClob)

            If ReturnValue IsNot Nothing Then
                ReturnValue.Seek(0, IO.SeekOrigin.Begin)
                Dim lobBuffer As Byte() = New Byte(ReturnValue.Length - 1) {}
                ReturnValue.Read(lobBuffer, 0, CInt(ReturnValue.Length))
                Dim newBuffer As Byte()
                newBuffer = Text.Encoding.Convert(Text.Encoding.Unicode, Text.Encoding.UTF8, lobBuffer, 0, lobBuffer.Length)
                Dim sb As New Text.StringBuilder()

                For x As Integer = 0 To newBuffer.GetUpperBound(0)
                    sb.Append(Convert.ToChar(newBuffer(x)))
                    'sb.Append((char)newBuffer[x]);
                    'sb.Append((char)newBuffer[x]);
                Next

                If CheckXMLerror(sb.ToString(), ErrorString) Then
                    Throw New System.Exception(ErrorString)
                Else
                    ds = New DataSet("ResultSet")

                    ds.ReadXml(New System.Xml.XmlTextReader(sb.ToString(), System.Xml.XmlNodeType.Document, Nothing))
                End If
            End If
        Catch ex As System.Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetPromotionalCodeDetails(ByVal promoCode As String) As DataSet
        Dim ErrorString As String = ""
        Dim ds As DataSet = Nothing
        Dim cmd As New OracleCommand("PROMOS.GetPromotionalCodeDetails")

        Try
            OpenConnection()
            cmd.Connection = dbConnection
            cmd.Parameters.Add("xPromotionalCodeDetails", OracleDbType.Varchar2, 50, promoCode, ParameterDirection.Input)
            cmd.Parameters.Add("xPromoInfo", OracleDbType.RefCursor, ParameterDirection.Output)

            ds = GetResults(cmd, True)
        Catch ex As System.Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetIndustryForPromotionCode(ByVal promoCode As String) As DataSet
        Dim ErrorString As String = ""
        Dim ds As DataSet = Nothing
        Dim cmd As New OracleCommand("PROMOS.GetIndustryForPromo")

        Try
            OpenConnection()
            cmd.Connection = dbConnection
            cmd.Parameters.Add("xPromoCode", OracleDbType.Varchar2, 50, promoCode, ParameterDirection.Input)
            cmd.Parameters.Add("xPromoInfo", OracleDbType.RefCursor, ParameterDirection.Output)

            ds = GetResults(cmd, True)
        Catch ex As System.Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Return ds
    End Function

    Public Overloads Function GetResults(ByRef command As OracleCommand, ByVal auto_close_connection As Boolean) As DataSet
        command.CommandType = CommandType.StoredProcedure
        command.BindByName = True
        Dim da As New OracleDataAdapter(command)
        Dim ds As New DataSet

        Try
            OpenConnection()
            command.Connection = dbConnection
            da.Fill(ds)
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            If auto_close_connection Then CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetResellerForPromotionCode(ByVal whereClause As String) As DataSet
        Dim ErrorString As String = ""
        Dim ds As DataSet = Nothing
        Dim cmd As New OracleCommand("PROMOS.GetModelForPromo")

        Try
            OpenConnection()
            cmd.Connection = dbConnection
            cmd.Parameters.Add("xPromoCode", OracleDbType.Varchar2, 50, whereClause, ParameterDirection.Input)
            cmd.Parameters.Add("xPromoInfo", OracleDbType.RefCursor, ParameterDirection.Output)

            ds = GetResults(cmd, True)
        Catch ex As System.Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Return ds
    End Function

    Public Function GetModelForPromotionCode(ByVal modelForPramotionCode As String) As DataSet
        Dim ErrorString As String = ""
        Dim ds As DataSet = Nothing
        Dim cmd As New OracleCommand("PROMOS.GetResellerForPromo")

        Try
            OpenConnection()
            cmd.Connection = dbConnection
            cmd.Parameters.Add("xResellerForPromotionCode", OracleDbType.Varchar2, 50, modelForPramotionCode, ParameterDirection.Input)
            cmd.Parameters.Add("xPromoInfo", OracleDbType.RefCursor, ParameterDirection.Output)

            ds = GetResults(cmd, True)
        Catch ex As System.Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        Finally
            CloseConnection()
        End Try

        Return ds
    End Function

    Private Function CheckXMLerror(ByVal XMLString As String, ByVal ErrorString As String) As Boolean
        ErrorString = ""
        Dim theDoc As New System.Xml.XmlDocument()
        Try
            theDoc.LoadXml(XMLString)
        Catch ex As System.Exception
            ErrorString = ex.Message
            Return True
        End Try
        Dim n As System.Xml.XmlNode = theDoc.SelectSingleNode("//ERROR")
        If n Is Nothing Then
            Return False
        Else
            ErrorString = n.InnerText
            Return True
        End If
    End Function

    Public Function GetConflictedModel(ByVal ModelNumber As String, ByRef objGlobalData As GlobalData) As String
        Dim cmd As New OracleCommand("getnewmaterial")
        cmd.Parameters.Add("p_oldmaterial", OracleDbType.Varchar2, 20, ModelNumber, ParameterDirection.Input)
        cmd.Parameters.Add("P_Sales_Org", OracleDbType.Varchar2, 4, objGlobalData.SalesOrganization, ParameterDirection.Input)

        Dim outparam2 As New OracleParameter("p_newmaterial", OracleDbType.Varchar2, 20)
        outparam2.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam2)

        ExecuteCMD(cmd, Nothing)
        If Not CType(cmd.Parameters("p_newmaterial")?.Value, INullable).IsNull Then
            Return cmd.Parameters("p_newmaterial").Value.ToString()
        Else
            Return Nothing
        End If
    End Function

End Class