Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class RMADataManager
    Inherits DataManager

    Public Sub New()
    End Sub

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim request As RMA = CType(data, RMA)
        Dim cmd As New OracleCommand

        cmd.CommandText = "addRACreditRequest"

        cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
        cmd.Parameters("xCustomerID").Value = request.Customer.CustomerID
        cmd.Parameters.Add("xInvoiceNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xInvoiceNumber").Value = request.InvoiceNumber
        cmd.Parameters.Add("xOrderNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xOrderNumber").Value = request.OrderNumber
        cmd.Parameters.Add("xPrice", OracleDbType.Decimal)
        cmd.Parameters("xPrice").Value = request.Price
        cmd.Parameters.Add("xSerialNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("xSerialNumber").Value = request.SerialNumber
        cmd.Parameters.Add("xWarranty", OracleDbType.Int32)

        If request.Warranty Then
            cmd.Parameters("xWarranty").Value = 1
        Else
            cmd.Parameters("xWarranty").Value = 0
        End If

        cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("xPartNumber").Value = request.PartNumber
        cmd.Parameters.Add("xComments", OracleDbType.Varchar2, 400)
        cmd.Parameters("xComments").Value = request.Comments

        cmd.Parameters.Add("xRACreditReasonID", OracleDbType.Int32)
        cmd.Parameters("xRACreditReasonID").Value = request.Reason.ReasonID

        Dim outparam As New OracleParameter("retVal", OracleDbType.Int32)
        outparam.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam)
        ExecuteCMD(cmd, context)
        If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
            request.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
        End If
    End Sub

    Public Function GetRACreditReasons() As RMAReason()
        Dim doc As XmlDocument
        Dim cmd As New OracleCommand("getCreditReasons")

        doc = GetResults(cmd)

        'internalize
        Dim root As XmlElement = doc.DocumentElement
        Dim nodes As XmlNodeList = root.ChildNodes
        Dim reason As RMAReason
        Dim reason_list As New ArrayList
        Dim j As Integer

        For j = 0 To nodes.Count - 1
            reason = New RMAReason With {
                .ReasonID = nodes(j)("RACREDITREASONID").InnerText,
                .Description = nodes(j)("DESCRIPTION").InnerText
            }
            reason_list.Add(reason)
        Next

        Dim reasons(reason_list.Count - 1) As RMAReason
        reason_list.CopyTo(reasons)

        Return reasons
    End Function

    Public Function GetConflictedPart(ByVal PartNumber As String, ByRef objGlobalData As GlobalData) As String
        Dim cmd As New OracleCommand("getnewmaterial")
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("p_oldmaterial", OracleDbType.Varchar2, 20)
        cmd.Parameters("p_oldmaterial").Value = PartNumber

        cmd.Parameters.Add("P_Sales_Org", OracleDbType.Varchar2, 4)
        cmd.Parameters("P_Sales_Org").Value = objGlobalData.SalesOrganization

        Dim outparam2 As New OracleParameter("p_newmaterial", OracleDbType.Varchar2, 20)
        outparam2.Direction = ParameterDirection.Output
        cmd.Parameters.Add(outparam2)

        Dim outparam3 As New OracleParameter("p_materialtype", OracleDbType.Varchar2, 1)
        outparam3.Value = "P"
        cmd.Parameters.Add(outparam3)

        ExecuteCMD(cmd, Nothing)
        If Not CType(cmd.Parameters("p_newmaterial")?.Value, INullable).IsNull Then
            Return cmd.Parameters("p_newmaterial").Value
        Else
            Return Nothing
        End If
    End Function
End Class
