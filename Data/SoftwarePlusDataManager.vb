Imports System.Xml
Imports System.Collections
Imports Oracle.ManagedDataAccess.Client
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Public Class SoftwarePlusDataManager
    Inherits DataManager

    Public Function SearchSoftwareModel(ByVal search_number As String, ByVal salesOrg As String, Optional ByVal SearchByPart As Boolean = False) As Software()
        Dim software_list As New ArrayList
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim doc As New XmlDocument
        Dim nodes As XmlNodeList
        Dim sw As Software
        Dim xml_data As String
        Dim cost As Double

        Dim cmd As New OracleCommand With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }

        If SearchByPart = False Then
            cmd.CommandText = "softwareplus.SearchSoftwareWithModel"
            cmd.Parameters.Add("XModelNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("XModelNumber").Value = search_number.ToUpper()
            cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2, 4)
            cmd.Parameters("xSalesOrg").Value = salesOrg
        Else
            cmd.CommandText = "softwareplus.SearchSoftwareWithPartNumber"
            cmd.Parameters.Add("xPartNumber", OracleDbType.Varchar2, 50)
            cmd.Parameters("xPartNumber").Value = search_number.ToUpper()
            cmd.Parameters.Add("xSalesOrg", OracleDbType.Varchar2, 4)
            cmd.Parameters("xSalesOrg").Value = salesOrg
        End If
        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        CloseConnection()

        'internalize model information
        Try
            doc.LoadXml(xml_data)
            nodes = doc.DocumentElement.ChildNodes

            For Each node As XmlNode In nodes
                sw = New Software With {
                    .ModelNumber = If(node("MODEL_ID")?.InnerText, ""),
                    .Version = If(node("VERSION")?.InnerText, ""),
                    .ModelDescription = If(node("MODEL_DESCRIPTION")?.InnerText, ""),
                    .FreeOfCharge = (If(node("FREE_OF_CHARGE")?.InnerText, "0").Trim() <> "0"),    ' Due to developer inexperience on Nov 2011, "true" is now stored as -1 or 1.
                    .Tracking = (If(node("TRACKING")?.InnerText, "0").Trim() <> "0"),              ' Due to developer inexperience on Nov 2011, "true" is now stored as -1 or 1.
                    .ProductCategory = If(node("PRODUCT_CATEGORY")?.InnerText, ""),
                    .DownloadFileName = If(node("DOWNLOAD_FILE")?.InnerText, ""),
                    .ReleaseNotesFileName = If(node("RELEASE_NOTES_FILE")?.InnerText, ""),
                    .PartNumber = If(node("KIT_PART_NUMBER")?.InnerText, ""),
                    .KitDescription = If(node("KIT_DESCRIPTION")?.InnerText, ""),
                    .Description = If(node("KIT_DESCRIPTION")?.InnerText, ""),
                    .BillingOnly = (If(node("BILLINGONLY")?.InnerText, "").Trim() = "Y"),
                    .RequiredKey = If(node("REQUIRED_KEY")?.InnerText, "")
                }

                If (node("SOFTWARETYPE") IsNot Nothing) AndAlso (node("SOFTWARETYPE")?.InnerText <> "-1") Then
                    Select Case node("SOFTWARETYPE").InnerText
                        Case "0" ' only for Classroom training
                        Case "1" ' only for computer based training
                            sw.Type = Product.ProductType.ComputerBasedTraining
                        Case "2" ' deleted from tainingtype table
                        Case "3" ' only for Certification
                            sw.Type = Product.ProductType.Certificate
                        Case "4" ' only Online Training
                            sw.Type = Product.ProductType.OnlineTraining
                        Case Else
                    End Select
                End If

                ' Parse the List Price, allowing for French decimal character ',' if the parse fails.
                If Not Double.TryParse(If(node("LISTPRICE")?.InnerText, "0.0"), cost) Then
                    cost = Convert.ToDouble(If(node("LISTPRICE")?.InnerText, "0.0").Replace(".", ","))
                End If
                sw.ListPrice = cost

                software_list.Add(sw)
            Next

        Catch ex As Exception
            Throw New ServicesPLUSOracleException(ERRORCODE.ora06550, ex)
        End Try

        Dim sws(software_list.Count - 1) As Software
        software_list.CopyTo(sws)

        Return sws
    End Function

    ' 2017-01-12 ASleight - I believe this method is orphaned (no longer used)
    'Public Function SearchSoftware(ByVal model_number As String, ByVal description As String, ByVal product_category As String) As Software()
    '    Dim software_list As New ArrayList

    '    'Dim connection_string As String
    '    'Dim config_settings As Hashtable
    '    'Dim handler As New ConfigHandler("SOFTWARE\SERVICESPLUS")
    '    'config_settings = handler.GetConfigSettings("//ServicesPLUSConfiguration").Item("SoftwarePLUS")
    '    'cn.ConnectionString = CType(config_settings.Item("ConnectionString"), String)

    '    Dim cmd As New OracleCommand

    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.CommandText = "softwareplus.SearchSoftwarePLUS"
    '    cmd.Parameters.Add("XModelNumber", OracleDbType.Varchar2, 50)
    '    cmd.Parameters("XModelNumber").Value = model_number.ToUpper() + "%"
    '    cmd.Parameters.Add("xDescription", OracleDbType.Varchar2, 80)
    '    cmd.Parameters("xDescription").Value = "%" + description.ToUpper() + "%"
    '    cmd.Parameters.Add("xCategory", OracleDbType.Varchar2, 50)
    '    cmd.Parameters("xCategory").Value = product_category + "%"

    '    Dim outparam1 As New OracleParameter("retVal", OracleDbType.RefCursor)
    '    outparam1.Direction = ParameterDirection.Output
    '    cmd.Parameters.Add(outparam1)

    '    If cn.State = ConnectionState.Closed Then
    '        cn.Open()
    '        cmd.Connection = cn
    '    End If

    '    Dim myAdaptor As New OracleDataAdapter(cmd)
    '    Dim dataset As New DataSet
    '    Dim xml_data As String

    '    Try
    '        myAdaptor.Fill(dataset)
    '        xml_data = dataset.GetXml()
    '        'Console.WriteLine(xml_data)
    '    Catch ex As Exception
    '        'log
    '        Console.WriteLine("troubles calling partsfinder: " + ex.Message)
    '        Throw ex
    '    End Try

    '    cn.Close()

    '    'internalize model information
    '    Dim doc As New XmlDocument

    '    Try
    '        doc.LoadXml(xml_data)

    '        Dim root As XmlElement = doc.DocumentElement
    '        Dim nodes As XmlNodeList = root.ChildNodes
    '        Dim elem As XmlElement
    '        Dim j As Integer
    '        Dim sw As Software

    '        For j = 0 To nodes.Count - 1
    '            sw = New Software

    '            elem = nodes(j)("MODEL_ID")
    '            If elem Is Nothing Then
    '            Else
    '                sw.ModelNumber = elem.InnerText
    '            End If

    '            elem = nodes(j)("VERSION")
    '            If elem Is Nothing Then
    '            Else
    '                sw.Version = elem.InnerText
    '            End If

    '            elem = nodes(j)("MODEL_DESCRIPTION")
    '            If elem Is Nothing Then
    '            Else
    '                sw.ModelDescription = elem.InnerText
    '            End If

    '            elem = nodes(j)("FREE_OF_CHARGE")  ' Due to a strange decision on Nov 2011, "true" is now stored as -1 or 1.
    '            If elem IsNot Nothing Then
    '                sw.FreeOfCharge = (elem.InnerText.Trim() <> "0")
    '            End If

    '            elem = nodes(j)("PRODUCT_CATEGORY")
    '            If elem Is Nothing Then
    '            Else
    '                sw.ProductCategory = elem.InnerText
    '            End If

    '            elem = nodes(j)("DOWNLOAD_FILE")
    '            If elem Is Nothing Then
    '            Else
    '                sw.DownloadFileName = elem.InnerText
    '            End If

    '            elem = nodes(j)("RELEASE_NOTES_FILE")
    '            If elem Is Nothing Then
    '            Else
    '                sw.ReleaseNotesFileName = elem.InnerText
    '            End If

    '            elem = nodes(j)("KIT_PART_NUMBER")
    '            If elem Is Nothing Then
    '            Else
    '                sw.PartNumber = elem.InnerText
    '            End If

    '            elem = nodes(j)("KIT_DESCRIPTION")
    '            If elem Is Nothing Then
    '            Else
    '                sw.KitDescription = elem.InnerText
    '                sw.Description = elem.InnerText
    '            End If

    '            elem = nodes(j)("LISTPRICE")
    '            If elem Is Nothing Then
    '                sw.ListPrice = 0.0
    '            Else
    '                sw.ListPrice = elem.InnerText
    '            End If
    '            'swplus starts
    '            elem = nodes(j)("REQUIRED_KEY")
    '            If elem Is Nothing Then
    '            Else
    '                sw.RequiredKey = elem.InnerText
    '            End If
    '            'swplus ends
    '            software_list.Add(sw)
    '        Next

    '    Catch ex As Exception
    '        'log error
    '        Throw ex
    '    End Try

    '    Dim sws(software_list.Count - 1) As Software
    '    software_list.CopyTo(sws)

    '    Return sws
    'End Function

    Public Function SearchSoftwareByID(ByVal kitpart_number As String) As SelectedSoftware
        Dim myAdaptor As OracleDataAdapter
        Dim dataset As New DataSet
        Dim doc As New XmlDocument
        Dim node As XmlNode
        Dim sw As SelectedSoftware
        Dim cost As Double
        Dim xml_data As String

        Dim cmd As New OracleCommand("softwareplus.SearchSoftwarePLUSByID") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("XKitPartNumber", OracleDbType.Varchar2, 20)
        cmd.Parameters("XKitPartNumber").Value = kitpart_number

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        myAdaptor = New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling softwaresfinder: " + ex.Message)
            Throw ex
        End Try

        CloseConnection()

        Try
            doc.LoadXml(xml_data)
            node = doc.DocumentElement.ChildNodes(0)    ' This should only ever return one value.

            sw = New SelectedSoftware With {
                .ModelNumber = If(node("MODEL_ID")?.InnerText, ""),
                .Version = If(node("VERSION")?.InnerText, ""),
                .ModelDescription = If(node("MODEL_DESCRIPTION")?.InnerText, ""),
                .FreeOfCharge = (If(node("FREE_OF_CHARGE")?.InnerText, "0").Trim() <> "0"),    ' Due to developer inexperience on Nov 2011, "true" is now stored as -1 or 1.
                .Tracking = (If(node("TRACKING")?.InnerText, "0").Trim() <> "0"),              ' Due to developer inexperience on Nov 2011, "true" is now stored as -1 or 1.
                .ProductCategory = If(node("PRODUCT_CATEGORY")?.InnerText, ""),
                .DownloadFileName = If(node("DOWNLOAD_FILE")?.InnerText, ""),
                .ReleaseNotesFileName = If(node("RELEASE_NOTES_FILE")?.InnerText, ""),
                .PartNumber = If(node("KIT_PART_NUMBER")?.InnerText, ""),
                .KitDescription = If(node("KIT_DESCRIPTION")?.InnerText, ""),
                .Description = If(node("KIT_DESCRIPTION")?.InnerText, ""),
                .BillingOnly = (If(node("BILLINGONLY")?.InnerText, "").Trim() = "Y"),
                .RequiredKey = If(node("REQUIRED_KEY")?.InnerText, "")
            }

            If (node("SOFTWARETYPE") IsNot Nothing) AndAlso (node("SOFTWARETYPE")?.InnerText <> "-1") Then
                Select Case node("SOFTWARETYPE").InnerText
                    Case "0" ' only for Classroom training
                    Case "1" ' only for computer based training
                        sw.Type = Product.ProductType.ComputerBasedTraining
                    Case "2" ' deleted from tainingtype table
                    Case "3" ' only for Certification
                        sw.Type = Product.ProductType.Certificate
                    Case "4" ' only Online Training
                        sw.Type = Product.ProductType.OnlineTraining
                    Case Else
                End Select
            End If

            ' Parse the List Price, allowing for French decimal character ',' if the parse fails.
            If Not Double.TryParse(If(node("LISTPRICE")?.InnerText, "0.0"), cost) Then
                cost = Convert.ToDouble(If(node("LISTPRICE")?.InnerText, "0.0").Replace(".", ","))
            End If
            sw.ListPrice = cost
        Catch ex As Exception
            'log error
            Throw ex
        End Try

        Return sw
    End Function

    Public Function GetSoftwareCategories() As String()
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cat_list As New ArrayList
        Dim cmd As New OracleCommand("getSoftwareCategories")

        doc = GetResults(cmd)

        nodes = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            cat_list.Add(node("PRODUCT_CATEGORY").InnerText)
        Next

        Dim cats(cat_list.Count - 1) As String
        cat_list.CopyTo(cats)

        Return cats
    End Function

    Public Function SearchSoftwareModel(ByVal model_number As String, ByVal description As String, ByVal product_category As String) As Software()
        Dim software_list As New ArrayList
        Dim dataset As New DataSet
        Dim doc As New XmlDocument
        Dim nodes As XmlNodeList
        Dim xml_data As String

        Dim cmd As New OracleCommand("softwareplus.SearchSoftwarePLUSModel") With {
            .CommandType = CommandType.StoredProcedure,
            .BindByName = True
        }
        cmd.Parameters.Add("XModelNumber", OracleDbType.Varchar2, 50)
        cmd.Parameters("XModelNumber").Value = model_number.ToUpper() + "%"
        cmd.Parameters.Add("xDescription", OracleDbType.Varchar2, 80)
        cmd.Parameters("xDescription").Value = "%" + description.ToUpper() + "%"
        cmd.Parameters.Add("xCategory", OracleDbType.Varchar2, 50)
        cmd.Parameters("xCategory").Value = product_category + "%"

        cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.RefCursor, ParameterDirection.Output))

        OpenConnection()
        cmd.Connection = dbConnection
        Dim myAdaptor As New OracleDataAdapter(cmd)

        Try
            myAdaptor.Fill(dataset)
            xml_data = dataset.GetXml()
            'Console.WriteLine(xml_data)
        Catch ex As Exception
            'log
            Console.WriteLine("troubles calling partsfinder: " + ex.Message)
            Throw ex
        End Try
        CloseConnection()

        Try
            doc.LoadXml(xml_data)
            nodes = doc.DocumentElement.ChildNodes
            For Each node As XmlNode In nodes
                software_list.Add(New Software With {
                    .ModelNumber = If(node("MODEL_ID")?.InnerText, ""),
                    .ModelDescription = If(node("MODEL_DESCRIPTION")?.InnerText, ""),
                    .ProductCategory = If(node("PRODUCT_CATEGORY")?.InnerText, "")
                })
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Dim sws(software_list.Count - 1) As Software
        software_list.CopyTo(sws)

        Return sws
    End Function
End Class
