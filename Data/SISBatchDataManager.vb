Imports System.Xml
Imports Oracle.ManagedDataAccess.Client

Public Class SISBatchDataManager
    Inherits DataManager

    Public Function GetSISBatchLog(ByRef beginDate As String, ByRef endDate As String) As XmlDataDocument
        Dim cmd As New OracleCommand
        cmd.CommandText = "SISBATCH.GETSISBATCHLOG"
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("xBeginDate", OracleDbType.Varchar2, 20)
        cmd.Parameters("xBeginDate").Value = beginDate
        cmd.Parameters.Add("xEndDate", OracleDbType.Varchar2, 20)
        cmd.Parameters("xEndDate").Value = endDate

        cmd.Parameters.Add(New OracleParameter("xLogCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output
        cmd.Parameters.Add(New OracleParameter("xErrorCursor", OracleDbType.RefCursor)).Direction = ParameterDirection.Output
        Dim ds As DataSet = GetResults(cmd, True)
        
        ds.Tables(0).TableName = "SISBATCHPROCESSLOG"
        ds.Tables(1).TableName = "SISBATCHPROCESSERROR"
        ds.Relations.Add("BatchLogError", ds.Tables(0).Columns("SISBATCHPROCESSID"), ds.Tables(1).Columns("SISBATCHPROCESSID"), False).Nested = True
        Dim xmlDoc As XmlDataDocument = New XmlDataDocument(ds)
        Return xmlDoc
    End Function
End Class


