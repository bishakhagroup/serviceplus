Imports System.Xml
Imports Sony.US.ServicesPLUS.Core
Imports Oracle.ManagedDataAccess.Client
Imports Oracle.ManagedDataAccess.Types

Public Class MarketingInterestDataManager
    Inherits DataManager

    Public Overrides Function Restore(ByVal identity As String) As Object
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub Store(ByRef data As Object, ByRef context As TransactionContext, Optional ByVal update As Boolean = False)
        Dim marketing_interest As MarketingInterest = CType(data, MarketingInterest)
        Dim cmd As New OracleCommand

        If (Not update) Or (update And marketing_interest.Dirty) Then
            If update Then
                If marketing_interest.Action = CoreObject.ActionType.DELETE Then
                    cmd.CommandText = "deleteMarketingInterest"
                Else
                    cmd.CommandText = "updateMarketingInterest"
                End If
                cmd.Parameters.Add("xSequenceNumber", OracleDbType.Int32)
                cmd.Parameters("xSequenceNumber").Value = marketing_interest.SequenceNumber
            Else
                cmd.CommandText = "addMarketingInterest"
                cmd.Parameters.Add("xEmailOk", OracleDbType.Int32)
                cmd.Parameters("xEmailOk").Value = IIf(marketing_interest.Customer.EmailOk, 1, 0)
            End If

            cmd.Parameters.Add("xCustomerID", OracleDbType.Int32)
            cmd.Parameters("xCustomerID").Value = marketing_interest.Customer.CustomerID

            cmd.Parameters.Add("xMarketingInterestID", OracleDbType.Int32)
            cmd.Parameters("xMarketingInterestID").Value = marketing_interest.MarketingInterestID

            cmd.Parameters.Add(New OracleParameter("retVal", OracleDbType.Int32, ParameterDirection.Output))
            ExecuteCMD(cmd, context)

            If marketing_interest.Action <> CoreObject.ActionType.DELETE Then
                marketing_interest.Action = CoreObject.ActionType.RESTORE
            End If

            If Not CType(cmd.Parameters("retVal")?.Value, INullable).IsNull Then
                marketing_interest.SequenceNumber = cmd.Parameters("retVal").Value.ToString()
            End If
        End If
    End Sub

    Public Function GetCustomerMarketingInterests(ByVal customer As Customer) As MarketingInterest()
        Dim marketing_interest As MarketingInterest
        Dim marketing_interest_list As New ArrayList
        Dim doc As XmlDocument
        Dim nodes As XmlNodeList
        Dim cmd As New OracleCommand("getCustomerMarketingInterests")

        cmd.Parameters.Add("xUserId", OracleDbType.Char)
        cmd.Parameters("xUserId").Value = customer.CustomerID

        doc = GetResults(cmd)

        'internalize
        nodes = doc.DocumentElement.ChildNodes
        For Each node As XmlNode In nodes
            marketing_interest = New MarketingInterest With {
                .MarketingInterestID = If(node("MARKETINGINTERESTID")?.InnerText, ""),
                .Description = If(node("TOPIC")?.InnerText, ""),
                .SequenceNumber = If(node("SEQUENCENUMBER")?.InnerText, ""),
                .Action = Account.ActionType.RESTORE
            }
            'marketing_interest.Customer = customer
            marketing_interest_list.Add(marketing_interest)
        Next

        Dim interests(marketing_interest_list.Count - 1) As MarketingInterest
        marketing_interest_list.CopyTo(interests)
        Return interests
    End Function
End Class
