﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public  class clsSoftwareRegistration
    {
        public clsSoftwareRegistration()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string KitPartNumber
        {
            get;
            set;
        }
        public string KitDescription
        {
            get;
            set;
        }
        public string SupercedKit
        {
            get;
            set;
        }
        public string ManufacturerRemarks
        {
            get;
            set;
        }
        public string DownloadFile
        {
            get;
            set;
        }
        public string ReleaseNoteFiles
        {
            get;
            set;
        }
        public string UserManualFile
        {
            get;
            set;
        }
        //public string Item_KitPartNumber
        //{
        //    get;
        //    set;
        //}
        //public string Item_Description
        //{
        //    get;
        //    set;
        //}
        //public string Item_Type
        //{
        //    get;
        //    set;
        //}
        //public string Item_SubAssembly
        //{
        //    get;
        //    set;
        //}
        //public string Item_Location
        //{
        //    get;
        //    set;
        //}
        //public string Item_Version
        //{
        //    get;
        //    set;
        //}
        //public string Item_Speed
        //{
        //    get;
        //    set;
        //}
        //public string Item_CheckSum
        //{
        //    get;
        //    set;
        //}
        //public string Item_Manufacturer
        //{
        //    get;
        //    set;
        //}
        //public string Item_LabelContents
        //{
        //    get;
        //    set;
        //}
        //public string Item_Packaging
        //{
        //    get;
        //    set;
        //}
        //public string Sub2_ModelID
        //{
        //    get;
        //    set;
        //}
        //public string Sub2_Version
        //{
        //    get;
        //    set;
        //}
        //public int Sub2_ReleaseQuantity
        //{
        //    get;
        //    set;
        //}
        //public string Sub2_TechnicalReferance
        //{
        //    get;
        //    set;
        //}
        //public int Sub2_FreeOfCharge
        //{
        //    get;
        //    set;
        //}
        //public string Sub2_KitPartNumber
        //{
        //    get;
        //    set;
        //}
    }
}
