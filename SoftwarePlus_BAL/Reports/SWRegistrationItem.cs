﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public  class clsSWRegistrationItem
    {
        public clsSWRegistrationItem()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string Item_KitPartNumber
        {
            get;
            set;
        }
        public string Item_Description
        {
            get;
            set;
        }
        public string Item_Type
        {
            get;
            set;
        }
        public string Item_SubAssembly
        {
            get;
            set;
        }
        public string Item_Location
        {
            get;
            set;
        }
        public string Item_Version
        {
            get;
            set;
        }
        public string Item_Speed
        {
            get;
            set;
        }
        public string Item_CheckSum
        {
            get;
            set;
        }
        public string Item_Manufacturer
        {
            get;
            set;
        }
        public string Item_LabelContents
        {
            get;
            set;
        }
        public string Item_Packaging
        {
            get;
            set;
        }
    }
}
