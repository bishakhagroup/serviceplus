﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public class clsSWRegistrationRelease
    {
        public clsSWRegistrationRelease()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string Sub2_ModelID
        {
            get;
            set;
        }
        public string Sub2_Version
        {
            get;
            set;
        }
        public int Sub2_ReleaseQuantity
        {
            get;
            set;
        }
        public string Sub2_TechnicalReferance
        {
            get;
            set;
        }
        public int Sub2_FreeOfCharge
        {
            get;
            set;
        }
        public string Sub2_KitPartNumber
        {
            get;
            set;
        }
    }
}
