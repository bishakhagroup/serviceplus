﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
    public class clsProductManagerModels
    {
        public clsProductManagerModels()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public int Beta
        {
            get;
            set;

        }
        public int Shipping
        {
            get;
            set;

        }
        public int Upgrade
        {
            get;
            set;

        }
        public int Special
        {
            get;
            set;

        }
        public string ProductManager
        {
            get;
            set;

        }
        public string ModelID
        {
            get;
            set;
        }
        public string Version
        {
            get;
            set;
        }
        public string ReleaseStatus
        {
            get;
            set;
        }
        public string Engineer
        {
            get;
            set;
        }
        public string KitPartNumber
        {
            get;
            set;
        }
        public string ModelDescription
        {
            get;
            set;
        }
        public string ReleaseType
        {
            get;
            set;
        }
        public string DistributionType
        {
            get;
            set;
        }
        public string VisibilityType
        {
            get;
            set;
        }

    }
}
