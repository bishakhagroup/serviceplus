﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public  class clsSystemErrorLog
    {
        public clsSystemErrorLog()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public DateTime StartDate
        {
            get;
            set;
        }
        public DateTime Enddate
        {
            get;
            set;
        }
        public string  UserID
        {
            get;
            set;
        }
        public DateTime ErrorDate
        {
            get;
            set;
        }
        public string Version
        {
            get;
            set;
        }
        public string FormName
        {
            get;
            set;
        }
        public string ProcedureName
        {
            get;
            set;
        }
        public string ErrorCode
        {
            get;
            set;
        }
        public string ErrorDescription
        {
            get;
            set;
        }
    }
}
