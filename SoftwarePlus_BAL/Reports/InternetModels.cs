﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public class clsInternetModels
    {
        public clsInternetModels()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string ReleaseType
        {
            get;
            set;
        }
        public string DistributionType
        {
            get;
            set;
        }
        public string VisibilityType
        {
            get;
            set;
        }
        public string ModelID
        {
            get;
            set;
        }
        public string ModelDescription
        {
            get;
            set;
        }
        public string Version
        {
            get;
            set;
        }
        public DateTime  ReleaseDate
        {
            get;
            set;
        }
    }
}
