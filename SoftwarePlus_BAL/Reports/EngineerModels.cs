﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
    public class clsEngineerModels
    {
        public clsEngineerModels()
	    {
		    //
		    // TODO: Add constructor logic here
		    //
	    }
        public string ModelId
        {
            get;
            set;
        }
        public string ModelVersion
        {
            get;
            set;
        }
        public DateTime  ReleaseDate
        {
            get;
            set;
        }
        public int Beta
        {
            get;
            set;
        }
        public int Shipping
        {
            get;
            set;
        }
        public int Upgrade
        {
            get;
            set;
        }
        public int Special
        {
            get;
            set;
        }
        public string TechnicalReference
        {
            get;
            set;
        }
        public string  KitPartNumber
        {
            get;
            set;
        }
        public string  SupercededKit
        {
            get;
            set;
        }
        public string  KitDescription
        {
            get;
            set;
        }
        public string engineerName
        {
            get;
            set;
        }
        public string ReleaseType
        {
            get;
            set;
        }
        public string DistributionType
        {
            get;
            set;
        }
        public string VisibilityType
        {
            get;
            set;
        }
    }
}
