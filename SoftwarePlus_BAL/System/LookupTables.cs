﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
   public class LookupTables
    {
       SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
       #region Constructor
       public LookupTables()
        { 
        
        }
        #endregion
       #region Category
       public DataSet GetLookuptablesbycategory()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xCategory", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbycategory", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }
           
       }

       public void UpdateLookuptablesbycategory(string PRODUCT_CATEGORY_OLD,string PRODUCT_CATEGORY_NEW)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xPRODUCT_CATEGORY_OLD", PRODUCT_CATEGORY_OLD);
               oraParam[1] = new OracleParameter("xPRODUCT_CATEGORY_NEW", PRODUCT_CATEGORY_NEW);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdateLookuptablesbycategory", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insertLookuptablesbycategory(string PRODUCT_CATEGORY)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xPRODUCT_CATEGORY", PRODUCT_CATEGORY);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insertLookuptablesbycategory", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void deleteLookuptablesbycategory(string PRODUCT_CATEGORY)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xPRODUCT_CATEGORY", PRODUCT_CATEGORY);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "deleteLookuptablesbycategory", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
       #region Country
       public DataSet GetLookuptablesbycountry()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xCountry", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbycountry", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void UpdateLookuptablesbycountry(string Country_OLD, string Country_NEW)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xCountry_OLD", Country_OLD);
               oraParam[1] = new OracleParameter("xCountry_NEW", Country_NEW);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdateLookuptablesbycountry", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insertLookuptablesbycountry(string Country)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xCountry", Country);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insertLookuptablesbycountry", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void deleteLookuptablesbycountry(string COUNTRY)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xCountry", COUNTRY);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "deleteLookuptablesbycountry", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
       #region Positions
       public DataSet GetLookuptablesbyposition()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xposition_title", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbyposition", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void UpdLookuptblbyposition(string position_title_OLD, string position_title_NEW)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xposition_title_OLD", position_title_OLD);
               oraParam[1] = new OracleParameter("xposition_title_NEW", position_title_NEW);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdLookuptblbyposition", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insertLookuptablesbyposition(string position_title)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xposition_title", position_title);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insertLookuptablesbyposition", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void delLookuptblbyposition(string position_title)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xposition_title", position_title);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "delLookuptblbyposition", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
       #region Model Product Category
       public DataSet GetLookuptablesbymodelprodcat()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xmodelprodcat", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbymodelprodcat", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void UpdLookuptblbymodelprodcat(string MODEL_ID, string PRODUCT_CATEGORY, string strMODEL_ID)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[3];

               oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
               oraParam[1] = new OracleParameter("xPRODUCT_CATEGORY", PRODUCT_CATEGORY);
               oraParam[2] = new OracleParameter("xMODEL_ID_OLD", strMODEL_ID);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdLookuptblbymodelprodcat", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insLookuptblbymodelprodcat(string MODEL_ID, string PRODUCT_CATEGORY)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
               oraParam[1] = new OracleParameter("xPRODUCT_CATEGORY", PRODUCT_CATEGORY);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insLookuptblbymodelprodcat", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void delLookuptblbymodelprodcat(string MODEL_ID, string PRODUCT_CATEGORY)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xMODEL_ID", MODEL_ID);
               oraParam[1] = new OracleParameter("xPRODUCT_CATEGORY", PRODUCT_CATEGORY);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "delLookuptblbymodelprodcat", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
       #region Regions
       public DataSet GetLookuptablesbyregions()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xregions", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbyregions", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void UpdLookuptblbyregions(string STATE, string REGION, string STATE_OLD)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[3];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xREGION", REGION);
               oraParam[2] = new OracleParameter("xSTATE_OLD", STATE_OLD);


               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdLookuptblbyregions", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insLookuptblbyregions(string STATE, string REGION)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xREGION", REGION);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insLookuptblbyregions", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void delLookuptblbyregions(string STATE, string REGION)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xREGION", REGION);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "delLookuptblbyregions", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
       #region States
       public DataSet GetLookuptablesbystates()
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
               OracleParameter[] oraParam = new OracleParameter[1];

               oraParam[0] = new OracleParameter("xstates", OracleDbType.RefCursor);
               oraParam[0].Direction = ParameterDirection.Output;

               DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                            CommandType.StoredProcedure,
                            "SYSTEM.GetLookuptablesbystates", oraParam);

               return ObjDS;
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }

       public void UpdLookuptblbystates(string STATE, string STATE_NAME, string STATE_OLD)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[3];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xSTATE_NAME", STATE_NAME);
               oraParam[2] = new OracleParameter("xSTATE_OLD", STATE_OLD);


               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UpdLookuptblbystates", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void insLookuptblbystates(string STATE, string STATE_NAME)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xSTATE_NAME", STATE_NAME);

               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "insLookuptblbystates", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       public void delLookuptblbystates(string STATE, string STATE_NAME)
       {
           try
           {
               string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

               OracleConnection orcleCon = new OracleConnection(connection);
               OracleParameter[] oraParam = new OracleParameter[2];

               oraParam[0] = new OracleParameter("xSTATE", STATE);
               oraParam[1] = new OracleParameter("xSTATE_NAME", STATE_NAME);
               OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "delLookuptblbystates", oraParam);
           }
           catch (Exception ex)
           {
               throw ex;
           }

       }
       #endregion
    }
}
