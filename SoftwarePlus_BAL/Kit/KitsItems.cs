﻿using System;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;

namespace SoftwarePlus.BusinessAccess
{
    public class KitsItems
    {
        SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
        public DataSet GetKitItemsInfoByKitPartNo(string KIT_PART_NUMBER)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("xKIT_PART_NUMBER", KIT_PART_NUMBER);

                oraParam[1] = new OracleParameter("xKitItemsInfo", OracleDbType.RefCursor);

                oraParam[1].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetKitItemsInfo", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataSet GetRequiredKeys()
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();
                OracleParameter[] oraParam = new OracleParameter[1];

                oraParam[0] = new OracleParameter("xRequiredKeys", OracleDbType.RefCursor);
                oraParam[0].Direction = ParameterDirection.Output;

                DataSet ObjDS = OracleDataAccess.ExecuteDataset(connection,
                             CommandType.StoredProcedure,
                             "KITINFO.GetRequiredKeys", oraParam);

                return ObjDS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddKitItems(string kitPartNumber, string itemDescription, string itemType, string subAssembly, string location, string version, string speed, string checksum, string manufacturer, string fileFormat, string fileName, string labelContents, string packaging)
           
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[13];
                oraParam[0] = new OracleParameter("xKIT_PART_NUMBER", kitPartNumber);
                //oraParam[1] = new OracleParameter("xITEM_ID", itemId);
                oraParam[1] = new OracleParameter("xITEM_DESCRIPTION", itemDescription);
                oraParam[2] = new OracleParameter("xITEM_TYPE", itemType);
                oraParam[3] = new OracleParameter("xSUB_ASSEMBLY", subAssembly);
                oraParam[4] = new OracleParameter("xLOCATION", location);
                oraParam[5] = new OracleParameter("xVERSION", version);
                oraParam[6] = new OracleParameter("xSPEED", speed);
                oraParam[7] = new OracleParameter("xCHECK_SUM", checksum);
                oraParam[8] = new OracleParameter("xMANUFACTURER", manufacturer);
                oraParam[9] = new OracleParameter("xFILE_FORMAT", fileFormat);
                oraParam[10] = new OracleParameter("xFILE_NAME", fileName);
                oraParam[11] = new OracleParameter("xLABEL_CONTENTS", labelContents);
                oraParam[12] = new OracleParameter("xPACKAGING", packaging);


                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDKITITEMS", oraParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public void UpdateKitItems(string kitPartNumber, string itemDescription, string itemType, string subAssembly, string location, string version, string speed, string checksum, string manufacturer, string fileFormat, string fileName, string labelContents, string packaging)
        {
            try
            {
                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[13];
                oraParam[0] = new OracleParameter("xKIT_PART_NUMBER", kitPartNumber);
                //oraParam[1] = new OracleParameter("xITEM_ID", itemId);
                oraParam[1] = new OracleParameter("xITEM_DESCRIPTION", itemDescription);
                oraParam[2] = new OracleParameter("xITEM_TYPE", itemType);
                oraParam[3] = new OracleParameter("xSUB_ASSEMBLY", subAssembly);
                oraParam[4] = new OracleParameter("xLOCATION", location);
                oraParam[5] = new OracleParameter("xVERSION", version);
                oraParam[6] = new OracleParameter("xSPEED", speed);
                oraParam[7] = new OracleParameter("xCHECK_SUM", checksum);
                oraParam[8] = new OracleParameter("xMANUFACTURER", manufacturer);
                oraParam[9] = new OracleParameter("xFILE_FORMAT", fileFormat);
                oraParam[10] = new OracleParameter("xFILE_NAME", fileName);
                oraParam[11] = new OracleParameter("xLABEL_CONTENTS", labelContents);
                oraParam[12] = new OracleParameter("xPACKAGING", packaging);

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEKITITEMS", oraParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteKitItems(string kitPartNumber,string kitItemId)
        {
            try {

                string connection = softwareConfigMgr.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(connection);
                OracleParameter[] oraParam = new OracleParameter[2];

                oraParam[0] = new OracleParameter("xKIT_PART_NUMBER", kitPartNumber);
                oraParam[1] = new OracleParameter("xKIT_ITEM_ID", kitItemId);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETEKITITEMS", oraParam);
            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
