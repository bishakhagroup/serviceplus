﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public class Users
    {
        #region Public Properties
        public string UserId { get; set; }
        public DateTime StartDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserRole { get; set; }
        public int UserStatus { get; set; }
        public string OfficePhone { get; set; }
        public string Extension { get; set; }
        public string FaxNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string HomePhone { get; set; }
        public string VoiceMail { get; set; }
        public string Pager { get; set; }
        public string Cellular { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string UpdateBy { get; set; }
        public string Password { get; set; }
        public DateTime LastUpdatedDate { get; set; }


        #endregion

    }
}