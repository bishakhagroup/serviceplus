﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using SoftwarePlus.DataAccess;


namespace SoftwarePlus.BusinessAccess
{
    public class UsersDataManager
    {
        public UsersDataManager()
        {}

        public void GetUserDeatais(string uid,string email)
        { }

        public void AddUser(Users user)
        {
            try
            {

                configManager = new SoftwarePlusConfigManager();
                string con = configManager.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(con);
                OracleParameter[] oraParam = new OracleParameter[23];


                oraParam[0] = new OracleParameter("xUSerId", user.UserId);
                oraParam[1] = new OracleParameter("xStartDate", user.StartDate);
                oraParam[2] = new OracleParameter("xFirstName", user.FirstName);
                oraParam[3] = new OracleParameter("xLastName", user.LastName);
                oraParam[4] = new OracleParameter("xUserRole", user.UserRole);
                oraParam[5] = new OracleParameter("xUserStatus", user.UserStatus);
                if (user.OfficePhone == null)
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", string.Empty);
                }
                else
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", user.OfficePhone);
                }
                if (user.Extension == null)
                {
                    oraParam[7] = new OracleParameter("xExtension", string.Empty);
                }
                else
                {
                    oraParam[7] = new OracleParameter("xExtension", user.Extension);
                }
                if (user.FaxNumber == null)
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", string.Empty);
                }
                else
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", user.FaxNumber);
                }
                if (user.Address1 == null)
                {
                    oraParam[9] = new OracleParameter("xAddress1", string.Empty);
                }
                else
                {
                    oraParam[9] = new OracleParameter("xAddress1", user.Address1);
                }
                if (user.Address2 == null)
                {
                    oraParam[10] = new OracleParameter("xAddress2", string.Empty);
                }
                else
                {
                    oraParam[10] = new OracleParameter("xAddress2", user.Address1);
                }
                if (user.City == null)
                {
                    oraParam[11] = new OracleParameter("xCity", string.Empty);
                }
                else
                {
                    oraParam[11] = new OracleParameter("xCity", user.City);
                }
                if (user.State == null)
                {
                    oraParam[12] = new OracleParameter("xState", string.Empty);
                }
                else
                {
                    oraParam[12] = new OracleParameter("xState", user.State);
                }
                if (user.Zip == null)
                {
                    oraParam[13] = new OracleParameter("xZip", string.Empty);
                }
                else
                {
                    oraParam[13] = new OracleParameter("xZip", user.Zip);
                }
               
                oraParam[14] = new OracleParameter("xCountry", user.Country);
                if (user.HomePhone == null)
                {
                    oraParam[15] = new OracleParameter("xHomePhone", string.Empty);
                }
                else
                {
                    oraParam[15] = new OracleParameter("xHomePhone", user.HomePhone);
                }
                if (user.VoiceMail == null)
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", string.Empty);
                }
                else
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", user.VoiceMail);
                }
                if (user.Pager == null)
                {
                    oraParam[17] = new OracleParameter("xPager", string.Empty);
                }
                else
                {
                    oraParam[17] = new OracleParameter("xPager", user.Pager);
                }
                if (user.Cellular == null)
                {
                    oraParam[18] = new OracleParameter("xCellular", string.Empty);
                }
                else
                {
                    oraParam[18] = new OracleParameter("xCellular", user.Cellular);
                }
                if (user.Email == null)
                {
                    oraParam[19] = new OracleParameter("xEmail", string.Empty);
                }
                else
                {
                    oraParam[19] = new OracleParameter("xEmail", user.Email);
                }
                if (user.Notes == null)
                {
                    oraParam[20] = new OracleParameter("xNotes", string.Empty);
                }
                else
                {
                    oraParam[20] = new OracleParameter("xNotes", user.Notes);
                }
               
                oraParam[21] = new OracleParameter("xCreatedBy", user.CreatedBy);
                oraParam[22] = new OracleParameter("xDateCreated", user.DateCreated);
                //oraParam[23] = new OracleParameter("xUpdateBy", user.UpdateBy);
                //oraParam[24/] = new OracleParameter("xLastUpdatedDate", user.LastUpdatedDate);


                //oraParam[0] = new OracleParameter("xUSerId", user.UserId);
                //oraParam[1] = new OracleParameter("xStartDate", user.StartDate);
                //oraParam[2] = new OracleParameter("xFirstName", user.FirstName);
                //oraParam[3] = new OracleParameter("xLastName", user.LastName);
                //oraParam[4] = new OracleParameter("xUserRole", "System Administrator");
                //oraParam[5] = new OracleParameter("xUserStatus", -1);
                //oraParam[6] = new OracleParameter("xOfficePhone", "");
                //oraParam[7] = new OracleParameter("xExtension", "");
                //oraParam[8] = new OracleParameter("xFaxNumber", "");
                //oraParam[9] = new OracleParameter("xAddress1", "");
                //oraParam[10] = new OracleParameter("xAddress2", "");
                //oraParam[11] = new OracleParameter("xCity", "");
                //oraParam[12] = new OracleParameter("xState", "AB");
                //oraParam[13] = new OracleParameter("xZip", "");
                //oraParam[14] = new OracleParameter("xCountry", "Australia");
                //oraParam[15] = new OracleParameter("xHomePhone", "");
                //oraParam[16] = new OracleParameter("xVoiceMail", "");
                //oraParam[17] = new OracleParameter("xPager", "");
                //oraParam[18] = new OracleParameter("xCellular", "");
                //oraParam[19] = new OracleParameter("xEmail", user.Email);
                //oraParam[20] = new OracleParameter("xNotes", "");
                //oraParam[21] = new OracleParameter("xCreatedBy", "");
                //oraParam[22] = new OracleParameter("xDateCreated", user.DateCreated);
                //oraParam[23] = new OracleParameter("xUpdateBy", "");
                //oraParam[24] = new OracleParameter("xLastUpdatedDate", user.LastUpdatedDate);
                

                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "ADDUSER", oraParam);
            }
            catch(Exception ex)
            {
                errorLog = new XmlErrorLog();
                errorLog.LogError(ex, severity.Error.ToString());
            }
        }

        public void UpdateChildUser(Users user)
        {
            try
            {

                configManager = new SoftwarePlusConfigManager();
                string con = configManager.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(con);
                OracleParameter[] oraParam = new OracleParameter[25];

                oraParam[0] = new OracleParameter("xUSerId", user.UserId);
                oraParam[1] = new OracleParameter("xStartDate", user.StartDate);
                oraParam[2] = new OracleParameter("xFirstName", user.FirstName);
                oraParam[3] = new OracleParameter("xLastName", user.LastName);
                oraParam[4] = new OracleParameter("xUserRole", user.UserRole);
                oraParam[5] = new OracleParameter("xUserStatus", user.UserStatus);
                if (user.OfficePhone == null)
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", string.Empty);
                }
                else
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", user.OfficePhone);
                }
                if (user.Extension == null)
                {
                    oraParam[7] = new OracleParameter("xExtension", string.Empty);
                }
                else
                {
                    oraParam[7] = new OracleParameter("xExtension", user.Extension);
                }
                if (user.FaxNumber == null)
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", string.Empty);
                }
                else
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", user.FaxNumber);
                }
                if (user.Address1 == null)
                {
                    oraParam[9] = new OracleParameter("xAddress1", string.Empty);
                }
                else
                {
                    oraParam[9] = new OracleParameter("xAddress1", user.Address1);
                }
                if (user.Address2 == null)
                {
                    oraParam[10] = new OracleParameter("xAddress2", string.Empty);
                }
                else
                {
                    oraParam[10] = new OracleParameter("xAddress2", user.Address1);
                }
                if (user.City == null)
                {
                    oraParam[11] = new OracleParameter("xCity", string.Empty);
                }
                else
                {
                    oraParam[11] = new OracleParameter("xCity", user.City);
                }
                if (user.State == null)
                {
                    oraParam[12] = new OracleParameter("xState", string.Empty);
                }
                else
                {
                    oraParam[12] = new OracleParameter("xState", user.State);
                }
                if (user.Zip == null)
                {
                    oraParam[13] = new OracleParameter("xZip", string.Empty);
                }
                else
                {
                    oraParam[13] = new OracleParameter("xZip", user.Zip);
                }

                oraParam[14] = new OracleParameter("xCountry", user.Country);
                if (user.HomePhone == null)
                {
                    oraParam[15] = new OracleParameter("xHomePhone", string.Empty);
                }
                else
                {
                    oraParam[15] = new OracleParameter("xHomePhone", user.HomePhone);
                }
                if (user.VoiceMail == null)
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", string.Empty);
                }
                else
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", user.VoiceMail);
                }
                if (user.Pager == null)
                {
                    oraParam[17] = new OracleParameter("xPager", string.Empty);
                }
                else
                {
                    oraParam[17] = new OracleParameter("xPager", user.Pager);
                }
                if (user.Cellular == null)
                {
                    oraParam[18] = new OracleParameter("xCellular", string.Empty);
                }
                else
                {
                    oraParam[18] = new OracleParameter("xCellular", user.Cellular);
                }
                if (user.Email == null)
                {
                    oraParam[19] = new OracleParameter("xEmail", string.Empty);
                }
                else
                {
                    oraParam[19] = new OracleParameter("xEmail", user.Email);
                }
                if (user.Notes == null)
                {
                    oraParam[20] = new OracleParameter("xNotes", string.Empty);
                }
                else
                {
                    oraParam[20] = new OracleParameter("xNotes", user.Notes);
                }

                oraParam[21] = new OracleParameter("xCreatedBy", user.CreatedBy);
                oraParam[22] = new OracleParameter("xDateCreated", user.DateCreated);
                //oraParam[23] = new OracleParameter("xUpdateBy", user.UpdateBy);
               // oraParam[24] = new OracleParameter("xLastUpdatedDate", user.LastUpdatedDate);


                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEUSER", oraParam);
            }
            catch (Exception ex)
            {
                errorLog = new XmlErrorLog();
                errorLog.LogError(ex, severity.Error.ToString());
            }
        }
        public void UpdateUser(Users user)
        {
            try
            {

                configManager = new SoftwarePlusConfigManager();
                string con = configManager.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(con);
                OracleParameter[] oraParam = new OracleParameter[25];

                oraParam[0] = new OracleParameter("xUSerId", user.UserId);
                oraParam[1] = new OracleParameter("xStartDate", user.StartDate);
                oraParam[2] = new OracleParameter("xFirstName", user.FirstName);
                oraParam[3] = new OracleParameter("xLastName", user.LastName);
                oraParam[4] = new OracleParameter("xUserRole", user.UserRole);
                oraParam[5] = new OracleParameter("xUserStatus", user.UserStatus);
                if (user.OfficePhone == null)
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", string.Empty);
                }
                else
                {
                    oraParam[6] = new OracleParameter("xOfficePhone", user.OfficePhone);
                }
                if (user.Extension == null)
                {
                    oraParam[7] = new OracleParameter("xExtension", string.Empty);
                }
                else
                {
                    oraParam[7] = new OracleParameter("xExtension", user.Extension);
                }
                if (user.FaxNumber == null)
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", string.Empty);
                }
                else
                {
                    oraParam[8] = new OracleParameter("xFaxNumber", user.FaxNumber);
                }
                if (user.Address1 == null)
                {
                    oraParam[9] = new OracleParameter("xAddress1", string.Empty);
                }
                else
                {
                    oraParam[9] = new OracleParameter("xAddress1", user.Address1);
                }
                if (user.Address2 == null)
                {
                    oraParam[10] = new OracleParameter("xAddress2", string.Empty);
                }
                else
                {
                    oraParam[10] = new OracleParameter("xAddress2", user.Address1);
                }
                if (user.City == null)
                {
                    oraParam[11] = new OracleParameter("xCity", string.Empty);
                }
                else
                {
                    oraParam[11] = new OracleParameter("xCity", user.City);
                }
                if (user.State == null)
                {
                    oraParam[12] = new OracleParameter("xState", string.Empty);
                }
                else
                {
                    oraParam[12] = new OracleParameter("xState", user.State);
                }
                if (user.Zip == null)
                {
                    oraParam[13] = new OracleParameter("xZip", string.Empty);
                }
                else
                {
                    oraParam[13] = new OracleParameter("xZip", user.Zip);
                }

                oraParam[14] = new OracleParameter("xCountry", user.Country);
                if (user.HomePhone == null)
                {
                    oraParam[15] = new OracleParameter("xHomePhone", string.Empty);
                }
                else
                {
                    oraParam[15] = new OracleParameter("xHomePhone", user.HomePhone);
                }
                if (user.VoiceMail == null)
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", string.Empty);
                }
                else
                {
                    oraParam[16] = new OracleParameter("xVoiceMail", user.VoiceMail);
                }
                if (user.Pager == null)
                {
                    oraParam[17] = new OracleParameter("xPager", string.Empty);
                }
                else
                {
                    oraParam[17] = new OracleParameter("xPager", user.Pager);
                }
                if (user.Cellular == null)
                {
                    oraParam[18] = new OracleParameter("xCellular", string.Empty);
                }
                else
                {
                    oraParam[18] = new OracleParameter("xCellular", user.Cellular);
                }
                if (user.Email == null)
                {
                    oraParam[19] = new OracleParameter("xEmail", string.Empty);
                }
                else
                {
                    oraParam[19] = new OracleParameter("xEmail", user.Email);
                }
                if (user.Notes == null)
                {
                    oraParam[20] = new OracleParameter("xNotes", string.Empty);
                }
                else
                {
                    oraParam[20] = new OracleParameter("xNotes", user.Notes);
                }

                oraParam[21] = new OracleParameter("xCreatedBy", user.CreatedBy);
                oraParam[22] = new OracleParameter("xDateCreated", user.DateCreated);
                oraParam[23] = new OracleParameter("xUpdateBy", user.UpdateBy);
                oraParam[24] = new OracleParameter("xLastUpdatedDate", user.LastUpdatedDate);


                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "UPDATEUSER", oraParam);
            }
            catch (Exception ex)
            {
                errorLog = new XmlErrorLog();
                errorLog.LogError(ex, severity.Error.ToString());
            }
        }

        public void DeleteUser(string userId)
        {
            try 
            {
                configManager = new SoftwarePlusConfigManager();
                string con = configManager.SoftwarePlusDBConnectionString();

                OracleConnection orcleCon = new OracleConnection(con);
                OracleParameter[] oraParam = new OracleParameter[1];

                oraParam[0] = new OracleParameter("xUSerId", userId);
                OracleDataAccess.ExecuteNonQuery(orcleCon, CommandType.StoredProcedure, "DELETEUSER", oraParam);
            }
            catch (Exception ex)
            {
                errorLog = new XmlErrorLog();
                errorLog.LogError(ex, severity.Error.ToString());
            }
        
        }
        public object GetUserInfo(string userId)
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[2];


            oraParam[0] = new OracleParameter("xUserId", userId);
            oraParam[1] = new OracleParameter("xUserInfo", OracleDbType.RefCursor);

            oraParam[1].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "SWUSERS.GetUserInfo", oraParam);
            Users userData = new Users();
             DataTable dt = ObjDS.Tables[0];
             foreach (DataRow row in dt.Rows)
             {
                 
                    userData.UserId =   row["USER_ID"].ToString();
                   if(row["START_DATE"] != null)
                   {
                    userData.StartDate = Convert.ToDateTime(row["START_DATE"]);
                   }
                        userData.FirstName =  Convert.ToString(row["FIRST_NAME"]);
                        userData.LastName  =  Convert.ToString(row["LAST_NAME"]);//.ToString(); 
                        userData.UserRole =  Convert.ToString(row["USER_POSITION"]);
                        userData.UserStatus = Convert.ToInt32(row["USER_STATUS"]);
                        userData.OfficePhone = Convert.ToString(row["OFFICE_PHONE"]);
                        userData.Extension = Convert.ToString(row["EXTENSION"]);
                        userData.FaxNumber = Convert.ToString(row["FAX_NUMBER"]);
                        userData.Address1 = Convert.ToString(row["ADDRESS"]);
                        userData.Address2 = Convert.ToString(row["ADDRESS_2"]);
                        userData.City =  Convert.ToString(row["CITY"]);
                       userData.State = Convert.ToString(row["STATE"]);
                       userData.Zip = Convert.ToString(row["ZIP"]);
                       userData.Country = Convert.ToString(row["COUNTRY"]);
                       userData.HomePhone = Convert.ToString(row["HOME_PHONE"]);
                       userData.VoiceMail = Convert.ToString(row["VOICE_MAIL"]);
                       userData.Pager =  Convert.ToString(row["PAGER"]);
                       userData.Cellular = Convert.ToString(row["CELLULAR"]);
                       userData.Email = Convert.ToString(row["E_MAIL"]);
                       userData.Notes = Convert.ToString(row["NOTES"]);
                       userData.CreatedBy = Convert.ToString(row["CREATED_BY"]);
                 if(row["DATE_CREATED"] != null)
                 {
               userData.DateCreated = Convert.ToDateTime(row["DATE_CREATED"]);

                 }
                 userData.UpdateBy = Convert.ToString(row["UPDATED_BY"]);
                 if (!String.IsNullOrEmpty(row["LAST_UPDATED"].ToString()))
                 {
                 userData.LastUpdatedDate = Convert.ToDateTime(row["LAST_UPDATED"]);

                 }
             

             }

            return userData;


        }

        public int ValidateUserData(string emailId)
        {
            string retValue = string.Empty;
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            //OracleParameter[] oraParam = new OracleParameter[2];
            OracleConnection connection = new OracleConnection(con);
           OracleCommand com = new OracleCommand();
        
           // oraParam[0] = new OracleParameter("xEmailAddress", emailId);
           //oraParam[1] = new OracleParameter("retVal", OracleDbType.Int32);

           //oraParam[1].Direction = ParameterDirection.Output;

           com.Parameters.Add("xEmailAddress", OracleDbType.Varchar2);
           com.Parameters["xEmailAddress"].Value = emailId;

          OracleParameter oraParam = new OracleParameter("retVal", OracleDbType.Int32);
          oraParam.Direction = ParameterDirection.Output;
          com.Parameters.Add(oraParam);
          com.Connection = connection;
          com.CommandType = CommandType.StoredProcedure;
          com.CommandText = "VALIDATEUSERDATA";
          connection.Open();
          com.ExecuteNonQuery();
          // retValue = OracleDataAccess.ExecuteNonQuery(connString, CommandType.StoredProcedure, "VALIDATEUSERDATA", oraParam);
          retValue = com.Parameters["retVal"].Value.ToString();
           connection.Close();
            return Convert.ToInt32(retValue);
        }

        //public void AddUser(Users user)
        //{
        //    try 
        //    { 
            
        //    }
        //    catch(Exception ex)
        //    {
        //        errorLog = new XmlErrorLog();
        //        errorLog.LogError(ex, severity.Error.ToString());
        //    }
        
        //}

        public DataSet GetStatesInfo()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];



            oraParam[0] = new OracleParameter("xStatesInfo", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "SWUSERS.GetStatesInfo", oraParam);

            return ObjDS;
        }
        public DataSet GetCountryInfo()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];



            oraParam[0] = new OracleParameter("xCountryInfo", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "SWUSERS.GetCountryInfo", oraParam);

            return ObjDS;
        }
        public DataSet GetUserIds()
        {
            configManager = new SoftwarePlusConfigManager();
            string con = configManager.SoftwarePlusDBConnectionString();
            OracleParameter[] oraParam = new OracleParameter[1];



            oraParam[0] = new OracleParameter("xUserIds", OracleDbType.RefCursor);

            oraParam[0].Direction = ParameterDirection.Output;

            DataSet ObjDS = OracleDataAccess.ExecuteDataset(con,
                         CommandType.StoredProcedure,
                         "SWUSERS.GetUserIds", oraParam);

            return ObjDS;
        }
     SoftwarePlusConfigManager configManager;
     XmlErrorLog errorLog;
    }
}