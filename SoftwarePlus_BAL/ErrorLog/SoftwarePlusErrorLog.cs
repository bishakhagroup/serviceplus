﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Web;

namespace SoftwarePlus.BusinessAccess
{
    public enum severity
    {
        Error, Message
    }
    public class XmlErrorLog
    {
        public XmlErrorLog()
        { }
       
        public void LogError(Exception ex, string type)
        {
            switch (type)
            {
                case "Error":
                  LogException(ex, type);
           
                    break;
                case "Message":
                default:
                    break;
            }


        }
        private void LogException(Exception ex, string type)
        {
            SoftwarePlusConfigManager mgr = new  SoftwarePlusConfigManager();
            string filePath = mgr.SoftwarePlusErrorLogFile() + "SoftwarePlusErrorLog.xml";
            if (!File.Exists(filePath))
            {

                XmlTextWriter textWritter = new XmlTextWriter(filePath, null);
                textWritter.WriteStartDocument();
                textWritter.WriteStartElement("SoftwarePlusError");
                textWritter.WriteEndElement();
                textWritter.Close();
            }

           

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(filePath);
            XmlElement subRoot = xmlDoc.CreateElement("Error");
            //UserName
            XmlElement appendedElementUsername = xmlDoc.CreateElement("UserName");
            XmlText xmlTextUserName = xmlDoc.CreateTextNode(HttpContext.Current.Session["userId"].ToString());
            appendedElementUsername.AppendChild(xmlTextUserName);
            subRoot.AppendChild(appendedElementUsername);
            xmlDoc.DocumentElement.AppendChild(subRoot);



            //Date

            XmlElement appendedElementDate = xmlDoc.CreateElement("Date");
            XmlText xmlTextDate = xmlDoc.CreateTextNode(System.DateTime.Now.ToString());
            appendedElementDate.AppendChild(xmlTextDate);
            subRoot.AppendChild(appendedElementDate);
            xmlDoc.DocumentElement.AppendChild(subRoot);


            XmlElement appendedElementSeverity = xmlDoc.CreateElement("Severity");
            XmlText xmlTextseverity = xmlDoc.CreateTextNode(type);
            appendedElementSeverity.AppendChild(xmlTextseverity);
            subRoot.AppendChild(appendedElementSeverity);
            xmlDoc.DocumentElement.AppendChild(subRoot);


            XmlElement appendedElementMessage = xmlDoc.CreateElement("Message");
            XmlText xmlTextMessage = xmlDoc.CreateTextNode(ex.Message);
            appendedElementMessage.AppendChild(xmlTextMessage);
            subRoot.AppendChild(appendedElementMessage);
            xmlDoc.DocumentElement.AppendChild(subRoot);


            XmlElement appendedElementstacktrace = xmlDoc.CreateElement("Stacktrace");
            XmlText xmlTextstacktrace = xmlDoc.CreateTextNode(ex.StackTrace);
            appendedElementstacktrace.AppendChild(xmlTextstacktrace);
            subRoot.AppendChild(appendedElementstacktrace);
            xmlDoc.DocumentElement.AppendChild(subRoot);

            xmlDoc.Save(filePath);
           
        }
       // string filePath = "c:\\error.xml";
        private void LogMessage()
        {

        }
       
    }
}