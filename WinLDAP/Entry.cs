using System;
using System.Collections;

namespace WinLDAP
{
	public class Entry
	{
		private Hashtable m_hshtAttributes=null;
		public string DistinguishedName = "";

		public Entry()
		{
			m_hshtAttributes = new Hashtable();
		}

		public string[] GetAttributeValues(string strAttributeName)
		{
			if (!m_hshtAttributes.ContainsKey(strAttributeName.ToUpper()))
				throw new System.ApplicationException("Attribute " + strAttributeName + " is not a valid attribute for LDAPEntry.");

			ArrayList lstValues = (ArrayList)m_hshtAttributes[strAttributeName.ToUpper()];
			string[] arrValues = new string[lstValues.Count];
			lstValues.CopyTo(arrValues);
			return arrValues;
		}

		public void SetAttributeValue(string strAttributeName, string strValue)
		{
			if (m_hshtAttributes.ContainsKey(strAttributeName.ToUpper()))
			{
				ArrayList lstValues = (ArrayList)m_hshtAttributes[strAttributeName.ToUpper()];
				lstValues.Add(strValue);
			}
			else
			{
				ArrayList lstValues = new ArrayList();
				lstValues.Add(strValue);
				m_hshtAttributes.Add(strAttributeName.ToUpper(), lstValues);
			}
		}

		public Hashtable Attributes
		{
			get
			{
				return (Hashtable)m_hshtAttributes.Clone();
			}
		}
	}
}
