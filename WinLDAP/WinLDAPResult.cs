using System;

namespace WinLDAP
{
	public struct WinLDAPResult
	{
		public string ReturnMessage;
		public int ReturnCode;
	}
}
