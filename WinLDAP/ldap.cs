using System;
using System.Runtime.InteropServices;
using System.Collections;
using Sony.US.SIAMUtilities;
using System.Diagnostics;
using System.Threading;
using System.DirectoryServices.Protocols;
namespace WinLDAP
{
    public enum SearchScope { LDAP_SCOPE_BASE = 0, LDAP_SCOPE_ONELEVEL = 1, LDAP_SCOPE_SUBTREE = 2 };
    public enum ModOperations { LDAP_MOD_ADD = 0, LDAP_MOD_REPLACE = 1, LDAP_MOD_DELETE = 2 };


    public class ldap
    {

        /// <summary>
        /// 
        /// </summary>
        //private SonyLogger Logger = new SonyLogger();
        private const string ClassName = "WinLDAP.ldap";
        private System.IntPtr _ldapHandle = (System.IntPtr)0;
        private string _host;
        private int _port;
        private string _TargetOU = string.Empty;

        NetLDAP objNetLDAP = new NetLDAP();

        public ldap(string Host, int Port)
        {
            _host = Host;
            _port = Port;
            //if (_ldapHandle.ToInt32() == 0)
            //{
            //    try
            //    {
            //        _ldapHandle = ldap_init(_host,_port);
            //    }
            //    catch
            //    {
            //        ldap_unbind(_ldapHandle);
            //        _ldapHandle = ldap_init(_host,_port);
            //        int x = LdapGetLastError();
            //    }
            //}

            //for (int x = 0; x <= ConfigurationData.Environment.UserTypes.GetUpperBound(0); x++)
            //{
            //    //if (config.UserTypes[x].Name == UserType)
            //    if (ConfigurationData.Environment.UserTypes[x].Name == "AccountHolder")
            //    {
            //        //if (config.UserTypes[x].AdministrationDelegates != null)
            //        if (ConfigurationData.Environment.UserTypes[x].AdministrationDelegates != null)
            //        {
            //            for (int y = 0; y <= ConfigurationData.Environment.UserTypes[x].AdministrationDelegates.GetUpperBound(0); y++)
            //            {
            //                for (int z = 0; z <= ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs.GetUpperBound(0); z++)
            //                {
            //                    //Args[z] = config.UserTypes[x].AdministrationDelegates[y].ConstructorArgs[z].Value;
            //                   _TargetOU= ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs[z].Value.ToString();
            //                }
            //            }
            //        }
            //    }
            //}

        }

        public WinLDAP.WinLDAPResult Bind(string DN, string Password, int nTry)
        {
            WinLDAP.WinLDAPResult result = NetBind(DN, Password, nTry);

            /*

            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);


            WinLDAP.WinLDAPResult result = new WinLDAPResult();
            try
            {
                // On a bind Op, the result will be -1 if it failed and the Message ID otherwise. This causes
                // a bit of confusion because on most other LDAP calls the return code is a true result code.
                //Logger.RaiseTrace(ClassName,"WinLDAP Bind Attemp with = " + DN + "," + Password,TraceLevel.Verbose);
                result.ReturnCode = ldap_simple_bind_s(_ldapHandle,DN,Password);
                int	n = 0;
                while (result.ReturnCode != 0 && n < nTry)
                {
                    Thread.Sleep(2); //wait for 2 milliseconds
                    result.ReturnCode = ldap_simple_bind_s(_ldapHandle,DN,Password);
                    n++;
                }
                result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes),result.ReturnCode); // 0 is the LDAP success code.
                //Logger.RaiseTrace(ClassName,"WinLDAP Bind Attemp result = " + result.ReturnMessage,TraceLevel.Verbose);
            }
            catch(System.Exception ex)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message + " " + ex.InnerException.Message;
            }

            // Log Data
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            */
            return result;

        }

        public WinLDAP.WinLDAPResult UnBind()
        {
            WinLDAP.WinLDAPResult result = NetUnBind();

            /*
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);

            WinLDAP.WinLDAPResult result = new WinLDAPResult();

            try
            {
                result.ReturnCode = ldap_unbind(_ldapHandle);
                result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes),result.ReturnCode);
            }
            catch(System.Exception ex)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message + " " + ex.InnerException.Message;
            }

            // Log Data
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            */
            return result;
        }

        public unsafe WinLDAP.WinLDAPSearchResults Search(string root, SearchScope scope, string filter)
        {
            WinLDAP.WinLDAPSearchResults results = NetSearch(root, scope, filter);
            /*
             
			// Log Data
			TimeSpan start = TimeKeeper.GetCurrentTime();
			//Logger.RaiseTrace(ClassName,"Entry:"+start.ToString(),TraceLevel.Verbose);
			
			WinLDAP.WinLDAPSearchResults results = new WinLDAPSearchResults();
			ArrayList lstEntries = new ArrayList();
			int* Results;

			try
			{
				results.ReturnCode = ldap_search_s(_ldapHandle, root, Convert.ToUInt32(scope),filter, null, 0, &Results);
				results.Message = System.Enum.GetName(typeof(WinLDAP.returnCodes),results.ReturnCode);
				if(results.ReturnCode != 0)
				{
					return results;
				}
				else
				{
					for (IntPtr pEntry = ldap_first_entry(_ldapHandle, Results); pEntry.ToInt32() != 0; pEntry = ldap_next_entry(_ldapHandle, pEntry))
					{
						Entry entry = new Entry();
						sbyte* pDN = ldap_get_dn(_ldapHandle, pEntry);
						if (null != pDN)
						{
							entry.DistinguishedName = new string(pDN);
							ldap_memfree(pDN);
						}

						int* pBerElement;
						for (sbyte* pAttribute = ldap_first_attribute(_ldapHandle, pEntry, &pBerElement);
							pAttribute != null; 
							pAttribute = ldap_next_attribute(_ldapHandle, pEntry, pBerElement))
						{
							sbyte** pValues = ldap_get_values(_ldapHandle, pEntry, pAttribute);
							if (pValues != null)
							{
								for (int nX = 0; pValues[nX] != null; ++nX)
								{
									entry.SetAttributeValue(new string(pAttribute), new string(pValues[nX]));
								}

								ldap_value_free(pValues);
							}

							ldap_memfree(pAttribute);
						}

						if (null != pBerElement)
							ber_free(pBerElement, 0);

						lstEntries.Add(entry);
					}
				}
				ldap_msgfree(Results);
				Entry[] arrEntries = new Entry[lstEntries.Count];
				lstEntries.CopyTo(arrEntries);
				results.LDAPEntries = arrEntries;
			}
			catch(System.Exception ex)
			{
				results.ReturnCode = -1;
				results.Message = ex.Message + " " + ex.InnerException.Message;
			}

			// Log Data
			TimeSpan end = TimeKeeper.GetCurrentTime();
			TimeSpan exectime = start.Subtract(end);
			//Logger.RaiseTrace(ClassName,"Exit:ExecutionTime="+exectime.ToString(),TraceLevel.Verbose);
            */
            return results;
        }

        public WinLDAP.WinLDAPResult Modify(string EntryDN, string Attribute, string NewValue)
        {
            // Log Data
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            WinLDAP.WinLDAPResult result = new WinLDAPResult();
            try
            {
                result = NetModify(EntryDN, Attribute, NewValue);
                //result.ReturnCode = 0; // LDAPModify(_ldapHandle,Attribute,NewValue,EntryDN);
                //result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes),result.ReturnCode);
            }
            catch (LdapException eLDAP)
            {
                result.ReturnCode = eLDAP.ErrorCode;
                result.ReturnMessage = eLDAP.Message;
            }
            catch (Exception ex)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message;
            }


            // Log Data
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);

            return result;
        }


        public WinLDAPResult NetBind(string DN, string Password, int nTry)
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            WinLDAP.WinLDAPResult result = new WinLDAPResult();

            try
            {
                //Logger.RaiseTrace(ClassName, "WinLDAP Bind Attemp with = " + DN + "," + Password, TraceLevel.Verbose);
                NetLDAP.CreateConnection(DN, Password, _host, _port);
                int n = 0;
                while (n < nTry)
                {
                    Thread.Sleep(2);
                    try
                    {
                        NetLDAP.CreateConnection(DN, Password, _host, _port);
                        n++;
                    }
                    catch (Exception exBind)
                    {
                        n++;
                        if (n >= nTry) throw; //exBind;
                    }
                }
                result.ReturnCode = 0;
                result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes), result.ReturnCode); // 0 is the LDAP success code.
                //Logger.RaiseTrace(ClassName, "WinLDAP Bind Attemp result = " + result.ReturnMessage, TraceLevel.Verbose);
            }
            catch (LdapException eLDAP)
            {
                result.ReturnCode = eLDAP.ErrorCode;
                result.ReturnMessage = eLDAP.Message;
            }
            catch (Exception ex)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message;
            }

            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);

            return result;

        }

        public WinLDAP.WinLDAPResult NetUnBind()
        {
            WinLDAP.WinLDAPResult result = new WinLDAPResult();
            try
            {
                NetLDAP.CloseConnection();
                result.ReturnCode = 0;
                result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes), result.ReturnCode);
            }
            catch (LdapException eLDAP)
            {
                result.ReturnCode = eLDAP.ErrorCode;
                result.ReturnMessage = eLDAP.Message;
            }
            
            catch (Exception ex)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = ex.Message;
            }
            return result;

        }

        public WinLDAPResult NetModify(string EntryDN, string Attribute, string NewValue)
        {
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            WinLDAP.WinLDAPResult result = new WinLDAPResult();

            try
            {
                NetLDAP.Modify(Attribute, NewValue, EntryDN);
                result.ReturnCode = 0;
                result.ReturnMessage = System.Enum.GetName(typeof(WinLDAP.returnCodes), result.ReturnCode); // 0 is the LDAP success code.
            }
            catch (LdapException eLDAP)
            {
                result.ReturnCode = eLDAP.ErrorCode;
                result.ReturnMessage = eLDAP.Message;
            }
            catch (Exception exModify)
            {
                result.ReturnCode = -1;
                result.ReturnMessage = exModify.Message;
            }
            return result;
        }

        public unsafe WinLDAP.WinLDAPSearchResults NetSearch(string root, SearchScope scope, string filter)
        {

            //rgs[z] = ConfigurationData.Environment.UserTypes[x].AdministrationDelegates[y].ConstructorArgs[z].Value;
            TimeSpan start = TimeKeeper.GetCurrentTime();
            //Logger.RaiseTrace(ClassName, "Entry:" + start.ToString(), TraceLevel.Verbose);

            WinLDAP.WinLDAPSearchResults results = new WinLDAPSearchResults();
            Entry[] lstEntries = null;
            try
            {
                System.DirectoryServices.Protocols.SearchScope objScope = System.DirectoryServices.Protocols.SearchScope.Subtree;
                switch (scope)
                {
                    case SearchScope.LDAP_SCOPE_BASE:
                        objScope = System.DirectoryServices.Protocols.SearchScope.Base;
                        break;
                    case SearchScope.LDAP_SCOPE_ONELEVEL:
                        objScope = System.DirectoryServices.Protocols.SearchScope.OneLevel;
                        break;
                    case SearchScope.LDAP_SCOPE_SUBTREE:
                        objScope = System.DirectoryServices.Protocols.SearchScope.Subtree;
                        break;
                }

                lstEntries = NetLDAP.Search(filter, root, objScope, null);
                results.LDAPEntries = lstEntries;
                results.ReturnCode = 0;
                results.Message = System.Enum.GetName(typeof(WinLDAP.returnCodes), results.ReturnCode); // 0 is the LDAP success code.
            }
            catch (LdapException eLDAP)
            {
                results.ReturnCode = eLDAP.ErrorCode;
                results.Message = eLDAP.Message;
            }
            catch (Exception ex)
            {
                results.ReturnCode = -1;
                results.Message = ex.Message;
            }

            // Log Data
            TimeSpan end = TimeKeeper.GetCurrentTime();
            TimeSpan exectime = start.Subtract(end);
            //Logger.RaiseTrace(ClassName, "Exit:ExecutionTime=" + exectime.ToString(), TraceLevel.Verbose);

            return results;
        }

    }


    /*
    #region WinLDAP Struct

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct LDAPMod
    {
        public int mod_op;
        public string mod_type;
        public IntPtr mod_vals;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct berval
    {
        public int bv_len;
        IntPtr bv_val;
    }

    #endregion

    #region Dll Imports

    [DllImport("LDAPAddWrapper.dll")]
    private static extern unsafe int LDAPModify(System.IntPtr pLDAP, [MarshalAs(UnmanagedType.LPStr)]string Attribute, [MarshalAs(UnmanagedType.LPStr)]string NewValue, [MarshalAs(UnmanagedType.LPStr)]string strDN);

    [DllImport("wldap32.dll")]
    private static extern unsafe int ldap_search_s(IntPtr ldap,[MarshalAs(UnmanagedType.LPStr)] string strBase,uint ulScope,[MarshalAs(UnmanagedType.LPStr)] string strFilter,[MarshalAs(UnmanagedType.LPStr)] string strAttributes,uint ulAttributesOnly,int** Results);

    [DllImport("wldap32.dll")]
    private static extern int LdapGetLastError();

    [DllImport("wldap32.dll")]
    private static extern int ldap_unbind(System.IntPtr ldap);

    [DllImport("wldap32.dll")]
    private static extern int ldap_simple_bind_s(System.IntPtr ldap,[MarshalAs(UnmanagedType.LPStr)] string DN,[MarshalAs(UnmanagedType.LPStr)] string password);

    [DllImport("wldap32.dll")]
    private static extern System.IntPtr ldap_init([MarshalAs(UnmanagedType.LPStr)]	string host,int port);
	
    [DllImport("wldap32.dll")]
    private static extern unsafe IntPtr ldap_first_entry(IntPtr ldap, int* ldapMsg);

    [DllImport("wldap32.dll")]
    private static extern unsafe IntPtr ldap_next_entry(IntPtr ldap, IntPtr ldapMsg);

    [DllImport("wldap32.dll")]
    private static extern unsafe sbyte* ldap_get_dn(IntPtr ldap, IntPtr entry);

    [DllImport("wldap32.dll")]
    private static extern unsafe void ldap_memfree(sbyte* block);

    [DllImport("wldap32.dll")]
    private static extern unsafe sbyte* ldap_first_attribute(IntPtr ldap, IntPtr entry, int** berElement);

    [DllImport("wldap32.dll")]
    private static extern unsafe sbyte* ldap_next_attribute(IntPtr ldap, IntPtr entry, int* berElement);

    [DllImport("wldap32.dll")]
    private static extern unsafe uint ldap_value_free(sbyte** pVals);

    [DllImport("wldap32.dll")]
    private static extern unsafe void ber_free(int* pBerElement, int fBuf);

    [DllImport("wldap32.dll")]
    private static extern unsafe uint ldap_msgfree(int* Results);

    [DllImport("wldap32.dll")]
    private static extern unsafe sbyte** ldap_get_values(IntPtr ldap, IntPtr pEntry,sbyte* pAttribute);

    [DllImport("wldap32.dll")]
    private static extern unsafe uint ldap_modify_s(System.IntPtr ldap, [MarshalAs(UnmanagedType.LPStr)]string dn, [MarshalAs(UnmanagedType.LPArray)]LDAPMod[] mods);




    #endregion

*/

    //	public struct LDAPMod 
    //	{  
    //		public long mod_op;
    //		public string ModType;
    //
    //
    // 		[MarshalAs(UnmanagedType.LPStr)]
    //		[FieldOffset(0)]public string mod_type;
    //
    //
    //		[MarshalAs(UnmanagedType.LPArray)]
    //		[FieldOffset(0)]public string[] newValues;
    //
    //	}
}
