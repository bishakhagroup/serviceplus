﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicesPlusException
{
    public class ServicesPLUSUIException : ServicesPLUSBaseException 
    {
        public ServicesPLUSUIException(string ErrorDescription ,Exception objException)
        {
            if (!String.IsNullOrEmpty(ErrorDescription))
            {
                this.InitializeExceptionForMessage(ErrorDescription, objException);
            }
            else
            {
                throw new ArgumentNullException() ;
            }
        }
    }
}
