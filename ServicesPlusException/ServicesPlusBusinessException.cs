﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicesPlusException
{
   public class ServicesPlusBusinessException : ServicesPLUSBaseException 
    {
         public ServicesPlusBusinessException(string ErrorId, Exception objException)
        {
            if (!String.IsNullOrEmpty(ErrorId))
            {
                this.InitializeException("ServicesPlusBusinessException", ErrorId, objException);
            }
            else
            {
                throw new ArgumentNullException() ;
            }
        }
         public ServicesPlusBusinessException(string ErrorDescription)
        {
            if (!String.IsNullOrEmpty(ErrorDescription))
            {
                this.InitializeException(ErrorDescription, new ServicesPLUSBaseException());
            }
            else
            {
                throw new ArgumentNullException();
            }
        }
    }
}
