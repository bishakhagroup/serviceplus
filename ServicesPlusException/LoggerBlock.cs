﻿using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace ServicesPlusException {
    public class LoggerBlock {
        protected LogWriter logWriter = null;
        public LogWriter LogWriter => logWriter;

        public LoggerBlock() {
            if (logWriter == null)
                InitLogging();
        }

        public void InitLogging() {
            logWriter = new LogWriterFactory().Create();
            Logger.SetLogWriter(logWriter, false);
        }
    }
}
