﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data;


namespace ServicesPlusException {
    public static class ERRORCODE {
        public static readonly string wM109 = "wM-109";
        public static readonly string ora06550 = "ORA-06550";
        public static readonly string SEP0001 = "SEP0001";
        //test
    }

    /// <summary>
    ///  ServicesPLUSBaseException class is used to show ErrorID , ErrorDescription and also the error code
    /// </summary>
    public class ServicesPLUSBaseException : ApplicationException {
        private string strErrorID;
        private string strErrorDescription;
        private Exception mInnerException;
        private string strErrorCode;  // Contains GUID that needs to be displayed to the user
        private bool? _displayIncidentNumber; //added by satheesh for 9019 change customer error message on popup

        /// <summary>
        /// Error Code is used for Custom Error ID
        /// </summary>
        public string ErrorCode {
            get { return strErrorCode; }
            set { strErrorCode = value; }
        }
        public Boolean logExceptionToFile {
            set;
            get;
        }
        public Boolean setcustomMessage {
            set;
            get;
        }
        public Boolean hideContactus {
            set;
            get;
        }
        public string errorMessage {
            set;
            get;
        }
        public bool? DisplayIncidentNumber {
            get {
                return _displayIncidentNumber;
            }
            set {
                _displayIncidentNumber = value;
            }
        }
        /// <summary>
        /// Inner Exception will be defined to retrieve the actual exception 
        /// </summary>
        public Exception MInnerException {
            get { return mInnerException; }
            set { mInnerException = value; }
        }

        /// <summary>
        /// Describes the actual error for the description
        /// </summary>
        public string ErrorDescription {
            get { return strErrorDescription; }
            set { strErrorDescription = value; }
        }

        /// <summary>
        /// Describes the Error Id 
        /// </summary>
        public string ErrorID {
            get { return strErrorID; }
            set { strErrorID = value; }
        }

        /// <summary>
        /// Initialize Exception for the Message 
        /// </summary>
        protected void InitializeExceptionForMessage(string strMessageValue, Exception objException) {
            this.ErrorDescription = strMessageValue;
            this.mInnerException = objException;
        }

        /// <summary>
        /// Initialize Exception for the Error Id 
        /// </summary>
        protected void InitializeException(string ErrorGroup, string messageId, Exception objException) {
            this.ErrorID = messageId;
            this.ErrorDescription = GetMessage(ErrorGroup, messageId);
            this.mInnerException = objException;
        }

        /// <summary>
        /// Initialize Exception for the Error Id 
        /// </summary>
        protected void InitializeException(string ErrorDescription, Exception objException) {
            this.ErrorDescription = ErrorDescription;
            this.mInnerException = objException;
        }

        /// <summary>
        /// Initialize Exception for the Error Id 
        /// </summary>
        protected void InitializeException(string strMessageValue) {
            this.ErrorID = string.Empty;
            this.ErrorDescription = GetMessage("WebMethod", strMessageValue.Split(":".ToCharArray())[0]);
            this.mInnerException = null;
        }

        /// <summary>
        ///  Retrieves messages from the Application DataSet
        /// </summary>
        private string GetMessage(string ErrorGroup, string ErrorId) {
            // Form the Generic Error Message That needs to be logged
            string strReturnMessage = "Error occurred in the Application. ";
            DataSet dsErrorList = (DataSet)HttpContext.Current.Application["ErrorList"];

            if (dsErrorList != null && dsErrorList.Tables[0].Rows.Count > 0) {
                DataRow[] drRows = dsErrorList.Tables[0].Select("ERRORGROUP = '" + ErrorGroup + "' and ERRORCODE='" + ErrorId + "'");
                strReturnMessage = (drRows != null && drRows.Length > 0) ? Convert.ToString(drRows[0]["CUSTOMMESSAGE"]) : string.Empty;
            }

            return strReturnMessage;
        }

    }
}
