﻿using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Sony.US.ServicesPLUS.Core;
using Sony.US.siam;
using Sony.US.SIAMUtilities;

namespace ServicesPlusException {
    public class Utilities {
        //public Customer customer;
        static ManualResetEvent Nop = new ManualResetEvent(true);
        static byte[] bytes = Encoding.ASCII.GetBytes("ZeroCool");

        /// <summary>
        /// Wraps the Exception with Oracle Exception
        /// </summary>
        public static Exception WrapException(Exception objException) {
            try {
                string exceptionType = objException.GetType().Name;
                if (exceptionType == "OracleException")
                    return new ServicesPLUSOracleException(objException.Message.Split(':')[0], objException);
                else
                    return objException;
            } catch (Exception ecObj) {
                return ecObj;
            }
        }

        /// <summary>
        ///  Wraps the Exception Happened at UI
        /// </summary>
        public static string WrapExceptionforUI(Exception objException) {
            ServicesPLUSBaseException objBaseException = null;

            try {
                if (objException.GetType().ToString().Equals("System.Threading.ThreadAbortException"))
                    return string.Empty;
                else if (objException.GetType().ToString().Equals("The operation has timed out"))
                    objException = new ServicesPLUSUIException("The operation has timed out. Please try again OR ", objException);

                var exceptionType = objException.GetType().Name;
                objBaseException = (exceptionType.Equals("ServicesPLUSOracleException")
                    || exceptionType.Equals("ServicesPLUSOracleException")
                    || exceptionType.Equals("ServicesPLUSWebMethodException")
                    || exceptionType.Equals("ServicesPlusBusinessException"))
                   ? (ServicesPLUSBaseException)objException
                   : new ServicesPLUSUIException("Unexpected error encountered", objException);

                return LogException(objBaseException, "LogUIException", string.Empty);
            } catch {
                return string.Empty;
            }
        }

        //7382 starts
        public static string WrapExceptionforUINumber(Exception objException, string strUniqueNumber) {
            try {
                if (objException.GetType().ToString().Equals("System.Threading.ThreadAbortException"))
                    return string.Empty;
                else if (objException.GetType().ToString().Equals("The operation has timed out"))
                    objException = new ServicesPLUSUIException("The operation has timed out.Please try again OR", objException);

                ServicesPLUSBaseException objBaseException = null;
                objBaseException = (objException.GetType().Name.Equals("ServicesPLUSOracleException")
                    || objException.GetType().Name.Equals("ServicesPLUSOracleException")
                    || objException.GetType().Name.Equals("ServicesPLUSWebMethodException")
                    || objException.GetType().Name.Equals("ServicesPlusBusinessException"))
                   ? (ServicesPLUSBaseException)objException
                   : new ServicesPLUSUIException("Unexpected error encountered", objException);
                return LogException(objBaseException, "LogUIException", strUniqueNumber);
            } catch {
                return string.Empty;
            }
        }
        //7382 ends


        /// <summary>
        /// Log the exception and returns a generic message to the UI
        /// </summary>
        public static string LogException(ServicesPLUSBaseException ec, string ExceptionLogHeader, string errorNumber) {
            var BR = Environment.NewLine;
            var debugString = $"Utilities.LogException - START at {DateTime.Now.ToLongTimeString()}{BR}";

            try {
                var baseException = (ServicesPLUSBaseException)ec;

                if (string.IsNullOrEmpty(errorNumber))
                    errorNumber = GetUniqueKey();
                //Convert.ToString(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);

                var strBuilder = new StringBuilder();
                strBuilder.Append($"<ServicesPLUSErrorMessage>{BR}");
                strBuilder.Append($"  <{ExceptionLogHeader}>{BR}");
                strBuilder.Append($"  <UserIdentification>{BR}");
                if (HttpContext.Current?.Session?["customer"] != null) {
                    var customer = (Customer)HttpContext.Current.Session["customer"];
                    debugString += $"- Customer found. LDAP ID: {customer.LdapID}.{BR}";
                    strBuilder.Append($"    <LDAP_ID>{customer.LdapID}</LDAP_ID>{BR}");
                    strBuilder.Append($"    <UserName>{customer.UserName}</UserName>{BR}");
                    strBuilder.Append($"    <Company>{customer.CompanyName}</Company>{BR}");
                    strBuilder.Append($"    <CustomerID>{customer.CustomerID}</CustomerID>{BR}");
                    strBuilder.Append($"    <CustomerID_SequenceNumber>{customer.SequenceNumber}</CustomerID_SequenceNumber>{BR}");
                    strBuilder.Append($"    <SIAM_ID>{customer.SIAMIdentity}</SIAM_ID>{BR}");
                } else if (HttpContext.Current?.Session?["siamuser"] != null) {
                    var user = (User)HttpContext.Current.Session["siamuser"];
                    debugString += $"- SIAM User found. LDAP ID: {user.UserId}.{BR}";
                    strBuilder.Append($"    <LDAP_ID>{user.UserId}</LDAP_ID>{BR}");
                    strBuilder.Append($"    <UserName>{user.FirstName} {user.LastName}</UserName>{BR}");
                    strBuilder.Append($"    <Company>{user.CompanyName}</Company>{BR}");
                    strBuilder.Append($"    <CustomerID>{user.CustomerID}</CustomerID>{BR}");
                    strBuilder.Append($"    <CustomerID_SequenceNumber>{user.SequenceNumber}</CustomerID_SequenceNumber>{BR}");
                    strBuilder.Append($"    <SIAM_ID>{user.Identity}</SIAM_ID>{BR}");
                } else {
                    debugString += $"- User not logged in.{BR}";
                }
                strBuilder.Append($"    <UserHostAddress>{HttpContext.Current?.Request?.UserHostAddress}</UserHostAddress>{BR}");
                strBuilder.Append($"    <HTTP_USER_AGENT>{HttpContext.Current?.Request?.ServerVariables["HTTP_USER_AGENT"]}</HTTP_USER_AGENT>{BR}");
                strBuilder.Append($"  </UserIdentification>{BR}");
                debugString += $"- UserIdentification parsed.{BR}";
                strBuilder.Append($"  <EventType></EventType>{BR}");
                strBuilder.Append($"  <EventDateTime>{DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")} EST</EventDateTime>{BR}");
                strBuilder.Append($"  <IndicationSuccessFailure></IndicationSuccessFailure>{BR}");
                debugString += $"- Starting OriginationOfEvent.{BR}";
                strBuilder.Append($"  <OriginationOfEvent>{BR}");
                strBuilder.Append($"    <EventOriginMachineName>{Environment.MachineName}</EventOriginMachineName>{BR}");
                strBuilder.Append($"    <HTTPReferer>{HttpContext.Current?.Request?.UrlReferrer}</HTTPReferer>{BR}");
                strBuilder.Append($"    <HTTPURL>{HttpContext.Current?.Request?.Url?.AbsoluteUri}</HTTPURL>{BR}");
                strBuilder.Append($"    <EventOriginApplicationName>ServicesPLUS</EventOriginApplicationName>{BR}");
                strBuilder.Append($"  </OriginationOfEvent>{BR}");
                debugString += $"- Starting to parse baseException. Exception type is: {baseException?.GetType()?.ToString() ?? "unknown"}{BR}";
                strBuilder.Append($"  <Message>{baseException?.errorMessage}</Message>{BR}");
                strBuilder.Append($"  <ErrorCode>{errorNumber}</ErrorCode>{BR}");
                strBuilder.Append($"  <ErrorID>{baseException?.ErrorID}</ErrorID>{BR}");
                strBuilder.Append($"  <ErrorDescription>{baseException?.ErrorDescription}</ErrorDescription>{BR}");
                strBuilder.Append($"  <Source>{baseException?.MInnerException?.Source ?? ""}</Source>{BR}");
                strBuilder.Append($"  <Message>{baseException?.MInnerException?.Message ?? ""}</Message>{BR}");
                strBuilder.Append($"  <StackTrace>{baseException?.MInnerException?.StackTrace ?? ""}</StackTrace>{BR}");
                strBuilder.Append($"  <InnerException>{baseException?.MInnerException?.InnerException?.ToString() ?? ""}</InnerException>{BR}");
                strBuilder.Append($"  <MInnerException>{baseException?.MInnerException?.ToString() ?? ""}</MInnerException>{BR}");
                strBuilder.Append($"  <Data>{baseException?.MInnerException?.Data?.ToString() ?? ""}</Data>{BR}");
                strBuilder.Append($"  </{ExceptionLogHeader}>{BR}");
                strBuilder.Append($"</ServicesPLUSErrorMessage>{BR}");
                strBuilder.Replace("&", " ");
                debugString += $"- Pushing string to logger.{BR}";
                LogMessages(strBuilder.ToString());

                if (baseException.setcustomMessage) {
                    debugString += $"- Set Custom Message.{BR}";
                    if (baseException?.DisplayIncidentNumber == true)
                        return $"{baseException.ErrorDescription} {errorNumber}."; //show incident number 
                    else
                        return baseException.ErrorDescription + "."; // do not show incident number 
                } else if (baseException.hideContactus) {
                    debugString += $"- Hide Contact Us.{BR}";
                    return baseException.ErrorDescription;
                } else if (HttpContext.Current.Session?["UserStatus"]?.ToString() == "I") {
                    debugString += $"- Internal user.{BR}";
                    return $"<b>{baseException.ErrorDescription}. For assistance, please <a style=\"color:Blue\" href=\"Mailto:ApplicationSupportCenter@am.sony.com?cc=ServicesPLUS@am.sony.com&subject=ServicesPLUSIncident:%20{errorNumber}\">contact us</a> and mention incident number {errorNumber}</b>";
                } else {
                    debugString += $"- Final Else statement hit.{BR}";
                    string sFeature = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 ";
                    return $"<b>{baseException.ErrorDescription}. For assistance, please <a style=\"color:Blue\" href=\"#\" onclick=\"javascript:window.open('{ConfigurationData.GeneralSettings.URL.contact_us_popup_link}',null,'{sFeature}');\">contact us</a> and mention incident number {errorNumber}</b>";
                }
            } catch (Exception ex) {
                LogMessages($"Exception during Utilities.LogException. {ex.Message}{Environment.NewLine}" +
                            $" Debug String for above exception: {debugString}");
                throw; // ex;
            }
        }

        /// <summary>
        /// Logs the messages to the file.
        /// </summary>
        /// <param name="obj"></param>
        public void fnLogException(Exception ex) {
            try {
                //              System.Text.StringBuilder strBuilder = new StringBuilder();
                //strBuilder.Append(@"<ServicesPLUSErrorMessage>" + Environment.NewLine);

                //strBuilder.Append(@"<Message>" + ex.Message + @"</Message>" + Environment.NewLine);
                //strBuilder.Append(@"<StackTrace>" + ex.StackTrace + @"</StackTrace>" + Environment.NewLine);
                //strBuilder.Append(@"</ServicesPLUSErrorMessage>" + Environment.NewLine);
                //strBuilder.Replace("&", " ");
                //LogMessages(strBuilder.ToString());   
                if (ConfigurationData.GeneralSettings.Environment.ToString() == "QA") {
                    string file = ConfigurationData.Environment.SoftwarePlusConfiguration.ErrorLogFile + @"/Error.txt";

                    if (!File.Exists(file)) {
                        using (StreamWriter sw = File.CreateText(file)) {
                            sw.WriteLine(ex.Message);
                            sw.WriteLine(Environment.NewLine);
                            sw.WriteLine(ex.StackTrace);
                            sw.WriteLine(Environment.NewLine);
                        }
                    } else {
                        using (StreamWriter sw = File.AppendText(file)) {
                            sw.WriteLine(ex.Message);
                            sw.WriteLine(Environment.NewLine);
                            sw.WriteLine(ex.StackTrace);
                            sw.WriteLine(Environment.NewLine);
                        }
                    }
                }
            } catch (Exception) {
                //   LogMessages(e.Message);
                //throw ex;
            }
            //return "Error occured in the application . Please contact support with the following Id : " + "<a href='" + ConfigurationData.GeneralSettings.URL.Contact_Us_Link  + "' target='_blank'>" + strUniqueCode + "</a>";
        }

        /// <summary>
        /// Outputs the provided message to the Error Log only when the code is not running in Production
        /// </summary>
        /// <param name="message">The exact message to display in the log.</param>
        public static void LogDebug(string message) {
            if (ConfigurationData.GeneralSettings.Environment.ToLower() != "production")                
                LogMessages(message);
        }

        public static void LogMessages(string strMessage, string category = "General") {
            // Obsolete code below, for reference.
            //LogEntry objEntry = new LogEntry();
            //string[] category = { "General" };
            //objEntry.Categories = category;
            //objEntry.Message = strMessage; // pass the xml to the log entry object
            //Logger.Write(objEntry);

            var loggerBlock = new LoggerBlock();
            var objEntry = new LogEntry {
                Categories = { category },
                Priority = 2,
                Message = strMessage
            };
            loggerBlock.LogWriter.Write(objEntry);
        }

        /// <summary>
        /// GetUniqueKey function returns the unique value that needs to be displyed to the user. 
        /// </summary>
        /// <returns></returns>

        public static string GetUniqueKey() {
            int maxSize = 8;

            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data) { result.Append(chars[b % (chars.Length - 1)]); }
            return result.ToString();
        }

        public static void fnActionLog(string strMsg) {
            try {
                string strIpAddress = "";
                strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (strIpAddress == null) {
                    strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                Nop.WaitOne();
                Nop.Reset();
                System.Data.DataTable Dt = new DataTable("ActionLogger");
                string strPath = @"C:\ActionLog\";
                if (!(Directory.Exists(strPath)))
                    Directory.CreateDirectory(strPath);
                if (System.IO.File.Exists(strPath + "Log_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".xml")) {
                    DataSet ds = new DataSet();
                    ds.ReadXml(strPath + "Log_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".xml");
                    Dt = ds.Tables[0];
                    Dt.Rows.Add(new object[] { DateTime.Now.ToShortDateString() + " " + DateTime.Now.TimeOfDay.ToString(), strIpAddress, strMsg });
                } else {
                    Dt.Columns.Add("DateTime");
                    Dt.Columns.Add("ClientIP");
                    Dt.Columns.Add("ActionMessage");
                    Dt.Rows.Add(new object[] { DateTime.Now.ToShortDateString() + " " + DateTime.Now.TimeOfDay.ToString(), strIpAddress, strMsg });
                }
                Dt.WriteXml(strPath + "Log_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".xml");

            } catch {
            } finally {
                Nop.Set();
            }
        }


    }
}
