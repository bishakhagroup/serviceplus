﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServicesPlusException
{
   
    public class ServicesPLUSWebMethodException :  ServicesPLUSBaseException 
    {

        public ServicesPLUSWebMethodException(string ErrorCode  )
        {
            if (!String.IsNullOrEmpty(ErrorCode))
            {
                this.InitializeException("wMException", ErrorCode, new Exception());
            }
            
            else
            {
                throw new ArgumentNullException() ;
            }
        }
        public ServicesPLUSWebMethodException(string ErrorCode, Exception objException)
        {
            if (!String.IsNullOrEmpty(ErrorCode))
            {
                this.InitializeException("wMException", ErrorCode, objException);
            }
            else
            {
                throw new ArgumentNullException();
            }
        }
    }
}
