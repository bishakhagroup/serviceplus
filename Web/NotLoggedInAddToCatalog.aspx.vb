Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports ServicesPlusException


Namespace ServicePLUSWebApp



    Partial Class NotLoggedInAddToCatalog
        Inherits SSL

        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Dim responseurl As String = "SignIn.aspx"
            Dim responseurl As String = "SignIn-Register.aspx"
            If Not Request.QueryString("PartLineNo") Is Nothing Then
                If Not Session.Item("addCatPvalue") Is Nothing Then
                    Session.Remove("addCatPvalue")
                End If
                Session.Add("addCatPvalue", Request.QueryString("PartLineNo"))
            End If

            ImageButton1.Visible = False
            Try
                Dim strParamaters As String = String.Empty
                strParamaters = ResolveURLParameter()
                responseurl = responseurl + IIf(strParamaters = String.Empty, String.Empty, "?" + strParamaters)
                Response.Redirect(responseurl)
            Catch ex As Threading.ThreadAbortException
                'do nothing
            Catch ex As Exception
                MessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
        '        If Not Session.Item("addCatPvalue") Is Nothing Then
        '            Session.Remove("addCatPvalue")
        '        End If
        '        Response.Write("<script language='javascript'> { window.close() }</script>")
        'End Sub
    End Class

End Namespace
