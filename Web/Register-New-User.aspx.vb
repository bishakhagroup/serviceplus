Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp

    Partial Class Register_New_User
        Inherits SSL

        Public focusFormField As String
        Dim isVerified As Boolean
        Dim customer As New Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Dim myLdap As String = String.Empty

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                If Not Page.IsPostBack Then
                    'controlMethod(sender, e)
                    BuildStateDropDown()
                    fnZipCodeConfig()
                    If Session("Language_Changed") IsNot Nothing AndAlso Session("Language_Changed") = "1" Then
                        fnSetSession()
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnNext.Click
            Try
                'controlMethod(sender, e)
                If IsDataValid() Then
                    ProcessRegistrationForm()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        Private Sub ProcessRegistrationForm()
            Dim data As String
            Dim newUserData As String()
            Dim strCountryName As String
            Dim strCountryCode As String = String.Empty
            Dim cm As New CatalogManager

            If ((Session("NewUserData") Is Nothing) And (Session("newCustomer") Is Nothing)) Then
                ErrorLabel.Text = "An error occurred while trying to read session variables. Please retry your registration from the <a href='default.aspx'>home page</a>.<br/>"
                ErrorLabel.Text &= "If you continue to experience errors, try to close and re-open your browser to ensure a fresh session."
                ErrorLabel.Focus()
                Return
            End If

            Try
                If Session("newCustomer") IsNot Nothing Then customer = Session.Item("newCustomer")

                If Session("NewUserData") IsNot Nothing Then
                    data = Session.Item("NewUserData")
                    newUserData = data.Split("*")
                    customer.FirstName = newUserData(0)
                    customer.LastName = newUserData(1)
                    customer.EmailAddress = newUserData(2)
                    customer.UserName = newUserData(2)
                End If

                If Not HttpContextManager.GlobalData.IsCanada And customer.UserType = Nothing Then  ' What if a Canadian hits this logic?
                    customer.UserType = "C"
                End If

                customer.CompanyName = txtCompanyName.Text.Trim()
                customer.Address.Line1 = txtAddress1.Text.Trim()
                customer.Address.Line2 = txtAddress2.Text.Trim()
                customer.Address.City = txtCity.Text.Trim()
                customer.Address.State = ddlState.SelectedValue
                customer.Address.PostalCode = txtZipCode.Text.Trim()
                customer.PhoneNumber = txtPhone.Text.Trim()
                customer.PhoneExtension = txtExtension.Text.Trim()
                customer.FaxNumber = txtFax.Text.Trim()
                customer.UserLocation = 1
                customer.StatusCode = 1

                'Add Country to Customer 
                strCountryName = cm.CountryBySalesOrg(HttpContextManager.GlobalData, strCountryCode)
                customer.CountryName = strCountryName
                customer.CountryCode = strCountryCode
                customer.Address.Country = strCountryCode

                Session("siamID") = customer.SIAMIdentity
                Session("newCustomer") = customer
                If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                    Response.Redirect("Register-Password.aspx?SessionID=" & Request.QueryString("SessionID"), False)
                Else
                    Response.Redirect("Register-Password.aspx", False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Focus()
            End Try
        End Sub

        Private Function IsDataValid() As Boolean
            Dim rgxPhoneNumber As New Regex("(^\d{3}-\d{3}-\d{4}$)")
            Dim rgxExtension As New Regex("^[0-9]*$")

            ' Reset all labels and validations
            ErrorValidation.Visible = False
            lblCompany.Attributes("class") = ""
            lblAddress1.Attributes("class") = ""
            lblAddress1Error.Text = "*"
            lblAddress2.Attributes("class") = ""
            lblAddress2Error.Text = "*"
            lblCity.Attributes("class") = ""
            lblCityError.Text = "*"
            lblState.Attributes("class") = ""
            lblZipCode.Attributes("class") = ""
            lblZipError.Text = "*"
            lblPhone.Attributes("class") = ""
            lblPhoneError.Text = ""
            lblExtension.Attributes("class") = ""
            lblEtxErr.Text = ""
            lblFax.Attributes("class") = ""
            lblFaxError.Text = ""

            If String.IsNullOrWhiteSpace(txtCompanyName.Text) Then
                ErrorValidation.Visible = True
                lblCompany.Attributes("class") = "redAsterick"
            End If

            If String.IsNullOrWhiteSpace(txtAddress1.Text) Then
                ErrorValidation.Visible = True
                lblAddress1.Attributes("class") = "redAsterick"
            ElseIf txtAddress1.Text.Length > 35 Then
                ErrorValidation.Visible = True
                lblAddress1.Attributes("class") = "redAsterick"
                lblAddress1Error.Text = Resources.Resource.el_ValidLength_StreetAddress ' Street Address must be 35 characters or less in length.
            End If

            If txtAddress2.Text.Length > 35 Then
                ErrorValidation.Visible = True
                lblAddress2.Attributes("class") = "redAsterick"
                lblAddress2Error.Text = Resources.Resource.el_ValidLength_Address2      ' Address 2nd Line must be 35 characters or less in length.
            End If

            If String.IsNullOrWhiteSpace(txtCity.Text) Then
                ErrorValidation.Visible = True
                lblCity.Attributes("class") = "redAsterick"
            ElseIf txtCity.Text.Length > 35 Then
                ErrorValidation.Visible = True
                lblCity.Attributes("class") = "redAsterick"
                lblCityError.Text = Resources.Resource.el_ValidLength_City              ' City must be 35 characters or less in length.
            End If

            If ddlState.SelectedIndex = 0 Then
                ErrorValidation.Visible = True
                lblState.Attributes("class") = "redAsterick"
            End If

            rgxZipCode.Validate()
            If String.IsNullOrWhiteSpace(txtZipCode.Text) Or Not rgxZipCode.IsValid Then
                ErrorValidation.Visible = True
                lblZipCode.Attributes("class") = "redAsterick"
                lblZipError.Text = $"Please enter {lblZipCode.InnerText} in valid format."
            End If

            If Not rgxPhoneNumber.IsMatch(txtPhone.Text.Trim()) Then
                ErrorValidation.Visible = True
                lblPhone.Attributes("class") = "redAsterick"
                If Not String.IsNullOrWhiteSpace(txtPhone.Text) Then
                    lblPhoneError.Text = "</br>" + Resources.Resource.el_validate_Phone ' Please enter phone number in format xxx-xxx-xxxx.
                End If
            End If

            Dim sExtension As String = txtExtension.Text.Trim()
            If Not String.IsNullOrWhiteSpace(sExtension) Then
                If Not rgxExtension.IsMatch(sExtension) Or sExtension.Length > 6 Then
                    ErrorValidation.Visible = True
                    lblExtension.Attributes("class") = "redAsterick"
                    lblEtxErr.Text = "</br>" + Resources.Resource.el_validate_Extn      ' Extension must be numeric and 6 or fewer digits.
                End If
            End If

            Dim sFaxNumber As String = txtFax.Text.Trim()
            If Not String.IsNullOrWhiteSpace(sFaxNumber) Then
                If Not rgxPhoneNumber.IsMatch(sFaxNumber) Then
                    ErrorValidation.Visible = True
                    lblFax.Attributes("class") = "redAsterick"
                    lblFaxError.Text = "</br>" + Resources.Resource.el_validate_Fax     ' Please enter fax number in format xxx-xxx-xxxx.
                End If
            End If

            If Not Page.IsValid Then ErrorValidation.Visible = True

            If ErrorValidation.Visible Then
                ErrorValidation.Text = Resources.Resource.el_validate_RequireWarning    ' Please enter required field.
                ErrorValidation.Focus()
                Return False
            Else
                Return True
            End If
        End Function

        Private Sub BuildStateDropDown()
            Dim cm As New CatalogManager
            Dim statedetails() As StatesDetail
            Dim strMsg As String = Resources.Resource.el_rgn_Select
            Dim defaultItem As New ListItem

            statedetails = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
            'bind card type array to dropdown list control   

            defaultItem.Text = strMsg
            defaultItem.Value = ""
            defaultItem.Selected = True

            ddlState.DataSource = statedetails
            ddlState.DataTextField = "StateAbbr"
            ddlState.DataValueField = "StateAbbr"
            ddlState.DataBind()
            ddlState.Items.Insert(0, defaultItem)
        End Sub

        Private Sub fnZipCodeConfig()
            Dim intLength As Integer
            Dim strValidate As String = String.Empty

            If HttpContextManager.GlobalData.IsAmerica Then
                lblZipCode.InnerText = "Zip Code"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                strValidate = "\d{" + intLength.ToString() + "}(-\d{4})?$"
            Else
                lblZipCode.InnerText = "Postal Code"
                intLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                strValidate = "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"
            End If
            txtZipCode.Text = ""
            txtZipCode.MaxLength = intLength
            rgxZipCode.ValidationExpression = strValidate
            ' zipRegularExpressionValidator.ErrorMessage = "</br>Please enter " + intLength.ToString() + " digit zip code."
            rgxZipCode.ErrorMessage = "</br>Invalid " + lblZipCode.InnerText + "."

            'Upper case - Zip code 
            txtZipCode.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
        End Sub

        Public Sub fnGetSession()
            Dim ObjCst As New Customer
            ObjCst.CompanyName = txtCompanyName.Text.Trim()
            ObjCst.Address.Line1 = txtAddress1.Text.Trim()
            ObjCst.Address.Line2 = txtAddress2.Text.Trim()
            ObjCst.Address.City = txtCity.Text.Trim()
            ObjCst.Address.State = ddlState.SelectedValue
            ObjCst.Address.PostalCode = txtZipCode.Text.Trim()
            ObjCst.PhoneNumber = txtPhone.Text.Trim()
            ObjCst.PhoneExtension = txtExtension.Text.Trim()
            ObjCst.FaxNumber = txtFax.Text.Trim()
            Session.Add("CultureData", ObjCst)  ' ...huh?
        End Sub

        Public Sub fnSetSession()
            If Session("CultureData") IsNot Nothing Then
                Dim ObjCst As New Customer
                ObjCst = Session.Item("CultureData")
                txtCompanyName.Text = ObjCst.CompanyName
                txtAddress1.Text = ObjCst.Address.Line1
                txtAddress2.Text = ObjCst.Address.Line2
                txtCity.Text = ObjCst.Address.City
                ' ObjCst.Address.State = DropDownList1.SelectedValue
                txtZipCode.Text = ObjCst.Address.PostalCode
                txtPhone.Text = ObjCst.PhoneNumber
                txtExtension.Text = ObjCst.PhoneExtension
                txtFax.Text = ObjCst.FaxNumber
                Session.Remove("CultureData")
                Session("Language_Changed") = "0"
            End If
        End Sub

    End Class

End Namespace
