<%@ Reference Control="~/ShipMethod.ascx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ShipMethod" Src="ShipMethod.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.AccntHolder"
    EnableViewStateMac="True" CodeFile="AccntHolder.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_AccountHolder%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="Pragma" content="no-cache" />
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet" />

    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function checkSession() {
            if (document.getElementById("txtHidden").value === "load") {
                document.getElementById("txtHidden").value = "done";
            } else {
                location.href = "SessionExpired.aspx";
            }
        }

        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("txtPONumber").value = "NO-PO-REQ";
            } else {
                document.getElementById("txtPONumber").value = "";
            }
        }
    </script>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;" onload="javascript:checkSession();">
    <form id="frm4" method="post" runat="server">
        <input id="txtHidden" type="hidden" value="load">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <center>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table width="728" border="0" role="presentation">
                        <tr>
                            <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                                <img height="25" src="images/spacer.gif" width="25" alt="">
                            </td>
                            <td width="710" bgcolor="#ffffff">
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td>
                                            <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td width="688">
                                                        <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="688">
                                                        <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table border="0" width="710" role="presentation">
                                                            <tr>
                                                                <td style="width: 464px; height: 57px; background: url(images/sp_int_header_top_ServicesPLUS_onepix.gif); text-align: right; vertical-align: middle;">
                                                                    <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                                                </td>
                                                                <td style="background: #363d45; padding: 5px; vertical-align: middle; text-align: center;">
                                                                    <span class="memberName">
                                                                        <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="background: #f2f5f8; vertical-align: middle;">
                                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_Checkout%></h2>
                                                                </td>
                                                                <td bgcolor="#99a8b5">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 9px;">
                                                                <td style="width: 464px; background: #f2f5f8 url(images/sp_int_header_btm_left_onepix.gif);">
                                                                    <img height="9" src="images/spacer.gif" width="1" alt="">
                                                                </td>
                                                                <td style="background: #99a8b5 url(images/sp_int_header_btm_right.gif);">
                                                                    <img height="9" src="images/spacer.gif" width="1" alt="">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="688">
                                            <main>
                                                <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td width="20" height="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                    <td width="670" height="20">
                                                        <img height="20" src="images/spacer.gif" width="670" alt="">
                                                    </td>
                                                    <td width="20" height="20">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                    <td valign="top" width="670">
                                                        <table cellpadding="3" width="670" border="0" runat="server" id="tbHeader" role="presentation">
                                                            <tr>
                                                                <td width="500" colspan="3">
                                                                    <span class="tableData"><strong><%=Resources.Resource.el_Requirevalidate%></strong></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tableHeader" align="right" width="670" colspan="3">
                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                            <table cellpadding="3" width="100%" border="0" role="presentation">
                                                                                <tr>
                                                                                    <td align="right" width="55">
                                                                                        <label for="ddlBillTo" class="tableHeader"><%=Resources.Resource.el_Billto%></label>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:DropDownList ID="ddlBillTo" runat="server" AutoPostBack="True" CssClass="tableData" />
                                                                                        <asp:Label ID="LabelBillToStar" runat="server" CssClass="redAsterick">
																				            <span class="redAsterick">*</span>
                                                                                        </asp:Label>
                                                                                        <asp:UpdateProgress runat="server" ID="UpdateProgress1" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel2">
                                                                                            <ProgressTemplate>
                                                                                                <img id="Img1" runat="Server" src="~/images/progbar.gif" height="12" alt="" />
                                                                                            </ProgressTemplate>
                                                                                        </asp:UpdateProgress>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlBillTo" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tableHeader" align="right" width="670" colspan="3">
                                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                                        <ContentTemplate>
                                                                            <table cellpadding="3" width="100%" border="0" role="presentation">
                                                                                <tr id="ShipTo" runat="server" visible="false">
                                                                                    <td class="tableHeader" width="55" align="right">
                                                                                        <label id="lblShipTo" runat="server" for="ddlShipTo"><%=Resources.Resource.el_Shipto%></label>
                                                                                    </td>
                                                                                    <td width="562" colspan="2">
                                                                                        <asp:DropDownList ID="ddlShipTo" runat="server" CssClass="tableData"
                                                                                            AutoPostBack="True" />
                                                                                        <span class="redAsterick"></span>
                                                                                        <asp:Label ID="LabelShipToStar" runat="server">
																				            <span class="redAsterick">*</span>
                                                                                        </asp:Label>
                                                                                        <asp:UpdateProgress runat="server" ID="UpdateProgress2" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel3">
                                                                                            <ProgressTemplate>
                                                                                                <img id="Img2" runat="Server" src="~/images/progbar.gif" height="12" alt="" />
                                                                                            </ProgressTemplate>
                                                                                        </asp:UpdateProgress>
                                                                                    </td>
                                                                                </tr>
                                                                                <div id="ShipToDetails" runat="server" visible="false">
                                                                                    <tr>
                                                                                        <td class="tableHeader" align="right" width="92">
                                                                                        </td>
                                                                                        <td width="562" colspan="2">
                                                                                            <asp:CheckBox runat="server" Text="<%$ Resources:Resource, el_AnotheShippingAddress%>"
                                                                                                class="tableData" ID="chkDropShip" Visible="false" AutoPostBack="True" CausesValidation="True" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92">
                                                                                            <label id="lblCompany" runat="server" for="txtCompanyName" class="tableData"><%=Resources.Resource.el_Company%></label>
                                                                                        </td>
                                                                                        <td width="562">
                                                                                            <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="tableData"
                                                                                                MaxLength="100" Width="184px" /><asp:Label ID="LabelCompanyStar" runat="server">
																				                    <span class="redAsterick">*</span>
                                                                                                </asp:Label>
                                                                                        </td>
                                                                                        <td valign="top" height="231" rowspan="8">
                                                                                            <uc1:ShipMethod ID="ShipMethod" runat="server" />
                                                                                            <asp:Label ID="LabelCalifornia" runat="server" CssClass="redAsterick"></asp:Label>
                                                                                            <br />
                                                                                            <%--   <table style="height: 37px; width: 100%; text-align: left;
                                                                                        border-color: Black" runat="server" id="tbCalifornia" visible="false" border="1">
                                                                                        <tr align="left">
                                                                                            <td class="tabledata">
                                                                                                Is your order eligible for the California section 6378 partial
                                                                                                <br />
                                                                                                tax exemption?
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr align="center">
                                                                                            <td class="style3">
                                                                                                <asp:RadioButtonList ID="rbCalifornia" RepeatDirection="Horizontal" runat="server"
                                                                                                    CssClass="tableData" Width="150px">
                                                                                                    <asp:ListItem Value="0" Selected="False" Text="Yes">Yes</asp:ListItem>
                                                                                                    <asp:ListItem Value="1" Selected="False" Text="No">No</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>--%>
                                                                                            <table id="tbCalifornia" runat="server" width="350"
                                                                                                border="0" role="presentation">
                                                                                                <tr bgcolor="#b7b7b7">
                                                                                                    <td colspan="6">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr align="left">
                                                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                    <td colspan="2" width="10">
                                                                                                    </td>
                                                                                                    <td colspan="2">
                                                                                                        <span class="tableHeader">
                                                                                                            <SPS:SPSLabel ID="spCali" runat="server" Text="<%$ Resources:Resource, el_Exemptioncertificate%>"></SPS:SPSLabel><br />
                                                                                                            <SPS:SPSLabel ID="spCali1" runat="server" Text="<%$ Resources:Resource, el_Exemptioncertificate1%>"></SPS:SPSLabel>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr bgcolor="#b7b7b7">
                                                                                                    <td colspan="6">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr align="center">
                                                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                    <td colspan="4" class="tabledata">
                                                                                                        <asp:RadioButtonList ID="rbCalifornia" RepeatDirection="Horizontal" runat="server"
                                                                                                            CssClass="tableData" Width="150px">
                                                                                                            <asp:ListItem Value="0" Selected="False" Text="<%$ Resources:Resource, el_yes%>">Yes</asp:ListItem>
                                                                                                            <asp:ListItem Value="1" Selected="False" Text="<%$ Resources:Resource, el_no%>">No</asp:ListItem>
                                                                                                        </asp:RadioButtonList>
                                                                                                    </td>
                                                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr bgcolor="#b7b7b7">
                                                                                                    <td colspan="6">
                                                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" height="32">
                                                                                            <label id="lblAttnName" runat="server" for="txtAttnName"><%=Resources.Resource.el_AttnName%></label>
                                                                                        </td>
                                                                                        <td width="562" height="32">
                                                                                            <SPS:SPSTextBox ID="txtAttnName" runat="server" CssClass="tableData"
                                                                                                MaxLength="30" Width="184px" /><br />
                                                                                            <asp:Label ID="LabelLineAttention" runat="server" CssClass="redAsterick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" valign="top">
                                                                                            <label id="lblAddress1" runat="server" for="txtAddress1"><%=Resources.Resource.el_rgn_StreetAddress%></label>
                                                                                        </td>
                                                                                        <td width="562">
                                                                                            <SPS:SPSTextBox ID="txtAddress1" runat="server" CssClass="tableData"
                                                                                                MaxLength="50" Width="184px" /><asp:Label ID="LabelLine" runat="server">
																				                    <span class="redAsterick">*</span>
                                                                                                </asp:Label><br />
                                                                                            <asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" height="31" valign="top">
                                                                                            <label id="lblAddress2" runat="server" for="txtAddress2"><%=Resources.Resource.el_rgn_2ndAddress%></label>
                                                                                        </td>
                                                                                        <td width="562" height="31">
                                                                                            <SPS:SPSTextBox ID="txtAddress2" runat="server" CssClass="tableData"
                                                                                                Width="184px" MaxLength="50" /><br />
                                                                                            <asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" valign="top">
                                                                                            <label id="lblCity" runat="server" for="txtCity"><%=Resources.Resource.el_rgn_City%></label>
                                                                                        </td>
                                                                                        <td width="562">
                                                                                            <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="tableData" Width="184px"
                                                                                                MaxLength="50" /><br />
                                                                                            <asp:Label ID="LabelLine4Star" runat="server" CssClass="redAsterick" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" height="29">
                                                                                            <label id="lblState" runat="server" for="ddlState" />
                                                                                        </td>
                                                                                        <td width="562" height="29">
                                                                                            <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="tableData" />
                                                                                            <asp:Label ID="LabelStateStar" runat="server">
																				                <span class="redAsterick">*</span>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="92" height="30">
                                                                                            <label id="lblZip" runat="server" for="txtZip" />
                                                                                        </td>
                                                                                        <td width="562" height="30">
                                                                                            <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="tableData" MaxLength="10" /><asp:Label
                                                                                                ID="LabelZipStar" runat="server">
                                                                                                <span class="redAsterick">*</span>
                                                                                            </asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="84">
                                                                                            <label id="lblPhoneShip" runat="server" for="txtPhoneShip"><%=Resources.Resource.el_PhoneNumber%></label>
                                                                                        </td>
                                                                                        <td width="266">
                                                                                            <SPS:SPSTextBox ID="txtPhoneShip" runat="server" MaxLength="12" CssClass="tableData"
                                                                                                Width="100px" /><asp:Label ID="LabelShipPhoneStar" runat="server">
																				                <span class="redAsterick">*</span>
                                                                                            </asp:Label>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" align="right" width="84">
                                                                                            <label id="lblShipExtension" runat="server" for="txtShipExtension"><%=Resources.Resource.el_rgn_Extension%></label>
                                                                                        </td>
                                                                                        <td width="266">
                                                                                            <SPS:SPSTextBox ID="txtShipExtension" runat="server" CssClass="tableData"
                                                                                                MaxLength="6" Width="64px"></SPS:SPSTextBox>&nbsp;&nbsp;<span class="bodyCopy">xxxxxx</span>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="tableData" valign="top" align="right" width="92" height="50">
                                                                                            <label id="lblPONumber" runat="server" for="txtPONumber"><%=Resources.Resource.el_PONumber%></label>
                                                                                        </td>
                                                                                        <td width="572" height="50" class="tableData" valign="top">
                                                                                            <SPS:SPSTextBox ID="txtPONumber" runat="server" CssClass="tableData"
                                                                                                MaxLength="20" /><asp:Label ID="LabelPoNumberStar" runat="server">
																				                <span class="redAsterick">*</span>
                                                                                            </asp:Label><br />
                                                                                            <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData"
                                                                                                onclick="javascript: poNotRequired();" />
                                                                                            <label for="noPoRequiredCheckBox"><%=Resources.Resource.el_NoPurchaseOrder%></label>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="right" width="84">
                                                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                                        </td>
                                                                                        <td colspan="2">
                                                                                            <asp:RadioButtonList ID="rbBillTo" runat="server" CssClass="tableData" AutoPostBack="True">
                                                                                                <asp:ListItem Value="1" Selected="True" Text="<%$ Resources:Resource, el_BilltoAcct%>" />
                                                                                                <asp:ListItem Value="2" Text="<%$ Resources:Resource, el_BilToCreditCard%>" />
                                                                                            </asp:RadioButtonList>
                                                                                        </td>
                                                                                    </tr>
                                                                                </div>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlShipTo" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <div id="divCCDetails" runat="server">
                                                            <table cellpadding="3" width="664" border="0" role="presentation">
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="1" src="images/spacer.gif" width="84" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr id="TokenButton" runat="server">
                                                                    <td align="right" class="tableData" width="92">
                                                                    </td>
                                                                    <td class="tableData" colspan="2">
                                                                        <asp:Button ID="btnNewCC" Text="<%$ Resources:Resource, el_EnterNewCreditcard%>"
                                                                            runat="server" OnClientClick="return popup();" />
                                                                        &nbsp;&nbsp;&nbsp;
                                                                    <asp:Button ID="btnCCSaved" Text="<%$ Resources:Resource, el_SelectSavedCredit%>"
                                                                        runat="server" />
                                                                        <asp:Button ID="btnHidden" runat="Server" Style="display: none" />
                                                                    </td>
                                                                </tr>
                                                                <asp:Panel ID="pnlSavedCard" runat="server">
                                                                    <tr>
                                                                        <td class="tableData" align="right" width="92" height="25">
                                                                            <asp:Label ID="LabelOneCard" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_OneOfMycards%>"> </asp:Label>
                                                                        </td>
                                                                        <td width="572" height="25">
                                                                            <asp:DropDownList ID="ddlMyCard" runat="server" CssClass="tableData" AutoPostBack="True">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td class="tableData" align="right" width="92" height="25">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" width="210" height="25">
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlCCInfo" runat="server">
                                                                    <tr>
                                                                        <td class="tableData" align="right" width="92">
                                                                            &nbsp;
                                                                        <asp:Label ID="LabelCC" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_CardType%>"></asp:Label>
                                                                        </td>
                                                                        <td width="572">
                                                                            <asp:TextBox ID="txtCCType" runat="server" Width="100" CssClass="bodyCopy" Enabled="false"></asp:TextBox>
                                                                            <asp:DropDownList ID="ddlType" runat="server" CssClass="tableData" Enabled="false" Visible="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td class="tableData" align="right" width="92">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" width="210">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tableData" align="right" width="92">
                                                                            <asp:Label ID="LabelCCNumber" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_CardNumber%>"></asp:Label>
                                                                        </td>
                                                                        <td width="572" valign="middle">
                                                                            <SPS:SPSTextBox ID="CCNumber" runat="server" CssClass="tableData" MaxLength="19"
                                                                                Enabled="false"></SPS:SPSTextBox>
                                                                        </td>
                                                                        <td class="tableData" align="left" width="100%" colspan="2" valign="bottom">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" width="84" height="57">
                                                                            &nbsp;&nbsp;
                                                                        <asp:Label ID="LabelExpire" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_ExpirationDate%>"> </asp:Label>
                                                                        </td>
                                                                        <td width="572" height="57">
                                                                            <table width="200" border="0" role="presentation">
                                                                                <tr>
                                                                                    <td class="tableData" colspan="2">
                                                                                        <asp:TextBox ID="txtMonth" runat="server" Width="70" CssClass="tableData" Enabled="false"></asp:TextBox>
                                                                                        <asp:TextBox ID="txtYear" runat="server" Width="30" CssClass="tableData" Enabled="false"></asp:TextBox>
                                                                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="tableData" Enabled="false" Visible="false">
                                                                                        </asp:DropDownList>
                                                                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="tableData" Enabled="false" Visible="false">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td class="tableData" align="right" width="92" height="57">
                                                                        </td>
                                                                        <td valign="top" width="210" height="57">
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlNickName" runat="server">
                                                                    <tr>
                                                                        <td class="tableData" align="right" width="92">
                                                                            <asp:Label ID="LabelNName" runat="server" Text="<%$ Resources:Resource, el_NickName%>"></asp:Label>
                                                                        </td>
                                                                        <td width="572" colspan="3">
                                                                            <SPS:SPSTextBox ID="TextBox6" runat="server" CssClass="tableData" MaxLength="17"></SPS:SPSTextBox><br />
                                                                        </td>
                                                                        <td class="tableData" align="right" width="92">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td valign="top" width="210">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tableData" align="right" width="92">
                                                                            &nbsp;
                                                                        </td>
                                                                        <%--  <td width="572" colspan="3">
                                                                    &nbsp;
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" CssClass="tableData" Text="Save this card"
                                                                       ></asp:CheckBox>
                                                                </td>--%>
                                                                    </tr>
                                                                </asp:Panel>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" height="25">
                                                                        <label id="lblCCName" runat="server" for="CCName"><%=Resources.Resource.el_Creditcard_NichName%></label>
                                                                    </td>
                                                                    <td width="572" height="25">
                                                                        <SPS:SPSTextBox ID="CCName" runat="server" CssClass="tableData" MaxLength="50" />
                                                                        <asp:Label ID="LabelLine5" runat="server"><span class="redAsterick"></span></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92">
                                                                        <label id="lblCCAttn" runat="server" for="TextboxAttn"><%=Resources.Resource.el_CreditCard_Name%></label>
                                                                    </td>
                                                                    <td width="580">
                                                                        <SPS:SPSTextBox ID="TextboxAttn" MaxLength="50" runat="server" CssClass="tableData"
                                                                            Width="200px"></SPS:SPSTextBox>
                                                                        <asp:Label ID="lblAttnStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="380" colspan="2">
                                                                        <asp:Label ID="LabelBillingAddress" runat="server" CssClass="tableData" Text="<%$ Resources:Resource, el_Address_match%>"> </asp:Label>
                                                                    </t
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" valign="top">
                                                                        <label id="lblCCAddress1" runat="server" for="CCAddress1"><%=Resources.Resource.el_rgn_StreetAddress%></label>
                                                                    </td>
                                                                    <td width="562">
                                                                        <SPS:SPSTextBox ID="CCAddress1" runat="server" CssClass="tableData" MaxLength="50"
                                                                            Width="184px"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelLine6" runat="server" CssClass="redAsterick">
                                                                    <span class="redAsterick">*</span> </asp:Label>
                                                                        <br />
                                                                        <asp:Label ID="LabelCCAddress1Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" valign="top">
                                                                        <label id="lblCCAddress2" runat="server" for="CCAddress2"><%=Resources.Resource.el_rgn_2ndAddress%></label>
                                                                    </td>
                                                                    <td width="562">
                                                                        <SPS:SPSTextBox ID="CCAddress2" runat="server" CssClass="tableData" MaxLength="50"
                                                                            Width="184px"></SPS:SPSTextBox><br />
                                                                        <asp:Label ID="LabelCCAddress2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" valign="top">
                                                                        <label id="lblCCCity" runat="server" for="CCCity"><%=Resources.Resource.el_rgn_City%></label>
                                                                    </td>
                                                                    <td style="width: 580px">
                                                                        <SPS:SPSTextBox ID="CCCity" runat="server" CssClass="tableData" Width="184px" MaxLength="50"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelLine7" runat="server" CssClass="redAsterick">
                                                                    <span class="redAsterick">*</span> </asp:Label>
                                                                        <br />
                                                                        <asp:Label ID="LabelCCCityStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" height="29">
                                                                        <label id="lblCCState" runat="server" for="ddlCCState" />
                                                                    </td>
                                                                    <td width="562" height="29">
                                                                        <asp:DropDownList ID="ddlCCState" runat="server" CssClass="tableData">
                                                                        </asp:DropDownList>
                                                                        <asp:Label ID="LabelLine8" runat="server" CssClass="redAsterick">
                                                                    <span class="redAsterick">*</span> </asp:Label>
                                                                        <asp:Label ID="LabelCCStateStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92" height="30">
                                                                        <label id="lblCCZip" runat="server" for="CCZip" />
                                                                    </td>
                                                                    <td width="562" height="30">
                                                                        <SPS:SPSTextBox ID="CCZip" runat="server" CssClass="tableData" MaxLength="10"></SPS:SPSTextBox>
                                                                        <asp:Label ID="LabelLine9" runat="server" CssClass="redAsterick">
                                                                    <span class="redAsterick">*</span> </asp:Label>
                                                                        <asp:Label ID="LabelCCZipStar" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tableData" align="right" width="92">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td valign="top">
                                                                    </td>
                                                                    <td valign="top" width="210">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <table role="presentation">
                                                            <tr>
                                                                <td align="right" width="84">
                                                                    <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                </td>
                                                                <td width="572">
                                                                    <table width="240" border="0" id="tablebuttons" runat="server" role="presentation">
                                                                        <tr>
                                                                            <td width="5">
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="btnNext" runat="server" AlternateText="Next" ImageUrl="<%$Resources:Resource,img_btnNext%>" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageUrl="<%$Resources:Resource,sna_svc_img_7%>" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="ReturnToCartImageButton" runat="server" ImageUrl="<%$Resources:Resource,img_btnReturnToCart%>"
                                                                                    AlternateText="Return to Cart" PostBackUrl="~/vc.aspx" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdateProgress runat="server" ID="UpdateProgress3" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1">
                                                                                    <ProgressTemplate>
                                                                                        <img id="Img3" runat="Server" src="~/images/progbar.gif" height="12" alt="" />
                                                                                    </ProgressTemplate>
                                                                                </asp:UpdateProgress>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td valign="top" width="54">
                                                                    &nbsp;
                                                                </td>
                                                                <td valign="top" width="210">
                                                                    <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:ImageButton ID="btnReturnToCart" runat="server" ImageUrl="<%$Resources:Resource,img_btnReturnToCart%>"
                                                            AlternateText="Return to Cart" PostBackUrl="~/vc.aspx" Visible="false" />
                                                        <div id="divFiller" runat="server">
                                                        </div>
                                                    </td>
                                                    <td width="20">
                                                    </td>
                                                </tr>
                                            </table>
                                            </main>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="688">
                                            <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                                <img height="20" src="images/spacer.gif" width="25" alt="">
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </form>
</body>
</html>