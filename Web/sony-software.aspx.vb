Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class SoftwarePLUSSearch
        Inherits SSL

        'Protected WithEvents lblSubmitError As Label
        'Protected WithEvents lblSoftwareResults As Label
        'Protected WithEvents lblNextResult As Label

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.        
            isProtectedPage(False)
            InitializeComponent()
            FormFieldInitialization()
        End Sub

#End Region

#Region "Page members"
        '-- constants --
        'category drop down messages
        Private Const softwareCategoryDropdownUnavailableMessage As String = "No categories are available as this time."

        'search parameter array length
        Private Const conSearchParmsArrayLength As Int16 = 2
        Private Const conMaxResultsPerPage As Int16 = 50

        '-- variables --
        'result page variables
        Private currentResultPage As Integer = 1
        Private endingResultPage As Integer = 1
        Private startingResultItem As Integer = 1
        Private resultPageRequested As Integer = 3
        Private totalSearchResults As Integer = 0

        'sort order variables
        Private sortOrder As Software.enumSortOrder = Software.enumSortOrder.ProductCategoryASC

        'default form field with focus
        Public focusField As String = "txtModelNumber"
        Public bColor As Boolean = False

        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
            Current = 5
        End Enum

        Private Enum enumSortBy
            ProductCategory = 0
            ModelNumber = 1
            'KitPartNumber = 2
            'Version = 3
        End Enum
#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            errorMessageLabel.Text = ""     '-- clear the error label        
            Try
                '-- this is kind of jacked up but the only way i could make it work. 
                '-- if not post back the we want to check the event target form field to see if the add to cart button was click.
                '-- if it was then we change the id of the send and call the control method (processPageRequest). The ID is change
                '-- to make it easy to determine what method need to be called on the control method. 

                'If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                '    Me.DefaultSonySoftware.Visible = False
                '    Me.hiddenSonySoftware.Visible = True
                '    Return
                'Else
                '    Me.DefaultSonySoftware.Visible = True
                '    Me.hiddenSonySoftware.Visible = False
                'End If

                If Not Page.IsPostBack Then
                    If HttpContextManager.GlobalData.IsAmerica Then
                        lnkProSite.NavigateUrl = "https://pro.sony/en_US/support/software"
                    Else
                        If HttpContextManager.GlobalData.IsFrench Then
                            lnkProSite.NavigateUrl = "https://pro.sony/qf_CA/support/software"
                        Else
                            lnkProSite.NavigateUrl = "https://pro.sony/en_CA/support/software"
                        End If
                    End If

                    If Request.QueryString("Model") IsNot Nothing Then
                        If Request.QueryString("Model").Contains("/") Then
                            If Session("URLEncoded") Is Nothing Then
                                Session("URLEncoded") = "Yes"
                                Response.Redirect("sony-software-" + HttpUtility.UrlEncode(ParseURL(Request.QueryString("Model"))) + ".aspx")
                            Else
                                Session.Remove("URLEncoded")
                                txtModelNumber.Text = Request.QueryString("Model")
                            End If
                        Else
                            Session.Remove("URLEncoded")
                            txtModelNumber.Text = Request.QueryString("Model")
                        End If
                    End If

                    ProcessPageRequest(sender, e)

                    '-- hide model sort arrow on initial load
                    imgModelSort.Visible = False
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub allImage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click, btnNextPage.Click, btnSearch.Click, btnGoToPage.Click, btnPreviousPage2.Click, btnNextPage2.Click, btnGoToPage2.Click
            ProcessPageRequest(sender, e)
        End Sub

        Private Sub allLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSortByModel.Click, PreviousLinkButton.Click, NextLinkButton.Click, lnkSortByCategory.Click, PreviousLinkButton2.Click, NextLinkButton2.Click   ' ProductCategorySortLink2.Click, 
            ProcessPageRequest(sender, e)
        End Sub
#End Region

#Region "Methods"

        Private Sub ProcessPageRequest(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim callingControl As String = String.Empty

                If Not sender.ID Is Nothing Then callingControl = sender.ID.ToString()

                Select Case (callingControl)
                    Case Page.ID.ToString()
                        PopulateSoftwareCategoryDropDown()              '-- populate the ddl
                End Select

                resultPageRequested = GetRequestedPage(callingControl)  '-- set the result page requested             
                SetSortOrder(callingControl)                            '-- set the sort order
                SearchForSoftware()                                     '-- search for software
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "Search Functionality"
        Private Sub SearchForSoftware()
            Try
                Dim model_number As String = txtModelNumber.Text.Trim().Replace("-", "")
                Dim description As String = txtModelDesc.Text.Trim().Replace("-", "")
                Dim category As String = ddSoftwareCategory.SelectedValue.Trim()

                '-- just a check to make sure we are not passing in to much data. --
                If model_number.Length > 30 Then model_number.Substring(0, 30)
                If description.Length > 100 Then description.Substring(0, 100)
                If category.Length > 100 Then category.Substring(0, 100)
                If category.ToString = Resources.Resource.el_AllCategories Then category = "" '-- if category name = "all categories" then clear category name to return all categories.

                BuildSearchResultsTable(model_number, category, description)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function GetSearchParms() As String()
            Dim returnSearchParms(conSearchParmsArrayLength) As String

            Return returnSearchParms
        End Function

        Private Function BuildSearchResultsTable(ByVal model_number As String, ByVal category As String, ByVal description As String) As Table
            Dim softwareResults() As Software
            Dim softwareResultsDisplayed As New ArrayList
            Dim resultTable As New Table
            Dim currentResultItem As Int32 = 1
            Dim x As Integer = 0

            Try
                'Dim accountType As enumAccountType = SSL.enumAccountType.NoAccountTypeFound
                'If Not Session.Item("customer") Is Nothing Then accountType = getUsersAccountType(CType(Session.Item("customer"), Customer).SIAMIdentity.ToString())


                '-- get search results collection
                softwareResults = GetSoftwareResultCollection(model_number, category, description)

                If softwareResults.Length <> 0 Then
                    '-- calculate pagination 
                    totalSearchResults = softwareResults.Length
                    CalcStaringResultItem(resultPageRequested)

                    'sort the collection of software
                    'If IsSort Then
                    Software.SortOrder = sortOrder
                    Array.Sort(softwareResults)
                    ' End If

                    '-- loop through all the software 
                    For Each softwareItem As Software In softwareResults
                        If currentResultItem >= startingResultItem Then
                            SoftwareSearchResultTable.Controls.Add(BuildSoftwareResultRow(softwareItem, x))
                            softwareResultsDisplayed.Add(softwareItem)
                            x = (x + 1)
                        End If
                        currentResultItem = (currentResultItem + 1)
                        If currentResultItem >= (startingResultItem + conMaxResultsPerPage) Then Exit For
                    Next

                    '-- add collection of displayed software to array - this is used for add to cart functionality.
                    Session.Add("software", softwareResultsDisplayed)
                Else
                    '-- no software found --
                    SoftwareSearchResultTable.Controls.Add(BuildSoftwareResultRow(totalSearchResults))
                End If

                '-- update the pagination controlsl                 
                UpdateResultPageNav()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return resultTable
        End Function

        Private Function GetSoftwareResultCollection(ByVal model_number As String, ByVal category As String, ByVal description As String) As Software()
            Dim resultCollection As Software() = Nothing
            Dim catalogManager As New CatalogManager

            Try
                resultCollection = catalogManager.SoftwareModelSearch(model_number, category, description)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return resultCollection
        End Function

        Private Function GetRequestedPage(ByVal callingControl As String) As enumResultPageRequested
            Dim returnValue As enumResultPageRequested = enumResultPageRequested.Current
            Try
                Select Case (callingControl)
                    Case Page.ID.ToString()
                        returnValue = enumResultPageRequested.Current
                    Case btnSearch.ID.ToString()
                        returnValue = enumResultPageRequested.FirstPage
                    Case btnGoToPage.ID.ToString, btnGoToPage2.ID.ToString
                        returnValue = enumResultPageRequested.UserEnteredPage
                    Case btnPreviousPage.ID.ToString, PreviousLinkButton.ID.ToString(), btnPreviousPage2.ID.ToString, PreviousLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.PreviousPage
                    Case btnNextPage.ID.ToString(), NextLinkButton.ID.ToString(), btnNextPage2.ID.ToString(), NextLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.NextPage
                    Case lnkSortByCategory.ID.ToString, lnkSortByModel.ID.ToString   ' ProductCategorySortLink2.ID.ToString,
                        returnValue = enumResultPageRequested.UserEnteredPage
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

#End Region

#Region "result table"
        Private Function BuildSoftwareResultRow(ByVal software As Software, ByVal x As Integer) As TableRow
            Dim tr As New TableRow
            Dim td As New TableCell
            Dim productCategory As New Label
            'Dim modelNumber As New Label
            Dim modelNumber As New HyperLink
            Dim modelDescription As New Label

            Try
                productCategory.ID = "productCategory" + x.ToString
                productCategory.CssClass = "tableData"
                productCategory.Text = "&nbsp;"

                modelNumber.ID = "modelNumber" + x.ToString
                modelNumber.CssClass = "tableData"
                modelNumber.ForeColor = System.Drawing.Color.Blue
                modelNumber.Text = "&nbsp;"

                modelDescription.ID = "modelDescription" + x.ToString
                modelDescription.CssClass = "tableData"
                modelDescription.Text = "&nbsp;"

                'modelNumber.NavigateUrl = "sony-software-model.aspx?model=" + software.ModelNumber.ToString()
                ' Dim strModelNew = HttpUtility.UrlEncode(parseURL(software.ModelNumber.ToString().Replace("/", "+")))

                Dim strModelNew = HttpUtility.UrlEncode(ParseURL(ReturnCharacterSub(software.ModelNumber.ToString())))

                If strModelNew.Contains("%2b") And software.ModelNumber.Contains("/") Then
                    strModelNew = strModelNew.Replace("%2b", "+")
                End If

                modelNumber.NavigateUrl = "sony-software-model-" + strModelNew + ".aspx"
                'modelNumber.NavigateUrl = "sony-software-model-" + HttpUtility.UrlEncode(parseURL(software.ModelNumber.ToString().Replace("/", "+"))) + ".aspx"

                If Not software.ProductCategory Is Nothing Then productCategory.Text = software.ProductCategory.ToString()
                If Not software.ModelNumber Is Nothing Then modelNumber.Text = software.ModelNumber.ToString()
                If Not software.ModelDescription Is Nothing Then modelDescription.Text = software.ModelDescription.ToString()

                If bColor = False Then
                    ' -- product category
                    td.Controls.Add(New LiteralControl("<tr style=""height: 20px;"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"">" + vbCrLf))
                    td.Controls.Add(productCategory)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- model number
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"">" + vbCrLf))
                    td.Controls.Add(modelNumber)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- model description
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"">" + vbCrLf))
                    td.Controls.Add(modelDescription)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- spacer
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    tr.Controls.Add(td)

                    bColor = True
                Else
                    ' -- product category
                    td.Controls.Add(New LiteralControl("<tr style=""height: 20px;"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" bgcolor=""F2F5F8"">" + vbCrLf))
                    td.Controls.Add(productCategory)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- model number
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" bgcolor=""F2F5F8"">" + vbCrLf))
                    td.Controls.Add(modelNumber)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- model description
                    td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" bgcolor=""F2F5F8"">" + vbCrLf))
                    td.Controls.Add(modelDescription)
                    td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                    ' -- spacer
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    tr.Controls.Add(td)

                    bColor = False
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        ''' <summary>
        ''' This method provides an override for when no search results are returned.
        ''' </summary>
        Private Function BuildSoftwareResultRow(ByVal totalResults As Integer) As TableRow
            Dim tr As New TableRow

            Dim td As New TableCell

            '-- no result for search - show search parameters
            td.Controls.Add(New LiteralControl($"<tr style=""height: 20px;"">{Environment.NewLine}"))
            td.Controls.Add(New LiteralControl($"    <td colspan=""3"" class=""bodyCopy""><strong>No match found for</strong></td>{Environment.NewLine}"))
            td.Controls.Add(New LiteralControl($"</tr>{Environment.NewLine}"))

            If ddSoftwareCategory.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl($"<tr style=""height: 20px;"">{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"    <td colspan=""3"" class=""bodyCopy""><strong>{Resources.Resource.el_ProductCategory}:</strong> {ddSoftwareCategory.SelectedValue}</td>{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"</tr>{Environment.NewLine}"))
            End If

            If txtModelNumber.Text <> "" Then
                td.Controls.Add(New LiteralControl($"<tr style=""height: 20px;"">{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"    <td colspan=""3"" class=""bodyCopy""><strong>{Resources.Resource.el_ModelNumber}:</strong> {txtModelNumber.Text}</td>{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"</tr>{Environment.NewLine}"))
            End If

            If txtModelDesc.Text <> "" Then
                td.Controls.Add(New LiteralControl($"<tr style=""height: 20px;"">{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"    <td colspan=""3"" class=""bodyCopy""><strong>{Resources.Resource.el_ModelDescription}:</strong> {txtModelDesc.Text}</td>{Environment.NewLine}"))
                td.Controls.Add(New LiteralControl($"</tr>{Environment.NewLine}"))
            End If

            tr.Controls.Add(td)

            Return tr
        End Function


        'Private Function buildReleaseNotesData(ByRef fileName As String, ByRef description As String) As String
        '    Dim returnValue As String = String.Empty
        '    If File.Exists(downloadPath + "/" + conReleaseNotesPath + "/" + fileName) Then
        '        returnValue = "<a href=""" + downloadURL + "/" + conReleaseNotesPath + "/" + fileName + """ target=""_blank"" id=""bodyClass"">"
        '        If Not description Is Nothing Then
        '            returnValue = returnValue + description + "</a>"
        '        Else
        '            returnValue = returnValue + "<img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Release Notes""/></a>"
        '        End If
        '    End If
        '    Return returnValue
        'End Function
#End Region

#Region "Pagination"
        Private Sub CalcCurrentResultPage()
            Try
                currentResultPage = Int(startingResultItem / conMaxResultsPerPage) + IIf((startingResultItem Mod conMaxResultsPerPage) = 0, 0, 1)
                If currentResultPage > endingResultPage Then currentResultPage = endingResultPage
            Catch ex As Exception
                currentResultPage = 1
            End Try
        End Sub

        Private Sub CalcEndingResultPage()
            Try
                endingResultPage = Int(totalSearchResults / conMaxResultsPerPage) + IIf((totalSearchResults Mod conMaxResultsPerPage) = 0, 0, 1)
            Catch ex As Exception
                endingResultPage = 1
            End Try
        End Sub

        Private Sub CalcStaringResultItem(ByVal thisPage As enumResultPageRequested)
            Dim tempPageNumber As Integer = 1
            Dim tempCurrentPageNumber As Integer = 1
            CalcEndingResultPage()

            Try
                If txtPageNumber.Text.ToString().Trim() <> "" Or txtPageNumber2.Text.ToString().Trim() <> "" Then
                    If IsNumeric(txtPageNumber.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                    ElseIf IsNumeric(txtPageNumber2.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber2.Text.ToString())
                    End If
                    If tempPageNumber > endingResultPage Then tempPageNumber = endingResultPage
                End If

                If lblCurrentPage.Text.ToString().Trim() <> "" Then
                    If IsNumeric(lblCurrentPage.Text.ToString()) Then tempCurrentPageNumber = Integer.Parse(lblCurrentPage.Text.ToString())
                End If

                Select Case (thisPage)
                    Case enumResultPageRequested.FirstPage
                        startingResultItem = 1
                    Case enumResultPageRequested.LastPage
                        If endingResultPage > 1 Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.NextPage
                        If (tempCurrentPageNumber + 1) > endingResultPage Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = (((tempCurrentPageNumber) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.PreviousPage
                        If tempCurrentPageNumber > 2 Then
                            startingResultItem = (((tempCurrentPageNumber - 2) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.UserEnteredPage
                        If tempPageNumber <= 1 Then
                            startingResultItem = 1
                        Else
                            startingResultItem = (((tempPageNumber - 1) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.Current
                        startingResultItem = (((tempCurrentPageNumber - 1) * conMaxResultsPerPage) + 1)

                End Select

                CalcCurrentResultPage()

            Catch ex As Exception
                startingResultItem = 1
            End Try
        End Sub

        Private Sub UpdateResultPageNav()
            Try
                '2018-10-12 ASleight - Hide all pagination when there is only one page, to reduce screen clutter and confusion.
                If endingResultPage = 1 Then
                    tblPagerTop.Visible = False
                    tblPagerBottom.Visible = False
                    Exit Sub
                End If

                Dim hidePrevButtons As Boolean = False
                Dim hideNextButtons As Boolean = False

                PreviousLinkButton.Visible = True
                btnPreviousPage.Visible = True
                PreviousLinkButton2.Visible = True
                btnPreviousPage2.Visible = True
                NextLinkButton.Visible = True
                btnNextPage.Visible = True
                NextLinkButton2.Visible = True
                btnNextPage2.Visible = True

                Select Case resultPageRequested
                    Case enumResultPageRequested.FirstPage
                        hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.LastPage
                        hideNextButtons = True

                        lblCurrentPage.Text = endingResultPage.ToString()
                        lblCurrentPage2.Text = endingResultPage.ToString()
                    Case enumResultPageRequested.NextPage
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.PreviousPage
                        If currentResultPage = 1 Then hidePrevButtons = True


                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.UserEnteredPage, enumResultPageRequested.Current
                        If currentResultPage = 1 Then hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case Else
                        lblCurrentPage.Text = "1"
                End Select

                If hidePrevButtons Then
                    PreviousLinkButton.Visible = False
                    btnPreviousPage.Visible = False
                    PreviousLinkButton2.Visible = False
                    btnPreviousPage2.Visible = False
                End If
                If hideNextButtons Then
                    NextLinkButton.Visible = False
                    btnNextPage.Visible = False
                    NextLinkButton2.Visible = False
                    btnNextPage2.Visible = False
                End If

                lblEndingPage.Text = endingResultPage.ToString()
                lblEndingPage2.Text = endingResultPage.ToString()
                txtPageNumber.Text = ""
                txtPageNumber2.Text = ""
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        Private Sub SetSortOrder(ByVal callingControl As String)
            Select Case (callingControl)
                Case lnkSortByCategory.ID.ToString()
                    If lnkSortByCategory.CommandArgument = Software.enumSortOrder.ProductCategoryASC.ToString() Then
                        sortOrder = Software.enumSortOrder.ProductCategoryASC
                        lnkSortByCategory.CommandArgument = Software.enumSortOrder.ProductCategoryDESC.ToString()
                        imgPCSort.Src = "images/arrow_up.gif"
                    Else
                        sortOrder = Software.enumSortOrder.ProductCategoryDESC
                        lnkSortByCategory.CommandArgument = Software.enumSortOrder.ProductCategoryASC.ToString()
                        imgPCSort.Src = "images/arrow_down.gif"
                    End If
                    lnkSortByModel.CommandArgument = Software.enumSortOrder.ModelNumberASC.ToString()
                    imgPCSort.Visible = True
                    imgModelSort.Visible = False

                    btnSearch.CommandArgument = enumSortBy.ProductCategory.ToString()
                Case lnkSortByModel.ID
                    If lnkSortByModel.CommandArgument.ToString() = Software.enumSortOrder.ModelNumberASC.ToString() Then
                        sortOrder = Software.enumSortOrder.ModelNumberASC
                        lnkSortByModel.CommandArgument = Software.enumSortOrder.ModelNumberDESC.ToString()
                        'ProductCategorySortLink2.CommandArgument = enumSortOrder.ProductCategoryASC.ToString()
                        imgModelSort.Src = "images/arrow_up.gif"
                    Else
                        sortOrder = Software.enumSortOrder.ModelNumberDESC
                        lnkSortByModel.CommandArgument = Software.enumSortOrder.ModelNumberASC.ToString()
                        'ProductCategorySortLink2.CommandArgument = enumSortOrder.ProductCategoryASC.ToString()
                        imgModelSort.Src = "images/arrow_down.gif"
                    End If
                    lnkSortByCategory.CommandArgument = Software.enumSortOrder.ProductCategoryASC.ToString()
                    imgPCSort.Visible = False
                    imgModelSort.Visible = True

                    btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString()

                    'Case btnSearch.ID.ToString()
                    '    ' we need to sort my model number if in a Product Category Search and "All Categories" is not selected 
                    '    If ddSoftwareCategory.SelectedValue.ToString() <> "" Then
                    '        sortOrder = enumSortOrder.ModelNumberASC
                    '        lnkSortByModel.CommandArgument = enumSortOrder.ModelNumberDESC.ToString()
                    '        Me.imgModelSort.Src = "images/arrow_up.gif"
                    '        Me.imgPCSort.Visible = False
                    '        Me.imgModelSort.Visible = True
                    '        btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString()
                    '    Else
                    '        sortOrder = enumSortOrder.ProductCategoryASC
                    '        lnkSortByModel.CommandArgument = enumSortOrder.ModelNumberDESC.ToString()
                    '        'lnkSortByCategory.CommandArgument = enumSortOrder.ProductCategoryDESC.ToString()
                    '        'ProductCategorySortLink2.CommandArgument = enumSortOrder.ProductCategoryDESC.ToString()
                    '        Me.imgPCSort.Src = "images/arrow_up.gif"
                    '        Me.imgModelSort.Visible = False
                    '        Me.imgPCSort.Visible = True
                    '        btnSearch.CommandArgument = enumSortBy.ProductCatagory.ToString()
                    '    End If

                    '' sorting is done in the stored proc, disable sorting when search button is clicked 
                    'IsSort = False
                Case btnSearch.ID.ToString(), btnGoToPage.ID.ToString(), btnNextPage.ID.ToString(), btnPreviousPage.ID.ToString(), NextLinkButton.ID.ToString(), PreviousLinkButton.ID.ToString(), btnGoToPage2.ID.ToString(), btnNextPage2.ID.ToString(), btnPreviousPage2.ID.ToString(), NextLinkButton2.ID.ToString(), PreviousLinkButton2.ID.ToString()
                    If btnSearch.CommandArgument = enumSortBy.ProductCategory.ToString() Then
                        If lnkSortByCategory.CommandArgument.ToString() = Software.enumSortOrder.ProductCategoryASC.ToString() Then
                            sortOrder = Software.enumSortOrder.ProductCategoryDESC
                            imgPCSort.Src = "images/arrow_down.gif"
                        Else
                            sortOrder = Software.enumSortOrder.ProductCategoryASC
                            imgPCSort.Src = "images/arrow_up.gif"
                        End If
                        imgModelSort.Visible = False
                        imgPCSort.Visible = True
                    ElseIf btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString() Then
                        If lnkSortByModel.CommandArgument.ToString() = Software.enumSortOrder.ModelNumberASC.ToString() Then
                            sortOrder = Software.enumSortOrder.ModelNumberDESC
                            imgModelSort.Src = "images/arrow_down.gif"
                        Else
                            sortOrder = Software.enumSortOrder.ModelNumberASC
                            imgModelSort.Src = "images/arrow_up.gif"
                        End If
                        imgPCSort.Visible = False
                        imgModelSort.Visible = True
                    Else
                        sortOrder = Software.enumSortOrder.ProductCategoryASC
                        imgPCSort.Src = "images/arrow_up.gif"
                        imgModelSort.Visible = False
                        imgPCSort.Visible = True
                        btnSearch.CommandArgument = enumSortBy.ProductCategory.ToString()
                    End If

            End Select
        End Sub

        Private Sub PopulateSoftwareCategoryDropDown()
            Try
                Dim categoryManager As New CatalogManager
                Dim categories() As String
                categories = AddItemToCategoriesArray(categoryManager.GetSoftwareCategories())        'get an array of all the software categories 
                ddSoftwareCategory.DataSource = categories
                Page.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function AddItemToCategoriesArray(ByVal categories() As String) As String()
            Dim returnValue() As String = Nothing
            Try
                Dim tempCategories As New ArrayList

                If Not categories Is Nothing Then
                    If categories.Length > 0 Then
                        'tempCategories.Add(softwareCategoryDropdownMessage)
                        tempCategories.Add(Resources.Resource.el_AllCategories)

                        For Each tempCategory As String In categories
                            tempCategories.Add(tempCategory)
                        Next
                    Else
                        tempCategories.Add(softwareCategoryDropdownUnavailableMessage)
                    End If
                Else
                    tempCategories.Add(softwareCategoryDropdownUnavailableMessage)
                End If

                returnValue = tempCategories.ToArray(Type.GetType("System.String"))
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

#Region "Helpers"
        Private Sub FormFieldInitialization()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
            errorMessageLabel.Text = ""
        End Sub

        Private Function ParseURL(ByVal psURL As String) As String
            Dim sURL As String = psURL
            Dim token As String = "//"
            Dim index As Integer = sURL.IndexOf(token)

            ' if no -1
            If index <> -1 Then
                sURL = Left(sURL, index + 1) + "+" + sURL.Substring(index + 2).ToString()
            End If

            token = "\"
            index = sURL.IndexOf(token)
            If index <> -1 Then
                sURL = Left(sURL, index + 1) + "!" + sURL.Substring(index + 2).ToString()
            End If

            Return sURL
        End Function

        'Private Function getDeliveryMethod(ByVal itemNumber As Integer) As ShoppingCartItem.DeliveryMethod
        '    Try
        '        Dim returnValue As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship
        '        Dim willCustomerDownload As Boolean = False
        '        Dim willCustomerBeShipped As Boolean = False

        '        If Not Request.Form("deliverymethod" + itemNumber.ToString()) Is Nothing Then
        '            '-- is the download selected.
        '            If Request.Form("deliverymethod" + itemNumber.ToString()) = ("download" + itemNumber.ToString()) Then
        '                willCustomerDownload = True
        '            End If
        '            If Request.Form("deliverymethod" + itemNumber.ToString()) = ("ship" + itemNumber.ToString()) Then
        '                willCustomerBeShipped = True
        '            End If
        '        End If

        '        If willCustomerDownload And willCustomerBeShipped Then
        '            returnValue = ShoppingCartItem.DeliveryMethod.DownloadAndShip
        '        Else
        '            If willCustomerDownload And Not willCustomerBeShipped Then
        '                returnValue = ShoppingCartItem.DeliveryMethod.Download
        '            ElseIf Not willCustomerDownload And Not willCustomerBeShipped Then
        '                returnValue = Nothing
        '            End If
        '        End If

        '        Return returnValue
        '    Catch ex As Exception
        '        errorMessageLabel.Text = ex.Message.ToString()
        '    End Try

        'End Function

        'Private Function didCustomerSelectDeliveryMethod(ByRef thisSoftware As Software, ByVal itemNumber As Integer) As Boolean
        '    Dim returnValue As Boolean = True
        '    Try
        '        If Not thisSoftware Is Nothing Then
        '            Dim willCustomerDownload As Boolean = False
        '            Dim willCustomerBeShipped As Boolean = False


        '            '-- does the file exist for download.         
        '            If File.Exists(downloadPath + "/" + conSoftwareDownloadPath + "/" + thisSoftware.DownloadFileName.ToString()) Then
        '                If thisSoftware.FreeOfCharge = Software.FreeOfChargeType.NotFree Then
        '                    If Not Request.Form("deliverymethod" + itemNumber.ToString()) Is Nothing Then
        '                        '-- is the download selected.
        '                        If Request.Form("deliverymethod" + itemNumber.ToString()) = ("download" + itemNumber.ToString()) Then
        '                            willCustomerDownload = True
        '                        End If
        '                        If Request.Form("deliverymethod" + itemNumber.ToString()) = ("ship" + itemNumber.ToString()) Then
        '                            willCustomerBeShipped = True
        '                        End If
        '                    End If
        '                Else 'if free download, and still click on add cart, means want to shipped
        '                    willCustomerBeShipped = True
        '                End If
        '                If Not willCustomerDownload And Not willCustomerBeShipped Then returnValue = False
        '            End If
        '        Else
        '            errorMessageLabel.Text = "Error getting delivery method. Software does not exist."
        '        End If
        '    Catch ex As Exception
        '        errorMessageLabel.Text = "Error getting software delivery method."
        '    End Try
        '    Return returnValue
        'End Function

#End Region

        ' 2016-06-07 ASleight - This method is no longer used
        '    Private Sub LoadGlobalData()
        '        Try
        'Dim gdobj As GlobalData = Nothing
        '            Dim culinfo As String = String.Empty

        '            If (Session("SelectedLang") IsNot Nothing) Then
        '                Dim ci As New CultureInfo(Session("SelectedLang").ToString())
        '                Page.Culture = ci.ToString()
        '                Page.UICulture = ci.ToString()
        '            Else
        '                HttpContext.Current.Session.Add("SelectedLang", "en-US")
        '                Page.Culture = "en-US"
        '                Page.UICulture = "en-US"
        '            End If

        '            gdobj = New GlobalData()

        '            Dim RequestURL As String = String.Empty
        '            If ConfigurationData.GeneralSettings.Environment = "Development" Then
        '                RequestURL = Request.Url.AbsolutePath
        '                'RequestURL = "https://servicesplus-qa.sony.ca/default.aspx"
        '            Else
        '                RequestURL = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_HOST")
        '            End If

        '            If RequestURL.Contains("sony.ca") Then
        '                gdobj.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
        '                If Not Session.Item("SelectedLang") Is Nothing Then
        '                    culinfo = Session.Item("SelectedLang")
        '                    gdobj.Language = culinfo
        '                Else
        '                    gdobj.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
        '                End If
        '            Else
        '                gdobj.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
        '                gdobj.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
        '            End If
        '            gdobj.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
        '            gdobj.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value

        '            If (gdobj IsNot Nothing) Then
        '                HttpContext.Current.Session.Add("GlobalData", gdobj)
        '            End If
        '        Catch ex As Exception

        '        End Try
        '    End Sub

#End Region

    End Class

End Namespace
