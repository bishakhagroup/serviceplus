Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException


Namespace ServicePLUSWebApp


    Partial Class TechBulletinSearch
        Inherits SSL

        '-- asp generated code ------------------------------------------------------------------------
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblSubmitError As System.Web.UI.WebControls.Label
        Protected WithEvents lblSoftwareResults As System.Web.UI.WebControls.Label
        Protected WithEvents lblNextResult As System.Web.UI.WebControls.Label
        Protected WithEvents phCalendar1 As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents phCalendar2 As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents txtCal1 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents txtCal2 As Sony.US.ServicesPLUS.Controls.SPSTextBox
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.






        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.        
            isProtectedPage(False)
            InitializeComponent()
            formFieldInitialization()
        End Sub

#End Region

#Region "Page members"

#Region "constants"
		'category drop down messages
		Private Const softwareCategoryDropdownMessage As String = "All categories."
		Private Const softwareCategoryDropdownUnavailableMessage As String = "No categories are available as this time."
		'search parameter array length
		Private Const conSearchParmsArrayLength As Int16 = 2
		Private Const conMaxResultsPerPage As Int16 = 50
		'file size denominator
		Private Const MegaBytes As Integer = 1064960
		Private Const KiloBytes As Integer = 1024

		''file path
		'Private Const conSoftwareDownloadPath As String = "Software"
		'Private Const conReleaseNotesPath As String = "ReleaseNotes"

		'modified to read from config file by Deepa V, Mar 24 ,2006
		'file path
		Private conTechBulletinPath As String


		'use to tell if the addToCart icon was clicked.
		Private Const conAddToCart As String = "addToCart"
		Private Const conTBGeneralInfo As String = "Sony-Technical-Bulletins-GenInfo.aspx"

#End Region

#Region "    Variables    "
		'result page variables
		Shared currentResultPage As Integer = 1
		Shared endingResultPage As Integer = 1
		Shared startingResultItem As Integer = 1
		Shared resultPageRequested As Integer = 3
		Shared totalSearchResults As Long = 0
		Shared currRecordCount As Long = 0
		Shared lowerLimit As Long = 1
		Shared upperLimit As Long = 1
		Shared searchCondition As String = ""

		'sort order variables
		Private sortOrder As enumSortOrder = enumSortOrder.ProductCategoryASC
		'file download and release notes directory    
		Private downloadURL As String
		Private downloadPath As String
		'default form field with focus
		Public focusField As String = "txtModelNo"
        'Dim objUtilties As Utilities = New Utilities
		'Dim ddate As WebCalControl = Nothing
		'Dim ddate1 As WebCalControl = Nothing

#End Region

#Region "   Enumerations  "
		'-- result page
		Private Enum enumResultPageRequested
			PreviousPage = 0
			NextPage = 1
			UserEnteredPage = 2
			FirstPage = 3
			LastPage = 4
			Current = 5
		End Enum

		'-- sort order 
		Private Enum enumSortOrder
			ProductCategoryASC = 0
			ProductCategoryDESC = 1
			ModelNumberASC = 2
			ModelNumberDESC = 3
		End Enum

		'-- sort type
		Private Enum enumSortBy
			ProductCatagory = 0
			ModelNumber = 1
		End Enum
#End Region

#End Region

#Region "Events"
		Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
			Try
				AddToCartAfterLogin(errorMessageLabel)
				setFileDownloadPaths()
				'----- Check if the customer has logged in and has a valid subscription id
				If Session.Item("customer") IsNot Nothing And Session.Item("IsValidSubscription") IsNot Nothing Then
					If CType(Session.Item("IsValidSubscription"), Boolean) = True Then
						If Not IsPostback() Then
							errorMessageLabel.Text = ""
							populateProductLineDropDown()
							totalSearchResults = GetRecordcount(String.Empty, "false")
							lowerLimit = 0
							upperLimit = conMaxResultsPerPage
							currentResultPage = 1
							If totalSearchResults > conMaxResultsPerPage Then
								endingResultPage = Convert.ToInt64(totalSearchResults / conMaxResultsPerPage)
							Else
								endingResultPage = 1
								'when the total search count is less than 50, then search only until that count
								upperLimit = totalSearchResults
							End If
							searchCondition = ""
							populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
							loadDateControls()
						End If
					Else
						Response.Redirect(conTBGeneralInfo)
						Response.End()
					End If
				Else
					Response.Redirect(conTBGeneralInfo)
					Response.End()
				End If

			Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                '-- when there is some input error, the navigations shouldnt be displayed
                If errorMessageLabel.Text <> "" Then
                    hideNavigations()
                End If
            End Try
        End Sub

#End Region

#Region "Methods"
        Private Sub loadDateControls()
            Try
                'ddate = CType(Page.LoadControl("WebCalControl.ascx"), WebCalControl)
                'ddate.DefaultDate = Convert.ToDateTime("1/1/1967")
                'ddate.LowerLimit = Convert.ToDateTime("1/1/1967")
                'ddate.UpperLimit = DateTime.Today()
                'phCalendar1.Controls.Add(ddate)

                'ddate1 = CType(Page.LoadControl("WebCalControl.ascx"), WebCalControl)
                'ddate1.DefaultDate = DateTime.Today()
                'ddate1.LowerLimit = ddate.getSelectedDate()
                'ddate1.UpperLimit = DateTime.Today()
                'phCalendar2.Controls.Add(ddate1)

                Dim dateFrom As Date = Convert.ToDateTime("01/01/1967")
                Dim dateTo As Date = DateTime.Today()

                txtDateFrom.Text = dateFrom.ToShortDateString()
                txtDateTo.Text = dateTo.ToShortDateString()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub updatePageDisplay()
            Me.lblCurrentPage.Text = currentResultPage
            Me.lblEndingPage.Text = endingResultPage
            Me.lblCurrentPage2.Text = currentResultPage
            Me.lblEndingPage2.Text = endingResultPage
            If endingResultPage = 1 Then ' only one page is available 
                Me.lblPage1.Visible = False
                Me.lblPage2.Visible = False
                Me.txtPageNumber.Visible = False
                Me.txtPageNumber2.Visible = False
                Me.btnGoToPage.Visible = False
                Me.btnGoToPage2.Visible = False
                Me.btnNextPage.Visible = False
                Me.btnNextPage2.Visible = False
                Me.btnPreviousPage.Visible = False
                Me.btnPreviousPage2.Visible = False
                Me.PreviousLinkButton.Visible = False
                Me.PreviousLinkButton2.Visible = False
                Me.NextLinkButton.Visible = False
                Me.NextLinkButton2.Visible = False
            ElseIf endingResultPage = currentResultPage Then ' reached the last page
                Me.lblPage1.Visible = True
                Me.lblPage2.Visible = True
                Me.txtPageNumber.Visible = True
                Me.txtPageNumber2.Visible = True
                Me.btnGoToPage.Visible = True
                Me.btnGoToPage2.Visible = True
                Me.btnNextPage.Visible = False
                Me.btnNextPage2.Visible = False
                Me.btnPreviousPage.Visible = True
                Me.btnPreviousPage2.Visible = True
                Me.PreviousLinkButton.Visible = True
                Me.PreviousLinkButton2.Visible = True
                Me.NextLinkButton.Visible = False
                Me.NextLinkButton2.Visible = False
            ElseIf currentResultPage = 1 Then ' first page
                Me.lblPage1.Visible = True
                Me.lblPage2.Visible = True
                Me.txtPageNumber.Visible = True
                Me.txtPageNumber2.Visible = True
                Me.btnGoToPage.Visible = True
                Me.btnGoToPage2.Visible = True
                Me.btnNextPage.Visible = True
                Me.btnNextPage2.Visible = True
                Me.btnPreviousPage.Visible = False
                Me.btnPreviousPage2.Visible = False
                Me.PreviousLinkButton.Visible = False
                Me.PreviousLinkButton2.Visible = False
                Me.NextLinkButton.Visible = True
                Me.NextLinkButton2.Visible = True
            Else ' intermittent pages - make all buttons visible
                Me.lblPage1.Visible = True
                Me.lblPage2.Visible = True
                Me.txtPageNumber.Visible = True
                Me.txtPageNumber2.Visible = True
                Me.btnGoToPage.Visible = True
                Me.btnGoToPage2.Visible = True
                Me.btnNextPage.Visible = True
                Me.btnNextPage2.Visible = True
                Me.btnPreviousPage.Visible = True
                Me.btnPreviousPage2.Visible = True
                Me.PreviousLinkButton.Visible = True
                Me.PreviousLinkButton2.Visible = True
                Me.NextLinkButton.Visible = True
                Me.NextLinkButton2.Visible = True
            End If
        End Sub

        Private Sub fetchNextResultSet()
            If lowerLimit < upperLimit Then
                lowerLimit = upperLimit + 1 ' 0-50, 51-100
            End If

            If upperLimit < totalSearchResults Then
                If (totalSearchResults - upperLimit) > conMaxResultsPerPage Then
                    upperLimit += conMaxResultsPerPage
                Else
                    upperLimit = totalSearchResults - upperLimit
                End If
            End If
            populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
            currentResultPage += 1
            updatePageDisplay()
        End Sub

        Private Sub fetchPreviousResultSet()
            Try
                If lowerLimit < upperLimit And lowerLimit > 1 Then
                    upperLimit = lowerLimit - 1
                    If upperLimit - conMaxResultsPerPage + 1 > 0 Then
                        lowerLimit = upperLimit - conMaxResultsPerPage + 1
                    Else
                        lowerLimit = 0
                    End If
                End If
                populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                currentResultPage -= 1
                updatePageDisplay()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub fetchSpecifiedResultSet(ByVal btnId As Integer)
            errorMessageLabel.Text = ""
            Dim tempPageNumber As Long = 0
            Dim valTxt As Int64
            Try
                If txtPageNumber2.Text.Trim() <> "" And btnId = 2 Then
                    valTxt = Convert.ToInt64(txtPageNumber2.Text.Trim())
                End If
                If txtPageNumber.Text.Trim() <> "" And btnId = 1 Then
                    valTxt = Convert.ToInt64(txtPageNumber.Text.Trim())
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return
            End Try
            If txtPageNumber.Text.Trim() <> "" Or txtPageNumber2.Text.Trim() <> "" Or valTxt < 1 Then
                If IsNumeric(txtPageNumber.Text.ToString()) And btnId = 1 Then
                    tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                ElseIf IsNumeric(txtPageNumber2.Text.ToString()) And btnId = 2 Then
                    tempPageNumber = Integer.Parse(txtPageNumber2.Text.ToString())
                End If
                If Not tempPageNumber > endingResultPage Then ' tempPageNumber < or = endingResultPage
                    errorMessageLabel.Text = ""
                    If tempPageNumber = 1 Then
                        lowerLimit = 0
                        If totalSearchResults > conMaxResultsPerPage Then
                            upperLimit = conMaxResultsPerPage
                        Else
                            upperLimit = totalSearchResults
                        End If
                    Else
                        ' the requested page is not the first page
                        If tempPageNumber = endingResultPage Then
                            lowerLimit = (tempPageNumber - 1) * conMaxResultsPerPage + 1
                            upperLimit = totalSearchResults
                        Else
                            ' the requested page is in between the first and the last page
                            upperLimit = conMaxResultsPerPage * tempPageNumber
                            lowerLimit = upperLimit - conMaxResultsPerPage + 1
                        End If
                    End If
                    populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                    currentResultPage = tempPageNumber
                    updatePageDisplay()
                Else
                    errorMessageLabel.Text = "Please enter a page lesser than the total number of pages"
                End If
            Else
                errorMessageLabel.Text = "Please enter a valid page number"
            End If
            txtPageNumber.Text = ""
            txtPageNumber2.Text = ""
        End Sub


        Private Function GetRecordcount(ByVal strSearch As String, ByVal searchSerialNo As String) As Long
            Dim techManager As TechBulletinManager = New TechBulletinManager
            Dim recCount As Long = techManager.getTechBulletinRecCount(strSearch, searchSerialNo)
            Return recCount
        End Function

        Private Sub populateDataGrid(ByVal strSearch As String)
            Dim techManager As TechBulletinManager = New TechBulletinManager
            Me.dgTechBulletinResults.DataSource = techManager.searchTechBulletins(strSearch)
            Me.dgTechBulletinResults.DataBind()
        End Sub

        Private Sub populateDataGridByRow(ByVal strSearch As String, ByVal minrow As Long, ByVal maxrow As Long)
            Dim techManager As TechBulletinManager = New TechBulletinManager
            Me.dgTechBulletinResults.DataSource = techManager.searchTechBulletinsByRows(strSearch, minrow, maxrow)
            Me.dgTechBulletinResults.DataBind()
            updatePageDisplay()
        End Sub


        '-- file info -------------------------------------------------------------------------------------
#Region "File Info"
        Private Function getFileSize(ByVal path As String) As String
            Dim fileSize As String = ""

            Try
                If File.Exists(path) Then
                    Dim thisFile As New FileInfo(path.ToString())

                    If thisFile.Length() >= MegaBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / MegaBytes))).ToString() + " MB"
                    ElseIf thisFile.Length() > KiloBytes Then
                        fileSize = (String.Format("{0:N}", (thisFile.Length / KiloBytes))).ToString() + " KB"
                    Else
                        fileSize = thisFile.Length.ToString() + " Bytes"
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                fileSize = "N/A"
            End Try

            Return fileSize
        End Function


        Private Sub setFileDownloadPaths()
            Try
                'Dim config_settings As Hashtable
                'Dim handler As New Sony.US.SIAMUtilities.ConfigHandler("SOFTWARE\SERVICESPLUS")
                'config_settings = handler.GetConfigSettings("//item[@name='Download']")
                'downloadURL = config_settings.Item("url").ToString()
                'downloadPath = config_settings.Item("physical").ToString()
                'conTechBulletinPath = config_settings.Item("techbulletin").ToString()
                downloadURL = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url
                downloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                conTechBulletinPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.TechBulletin
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Function buildReleaseNotesData(ByRef fileName As String, ByRef description As String) As String
            Dim returnValue As String = String.Empty
            If File.Exists(downloadPath + "/" + conTechBulletinPath + "/" + fileName) Then
                returnValue = "<a href=""" + downloadURL + "/" + conTechBulletinPath + "/" + fileName + """ target=""_blank"" id=""bodyClass"">"
                If Not description Is Nothing Then
                    returnValue = returnValue + description + "</a>"
                Else
                    returnValue = returnValue + "<img name=""releaseNotes"" src=""images/sp_int_releaseNotes_btn.gif"" border=""0"" height=""13"" width=""18"" alt=""Release Notes""/></a>"
                End If
            Else
                ' ref to file unavailable online page

            End If
            Return returnValue
        End Function

#End Region







        '-- populate dropdown list ------------------------------------------------------------------------
#Region "populate dropdown list"
        Private Sub populateProductLineDropDown()
            Try
                Dim techMgr As TechBulletinManager = New TechBulletinManager
                Dim arrayProducts() As TechBulletin.ProductLines
                Dim productItem As TechBulletin.ProductLines
                arrayProducts = techMgr.getProductLineItems()


                For Each productItem In arrayProducts
                    Dim prodlist As ListItem = New ListItem
                    prodlist.Text = productItem.productLineDesc
                    prodlist.Value = productItem.productLineNo
                    ddlProductLine.Items.Add(prodlist)
                Next

                Dim list0 As ListItem = New ListItem
                list0.Value = "-1"
                list0.Text = "All Product Lines"
                ddlProductLine.Items.Insert(0, list0)
                ddlProductLine.CssClass = "tableData"
                ddlProductLine.SelectedValue = "-1"
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

#End Region



        '-- helper functions 
#Region "Helpers"

        Private Sub formFieldInitialization()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
            errorMessageLabel.Text = ""
        End Sub


#End Region

#End Region

        Private Sub ddlProductLine_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlProductLine.SelectedIndexChanged
            If ddlProductLine.SelectedValue.ToString() <> "-1" Then
                Me.txtBulletinNo.Text = ddlProductLine.SelectedValue.ToString()
            Else
                Me.txtBulletinNo.Text = ""
            End If
        End Sub

        Private Sub dgTechBulletinResults_ItemDataBound(ByVal sender As System.Object, ByVal e As DataGridItemEventArgs) Handles dgTechBulletinResults.ItemDataBound
            ' The config file should hold the directory path
            Dim pdfPath As String = ""

            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim rec As TechBulletin = CType(e.Item.DataItem, TechBulletin)
                    If rec IsNot Nothing Then
                        Dim lblValue As Label = CType(e.Item.FindControl("lblTBulletinNo"), Label)
                        Dim modelArray() As String
                        Dim modeldata As String = ""
                        'Dim modelcnt As Int64

                        If Not String.IsNullOrEmpty(rec.BulletinNo) Then
                            lblValue.Text = rec.BulletinNo
                        Else
                            lblValue.Text = ""
                        End If
                        lblValue = Nothing
                        lblValue = CType(e.Item.FindControl("lblModelList"), Label)

                        If Not rec.Model Is Nothing Then
                            '- sort the model array before display
                            modelArray = rec.Model.Split(", ")
                            Array.Sort(modelArray)
                            'For modelcnt = 0 To modelArray.Length() - 1
                            '    If modelcnt <> modelArray.Length() - 1 Then
                            '        modeldata += modelArray(modelcnt) + ", "
                            '    Else
                            '        modeldata += modelArray(modelcnt)
                            '    End If
                            'Next
                            modeldata = String.Join(", ", modelArray)
                            lblValue.Text = modeldata 'rec.Model
                        Else
                            lblValue.Text = ""
                        End If

                        lblValue = Nothing
                        lblValue = CType(e.Item.FindControl("lblPrint"), Label)

                        If rec.PrintDate IsNot Nothing Then
                            Dim dispdate As Date = rec.PrintDate
                            lblValue.Text = dispdate.ToShortDateString()
                        Else
                            lblValue.Text = ""
                        End If

                        lblValue = Nothing
                        Dim lnkValue As HyperLink = CType(e.Item.FindControl("lnkBulletin"), HyperLink)
                        Dim lnkImg As HyperLink = CType(e.Item.FindControl("lnkImgRelease"), HyperLink)
                        lnkValue.Style.Add("cursor", "hand")
                        lnkImg.Style.Add("cursor", "hand")
                        lnkValue.Style.Add("text-decoration", "underline")

                        Dim filename As String = ""
                        Dim description As String = ""
                        'Dim filePath As Path
                        If Not rec.Subject Is Nothing And Not rec.BulletinNo.ToString() Is Nothing Then
                            description = rec.Subject.ToString().Trim()
                            filename = rec.BulletinNo.ToString() + ".pdf"
                            lnkValue.Text = description
                            If File.Exists(downloadPath + "/" + conTechBulletinPath + "/" + filename) Then
                                lnkValue.NavigateUrl = downloadURL + "/" + conTechBulletinPath + "/" + filename
                                lnkImg.NavigateUrl = downloadURL + "/" + conTechBulletinPath + "/" + filename
                            Else
                                'lnkValue.Attributes.Add("onclick", "javascript:launchWin2('fileUnavailableOnline.aspx','410','400');")
                                'lnkImg.Attributes.Add("onclick", "javascript:launchWin2('fileUnavailableOnline.aspx','410','400');")
                                lnkValue.Attributes.Add("onclick", "mywin=window.open('fileUnavailableOnline.aspx','','" + CON_POPUP_FEATURES + "');")
                                lnkImg.Attributes.Add("onclick", "mywin=window.open('fileUnavailableOnline.aspx','','" + CON_POPUP_FEATURES + "');")
                            End If
                        Else
                            ' record not having the subject itself
                            lnkValue.Visible = False
                            lnkImg.Visible = False
                        End If
                        lnkValue = Nothing
                    End If
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Private Sub OpenDownloadFile(ByVal fname As String)
        '    Dim path As Path
        '    Dim fullpath = path.GetFullPath(fname)
        '    Dim name = path.GetFileName(fullpath)
        '    Dim ext = path.GetExtension(fullpath)
        '    Dim type As String = ""

        '    If Not IsDBNull(ext) Then
        '        ext = LCase(ext)
        '    End If

        '    Select Case ext
        '        Case ".pdf"
        '            type = "Application/pdf"
        '        Case ".htm", ".html"
        '            type = "text/HTML"
        '        Case ".txt"
        '            type = "text/plain"
        '        Case ".doc", ".rtf"
        '            type = "Application/msword"
        '        Case ".csv", ".xls"
        '            type = "Application/x-msexcel"
        '        Case Else
        '            type = "text/plain"
        '    End Select

        '    Response.AppendHeader("content-disposition", "attachment; filename=" + name)

        '    If type <> "" Then
        '        Response.ContentType = type
        '    End If

        '    Response.WriteFile(fullpath)
        '    Response.End()

        'End Sub

        Private Sub hideNavigations()
            Me.lblCurrentPage.Visible = False
            Me.lblEndingPage.Visible = False
            Me.lblCurrentPage2.Visible = False
            Me.lblEndingPage2.Visible = False
            Me.btnGoToPage.Visible = False
            Me.btnGoToPage2.Visible = False
            Me.btnNextPage.Visible = False
            Me.btnNextPage2.Visible = False
            Me.btnPreviousPage.Visible = False
            Me.btnPreviousPage2.Visible = False
            Me.txtPageNumber.Visible = False
            Me.txtPageNumber2.Visible = False
            Me.PreviousLinkButton.Visible = False
            Me.PreviousLinkButton2.Visible = False
            Me.NextLinkButton.Visible = False
            Me.NextLinkButton2.Visible = False
            Me.lblof1.Visible = False
            Me.lblof2.Visible = False
            Me.lblPage1.Visible = False
            Me.lblPage2.Visible = False

            errorMessageLabel.Text = "No matching technical bulletin found. Please revise your search criteria."
            dgTechBulletinResults.Visible = False
        End Sub

        Private Sub showNavigations()
            Me.lblCurrentPage.Visible = True
            Me.lblEndingPage.Visible = True
            Me.lblCurrentPage2.Visible = True
            Me.lblEndingPage2.Visible = True
            Me.btnGoToPage.Visible = True
            Me.btnGoToPage2.Visible = True
            Me.btnNextPage.Visible = True
            Me.btnNextPage2.Visible = True
            Me.btnPreviousPage.Visible = True
            Me.btnPreviousPage2.Visible = True
            Me.txtPageNumber.Visible = True
            Me.txtPageNumber2.Visible = True
            Me.PreviousLinkButton.Visible = True
            Me.PreviousLinkButton2.Visible = True
            Me.NextLinkButton.Visible = True
            Me.NextLinkButton2.Visible = True
            Me.lblof1.Visible = True
            Me.lblof2.Visible = True
            Me.lblPage1.Visible = True
            Me.lblPage2.Visible = True
            errorMessageLabel.Text = ""
            dgTechBulletinResults.Visible = True
        End Sub


        Private Sub btnLast30days_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLast30days.Click
            '-- fetch the records whose print date falls under last 30 days
            errorMessageLabel.Text = ""
            Try
                ''ddate = CType(Page.LoadControl("WebCalControl.ascx"), WebCalControl)
                'Me.ddate.DefaultDate = DateTime.Today().AddMonths(-1)
                'Me.ddate.LowerLimit = DateTime.Today().AddMonths(-1)
                'Me.ddate.UpperLimit = DateTime.Today()
                ''phCalendar1.Controls.Add(ddate)
                ''ddate1 = CType(Page.LoadControl("WebCalControl.ascx"), WebCalControl)
                'Me.ddate1.DefaultDate = DateTime.Today()
                'Me.ddate1.LowerLimit = ddate.getSelectedDate()
                'Me.ddate1.UpperLimit = DateTime.Today()
                ''phCalendar2.Controls.Add(ddate1)

                'Me.ddate.setDate(DateTime.Today().AddMonths(-1))
                'Me.ddate1.setDate(DateTime.Today())

                'searchCondition = " to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') -  to_date('" + ddate.DefaultDate.Month.ToString() + "/" + ddate.DefaultDate.Day.ToString() + "/" + ddate.DefaultDate.Year.ToString() + "', 'mm/dd/yyyy') > 0"
                'searchCondition += " and to_date('" + ddate1.DefaultDate.Month.ToString() + "/" + ddate1.DefaultDate.Day.ToString() + "/" + ddate1.DefaultDate.Year.ToString() + "', 'mm/dd/yyyy') -  to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') > 0"
                '--------------------------------------------------

                Dim dateTo As Date = DateAndTime.Today()
                Dim dateFrom As Date = DateTime.Today().AddMonths(-1)

                txtDateFrom.Text = dateFrom.ToShortDateString()
                txtDateTo.Text = dateTo.ToShortDateString()
                searchCondition = "" ' need to clean up the previous value
                'searchCondition = " to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') -  to_date('" + dateFrom + "', 'mm/dd/yyyy') > 0"
                'searchCondition += " and to_date('" + dateTo + "', 'mm/dd/yyyy') -  to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') > 0"
                searchCondition = " printdate -  to_date('" + dateFrom + "', 'mm/dd/yyyy') >= 0"
                searchCondition += " and to_date('" + dateTo + "', 'mm/dd/yyyy') -  printdate >= 0"
                totalSearchResults = GetRecordcount(searchCondition, "false")
                lowerLimit = 0
                upperLimit = conMaxResultsPerPage
                currentResultPage = 1

                If totalSearchResults > conMaxResultsPerPage Then
                    endingResultPage = Convert.ToInt64(totalSearchResults / conMaxResultsPerPage)
                Else
                    endingResultPage = 1
                    'when the total search count is less than 50, then search only until that count
                    upperLimit = totalSearchResults
                End If

                If totalSearchResults <> 0 Then
                    showNavigations()
                    populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                Else
                    hideNavigations()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                dgTechBulletinResults.Visible = False
            End Try
        End Sub

        Private Function validateDates() As Boolean
            Dim date1 As Date
            Dim date2 As Date
            Try
                date1 = Convert.ToDateTime(txtDateFrom.Text)
                date2 = Convert.ToDateTime(txtDateTo.Text)
                If DateTime.Compare(date1, date2) > 0 Then
                    '-- from date cannot be > to date
                    Return False
                End If
                If DateTime.Compare(date2, DateTime.Today()) > 0 Then
                    '-- to date cannot be greater than today
                    Return False
                End If
                If DateTime.Compare(date1, Convert.ToDateTime("01/01/1967")) < 0 Then
                    '-- from date cannot be lesser than 01/01/1967
                    Return False
                End If
                '-- dates are valid
                Return True
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try
        End Function

        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Try
                searchCondition = "" ' need to clean up the previous value
                Dim IsSearchBySerialNo As Boolean = False

                errorMessageLabel.Text = ""
                If txtDateFrom.Text.Trim() <> "" And txtDateTo.Text.Trim() <> "" Then
                    If Not validateDates() Then
                        errorMessageLabel.Text = "Please select valid date from and date to"
                        Return
                    End If
                End If

                If Me.txtModelNo.Text.Trim() <> "" Then
                    Me.txtModelNo.Text = txtModelNo.Text.ToString().Trim().Replace("-", "")
                    searchCondition += " lower(Model) like '%" + Me.txtModelNo.Text.Trim().ToLower() + "%'"
                End If

                If Me.txtSubject.Text.Trim() <> "" Then
                    If Not searchCondition = "" Then
                        searchCondition += " And "
                    End If
                    searchCondition += " lower(Subject) like '%" + Me.txtSubject.Text.Trim().ToLower() + "%'"
                End If
                'If Me.ddate.getSelectedDate() <> "" Then
                '    If Not searchCondition = "" Then
                '        searchCondition += " And "
                '    End If
                '    searchCondition += " to_date(printdate,'mm/dd/yyyy') > to_date('" + ddate.DefaultDate.Month.ToString() + "/" + ddate.DefaultDate.Day.ToString() + "/" + ddate.DefaultDate.Year.ToString() + "','mm/dd/yyyy') "
                'End If
                'If Me.ddate1.getSelectedDate() <> "" Then
                '    If Not searchCondition = "" Then
                '        searchCondition += " And "
                '    End If
                '    searchCondition += " to_date(printdate,'mm/dd/yyyy') < to_date('" + ddate1.DefaultDate.Month.ToString() + "/" + ddate1.DefaultDate.Day.ToString() + "/" + ddate1.DefaultDate.Year.ToString() + "','mm/dd/yyyy')"
                'End If

                Try
                    If Me.txtDateFrom.Text <> "" Then
                        If Not searchCondition = "" Then
                            searchCondition += " And "
                        End If
                        'searchCondition += " to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') -  to_date('" + txtDateFrom.Text + "', 'mm/dd/yyyy') > 0"
                        searchCondition += " printdate  -  to_date('" + txtDateFrom.Text + "', 'mm/dd/yyyy') >= 0"
                    End If

                    If Me.txtDateTo.Text <> "" Then
                        If Not searchCondition = "" Then
                            searchCondition += " And "
                        End If
                        'searchCondition += "  to_date('" + txtDateTo.Text + "', 'mm/dd/yyyy') - to_date(to_char(substr(printdate,1,instr(printdate||'/','/',1,3)-1)),'mm/dd/yyyy') > 0"
                        searchCondition += " to_date('" + txtDateTo.Text + "', 'mm/dd/yyyy') -   printdate >= 0"
                    End If

                    If txtBulletinNo.Text.Trim() <> "" Then
                        txtBulletinNo.Text = Convert.ToInt64(txtBulletinNo.Text.Trim()).ToString()
                        If Not searchCondition = "" Then
                            searchCondition += " And "
                        End If
                        searchCondition += " to_char(bulletin_number) like '" + txtBulletinNo.Text.Trim() + "%'"
                    End If

                    If txtSerialNo.Text.Trim() <> "" Then
                        txtSerialNo.Text = Convert.ToInt64(txtSerialNo.Text.Trim()).ToString()
                        If Not searchCondition = "" Then
                            searchCondition += " And "
                        End If
                        'convert to integer to compare with the data, but string concatenation needs the tostring at the end
                        'searchCondition += " startofrange >= " + Convert.ToInt64(txtSerialNo.Text.Trim()).ToString() + " and endofrange >= " + Convert.ToInt64(txtSerialNo.Text.Trim()).ToString()
                        searchCondition += Convert.ToInt64(txtSerialNo.Text.Trim()).ToString() + " between startofrange  and endofrange "
                        IsSearchBySerialNo = True
                    End If
                Catch ex As Exception
                    errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try

                If IsSearchBySerialNo = True Then
                    totalSearchResults = GetRecordcount(searchCondition, "true")
                Else
                    totalSearchResults = GetRecordcount(searchCondition, "false")
                End If

                lowerLimit = 0
                upperLimit = conMaxResultsPerPage
                currentResultPage = 1
                If totalSearchResults > conMaxResultsPerPage Then
                    endingResultPage = Convert.ToInt64(totalSearchResults / conMaxResultsPerPage)
                Else
                    endingResultPage = 1
                    'when the total search count is less than 50, then search only until that count
                    upperLimit = totalSearchResults
                End If
                If totalSearchResults <> 0 Then
                    showNavigations()
                    populateDataGridByRow(searchCondition, lowerLimit, upperLimit)
                Else
                    hideNavigations()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                dgTechBulletinResults.Visible = False
            End Try

        End Sub

        Private Sub btnGoToPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGoToPage.Click
            fetchSpecifiedResultSet(1)
        End Sub

        Private Sub btnGoToPage2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGoToPage2.Click
            fetchSpecifiedResultSet(2)
        End Sub

        Private Sub NextLinkButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextLinkButton.Click
            Try
                fetchNextResultSet()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub NextLinkButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NextLinkButton2.Click
            Try
                fetchNextResultSet()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PreviousLinkButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousLinkButton.Click
            fetchPreviousResultSet()
        End Sub

        Private Sub PreviousLinkButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviousLinkButton2.Click
            fetchPreviousResultSet()
        End Sub
    End Class

End Namespace
