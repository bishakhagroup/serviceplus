Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class sony_technical_bulletin
        Inherits SSL
        Public pagetitle As String = String.Empty
        Public filename As String = String.Empty
        Private recTechBulletin As TechBulletin
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            isProtectedPage(False)
            InitializeComponent()
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Session("TBObject") Is Nothing Then
                If Session("customer") Is Nothing Then
                    Response.Redirect("sony-knowledge-base-search.aspx")
                End If
                recTechBulletin = CType(Session("TBObject"), TechBulletin)
                pagetitle = recTechBulletin.Subject
                ViewState.Add("Subject", pagetitle)
            Else
                Redirect()
            End If
            If IsPostBack Then
                filename = ViewState("FileName").ToString()
                'FormsAuthentication.SetAuthCookie(CType(Session("customer"), Customer).UserName, False)
                'Dim ticket1 As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, CType(Session("customer"), Customer).UserName, DateTime.Now, DateTime.Now.AddMinutes(10), False, "TBUSER")
                'Dim cookie1 As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket1))
                'Response.Cookies.Add(cookie1)
            Else
                'FormsAuthentication.SetAuthCookie(CType(Session("customer"), Customer).UserName, False)
                'Dim ticket1 As FormsAuthenticationTicket = New FormsAuthenticationTicket(1, CType(Session("customer"), Customer).UserName, DateTime.Now, DateTime.Now.AddMinutes(10), False, "TBUSER")
                'Dim cookie1 As HttpCookie = New HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket1))
                'Response.Cookies.Add(cookie1)
                If Session("TBFile") Is Nothing Then
                    Redirect()
                Else
                    filename = Session("TBFile").ToString()
                    If Not Request.QueryString("fn") Is Nothing Then
                        If filename.IndexOf(Request.QueryString("fn")) < 0 Then
                            Redirect()
                        ElseIf Not Session("IsValidSubscription") Is Nothing Then
                            If Convert.ToBoolean(Session("IsValidSubscription")) = False Then
                                Response.Redirect("Sony-Technical-Bulletins-GenInfo.aspx")
                            Else
                                ViewState.Add("BulletinNo", Request.QueryString("fn"))

                                ''filename = "./" + Encryption.Encrypt(Session.SessionID.ToString(), Encryption.PWDKey) + "/Bulletin_" + Request.QueryString("fn") + ".pdf"
                                filename = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url + "/" + Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.TechBulletin + "/" + Request.QueryString("fn") + ".pdf"
                                ViewState.Add("FileName", filename)
                            End If
                        End If

                    End If
                End If
            End If
            If Not IsPostBack Then
                SaveKBView()
            End If
        End Sub
        Private Sub Redirect()
            If Not Session("customer") Is Nothing Then
                Response.Redirect("au-sony-knowledge-base-search.aspx")
            Else
                Response.Redirect("sony-knowledge-base-search.aspx")
            End If
        End Sub
#End Region


        Private Sub SaveKBView()
            Try
                LogTransaction(0, -1, "")
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try

        End Sub

        Private Sub LogTransaction(ByVal TransactionType As Int16, ByVal FeedBackAnswer As Int16, ByVal Suggestion As String)
            Dim xCUSTOMERID As Integer = "-1"
            Dim xCUSTOMERSEQUENCENUMBER As Integer = "-1"
            If Not Session.Item("customer") Is Nothing Then
                xCUSTOMERID = Convert.ToInt32(CType(Session.Item("customer"), Customer).CustomerID)
                xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(CType(Session.Item("customer"), Customer).SequenceNumber)
            End If
			Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
			Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
			Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
			Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")

			If xHTTP_URL = "" Then
				xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
			End If

			Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

            Try
                Dim intFeedBackID As Int64 = 0
                If Not ViewState("FeedBackID") Is Nothing Then
                    intFeedBackID = Convert.ToInt64(ViewState("FeedBackID").ToString())
                End If
                recTechBulletin.Type = 2
                recTechBulletin.BulletinNo = ViewState("BulletinNo").ToString()
                recTechBulletin.Subject = ViewState("Subject").ToString()
                If Not recTechBulletin Is Nothing Then
                    Dim tbManager As TechBulletinManager = New TechBulletinManager()
                    intFeedBackID = (tbManager.SaveFeedBack(recTechBulletin, FeedBackAnswer, intFeedBackID, TransactionType, Suggestion, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT))
                    ViewState.Add("FeedBackID", intFeedBackID)
                End If
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try

        End Sub
        Protected Sub imgYes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgYes.Click
            Try
                LogTransaction(1, 1, "")
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Protected Sub ImgNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgNo.Click
            Try
                LogTransaction(1, 0, "")
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Private Sub FeedBackResponse()
            feedbackTr.Visible = False
            Dim lblFBresponse As New Label()
            lblFBresponse.CssClass = "TableHeader"
            '------------------------------------------------------
            lblFBresponse.Text = "Thank you for your feedback. Do you have any suggestions for improving the above information?"
            btnSubmit.Visible = True
            txtSuggestion.Visible = True
            txtSuggestion.Focus()
            '------------------------------------------------------
            feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub

        Private Sub SuggestionResponse()
            feedbackTr.Visible = False
            Dim lblFBresponse As New Label()
            lblFBresponse.CssClass = "TableHeader"
            '------------------------------------------------------
            lblFBresponse.Text = "Thank you for your suggestions."
            btnSubmit.Visible = False
            txtSuggestion.Visible = False
            '------------------------------------------------------
            feedbackPlaceHolder.Controls.Clear()
            feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub

        'Protected Sub imgYes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgYes.Click
        '    Try
        '        recTechBulletin.Type = 2
        '        recTechBulletin.BulletinNo = ViewState("BulletinNo").ToString()
        '        recTechBulletin.Subject = ViewState("Subject").ToString()
        '        If Not recTechBulletin Is Nothing Then
        '            Dim tbManager As TechBulletinManager = New TechBulletinManager()
        '            'tbManager.SaveFeedBack(recTechBulletin, 1)
        '        End If
        '    Catch ex As Exception
        '        Response.Redirect("Member.aspx")
        '    End Try
        '    FeedBackResponse()
        'End Sub

        'Protected Sub ImgNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgNo.Click
        '    Try
        '        recTechBulletin.Type = 2
        '        recTechBulletin.BulletinNo = ViewState("BulletinNo").ToString()
        '        recTechBulletin.Subject = ViewState("Subject").ToString()
        '        If Not recTechBulletin Is Nothing Then
        '            Dim tbManager As TechBulletinManager = New TechBulletinManager()
        '            'tbManager.SaveFeedBack(recTechBulletin, 0)
        '        End If
        '    Catch ex As Exception
        '        Response.Redirect("Member.aspx")
        '    End Try
        '    FeedBackResponse()
        'End Sub
        'Private Sub FeedBackResponse()
        '    feedbackTr.Visible = False
        '    Dim lblFBresponse As New Label()
        '    lblFBresponse.CssClass = "TableHeader"
        '    lblFBresponse.Text = "Thank you for your feedback."
        '    feedbackPlaceHolder.Controls.Add(lblFBresponse)
        'End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            ImgNo.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
            imgYes.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
        End Sub
        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmit.Click
            Try
                If txtSuggestion.Text <> String.Empty Then
                    LogTransaction(2, 0, txtSuggestion.Text)
                    SuggestionResponse()
                Else
                    Dim lblFBresponse As New Label()
                    lblFBresponse.CssClass = "redAsterick"
                    '------------------------------------------------------
                    lblFBresponse.Text = "Please enter your suggestions to submit."
                    btnSubmit.Visible = True
                    txtSuggestion.Visible = True
                    '------------------------------------------------------
                    feedbackPlaceHolder.Controls.Clear()
                    feedbackPlaceHolder.Controls.Add(lblFBresponse)
                End If
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try
        End Sub
    End Class

End Namespace
