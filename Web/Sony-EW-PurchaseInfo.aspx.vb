Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
'6994 starts
Imports System.Web.Script.Services
Imports System.Web.Services
Imports Sony.US.AuditLog
'6994 ends

Namespace ServicePLUSWebApp
    Partial Class Sony_EW_PurchaseInfo
        Inherits SSL
        Public cm As New CustomerManager
        Public customer As Customer
        Public ProductCode As String
        Public EWProductCode As String
        Public EWModelNumber As String
        Dim ew As ExtendedWarrantyManager
        Dim ewmodels As DataSet
        'Dim objUtilties As Utilities = New Utilities



#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ModelDescription As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    If Not Request.QueryString("ewc") Is Nothing And Not Request.QueryString("pc") Is Nothing And Not Request.QueryString("mdl") Is Nothing Then
                        ProductCode = Request.QueryString("pc")
                        EWProductCode = Request.QueryString("ewc")
                        EWModelNumber = Request.QueryString("mdl")
                        lblHeader.Text = "Sony Extended Warranty for " + EWModelNumber
                        If ProductCode.Trim() = String.Empty Or EWProductCode.Trim() = String.Empty Then
                            'Modified by prasad for 2523
                            Response.Redirect("sony-extended-warranties.aspx")
                        Else
                            If Session.Item("customer") Is Nothing Then
                                'Response.Redirect("SignIn.aspx?post=ewprod&ewc=" + Request.QueryString("ewc").ToString() + "&pc=" + Request.QueryString("pc").ToString() + "&mdl=" + Request.QueryString("mdl"))
                                Response.Redirect("SignIn-Register.aspx?post=ewprod&ewc=" + Request.QueryString("ewc").ToString() + "&pc=" + Request.QueryString("pc").ToString() + "&mdl=" + Request.QueryString("mdl"))
                                Response.End()
                            Else
                                ViewState.Add("ewcode", Request.QueryString("ewc"))
                                ViewState.Add("pcode", Request.QueryString("pc"))
                                ViewState.Add("mdl", Request.QueryString("mdl"))
                            End If
                        End If
                    Else
                        'Modified by prasad for 2523
                        Response.Redirect("sony-extended-warranties.aspx", True)
                        Response.End()
                    End If
                End If

                formFieldInitialization()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub formFieldInitialization()
            txtSerialNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'SearchModel')"
            txtDate.Attributes("onkeydown") = "SetTheFocusButton(event, 'SearchModel')"
        End Sub
        Private Function IsValidate() As Boolean
            ErrorValidation.Text = String.Empty
            CartMessage.Text = String.Empty

            ErrorValidation.Visible = False

            If ViewState.Item("mdl") Is Nothing Then
                Response.Redirect("sony-extended-warranties.aspx", True)
                Return False
            Else
                If (txtDate.Text.Trim() = String.Empty) Then
                    ErrorValidation.Text = "Please enter purchase date."
                    ErrorValidation.Visible = True
                    Return False
                Else
                    If IsDate(txtDate.Text.ToString()) Then
                        Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                        If Not System.Text.RegularExpressions.Regex.IsMatch(txtDate.Text.ToString(), thisPattern) Then
                            ErrorValidation.Text = "Please enter date in mm/dd/yyyy format."
                            ErrorValidation.Visible = True
                            Return False
                        Else
                            If Convert.ToDateTime(txtDate.Text.ToString()) >= Now.Date Then
                                ErrorValidation.Text = "Please enter date before or today."
                                ErrorValidation.Visible = True
                                Return False
                            End If
                        End If
                    Else
                        ErrorValidation.Text = "Please enter valid date."
                        ErrorValidation.Visible = True
                        Return False
                    End If
                End If


                If (txtSerialNumber.Text.Trim() = String.Empty) Then
                    ErrorValidation.Text = "Please enter Serial Number."
                    ErrorValidation.Visible = True
                    Return False
                End If
                Return True
            End If
        End Function

        Private Sub AddEWModelToCart()
            Try
                CartMessage.Text = String.Empty
                spnTop.Visible = False
                spnbottom.Visible = False
                ErrorValidation.Text = String.Empty
                Dim objExtWarranty As ExtendedWarrantyModel
                ProductCode = ViewState.Item("pcode").ToString()
                EWProductCode = ViewState.Item("ewcode").ToString()
                'Modified by prasad for 2523
                If ProductCode.Trim() = String.Empty Or EWProductCode.Trim() = String.Empty Then Response.Redirect("sony-extended-warranties.aspx")
                objExtWarranty = GETExtendedWarrantyForCART(EWProductCode, ProductCode)
                If Not objExtWarranty Is Nothing Then
                    If objExtWarranty.Type = Product.ProductType.ExtendedWarranty Then
                        objExtWarranty.Download = True
                        objExtWarranty.Ship = False
                        objExtWarranty.EWPurchaseDate = Convert.ToDateTime(txtDate.Text.ToString())
                        objExtWarranty.EWModelNumber = ViewState.Item("mdl").ToString()
                        objExtWarranty.EWSerialNumber = txtSerialNumber.Text.Trim().ToUpper()
                    End If
                    Session.Add("sesssionobjexwarranty", objExtWarranty)
                    If Not Session("Customer") Is Nothing Then
                        'AddToCart(CType(Session("Customer"), Customer), objExtWarranty, IIf(objExtWarranty.Download, ShoppingCartItem.DeliveryMethod.Download, ShoppingCartItem.DeliveryMethod.Ship), ErrorValidation) '6994
                        RegisterStartupScript("AddToCart_onclick", "<script language='javascript'>AddToCart_onclick('" + EWProductCode.ToString() + "');</script>") '6994
                        txtDate.Text = String.Empty
                        txtSerialNumber.Text = String.Empty
                        CartMessage.Text = "To purchase another Extended Warranty for the same product model, enter the information below and click Add to Cart."
                        spnTop.Visible = True
                        spnbottom.Visible = True
                        lblAnotherPurchaseMessage.Text = "To purchase another Extended Warranty for a different product, <a href='sony-extended-warranties.aspx'>click here</a> to return to the Extended Warranty search page.</br><img height=""2"" src=""images/spacer.gif"" width=""20"">"
                    End If
                Else
                    ErrorValidation.Text = "Error adding Extended Warranty item to cart. This Extended Warranty is not available. Please search again for your model. <a href='sony-extended-warranties.aspx'>Click here</a> to go to search page."
                    ErrorValidation.Visible = True
                End If

            Catch ex As Exception
                ErrorValidation.Text = ("Error adding Extended Warranty item to cart. Please call " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail)
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
        End Sub
        Private Function GETExtendedWarrantyForCART(ByVal ChildProductCode As String, ByVal ParentProductCode As String) As ExtendedWarrantyModel
            Dim objEW As ExtendedWarrantyModel = Nothing
            Try
                Dim objExtendedWarrantyManager As New ExtendedWarrantyManager
                objEW = objExtendedWarrantyManager.GETEWFORCART(ChildProductCode, ParentProductCode)

            Catch ex As Exception
                'Modified by Prasad for 2517
                'ErrorValidation.Text = "Search failed. Unable to display data at this time."
                'ErrorValidation.Text = ("Error adding Extended Warranty item to cart. Please call " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail)
                ErrorValidation.Text = Utilities.WrapExceptionforUI(ex)
                ErrorValidation.Visible = True
            End Try
            Return objEW
        End Function
        Protected Sub SearchModel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SearchModel.Click
            Try
                If IsValidate() Then
                    AddEWModelToCart()
                Else
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        'End Class'6994
        '6994 starts
        <System.Web.Services.WebMethod(EnableSession:=True)> _
            Public Shared Function addPartToCartwSPurchase(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultPurchase '6994
            'Public Shared Function addPartToCartwSPurchase(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultPurchase '6994

            Dim objPCILogger As New PCILogger() '6524
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim objResult As New PartresultPurchase '6994
            objResult.partnumber = sPartNumberUpperCase
            'Dim objUtilties As Utilities = New Utilities
            objResult.Quantity = 1 '6994
            Dim message As String

            Dim customer As New Customer
            If Not HttpContext.Current.Session.Item("customer") Is Nothing Then
                customer = HttpContext.Current.Session.Item("customer")
            End If
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim carts As ShoppingCart() = objSSL.getAllShoppingCarts(customer, lmessage)
            ''6994 starts
            'Dim objextwarmodel As ExtendedWarrantyModel = HttpContext.Current.Session.Item("sesssionobjexwarranty")
            '6994 ends
            message = lmessage.Text
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing
            objResult.success = False

            Try
                '6524 V11 Starts
                '6524 start
                'objPCILogger.EventOriginApplication = "ServicesPLUS"
                ''objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                'objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                'objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                'objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                'objPCILogger.IndicationSuccessFailure = "Success"
                'objPCILogger.CustomerID = customer.CustomerID
                'objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                ''objPCILogger.UserName = customer.UserName
                'objPCILogger.EmailAddress = customer.EmailAddress '6524
                'objPCILogger.SIAM_ID = customer.SIAMIdentity
                'objPCILogger.LDAP_ID = customer.LdapID '6524
                'objPCILogger.EventOriginMethod = "addPartToCart"
                'objPCILogger.Message = "addPartToCart Success."
                'objPCILogger.EventType = EventType.Add
                '6524 end
                '6524 V11 ends
                ''Output1 value return form SAP helpler
                Dim output1 As String = String.Empty
                'cart = sm.AddProductToCartQuickCartExtendedWarranty(carts, sPartNumberUpperCase, sPartQuantity, True, output1, objextwarmodel) '6994
                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, sPartQuantity, False, output1) '6994
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If
                For Each i As ShoppingCartItem In cart.ShoppingCartItems

                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = "Replacement item " + i.Product.PartNumber + " added to cart in place of requested item  " + sPartNumberUpperCase + " which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1

                        message = "Item " + sPartNumberUpperCase + " added to cart."
                        objResult.message = message
                    End If
                Next


            Catch exArgumentalExceptio As ArgumentException
                If exArgumentalExceptio.ParamName = "SomePartNotFound" Then

                    message = exArgumentalExceptio.Message.Replace("Parameter name: SomePartNotFound", "")

                ElseIf exArgumentalExceptio.ParamName = "PartNotFound" Then

                    message = exArgumentalExceptio.Message.Replace("Parameter name: PartNotFound", "")

                ElseIf exArgumentalExceptio.ParamName = "BillingOnlyItem" Then

                    message = exArgumentalExceptio.Message.Replace("Parameter name: BillingOnlyItem", "")

                End If
            Catch ex As Exception
                '6524 V11 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "addPartToCartwSPurchase"
                objPCILogger.EventType = EventType.View
                '6524 V11 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString() '6524
                objPCILogger.PushLogToMSMQ() '6524'6524 V11
                '7813 starts
                'message = Utilities.WrapExceptionforUI(ex)'7813
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
                '7813 ends
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            objResult.message = message
            Return objResult
        End Function
    End Class
    Public Class PartresultPurchase '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property
        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property
        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property
        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property
        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property
        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property

    End Class
    '6994 ends


End Namespace
