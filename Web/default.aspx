<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="bodyContent" Src="bodyContent.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SPSPromotion" Src="~/UserControl/SPSPromotion.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp._default"
    EnableViewStateMac="true" CodeFile="default.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_title_msg %></title>
    <meta http-equiv="refresh" content="600">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Training, Technical Bulletin, Technical Bulletins"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <link href="includes/svcplus_globalization.css" rel="stylesheet" />

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script type="text/javascript">
        var mywin = null;

        function openPopupWindow(url) {
            var winX = (screen.availWidth - 432) / 2;
            var winY = (screen.availHeight - 265) / 2;
            mywin = window.open(url, '', 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resizable=no,width=432,height=265,top=' + winY + ',left=' + winX + ',target=_self');
        }

        function closePopupWindow() {
            if (mywin !== null && mywin.open) {
                mywin.close();
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="tab1" style="width: 710px; height: 123px; border: none;" role="presentation">
                                        <tr>
                                            <td id="td1" align="right" style="width: 710px; height: 123px; background: url('images/Homepage_Banner.gif');" runat="server">
                                                <table role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <h1 class="headerText">
                                                                <%=Resources.Resource.header_wlcserviceplus %><sup class="bodyCopySMWhite"><%=Resources.Resource.header_SM %></sup>
                                                            </h1>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodyCopySMWhite" colspan="3">
                                                            <%=Resources.Resource.header_wlcpfsnlmsg %>
                                                            <%-- "Put our professional product support team to work for you." --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="50">
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="2" style="text-align: left">
                                                            <a href="#" onclick="window.location.href='<%=LoginURL %>'; return false;" class="PromoBoldWhite"
                                                                target="_blank"><%=Resources.Resource.header_mem_login %></a>
                                                            &nbsp;<span style="color: white; font-weight: bold;">|</span>&nbsp;
                                                            <a class="PromoBoldWhite" href="Register-Account-Tax-Exempt.aspx"><%=Resources.Resource.header_reg_svcpls %></a>
                                                            &nbsp;<span style="color: white; font-weight: bold;">|</span>&nbsp;
                                                            <a href="#" onclick="javascript:window.open('<%=ForgotPassword %>');" class="PromoBoldWhite"
                                                                id="refForgotpwd"><%=Resources.Resource.header_fogot_pwd %></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 17px; background: url('images/sp_hdr_btm_bar.gif');">
                                    <img height="17" src="images/spacer.gif" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img height="20" src="images/spacer.gif" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <main>
                                                    <table border="0" role="presentation">
                                                    <tr valign="top">
                                                        <td style="width: 430px">
                                                            <table border="0" role="main">
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-knowledge-base-search.aspx">--%>
                                                                            <img style="width: 70px; height: 50px" height="50" alt="" src="images/sp_int_home_techbulletin.gif"
                                                                                width="70" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-knowledge-base-search.aspx"><%=Resources.Resource.menu_knowldgebase%></a>
                                                                        &nbsp;-<%=Resources.Resource.hplnk_kngbse_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-operation-manual.aspx">--%>
                                                                            <img style="width: 70px; height: 50px" height="50" alt="" src="images/Manuals_Button.jpg"
                                                                                width="70" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-operation-manual.aspx"><%=Resources.Resource.menu_manuals%></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_manuals_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-parts.aspx">--%>
                                                                            <img style="width: 70px; height: 50px" height="50" alt="" src="images/parts_button.jpg"
                                                                                width="70" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-parts.aspx"><%=Resources.Resource.hplnk_parts_acc%></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_parts_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./Sony-repair.aspx">--%>
                                                                            <img style="width: 70px; height: 50px" height="50" alt=""
                                                                                src="images/repair_button.jpg" width="70" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./Sony-repair.aspx"><%=Resources.Resource.hplnk_depot_repair%></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_depot_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-software.aspx">--%>
                                                                            <img src="images/Software_and_Firmware_Button.jpg" style="width: 70px; height: 50px" alt="" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-software.aspx"><%=Resources.Resource.hplnk_software_frmwre%></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_software_frmwre_msg%>
                                                                        <% If isCanada Then%>
                                                                        <br />
                                                                            <% If isFrench Then %>
                                                                            <span class="redAsterick"><strong>NOUVEAU!</strong> - Maintenant disponible au Canada.</span>
                                                                            <% Else%>
                                                                            <span class="redAsterick"><strong>NEW!</strong> - Now available in Canada.</span>
                                                                            <% End If%>
                                                                        <% End If%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-training-catalog.aspx">--%>
                                                                            <img src="images/STI_Button.jpg" style="width: 70px; height: 50px" alt="" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-training-catalog.aspx"><%=Resources.Resource.hplnk_sony_training_inst %></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_sony_training_inst_msg %>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-service-agreements.aspx">--%>
                                                                            <img src="images/sp_int_home_serv-agrmnts.gif" style="width: 70px; height: 50px" alt="" border="0">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-service-agreements.aspx"><%=Resources.Resource.hplnk_svc_agreement %></a>
                                                                        &nbsp; -<%=Resources.Resource.hplnk_svc_agreement_msg%><br />
                                                                        <%If Not isCanada Then%>
                                                                            <div id="hideProductionReg" runat="server">
                                                                                <a class="promoCopyBold" href="./sony-service-agreements.aspx#identifier"><%=Resources.Resource.hplnk_productrstn%></a>
                                                                                &nbsp; -<%=Resources.Resource.hplnk_productrstn_msg%>
                                                                            </div>
                                                                        <%End If%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 70px" height="20">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="17">
                                                            <img height="20" src="images/spacer.gif" width="17" alt="">
                                                        </td>
                                                        <td width="230" height="260">
                                                            <%If Not isCanada Then%>
                                                            <table role="presentation">
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="RHSPromos" Type="1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="SPSPromotion1" Type="4" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="Promotions1" Type="2" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="SPSPromotionBottomRight" Type="3" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%End If%>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </main>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <a class="promoCopyBold" href="sony-service-repair-maintenance.aspx" runat="server"><%=Resources.Resource.hplnk_sonysvc_modellst%></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
        <asp:HiddenField ID="hdnSessionId" runat="server" />
    </form>
</body>
</html>
