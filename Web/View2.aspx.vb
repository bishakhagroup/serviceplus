Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Net
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports System.Web.HttpUtility
Imports ServicesPlusException


Namespace ServicePLUSWebApp



Partial Class View2
    Inherits SSL

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        ''Dim objUtilties As Utilities = New Utilities
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If IsPostBack Then
                    Return
                End If
                If (Session.Item("models") IsNot Nothing) And (Session.Item("partList") IsNot Nothing) Then
                    'Put user code to initialize the page here
                    Dim models = Session.Item("models")
                    Dim part As Sony.US.ServicesPLUS.Core.Part
                    Dim parts() As Sony.US.ServicesPLUS.Core.Part
                    'Dim pdf() As Byte
                    Dim explodedview_link As String
                    Dim pdf_path As String = ""
                    Dim catMan As New CatalogManager
                    Dim whichpart As Int32

                    parts = Session.Item("partList")
                    whichpart = Request.QueryString("ArrayVal")

                    If (parts Is Nothing) OrElse (whichpart > parts.Length) Then
                        ' The Session has probably expired, or they modified their URL.
                        Response.Redirect("SessionExpired.aspx?error=searchexpired&returnto=partsearch", True)
                        Return
                    End If
                    part = parts.GetValue(whichpart)

                    ' Response.Write("Reference ID: " + Request.QueryString.Item("refID"))
                    ' Response.Write("<br> PDF will be displayed here.")

                    'First try to et file path
                    'Dim config_settings As Hashtable
                    'Dim handler As New ConfigHandler("SOFTWARE\SERVICESPLUS")
                    'config_settings = handler.GetConfigSettings("//item[@name='PartsFinder']")
                    'If (Not config_settings Is Nothing) Then
                    '    If (Not config_settings.Item("explodedview_link") Is Nothing) Then
                    'explodedview_link = config_settings.Item("explodedview_link").ToString()
                    explodedview_link = ConfigurationData.GeneralSettings.PartsFinder.Explodedview_Link
                    pdf_path = catMan.GetExplodedViewPath(part)
                    '    End If
                    'End If

                    ' new code for exploded view to come from file location than from Oracle blob
                    ' modified by Deepa Vembu Feb 10,2006
                    Try
                        pdf_path = Replace(pdf_path, "\", "/")
                        pdf_path = UrlDecode(UrlEncode(explodedview_link + pdf_path))
                    Catch ex As Exception
                        Response.Write("path not found")
                        Response.Write(pdf_path)
                        Response.End()
                    End Try

                    Try
                        Response.Redirect(pdf_path)
                        Response.End()
                    Catch ex As Exception
                        Response.Write(ex.Message)
                        Response.End()
                    End Try
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

End Class

End Namespace
