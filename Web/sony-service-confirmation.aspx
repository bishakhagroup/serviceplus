<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-service-confirmation.aspx.vb"
    Inherits="ServicePLUSWebApp.sony_service_confirmation" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_svcacc_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link type="text/css" rel="stylesheet" href="includes/ServicesPLUS_style.css">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        history.forward();
        function checkServiceContract() {
            var radioObj = document.forms['Form1'].elements['group1'];
            var radioLength = radioObj.length;
            var iSelect = 0;

            if (radioLength === undefined) {
                if (radioObj.checked)
                    return radioObj.value;
                else
                    return "";
            }
            for (var i = 0; i < radioLength; i++) {
                if (radioObj[i].checked) {
                    //return group1[i].value;
                    iSelect = radioObj[i].value;
                }
            }

            if (iSelect === 1) {
                if (document.forms['Form1'].txtContractNo.value === "") {
                    document.forms['Form1'].cvServiceContract.value = '<%=Resources.Resource.repair_pg_stus_msg_2()%>';
                    return false;
                }
            }
        }

        function PrintForm() {
            my_window = window.open('Printer-Service-Result.aspx?model=' + '<%=sModelName%>', '', 'toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=1,width=700,height=700');
            if (my_window.opener === null) my_window.opener = self;
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45" height="82"
                                                style="width: 465px">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.repair_mn_tg_msg_1()%></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 5px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 796px; height: 22px;">
                                                &nbsp;<asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
                                            </td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px; height: 24px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2" style="height: 24px;">
                                                <p class="promoCopyBold">
                                                    <%="" %> <%--This empty tag prevents IntelliSense errors in Visual Studio --%>
                                                    <%If isRequestComplete = False Then%>
                                                    <%=Resources.Resource.repair_svc_confirm_info_msg()%>
                                                    <%Else%>
                                                    <%=Resources.Resource.repair_svc_confirm_info_msg_1()%>
                                                    <%End If%>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 5px; height: 107px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 107px;" colspan="2" class="tableHeader">
                                                <table cellpadding="3" border="0" role="presentation"
                                                    style="width: 93%; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666; border: 1px #666666 solid;">
                                                    <%isRowWhite = False%>
                                                    <tr valign="top">
                                                        <td style="width: 173px;">
                                                            <%=Resources.Resource.repair_svc_confirm_date_msg()%>
                                                        </td>
                                                        <td style="width: 376px;">
                                                            <%=DateTime.Now.ToString("MM/dd/yyyy")%>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td>
                                                            <%=Resources.Resource.repair_snysvc_cfrmrd_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=strServicesRequestID %>
                                                        </td>
                                                    </tr>
                                                    <%If bShow = True Then%>
                                                    <%If isAccntHolder = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px;">
                                                            <%=Resources.Resource.repair_svc_confirm_pyracc_msg()%>
                                                        </td>
                                                        <td style="width: 376px;">
                                                            <%=sPayerAccount%>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%End If%>
                                                    <%If bShow = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px;">
                                                            <%=Resources.Resource.repair_svc_confrm_bta_msg()%>
                                                        </td>
                                                        <td style="width: 376px;">
                                                            <%=sBillToAddr %>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_sta_msg()%>
                                                            <%If isProductShipOnHold Then%>
                                                            <br />
                                                            <span class="tabledata"><strong><u>
                                                                <%=Resources.Resource.repair_svc_confrm_dnsu_msg()%></u></strong>
                                                                <%=Resources.Resource.repair_svc_confrm_dnsu_msg_1()%></span>
                                                            <%End If%>
                                                        </td>
                                                        <td>
                                                            <%=sShipToAddr%>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_mno_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=sModelName%>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_sno_msg()%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblSerialNo" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px" valign="top">
                                                            <%=Resources.Resource.repair_svc_confrm_acsries_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=sAccessories%>
                                                        </td>
                                                    </tr>
                                                    <%If bShow = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px" valign="top">
                                                            <%=Resources.Resource.repair_svc_confrm_pono_msg()%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblPONumber" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If bApprFlag = True And bShow = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px" valign="top">
                                                            <%=Resources.Resource.repair_svc_confrm_apprvl_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=Resources.Resource.repair_snysvc_cfrmpgt_msg_7()%><%=mDepotServiceCharge%>
                                                            <%=Resources.Resource.repair_svcacc_info_msg_1()%>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If isTaxExempt = True And bShow = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px" valign="top">
                                                            <%=Resources.Resource.repair_svcacc_txexe_msg()%>
                                                        </td>
                                                        <td>
                                                            <span class="bodyCopy">
                                                                <%=Resources.Resource.repair_svc_confrm_txinfo_msg()%></span>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If bWarrantyFlag = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px" valign="top">
                                                            <%=Resources.Resource.repair_svc_confrm_wrnty_msg()%>
                                                        </td>
                                                        <td>
                                                            <span class="bodyCopy">
                                                                <%=Resources.Resource.repair_svc_confrm_wrnty_msg_1()%></span>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If bContractFlag = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_scn_msg()%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblContractNo" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If bShow = True Then%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_pt_msg()%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblPaymentType" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%End If%>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                    <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                    <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_cname_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=strContactPersonName%>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_cemailadrs_msg()%>
                                                        </td>
                                                        <td>
                                                            <%=sContactEmailAddr%>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_intrmtntprblm_msg()%>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblIntermittent" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px">
                                                            <%=Resources.Resource.repair_svc_confrm_prtvemain_msg()%>
                                                        </td>
                                                        <td>
                                                            <span class="bodyCopy">
                                                                <asp:Label ID="lblPreventive" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></span>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td style="width: 173px;" valign="top">
                                                            <%=Resources.Resource.repair_svcspdsc_pd_msg()%>
                                                        </td>
                                                        <td>
                                                            <span class="bodyCopy">
                                                                <%=sProblemDescr%></span>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td colspan="2">
                                                            <table width="100%" border="0" cellpadding="1" role="presentation"
                                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: #666666">
                                                                <tr valign="top">
                                                                    <td colspan="2">
                                                                        <span class="bodyCopy"><span style="text-decoration: underline">
                                                                            <%=sInclude%></span></span>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td colspan="2" valign="top">
                                                                        <span class="bodyCopy"><span style="text-decoration: underline">
                                                                            <%=sNotify%><br />
                                                                        </span></span>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td style="width: 15px">
                                                                        <img src="/images/checkbox.jpg" border="0" alt="Checkmark" />
                                                                    </td>
                                                                    <td>
                                                                        <span class="bodyCopy">
                                                                            <%=Resources.Resource.repair_svc_confrm_prtvemain_msg_1()%></span>
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td colspan="2">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr valign="top">
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <span class="bodyCopy">
                                                                            <%=sServiceAddr%></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <%If isRowWhite = False Then%>
                                                    <tr valign="top" bgcolor="#d5dee9">
                                                        <%isRowWhite = True%>
                                                        <%Else%>
                                                    <tr valign="top">
                                                        <%isRowWhite = False%>
                                                        <%End If%>
                                                        <td colspan="2">
                                                            <%=Resources.Resource.repair_snysvc_cfrmpgt_msg()%>
                                                            <a href="<%=sNavBackToService%>">
                                                                <%=sNavBackToService%>
                                                            </a>
                                                            <%=Resources.Resource.repair_snysvc_cfrmpgt_msg_1()%>&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="height: 34px">
                                                            <asp:ImageButton ID="imgBtnCompleteRequest" runat="server" ImageUrl="<%$Resources:Resource,repair_svc_img_6%>"
                                                                AlternateText="Complete Depot Service Request" />&nbsp; <a id="lnkPrintRequest"
                                                                    runat="server" href="javascript:PrintForm();">
                                                                    <img src="<%=Resources.Resource.repair_svc_img_7()%>" border="0" alt="Print Depot Service Request Form" /></a>
                                                            &nbsp; <a id="lnkChangeRequest" runat="server">
                                                                <img src='<%=Resources.Resource.repair_svc_img_3() %>' border="0" alt='Change Depot Service Request'></a>
                                                            &nbsp;<a id="lnkCancelRequest" runat="server" href='sony-repair.aspx'><img src='<%=Resources.Resource.repair_svc_img_2() %>'
                                                                border="0" alt='Cancel Depot Service Request'></a> &nbsp;<a id="lnkNewRequest" runat="server"
                                                                    href='sony-repair.aspx'><img src='<%=Resources.Resource.repair_svc_img_4() %>' border="0"
                                                                        alt='New Depot Service Request'></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px">
                                                &nbsp;
                                            </td>
                                            <td colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
