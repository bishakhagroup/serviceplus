Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

	Partial Class SonyFooter
		Inherits System.Web.UI.UserControl
        Public isAmerica As Boolean = True
        Public urlProSite As String = "https://pro.sony/ue_US/"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

		End Sub


		Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
			'CODEGEN: This method call is required by the Web Form Designer
			'Do not modify it using the code editor.
			InitializeComponent()
		End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            lblYear.Text = DateTime.Now.Year.ToString()
            lblBuildNumber.Text = Application("Build").ToString().Trim()

            If Not (Application("AppServer") Is Nothing) Then
                lblBuildNumber.Text += Application("AppServer").ToString().Trim()
            End If

            If DirectCast(DirectCast(Parent, System.Web.UI.Control).Page, System.Web.UI.Page).AppRelativeVirtualPath.Contains("UnderMaintenance.aspx") Then
                hideDuringMaintenance.Visible = False
            Else
                hideDuringMaintenance.Visible = True
            End If

            isAmerica = HttpContextManager.GlobalData.IsAmerica
            If HttpContextManager.GlobalData.Language = "en-CA" Then
                urlProSite = "https://pro.sony/en_CA/"
            ElseIf HttpContextManager.GlobalData.Language = "fr-CA" Then
                urlProSite = "https://pro.sony/qf_CA/"
            Else
                urlProSite = "https://pro.sony/ue_US/"
            End If
        End Sub
    End Class

End Namespace
