Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Text.RegularExpressions
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class sony_service_confirmation
        Inherits SSL

        Dim oServiceInfo As New ServiceItemInfo
        Public isTaxExempt As Boolean
        Public bWarrantyFlag As Boolean
        Public sBillToAddr As String = ""
        Public sShipToAddr As String = ""
        Public sNotify As String
        Public sAccessories As String
        Public sProblemDescr As String
        Public bApprFlag As Boolean
        Public sModelName As String
        Public bShow As Boolean = True
        Public isRowWhite As Boolean = False
        Public sURL As String
        Public sInclude As String = ""
        Public sServiceAddr As String = ""
        Dim current_bill_to As New Account
        Dim current_ship_to As New SISAccount
        Public sPayerAccount As String = ""
        Public sShipToAccnt As String = ""
        Public sContactEmailAddr As String = ""
        Public isAccntHolder As Boolean = False
        Public bContractFlag As Boolean = False 'For fixing Bug# 177
        Public isRequestComplete As Boolean = False
        Public isProductShipOnHold As Boolean = False
        Public strServicesRequestID As String = "NEW"
        Public strContactPersonName As String
        Public mDepotServiceCharge As String = ""
        Public sNavBackToService As String = "" ' Code added for bug 5644

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim objAddress As New CustomerAddr()
                'Dim bFax As String = ""
                Dim tempString As String = ""

                ' ASleight 2017-12-14 - Added check for Session loss.
                If (Session("snServiceInfo") Is Nothing) OrElse (Session("ServiceModelNumber") Is Nothing) Then
                    Response.Redirect("sony-repair.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If

                mDepotServiceCharge = If(HttpContextManager.GlobalData.IsCanada,
                    ConfigurationData.GeneralSettings.RepairCosting.DepotServiceChargeForCanada,
                    ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge)

                oServiceInfo = Session("snServiceInfo")
                'If Not IsPostBack Then
                isTaxExempt = oServiceInfo.isTaxExempt
                isProductShipOnHold = oServiceInfo.IsProductShipOnHold
                If (oServiceInfo.ServicesRequestID <> 0) Then
                    strServicesRequestID = oServiceInfo.ServicesRequestID.ToString()
                End If
                strContactPersonName = oServiceInfo.ContactPersonName
                sModelName = Session("ServiceModelNumber").ToString()
                sInclude = $"# {sModelName} {Resources.Resource.repair_snysvc_confirm_msg}"

                'If Not Request.QueryString("model") Is Nothing Then
                '    sModelName = Request.QueryString("model").Trim()
                '    sInclude = "# " + sModelName + " unit along with the following items to the Sony service center address below:"
                'End If

                oServiceInfo.ModelNumber = sModelName

                lblSubHdr.InnerText = sModelName + Resources.Resource.repair_svcacc_dept_msg

                If oServiceInfo.WarrantyType = "w" Or oServiceInfo.WarrantyType = "s" Then 'Modified for fixing Bug# 177
                    bWarrantyFlag = True
                Else
                    bWarrantyFlag = False
                End If
                bContractFlag = (oServiceInfo.WarrantyType = "c")

                If bShow = True And Session("current_bill_to") IsNot Nothing Then
                    isAccntHolder = True
                    current_bill_to = Session("current_bill_to")
                    If Not String.IsNullOrWhiteSpace(current_bill_to.AccountNumber) Then
                        sPayerAccount = current_bill_to.AccountNumber
                    End If
                End If

                isAccntHolder = oServiceInfo.IsAccountHolder
                oServiceInfo.CustomerID = HttpContextManager.Customer.CustomerID

                If Session("current_ship_to") IsNot Nothing Then
                    isAccntHolder = True
                    current_ship_to = CType(Session("current_ship_to"), SISAccount)
                    If Not String.IsNullOrWhiteSpace(current_ship_to.AccountNumber) Then sShipToAccnt = current_ship_to.AccountNumber
                End If

                oServiceInfo.ShipToAccount = sShipToAccnt

                If bShow And oServiceInfo.WarrantyType.ToString().ToUpper() = "O" Then
                    oServiceInfo.PayerAccount = sPayerAccount
                    If oServiceInfo.BillToAddr IsNot Nothing Then
                        objAddress = oServiceInfo.BillToAddr
                        If Not String.IsNullOrWhiteSpace(objAddress.Company) Then sBillToAddr = objAddress.Company
                        If Not String.IsNullOrWhiteSpace(objAddress.AttnName) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.AttnName}"
                        If Not String.IsNullOrWhiteSpace(objAddress.Address1) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.Address1}"
                        If Not String.IsNullOrWhiteSpace(objAddress.Address2) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.Address2}"
                        If Not String.IsNullOrWhiteSpace(objAddress.Address3) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.Address3}"
                        If Not String.IsNullOrWhiteSpace(objAddress.Address4) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.Address4}"
                        If Not String.IsNullOrWhiteSpace(objAddress.City) Then sBillToAddr &= $"<br/>{vbCrLf}{objAddress.City}"
                        If Not String.IsNullOrWhiteSpace(objAddress.State) Then sBillToAddr &= $", {objAddress.State}"
                        If Not String.IsNullOrWhiteSpace(objAddress.ZipCode) Then sBillToAddr &= $" {objAddress.ZipCode}"

                        ' ASleight - With the input being a string that we cannot guarantee the format of, parsing with exact character counts is causing issues.
                        'If objAddress.Phone.Trim().Length > 3 Then
                        '   sBillToAddr &= "<br><br>" & Resources.Resource.repair_snysvc_cfrmphn_msg '& "("
                        '	tempString = objAddress.Phone
                        '	tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                        '	sBillToAddr = sBillToAddr + tempString
                        'End If
                        'If objAddress.PhoneExtn.Trim().Length > 0 Then
                        '	sBillToAddr = sBillToAddr + "<br>" + Resources.Resource.repair_snysvc_cfrmextn_msg + ""
                        '	tempString = objAddress.PhoneExtn
                        '	sBillToAddr = sBillToAddr + tempString
                        'End If
                        'If objAddress.Fax.Trim().Length > 3 Then
                        '	sBillToAddr = sBillToAddr + "<br>" + Resources.Resource.repair_svcacc_fxno_msg + "("
                        '	tempString = objAddress.Fax
                        '	tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                        '	sBillToAddr = sBillToAddr + tempString
                        'End If
                        If Not String.IsNullOrWhiteSpace(objAddress.Phone) Then
                            sBillToAddr &= $"<br/><br/>{vbCrLf}{Resources.Resource.repair_snysvc_cfrmphn_msg} {objAddress.Phone}"
                        End If
                        If Not String.IsNullOrWhiteSpace(objAddress.PhoneExtn) Then
                            sBillToAddr &= $"<br/>{vbCrLf}{Resources.Resource.repair_snysvc_cfrmextn_msg} {objAddress.PhoneExtn}"
                        End If
                        If Not String.IsNullOrWhiteSpace(objAddress.Fax) Then
                            sBillToAddr &= $"<br/>{vbCrLf}{Resources.Resource.repair_svcacc_fxno_msg} {objAddress.Fax}"
                        End If
                    End If
                End If

                objAddress = oServiceInfo.ShipToAddr
                If Not String.IsNullOrWhiteSpace(objAddress.Company) Then sShipToAddr = objAddress.Company
                If Not String.IsNullOrWhiteSpace(objAddress.AttnName) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.AttnName}"
                If Not String.IsNullOrWhiteSpace(objAddress.Address1) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.Address1}"
                If Not String.IsNullOrWhiteSpace(objAddress.Address2) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.Address2}"
                If Not String.IsNullOrWhiteSpace(objAddress.Address3) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.Address3}"
                If Not String.IsNullOrWhiteSpace(objAddress.Address4) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.Address4}"
                If Not String.IsNullOrWhiteSpace(objAddress.City) Then sShipToAddr &= $"<br/>{vbCrLf}{objAddress.City}"
                If Not String.IsNullOrWhiteSpace(objAddress.State) Then sShipToAddr &= $", {objAddress.State}"
                If Not String.IsNullOrWhiteSpace(objAddress.ZipCode) Then sShipToAddr &= $" {objAddress.ZipCode}"
                '2016-05-11 ASleight Updating phone number formatting; expects phone numbers stored like XXX-XXX-XXXX.
                ' If there's a 1- in front it messes up. ie: "(180) 088-36817"
                'If objAddress.Phone.Trim().Length > 3 Then
                '    sShipToAddr = sShipToAddr + "<br><br>" + Resources.Resource.repair_snysvc_cfrmphn_msg + "("
                '    tempString = objAddress.Phone
                '    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                '    sShipToAddr = sShipToAddr + tempString
                'End If
                'If objAddress.PhoneExtn.Trim().Length > 0 Then
                '    sShipToAddr = sShipToAddr + "<br>" + Resources.Resource.repair_snysvc_cfrmextn_msg + ""
                '    tempString = objAddress.PhoneExtn
                '    sShipToAddr = sShipToAddr + tempString
                'End If
                'If objAddress.Fax.Trim().Length > 3 Then
                '    sShipToAddr = sShipToAddr + "<br> Fax: ("
                '    tempString = objAddress.Fax
                '    tempString = tempString.Insert(3, ")").Replace(")-", ") ")
                '    sShipToAddr = sShipToAddr + tempString
                'End If

                If Not String.IsNullOrEmpty(objAddress.Phone.Trim) Then
                    sShipToAddr &= $"<br/><br/>{vbCrLf}{Resources.Resource.repair_snysvc_cfrmphn_msg} {objAddress.Phone}"
                End If
                If Not String.IsNullOrEmpty(objAddress.PhoneExtn.Trim) Then
                    sShipToAddr &= $"<br/>{vbCrLf}{Resources.Resource.repair_snysvc_cfrmextn_msg} {objAddress.PhoneExtn}"
                End If
                If Not String.IsNullOrEmpty(objAddress.Fax.Trim) Then
                    sShipToAddr &= $"<br/>{vbCrLf}{Resources.Resource.repair_svcacc_fxno_msg} {objAddress.Fax}"
                End If

                lblPONumber.Text = oServiceInfo.PONumber
                'lblPreapprAmt.Text = "$" + oServiceInfo.PreApproveAmount.ToString() 'Modified for fixing Bug# 176 "Commented to fix Bug# 610
                sProblemDescr = oServiceInfo.ProblemDescription
                lblSerialNo.Text = oServiceInfo.SerialNumber
                sAccessories = oServiceInfo.Accessories
                bApprFlag = oServiceInfo.isApproval

                If UCase(oServiceInfo.WarrantyType) = "C" Or UCase(oServiceInfo.WarrantyType) = "W" Then
                    bShow = False   ' ASleight - This is the only place the variable is changed... halfway through a method that uses the value twice above.
                End If

                sNotify = "<tr><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" & Resources.Resource.repair_snysvc_cfrminfo_msg & vbCrLf &
                    Resources.Resource.repair_snysvc_cfrminfo_msg_1 & "&nbsp;</td></tr>"
                If Not String.IsNullOrEmpty(oServiceInfo.Accessories) Then
                    sNotify &= "<tr><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" & Resources.Resource.repair_snysvc_snacc_msg & oServiceInfo.Accessories & "</td></tr>"
                End If
                If oServiceInfo.isTaxExempt = True And UCase(oServiceInfo.WarrantyType) = "O" Then
                    sNotify &= "<tr><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark""  /></td><td>" & Resources.Resource.repair_snysvc_cfrminfo_msg_5 &
                        vbCrLf & Resources.Resource.repair_snysvc_cfrminfo_msg_2 &
                        vbCrLf & Resources.Resource.repair_snysvc_cfrminfo_msg_3 & "&nbsp;</td></tr>"
                End If

                If UCase(oServiceInfo.WarrantyType) = "W" Then
                    sNotify &= "<tr valign=""top""><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" & Resources.Resource.repair_snysvc_cfrminfo_msg_5 & vbCrLf &
                        Resources.Resource.repair_snysvc_cfrminfo_msg_4 & vbCrLf &
                        Resources.Resource.repair_snysvc_warnty_msg & ".&nbsp;</td></tr>"
                End If
                sNotify &= "<tr valign=""top""><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" & Resources.Resource.repair_snysvc_cfrminfo_msg_6 & vbCrLf &
                    Resources.Resource.repair_snysvc_cfrminfo_msg_7 & "&nbsp;</td></tr>"

                'If oServiceInfo.IsProductShipOnHold Then
                '    sNotify = sNotify + "<tr valign=""top""><td><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" + vbCrLf + "Do not ship unit: Hold unit for pickup at service center." + vbCrLf + "&nbsp;</td></tr>"
                'End If

                If oServiceInfo.DepotCenterAddress.Length > 0 Then
                    sServiceAddr = "<strong>" & Resources.Resource.repair_snysvc_cfrmlg_msg & Resources.Resource.repair_snysvc_cfrmlg_msg_1 & "</strong><br/>" & vbCrLf &
                        Resources.Resource.repair_snysvc_cfrmlg_msg_2 & "<br/>" &
                        oServiceInfo.DepotCenterAddress
                End If

                If Not String.IsNullOrWhiteSpace(oServiceInfo.ContractNumber) Then
                    lblContractNo.Text = oServiceInfo.ContractNumber
                Else
                    lblContractNo.Text = Resources.Resource.repair_snysvc_cfrmdtrn_msg
                End If

                If oServiceInfo.PaymentType = "1" Then
                    lblPaymentType.Text = Resources.Resource.repair_snysvc_cfrmsnyaccnt_msg
                ElseIf oServiceInfo.PaymentType = "2" Then
                    lblPaymentType.Text = Resources.Resource.repair_svcacc_crdtcrd_msg
                Else
                    lblPaymentType.Text = Resources.Resource.repair_svcacc_chk_msg
                End If

                sContactEmailAddr = oServiceInfo.ContactEmailAddress

                If oServiceInfo.isPreventiveMaintenance = True Then
                    lblPreventive.Text = Resources.Resource.repair_svcspdsc_ys_msg
                Else
                    lblPreventive.Text = Resources.Resource.repair_svcspdsc_no_msg
                End If
                If oServiceInfo.isIntermittentProblem = True Then
                    lblIntermittent.Text = Resources.Resource.repair_svcspdsc_ys_msg
                Else
                    lblIntermittent.Text = Resources.Resource.repair_svcspdsc_no_msg
                End If

                Session("snServiceInfo") = oServiceInfo

                sURL = "/sony-repair.aspx"
                If (Session("RequestPath") IsNot Nothing) AndAlso (Session("RequestPath").ToString() = "engine") Then
                    sURL = "/sony-service-repair-maintenance.aspx"
                End If
                '2016-05-11 ASleight Modifying to add Canadian site, instead of Canadian users being sent to the US site by this link.
                If HttpContextManager.GlobalData.IsCanada Then
                    sNavBackToService = ConfigurationData.Environment.URL.CA + sURL
                Else
                    sNavBackToService = ConfigurationData.Environment.URL.US + sURL
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            lnkPrintRequest.Visible = isRequestComplete
            lnkChangeRequest.Visible = Not isRequestComplete
            imgBtnCompleteRequest.Visible = Not isRequestComplete
            lnkNewRequest.Visible = isRequestComplete
            lnkCancelRequest.Visible = Not isRequestComplete
            lnkChangeRequest.HRef = "sony-service-sn.aspx?model=" + sModelName

            If (isRequestComplete = True) Then
                sInclude = sInclude.Replace("#", "Please send your")
            Else
                sInclude = sInclude.Replace("#", "After confirming this request, please send your")
            End If
        End Sub

        Protected Sub imgBtnCompleteRequest_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnCompleteRequest.Click
            Dim mailBody As String = String.Empty
            Dim oServiceItemInfo As ServiceItemInfo
            Dim custMan As New CustomerManager
            Dim customer As Customer
            Dim toMailID As String = ""
            Dim EmailBody As New StringBuilder(5000)
            Dim Subject As String = String.Empty
            Dim objPCILogger = New PCILogger()

            Try
                objPCILogger.EventOriginApplication = Resources.Resource.header_serviceplus
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "imgBtnCompleteRequest_Click"
                objPCILogger.Message = "Depot Service Request Success"
                objPCILogger.EventType = EventType.Others
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                    objPCILogger.CustomerID = customer.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                    objPCILogger.EmailAddress = customer.EmailAddress
                    objPCILogger.SIAM_ID = customer.SIAMIdentity
                    objPCILogger.LDAP_ID = customer.LdapID
                    toMailID = customer.EmailAddress
                End If
                If Session("snServiceInfo") Is Nothing Then Throw New NullReferenceException("Session('snServiceInfo') was null. Possible Session loss occurred.")
                oServiceItemInfo = CType(Session("snServiceInfo"), ServiceItemInfo)
                oServiceItemInfo.ServicesRequestID = custMan.SaveServiceRequestData(oServiceInfo)
                strServicesRequestID = oServiceItemInfo.ServicesRequestID.ToString()
                Session("snServiceInfo") = oServiceItemInfo

                BuildMailBody(EmailBody, Subject)
                custMan.SendServiceConfirmationMail(EmailBody.ToString(), toMailID, sContactEmailAddr, sServiceAddr, oServiceItemInfo.DepotEmail, Subject)
                isRequestComplete = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "Depot Service Request Failed. " & ex.Message
            Finally
                objPCILogger.PushLogToMSMQ() '6524
                custMan = Nothing
            End Try
        End Sub

        ' 2018-10-17 ASleight - More cleanup of this code. Added C#-style string interpolation, shortened the logic for alternating coloured rows, fixed some style issues.
        Sub BuildMailBody(ByRef Emailbody As StringBuilder, ByRef Subject As String)
            Try
                If Session("snServiceInfo") Is Nothing Then Throw New NullReferenceException("Session('snServiceInfo') was null. Possible Session loss occurred.")
                oServiceInfo = Session("snServiceInfo")
                Dim sb As StringBuilder = New StringBuilder()

                'Dim objGlobalData As New GlobalData
                'If Not Session.Item("GlobalData") Is Nothing Then
                '    objGlobalData = Session.Item("GlobalData")
                'End If

                Emailbody.Append("<table cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width:1px; border-right-color:666666; border-right-style:solid; border-right-width:1px; border-left-color:666666; border-left-style:solid; border-left-width:1px; border-top-color:666666; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:666666"">")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append($"<td style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=""{Resources.Resource_mail.ml_ProductRegistration_body_imglogo_en}"" alt=""Product Registration""/></td>")
                Emailbody.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin;"" align=""right"" valign=""middle"">&nbsp;</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
                Emailbody.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"">")
                Emailbody.Append("<tr>")
                Emailbody.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>ServicesPLUS -" + vbCrLf + Resources.Resource_mail.ml_serviceconfirm_body_line1_en + "</strong></td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr>")
                Emailbody.Append("<td style=""width: 100px""></td>")
                Emailbody.Append("<td style=""width: 100px""></td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("</table>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")
                Emailbody.Append("<tr valign=""top"">")
                Emailbody.Append("<td style=""width: 173px;"">Date:</td>")
                Emailbody.Append("<td style=""width: 376px;"">" + DateTime.Now.ToString("MM/dd/yyyy") + "</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px;"">" + vbCrLf + " " + Resources.Resource_mail.ml_serviceconfirm_RequestId_en + "</td>")
                Emailbody.Append("<td style=""width: 376px;"">" + strServicesRequestID + "</td>")
                Emailbody.Append("</tr>")

                If bShow = True Then
                    If isAccntHolder = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_serviceconfirm_payeraccount_en + "</td>")
                        Emailbody.Append("<td style=""width: 376px;"">" + sPayerAccount + "</td>")
                        Emailbody.Append("</tr>")
                    End If

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_serviceconfirm_BillToAddress_en + "</td>")
                    Emailbody.Append("<td style=""width: 376px;"">" + sBillToAddr + "</td>")
                    Emailbody.Append("</tr>")
                End If

                'If isAccntHolder = True Then
                '    If isRowWhite = False Then
                '        Emailbody.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                '        isRowWhite = True
                '        Emailbody.Append("<td style=""width: 173px;"">Ship To" + vbCrLf + "Account:</td>")
                '        Emailbody.Append("<td style=""width: 376px;"">" + sShipToAccnt + "</td>")
                '        Emailbody.Append("</tr>")
                '    Else
                '        Emailbody.Append("<tr valign=""top"">")
                '        isRowWhite = False
                '        Emailbody.Append("<td style=""width: 173px;"">Ship To" + vbCrLf + "Account:</td>")
                '        Emailbody.Append("<td style=""width: 376px;"">" + sShipToAccnt + "</td>")
                '        Emailbody.Append("</tr>")
                '    End If
                'End If

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_ShipToAddress_en)
                If isProductShipOnHold Then
                    Emailbody.Append("<br /><span class=""tabledata""><strong><u>" + Resources.Resource_mail.ml_serviceconfirm_donotship_en + "</u></strong></span>")
                End If
                Emailbody.Append("</td>")
                Emailbody.Append($"<td>{sShipToAddr}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_en + "</td>")
                Emailbody.Append($"<td>{sModelName}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_en + "</td>")
                Emailbody.Append($"<td>{oServiceInfo.SerialNumber}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_Accessories_en + "</td>")
                Emailbody.Append($"<td>{sAccessories}</td>")
                Emailbody.Append("</tr>")

                If bShow = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_EmailQuote_PONumber_en + "</td>")
                    Emailbody.Append($"<td>{oServiceInfo.PONumber}</td>")
                    Emailbody.Append("</tr>")
                End If

                If bApprFlag = True And bShow = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_Approval_en + "</td>")
                    'Emailbody.Append("<td>" + Resources.Resource_mail.ml_serviceconfirm_laborcharge_en + Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge + " " + Resources.Resource_mail.ml_serviceconfirm_shipping_en + "</td>")
                    Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_laborcharge_en}{mDepotServiceCharge} {Resources.Resource_mail.ml_serviceconfirm_shipping_en}</td>")
                    Emailbody.Append("</tr>")
                End If

                'If bShow = True Then 'Commented to fix Bug# 610
                '    If isRowWhite = False Then
                '        Emailbody.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                '        isRowWhite = True
                '        Emailbody.Append("<td style=""width: 173px"" valign=""top"">Pre-approval:</td>")
                '        Emailbody.Append("<td>$" + oServiceInfo.PreApproveAmount.ToString() + "</td>")
                '        Emailbody.Append("</tr>")
                '    Else
                '        Emailbody.Append("<tr valign=""top"">")
                '        isRowWhite = False
                '        Emailbody.Append("<td style=""width: 173px"" valign=""top"">Pre-approval:</td>")
                '        Emailbody.Append("<td>$" + oServiceInfo.PreApproveAmount.ToString() + "</td>")
                '        Emailbody.Append("</tr>")
                '    End If
                'End If

                If isTaxExempt = True And bShow = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_TaxExempt_en + "</td>")
                    Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_TaxExemptcert_en}</td>")
                    Emailbody.Append("</tr>")
                End If

                If bWarrantyFlag = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_RARequest_warranty_en + "</td>")
                    Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_salesinvoice_en}</td>")
                    Emailbody.Append("</tr>")
                End If

                If bContractFlag = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_servicecontractnumber_en + "</td>")
                    If Not String.IsNullOrEmpty(oServiceInfo.ContractNumber) Then
                        Emailbody.Append($"<td>{oServiceInfo.ContractNumber}</td>")
                    Else
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_determined_en}</td>")
                    End If
                    Emailbody.Append("</tr>")
                End If

                If bShow = True Then
                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_paymenttype_en + "</td>")
                    If oServiceInfo.PaymentType = "1" Then
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_sonyaccount_en}</td>")
                    ElseIf oServiceInfo.PaymentType = "2" Then
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_creditcard_en}</td>")
                    Else
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_check_en}</td>")
                    End If
                    Emailbody.Append("</tr>")
                End If

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_contactname_en + "</td>")
                Emailbody.Append($"<td>{strContactPersonName}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_contactemailaddress_en + "</td>")
                Emailbody.Append($"<td>{sContactEmailAddr}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_intermittentproblem_en + "</td>")
                If oServiceInfo.isIntermittentProblem = True Then
                    Emailbody.Append("<td>Yes</td>")
                Else
                    Emailbody.Append("<td>No</td>")
                End If
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_preventivemaintenance_en + "</td>")
                If oServiceInfo.isPreventiveMaintenance = True Then
                    Emailbody.Append("<td>Yes</td>")
                Else
                    Emailbody.Append("<td>No</td>")
                End If
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td style=""width: 173px;"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_problemdescription_en + "</td>")
                Emailbody.Append($"<td>{sProblemDescr}</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td colspan=""2"">")
                Emailbody.Append("<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""1"" style=""font-family:Arial, Helvetica, sans-serif; font-size:11px; color:666666"">")
                Emailbody.Append("<tr valign=""top""><td colspan=""2""><span style=""text-decoration: underline"">" + Resources.Resource_mail.ml_serviceconfirm_pleasesendyour_en + sModelName + Resources.Resource_mail.ml_serviceconfirm_sonyunit_en + "</span></td></tr>")
                Emailbody.Append("<tr valign=""top""><td colspan=""2"" valign=""top""><span style=""text-decoration: underline"">" + sNotify + "<br /></span></td></tr>")
                Emailbody.Append("<tr valign=""top""><td style=""width:15px""><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" alt=""Checkmark"" /></td><td>" + Resources.Resource_mail.ml_serviceconfirm_mediasample_en + "</td></tr>")
                Emailbody.Append("<tr valign=""top""><td colspan=""2"">&nbsp;</td></tr>")
                Emailbody.Append("<tr valign=""top""><td>&nbsp;</td><td>" + sServiceAddr + "</td></tr>")
                Emailbody.Append("</table>")
                Emailbody.Append("</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
                Emailbody.Append("</tr>")

                Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                    Emailbody.Append("<td colspan=""2"">" + Resources.Resource_mail.ml_serviceconfirm_trackurl_en + "</td>")    ' https://www.servicesplus.sel.sony.com/sony-repair.aspx
                Else
                    Emailbody.Append("<td colspan=""2"">" + Resources.Resource_mail.ml_serviceconfirm_trackurl_fr_en + "</td>") ' https://servicesplus.sony.ca/sony-repair.aspx
                End If
                Emailbody.Append("</tr>")
                Emailbody.Append("</table>")

                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    Emailbody.Append(vbCrLf + vbCrLf)
                    Emailbody.Append("<table cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 93%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width:1px; border-right-color:666666; border-right-style:solid; border-right-width:1px; border-left-color:666666; border-left-style:solid; border-left-width:1px; border-top-color:666666; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:666666"">")
                    Emailbody.Append("<tr valign=""top"">")
                    Emailbody.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=" + Resources.Resource_mail.ml_ProductRegistration_body_imglogo_fr + " alt=""Product Registration""/></td>")
                    Emailbody.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin;"" align=""right"" valign=""middle"">&nbsp;</td>")
                    Emailbody.Append("</tr>")
                    Emailbody.Append("<tr valign=""top"">")
                    Emailbody.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
                    Emailbody.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"">")
                    Emailbody.Append("<tr>")
                    Emailbody.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>ServicesPLUS -" + vbCrLf + Resources.Resource_mail.ml_serviceconfirm_body_line1_fr + "</strong></td>")
                    Emailbody.Append("</tr>")
                    Emailbody.Append("<tr>")
                    Emailbody.Append("<td style=""width: 100px""></td>")
                    Emailbody.Append("<td style=""width: 100px""></td>")
                    Emailbody.Append("</tr>")
                    Emailbody.Append("</table>")
                    Emailbody.Append("</td>")
                    Emailbody.Append("</tr>")
                    Emailbody.Append("<tr valign=""top"">")
                    Emailbody.Append("<td style=""width: 173px;"">Date:</td>")
                    Emailbody.Append("<td style=""width: 376px;"">" + DateTime.Now.ToString("MM/dd/yyyy") + "</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px;"">" + vbCrLf + " " + Resources.Resource_mail.ml_serviceconfirm_RequestId_fr + "</td>")
                    Emailbody.Append("<td style=""width: 376px;"">" + strServicesRequestID + "</td>")
                    Emailbody.Append("</tr>")

                    If bShow = True Then
                        If isAccntHolder = True Then
                            Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                            isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                            Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_serviceconfirm_payeraccount_fr + "</td>")
                            Emailbody.Append("<td style=""width: 376px;"">" + sPayerAccount + "</td>")
                            Emailbody.Append("</tr>")
                        End If
                    End If

                    If bShow = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px;"">" + Resources.Resource_mail.ml_serviceconfirm_BillToAddress_fr + "</td>")
                        Emailbody.Append("<td style=""width: 376px;"">" + sBillToAddr + "</td>")
                        Emailbody.Append("</tr>")
                    End If

                    'If isAccntHolder = True Then
                    '    If isRowWhite = False Then
                    '        Emailbody.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                    '        isRowWhite = True
                    '        Emailbody.Append("<td style=""width: 173px;"">Ship To" + vbCrLf + "Account:</td>")
                    '        Emailbody.Append("<td style=""width: 376px;"">" + sShipToAccnt + "</td>")
                    '        Emailbody.Append("</tr>")
                    '    Else
                    '        Emailbody.Append("<tr valign=""top"">")
                    '        isRowWhite = False
                    '        Emailbody.Append("<td style=""width: 173px;"">Ship To" + vbCrLf + "Account:</td>")
                    '        Emailbody.Append("<td style=""width: 376px;"">" + sShipToAccnt + "</td>")
                    '        Emailbody.Append("</tr>")
                    '    End If
                    'End If

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_ShipToAddress_fr)
                    If isProductShipOnHold Then
                        Emailbody.Append("<br /><span class=""tabledata""><strong><u>" + Resources.Resource_mail.ml_serviceconfirm_donotship_fr + "</u></strong></span>")
                    End If
                    Emailbody.Append("</td>")
                    Emailbody.Append($"<td>{sShipToAddr}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_fr + "</td>")
                    Emailbody.Append($"<td>{sModelName}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_fr + "</td>")
                    Emailbody.Append($"<td>{oServiceInfo.SerialNumber}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_Accessories_fr + "</td>")
                    Emailbody.Append($"<td>{sAccessories}</td>")
                    Emailbody.Append("</tr>")

                    If bShow = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_EmailQuote_PONumber_fr + "</td>")
                        Emailbody.Append($"<td>{oServiceInfo.PONumber}</td>")
                        Emailbody.Append("</tr>")
                    End If

                    If bApprFlag = True And bShow = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_Approval_fr + "</td>")
                        'Emailbody.Append("<td>" + Resources.Resource_mail.ml_serviceconfirm_laborcharge_fr + Sony.US.SIAMUtilities.ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge + " " + Resources.Resource_mail.ml_serviceconfirm_shipping_fr + "</td>")
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_laborcharge_fr}{mDepotServiceCharge} {Resources.Resource_mail.ml_serviceconfirm_shipping_fr}</td>")
                        Emailbody.Append("</tr>")
                    End If

                    'If bShow = True Then 'Commented to fix Bug# 610
                    '    If isRowWhite = False Then
                    '        Emailbody.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                    '        isRowWhite = True
                    '        Emailbody.Append("<td style=""width: 173px"" valign=""top"">Pre-approval:</td>")
                    '        Emailbody.Append("<td>$" + oServiceInfo.PreApproveAmount.ToString() + "</td>")
                    '        Emailbody.Append("</tr>")
                    '    Else
                    '        Emailbody.Append("<tr valign=""top"">")
                    '        isRowWhite = False
                    '        Emailbody.Append("<td style=""width: 173px"" valign=""top"">Pre-approval:</td>")
                    '        Emailbody.Append("<td>$" + oServiceInfo.PreApproveAmount.ToString() + "</td>")
                    '        Emailbody.Append("</tr>")
                    '    End If
                    'End If

                    If isTaxExempt = True And bShow = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_TaxExempt_fr + "</td>")
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_TaxExemptcert_fr}</td>")
                        Emailbody.Append("</tr>")
                    End If

                    If bWarrantyFlag = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"" valign=""top"">" + Resources.Resource_mail.ml_RARequest_warranty_fr + "</td>")
                        Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_salesinvoice_fr}</td>")
                        Emailbody.Append("</tr>")
                    End If

                    If bContractFlag = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_servicecontractnumber_fr + "</td>")
                        If oServiceInfo.ContractNumber.ToString().Length > 0 Then
                            Emailbody.Append($"<td>{oServiceInfo.ContractNumber}</td>")
                        Else
                            Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_determined_fr}</td>")
                        End If
                        Emailbody.Append("</tr>")
                    End If

                    If bShow = True Then
                        Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                        isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                        Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_paymenttype_fr + "</td>")
                        If oServiceInfo.PaymentType = "1" Then
                            Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_sonyaccount_fr}</td>")
                        ElseIf oServiceInfo.PaymentType = "2" Then
                            Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_creditcard_fr}</td>")
                        Else
                            Emailbody.Append($"<td>{Resources.Resource_mail.ml_serviceconfirm_check_fr}</td>")
                        End If
                        Emailbody.Append("</tr>")
                    End If

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_contactname_fr + "</td>")
                    Emailbody.Append($"<td>{strContactPersonName}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_contactemailaddress_fr + "</td>")
                    Emailbody.Append($"<td>{sContactEmailAddr}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_intermittentproblem_fr + "</td>")
                    If oServiceInfo.isIntermittentProblem = True Then
                        Emailbody.Append("<td>Yes</td>")
                    Else
                        Emailbody.Append("<td>No</td>")
                    End If
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px"">" + Resources.Resource_mail.ml_serviceconfirm_preventivemaintenance_fr + "</td>")
                    If oServiceInfo.isPreventiveMaintenance = True Then
                        Emailbody.Append("<td>Yes</td>")
                    Else
                        Emailbody.Append("<td>No</td>")
                    End If
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td style=""width: 173px;"" valign=""top"">" + Resources.Resource_mail.ml_serviceconfirm_problemdescription_fr + "</td>")
                    Emailbody.Append($"<td>{sProblemDescr}</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td colspan=""2"">")
                    Emailbody.Append("<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""1"" style=""font-family:Arial, Helvetica, sans-serif; font-size:11px; color:666666"">")
                    Emailbody.Append("<tr valign=""top""><td colspan=""2""><span style=""text-decoration: underline"">" + Resources.Resource_mail.ml_serviceconfirm_pleasesendyour_fr + sModelName + Resources.Resource_mail.ml_serviceconfirm_sonyunit_fr + "</span></td></tr>")
                    Emailbody.Append("<tr valign=""top""><td colspan=""2"" valign=""top""><span style=""text-decoration: underline"">" + sNotify + "<br /></span></td></tr>")
                    Emailbody.Append("<tr valign=""top""><td style=""width:15px""><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" /></td><td>" + Resources.Resource_mail.ml_serviceconfirm_mediasample_fr + "</td></tr>")
                    Emailbody.Append("<tr valign=""top""><td colspan=""2"">&nbsp;</td></tr>")
                    Emailbody.Append("<tr valign=""top""><td>&nbsp;</td><td>" + sServiceAddr + "</td></tr>")
                    Emailbody.Append("</table>")
                    Emailbody.Append("</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td colspan=""2"">&nbsp;</td>")
                    Emailbody.Append("</tr>")

                    Emailbody.Append($"<tr valign=""top""{IIf(isRowWhite, "", " bgColor = ""#d5dee9""")}>")
                    isRowWhite = Not isRowWhite   ' Invert the flag for the next row.
                    Emailbody.Append("<td colspan=""2"">" + Resources.Resource_mail.ml_serviceconfirm_trackurl_fr + "</td>")
                    Emailbody.Append("</tr>")
                    Emailbody.Append("</table>")

                    If ConfigurationData.GeneralSettings.Environment.ToUpper() <> "PRODUCTION" Then
                        Subject = Resources.Resource_mail.ml_serviceconfirm_subject_en + "(" + ConfigurationData.GeneralSettings.Environment + ") / " + Resources.Resource_mail.ml_serviceconfirm_subject_fr + "(" + ConfigurationData.GeneralSettings.Environment + ")"
                    Else
                        Subject = Resources.Resource_mail.ml_serviceconfirm_subject_en + "/" + Resources.Resource_mail.ml_serviceconfirm_subject_fr
                    End If
                Else
                    ' Non-Canadian mail
                    If ConfigurationData.GeneralSettings.Environment.ToUpper() <> "PRODUCTION" Then
                        Subject = Resources.Resource_mail.ml_serviceconfirm_subject_en + "(" + ConfigurationData.GeneralSettings.Environment + ")"
                    Else
                        Subject = Resources.Resource_mail.ml_serviceconfirm_subject_en
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class
End Namespace
