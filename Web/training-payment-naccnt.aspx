<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" SmartNavigation="true" AutoEventWireup="false" Inherits="ServicePLUSWebApp.training_payment_naccnt" CodeFile="training-payment-naccnt.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute � Class Payment</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("TextBoxPO").value = "NO-PO-REQ";
            }
            else {
                document.getElementById("TextBoxPO").value = "";
            }
        }

        function copyAddress() {
            if (document.getElementById('chkSameShip').checked === true) {
                document.getElementById('TextBoxNameShip').value = document.getElementById('TextBoxName').value;
                document.getElementById('TextboxShipAttn').value = document.getElementById('TextboxAttn').value;
                document.getElementById('TextBoxLineShip').value = document.getElementById('TextBoxLine1').value;
                document.getElementById('TextBoxLine2Ship').value = document.getElementById('TextBoxLine2').value;
                document.getElementById('TextBoxCityShip').value = document.getElementById('TextBoxCity').value;

                document.getElementById('DDLStateShip').selectedIndex = document.getElementById('DDLState').selectedIndex;

                document.getElementById('TextBox5').value = document.getElementById('TextBoxZip').value;

                document.getElementById('LabelName2Star').value = "";
                document.getElementById('LabelLine1ShipStar').value = "";
                document.getElementById('LabelState2Star').value = "";
                document.getElementById('LabelZip2Star').value = "";
                document.getElementById('LabelCity2Star').value = "";
            }
            document.getElementById('btnNext').focus();
        }

        function showHideCC() {

        }
    </script>

    <style type="text/css">
        #Text1 {
            width: 22px;
        }

        #NewCC {
            width: 115px;
        }
    </style>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table id="Table1" width="760" border="0" role="presentation">
                <tbody>
                    <tr>
                        <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                            <img src="images/spacer.gif" width="25" height="25" alt="">
                        </td>
                        <td width="710" bgcolor="#ffffff">
                            <table id="Table2" width="710" border="0" role="presentation">
                                <tr>
                                    <td>
                                        <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="Table3" width="710" border="0" role="presentation">
                                            <tr>
                                                <%--<td width=464 bgColor=#363d45><IMG height=40 src="images/sp_int_header_top_ServicesPLUS_short.gif" width=464 ></td>--%>
                                                <td width="464" bgcolor="#363d45" height="1" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                    align="right" valign="top">
                                                    <br>
                                                    <%--<td align="right"  bgColor="#363d45"
														height="82" style="width: 465px"><br>--%>
                                                    <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                                </td>
                                                <td valign="top" bgcolor="#363d45">
                                                    <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#f2f5f8">
                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Payment</h2>
                                                </td>
                                                <td bgcolor="#99a8b5">&nbsp;</td>
                                            </tr>
                                            <tr height="9">
                                                <td bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="Table6" width="710" border="0" role="presentation">
                                            <tr>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td width="670" height="20"></td>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20" style="height: 746px">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td valign="top" width="670" style="height: 746px">
                                                    <table id="Table7" cellpadding="3" width="664" border="0" role="presentation">
                                                        <tr>
                                                            <td colspan="2">
                                                                <span class="tableData">
                                                                    <strong>Please complete required fields below marked with an asterisk.</strong>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="92" style="height: 7px"></td>
                                                            <td style="height: 7px; width: 580px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#d5dee9" colspan="2" style="height: 19px"><span class="tableHeader">&nbsp;&nbsp;&nbsp;Bill to:</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                &nbsp;<asp:Label ID="lblCompanyName" runat="server" CssClass="tableData" Width="100px">Company Name:</asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="txtBillToCompanyName" runat="server" CssClass="tableData" Width="200px"></SPS:SPSTextBox><asp:Label ID="lblBillToCompanyNameStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">&nbsp;<asp:Label ID="LabelName" runat="server" CssClass="tableData">Name as it appears on your credit card:</asp:Label></td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxName" runat="server" CssClass="tableData" Width="200px"></SPS:SPSTextBox>
                                                                <asp:Label ID="LabelNameStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>

                                                            <td class="tableData" align="left" colspan="2" width="100%">
                                                                &nbsp;
                                                            <br />
                                                                <asp:Label ID="Label1" runat="server" CssClass="tableData">Billing Address - Must match the address on your credit card statement.</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="LabelLine1" runat="server" CssClass="tableData">Street Address: </asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxLine1" runat="server" CssClass="tableData" Width="200px"></SPS:SPSTextBox>&nbsp; 
                                                                <asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick">*</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="LabelLine2" runat="server" CssClass="tableData">Address 2nd Line: </asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxLine2" runat="server" CssClass="tableData" Width="200px" />&nbsp; 
				                                                <asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick" />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" visible="false">
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="LabelLine3" runat="server" CssClass="tableData">Address 3rd Line: </asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxLine3" runat="server" CssClass="tableData" Width="200px" />&nbsp; 
				                                                <asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="LabelCity" runat="server" CssClass="tableData">City:</asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxCity" runat="server" CssClass="tableData" Width="160px" />&nbsp; 
                                                                <asp:Label ID="LabelCityStar" runat="server" CssClass="redAsterick">*</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                &nbsp;
                                                                <asp:Label ID="LabelState" runat="server" CssClass="tableData">State:</asp:Label>
                                                            </td>
                                                            <td style="width: 580px">
                                                                <asp:DropDownList ID="DDLState" runat="server" CssClass="tableData" />
                                                                <asp:Label ID="LabelStateStar" runat="server" CssClass="redAsterick">*</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92" height="27">
                                                                &nbsp;
                                                                <asp:Label ID="LabelZip" runat="server" CssClass="tableData">Zip Code</asp:Label>
                                                            </td>
                                                            <td height="27" style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxZip" runat="server" CssClass="tableData" Width="100px" MaxLength="5" />&nbsp; 
                                                                <asp:Label ID="LabelZipStar" runat="server" CssClass="redAsterick">*</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="PoNumberLabel" runat="server" CssClass="tableData">PO Number:</asp:Label>
                                                            </td>
                                                            <td class="tableData" style="width: 580px">
                                                                <SPS:SPSTextBox ID="TextBoxPO" runat="server" CssClass="tableData" Width="100px"></SPS:SPSTextBox><asp:Label ID="PoNumberAsterick" runat="server" CssClass="redAsterick">
																                                <span class="redAsterick">*</span>
                                                                </asp:Label>
                                                                <%--<asp:checkbox id=noPoRequiredCheckBox runat="server" CssClass="tabledata" Text="Company does not require a purchase order"></asp:checkbox>--%>
                                                                <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData" onclick="javascript: poNotRequired();" />Company does not require a purchase order
            																				
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="tableData" width="92">
                                                            </td>
                                                            <td class="tableData" style="width: 580px">
                                                                <asp:RadioButtonList ID="rbBillTo" runat="server" CssClass="tableData" onclick="showHideCC();">
                                                                    <asp:ListItem Value="1" Selected="True">Bill to my Credit Card</asp:ListItem>
                                                                    <asp:ListItem Value="2">Send me an Invoice to my Email Address</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                <asp:Label ID="lblBudget" runat="server" Visible="False" CssClass="tableData">Budget Code:</asp:Label>
                                                                <SPS:SPSTextBox ID="txtBudgetCode1" runat="server" CssClass="tableData" Width="100px" MaxLength="4" Visible="False"></SPS:SPSTextBox>
                                                                <asp:Label ID="lblhyphen" runat="server" Visible="False" CssClass="tableData">-</asp:Label>
                                                                <SPS:SPSTextBox ID="txtBudgetCode2" runat="server" CssClass="tableData" Width="100px" MaxLength="7" Visible="False"></SPS:SPSTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="tableData" width="92">
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="tableData" width="92">
                                                            </td>
                                                            <td class="tableData" style="width: 580px">
                                                                <table id="Table13" width="210" border="0" role="presentation">
                                                                    <tr>
                                                                        <td>
                                                                            &nbsp;&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="btnNext" runat="server" AlternateText="Next" ImageUrl="images/sp_int_next_btn.gif"></asp:ImageButton><asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancel" ImageUrl="images/sp_int_cancel_btn.gif"></asp:ImageButton>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20" style="height: 746px">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                            <img src="images/spacer.gif" width="25" height="20" alt="">
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>
    </form>
</body>
</html>
