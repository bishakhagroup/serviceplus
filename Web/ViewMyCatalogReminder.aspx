<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ViewMyCatalogReminder" EnableViewStateMac="true" CodeFile="ViewMyCatalogReminder.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - My Catalog Reminder</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form name="Form1" method="post" runat="server">
        <table width="100%" border="0" role="presentation">
            <tr>
                <td>
                    <img src="images/sp_popup_MyCatRemind_hdrbkgd.gif" alt="My Catalog Reminder">
                </td>
            </tr>
            <tr>
                <td>
                    <img src="images/spacer.gif" width="4" height="10" alt=""><br>
                    <table width="325" border="0" role="presentation">
                        <tr>
                            <td width="20" height="12">
                                <img src="images/spacer.gif" width="20" height="4" alt="">
                            </td>
                            <td width="142" height="12" class="popupCopyBold">
                                <img src="images/sp_int_startDate_label.gif" width="122" height="12" alt="Start Date">
                            </td>
                            <td width="5" height="12">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="141" height="12">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                        </tr>
                        <tr>
                            <td width="20" valign="top">
                                <img src="images/spacer.gif" width="20" height="4" alt="">
                            </td>
                            <td width="142" valign="top">
                                <SPS:SPSTextBox ID="ReminderDate" runat="server"></SPS:SPSTextBox>
                            </td>
                            <td width="5" valign="top">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="141">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                                <asp:Label ID="messageLabel" runat="server" Width="136px" CssClass="tableData" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td height="12" width="20">
                                <img src="images/spacer.gif" width="20" height="4" alt="">
                            </td>
                            <td height="12" width="142" class="popupCopyBold">
                                <img src="<%=Resources.Resource.sna_svc_img_24()%>" width="87" height="12" alt="Recurrence Pattern">
                            </td>
                            <td height="12" width="5">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td height="12" width="141">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                        </tr>
                        <tr>
                            <td width="20" class="tableHeader">
                                <img src="images/spacer.gif" width="20" height="4" alt="">
                            </td>
                            <td width="142" class="popupCopy">
                                <asp:DropDownList ID="RecurrancePatternDDL" runat="server" AutoPostBack="True"></asp:DropDownList>
                            </td>
                            <td width="5" class="tableHeader">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="141" class="popupRedPrice">
                                <asp:DropDownList ID="SecondaryRecurrancePatternDDL" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="20">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="142">
                                <asp:ImageButton ID="AddReminder" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_22%>" AlternateText="Add Reminder"></asp:ImageButton>
                            </td>
                            <td width="5">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="141" align="right">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                        </tr>
                        <tr>
                            <td width="20">
                                <img src="images/spacer.gif" width="20" height="4" alt="">
                            </td>
                            <td width="142">
                                <a href="#" onclick="javascript:window.close();">
                                    <img src="<%=Resources.Resource.img_btnCloseWindow() %>" border="0" alt="Close"></a>&nbsp;
                            </td>
                            <td width="5">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                            <td width="141" align="right">
                                <img src="images/spacer.gif" width="1" height="8" alt="">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
