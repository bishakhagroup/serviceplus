<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_technical_bulletin"
    CodeFile="sony-technical-bulletin.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=pagetitle%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Technical Bulletins"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"> 
    </script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="frmTechBulletinSearch" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_subhd_technical_bulletins.jpg"
                                                bgcolor="#363d45" height="82">
                                                <br />
                                                <h1 class="headerText">Knowledge Base</h1>
                                                &nbsp;&nbsp;
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="466" bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Technical Bulletin</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="errorMessageLabel" runat="server"
                                                    CssClass="tableData" Width="641px" ForeColor="Red" EnableViewState="False"></asp:Label><br />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr width="100%">
                                <td width="100%">
                                    <iframe width="709" src="<%=filename%>" height="880"></iframe>

                                </td>
                            </tr>
                            <tr>
                                <td>

                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                        <ProgressTemplate>
                                            <img height="10" src="images/spacer.gif" width="10" alt=""><span id="Span4" class="bodycopy">Please wait...</span>
                                            <img src="images/progbar.gif" width="100"  alt="Please Wait"/>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" align="left">

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <table width="100%" border="0" align="left" role="presentation">
                                                <tr>
                                                    <td colspan="2" width="100%">
                                                        <img src="images/spacer.gif" width="10" height="20" alt="">
                                                        <asp:PlaceHolder ID="feedbackPlaceHolder" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" width="100%">
                                                        <img src="images/spacer.gif" width="10" height="20" alt="">
                                                        <SPS:SPSTextBox Visible="false" Width="60%" Height="60" ID="txtSuggestion" TextMode="MultiLine"
                                                            CssClass="tableData" runat="server" MaxLength="2000" />
                                                        <img width="20" src="images/spacer.gif" alt="">
                                                        <asp:ImageButton Visible="false" ID="btnSubmit" runat="server" AlternateText="Submit Suggestions"
                                                            ImageUrl="<%=Resources.Resource.img_btnSubmit()%>" />
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="feedbackTr">
                                                    <td align="left" width="35%">
                                                        <img src="images/spacer.gif" width="10" alt=""><span class="tableHeader">Was this 
                                                    information helpful to you?</span>
                                                        <img src="images/spacer.gif" width="10" alt="">
                                                    </td>
                                                    <td align="left" valign="bottom">
                                                        <asp:ImageButton ID="imgYes" runat="server" ImageUrl="<%=Resources.Resource.img_btnYes()%>"
                                                            AlternateText="<%=Resources.Resource.el_yes()%>" />
                                                        <img src="images/spacer.gif" width="5" alt="">
                                                        <asp:ImageButton ID="ImgNo" runat="server" ImageUrl="<%=Resources.Resource.img_btnNo()%>"
                                                            AlternateText="<%=Resources.Resource.el_no()%>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
