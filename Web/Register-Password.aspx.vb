Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports System.Text
Imports System.Text.RegularExpressions
Imports ServicesPlusException
Imports Sony.US.siam.Administration
Imports Sony.US.AuditLog


Namespace ServicePLUSWebApp



    Partial Class Register_Password
        Inherits SSL

        Dim customer As New Customer
        Dim objAdmin As New SecurityAdministrator
        Dim isVerified As Boolean
        Public LanguageSW As String = "ER"
        Public focusFormField As String
        Public maxPwdLength As Int16 = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount)
        Public minPwdLength As Int16 = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength)
        'Dim objUtilties As Utilities = New Utilities
        Dim isError As Boolean
        Dim userEmail As String = String.Empty

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
#End Region

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            Password.MaxLength = maxPwdLength
            cPassword.MaxLength = maxPwdLength
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session.Item("newCustomer") IsNot Nothing Then
                    customer = Session.Item("newCustomer")
                    userEmail = customer.EmailAddress
                    lblemail.Text = userEmail
                    lblemail1.Text = userEmail
                ElseIf Session.Item("ExistingCustomer") IsNot Nothing Then
                    customer = Session.Item("ExistingCustomer")
                    userEmail = customer.EmailAddress
                    lblemail.Text = userEmail
                    lblemail1.Text = userEmail
                End If

                If Not Page.IsPostBack Then
                    ControlMethod(sender, e)
                End If
                If Session("Language_Changed") IsNot Nothing And Session("Language_Changed") = "1" Then
                    fnSetSession()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                isError = True
            End Try
        End Sub

        Private Sub NextButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles NextButton.Click
            Try
                ControlMethod(sender, e)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                isError = True
            End Try
        End Sub

        Private Sub ControlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim caller As String = String.Empty
            Dim custMan As New CustomerManager
            Dim EmailBody As New StringBuilder(5000)
            Dim Subject As String = String.Empty

            Try
                If sender.ID IsNot Nothing Then caller = sender.ID.ToString()
                If caller = NextButton.ID.ToString() Then
                    If DataValidate() Then
                        processRegistrationForm()
                        isError = AddUsertoLDAP() 'commented by satheesh for siteminder issue.
                        If Not isError Then
                            customer = Session.Item("newCustomer")
                            Dim customerId As Int64 = custMan.GetINACTIVECUSTOMERDATA(customer.EmailAddress.Trim().ToUpper())
                            If customerId > 0 Then custMan.DeleteCustomerRecord(customerId)

                            ' 2018-05-04 - Added If to check if the user is retrying the registration.
                            If custMan.GetCustomer(customer.LdapID, customer.EmailAddress, customer.FirstName, customer.LastName) Is Nothing Then
                                custMan.RegisterCustomer(customer)   ' ASleight - This is where Registrations fail on multiple clicks or if a timeout happens. We need to check for DB User again.

                                Dim cm_email As New TempCustomerValues With {
                                    .EmailAddress = customer.EmailAddress,
                                    .IsEmailVerified = Convert.ToInt32(If(Session("Isverified")?.ToString(), "0"))
                                }

                                Dim customer_email As New TempCustomer()
                                customer_email.UpdateCustomer(cm_email)

                                BuildMailBody(EmailBody, Subject)
                                custMan.SendRegisterWelcomeEmail(customer.EmailAddress, EmailBody.ToString(), Subject) 'Enhanc 
                            End If

                            If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                                Response.Redirect($"SignIn.aspx?Lang={HttpContextManager.GlobalData.LanguageForIDP}&post=taxaccount&SessionID={Request.QueryString("SessionID")}", False)
                            Else
                                Response.Redirect($"SignIn.aspx?Lang={HttpContextManager.GlobalData.LanguageForIDP}&post=NewUser", False)
                            End If
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                isError = True
                Return
            End Try
        End Sub

        Private Function AddUsertoLDAP() As Boolean
            Dim pciLog As New PCILogger() '6524
            Dim theUser As New User
            Dim objLdap As New SecurityAdministrator
            Dim sErrorMessage As String = String.Empty
            Dim sLapdID As String = String.Empty
            Dim userData As String = String.Empty

            Try
                If Session.Item("newCustomer") IsNot Nothing Then
                    customer = Session.Item("newCustomer")

                    pciLog.EventOriginApplication = "ServicesPLUS"
                    pciLog.EventOriginApplicationLocation = Me.Page.GetType().Name
                    pciLog.EventOriginMethod = "addUsertoLDAP()"
                    pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    pciLog.OperationalUser = Environment.UserName
                    pciLog.EmailAddress = customer.EmailAddress
                    pciLog.EventType = EventType.Add
                    pciLog.EventDateTime = DateTime.Now.ToLongTimeString()
                    pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
                    pciLog.IndicationSuccessFailure = "Success"
                    pciLog.Message = "Add User to LDAP Success."

                    theUser.FirstName = customer.FirstName
                    theUser.LastName = customer.LastName
                    theUser.CompanyName = customer.CompanyName
                    theUser.EmailAddress = customer.EmailAddress
                    theUser.Fax = customer.FaxNumber
                    theUser.Phone = customer.PhoneNumber
                    theUser.AddressLine1 = customer.Address.Address1
                    theUser.AddressLine2 = customer.Address.Address2
                    theUser.City = customer.Address.City
                    theUser.State = customer.Address.State
                    theUser.Country = customer.CountryName
                    theUser.Zip = customer.Address.Zip
                    theUser.Password = Password.Text.Trim()
                    theUser.UserId = customer.EmailAddress
                    theUser.Country = customer.CountryName

                    Try
                        ' Let's check again if the user is in LDAP, in case they click twice.
                        userData = objAdmin.SearchUser(theUser.EmailAddress)
                        If (String.IsNullOrWhiteSpace(userData)) Then
                            ' Add the user to LDAP
                            sLapdID = objAdmin.AddUser(theUser, sErrorMessage)
                            customer.LdapID = sLapdID
                            pciLog.LDAP_ID = sLapdID
                            pciLog.SIAM_ID = customer.SIAMIdentity
                        Else
                            Utilities.LogMessages($"Register-Password.addUsertoLDAP - Existing user found, trying to recover data. userData = {userData}")
                            Dim newUserData = userData.Split("*"c)
                            customer.FirstName = newUserData(0)
                            customer.LastName = newUserData(1)
                            customer.CompanyName = newUserData(2)
                            customer.LdapID = newUserData(3)
                            customer.EmailAddress = theUser.EmailAddress
                            sLapdID = customer.LdapID
                            pciLog.LDAP_ID = sLapdID
                        End If
                    Catch ex As Exception
                        Dim erEmail = IIf(theUser IsNot Nothing, theUser.EmailAddress, "null")
                        Utilities.LogMessages($"Register-Password.addUsertoLDAP FAILURE - User Email: {erEmail}, Message: {sErrorMessage}")
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        'Dim er As String
                        'er = ErrorLabel.Text.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace)
                        'isError = True
                        Return True
                    End Try
                    Session.Item("newCustomer") = customer  ' ASleight - We need to store this again, don't we?
                    Session("testemail") = customer.EmailAddress
                    Session("testldapid") = customer.LdapID
                    If String.IsNullOrEmpty(sLapdID) Then
                        Throw New Exception(Resources.Resource.el_Err_LDAPID) '"LDAPID is Empty while trying to Add User to IdentityMinder")
                    Else
                        Return False
                    End If
                End If

            Catch ex As Exception
                pciLog.IndicationSuccessFailure = "Failure"
                pciLog.Message = "Add User to LDAP Failed. " & ex.Message.ToString()

                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Dim er As String
                'Replace(ErrorLabel.Text, "Unexpected error encountered. For assistance, please", "For registration call to IdentityMinder: Operation to register timed out. Please try again or")
                er = ErrorLabel.Text.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace) '"Operation to register timed out. Please try again or")
                ErrorLabel.Text = er
                isError = True
                Return True
            Finally
                pciLog.PushLogToMSMQ() '6524
            End Try
        End Function

        Private Sub processRegistrationForm()
            If ErrorValidation.Visible Then
                ' ErrorValidation.Text = "Please complete the required fields."
            Else
                Try
                    customer.Password = Password.Text.Trim()
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    isError = True
                End Try
            End If
        End Sub

        Private Function IsPasswordValid() As Boolean
            Dim IsValid As Boolean = True
            Dim minLen As Integer = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength)
            Dim maxLen As Integer = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthAccount
            Dim isNotAlphaNumeric As New Regex("((?<=(\w*\d+))(?=([a-zA-Z]+\w*)))|((?<=(\w*[a-zA-Z]+))(?=(\d+\w*)))") '("\w*[a-zA-Z]+\w*\d+\w*")
            Dim isSpecialChar As New Regex("\W+")

            If userEmail = Password.Text.Trim() Then
                IsValid = False
                ErrorPasswordNew.Text = Resources.Resource.el_rgn_validate_email '"Your email address and password cannot be the same." '6771
            ElseIf Password.Text.Length < minLen Or Password.Text.Length > maxLen Then
                IsValid = False
                '<Satyam>Show dynamic password length restriction message
                ErrorPasswordNew.Text = Resources.Resource.el_Validate_pswd1 '"Your password must be " + minLen.ToString() + " to " + maxLen.ToString() + " characters long." '6771
                '<Satyam>
            ElseIf Not isNotAlphaNumeric.IsMatch(Password.Text.Trim()) Then
                IsValid = False
                'ErrorPasswordNew.Text = "Your password must contain both alphabetic and numeric characters." '6771
            ElseIf isSpecialChar.IsMatch(Password.Text.Trim()) Then
                IsValid = False
                ErrorPasswordNew.Text = Resources.Resource.el_Validate_pswd2 ' "Your password must not contain special characters." '6771
            End If

            Return IsValid
        End Function

        Private Function DataValidate() As Boolean
            Dim isValid As Boolean = True
            Dim message As String = String.Empty

            ErrorPassword.Text = ""
            ErrorPasswordNew.Text = ""
            ErrorValidation.Text = ""
            If objAdmin.IsValidPassword(Password.Text.Trim(), "P") = False Then
                ErrorValidation.Visible = True
                LabelCPWD.CssClass = "redAsterick"
                ErrorPassword.Text = "Invalid password. Please review the password requirements." 'Resources.Resource.el_Validate_pswd3 '"Passwords do not match."
                isValid = False
            End If
            If objAdmin.ValidateB2Bpassword(Password.Text.Trim(), userEmail, message) = False Then
                ErrorValidation.Visible = True
                ErrorPassword.Text = message
                isValid = False
            End If
            If IsPasswordValid() = False Then
                ErrorValidation.Visible = True
                LabelCPWD.CssClass = "redAsterick"
                isValid = False
            End If
            If Password.Text <> cPassword.Text Then
                ErrorValidation.Visible = True
                ErrorPassword.Text = Resources.Resource.el_Validate_pswd3 ' "Passwords do not match."
                LabelPWD.CssClass = "redAsterick" '6920
                LabelCPWD.CssClass = "redAsterick" '6920
                isValid = False
            End If
            If Password.Text Is "" Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_validate_RequireWarning '"Please enter required field." '6920
                LabelPWD.CssClass = "redAsterick"
                isValid = False
            End If
            If cPassword.Text Is "" Then
                ErrorValidation.Visible = True
                ErrorValidation.Text = Resources.Resource.el_validate_RequireWarning '"Please enter required field." '6920
                LabelCPWD.CssClass = "redAsterick"
                isValid = False
            End If

            Return isValid
        End Function

        Public Sub fnGetSession()
            Dim ObjCst As New Customer
            ObjCst.Password = Password.Text.Trim() + "�" + cPassword.Text.Trim()
            Session.Add("CultureData", ObjCst)
        End Sub

        Public Sub fnSetSession()
            Try
                If Not Session("CultureData") Is Nothing Then

                    Dim ObjCst As New Customer
                    ObjCst = Session.Item("CultureData")
                    Dim strPswd() As String = ObjCst.Password.Split("�")
                    Password.Text = strPswd(0)
                    cPassword.Text = strPswd(1)
                    Session.Remove("CultureData")
                    Session("Language_Changed") = "0"
                End If
            Catch

            End Try
        End Sub

        Sub BuildMailBody(ByRef Emailbody As StringBuilder, ByRef Subject As String)
            customer = Session.Item("newCustomer")

            'Dim objGlobalData As New GlobalData
            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If

            'EmailBody.Append("Thank you for registering with Sony's ServicesPLUS(sm) system for support of your ")
            'EmailBody.Append("Sony professional products. Please bookmark the following link for future ")
            'EmailBody.Append("access to Sony Professional Services: " + vbCrLf + vbCrLf)
            'EmailBody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)

            'If (customer.UserType = "P") Then
            '    EmailBody.Append("Please allow five (5) business days for us to verify your account information. ")
            '    EmailBody.Append("You currently have access to order on a credit card." + vbCrLf + vbCrLf)
            '    EmailBody.Append("If you have submitted a tax exemption request we will verify the exemption ")
            '    EmailBody.Append("status within five (5) business days. " + vbCrLf + vbCrLf)
            'End If
            'EmailBody.Append("If you have any questions, please contact our customer service group at 1-800-538-7550." + vbCrLf + vbCrLf)

            'EmailBody.Append("Sincerely," + vbCrLf)
            'EmailBody.Append("The ServicesPLUS(sm) Team" + vbCrLf + vbCrLf)
            'EmailBody.Append("Copyright 2011 Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
            'EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
            'EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
            'EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)

            'Subject = "ServicesPLUS(sm) - Thank you for registering"

            Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
            Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
            Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
            Dim terms_and_conditions_link_ca As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link_ca
            Dim contact_us_link_ca As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link_ca

            Emailbody.Append(Resources.Resource_mail.ml_common_Dear_en + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line1_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line2_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line3_en)
            Emailbody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)

            If (customer.UserType = "P") Then
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line4_en)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line5_en + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line6_en)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line7_en + vbCrLf + vbCrLf)
            End If
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line8_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_Sincerely_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_en + vbCrLf + vbCrLf)

            If HttpContextManager.GlobalData.IsAmerica Then
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link + vbCrLf)
            Else
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link_ca + vbCrLf)
            End If

            Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_en + privacy_policy_link + vbCrLf)


            If HttpContextManager.GlobalData.IsCanada Then
                Emailbody.Append("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + vbCrLf + vbCrLf)

                Emailbody.Append(Resources.Resource_mail.ml_common_Dear_fr + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line1_fr)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line2_fr)
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line3_fr)
                Emailbody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)

                If (customer.UserType = "P") Then
                    Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line4_fr)
                    Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line5_fr + vbCrLf + vbCrLf)
                    Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line6_fr)
                    Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line7_fr + vbCrLf + vbCrLf)
                End If
                Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line8_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_sincerely_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_fr + contact_us_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_fr + terms_and_conditions_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_fr + privacy_policy_link + vbCrLf)

                Subject = Resources.Resource_mail.ml_newuserconfirm_subject_en + "/" + Resources.Resource_mail.ml_newuserconfirm_subject_fr
            Else
                Subject = Resources.Resource_mail.ml_newuserconfirm_subject_en
            End If
        End Sub

    End Class

End Namespace
