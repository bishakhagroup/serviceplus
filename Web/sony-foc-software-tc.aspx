﻿<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.SoftwareTC"
    EnableViewStateMac="true" CodeFile="sony-foc-software-tc.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Reference Page="~/SSL.aspx" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS:Sony Software for Professional Products - Terms and Conditions
    </title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function checkSession() {
            if (document.getElementById("txtHidden").value === "load") {
                document.getElementById("txtHidden").value = "done";
            } else {
                location.href = "SessionExpired.aspx";
            }
        }

        function checkLogin(itemN) {
            if (document.getElementById('chkRead').checked === true) {
                window.location.href = 'SignIn.aspx?Lang=' + '<%=LanguageForIDP%>' + '&PartNumber=' + '<%=sKitPartNumber%>';
            } else {
                var newURL = "sony-foc-software-tc.aspx?ln=" + itemN + "&readTC=false"
                window.location.href = newURL
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="javascript:checkSession();">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                bgcolor="#363d45" height="57">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_TermsConditions%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="txtHidden" type="hidden" value="load">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" border="0" role="presentation">
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <% If isEnglish Then %>
                                                <span class="bodyCopyBold">SOFTWARE</span>
                                                <% Else%>
                                                <span class="bodyCopyBold">LOGICIEL</span>
                                                <% End If%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td class="bodyCopy">
                                                <% If isEnglish Then %>
                                                <p class="bodyCopyBold">Software License Agreement: </p>
                                                <p>
                                                    <span class="bodyCopyBold">IMPORTANT - READ CAREFULLY:</span>
                                                    This End-User License Agreement ("License") is a legal agreement between you and
                                                Sony Corporation ("SONY"), the manufacturer of your SOFTWARE. The SOFTWARE includes
                                                computer software, and any associated media and printed materials. You may use the
                                                SOFTWARE only in connection with the use of the personal computer (hereinafter referred
                                                to as SYSTEM). By installing, copying or otherwise using the SOFTWARE, you agree
                                                to be bound by the terms of this License. If you do not agree to the terms of this
                                                License, SONY is unwilling to license the SOFTWARE to you. In such event, you may
                                                not use or copy the SOFTWARE, and you should promptly contact SONY for instructions
                                                on return of the unused product for a refund of the purchase price of the SOFTWARE.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">SOFTWARE LICENSE: </span>
                                                    The SOFTWARE is protected by copyright laws and international copyright treaties,
                                                 as well as other intellectual property laws and treaties. The SOFTWARE is licensed,
                                                 not sold.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">GRANT OF LICENSE: </span>
                                                    This License grants you the following rights:<br />
                                                    - Software. You may install and use one (1) copy of the SOFTWARE.<br />
                                                    - Back-up Copy. You may use a back-up copy solely for archival purposes.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">MPEG-4 VISUAL PATENT PORTOFOLIO LICENSE: </span>
                                                    MPEG-4 Encoding products are licensed under the MPEG-4 Visual Patent Portfolio License(i)
                                                for the personal and non-commercial use of a consumer for (i) encoding video in
                                                compliance with the MPEG-4 visual standard ("MPEG-4 Video") and/or (ii) decoding
                                                MPEG-4 video that was encoded by a consumer engaged in a personal and non-commercial
                                                activity and/or was obtained from a video provider licensed by MPEG LA to provide
                                                MPEG-4 video. No license will be granted or shall be implied for any other use.
                                                Additional information including that relating to promotional, internal and commercial
                                                uses and licensing may be obtained from MPEG LA, LLC. See http://www.mpegla.com
                                                for more information.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">DESCRIPTION OF OTHER RIGHTS LIMITATIONS</span><br />
                                                    - Limitation on Reverse Engineering, Decompilation and Disassembly. You may not
                                                modify, reverse engineer, decompile, or disassemble the SOFTWARE in whole or in
                                                part.<br />
                                                    - Single SYSTEM. The SOFTWARE is licensed with the SYSTEM as a single integrated
                                                product. The SOFTWARE may only be used with the SYSTEM specified by SONY.<br />
                                                    - Rental. You may not rent or lease the SOFTWARE.<br />
                                                    - Software Transfer. You may transfer all of your rights under this License only
                                                as part of a sale or transfer of the SYSTEM specified by SONY, provided you retain
                                                no copies, transfer all of the SOFTWARE (including all copies, component parts,
                                                the media and printed materials, all versions and any upgrades of the SOFTWARE and
                                                this License), and the recipient agrees to the terms of this License.<br />
                                                    - Termination. Without prejudice to any other rights, SONY may terminate this License
                                                if you fail to comply with the terms and conditions of this License. In such event,
                                                you must destroy all copies of the SOFTWARE and all of its component parts.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">COPYRIGHT: </span>
                                                    All title and copyrights in and to the SOFTWARE (including but not limited
                                                to any images, photographs, animation, video, audio, music, text and "applets",
                                                incorporated into the SOFTWARE), and any copies of the SOFTWARE, are owned by SONY
                                                or its suppliers. All rights not specifically granted under this License are reserved
                                                by SONY.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">PRODUCT SUPPORT: </span>
                                                    Product support for the SOFTWARE is not provided by SONY.
                                            <p>
                                                <span class="bodyCopyBold">LIMITED WARRANTY FOR CD-ROM MEDIA: </span>
                                                SONY warrants that for a period of ninety (90) days from the date of its delivery
                                                to you the CD-ROM media on which the copy of the SOFTWARE is furnished to you will
                                                be free from defects in materials and workmanship under normal use. This limited
                                                warranty extends only to you as the original licensee. SONY's entire liability and
                                                your exclusive remedy will be replacement of the CD-ROM media not meeting SONY's
                                                limited warranty and which is returned to SONY with proof of purchase in the form
                                                of a bill of sale (which is evidence that the CD-ROM media is within the warranty
                                                period). SONY will have no responsibility to replace a disk damaged by accident,
                                                abuse or misapplication.
                                            <p>
                                                <span class="bodyCopyBold">EXCLUSION OF WARRANTY ON SONY SOFTWARE: </span>
                                                You expressly acknowledge and agree that use of the SOFTWARE is at your sole
                                                risk. The SOFTWARE is provided "AS IS" and without warranty of any kind and SONY
                                                and its representative (hereinafter collectively referred to as SONY) EXPRESSLY
                                                DISCLAIM ALLWARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
                                                WARRANTIES OF MERCHANTABILITY FITNESS FOR A PARTICULAR PURPOSE. SONY DOES NOT WARRANT
                                                THAT THE FUNCTIONS CONTAINED IN THE SOFTWARE WILL MEET YOUR REQUIREMENTS, OR THAT
                                                THE OPERATION OF THE SOFTWARE WILL BE CORRECTED. FURTHERMORE, SONY DOES NOT WARRANT
                                                OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE SOFTWARE
                                                IN TERMS OF ITS CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. NO ORAL OR WRITTEN
                                                INFORMATION OR ADVICE GIVEN BY SONY SHALL CREATE A WARRANTY OR IN ANY WAY INCREASE
                                                THE SCOPE OF THIS WARRANTY. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU (NOT SONY)
                                                ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
                                            <p>
                                                <span class="bodyCopyBold">LIMITATION OF LIABILITY: </span>
                                                SONY SHALL NOT BE LIABLE FOR ANY INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR BREACH OF ANY
                                                 EXPRESS OR IMPLIED WARRANTY, BREACH OF CONTRACT, NEGLIGENCE, STRICT LIABILITY OR ANY
                                                 OTHER LEGAL THEORY RELATED TO THIS PRODUCT. SUCH DAMAGES INCLUDE, BUT ARE NOT LIMITED
                                                 TO, LOSS OF PROFITS, LOSS OF REVENUE, LOSS OF DATA, LOSS OF USE OF THE PRODUCT OR ANY
                                                 ASSOCIATED EQUIPMENT, DOWN TIME AND PURCHASER'S TIME, EVEN IF SONY HAS BEEN ADVISED OF
                                                 THE POSSIBILITY OF SUCH DAMAGES. IN ANY CASE, SONY'S ENTIRE LIABILITY UNDER ANY PROVISION
                                                 OF THIS AGREEMENT SHALL BE LIMITED TO THE AMOUNT ACTUALLY PAID BY YOU ALLOCABLE TO THE SOFTWARE.
                                            </p>
                                                <% Else%>
                                                <p class="bodyCopyBold">Licence d'Utilisation du Logiciel: </p>
                                                <p>
                                                    <span class="bodyCopyBold">IMPORTANT - À LIRE ATTENTIVEMENT: </span>
                                                    Cette licence d'utilisation destiné à l'utilisateur final (ci-après, la ''licence'') constitue
                                                 un accord juridique entre vous et Sony Corporation (ci-après, ''SONY''), le fabricant du LOGICIEL.
                                                 Par LOGICIEL, nous entendons les divers programmes informatiques intégrés à l'ordinateur, ainsi
                                                 que tout les fichiers de type média et le  matériel imprimé. Vous être autorisé à utiliser le
                                                 LOGICIEL exclusivement dans le cadre de l'utilisation de l'ordinateur personnel (ci-après, le
                                                 ''SYSTÈME''). En installant, copiant ou  utilisant le LOGICIEL, vous acceptez d'être lié par les
                                                 conditions de la présente licence d'utilisation du logiciel. Si vous refusez une ou plusieurs des
                                                 conditions énoncées dans ce document, SONY refusera de vous accorder une licence d'utilisation.
                                                 Dans un tel cas, vous ne pouvez pas utiliser ou copier le LOGICIEL, et vous devez communiquer avec
                                                 SONY dans les meilleurs délais pour obtenir des instructions sur le retour du produit non utilisé,
                                                 et le remboursement du prix d'achat payé pour le LOGICIEL.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">LICENCE DU LOGICIEL: </span>
                                                    Le LOGICIEL est protégé par des droits d'auteur et les traités internationaux relatifs aux droits
                                                d'auteur, ainsi que par d'autres lois et ententes relatives à la propriété intellectuelle. Seule
                                                l'utilisation du LOGICIEL est permise. Sa vente est interdite.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">OCTROI DE LICENCE: </span>
                                                    La présente licence vous accorde les droits suivants:
                                                    <br />
                                                    - Logiciel. Vous pouvez installer et utiliser une (1) copie du LOGICIEL.
                                                    <br />
                                                    - Copie de sauvegarde. Vous pouvez conserver une copie de sauvegarde uniquement à des fins d'archivage.

                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">LICENCE PORTFOLIO DE BREVET MPEG-4: </span>
                                                    Les produits d'encodage MPEG-4 sont utilisés sous la licence ''Visual Patent Portfolio MPEG-4'':
                                                 (i) pour l'usage personnel, et non commercial, d'un consommateur pour (i) le codage vidéo conformément
                                                 au standard visuel MPEG-4 (ci-après, le "Vidéo MPEG-4") et / ou (ii) le décodage d'une vidéo MPEG-4
                                                 déjà encodée par un autre consommateur, réalisant une activité personnelle et non commerciale
                                                 et / ou obtenue auprès d'un fournisseur de vidéos agréé par MPEG LA pour fournir une cette vidéo
                                                 encodée en MPEG-4. Aucune licence ne sera accordée ou ne pourra être présumée avoir été accordée pour
                                                 toute autre utilisation. Des renseignements supplémentaires, incluant des droits d'utilisation
                                                 promotionnelles, internes et commerciales, ainsi qu'à l'octroi de licences et ses conditions, peuvent
                                                 être obtenues en vous adressant à MPEG LA, LLC. Pour obtenir plus d'informations, consultez http://www.mpegla.com.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">DÉTAILS SUR LES AUTRES DROITS LIMITÉS</span><br />
                                                    - Limites concernant la rétroingénierie, la décompilation et le démontage. Il vous est interdit de
                                                 modifier, faire de la rétroingénierie, de décompiler ou de démonter le LOGICIEL, en totalité ou en partie.<br />
                                                    - SYSTÈME unique. Ce LOGICIEL est conçu et autorisé pour une utilisation combinée au SYSTÈME, en
                                                 tant que produit intégré unique. Ce LOGICIEL doit être utilisé uniquement avec le SYSTÈME déterminé par SONY.<br />
                                                    - Location. Vous ne pouvez pas sous-louer ce LOGICIEL.<br />
                                                    - Transfert de licence du logiciel. Vous pouvez transférer tous vos droits en vertu de cette licence
                                                 uniquement dans le cadre d'une vente ou d'un transfert de SYSTÈME déterminé par SONY, à condition que vous
                                                 ne conserviez aucune copie, et que vous transfériez tout le LOGICIEL (y compris toutes les copies, pièces
                                                 composantes, médias et contenus imprimés, toutes les versions précédentes et toutes les mises à niveau du
                                                 LOGICIEL et la présente licence), et que le destinataire acquiesce à chacune des conditions de cette licence.<br />
                                                    - Résiliation. Sans préjudice à tout autre droit, SONY peut mettre fin à cette licence si vous ne respectez
                                                 pas les termes ou conditions. En tel cas, vous devez détruire toutes les copies du LOGICIEL et les parties
                                                 qui le composent.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">DROITS D'AUTEUR: </span>
                                                    Tout titre et droits d'auteur compris et envers l'utilisation du LOGICIEL (incluant mais sans s'y limiter,
                                                 les images, les photographies, l'animation, la vidéo, l'audio, la musique, le texte et les ''appliquettes'',
                                                 incluses dans le LOGICIEL) et toutes les copies du LOGICIEL sont réputés appartenir à SONY ou à ses
                                                 fournisseurs. Tout droit non spécifiquement accordé en vertu de cette licence est une propriété de SONY.
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold">ENTRETIEN DU PRODUIT: </span>
                                                    L'entretien du LOGICIEL lié au produit n'est pas fourni par SONY.
                                            <p>
                                                <span class="bodyCopyBold">GARANTIE LIMITÉE POUR LES DISQUES CÉDÉROM: </span>
                                                SONY garantit que, pendant une période de quatre-vingt-dix (90) jours à compter de la date de livraison du
                                                 cédérom sur lequel est enregistré le LOGICIEL, qu'il sera exempt de tout vice matériel ou de main-d'œuvre,
                                                 lors d'une utilisation normale. Cette garantie limitée ne s'applique qu'à vous en tant que titulaire
                                                 initial de la licence. La seule responsabilité de SONY, ainsi que votre seul recours, sera le remplacement
                                                 du disque cédérom qui ne satisfait pas à la garantie limitée de SONY, à condition qu'il soit retourné à
                                                 SONY avec une preuve d'achat, sous la forme d'un reçu de vente (ce qui servira de preuve que le disque
                                                 cédérom est encore couvert par la garantie). SONY ne pourra être tenue responsable si le disque a été
                                                 endommagé suite à un accident, un abus ou une manipulation inadéquate.
                                            <p>
                                                <span class="bodyCopyBold">EXCLUSIONS DE LA GARANTIE SUR LE LOGICIEL SONY: </span>
                                                Vous reconnaissez et acceptez expressément que l'utilisation du LOGICIEL est à votre entière
                                                 responsabilité. Le LOGICIEL vous est fourni ''TEL QUEL'' et sans garantie de quelque nature que ce
                                                 soit par SONY et son personnel (ci-après, collectivement "SONY") ET QU'ILS DÉCLINENT EXPRESSÉMENT
                                                 TOUTES RESPONSABILITÉS, EXPRESSES OU IMPLICITES, Y COMPRIS MAIS SANS S'Y LIMITER, LES GARANTIES
                                                 IMPLICITES D'ADAPTATION DE LA QUALITÉ MARCHANDE PAR UN PARTICULIER. SONY NE GARANTIT PAS QUE LES
                                                 FONCTIONNALITÉS OFFERTES PAR LE LOGICIEL RÉPONDRONT À VOS DEMANDES OU QUE LE FONCTIONNEMENT DU
                                                 LOGICIEL SERA MODIFIÉ. EN OUTRE, SONY NE GARANTIT, NI NE FAIT AUCUNE REPRÉSENTATION CONCERNANT
                                                 L'UTILISATION, OU LES RÉSULTATS DU LOGICIEL EN MATIÈRE D'EXACTITUDE, DE FIABILITÉ OU DE TOUTE
                                                 AUTRE FACTEUR. AUCUN RENSEIGNEMENT FOURNI ORALEMENT OU PAR ÉCRIT PAR SONY NE POURRA MODIFIER LA
                                                 PRÉSENTE GARANTIE ET AUCUN MANQUEMENT NE POURRAIT MODIFIER LE CHAMP D'APPLICATION DE CETTE GARANTIE.
                                                 SI LE LOGICIEL DEVENAIT RECONNU COMME ÉTANT DÉFECTUEUX, VOUS (ET NON SONY) DEVREZ ASSUMER TOUS LES
                                                 FRAIS REQUIS POUR LE RÉPARER, LE METTRE À NIVEAU OU LE CORRIGER.
                                            <p>
                                                <span class="bodyCopyBold">RESPONSABILITÉ LIMITÉE: </span>
                                                SONY NE POURRA PAS ÊTRE TENUE RESPONSABLE DE TOUT DOMMAGE DIRECT, INDIRECT OU CONSÉCUTIF SUITE AU
                                                 MANQUEMENT À TOUTE GARANTIE EXPRESSE OU IMPLICITE, VIOLATION DE CONTRAT, NÉGLIGENCE, RESPONSABILITÉ
                                                 STRICTE OU TOUTE AUTRE THÉORIE JURIDIQUE RELATIVE À CE PRODUIT. DE TELS DOMMAGES INCLUENT MAIS SANS
                                                 S'Y LIMITER, TOUTE PERTE DE BÉNÉFICES, TOUTE PERTE DE REVENUS, TOUTE PERTE DE DONNÉES INFORMATIQUES,
                                                 TOUTE INCAPACITÉ D'UTILISATION DU PRODUIT OU DU MATÉRIEL ASSOCIÉ, ET LE TEMPS DE REDÉMARRAGE ET LE
                                                 TEMPS DE L'ACHETEUR, MÊME SI SONY AVAIT ÉTÉ AVISÉE DE LA POSSIBILITÉ DE TELS DÉGÂTS. EN CONCLUSION,
                                                 LA RESPONSABILITÉ INTÉGRALE DE SONY EN VERTU DE TOUTES LES DISPOSITIONS DU PRÉSENT CONTRAT SE LIMITE
                                                 AU MONTANT QUE VOUS AVEZ PAYÉ POUR L'OBTENTION D'UNE LICENCE D'UTILISATION DE CE LOGICIEL.
                                            </p>
                                                <% End If%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="bodyCopy">
                                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="bodycopy" ForeColor="red"></asp:Label><br />
                                    <input type="checkbox" id="chkRead" runat="server" class="tableData" width="192px" />
                                    <%=Resources.Resource.el_AcceptTC %><br />
                                    <br />
                                    <asp:ImageButton ID="Accept" Visible="false" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_17 %>"
                                        AlternateText="Accept" />&nbsp;&nbsp;&nbsp;
                                <a id="lnkLogin" visible="false" runat="server">
                                    <img src="<%=Resources.Resource.sna_svc_img_17 %>" alt="Accept" border="0" /></a>&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="Decline" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_32 %>" AlternateText="Decline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
