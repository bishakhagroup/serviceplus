﻿<%@ WebHandler Language="VB" Class="ImageHandler" %>

Imports System
Imports System.Web
Imports System.Drawing.Text
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports ServicePLUSWebApp
    
Public Class ImageHandler : Implements IHttpHandler, IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim teks As String = GenerateRandomCode()
        HttpContext.Current.Session.Add("CaptchaImageText", teks)

        Dim width, height As Integer
        If String.IsNullOrEmpty(context.Request.QueryString("w")) Or _
        CInt(context.Request.QueryString("w")) < 0 Then
            width = 100
        Else
            width = CInt(context.Request.QueryString("w"))
        End If

        If String.IsNullOrEmpty(context.Request.QueryString("h")) Or _
        CInt(context.Request.QueryString("h")) < 0 Or _
        CInt(context.Request.QueryString("h")) >= CInt(context.Request.QueryString("w")) Then
            height = CInt(0.3 * width)
        Else
            height = CInt(context.Request.QueryString("h"))
        End If
        
        'Dim ci As New RandomImage(HttpContext.Current.Session("CaptchaImageText").ToString(), 300, 75)
        Dim ci As New RandomImage(HttpContext.Current.Session("CaptchaImageText").ToString(), width, height)

        context.Response.Clear()
        context.Response.ContentType = "image/jpeg"
        ci.Image.Save(context.Response.OutputStream, ImageFormat.Jpeg)
        ci.Dispose()
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Private Shared Function GenerateRandomCode() As String
        Dim r As New Random()
        Dim s As String = ""
        For j As Integer = 0 To 4
            Dim i As Integer = r.[Next](3)
            Dim ch As Integer
            Select Case i
                Case 1
                    ch = r.[Next](0, 9)
                    s = s & ch.ToString()
                    Exit Select
                Case 2
                    ch = r.[Next](65, 90)
                    s = s & Convert.ToChar(ch).ToString()
                    Exit Select
                Case 3
                    ch = r.[Next](97, 122)
                    s = s & Convert.ToChar(ch).ToString()
                    Exit Select
                Case Else
                    ch = r.[Next](97, 122)
                    s = s & Convert.ToChar(ch).ToString()
                    Exit Select
            End Select
            r.NextDouble()
            r.[Next](100, 1999)
        Next
        Return s
    End Function
    

End Class
