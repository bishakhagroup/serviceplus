Namespace ServicePLUSWebApp

    Partial Class CartItemsAndTotalControl
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
        End Sub


        Private Sub buildResults()
            If HttpContextManager.Customer IsNot Nothing Then
                displayLiteral.Controls.Add(New LiteralControl("<td width=""328"" background=""images/sp_int_nav_bar_right.gif"">"))
                displayLiteral.Controls.Add(New LiteralControl("<table width=""328"" border=""0"" cellspacing=""0"" cellpadding=""0"">"))
                displayLiteral.Controls.Add(New LiteralControl("<tr>"))
                displayLiteral.Controls.Add(New LiteralControl("<td width=""68""><img src=""images/spacer.gif"" width=""68"" height=""4"" alt=""""></td>"))
                displayLiteral.Controls.Add(New LiteralControl("<td><img src=""images/sp_int_itemsNcart.gif"" width=""74"" height=""8"" alt=""Number of items in cart""><span class=""ItemsInCart"">XX</span></td>"))
                displayLiteral.Controls.Add(New LiteralControl("</tr>"))
                displayLiteral.Controls.Add(New LiteralControl("<tr>"))
                displayLiteral.Controls.Add(New LiteralControl("<td width=""68""><img src=""images/spacer.gif"" width=""68"" height=""4"" alt=""""></td>"))
                displayLiteral.Controls.Add(New LiteralControl("<td><img src=""images/sp_int_total.gif"" width=""35"" height=""8"" alt=""Grand Total""><span class=""ItemsInCart"">$5,000</span></td>"))
                displayLiteral.Controls.Add(New LiteralControl("</tr>"))
                displayLiteral.Controls.Add(New LiteralControl("</table>"))
                displayLiteral.Controls.Add(New LiteralControl("</td>"))
            Else
                displayLiteral.Controls.Add(New LiteralControl(vbTab + vbTab + "<td width=""328""><img height=""29"" src=""images/sp_int_nav_bar_right.gif"" width=""327"" alt=""""></td>" + vbCrLf))
            End If
        End Sub

    End Class

End Namespace
