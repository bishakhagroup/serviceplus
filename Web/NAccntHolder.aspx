<%@ Reference Control="~/ShipMethod.ascx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ShipMethod" Src="ShipMethod.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.NAccntHolder"
    EnableViewStateMac="true" CodeFile="NAccntHolder.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Checkout</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet" />

    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">      
        function checkSession() {
            if (document.getElementById("txtHidden").value === "load") {
                document.getElementById("txtHidden").value = "done";
            }
            else {
                location.href = "SessionExpired.aspx";
            }
        }

        function copyAddress() {
            if (document.getElementById('chkSameShip').checked === true) {
                document.getElementById('TextBoxNameShip').value = document.getElementById('TextBoxName').value;
                document.getElementById('TextboxShipAttn').value = document.getElementById('TextboxAttn').value;
                document.getElementById('TextBoxLineShip').value = document.getElementById('TextBoxLine1').value;
                document.getElementById('TextBoxLine2Ship').value = document.getElementById('TextBoxLine2').value;
                //document.getElementById('TextBoxLine3Ship').value = document.getElementById('TextBoxLine3').value;//7901
                document.getElementById('TextBoxCityShip').value = document.getElementById('TextBoxCity').value;
                document.getElementById('DDLStateShip').selectedIndex = document.getElementById('DDLState').selectedIndex;
                document.getElementById('TextBox5').value = document.getElementById('TextBoxZip').value;
                //                'Reset form validation err msg
                //                LabelStateShip.CssClass = "tableData"
                //                ZipShip.CssClass = "tableData"
                //                LabelStateShip.CssClass = "tableData"
                //                LabelCityShip.CssClass = "tableData"
                //                LabelNameShip.CssClass = "tableData"
                //                LabelLine1Ship.CssClass = "tableData"
                document.getElementById('LabelName2Star').value = "";
                document.getElementById('LabelLine1ShipStar').value = "";
                document.getElementById('LabelState2Star').value = "";
                document.getElementById('LabelZip2Star').value = "";
                document.getElementById('LabelCity2Star').value = "";
                //document.getElementById('TextBox5').visible=false;
                var control = document.getElementById('TextBox5');
                control.style.visibility = "visible";
                //document.getElementById('tbCalifornia').visible = "true";
            }
            document.getElementById('btnNext').focus();
        }

        function poNotRequired() {
            if (document.getElementById('noPoRequiredCheckBox').checked === true) {
                document.getElementById("TextBoxPO").value = "NO-PO-REQ";
            }
            else {
                document.getElementById("TextBoxPO").value = "";
            }
        }

        function chkSameShip_onclick() {

        }

        function disableNextButton() {
            document.getElementById('btnNext').style.display = "none";
        }
    </script>

    <style type="text/css">
        .style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
        }

        .style3 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
            width: 370px;
        }

        .style4 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 12px;
            color: #2a3d47;
            font-weight: normal;
            width: 370px;
            text-align: left;
        }

        #tbCalifornia {
            height: 69px;
        }
    </style>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="javascript:checkSession();">
    <center>
        <form id="Form1" method="post" runat="server">
            <table id="Table1" width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table id="Table2" width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="2">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="Table3" width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="64" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <%--<td width=464 bgColor=#363d45 height=2>
                        <img height=64 src="images/sp_int_header_top_ServicesPLUS_heigh.gif" width=464 alt="" >
                    </TD>--%>
                                            <td valign="top" bgcolor="#363d45" height="2">
                                                <table id="Table4" width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">
                                                        </td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Checkout</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label><input
                                        id="txtHidden" type="hidden" value="load"><asp:HiddenField ID="hdCAPartialTax" runat="server" />
                                    <asp:HiddenField ID="hdTaxExempt" runat="server" />
                                    <asp:Label ID="lblCustomError" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="Table6" width="710" border="0" role="form">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img height="20" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="20">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td valign="top" width="670">
                                                <table id="Table7" cellpadding="3" width="664" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="2" class="tableData">
                                                            <strong>Please complete required fields below marked with an asterisk.</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="92">
                                                            <img height="1" src="images/spacer.gif" width="84" alt="">
                                                        </td>
                                                        <td width="580">
                                                            <img height="1" src="images/spacer.gif" width="574" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td bgcolor="#d5dee9" colspan="2">
                                                            <span class="tableHeader">&nbsp;&nbsp;&nbsp;Bill to:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="92">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="580">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr id="TokenButton" runat="server">
                                                        <td align="right" class="tableData" width="92">
                                                        </td>
                                                        <td class="tableData">
                                                            <asp:Button ID="btnNewCC" Text="Enter New Credit Card" runat="server" OnClientClick="return popup();" />
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:Button ID="btnCCSaved" Text="Select Saved Credit Card" runat="server" />
                                                            <asp:Button ID="btnHidden" runat="Server" Style="display: none" />
                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="pnlSavedCard" runat="server">
                                                        <tr id="TextHide">
                                                            <td class="tableData" align="right" width="92" height="29">
                                                            </td>
                                                            <td class="tableData" width="580" height="29">
                                                                Please select a credit card from your saved list.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92" height="21">
                                                                <asp:Label ID="LabelOneCard" runat="server" CssClass="tableData">Saved Cards: </asp:Label>
                                                            </td>
                                                            <td width="580" height="21">
                                                                <asp:DropDownList ID="DDLCCName" runat="server" CssClass="tableData"
                                                                    AutoPostBack="True" OnSelectedIndexChanged="DDLCCName_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlCCInfo" runat="server">
                                                        <tr>
                                                            <td class="tableData" align="right" width="92" height="32">
                                                                <asp:Label ID="LabelCC" runat="server" CssClass="tableData">Card Type : </asp:Label>
                                                            </td>
                                                            <td width="580" height="32">
                                                                <asp:TextBox ID="txtCCType" runat="server" Width="100" CssClass="bodyCopy" Enabled="false"></asp:TextBox>
                                                                <asp:DropDownList ID="DDLType" runat="server" CssClass="tableData" Enabled="false"
                                                                    Visible="false">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tableData" align="right" width="92">
                                                                <asp:Label ID="LabelCCNumber" runat="server" CssClass="tableData">Card Number: </asp:Label>
                                                            </td>
                                                            <td width="580">
                                                                <SPS:SPSTextBox ID="TextBoxCCNumber" runat="server" CssClass="tableData"
                                                                    Width="200px" MaxLength="30" Enabled="false"></SPS:SPSTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" width="92">
                                                                &nbsp;&nbsp;
                                                                <asp:Label ID="LabelExpire" runat="server" CssClass="tableData">Expiration Date: </asp:Label>
                                                                &nbsp;
                                                            </td>
                                                            <td width="580">
                                                                <table id="Table8" width="200" border="0" role="presentation">
                                                                    <tr>
                                                                        <td class="tableData">
                                                                            <asp:TextBox ID="txtMonth" runat="server" Width="70" CssClass="tableData" Enabled="false"></asp:TextBox>
                                                                            <asp:TextBox ID="txtYear" runat="server" Width="30" CssClass="tableData" Enabled="false"></asp:TextBox>
                                                                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="tableData"
                                                                                Enabled="false" Visible="false">
                                                                            </asp:DropDownList>
                                                                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="tableData"
                                                                                Enabled="false" Visible="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlNickName" runat="server">
                                                        <tr>
                                                            <td class="tableData" align="right" width="92" height="29">
                                                                <asp:Label ID="LabelNName" runat="server" CssClass="tableData">To save new credit card, enter Nickname:</asp:Label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="TextBoxNName" runat="server" CssClass="tableData"
                                                                    MaxLength="17"></SPS:SPSTextBox>
                                                                <%--<asp:Label ID="LabelNNameStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>--%>
                                                                <asp:Label ID="NickNameFormatError" runat="server" CssClass="tableData" EnableViewState="False">Nicknames can be alphanumeric and cannot contain spaces or special characters.</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" width="92">
                                                                &nbsp;&nbsp;&nbsp; <span class="tableData">&nbsp;</span>
                                                            </td>
                                                            <td width="580" colspan="3">
                                                                <table id="Table9" width="100%" border="0" role="presentation">
                                                                    <tr>
                                                                        <td width="30">
                                                                            <asp:CheckBox ID="chkSaveCard" runat="server" CssClass="tableData" Text="Save this card"></asp:CheckBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td class="tableData" width="258">
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            &nbsp;<asp:Label ID="LabelName" runat="server" CssClass="tableData">Company Name:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextBoxName" runat="server" CssClass="tableData" Width="200px"
                                                                MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelNameStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            &nbsp;<asp:Label ID="LabelAttn" runat="server" CssClass="tableData">Name as it appears on your credit card:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextboxAttn" MaxLength="50" runat="server" CssClass="tableData"
                                                                Width="200px"></SPS:SPSTextBox>
                                                            <asp:Label ID="lblAttnStar" runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="left" colspan="2" width="100%">
                                                            &nbsp;<asp:Label ID="Label1" runat="server" CssClass="tableData">Billing Address - 
                                                            Must match the address on your credit card statement.</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            <asp:Label ID="LabelLine1" runat="server" CssClass="tableData">Street Address: </asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextBoxLine1" runat="server" CssClass="tableData"
                                                                Width="200px" MaxLength="50"></SPS:SPSTextBox>&nbsp;
                                                            <asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick">
																					<span class="redAsterick">*</span>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            <asp:Label ID="LabelLine2" runat="server" CssClass="tableData">Address 2nd Line:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextBoxLine2" runat="server" CssClass="tableData"
                                                                Width="200px" MaxLength="50"></SPS:SPSTextBox>&nbsp;
                                                            <asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick">
																					
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--7901 starts--%>
                                                    <%--<tr>
                                                        <td class="tableData" align="right" width="92">
                                                            <asp:Label ID="LabelLine3" runat="server" CssClass="tableData">Address 3rd Line:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextBoxLine3" runat="server" CssClass="tableData"
                                                                Width="200px" MaxLength="50"></SPS:SPSTextBox>&nbsp;
                                                                   <asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick">
																					
                                                            </asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                    <%--7901 ends--%>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            &nbsp;<asp:Label ID="LabelCity" runat="server" CssClass="tableData">City:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextBoxCity" runat="server" CssClass="tableData"
                                                                Width="160px" MaxLength="50"></SPS:SPSTextBox>&nbsp;
                                                            <asp:Label ID="LabelCityStar" runat="server" CssClass="redAsterick">
																					<span class="redAsterick">*</span>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            &nbsp;
                                                            <asp:Label ID="LabelState" runat="server" CssClass="tableData">State:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <asp:DropDownList ID="DDLState" runat="server" CssClass="tableData">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="LabelStateStar" runat="server" CssClass="redAsterick">
																					<span class="redAsterick">*</span>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92" height="27">
                                                            &nbsp;
                                                            <asp:Label ID="LabelZip" runat="server" CssClass="tableData">Zip Code</asp:Label>
                                                        </td>
                                                        <td width="580" height="27">
                                                            <SPS:SPSTextBox ID="TextBoxZip" runat="server" CssClass="tableData"
                                                                Width="100px" MaxLength="5"></SPS:SPSTextBox>&nbsp;
                                                            <asp:Label ID="LabelZipStar" runat="server" CssClass="redAsterick">
																					<span class="redAsterick">*</span>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            <asp:Label ID="PoNumberLabel" runat="server" CssClass="tableData">PO Number:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td width="580" class="tableData">
                                                            <SPS:SPSTextBox ID="TextBoxPO" runat="server" CssClass="tableData" Width="100px"></SPS:SPSTextBox><asp:Label
                                                                ID="PoNumberAsterick" runat="server" CssClass="redAsterick">
																					<span class="redAsterick">*</span>
                                                            </asp:Label>
                                                            <%--<asp:checkbox id=noPoRequiredCheckBox runat="server" CssClass="tabledata" AutoPostBack="True" Text="Company does not require a purchase order"></asp:checkbox>--%>
                                                            <input type="checkbox" id="noPoRequiredCheckBox" runat="server" class="tableData"
                                                                onclick="javascript: poNotRequired();" />Company does not require a purchase
                                                            order
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92" height="29">
                                                        </td>
                                                        <td class="tableData" width="580" height="29">
                                                            <asp:Label ID="CCErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <table id="Table10" cellpadding="3" width="664" border="0" role="presentation">
                                                    <tr>
                                                        <td bgcolor="#d5dee9" colspan="3">
                                                            <span class="tableHeader">&nbsp;&nbsp;&nbsp;Ship to:</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="84">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="266">
                                                            <table id="Table11" width="200" border="0" role="presentation">
                                                                <tr>
                                                                    <td valign="top" colspan="2" class="tableData">                                                                        
                                                                        <input type="checkbox" id="chkSameShip" onclick="javascript: copyAddress();" runat="server"
                                                                            class="tableData" width="192px" />Copy billing address information to shipping
                                                                        address
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="580">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84">
                                                            <asp:Label ID="LabelNameShip" runat="server" CssClass="tableData">Company Name: </asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="TextBoxNameShip" runat="server" CssClass="tableData"
                                                                Width="150px" MaxLength="50"></SPS:SPSTextBox>&nbsp;<asp:Label ID="LabelName2Star"
                                                                runat="server" CssClass="redAsterick">*</asp:Label>
                                                        </td>
                                                        <td valign="top" rowspan="8">
                                                            <table id="Table12" width="350" border="0" role="presentation">
                                                            </table>
                                                            <uc1:ShipMethod ID="ShipMethod" runat="server"></uc1:ShipMethod>
                                                            <asp:Label ID="LabelCalifornia" runat="server" CssClass="redAsterick" />
                                                            <br />
                                                            <%--<table cellpadding=0 cellspacing=0 style=" height: 37px; width: 100%; text-align: left; border-color:Black" 
                                                                runat="server" id="tbCalifornia" visible=false border=1>
                                                                <tr align=left>
                                                                <td class="tabledata">Is your order eligible for the California section 6378 partial<br /> 
                                                                    tax exemption?</td></tr>
                                                                <tr align=center><td class="tabledata"><asp:radiobuttonlist id="rbCalifornia" RepeatDirection=Horizontal runat="server" CssClass="tableData" Width="150px">
																			<asp:ListItem Value="0" Selected=False Text="Yes">Yes</asp:ListItem>
																			<asp:ListItem Value="1" Selected=False Text="No">No</asp:ListItem>
																		</asp:radiobuttonlist></td></tr>
                                                                </table>--%>
                                                            <table id="tbCalifornia" runat="server" width="350" border="0" role="presentation">
                                                                <tr bgcolor="#b7b7b7">
                                                                    <td colspan="6">
                                                                        <img src="images/spacer.gif" width="1" height="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr align="left">
                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td colspan="2" width="10">
                                                                    </td>
                                                                    <td colspan="2">
                                                                        <span class="tableHeader">
                                                                            <SPS:SPSLabel ID="spCali" runat="server" Text="Apply California section 6378 blanket exemption certificate"></SPS:SPSLabel><br />
                                                                            <SPS:SPSLabel ID="spCali1" runat="server" Text=" for partial tax exemption for all items on this order?"></SPS:SPSLabel>
                                                                        </span>
                                                                    </td>
                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#b7b7b7">
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr align="center">
                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td colspan="4" class="tabledata">
                                                                        <asp:RadioButtonList ID="rbCalifornia" RepeatDirection="Horizontal" runat="server"
                                                                            CssClass="tableData" Width="150px">
                                                                            <asp:ListItem Value="0" Selected="False" Text="Yes">Yes</asp:ListItem>
                                                                            <asp:ListItem Value="1" Selected="False" Text="No">No</asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </td>
                                                                    <td width="1" bgcolor="#b7b7b7">
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#b7b7b7">
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="92">
                                                            &nbsp;<asp:Label ID="LabelShipAttn" runat="server" CssClass="tableData">Attn, Name:</asp:Label>
                                                        </td>
                                                        <td width="580">
                                                            <SPS:SPSTextBox ID="TextboxShipAttn" MaxLength="50" runat="server" CssClass="tableData"
                                                                Width="200px" /><br />
                                                            <asp:Label ID="LabelLine1ShipAttention" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84" valign="top">
                                                            <asp:Label ID="LabelLine1Ship" runat="server" CssClass="tableData">Street Address: </asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="TextBoxLineShip" runat="server" CssClass="tableData"
                                                                Width="150px" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="Label10" runat="server"
                                                                    CssClass="redAsterick">*</asp:Label><br />
                                                            <asp:Label ID="LabelLine1ShipStar" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84" valign="top">
                                                            <asp:Label ID="LabelLine2Ship" runat="server" CssClass="tableData">Address 2nd Line:</asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="TextBoxLine2Ship" runat="server" CssClass="tableData"
                                                                Width="150px" MaxLength="50" /><br />
                                                            <asp:Label ID="LabelLine2ShipStar" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84" height="27" valign="top">
                                                            <asp:Label ID="LabelCityShip" runat="server" CssClass="tableData">City:</asp:Label>
                                                        </td>
                                                        <td width="266" height="27">
                                                            <SPS:SPSTextBox ID="TextBoxCityShip" runat="server" CssClass="tableData"
                                                                Width="150px" MaxLength="50" /><asp:Label ID="Label11" runat="server"
                                                                    CssClass="redAsterick">*</asp:Label><br />
                                                            <asp:Label ID="LabelCity2Star" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84">
                                                            &nbsp;
                                                            <asp:Label ID="LabelStateShip" runat="server" CssClass="tableData">State:</asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <asp:DropDownList ID="DDLStateShip" runat="server" CssClass="tableData"
                                                                AutoPostBack="True" />
                                                            <asp:Label ID="LabelState2Star" runat="server" CssClass="redAsterick">*</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2" align="right" width="84">
                                                            &nbsp;
                                                            <asp:Label ID="ZipShip" runat="server" CssClass="tableData">Zip Code:</asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="TextBox5" runat="server" CssClass="tableData" Width="100px"
                                                                MaxLength="5" /><asp:Label ID="LabelZip2Star" runat="server" CssClass="redAsterick">*</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84">
                                                            &nbsp;
                                                            <asp:Label ID="PhoneShip" runat="server" CssClass="tableData">Phone Number:</asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="txtPhoneShip" runat="server" CssClass="tableData"
                                                                Width="100px" MaxLength="12" />
                                                            <asp:Label ID="LabelShipPhoneStar" runat="server" CssClass="redAsterick">*</asp:Label>&nbsp;&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84">
                                                            &nbsp;
                                                            <asp:Label ID="ExtensionShip" runat="server" CssClass="tableData">Phone Extension:</asp:Label>
                                                        </td>
                                                        <td width="266">
                                                            <SPS:SPSTextBox ID="txtShipExtension" runat="server" CssClass="tableData"
                                                                Width="64px" MaxLength="6" />&nbsp;&nbsp;<span class="bodyCopy">xxxxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tableData" align="right" width="84">
                                                            &nbsp;
                                                        </td>
                                                        <td width="266">
                                                            <table id="Table13" width="210" border="0" role="presentation">
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;
                                                                        <asp:ImageButton ID="btnNext" runat="server" ImageUrl="<%$ Resources:Resource, img_btnNext%>"
                                                                            AlternateText="Next" OnClientClick="disableNextButton()" /><asp:ImageButton
                                                                            ID="btnCancel" runat="server" ImageUrl="<%$ Resources:Resource,sna_svc_img_7%>"
                                                                            AlternateText="Cancel" />
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" width="84">
                                                            &nbsp;&nbsp;&nbsp;<span class="tableData">&nbsp;</span>
                                                        </td>
                                                        <td width="266">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
