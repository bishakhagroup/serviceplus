<%@ Page Language="VB" AutoEventWireup="false" CodeFile="printer-service-result.aspx.vb" Inherits="ServicePLUSWebApp.PrinterServiceResult" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ServicesPLUS - Sony Service Repair and Maintenance for Professional Products</title>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script language="javascript">
        function PrintPage() {
            setTimeout('window.print ()', 1000)
            //window.print ();
            //this.close ();
            setTimeout('window.close()', 2000)

        }
    </script>
</head>
<body onload="HideTable();">
    <form id="form1" runat="server">
        <table runat="server" visible="false" id="tblSessionOut" cellpadding="3" cellspacing="1" border="0" style="width: 93%; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width: 1px; border-right-color: 666666; border-right-style: solid; border-right-width: 1px; border-left-color: 666666; border-left-style: solid; border-left-width: 1px; border-top-color: 666666; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: 666666">
            <tr valign="top">
                <td style="border-bottom-style: solid; border-bottom-width: thin;">
                    <img src="https://www.servicesplus.sel.sony.com/images/sp_int_sonylogo.gif" /></td>
                <td style="border-bottom-style: solid; border-bottom-width: thin;" align="right" valign="middle"><%--<a href="javascript:PrintPage();">
                                <img src="images/print_request_form.gif" border=0 alt="Print Request Form" />
                            </a>--%>&nbsp;
                <asp:Label ID="ErrorLabel" runat="server" Text="" />
                </td>
            </tr>
            <tr valign="top">
                <td colspan="2" style="border-bottom-style: solid; border-bottom-width: thin;">
                    <table border="0" cellpadding="1" cellspacing="1" style="width: 100%">
                        <tr>
                            <td colspan="2" style="height: 16px" align="center">
                                <strong>ServicesPLUS - Sony Service Repair and Maintenance for Professional Products
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px"></td>
                            <td style="width: 100px"></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="width: 373px;" colspan="2">Your session expired, Please try again with a fresh login.</td>
            </tr>
        </table>

        <table id="tblMain" name="tblMain" cellpadding="3" cellspacing="1" border="0" style="width: 93%; visibility: visible; font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width: 1px; border-right-color: 666666; border-right-style: solid; border-right-width: 1px; border-left-color: 666666; border-left-style: solid; border-left-width: 1px; border-top-color: 666666; border-top-style: solid; border-top-width: 1px; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: 666666">
            <tr valign="top">
                <td style="border-bottom-style: solid; border-bottom-width: thin;">
                    <img src="https://www.servicesplus.sel.sony.com/images/sp_int_sonylogo.gif" /></td>
                <td style="border-bottom-style: solid; border-bottom-width: thin;" align="right" valign="middle"><%--<a href="javascript:PrintPage();">
                                <img src="images/print_request_form.gif" border=0 alt="Print Request Form" />
                            </a>--%>&nbsp;</td>
            </tr>
            <tr valign="top">
                <td colspan="2" style="border-bottom-style: solid; border-bottom-width: thin;">
                    <table border="0" cellpadding="1" cellspacing="1" style="width: 100%">
                        <tr>
                            <td colspan="2" style="height: 16px" align="center">
                                <strong>ServicesPLUS - Sony Service Repair and Maintenance for Professional Products
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px"></td>
                            <td style="width: 100px"></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr valign="top">
                <td style="width: 173px;">Date:</td>
                <td style="width: 376px;"><%=DateTime.Now.ToString("MM/dd/yyyy")%></td>
            </tr>

            <%If bColor = False Then%>
                <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
            <%ELSE%>
                <tr valign="top">
                <%bColor = False %>
            <%end if %>
                <td style="width: 173px;">Request ID:</td>
                <td style="width: 376px;"><%=strServicesRequestID %></td>
            </tr>



            <%If bShow = True Then%>
                <%If isAccntHolder = True Then%>
                    <%If bColor = False Then%>
                        <tr valign="top" bgcolor="#d5dee9">
                        <%bColor = True %>
                    <%Else%>
                        <tr valign="top">
                        <%bColor = False %>
                    <%End If %>
                    <td style="width: 173px;">Payer Account:</td>
                    <td style="width: 376px;"><%=sPayerAccount%></td>
                </tr>
                <%End If%>
            <%End If %>


            <%If bShow = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px;">Bill To Address:</td>
                <td style="width: 376px;"><%=sBillToAddr %></td>
                </tr>
            <%end if %>

            <%If bColor = False Then%>
                <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
            <%ELSE%>
                <tr valign="top">
                <%bColor = False %>
            <%end if %>
                <td style="width: 173px">Ship To Address:
				<%If blnShipOnHold Then%>
                    <br />
                    <span class="tabledata"><b><u>Do not ship unit:</u></b> Hold unit for pickup at service center.</span>
                <%End If%>
                </td>
                <td><%=sShipToAddr%></td>
            </tr>

            <%If bColor = False Then%>
                <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
            <%ELSE%>
                <tr valign="top">
                <%bColor = False %>
            <%end if %>
                <td style="width: 173px">Model Number:</td>
                <td><%=sModelName%></td>
            </tr>

            <%If bColor = False Then%>
                <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
            <%ELSE%>
                <tr valign="top">
                <%bColor = False %>
            <%end if %>
                <td style="width: 173px">Serial Number:</td>
                <td><asp:Label ID="lblSerialNo" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></td>
            </tr>

            <%If bColor = False Then%>
                <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
            <%ELSE%>
                <tr valign="top">
                <%bColor = False %>
            <%end if %>
                <td style="width: 173px" valign="top">Accessories:</td>
                <td><%=sAccessories%></td>
            </tr>

            <%If bShow = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px" valign="top">PO Number:</td>
                <td><asp:Label ID="lblPONumber" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></td>
            </tr>
            <%end if %>

            <%If bApprFlag = True And bShow = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>

                <td style="width: 173px" valign="top">Approval:</td>
                <td>A labor charge of $<%=mDepotServiceCharge%> plus shipping and handling if I decide not to accept an estimate</td>
            </tr>
            <%End If%>

            <%If bTaxFlag = True And bShow = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px" valign="top">Tax-exempt:</td>
                <td><span class="bodyCopy">Please include a copy of your tax-exempt certificate as proof of tax-exempt status.</span></td>
            </tr>
            <%end if %>

            <%If bWarrantyFlag = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px" valign="top">Warranty:</td>
                <td><span class="bodyCopy">If you did not purchase this unit directly from Sony Electronics Inc., then you will need to include a copy of your dated sales invoice.</span></td>
            </tr>
            <%end if %>

            <%If bContractFlag = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px">Service Contract Number:</td>
                <td><asp:Label ID="lblContractNo" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></td>
            </tr>
            <%End If%>

            <%If bShow = True Then%>
                <%If bColor = False Then%>
                    <tr valign="top" bgcolor="#d5dee9">
                    <%bColor = True %>
                <%ELSE%>
                    <tr valign="top">
                    <%bColor = False %>
                <%end if %>
                <td style="width: 173px">Payment Type:</td>
                <td><asp:Label ID="lblPaymentType" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></td>
            </tr>
            <%End If%>





            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>

                <td style="width: 173px">Contact Name:</td>
                <td><%=strContactPersonName%></td>
            </tr>


            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>

                <td style="width: 173px">Contact Email Address:</td>
                <td><%=sContactEmailAddr%></td>
            </tr>


            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>
                <td style="width: 173px">Intermittent Problem:</td>
                <td>
                    <asp:Label ID="lblIntermittent" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></td>
            </tr>


            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>
                <td style="width: 173px">Preventive Maintenance:</td>
                <td><span class="bodyCopy">
                                                                <asp:Label ID="lblPreventive" runat="server" CssClass="bodyCopy" Text="Label" Width="144px"></asp:Label></span></td>
            </tr>

            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>
                <td style="width: 173px;" valign="top">Problem Description:</td>
                <td><span class="bodyCopy"><%=sProblemDescr%></span></td>
            </tr>

            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>
                <td colspan="2">
                    <table width="100%" border="0" cellpadding="1">
                        <tr valign="top">
                            <td colspan="2"><span class="bodyCopy"><span style="text-decoration: underline"><%=sInclude%></span></span></td>
                        </tr>
                        <tr valign="top">
                            <td colspan="2" valign="top"><span class="bodyCopy"><span style="text-decoration: underline"><%=sNotify%><br /></span></span></td>
                        </tr>

                        <tr valign="top">
                            <td style="width: 15px">
                                <img src="https://www.servicesplus.sel.sony.com/images/checkbox.jpg" border="0" /></td>
                            <td><span class="bodyCopy">A media sample if you have a problem associated with specific media.&nbsp;Please do not send master copies.</span></td>
                        </tr>
                        <tr valign="top">
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr valign="top">
                            <td>&nbsp;</td>
                            <td><span class="bodyCopy"><%=sServiceAddr%></span></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <%If bColor = False Then%>
            <tr valign="top" bgcolor="#d5dee9">
                <%bColor = True %>
                <%ELSE%>
            <tr valign="top">
                <%bColor = False %>
                <%end if %>
                <td colspan="2">Please go to <a href="<%=sNavBackToService%>"><%=sNavBackToService%></a> to track service status.&nbsp;</td>
            </tr>
        </table>

    </form>
</body>
</html>
