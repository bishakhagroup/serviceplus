Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class pg_service_maintenance_repair
        Inherits SSL

        Public sModelName As String = String.Empty
        'Start Modification for E833 and E834
        Public sModelStatusCode As String = String.Empty
        'End Modification for E833 and E834
        Public nRateDepot As String = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge
        Public nMinLabour As String = ConfigurationData.GeneralSettings.RepairCosting.DepotMinLbrCharge
        Public nRateField As String = ConfigurationData.GeneralSettings.RepairCosting.FieldServiceCharge
        Public nMinLabourTm As String = ConfigurationData.GeneralSettings.RepairCosting.FieldMinLbrTime

        ' define constants
        Public Const SETFLAG As String = "1"
        Public Const UNSETFLAG As String = "0"

        ' define flags 
        Public fDepotRequest As String = SETFLAG
        Public fDeportContactMsg As String = SETFLAG
        Public fOverallLayout As String = SETFLAG
        Public fServiceLocResult As String = SETFLAG

        Public PhoneNo As String = ""
        Public sStateName As String = ""
        Public sServiceCenter As String = ""
        Public sCityName As String = ""

        Public sDepotURL As String
        Public customer As Customer
        Public isFieldService As Boolean = False

        Public fCustomerSession As Boolean = False
        Public thruSearchModel As Boolean = False
        'Dim objUtilties As Utilities = New Utilities
        Dim szipcode As String '7395
        Public divsvcrprShow As Boolean

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim flag As Boolean = True
                Dim cm As CatalogManager = New CatalogManager

                If Session("ShowServiceLoc") IsNot Nothing Then
                    If Session("ShowServiceLoc").ToString().Trim() <> "0" Then
                        ShowServiceLoc(Session("ShowServiceLoc").ToString().Trim())
                        Session.Remove("ShowServiceLoc") ' = "0"
                        isFieldService = True
                        flag = False
                    End If
                End If

                If Request.QueryString("model") IsNot Nothing Then
                    sModelName = Trim(Request.QueryString("model").Replace("+", "/")).ToUpper()
                    Session("ServiceModelNumber") = sModelName
                    If Not IsValidString(sModelName) Then
                        Response.Redirect("sony-repair.aspx?InvalidModelNo=True")
                        Return
                    ElseIf Request.QueryString("frmRepair") Is Nothing Then
                        Dim modelsGroups = cm.GetSrhModelDetails(sModelName, HttpContextManager.GlobalData)
                        If (modelsGroups Is Nothing) OrElse (modelsGroups.Length = 0) Then
                            Response.Redirect("sony-repair.aspx?InvalidModelNo=True")
                            Return
                        End If
                    End If
                End If

                If Not IsPostBack Then
                    PopulateStateDropdownList(ddlStates, True)  ' 2016-06-09 ASleight - Update to use SSL.aspx.vb's shared method
                End If

                cm.getRepairCostConf(nRateDepot, nRateField, nMinLabour, nMinLabourTm)

                If flag = True Then
                    SwitchFlag()
                    lblResult.Visible = False
                End If
                divsvcrprShow = HttpContextManager.GlobalData.IsAmerica

                If Not String.IsNullOrWhiteSpace(Request.QueryString("thruSearchModel")) Then
                    Try
                        thruSearchModel = Convert.ToBoolean(Request.QueryString("thruSearchModel"))
                    Catch ex As FormatException
                        ErrorLabel.Text = "Improper value in URL. Please go back and try again."
                    End Try
                End If
                If fDeportContactMsg <> "1" And sModelName <> String.Empty And Session.Item("customer") IsNot Nothing And (Session("ShowServiceLoc") Is Nothing Or Session("ShowServiceLoc") <> "0") Then
                    Session.Add("ServiceModelNumber", sModelName)
                    sDepotURL = "sony-service-sn.aspx"
                    'sDepotURL = "sony-service-sn.aspx?model=" + sModelName
                    Response.Redirect(sDepotURL)
                End If

                If (Session("ShowServiceLoc") IsNot Nothing Or Session("ShowServiceLoc") = "0") Then
                    Session.Remove("ShowServiceLoc")
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            Dim LanguageSM As String = HttpContextManager.GlobalData.LanguageForIDP

            Try
                If Not String.IsNullOrEmpty(sModelName) And Not isFieldService Then
                    lblSubHdr.InnerText = $"{sModelName} {Resources.Resource.repair_sny_pg_svcrqst_msg}"
                Else
                    lblSubHdr.InnerText = Resources.Resource.repair_pg_stus_msg_1
                End If

                If Session.Item("customer") IsNot Nothing Then
                    fCustomerSession = True
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin", "<script language=""javascript"">function ShowLogin(){window.location.href='SignIn.aspx?Lang=" + LanguageSM + "&SPGSModel=" + Trim(sModelName.ToUpper()) + "';}</script>")
                Else
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin", "<script language=""javascript"">function ShowLogin(){window.location.href='SignIn-Register.aspx?SPGSModel=" + Trim(sModelName.ToUpper()) + "'; }</script>")
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public ReadOnly Property ModelName() As String
            Get
                Return sModelName
            End Get
        End Property

        ' procedure to set up flags for layouts according to model status
        Protected Sub SwitchFlag()
            'Try catch added for 2426 by Prasad
            Try
                Dim cm As CatalogManager = New CatalogManager
                Dim strTo_Discont As String
                'Commented for E833 and E834
                'Dim sModelStatusCode As String
                sModelStatusCode = cm.GetModelStatusCode(sModelName)
                strTo_Discont = sModelStatusCode.Split(",".ToCharArray())(1).ToString()
                sModelStatusCode = sModelStatusCode.Split(",".ToCharArray())(0).ToString()

                ' if blank, Y1, Y2, Y5 then show button
                If (sModelStatusCode = "Y1" Or sModelStatusCode = "Y2" Or sModelStatusCode = "Y5" Or sModelStatusCode = "00") And strTo_Discont = "1" Then
                    SetFlagCode("0110")
                ElseIf sModelStatusCode = "Y3" Or sModelStatusCode = "Y4" Or sModelStatusCode = "Y6" Or sModelStatusCode = "Y7" Or strTo_Discont = "0" Then
                    SetFlagCode("1010")
                    ' Else
                    '    Default "FlagCode" is 1111 for other situations
                End If
            Catch ex As Exception
                Throw ex
            End Try


        End Sub
        ' 2016-06-09 ASleight - No longer used
        Private Sub PopulateStatesDDL()
            'populateStateDropdownList(ddlStates)
            'Dim objGlobalData As New GlobalData
            'If Not (Session.Item("GlobalData")) Is Nothing Then
            '    objGlobalData = (Session.Item("GlobalData"))
            'End If
            Dim cm As New CatalogManager
            Dim statesdtls() As StatesDetail
            Dim iState As StatesDetail
            'If Not (objGlobalData) Is Nothing Then
            Try
                statesdtls = cm.StatesBySalesOrg(HttpContextManager.GlobalData)
                Dim newListItem1 As New ListItem()
                newListItem1.Text = GetGlobalResourceObject("Resource", "repair_selstate_msg")
                newListItem1.Value = ""
                ddlStates.Items.Add(newListItem1)

                For Each iState In statesdtls
                    Dim newListItem As New ListItem()
                    newListItem.Text = ChangeCase(iState.StateName)
                    newListItem.Value = iState.StateAbbr
                    ddlStates.Items.Add(newListItem)
                Next
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
            'End If
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

        Private Sub SetFlagCode(ByVal psCodeByte As String)
            ' in byte code digits
            ' 1: Depot contract message display, 2: button display, 3: overall layout display, 4: Result display
            ' 1: enable, 0: disable
            '
            fDeportContactMsg = UNSETFLAG
            fDepotRequest = UNSETFLAG
            fOverallLayout = UNSETFLAG
            fServiceLocResult = UNSETFLAG

            If psCodeByte.Substring(0, 1) = "1" Then fDeportContactMsg = SETFLAG
            If psCodeByte.Substring(1, 1) = "1" Then fDepotRequest = SETFLAG
            If psCodeByte.Substring(2, 1) = "1" Then fOverallLayout = SETFLAG
            If psCodeByte.Substring(3, 1) = "1" Then fServiceLocResult = SETFLAG
        End Sub

        Protected Sub imgBtnGo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnGo.Click
            'System.Console.Write("Sai Ram")
            Dim sStateAbbrev As String = ddlStates.SelectedValue.ToString().Trim().ToUpper()
            Dim cm As CatalogManager = New CatalogManager
            Dim sServiceCenter As String
            Dim arServiceLoc() As String
            Dim sRequestState As String

            sServiceCenter = cm.GetServiceCenter(sStateAbbrev)
            arServiceLoc = sServiceCenter.Split("!")
            sServiceCenter = ""
            PhoneNo = ""
            sStateName = ""

            If arServiceLoc.Length = 4 Then
                SetFlagCode("0001")
                lblResult.Visible = True
                sServiceCenter = arServiceLoc.GetValue(0)
                PhoneNo = arServiceLoc.GetValue(2).ToString()
                sStateName = arServiceLoc.GetValue(1).ToString()
                'Changed by Chandra on 07-June-07 
                sRequestState = arServiceLoc.GetValue(3).ToString()
                'lblResult.Text = "To inquire about field service for your " + sModelName + "unit located in " + ChangeCase(sStateName) + ", please contact the Sony service center in " + sServiceCenter + ", " + ChangeCase(sStateName) + " at " + PhoneNo + "."
                lblResult.Text = GetGlobalResourceObject("Resource", "repair_pg_toinfldsvc_msg") + sModelName + Resources.Resource.repair_pg_untlctd_msg + ChangeCase(sRequestState) + Resources.Resource.repair_pg_untlctd_msg_1 + sServiceCenter + ", " + ChangeCase(sStateName) + Resources.Resource.repair_pg_stus_msg + PhoneNo + "."
            Else
                SetFlagCode("0001")
            End If
            isFieldService = True
            ''in case of field service make this field as empty
            'sModelName = String.Empty
        End Sub

        Private Sub ShowServiceLoc(ByVal sStateAbbrev As String)
            Dim cm As CatalogManager = New CatalogManager
            Dim sServiceCenter As String
            Dim arServiceLoc() As String
            Dim sRequestState As String

            sServiceCenter = cm.GetServiceCenter(sStateAbbrev)
            arServiceLoc = sServiceCenter.Split("!")
            sServiceCenter = ""
            PhoneNo = ""
            sStateName = ""

            If arServiceLoc.Length = 4 Then
                SetFlagCode("0001")
                lblResult.Visible = True
                sServiceCenter = arServiceLoc.GetValue(0)
                PhoneNo = arServiceLoc.GetValue(2).ToString()
                sStateName = arServiceLoc.GetValue(1).ToString()
                'Changed by Chandra on 07-June-07 
                sRequestState = arServiceLoc.GetValue(3).ToString()
                'lblResult.Text = "To inquire about field service for your " + sModelName + "unit located in " + ChangeCase(sStateName) + ", please contact the Sony service center in " + sServiceCenter + ", " + ChangeCase(sStateName) + " at " + PhoneNo + "."
                lblResult.Text = GetGlobalResourceObject("Resource", "repair_pg_toinfldsvc_msg") + sModelName + GetGlobalResourceObject("Resource", "repair_pg_untlctd_msg") + ChangeCase(sRequestState) + Resources.Resource.repair_pg_untlctd_msg_1 + sServiceCenter + ", " + ChangeCase(sStateName) + Resources.Resource.repair_pg_stus_msg + PhoneNo + "."
            Else
                SetFlagCode("0001")
            End If
        End Sub

    End Class
End Namespace
