Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class sony_extended_warranties
        Inherits SSL
        Dim ew As ExtendedWarrantyManager
        Dim ewmodels As DataSet
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ModelDescription As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Search1ErrorLabel.Text = ""
                Search2ErrorLabel.Text = ""

                formFieldInitialization()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub SearchModel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SearchModel.Click
            Try
                Search1ErrorLabel.Text = ""

                If ewModelNumber.Text = "" Then
                    Search1ErrorLabel.Text = "You must enter a model number."
                Else
                    ew = New ExtendedWarrantyManager
                    ewmodels = ew.EWModelSearch(ewModelNumber.Text.Trim())
                    ViewState.Add("searchparam", "1," + ewModelNumber.Text.Trim().ToUpper() + "," + "-")
                    ViewState.Add("pageIndex", 0)
                    ViewState.Add("sortCommand", "")
                    If ewmodels.Tables(0).Rows.Count = 0 Then
                        Search1ErrorLabel.Text = "No Extended Warranty models found."
                        dgEW.DataSource = Nothing
                        dgEW.CurrentPageIndex = 0
                        dgEW.DataBind()
                    Else
                        dgEW.DataSource = ewmodels
                        dgEW.CurrentPageIndex = 0
                        dgEW.DataBind()
                    End If
                End If
            Catch ex As Exception
                Search1ErrorLabel.Visible = True
                Search1ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub SearchEW_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SearchEW.Click
            Try
                Search2ErrorLabel.Text = ""

                If (ewDescription.Text.Trim() = "" And ewItemNumber.Text.Trim() = "") Then
                    Search2ErrorLabel.Text = "You must enter at least one search criteria."
                Else
                    ew = New ExtendedWarrantyManager
                    ewmodels = ew.getewmodelsbyItemNoOrDesc(ewItemNumber.Text.Trim().ToUpper(), ewDescription.Text.Trim().ToUpper())
                    ViewState.Add("searchparam", "2," + ewItemNumber.Text.Trim().ToUpper() + "," + ewDescription.Text.Trim().ToUpper())
                    ViewState.Add("pageIndex", 0)
                    ViewState.Add("sortCommand", "")
                    If ewmodels.Tables(0).Rows.Count = 0 Then
                        Search2ErrorLabel.Text = "No Extended Warranty Models found."
                        dgEW.DataSource = Nothing
                        dgEW.CurrentPageIndex = 0

                        dgEW.DataBind()
                    Else
                        dgEW.DataSource = ewmodels
                        dgEW.CurrentPageIndex = 0

                        dgEW.DataBind()
                    End If
                End If
            Catch ex As Exception
                Search2ErrorLabel.Visible = True
                Search2ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub formFieldInitialization()
            ewDescription.Attributes("onkeydown") = "SetTheFocusButton(event, 'SearchEW')"
            ewItemNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'SearchEW')"
            ewModelNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'SearchModel')"
        End Sub

        Protected Sub dgEW_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEW.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If e.Item.Cells(3).Text = "-1" Then
                        e.Item.Cells(3).Text = "<span class=""redAsterick"">Call " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail + "</span>"
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgEW_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgEW.PageIndexChanged
            Try
                If Not ViewState.Item("searchparam") Is Nothing Then
                    Dim strsearchParam() As String = ViewState.Item("searchparam").ToString().Split(",".ToCharArray())
                    If strsearchParam.Length > 0 Then
                        Select Case strsearchParam(0)
                            Case "1"
                                ew = New ExtendedWarrantyManager
                                ewmodels = ew.EWModelSearch(strsearchParam(1))
                                If ewmodels.Tables(0).Rows.Count = 0 Then
                                    Search1ErrorLabel.Text = "No Extended Warranty models found."
                                End If
                            Case "2"
                                ew = New ExtendedWarrantyManager
                                ewmodels = ew.getewmodelsbyItemNoOrDesc(strsearchParam(1), strsearchParam(2))
                                If ewmodels.Tables(0).Rows.Count = 0 Then
                                    Search2ErrorLabel.Text = "No Extended Warranty Models found."
                                End If
                        End Select
                    End If
                    If ewmodels.Tables(0).Rows.Count = 0 Then
                        dgEW.DataSource = Nothing
                        dgEW.CurrentPageIndex = 0
                        dgEW.DataBind()
                    Else

                        Dim dV As DataView
                        dV = ewmodels.Tables(0).DefaultView()
                        dV.Sort = ViewState.Item("sortCommand").ToString()

                        dgEW.DataSource = dV
                        dgEW.CurrentPageIndex = e.NewPageIndex
                        ViewState.Add("pageIndex", e.NewPageIndex)
                        dgEW.DataBind()
                    End If
                Else

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgEW_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgEW.SortCommand
            Try
                If Not ViewState.Item("searchparam") Is Nothing Then
                    Dim strsearchParam() As String = ViewState.Item("searchparam").ToString().Split(",".ToCharArray())
                    If strsearchParam.Length > 0 Then
                        Select Case strsearchParam(0)
                            Case "1"
                                ew = New ExtendedWarrantyManager
                                ewmodels = ew.EWModelSearch(strsearchParam(1))
                                If ewmodels.Tables(0).Rows.Count = 0 Then
                                    Search1ErrorLabel.Text = "No Extended Warranty models found."
                                End If
                            Case "2"
                                ew = New ExtendedWarrantyManager
                                ewmodels = ew.getewmodelsbyItemNoOrDesc(strsearchParam(1), strsearchParam(2))
                                If ewmodels.Tables(0).Rows.Count = 0 Then
                                    Search2ErrorLabel.Text = "No Extended Warranty Models found."
                                End If
                        End Select
                    End If

                    If ewmodels.Tables(0).Rows.Count = 0 Then
                        dgEW.DataSource = Nothing
                        dgEW.CurrentPageIndex = 0
                        dgEW.DataBind()
                    Else


                        Dim dV As DataView
                        dV = ewmodels.Tables(0).DefaultView()
                        dV.Sort = e.SortExpression
                        ViewState.Add("sortCommand", e.SortExpression)

                        dgEW.DataSource = dV
                        dgEW.CurrentPageIndex = ViewState.Item("pageIndex").ToString()
                        dgEW.DataBind()
                    End If
                Else

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            If Not ViewState.Item("sortCommand") Is Nothing Then
                If ViewState.Item("sortCommand").ToString() <> "" Then
                    Select Case ViewState.Item("sortCommand").ToString()
                        Case "EW_Code"
                            dgEW.Columns(2).HeaderStyle.BackColor = Color.FromName("#99a8b5")
                            dgEW.Columns(0).HeaderStyle.BackColor = Color.FromName("#D5DEE9")
                            '
                        Case "Model_Name"
                            dgEW.Columns(0).HeaderStyle.BackColor = Color.FromName("#99a8b5")
                            dgEW.Columns(2).HeaderStyle.BackColor = Color.FromName("#D5DEE9")
                        Case Else
                            dgEW.Columns(2).HeaderStyle.BackColor = Color.FromName("#D5DEE9")
                            dgEW.Columns(0).HeaderStyle.BackColor = Color.FromName("#D5DEE9")
                    End Select
                End If

            End If

        End Sub
    End Class

End Namespace
