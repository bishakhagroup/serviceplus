Imports System.Data
Imports System.Drawing
Imports System.Globalization
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class CreditCards
        Inherits SSL

#Region " Web Form Designer Generated Code "
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            isProtectedPage(True)
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub
#End Region

#Region "Page members"
        Const conControlsPerRow As Int32 = 3
        Const conNumberOfCCsToSave As Int16 = 4
        Const conExpiresInDays As Int16 = 45
        Const conDefaultFormFieldFocus As String = "CreditCardNicknameDropdownList"

        Private updateCreditCard As Boolean = False
        Public focusFormField As String
        Private customer As Customer
        Private CCNo As String = ""
        Private usCulture As New CultureInfo("en-US")
        'Dim objUtilties As Utilities = New Utilities
#End Region

#Region "Events"
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            If (CreditCardNicknameDropdownList.SelectedIndex > 0) Then
                If (MasterErrorLabel.Text.Length > 0) Then
                    CardNumber.Text = ""
                End If
            End If
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load, Me.Load
            Try
                If Not IsPostBack Then
                    If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                    customer = HttpContextManager.Customer
                    CreditCardNicknameDropdownList.Visible = (CreditCardNicknameDropdownList.Items.Count > 0)
                    CreditCardLabel.Visible = (CreditCardNicknameDropdownList.Items.Count > 0)
                    Dim validCards = From c In customer.CreditCards Where c.HideCardFlag = False
                    If customer.CreditCards.Length = 0 Or validCards.Count = 0 Then 'Is Nothing Then
                        btnCCSaved.Visible = False
                    Else
                        btnCCSaved.Visible = True
                    End If

                    pnlNewCCrd.Visible = False
                    pnlSavedCCrd.Visible = False
                    PnlNickName.Visible = False
                    FormFieldInitialization()
                    SetFocusField(conDefaultFormFieldFocus)
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub CreditCardNicknameDropdownList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles CreditCardNicknameDropdownList.SelectedIndexChanged
            PnlNickName.Visible = True
            pnlNewCCrd.Visible = False
            pnlSavedCCrd.Visible = True

            ProcessPage(sender, e)
            If CreditCardNicknameDropdownList.SelectedIndex > 0 Then
                CreditCardLabel.ForeColor = Color.Black
            End If
        End Sub

        Protected Sub CCMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            CardNumber.Text = ""
            PopulateYearddl()
        End Sub

        Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnDelete.Click
            ' ASleight - Renamed the button from "UpdateProfile", since its usage is deleting saved cards.
            Try
                ProcessPage(sender, e)
                pnlNewCCrd.Visible = False
                pnlSavedCCrd.Visible = False
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnCreditcardSave_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCreditcardSave.Click
            Dim rgxAlphaNumeric As String = "^[a-zA-Z0-9]*$"
            Dim rgxSpecialChar As String = "[!#-$ %*=+:;,?~@^&()_{}{}|\'<>?/]"
            Dim isFormValid As Boolean = True

            'If CreditCardNickname.Text = "" Then
            '    MasterErrorLabel.Visible = True
            '    MasterErrorLabel.CssClass = "redAsterick"
            '    CreditCardNickNameLabel.ForeColor = Color.Red
            '    MasterErrorLabel.Text = "Please enter the Nick Name."
            '    focusFormField = CreditCardNickname.ID
            'Else
            '    btnCCSaved.Visible = True
            '    processPage(sender, e)
            '    CreditCardNickname.Text = ""
            '    CreditCardNickNameLabel.ForeColor = Color.Black
            'End If
            Try
                If String.IsNullOrWhiteSpace(CreditCardNickname.Text) Then
                    isFormValid = False
                    CreditCardNickNameLabel.ForeColor = Color.Red
                    MasterErrorLabel.Text = Resources.Resource.el_Warning_NickName '"Please enter the Nick Name."
                    focusFormField = CreditCardNickname.ID
                End If

                If CreditCardNickname.Text.Length > 1 And CreditCardNickname.Text.Length <= 17 Then
                    If CreditCardNicknameDropdownList.Items.FindByText(CreditCardNickname.Text) Is Nothing Or CreditCardNickname.Text = CreditCardNicknameDropdownList.SelectedValue Then
                        If Not Regex.IsMatch(CreditCardNickname.Text, rgxAlphaNumeric) Then isFormValid = False
                        If Regex.IsMatch(CreditCardNickname.Text, rgxSpecialChar) Then
                            isFormValid = False
                            MasterErrorLabel.Text = Resources.Resource.el_req_NickName '"Please enter Nickname without any spaces or special characters."
                            CreditCardNickNameLabel.CssClass = "redAsterick"
                        End If
                    Else
                        isFormValid = False
                        MasterErrorLabel.Text = Resources.Resource.el_UniqueCreditCard + vbCrLf '"You must select a unique Credit Card Nickname."
                        CreditCardNickNameLabel.CssClass = "redAsterick"
                    End If
                Else
                    isFormValid = False
                    MasterErrorLabel.Text = Resources.Resource.el_ValidLength_NickName + vbCrLf '"Nicknames must be between 1 and 17 characters."
                    CreditCardNickNameLabel.CssClass = "redAsterick"
                End If

                If isFormValid Then
                    btnCCSaved.Visible = True
                    ProcessPage(sender, e)
                    CreditCardNickname.Text = ""
                    CreditCardNickNameLabel.ForeColor = Color.Black
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnCCSaved_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCCSaved.Click
            Try
                PnlNickName.Visible = True
                CreditCardNicknameDropdownList.Visible = True
                CreditCardLabel.Visible = True
                pnlNewCCrd.Visible = False
                pnlSavedCCrd.Visible = False

                btnCreditcardSave.Visible = False
                btnCancel.Visible = True
                btnDelete.Visible = True
                ProcessPage(sender, e)
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
            Response.Redirect("Credit-Cards.aspx")
            PnlNickName.Visible = True
            pnlNewCCrd.Visible = False
            pnlSavedCCrd.Visible = False
        End Sub

        Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
            ' ASleight 2017-10-11 - Fixing issues with dates
            Dim thisMonth As Integer = 0
            Dim thisYear As Integer = 2000
            Dim thisDay As Integer = 0
            'Dim thisDate As String = CType(thisMonth.ToString + "/" + thisDay.ToString() + "/" + thisYear.ToString(), Date)
            Dim thisDate As Date
            Dim CCnumber() As String

            Try
                pnlNewCCrd.Visible = True
                pnlSavedCCrd.Visible = True
                PnlNickName.Visible = False

                btnCreditcardSave.Visible = True
                btnCancel.Visible = False
                btnDelete.Visible = False

                CreditCardType.Items.Insert(0, Session("CreditCardType").ToString())
                CCnumber = Session("token").ToString().Split("-")
                CardNumber.Text = "XXXXXXXXXXXX" & CCnumber(2).ToString()
                thisMonth = Integer.Parse(Session("ExpirationMonth").ToString())
                thisYear = Integer.Parse(Session("ExpirationYear").ToString())
                thisDay = Date.DaysInMonth(thisYear, thisMonth)
                'Dim thisDate As String = CType(thisMonth.ToString + "/" + thisDay.ToString() + "/" + thisYear.ToString(), Date)
                thisDate = New Date(thisYear, thisMonth, thisDay)

                PopulateExpirationDropdownList()
                CreditCardType.Items.FindByValue(Session("CreditCardType").ToString()).Selected = True
                txtCCType.Text = CreditCardType.SelectedItem.Text

                'CCMonth.Items.FindByValue(Format(CType(thisDate.ToString(), Date), "MM").ToString()).Selected = True
                CCMonth.SelectedValue = thisDate.ToString("MM")   ' Ensures months are '01' instead of '1'
                txtMonth.Text = CCMonth.SelectedItem.Text

                PopulateYearddl()
                If CCYear.Items.FindByValue(thisYear) Is Nothing Then CCYear.Items.Add(New ListItem(thisYear, thisYear))
                CCYear.SelectedValue = thisYear
                txtYear.Text = thisYear
                'If CCYear.Items.FindByValue(Format(CType(thisDate.ToString(), Date), "yyyy").ToString()) IsNot Nothing Then
                '    CCYear.SelectedValue = (Format(CType(thisDate.ToString(), Date), "yyyy").ToString())
                '    txtYear.Text = CCYear.SelectedItem.Text
                'Else
                '    CCYear.Items.Add(New ListItem(Format(CType(thisDate.ToString(), Date), "yyyy").ToString(), Format(CType(thisDate.ToString(), Date), "yyyy").ToString()))
                '    CCYear.SelectedValue = (Format(CType(thisDate.ToString(), Date), "yyyy").ToString())
                '    txtYear.Text = CCYear.SelectedItem.Text
                'End If
            Catch ex As Exception
                Utilities.LogMessages($"Credit-Cards.btnHidden_Click - ERROR - Year = {thisYear}, Month = {thisMonth}, Day = {thisDay}, Date = {thisDate}, Formatted = {thisDate.ToString("MM")}")
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "Methods"

        Private Sub ProcessPage(ByVal sender As System.Object, ByRef e As EventArgs)
            Try
                If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                customer = HttpContextManager.Customer
                Dim callingControl As String = String.Empty
                If sender.ID IsNot Nothing Then callingControl = sender.ID.ToString()
                Select Case callingControl
                    Case btnCCSaved.ID
                        PopulateCreditCardSection() 'customer)
                    Case btnCreditcardSave.ID, btnDelete.ID
                        UpdateThisCustomerProfile() 'customer)
                    Case CreditCardNicknameDropdownList.ID
                        GetCreditCardData() 'customer)
                End Select
            Catch ex As Exception
                handleError(ex)
            End Try
        End Sub

        Private Sub UpdateThisCustomerProfile()
            Dim objPCILogger As New PCILogger()
            'Dim FlgSuccess As Boolean = False
            Try
                If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                customer = HttpContextManager.Customer

                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "updateThisCustomerProfile"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.Update
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "updateThisCustomerProfile Success"

                focusFormField = ""
                MasterErrorLabel.Text = ""
                If String.IsNullOrWhiteSpace(CreditCardNickname.Text) Then
                    Try
                        HideCreditCardData(customer.CreditCards)
                        'FlgSuccess = True
                    Catch ex As Exception
                        'FlgSuccess = False
                    End Try
                    CreditCardNicknameDropdownList.Items.Clear()
                    PopulateCreditCardSection()
                    ClearCreditCardFields()
                    MasterErrorLabel.Text = Resources.Resource.el_WarningCreditCard '"Your Credit Card has been deleted sucessfully."
                    Exit Sub
                End If
                If UpdateCreditCardData() Then
                    HttpContextManager.Customer = customer  ' Save the updated Customer detail to Session
                    ClearCreditCardFields()
                    If MasterErrorLabel.Text = String.Empty Then MasterErrorLabel.Text = Resources.Resource.el_SavedCreditCard '"Your Credit Card has been saved sucessfully."
                Else
                    MasterErrorLabel.Text = $"{Resources.Resource.el_UnableUpdate} {MasterErrorLabel.Text}"     '  "Unable to update profile. " 
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "updateThisCustomerProfile Failed. " & ex.Message
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Sub HideCreditCardData(ByRef thisCreditCards() As CreditCard)
            Dim cm As New CustomerManager
            Dim objPCILogger As New PCILogger()
            objPCILogger.IndicationSuccessFailure = "Failure"
            Try
                If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                customer = HttpContextManager.Customer

                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "hideCreditCardData"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Delete
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                For Each c As CreditCard In thisCreditCards
                    If c.NickName = CreditCardNicknameDropdownList.SelectedValue And c.HideCardFlag = False And (CreditCardNicknameDropdownList.SelectedIndex <> 0 And CreditCardNicknameDropdownList.SelectedValue.ToLower() <> "addnewcard") Then
                        CCNo = c.CreditCardNumber
                        Utilities.LogDebug($"Credit-Cards.HideCreditCardData - Hiding card with Sequence: {c.SequenceNumber}")
                        cm.HideCreditCard(c)
                        c.HideCardFlag = True
                        objPCILogger.CreditCardMaskedNumber = c.CreditCardNumberMasked
                        objPCILogger.CreditCardSequenceNumber = c.SequenceNumber
                        objPCILogger.Message = "Nickname : " & c.NickName
                        objPCILogger.IndicationSuccessFailure = "Success"
                        Exit For
                    End If
                Next
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.Message = ex.Message
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

#End Region

#Region "credit card section"

        Private Sub PopulateCreditCardSection()
            If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
            customer = HttpContextManager.Customer
            PopulateCreditCardDropdownList(customer.CreditCards)
            PopulateCardTypeDropdownList(customer.CreditCards)
            PopulateExpirationDropdownList()
        End Sub

        Private Sub GetCreditCardData()
            Dim objPCILogger As New PCILogger()
            Dim CCnumber() As String
            Dim expiryDate As DateTime
            Dim expiryYear As String

            Try
                If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                customer = HttpContextManager.Customer

                CardNumber.Text = ""
                CCMonth.SelectedItem.Selected = False
                CCYear.SelectedItem.Selected = False
                CreditCardNickname.Text = ""

                For Each thisCreditCard As CreditCard In customer.CreditCards
                    If thisCreditCard.NickName = CreditCardNicknameDropdownList.SelectedValue And thisCreditCard.HideCardFlag = False Then
                        PopulateCardTypeDropdownList(customer.CreditCards)
                        CCnumber = thisCreditCard.CreditCardNumber.Split("-")
                        CardNumber.Text = "XXXXXXXXXXXX" & CCnumber(2)

                        objPCILogger.CreditCardMaskedNumber = thisCreditCard.CreditCardNumberMasked
                        objPCILogger.CreditCardSequenceNumber = thisCreditCard.SequenceNumber
                        objPCILogger.EventOriginApplication = "ServicesPLUS"
                        objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                        objPCILogger.EventOriginMethod = "GetCreditCardData"
                        objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                        objPCILogger.OperationalUser = Environment.UserName
                        objPCILogger.CustomerID = customer.CustomerID
                        objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                        objPCILogger.EmailAddress = customer.EmailAddress '6524
                        objPCILogger.SIAM_ID = customer.SIAMIdentity
                        objPCILogger.LDAP_ID = customer.LdapID '6524
                        objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                        objPCILogger.EventType = EventType.View
                        objPCILogger.Message = "Nickname : " & thisCreditCard.NickName
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                        txtCCType.Text = thisCreditCard.Type.Description
                        CCNo = thisCreditCard.CreditCardNumber
                        ' Set up the Card Expiry date, being careful of non-US cultures.
                        expiryDate = Convert.ToDateTime(thisCreditCard.ExpirationDate, usCulture)
                        CCMonth.SelectedValue = expiryDate.ToString("MM")
                        txtMonth.Text = CCMonth.SelectedItem.Text
                        PopulateYearddl() 'Added for fixing Bug# 272
                        expiryYear = expiryDate.ToString("yyyy")
                        If CCYear.Items.FindByValue(expiryYear) Is Nothing Then
                            CCYear.Items.Add(New ListItem(expiryYear, expiryYear))
                        End If
                        CCYear.SelectedValue = expiryYear
                        txtYear.Text = CCYear.SelectedItem.Text
                        Exit For
                    End If
                Next
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function DoesCardNickNameExist(ByVal thisCreditCards() As CreditCard, ByVal cardNickName As String) As Boolean
            Try
                If thisCreditCards Is Nothing Then Return False
                If String.IsNullOrWhiteSpace(cardNickName) Then Return True

                For Each thisCreditCard As CreditCard In thisCreditCards
                    If Not String.IsNullOrWhiteSpace(thisCreditCard.NickName) Then
                        '-- included hidecardflag in the condition as it doesn't make sense just to compare the nickname when the card can be deleted by the customer
                        If (thisCreditCard.NickName.ToLower() = cardNickName.ToLower() And Not thisCreditCard.HideCardFlag) Then
                            Return True
                        End If
                    End If
                Next
            Catch ex As Exception
                Return True
            End Try

            Return False
        End Function

        Private Function UpdateCreditCardData() As Boolean
            Dim custMan As New CustomerManager
            Dim thisCreditCardType As CreditCardType
            Dim thisCreditCard As CreditCard
            Dim expiryDate As Date
            Dim thisMonth As Integer
            Dim thisYear As Integer
            Dim thisDay As Integer
            Dim updateCC As Boolean = False
            Dim returnValue As Boolean = False
            Dim objPCILogger As New PCILogger()
            Dim debugString As String = $"Credit-Cards.UpdateCreditCardData - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"

            Try
                If HttpContextManager.Customer Is Nothing Then Response.Redirect("default.aspx")
                customer = HttpContextManager.Customer
                MasterErrorLabel.Text = ""

                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "updateCreditCardData"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.EventType = EventType.Update
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID

                thisCreditCardType = GetCreditCardType()
                thisCreditCard = New CreditCard(thisCreditCardType)
                If thisCreditCard IsNot Nothing Then
                    If btnCreditcardSave.ID.ToLower() = "btnCreditcardSave".ToLower() Then
                        ' NEW CARD
                        debugString &= $"- New Card. Type: {thisCreditCardType.CreditCardTypeCode}.{Environment.NewLine}"
                        objPCILogger.EventType = EventType.Add
                        If Not DoesCardNickNameExist(customer.CreditCards, CreditCardNickname.Text.Trim()) Then
                            '-- removed the condition to check for max number of saved cards to 4 per bug 191
                            thisCreditCard.Type.CreditCardTypeCode = thisCreditCardType.CreditCardTypeCode
                            thisCreditCard.CreditCardNumber = Session("token").ToString()
                            thisCreditCard.NickName = CreditCardNickname.Text.Trim()
                            thisCreditCard.Customer = customer
                            thisCreditCard.HideCardFlag = False     ' ASleight - Added to stop inserting nulls to a boolean field.

                            ' If IsNumeric(CCMonth.SelectedValue) And IsNumeric(CCMonth.SelectedValue) Then
                            ' ASleight - Grabbing Session variables without checking for nulls. Errors will happen here if Session is lost.
                            thisMonth = Integer.Parse(Session("ExpirationMonth").ToString())
                            thisYear = Integer.Parse(Session("ExpirationYear").ToString())
                            thisDay = Date.DaysInMonth(thisYear, thisMonth)
                            expiryDate = New Date(thisYear, thisMonth, thisDay)
                            thisCreditCard.ExpirationDate = expiryDate.ToString("MM/dd/yyyy")

                            CCNo = thisCreditCard.CreditCardNumber
                            customer.AddCreditCard(thisCreditCard)
                            objPCILogger.CreditCardMaskedNumber = thisCreditCard.CreditCardNumberMasked
                            debugString &= $"- Card data parse. Nickname: {thisCreditCard.NickName}, Expiry: {thisCreditCard.ExpirationDate}.{Environment.NewLine}"

                            '-- clear the nick name drop down and rebuild it to include new card.
                            CreditCardNicknameDropdownList.Items.Clear()
                            PopulateCreditCardDropdownList(customer.CreditCards)
                            returnValue = True
                        Else
                            MasterErrorLabel.Text = Resources.Resource.el_UniqueCreditCard '"You must select a unique Credit Card Nickname."
                            objPCILogger.Message = "Nickname not Unique"
                            If String.IsNullOrEmpty(focusFormField) Then focusFormField = CreditCardNickname.ID
                            CreditCardNickNameLabel.ForeColor = Color.Red
                        End If
                    Else
                        ' UPDATE CARD - Check each property
                        debugString &= $"- Update Card. Type: {thisCreditCardType.CreditCardTypeCode}.{Environment.NewLine}"
                        For Each thisCreditCard In customer.CreditCards
                            If thisCreditCard.NickName = CreditCardNicknameDropdownList.SelectedValue And Not thisCreditCard.HideCardFlag Then
                                ' Check Card Type
                                If thisCreditCard.Type.CreditCardTypeCode <> CreditCardType.SelectedValue Then
                                    thisCreditCard.Type.CreditCardTypeCode = CreditCardType.SelectedValue
                                    updateCC = True
                                End If

                                ' Check Card Number
                                If CardNumber.Text.Trim() <> hideCCNumbers(thisCreditCard.CreditCardNumber) Then
                                    thisCreditCard.CreditCardNumber = CardNumber.Text.Trim()
                                    CCNo = thisCreditCard.CreditCardNumber
                                    updateCC = True
                                End If

                                ' Check Expiry Date
                                If IsNumeric(CCMonth.SelectedValue) And IsNumeric(CCYear.SelectedValue) Then
                                    thisMonth = Integer.Parse(CCMonth.SelectedValue)
                                    thisYear = Integer.Parse(CCYear.SelectedValue)
                                    thisDay = Date.DaysInMonth(thisYear, thisMonth)
                                    expiryDate = New Date(thisYear, thisMonth, thisDay)

                                    If expiryDate.ToString("MM/yyyy") <> ($"{CCMonth.SelectedValue}/{CCYear.SelectedValue}") Then
                                        thisCreditCard.ExpirationDate = expiryDate.ToString("MM/dd/yyyy")
                                        updateCC = True
                                    End If
                                End If

                                ' Check Nickname
                                If thisCreditCard.NickName <> CreditCardNickname.Text.Trim() Then
                                    If Not String.IsNullOrWhiteSpace(CreditCardNickname.Text) AndAlso Not DoesCardNickNameExist(customer.CreditCards, CreditCardNickname.Text.Trim()) Then
                                        thisCreditCard.NickName = CreditCardNickname.Text.Trim()
                                        CreditCardNicknameDropdownList.SelectedValue = thisCreditCard.NickName
                                        updateCC = True
                                    Else
                                        MasterErrorLabel.Text = Resources.Resource.el_UniqueCreditCard ' "You must select a unique Credit Card Nickname."
                                        objPCILogger.Message = "Nickname not Unique"
                                        If focusFormField = "" Then focusFormField = CreditCardNickname.ID
                                        CreditCardNickNameLabel.ForeColor = Color.Red
                                        Exit For
                                    End If
                                End If
                                If updateCC Then thisCreditCard.Customer = customer
                                objPCILogger.CreditCardMaskedNumber = thisCreditCard.CreditCardNumberMasked
                                returnValue = True
                                Exit For
                            End If
                        Next
                    End If

                    debugString &= $"- Calling UpdateCustomerCreditCard. Customer card count: {customer.CreditCards.Count}.{Environment.NewLine}"
                    custMan.UpdateCustomerCreditCard(customer)
                    If returnValue Then
                        For Each AddedCreditCard As CreditCard In customer.CreditCards
                            If AddedCreditCard.NickName = CreditCardNickname.Text.Trim() And AddedCreditCard.HideCardFlag = False Then
                                objPCILogger.CreditCardSequenceNumber = AddedCreditCard.SequenceNumber
                                debugString &= $"- Found the new card in Customer.CreditCards. Sequence: {AddedCreditCard.SequenceNumber}.{Environment.NewLine}"
                            End If
                        Next
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.Message = "Nick Name : " & CreditCardNickname.Text.Trim()
                    End If
                Else
                    MasterErrorLabel.Text = Resources.Resource.el_WarningCreditCard_4 '"Credit Cards do not exist."
                    objPCILogger.Message = MasterErrorLabel.Text.ToString()
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.Message = ex.Message
            Finally
                Utilities.LogDebug(debugString)
                objPCILogger.PushLogToMSMQ()    ' ASleight - Why was this commented out?
            End Try

            Return returnValue
        End Function

        Private Function GetCreditCardType() As CreditCardType
            Dim paymentManager As New PaymentManager
            Dim thisCreditCardType As CreditCardType = Nothing
            Dim creditCardTypes() As CreditCardType = paymentManager.GetCreditCardTypes()
            For Each ccType As CreditCardType In creditCardTypes
                If ccType.CardType.ToUpper() = Session("CreditCardType").ToString().ToUpper() Then
                    thisCreditCardType = ccType
                    Exit For
                End If
            Next
            Return thisCreditCardType
        End Function

        Private Sub ClearCreditCardFields()
            CardNumber.Text = ""
            If CreditCardNicknameDropdownList.Items.Count > 0 Then
                CreditCardNicknameDropdownList.SelectedItem.Selected = False
            End If
            If CreditCardNicknameDropdownList.Items.Count = 0 Then
                CreditCardNicknameDropdownList.Visible = False
                CreditCardLabel.Visible = False
            Else
                CreditCardNicknameDropdownList.Visible = True
                CreditCardLabel.Visible = True
            End If
            CreditCardType.SelectedItem.Selected = False
            CCMonth.SelectedItem.Selected = False
            CCYear.SelectedItem.Selected = False
            CreditCardNickname.Text = ""
            CreditCardType.SelectedIndex = 0
            'storeCreditCard.Checked = False
            'storeCreditCard.Visible = False
            'hideCardCheckbox.Checked = False
            'deletecardTR.Visible = False
            CCMonth.SelectedIndex = 0
            CCYear.SelectedIndex = 0
            txtMonth.Text = ""
            txtYear.Text = ""
            txtCCType.Text = ""
            PopulateYearddl() 'Added for fixing Bug# 272
        End Sub

#End Region

#Region "populate dropdown list"

        Private Sub PopulateCreditCardDropdownList(ByVal creditCards() As CreditCard)
            Dim ccYear As Integer
            Dim ccMonth As Integer
            Dim currentYear As Integer
            Dim currentMonth As Integer
            Dim cardDate As DateTime

            Try
                If CreditCardNicknameDropdownList.Items.Count = 0 Then
                    Dim defaultItem As New ListItem("Please select a Credit Card", "") 'el_WarningCreditCard_1
                    If creditCards.Length > 0 Then creditCards = GetNickNamesFromCC(creditCards)

                    'bind credit card object to dropdown list control
                    If creditCards Is Nothing Then Return
                    currentYear = DateTime.Today.Year
                    currentMonth = DateTime.Today.Month

                    For Each c As CreditCard In creditCards
                        Try
                            cardDate = Convert.ToDateTime(c.ExpirationDate, usCulture)
                            ccYear = cardDate.Year
                            ccMonth = cardDate.Month
                        Catch ex As Exception
                            '-- make the card invalid
                            ccYear = DateTime.Today.Year - 1
                        End Try

                        If Not String.IsNullOrWhiteSpace(c.NickName) And Not c.HideCardFlag And (ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth)) Then
                            CreditCardNicknameDropdownList.Items.Add(c.NickName)
                        End If
                    Next

                    If CreditCardNicknameDropdownList.Items.Count > 0 Then
                        CreditCardNicknameDropdownList.Items.Insert(0, defaultItem)
                        defaultItem.Selected = True
                    Else
                        btnCCSaved.Visible = False
                        MasterErrorLabel.Text = Resources.Resource.el_WarningCreditCard_2 '"Credit Card not added."
                    End If
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateCardTypeDropdownList(ByVal creditCards() As CreditCard)
            Dim paymentManager As New PaymentManager
            Dim theseCCTypes() As CreditCardType
            Dim defaultItem As New ListItem

            Try
                If CreditCardType.Items.Count = 0 Then
                    defaultItem.Text = Resources.Resource.el_WarningCreditCard_3 '"Select a Credit Card Type"
                    defaultItem.Value = ""
                    defaultItem.Selected = True

                    theseCCTypes = paymentManager.GetCreditCardTypes()

                    'bind card type array to dropdown list control        
                    CreditCardType.DataSource = theseCCTypes
                    CreditCardType.DataTextField = "Description"
                    CreditCardType.DataValueField = "CreditCardTypeCode"
                    CreditCardType.DataBind()
                    CreditCardType.Items.Insert(0, defaultItem)
                Else
                    If Not String.IsNullOrWhiteSpace(CreditCardNicknameDropdownList.SelectedValue) Then
                        For Each creditCard As CreditCard In creditCards
                            If Not String.IsNullOrWhiteSpace(creditCard.NickName) Then
                                If creditCard.NickName = CreditCardNicknameDropdownList.SelectedValue And creditCard.HideCardFlag = False Then
                                    For Each thisListItem In CreditCardType.Items
                                        If thisListItem.Value.ToString = creditCard.Type.CreditCardTypeCode Then
                                            CreditCardType.SelectedItem.Selected = False
                                            thisListItem.Selected = True
                                            Exit For
                                        End If
                                    Next
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If
            Catch ex As Exception
                MasterErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateExpirationDropdownList()
            CCMonth.Items.Clear()
            CCMonth.Items.Add(New ListItem("Month", ""))
            CCMonth.Items.Add(New ListItem("01 - January", "01"))
            CCMonth.Items.Add(New ListItem("02 - February", "02"))
            CCMonth.Items.Add(New ListItem("03 - March", "03"))
            CCMonth.Items.Add(New ListItem("04 - April", "04"))
            CCMonth.Items.Add(New ListItem("05 - May", "05"))
            CCMonth.Items.Add(New ListItem("06 - June", "06"))
            CCMonth.Items.Add(New ListItem("07 - July", "07"))
            CCMonth.Items.Add(New ListItem("08 - August", "08"))
            CCMonth.Items.Add(New ListItem("09 - September", "09"))
            CCMonth.Items.Add(New ListItem("10 - October", "10"))
            CCMonth.Items.Add(New ListItem("11 - November", "11"))
            CCMonth.Items.Add(New ListItem("12 - December", "12"))
            PopulateYearddl()
        End Sub

        Private Sub PopulateYearddl()
            Dim currentDate As Date = DateTime.Today()
            Dim currentYear As Integer
            Dim i As Integer

            currentYear = currentDate.Year()
            CCYear.Items.Clear()

            'If Me.CCMonth.SelectedIndex > 0 Then
            '    Dim selectedMonth As Integer = Me.CCMonth.SelectedValue
            '    If selectedMonth < currentDate.Month Then
            '        currentYear += 1
            '    End If
            '    Dim i As Integer
            '    For i = currentYear To currentYear + 20
            '        CCYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            '    Next i
            'End If

            For i = currentYear To currentYear + 20
                CCYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            Next i

            Dim list0 As New ListItem("Year", "")
            CCYear.Items.Insert(0, list0)
            CCYear.CssClass = "tableData"
            CCYear.SelectedValue = ""
        End Sub

        Private Function GetNickNamesFromCC(ByVal creditCards As CreditCard()) As Array
            Dim ccArrayList As New ArrayList
            Dim thisCC As CreditCard = Nothing

            '-- added the hidecardflag in the condition to retrieve cards that are not deleted
            For Each thisCC In creditCards
                If Not String.IsNullOrWhiteSpace(thisCC.NickName) And Not thisCC.HideCardFlag Then
                    ccArrayList.Add(thisCC)
                End If
            Next

            Return ccArrayList.ToArray(thisCC.GetType())
        End Function
#End Region

#Region "helpers"

        Private Sub SetFocusField(ByVal callingControl As String)
            If focusFormField Is Nothing Then
                Select Case callingControl
                    Case Page.ID
                        focusFormField = conDefaultFormFieldFocus
                    Case btnDelete.ID
                        focusFormField = conDefaultFormFieldFocus
                    Case CreditCardNicknameDropdownList.ID
                        focusFormField = CreditCardNickname.ID
                    Case Else
                        focusFormField = conDefaultFormFieldFocus
                End Select
            End If
        End Sub

        Private Sub FormFieldInitialization()
            CardNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnDelete')"
            CreditCardNickname.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnDelete')"
        End Sub

        Public Function SelectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)
            Return creditNo
        End Function

        Public Function GetAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = String.Empty

            If Not String.IsNullOrWhiteSpace(thisCCNo) Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If

            Return returnValue
        End Function
#End Region
    End Class
End Namespace
