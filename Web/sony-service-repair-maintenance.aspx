<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="vb" SmartNavigation="False" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_service_repair_maintenance"
    EnableViewStateMac="true" CodeFile="sony-service-repair-maintenance.aspx.vb"
    CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.repair_mn_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" background="images/repair_lrg.gif" bgcolor="#363d45" height="82"
                                                style="width: 465px">
                                                <br>
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4()%></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <table width="100%" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <h2 class="headerTitle"><%=Resources.Resource.repair_sny_pg_svcrqst_msg()%></h2>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <table width="100%" border="0" role="presentation">
                                                                <tr>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="finderSubhead" style="height: 21px;">
                                                                        <%=Resources.Resource.repair_mn_tg_msg_2()%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 17px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 425px; height: 22px;">
                                                &nbsp;
                                            </td>
                                            <td width="238" style="height: 22px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 17px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 425px">
                                                <p class="promoCopyBold">
                                                    <%=Resources.Resource.repair_svc_main_msg()%>
                                                </p>
                                            </td>
                                            <td align="center">
                                                &nbsp; &nbsp; &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 17px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 425px">
                                                <table border="0" cellpadding="2" cellspacing="1" style="width: 100%" role="presentation">
                                                    <tr>
                                                        <td style="height: 23px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 100px; height: 14px;">
                                                            <p class="promoCopyBold">
                                                                <%=Resources.Resource.repair_dptsvc_msg()%>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-size: 8pt; color: #666666; font-family: Arial"><%=Resources.Resource.repair_svc_main_msg_1()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p class="bodyCopy">
                                                                <strong><%=Resources.Resource.repair_md_msg()%>&nbsp;</strong><SPS:SPSTextBox ID="ModelNumber" runat="server" CssClass="bodyCopy"
                                                                   ></SPS:SPSTextBox>
                                                                <asp:Label ID="lbErrMsglModel" runat="server" CssClass="bodycopy" Font-Bold="True"
                                                                    ForeColor="Red" Text="Label" Visible="False" Width="153px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <asp:ImageButton ID="btnRequestDepotService" runat="server" ImageUrl="<%$Resources:Resource,repair_snypg_mdl_type_3_img%>"
                                                                ToolTip="Request Depot Service" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <span class="redAsterick"><%=Resources.Resource.repair_newchksts_msg()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="#">
                                                                <img src="<%=Resources.Resource.repair_snypg_mdl_type_2_img()%>" onclick="javascript:window.location='sony-repair-status.aspx';" border="0" alt="Repair Service Status" id="imbSearchRepairStatus" /></a><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p class="promoCopyBold">
                                                                <%=Resources.Resource.repair_fs_msg()%>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span style="font-size: 8pt; color: #666666; font-family: Arial"><%=Resources.Resource.repair_fs_msg_1()%>
                                                                <br />
                                                                <%=Resources.Resource. repair_fs_msg_2()%>
                                                                <br />
                                                                <%=Resources.Resource.repair_fs_msg_3()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 13px">
                                                            <span class="bodyCopy"><strong><%=Resources.Resource.el_rgn_State()%>&nbsp;</strong></span>
                                                            <asp:DropDownList ID="ddlStates" runat="server" Style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 13px; color: black" />
                                                            <asp:Label ID="lblErrStateMsg" runat="server" Font-Bold="True" CssClass="bodycopy"
                                                                ForeColor="Red" Text="Label" Visible="False" Width="208px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="imgBtnGo" runat="server" ImageAlign="AbsBottom" ImageUrl="<%$Resources:Resource,repair_snypg_mdl_type_1_img%>"
                                                                ToolTip="Request Field Service" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center">
                                                <table width="100%" border="0" role="presentation">
                                                    <tr align="center">
                                                        <td>
                                                            <asp:HyperLink ID="topPrevious" runat="server" CssClass="tableData"><%=Resources.Resource.el_Previous()%></asp:HyperLink><asp:HyperLink
                                                                ID="topPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous"></asp:HyperLink><img
                                                                    height="20" src="images/spacer.gif" width="20"><asp:HyperLink ID="topNext" runat="server"
                                                                        CssClass="tableData"><%=Resources.Resource.el_Next()%></asp:HyperLink><asp:HyperLink ID="topNextImg" runat="server"
                                                                            CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next"></asp:HyperLink>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:GridView ID="GridPartsGroup" runat="server" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:BoundField AccessibleHeaderText="Group ID" DataField="RNGNUMBER" HeaderText="rngNumber"
                                                                        Visible="False" />
                                                                    <asp:HyperLinkField AccessibleHeaderText="From" DataTextField="Range" HeaderText="Model Number Range">
                                                                        <HeaderStyle HorizontalAlign="Center" Width="100%" Wrap="false" />
                                                                    </asp:HyperLinkField>
                                                                </Columns>
                                                                <RowStyle BackColor="#F2F5F8" CssClass="tableData" />
                                                                <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" />
                                                                <AlternatingRowStyle BackColor="White" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td>
                                                            <asp:HyperLink ID="btmPreviousImg" runat="server" CssClass="tableData"><IMG src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous"></asp:HyperLink><asp:HyperLink
                                                                ID="btmPrevious" runat="server" CssClass="tableData"><%=Resources.Resource.el_Previous()%></asp:HyperLink><img
                                                                    height="20" src="images/spacer.gif" width="20"><asp:HyperLink ID="btmNext" runat="server"
                                                                        CssClass="tableData"><%=Resources.Resource.el_Next()%></asp:HyperLink><asp:HyperLink ID="btmNextImg" runat="server"
                                                                            CssClass="tableData"><IMG src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next"></asp:HyperLink>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="#top">
                                                                <img src="<%=Resources.Resource.repair_sny_type_3_img()%>" width="78" height="28" border="0" alt="Back to Top"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 17px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 425px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td align="center">
                                                &nbsp; &nbsp; &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 17px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 34px; width: 425px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 34px" align="center">
                                                <a href="#top"></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
