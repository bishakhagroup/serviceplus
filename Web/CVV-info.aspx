<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.CVV_info" EnableViewStateMac="true" CodeFile="CVV-info.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS-What is CVV?</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0">
    <form method="post" runat="server" id="form1">
        <table width="400" border="0" cellpadding="0">
            <tr>
                <td>
                    <table height="65" width="400" border="0">
                        <tr height="38" width="400">
                            <td colspan="2" width="400" background="images/sp_int_popup_login_hdrbkgd_onepix.gif">
                                <h1 class="headerText">&nbsp;&nbsp;&nbsp;&nbsp;What is CVV?</h1>
                            </td>
                        </tr>
                        <tr height="10">
                            <td colspan="2" background="images/sp_int_popup_login_hdrbkgd_lowr.gif">
                                &nbsp;
                            </td>
                        </tr>
                        <tr height="1">
                            <td cellpadding="0">
                                <table height="1" width="400" border="0">
                                    <td width="21" colspan="1" height="9" background="images/sp_int_header_btm_right.gif"></td>
                                    <td height="1" width="378" background="images/sp_int_header_btm_left_onepix.gif"></td>
                                    <td height="1" width="1" background="images/sp_int_header_btm_right.gif"></td>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divLogin" style="overflow: auto; scrollbar-track-color: AliceBlue; height: 330;" runat="server" visible="true">
                        <table width="370" border="0" cellpadding="2" class="redAsterick">
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="tableheader">
                                    Card Verification Value (CVV) provides an additional level of online fraud protection. The number is located on your credit card and is generally three to four digits long. See below for examples of number placement on the cards we accept.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td align="left" class="tableheader">
                                    <u>AMEX</u>
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="bodycopy" valign="top">
                                    <table>
                                        <tr valign="top" class="bodycopy">
                                            <td>
                                                <img src="images/amex_card_cvv.jpg" alt="Example of the location of an American Express card's CVV number." /></td>
                                            <td>A four digit non-embossed number on the face of the card.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td width="20">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td align="left" class="tableheader">
                                    <u>Discover</u>
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="bodycopy" valign="top">
                                    <table>
                                        <tr valign="top" class="bodycopy">
                                            <td>
                                                <img src="images/discover_card_cvv.jpg" alt="Example of the location of a Discover card's CVV number." /></td>
                                            <td>A three digit non-embossed number on the back of the card printed within the signature panel after the account number.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="20">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td align="left" class="tableheader">
                                    <u>Mastercard</u>
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="bodycopy" valign="top">
                                    <table>
                                        <tr valign="top" class="bodycopy">
                                            <td>
                                                <img src="images/master_card_cvv.jpg" alt="Example of the location of a MasterCard's CVV number." /></td>
                                            <td>A three digit non-embossed number on the back of the card printed within the signature panel after the account number.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="20">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td align="left" class="tableheader">
                                    <u>VISA</u>
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="bodycopy" valign="top">
                                    <table>
                                        <tr valign="top" class="bodycopy">
                                            <td>
                                                <img src="images/visa_card_cvv.jpg" alt="Example of the location of a Visa card's CVV number." /></td>
                                            <td>A three digit non-embossed number on the back of the card printed within the signature panel after the account number.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <br />
                    <img src="<%=Resources.Resource.img_btnCloseWindow()%>" onclick="window.close();" alt="Close this window." /></td>
            </tr>
        </table>
    </form>
</body>
</html>
