<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ConfirmOrder"
    EnableViewStateMac="true" CodeFile="ConfirmOrder.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_ConfirmOrder%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function checkSession() {
            if (document.getElementById("txtHidden").value === "load") {
                document.getElementById("txtHidden").value = "done";
            }
            else {
                location.href = "SessionExpired.aspx";
            }
        }
        function hideCompleteOrderBtn() {
            document.getElementById('btnConfirmOrder').style.display = "none";
        }
    </script>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;" onload="javascript:checkSession();" onkeydown="javascript:SetTheFocusButton(event, 'btnConfirmOrder');">
    <form id="form2" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br/>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="5" src="images/spacer.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">
                                                            &nbsp;
                                                        </td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            </span>
                                                        </td>
                                                        <td width="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_ConfirmOrder%></h2>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="20" height="20">
                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                </td>
                                <td>
                                    <input type="hidden" id="txtHidden" value="load">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2">
                                                <asp:Label ID="labelAction" runat="server" CssClass="redAsterick" Text="<%$ Resources:Resource, el_OrderMsg1%>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2">
                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <asp:Table ID="TableOrderDetail" runat="server" />
                                                <img height="5" src="images/spacer.gif" width="670" alt=""><br/>
                                                <img height="5" src="images/spacer.gif" width="670" alt=""><br/>
                                                <table width="300" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3" height="28">
                                                            <span class="tableData">
                                                                <asp:Label ID="labelSpecialTax" runat="server"><span class="redAsterick">*</span> <%=Resources.Resource.el_RecyclingFee1%>
                                                                </asp:Label><asp:HyperLink ID="specialTaxHyperLink" runat="server" Font-Underline="True"
                                                                    Font-Names="Arial"> <%=Resources.Resource.el_RecyclingFee2%></asp:HyperLink></span>
                                                        </td>
                                                    </tr>
                                                    <%--8091 starts--%>
                                                    <%--<tr>
															<td colSpan="3" height="28" class="tableData"><span class="tableData"><asp:label id="lblshowkitinfo" CssClass="tableData" ForeColor="Red" runat="server">Each kit will ship when all of its parts are available.</asp:label></span></td>
														</tr>--%>
                                                    <%--8091 ends--%>
                                                    <tr>
                                                        <td colspan="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="122">
                                                            <asp:ImageButton ID="ReturnToCartImageButton" runat="server" ImageUrl="<%$ Resources:Resource, img_btnReturnToCart%>"
                                                                AlternateText="<%$ Resources:Resource, el_ReturnToCart%>"></asp:ImageButton>&nbsp;
                                                        </td>
                                                        <td width="15">
                                                            <img height="1" src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td valign="top">
                                                            <asp:ImageButton ID="btnConfirmOrder" OnClientClick="hideCompleteOrderBtn()" runat="server"
                                                                ImageUrl="<%$ Resources:Resource, img_btnConfirmOrder%>" AlternateText="<%$ Resources:Resource, el_SubmitOrder%>"
                                                                CommandName="Confirm"></asp:ImageButton>
                                                        </td>
                                                        <td width="15">
                                                            <img height="1" src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" id="tdLegend" runat="server">
                                                            <span style="font-family: Wingdings; color: red; font-size: 12px;">v</span><span class="bodyCopy"> &nbsp;
                                                            <%=Resources.Resource.el_LimitedPromoPrice%>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br/>
                                                <img height="30" src="images/spacer.gif" width="670" alt=""><br/>
                                                <span class="legalCopy">
                                                    <a class="legalLink" href="" onclick="window.open('TC2.aspx'); return false;"><%=Resources.Resource.el_LimitedWaranty%></a></span>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
