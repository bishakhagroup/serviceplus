<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-repair.aspx.vb" Inherits="ServicePLUSWebApp.sony_repair"
    EnableViewStateMac="true" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_mn_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        var redirectTo;
        function SetFocus() {
            txtModelNumber.focus();
        }
        //Added and Modified by Sneha on 17th oct 2013 for showing popup
        //code written by Umesh Dubey
        function OrderLookUp(cnt, models, url) {
            var lstModels = new Array(models.split(','));
            redirectTo = url;
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");
            $("#divMessage").dialog({ // dialog box
                title: '<span class="modalpopup-title"><%=Resources.Resource.repair_mdl_msg()%></span>',
                height: 260,
                width: 560,
                modal: true,
                position: 'top',
                close: function () {
                    redirectLocation();
                }
            });
            var content = "<span class='modalpopup-content'><%=Resources.Resource.repair_pup_mdlsrch_msg()%>" + "<br/>" + "<%=Resources.Resource.repair_pup_mdlsrch_msg_1()%></span>"
            content += "<br>" + "<table class='modalpopup-content' style='margin-left:100px;'>"
            content += '<tr><td width="100px"><u><%=Resources.Resource.repair_pup_mdlsrch_msg_3()%></u></td><td width="100px"><u><%=Resources.Resource.repair_pup_mdlsrch_msg_2()%></u></td></tr>';
            for (i = 0; i < cnt; i++) {
                var lstmodel = new Array(lstModels[0][i].split(':'));
                content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1] + '</td></tr>';
            }
            content += "</table>"
            $("#divMessage").append(content); //message to display in divmessage div
            $("#divMessage").append('<br/> <br/><a><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
            return false;
        }

        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }
        function redirectLocation() {
            window.location.href = redirectTo;
        }
    </script>

    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="javascript:SetFocus();">
    <form id="Form1" method="post" runat="server">
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto; font-weight: bold;" runat="server"></div>
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background-image: url('images/sp_left_bkgd.gif')">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 465px; text-align: right; background: #363d45 url('images/repair_banner.JPG')">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4()%></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <h1 class="finderSubhead"><%=Resources.Resource.repair_mn_tg_msg_2()%></h1>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="width: 465px; background: #f2f5f8;">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="width: 238px; background: #99a8b5;">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td>
                                            </td>
                                            <td width="60%">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""><asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
                                            </td>
                                            <td width="40%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="60%">
                                                <h2 class="headerTitle"><%=Resources.Resource.repair_reqsc_msg %></h2>  <%-- "Request Service" title --%>
                                            </td>
                                            <td width="40%">
                                                <h2 class="headerTitle"><%=Resources.Resource.repair_chksts_msg %></h2> <%-- "Check Status" title --%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td class="tableHeader" width="60%">
                                                &nbsp;<%=Resources.Resource.repair_chksts_msg_1()%>     <%-- "for your Sony professional equipment." --%>
                                            </td>
                                            <td width="40%" style="font-size: 12pt; font-family: Times New Roman">
                                                <span class="redAsterick"><%=Resources.Resource.repair_newchksts_msg()%></span> <%-- "New: Click below to get depot service status." --%>
                                            </td>
                                        </tr>
                                        <tr style="font-size: 12pt; font-family: Times New Roman">
                                            <td>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="60%">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td align="left" valign="top" rowspan="2" width="40%">
                                                <%-- CHECK STATUS Table --%>
                                                <table height="90%" width="100%" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="2">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="txtModelChkStatus" class="tableHeader"><%=Resources.Resource.repair_snysvc_mdl_msg%></label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtModelChkStatus" runat="server" CssClass="bodyCopy" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="txtSerialChkStatus" class="tableHeader"><%=Resources.Resource.repair_snysvc_srl_msg%></label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtSerialChkStatus" runat="server" CssClass="bodyCopy" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblOr1" runat="server" CssClass="tableHeader" Text="<%$Resources:Resource,repair_snysvc_srl_msg_2%>" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="txtNotificationChkStatus" class="tableHeader"><%=Resources.Resource.repair_snysvc_srl_msg_1%></label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtNotificationChkStatus" runat="server" CssClass="bodyCopy" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblOr2" runat="server" CssClass="tableHeader" Text="<%$Resources:Resource,repair_snysvc_srl_msg_2%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="txtPhoneChkStatus" class="tableHeader"><%=Resources.Resource.repair_snysvc_srl_msg_3 %></label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtPhoneChkStatus" runat="server" CssClass="bodyCopy" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <img height="25" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ImageButton ID="imgCheckStatus" runat="server" ImageUrl="<%$ Resources:Resource,repair_snypg_mdl_type_2_img %>"
                                                                AlternateText="Check Service Status" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="font-size: 12pt; font-family: Times New Roman">
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td class="promoCopy" width="60%">
                                                <%-- REQUEST SERVICE Table --%>
                                                <table width="100%" cellpadding="3" role="presentation">
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <h3 class="promoCopyBold"><%=Resources.Resource.repair_dptsvc_msg %></h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <span class="bodyCopy"><%=Resources.Resource.repair_dptsvc_msg_1()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <p class="bodyCopy">
                                                                <label for="txtModelNumber" style="font-weight: bold;"><%=Resources.Resource.repair_md_msg%></label>
                                                                <SPS:SPSTextBox ID="txtModelNumber" runat="server" CssClass="bodyCopy" />&nbsp;
                                                                <asp:Label ID="lblErrMsgModel" runat="server" Font-Bold="True" ForeColor="Red" Text="Label" Visible="False" />
                                                                <asp:Label ID="lblNoMatch" runat="server" ForeColor="Red" Text="Label" Visible="False" />
                                                            </p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <asp:ImageButton ID="imgDepotService" runat="server" ImageUrl="<%$ Resources:Resource,repair_snypg_mdl_type_3_img %>"
                                                                AlternateText="Request Depot Service" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" height="20" alt="" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <h3 class="promoCopyBold"><%=Resources.Resource.repair_fs_msg %></h3>
                                                        </td>
                                                    </tr>
                                                    <tr valign="bottom">
                                                        <td style="width: 443px" class="bodyCopy">
                                                            <%=Resources.Resource.repair_fs_msg_1()%>
                                                            <br />
                                                            <%=Resources.Resource.repair_fs_msg_2()%>
                                                            <br />
                                                            <%=Resources.Resource.repair_fs_msg_3()%></>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px" class="bodyCopy">
                                                            <label for="ddlStates" style="font-weight: bold;"><%=Resources.Resource.repair_state_msg()%>&nbsp;</label>
                                                            <asp:DropDownList ID="ddlStates" runat="server" Style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 13px; color: black" />
                                                            <asp:Label ID="lblErrMsgState" runat="server" CssClass="bodyCopy" Font-Bold="True"
                                                                ForeColor="Red" Text="Label" Visible="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ImageButton ID="imgBtnGo" runat="server" ImageUrl="<%$ Resources:Resource,repair_snypg_mdl_type_1_img %>"
                                                                ImageAlign="AbsBottom" AlternateText="Request Field Service" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <%--<a href="#top"></a>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                            <td colspan="2" width="60%">
                                                <%If divrprShow = True Then%>
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <uc1:SPSPromotion ID="Promotions1" DisplayPage="sony_repair" Type="2" runat="server" />
                                                        </td>
                                                        <td width="16">
                                                            <img src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <uc1:SPSPromotion ID="SPSPromotionBottomRight" DisplayPage="sony_repair" Type="3" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%End If%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <table align="center" border="0" role="presentation">
                                        <tr align="center">
                                            <td align="center">
                                                <a class="promoCopyBold" href="sony-service-repair-maintenance.aspx" runat="server" visible="true"><%=Resources.Resource.hplnk_sonysvc_modellst()%></a>&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>

                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
