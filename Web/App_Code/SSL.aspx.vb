Imports System.Globalization
Imports System.IO
Imports System.Net
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Public Class SSL
        Inherits System.Web.UI.Page

#Region "Data Members"
        '-- Constants --    
        Protected Const notLoggedInRedirectPage As String = "default.aspx"
        Private Const errorHandlerPage As String = "ErrorHandler.aspx"
        Protected Const NullDateTime As String = "01/01/0001"
        Protected Const CON_POPUP_FEATURES As String = "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=400,height=400"

        '-- Variables
        Private PageIsProtected As Boolean = False
        Public errorText As String = ""
        'Dim Utilities As Utilities = New Utilities
        Protected jsSerializer As New System.Web.Script.Serialization.JavaScriptSerializer
        Protected jsVars As New Hashtable
        Public PageLanguage As String = "en"    ' 2018-08-23 Added for WCAG requiring <html lang="en"> type tags.

        '-- Enums --
        Public Enum enumAccountType
            NonAccount = 0
            AccountHolder = 1
            Internal = 2
            Unknown = 3
            PendingAccountHolder = 4
        End Enum
#End Region

#Region " Page Events "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected Overrides Sub InitializeCulture()
            'Utilities.LogMessages("SSL.InitializeCulture - SelectedLang = " & HttpContextManager.SelectedLang)
            Dim ci As New CultureInfo(HttpContextManager.SelectedLang)
            Page.Culture = ci.ToString()
            Page.UICulture = ci.ToString()
        End Sub

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            If HttpContextManager.GlobalData.IsFrench Then PageLanguage = "fr"  ' 2018-08-23 Added for WCAG requiring <html lang="en"> type tags.
            ViewStateUserKey = Session.SessionID
        End Sub
#End Region

#Region "Methods"
        Public Function GetRequestContextValue() As Object
            Dim objHTTPRequestObject = New HTTPRequestObject With {
                .HTTP_X_Forward_For = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR"),
                .RemoteADDR = HttpContextManager.GetServerVariableValue("REMOTE_ADDR"),
                .HTTPReferer = HttpContextManager.GetServerVariableValue("HTTP_REFERER"),
                .HTTPURL = HttpContextManager.GetServerVariableValue("URL"),
                .HTTPUserAgent = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")
            }
            Return objHTTPRequestObject
        End Function

        Private Sub SetUniqueRequestKey()
            Dim strRequestKey As String
            Dim objCookie As HttpCookie
            strRequestKey = System.Guid.NewGuid().ToString()
            objCookie = New HttpCookie("requestkey", strRequestKey)
            Response.Cookies.Add(objCookie)
            Session.Add("RequestKey", strRequestKey)

            If ConfigurationData.GeneralSettings.NoCachPage.IndexOf([GetType]().Name.ToUpper) < 0 Then
                Response.Buffer = True
                Response.ExpiresAbsolute = Date.Now.Subtract(New TimeSpan(1, 0, 0, 0))
                Response.Expires = 0
                Response.CacheControl = "no-cache"
            End If
        End Sub

        Public Function ResolveURLParameter() As String
            Dim prmNVCollection As NameValueCollection = Request.QueryString()
            Dim strParamString As String = String.Empty
            Dim iKeyCount As Short = 0

            If prmNVCollection.HasKeys Then
                For iKeyCount = 0 To prmNVCollection.Keys.Count - 1
                    If strParamString <> String.Empty Then strParamString &= "&"
                    strParamString &= prmNVCollection.Keys(iKeyCount) + "=" + prmNVCollection(iKeyCount)
                Next
            End If
            Return strParamString
        End Function

        Public Function ReturnCharacterSub(ByVal strSelect As String) As String
            Dim strCharacter As String = ""
            For Each strChar In strSelect.ToCharArray()
                Select Case strChar.ToString()
                    Case "~"
                        strCharacter = strSelect.Replace(strChar, "+01+")
                        Exit For
                    Case "/"
                        strCharacter = strSelect.Replace(strChar, "+02+")
                        Exit For
                    Case "{"
                        strCharacter = strSelect.Replace(strChar, "+03+")
                        Exit For
                    Case "}"
                        strCharacter = strSelect.Replace(strChar, "+04+")
                        Exit For
                    Case "|"
                        strCharacter = strSelect.Replace(strChar, "+05+")
                        Exit For
                    Case "\"
                        strCharacter = strSelect.Replace(strChar, "+06+")
                        Exit For
                    Case "^"
                        strCharacter = strSelect.Replace(strChar, "+07+")
                        Exit For
                    Case "["
                        strCharacter = strSelect.Replace(strChar, "+08+")
                        Exit For
                    Case "]"
                        strCharacter = strSelect.Replace(strChar, "+09+")
                        Exit For
                    Case "`"
                        strCharacter = strSelect.Replace(strChar, "+10+")
                        Exit For
                    Case "%"
                        strCharacter = strSelect.Replace(strChar, "+11+")
                        Exit For
                    Case "#"
                        strCharacter = strSelect.Replace(strChar, "+12+")
                        Exit For
                    Case "@"
                        strCharacter = strSelect.Replace(strChar, "+13+")
                        Exit For
                    Case "<"
                        strCharacter = strSelect.Replace(strChar, "+14+")
                        Exit For
                    Case ">"
                        strCharacter = strSelect.Replace(strChar, "+15+")
                        Exit For
                    Case "+"
                        strCharacter = strSelect.Replace(strChar, "+16+")
                        Exit For
                        '"{", "}", "|", "\", "^", "~",  "[", "]", and "`".
                    Case Else
                        strCharacter = strSelect
                        Exit Select
                End Select
            Next

            Return strCharacter
        End Function

        Public Function ReturnQueryString() As String
            Dim loop1, loop2 As Integer
            Dim arr1(), arr2() As String
            Dim coll As NameValueCollection
            Dim strValue As String = "?"
            Try
                coll = Request.QueryString
                arr1 = coll.AllKeys
                For loop1 = 0 To arr1.GetUpperBound(0)
                    strValue += Server.HtmlEncode(arr1(loop1).ToString())
                    arr2 = coll.GetValues(loop1)
                    For loop2 = 0 To arr2.GetUpperBound(0)
                        strValue += "=" + Server.HtmlEncode(arr2(loop2)).ToString()
                    Next loop2
                    strValue += "&"
                Next loop1
                strValue = strValue.Substring(0, strValue.Length - 1)
            Catch ex As Exception
                Return strValue
            End Try
            Return strValue
        End Function

        Private Sub isSecureConnection()
            'If Not Request.IsSecureConnection And New ConfigHandler().GetCurrentEnvironment = "Production" Then
            '    Dim sb As StringBuilder = New StringBuilder
            '    sb.Append("https://")
            '    sb.Append(Request.Url.Host + ":" + "91" + Request.Url.PathAndQuery)
            '    Response.Redirect(sb.ToString(), True)
            'End If
        End Sub

        Public Sub isProtectedPage(ByVal isProtected As Boolean)
            PageIsProtected = isProtected
            If isProtected Then
                Try
                    If HttpContextManager.Customer Is Nothing Then Response.Redirect(notLoggedInRedirectPage, True)
                Catch thisEx As System.Threading.ThreadAbortException
                    '-- do nothing expected error            
                End Try
            End If
        End Sub

#Region "Helper methods"

        ''' <summary>
        ''' Check for attempted XSS (Cross-Site Scripting) by looking for special characters
        ''' that may appear in attempts.
        ''' </summary>
        Protected Function IsValidString(ByVal input As String) As Boolean
            ' Check for Cross Site Scripting (such as <%whs/onmouseover=confirm()>)
            'Dim validCharacters As Regex = New Regex("[^a-zA-Z0-9\.\-\/\\]")
            'If (validCharacters.Replace(input, " ") <> input) Then
            '	Return False
            'End If
            If (String.IsNullOrEmpty(input)) Then
                Return True
            ElseIf (input.Contains("<") OrElse input.Contains(">") OrElse input.Contains("%") OrElse input.Contains(",")) Then
                Return False
            ElseIf (input.Contains("(") OrElse input.Contains(")") OrElse input.Contains(":") OrElse input.Contains(";")) Then
                Return False
            End If
            Return True
        End Function

        Protected Function IsValidPhone(ByVal pPhno As String) As Boolean
            Dim reg As New Regex("^([(]?\d{3}[-)]\d{3}-\d{4})*$")
            Return reg.IsMatch(pPhno)
        End Function

        Protected Function FilterString(ByRef text As String, ByRef filteredText As String) As String
            Dim delimStr As String = filteredText
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim split As String() = text.Split(delimiter)
            Dim s As String
            Dim newText As String = ""
            For Each s In split
                newText = newText + s
            Next s
            Return newText
        End Function

        Protected Function FormatDate(ByRef dateTime As Date) As String
            If dateTime = NullDateTime Then
                Return "TBD"
            Else
                Return dateTime.ToString("MM/dd/yyyy")
            End If
        End Function

        Private Function getDeliveryMethod(ByRef selectedSoftware As SelectedSoftware) As ShoppingCartItem.DeliveryMethod
            Try
                Dim returnValue As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship
                Dim willCustomerDownload As Boolean = selectedSoftware.Download
                Dim willCustomerBeShipped As Boolean = selectedSoftware.Ship

                If willCustomerDownload Then
                    If willCustomerBeShipped Then
                        Return ShoppingCartItem.DeliveryMethod.DownloadAndShip
                    Else
                        Return ShoppingCartItem.DeliveryMethod.Download
                    End If
                End If
            Catch ex As Exception
                Return Nothing
            End Try
            Return Nothing
        End Function

        Public Function getCustomer(ByVal siamId As String) As Customer
            Dim thisCustomer As Customer

            Try
                thisCustomer = New Customer
                Dim customerManager As New CustomerManager
                If Not siamId Is Nothing Then thisCustomer = Nothing ''customerManager.GetCustomer(siamId)
            Catch
                thisCustomer = Nothing
            End Try

            Return thisCustomer
        End Function

#Region "User-Related Methods"
        Public Function getUsersAccountType(ByVal siamId As String) As enumAccountType
            Dim returnValue As enumAccountType = enumAccountType.Unknown

            Try
                If Not String.IsNullOrWhiteSpace(siamId) Then
                    Dim siamSecurityAdmin As New SecurityAdministrator
                    Dim _usertype As String = siamSecurityAdmin.GetUser(siamId).TheUser.UserType

                    If _usertype = "A" Or _usertype = "P" Then
                        returnValue = enumAccountType.AccountHolder
                    ElseIf _usertype = "I" Then
                        returnValue = enumAccountType.Internal
                    ElseIf _usertype = "C" Then
                        returnValue = enumAccountType.NonAccount
                    ElseIf _usertype = "U" Then
                        returnValue = enumAccountType.Unknown
                    Else
                        returnValue = enumAccountType.Unknown
                    End If
                End If
            Catch
                returnValue = enumAccountType.Unknown
            End Try

            Return returnValue
        End Function

        Public Function getAccountTypes(ByVal siamId As String) As String()
            Dim thisUT() As String
            Try
                Dim thisSA As New SecurityAdministrator
                thisUT = thisSA.GetUserTypes.SiamPayLoad
            Catch
                thisUT = Nothing
            End Try

            Return thisUT
        End Function

        Public Function IsUserActive(ByVal customer As Customer) As Boolean
            Dim returnValue As Boolean = False

            Try
                If getUserStatusCode(customer) = 1 Then '6668
                    returnValue = True
                    customer.IsActive = returnValue
                End If
            Catch
                returnValue = False
                customer.IsActive = returnValue
            End Try
            Return returnValue
        End Function

        Private Function getUserStatusCode(ByRef customer As Customer) As Integer
            Dim thisStatusCode As Integer = 0
            Try
                Dim thisSA As New SecurityAdministrator
                thisStatusCode = thisSA.GetUser(customer.SIAMIdentity).TheUser.UserStatusCode
            Catch
                thisStatusCode = 0
            End Try

            Return thisStatusCode
        End Function
#End Region

#Region "Populate Controls"
        ''' <summary>
        ''' Appends a list of all States or Provinces into a provided DropDownList. Selection of US vs Canada
        ''' is based on the Session-stored HttpContextManager.GlobalData.SalesOrganization
        ''' </summary>
        ''' <param name="ddlState">The HTML Control to populate.</param>
        ''' <param name="UseLongNames">Whether to display the states as full names or two-letter abbreviations.</param>
        Public Sub PopulateStateDropdownList(ByRef ddlState As DropDownList, Optional ByVal UseLongNames As Boolean = False)
            Try
                Dim cm As New CatalogManager
                Dim statedetails() As StatesDetail
                statedetails = cm.StatesBySalesOrg(HttpContextManager.GlobalData)

                ' In case the page added a default item, append new databound items
                ddlState.AppendDataBoundItems = True
                ddlState.DataSource = statedetails
                ddlState.DataValueField = "StateAbbr"
                ddlState.DataTextField = If(UseLongNames, "StateName", "StateAbbr")
                ddlState.DataBind()
            Catch ex As Exception
                'Throw New Exception(ex.Message)    ' Catching and Throwing a new error removes the current stack trace. Don't do this.
                Throw ex
            End Try
        End Sub
#End Region

        Public Function hideCCNumbers(ByVal thisCCNumber As String) As String
            Dim returnValue As String = String.Empty

            If thisCCNumber <> String.Empty Then
                Dim str() As String
                str = thisCCNumber.Split("-")
                returnValue = ("xxxxxxxxxxxx" + str(2))
            End If

            Return returnValue
        End Function

        Public Function ViewStateSize() As String
            Dim returnValue As String

            returnValue = "function()" +
             "{" +
             "var buf = document.forms[0]['__VIEWSTATE'].value;" +
             "alert('View state is ' + buf.length + ' bytes');" +
             "}"
            Return returnValue
        End Function

#End Region

#Region "   Shopping Cart and Catalog   "
        ''' <summary>
        ''' Sets up a Hashtable of variables from Resource files and Configuration Data to pass back to the calling JavaScript.
        ''' This is a workaround because static .js files cannot include ASP tags.
        ''' </summary>
        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Function InitCartVariables() As Hashtable
            If jsVars.Count = 0 Then
                jsVars.Add("atc_ex_InvalidPartNumber", Resources.Resource.atc_ex_InvalidPartNumber)
                jsVars.Add("atc_ex_InvalidQuantityForPart", Resources.Resource.atc_ex_InvalidQuantityForPart)
                jsVars.Add("atc_ex_InvalidPartAndQuantity", Resources.Resource.atc_ex_InvalidPartAndQuantity)
                jsVars.Add("atc_ex_DifferentCart", Resources.Resource.atc_ex_DifferentCart)
                jsVars.Add("atc_title_OrderStatus", Resources.Resource.atc_title_OrderStatus)
                jsVars.Add("atc_title_OrderExceptions", Resources.Resource.atc_title_OrderExceptions)
                jsVars.Add("atc_title_QuickOrderStatus", Resources.Resource.atc_title_QuickOrderStatus)
                jsVars.Add("atc_title_QuickOrderExceptions", Resources.Resource.atc_title_QuickOrderExceptions)
                jsVars.Add("atc_AddingItem", Resources.Resource.atc_AddingItem)
                jsVars.Add("atc_ToCart", Resources.Resource.atc_ToCart)
                jsVars.Add("prg_PleaseWait", Resources.Resource.prg_PleaseWait)
                jsVars.Add("img_btnCloseWindow", Resources.Resource.img_btnCloseWindow)
                jsVars.Add("quickOrderMaxItems", ConfigurationData.GeneralSettings.General.QuickOrderMaxItems)
                jsVars.Add("orderUploadMaxItems", ConfigurationData.GeneralSettings.General.OrderUploadMaxItems)
            End If
            Return jsVars
        End Function

        Public Function getAllShoppingCarts(ByRef thisCustomer As Customer, ByRef errorMessageLabel As Label) As ShoppingCart()
            Dim carts(2) As ShoppingCart
            carts(ShoppingCart.enumCartType.Other) = getShoppingCart(thisCustomer, ShoppingCartItem.DeliveryMethod.Ship, errorMessageLabel)
            'carts(ShoppingCart.enumCartType.DownLoad) = Nothing 'Now we don't need the download only
            'code added by sneha on 10/02/2014
            carts(ShoppingCart.enumCartType.Download) = getShoppingCart(thisCustomer, ShoppingCartItem.DeliveryMethod.Download, errorMessageLabel)
            carts(ShoppingCart.enumCartType.PrintOnDemand) = Nothing 'Work!!! Now don't implement POD shopping cart 'getShoppingCart(thisCustomer, ShoppingCartItem.DeliveryMethod.PrintOnDemand, errorMessageLabel)
            Return carts
        End Function

        Protected Function getShoppingCart(ByRef thisCustomer As Customer, ByVal deliverType As ShoppingCartItem.DeliveryMethod, ByRef errorMessageLabel As Label) As ShoppingCart
            Dim cart As ShoppingCart = Nothing
            Dim type As ShoppingCart.enumCartType
            Dim shoppingCartManager As New ShoppingCartManager
            Dim debugMessage As String = ""

            If deliverType = ShoppingCartItem.DeliveryMethod.Download Then
                type = ShoppingCart.enumCartType.Download
            Else
                type = ShoppingCart.enumCartType.Other
            End If

            Try
                If thisCustomer Is Nothing Then
                    errorMessageLabel.Text = "Error getting shopping cart. Customer does not exist."
                    Return cart
                End If

                ' If the customer opens a saved cart, make it the current cart
                If Session.Item("carts") IsNot Nothing Then
                    cart = Session.Item("carts")
                    Session.Item(cart.Type.ToString().ToLower()) = cart
                    cart = Nothing
                End If

                ' If a cart isn't saved in Session, create a new one and store it in Session
                If Session.Item(type.ToString().ToLower()) Is Nothing Then
                    cart = shoppingCartManager.CreateShoppingCart(thisCustomer, type)
                    cart.Type = type
                    Session.Item(type.ToString().ToLower()) = cart
                    debugMessage &= $"SSL.getShoppingCart - New cart created. Type: {type.ToString().ToLower()}, Customer: {thisCustomer.EmailAddress}{Environment.NewLine}"
                Else
                    If (type = ShoppingCart.enumCartType.Other) And (Session.Item("NewCart") IsNot Nothing) Then
                        cart = shoppingCartManager.CreateShoppingCart(thisCustomer, type) 'NewCart button is clicked, need to creat a new cart for Other Type
                        cart.Type = type
                        Session.Item(type.ToString().ToLower()) = cart
                        Session.Remove("NewCart") 'remove it so next time it doesn't creat new cart unless NewCart button is clicked again
                        debugMessage &= $"SSL.getShoppingCart - New cart was clicked. Type: {type.ToString().ToLower()}, Customer: {thisCustomer.EmailAddress}{Environment.NewLine}"
                    Else
                        debugMessage &= $"SSL.getShoppingCart - Last Else block hit. Type: {type.ToString().ToLower()}, Customer: {thisCustomer.EmailAddress}{Environment.NewLine}"
                        If TypeOf Session.Item(type.ToString().ToLower()) Is ShoppingCart Then cart = Session.Item(type.ToString().ToLower())
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                errorMessageLabel.Text = ex.Message.ToString()
            End Try

            Return cart
        End Function

        Protected Sub removeAllCartsSessionVariable()
            Session.Remove(ShoppingCart.enumCartType.Other.ToString().ToLower())
            Session.Remove(ShoppingCart.enumCartType.Download.ToString().ToLower())
            Session.Remove(ShoppingCart.enumCartType.PrintOnDemand.ToString().ToLower())
        End Sub

        Public Function IsCurrentCartSameType(ByRef cart As ShoppingCart) As Boolean
            If (Session.Item("carts") Is Nothing) Then
                Return True
            Else
                Dim currentCart As ShoppingCart = Session.Item("carts")
                Return (currentCart.Type = cart.Type)
            End If
        End Function

        ''' <summary>
        ''' Adds the specified part to the customer's Catalog and sets a response in the provided Label control.
        ''' </summary>
        ''' <param name="partNumber">The Part to be added.</param>
        ''' <param name="errorMessageLabel">The Label control for displaying user error messages or feedback.</param>
        Protected Sub AddToCatalog(ByVal partNumber As String, ByRef errorMessageLabel As Label)
            Try
                Dim catMan As New CatalogManager
                Dim parts() As Part
                Dim part As Part = Nothing

                If HttpContextManager.Customer Is Nothing Then
                    errorMessageLabel.Text = GetGlobalResourceObject("Resource", "el_Catalog_MustLogin").ToString()    ' "You must be logged in to add items to MyCatalog."
                    Exit Sub
                End If

                parts = catMan.QuickSearch(String.Empty, partNumber, String.Empty, String.Empty, HttpContextManager.GlobalData)

                If parts?.Length = 1 Then
                    part = parts(0)
                ElseIf parts?.Length > 1 Then
                    part = Array.Find(parts, Function(x) (x.PartNumber = partNumber))
                End If

                If part Is Nothing Then
                    errorMessageLabel.Text = "An error occurred while searching for the part number specified. Please try again."
                    Exit Sub
                ElseIf part.PartNumber.ToUpper() <> part.ReplacementPartNumber.ToUpper() Then
                    errorMessageLabel.Text = "The part you searched for is no longer available. Part number " + part.ReplacementPartNumber + " has replaced it, and has been added to your Catalog instead."
                Else
                    errorMessageLabel.Text = "Part number " + part.PartNumber + " has been added to your Catalog."
                End If

                catMan.AddMyCatalogItem(HttpContextManager.Customer, part)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary> Checks if the User added item(s) to cart before logging in, then handles
        '''  the request with the new customer data. </summary>
        ''' <param name="errorMessageLabel">The Label control for displaying user error messages.</param>
        Protected Sub AddToCartAfterLogin(ByRef errorMessageLabel As Label)
            Try
                ' If the user isn't logged in, or they didn't try "Add to Cart" while logged out, do nothing.
                If HttpContextManager.Customer Is Nothing Or Session("SelectedItemsBeforeLoggedIn") Is Nothing Then Exit Sub

                ' If the user tried to add item(s) to a cart before logging in, add it to cart now.
                Dim productsToAdd As ArrayList = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)

                For Each currentItem As Product In productsToAdd
                    AddToCart(currentItem, HttpContextManager.Customer, errorMessageLabel)
                Next

                ' Clear the Session variable so we don't accidentally add the items again
                Session.Remove("SelectedItemsBeforeLoggedIn")
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function AddToCartWithResults(ByVal partNumber As String, ByVal partQuantity As String) As AddToCartResult
            Dim cartResult As New AddToCartResult
            Dim objPCILogger As New PCILogger()
            Dim cartManager As New ShoppingCartManager
            Dim objSSL As New SSL
            Dim lblErrorMessage As New Label
            Dim customer As Customer = HttpContextManager.Customer
            Dim carts As ShoppingCart()
            Dim cart As ShoppingCart
            Dim addedProduct As Product = Nothing
            Dim output1 As String = String.Empty
            Dim message As String = ""
            Dim debugMessage As String = $"SSL.AddToCartWithResults - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"
            'Dim sPartNumberUpperCase As String = partNumber.ToUpper()

            Try
                ' Start setting up the return object values
                cartResult.success = 0
                partNumber = partNumber.ToUpper()
                cartResult.PartNumber = partNumber
                ' If no Quantity was provided, assume a default value of one. (Web Methods cannot have Optional parameters.)
                If String.IsNullOrEmpty(partQuantity) Then partQuantity = "1"
                cartResult.Quantity = partQuantity

                If customer Is Nothing Then
                    cartResult.success = -1
                    cartResult.message = "Must be logged in, or an error occured loading user data."
                    Return cartResult
                End If

                If customer.UserType = "P" Then
                    cartResult.success = -1
                    cartResult.message = "Your account is still pending approval. You will be unable to place orders until an administrator has approved your account."
                ElseIf (HttpContextManager.GlobalData.IsCanada) And (customer.UserType <> "A") Then
                    ' Canadian Accounts must be Approved Account Holders to place orders.
                    cartResult.success = -1
                    cartResult.message = "Canadian customers must be Approved Account Holders to place orders. Please contact <a href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a> to place a credit card order, or wait for your Account(s) to be validated."
                    Return cartResult
                End If

                ' Check for illegal characters in the provided part number
                If (Not objSSL.IsValidString(partNumber)) Then
                    cartResult.success = -1
                    cartResult.message = "Illegal characters detected in Part Number."
                    Return cartResult
                End If

                'carts = objSSL.getAllShoppingCarts(customer, New Label)    ' This was very effective at hiding errors!
                carts = objSSL.getAllShoppingCarts(customer, lblErrorMessage)
                If Not String.IsNullOrWhiteSpace(lblErrorMessage.Text) Then
                    cartResult.success = -1
                    cartResult.message = lblErrorMessage.Text
                    Return cartResult
                End If
                'If (carts IsNot Nothing) Then Utilities.LogDebug("SSL.AddToCartWithResults - Carts array length: " & carts.Length)
                debugMessage &= $"- partNumber = {partNumber}, partQuantity = {partQuantity}, GlobalData = {HttpContextManager.GlobalData}.{Environment.NewLine}"
                cart = cartManager.AddProductToCartQuickCart(carts, partNumber, partQuantity, True, output1, HttpContextManager.GlobalData)
                debugMessage &= $"- Output1 = {output1}.{Environment.NewLine}"
                HttpContext.Current.Session("carts") = cart
                For Each cartItem As ShoppingCartItem In cart.ShoppingCartItems
                    If (cartItem.Product.PartNumber = partNumber) Then
                        addedProduct = cartItem.Product
                        Exit For
                    End If
                Next
                ' This is just a safety; it should find something in the loop above.
                If addedProduct Is Nothing Then addedProduct = cart.ShoppingCartItems(cart.ShoppingCartItems.Length - 1).Product
                If addedProduct Is Nothing Then
                    debugMessage &= $"SSL.AddToCartWithResults - addedProduct is null twice.{Environment.NewLine}"
                Else
                    debugMessage &= $"SSL.AddToCartWithResults - addedProduct = {addedProduct.PartNumber}.{Environment.NewLine}"
                End If

                cartResult.items = cart.ShoppingCartItems.Length
                cartResult.totalAmount = cart.ShoppingCartTotalPrice.ToString("C")
                cartResult.differentCart = (Not objSSL.IsCurrentCartSameType(cart))

                ' The ShoppingCartManager will set the "IsReplaced" property if the requested part number doesn't match the one returned by SAP.
                If (addedProduct.IsReplaced Or (partNumber <> addedProduct.PartNumber)) Then
                    cartResult.success = 2
                    Dim resMessage As String = Resources.Resource.atc_ReplacementAdded ' "Replacement item {0} added to cart in place of requested item {1} which is no longer available."
                    message = String.Format(resMessage, addedProduct.ReplacementPartNumber, partNumber)
                ElseIf (Not String.IsNullOrEmpty(addedProduct.PartNumber)) AndAlso (addedProduct.PartNumber.ToUpper() = partNumber) Then
                    ' If the PartNumbers match, they got the part they requested.
                    cartResult.success = 1
                    Dim resMessage As String = Resources.Resource.atc_AddedToCart ' " added to the cart."
                    message = addedProduct.PartNumber & resMessage
                Else
                    ' For debugging. This shouldn't happen.
                    message = "Else condition triggered while checking for Replacement Part ..."
                    debugMessage &= $"addedProduct.PartNumber = {addedProduct.PartNumber}, addedProduct.ReplacementPartNumber = {addedProduct.ReplacementPartNumber}, partNumber = {partNumber}.{Environment.NewLine}"
                End If

            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventOriginMethod = "AddToCartWithResults"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "AddToCartWithResults Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
                message &= Utilities.WrapExceptionforUI(ex)
                cartResult.message = message
                Return cartResult
            End Try
            cartResult.message = message
            Return cartResult
        End Function

        ''' <summary>
        ''' Prepares a Product to be added to the cart. Generic method that should be called from the inheriting Page.
        ''' </summary>
        ''' <param name="product">The item to be added to the cart (any type).</param>
        ''' <param name="customer">The Customer object as obtained from the Session Item after login.</param>
        ''' <param name="errorLabel">The Web Label Control to be used for displaying friendly error messages to the user/</param>
        ''' <remarks></remarks>
        Protected Sub AddToCart(ByRef product As Product, ByRef customer As Customer, ByRef errorLabel As Label, Optional ByVal quantity As String = "1")
            ' This shouldn't be possible from the user side, but it never hurts to be sure.
            If product Is Nothing Or customer Is Nothing Then Exit Sub

            If TypeOf product Is SelectedSoftware Then
                AddSoftwareToCart(CType(product, SelectedSoftware), customer, errorLabel)
            ElseIf TypeOf product Is Training Then
                AddTrainingToCart(CType(product, Training), customer, errorLabel)
            ElseIf TypeOf product Is Part Then
                AddPartToCart(CType(product, Part), customer, errorLabel)
            ElseIf TypeOf product Is Kit Then
                AddKitToCart(CType(product, Kit), customer, errorLabel)
            ElseIf TypeOf product Is ExtendedWarrantyModel Then
                AddWarrantyToCart(CType(product, ExtendedWarrantyModel), customer, errorLabel)
            End If
        End Sub

        ''' <summary>
        ''' Internal method of adding a Software item to the cart. Called by the generic AddToCart.
        ''' </summary>
        Private Sub AddSoftwareToCart(ByRef software As SelectedSoftware, ByRef customer As Customer, ByRef errorLabel As Label, Optional ByVal quantity As String = "1")
            Try
                Dim cart As ShoppingCart
                Dim scm As New ShoppingCartManager
                Dim deliveryMethod As ShoppingCartItem.DeliveryMethod = getDeliveryMethod(software)

                If software.ListPrice <= 0 Then
                    errorLabel.Text = "Requested software " + software.KitPartNumber + " not available. Please contact ServicesPLUS support at 1-800-538-7550 and request status on release availability."
                Else
                    ' Fetch a new or existing Shopping Cart based on DeliveryMethod
                    cart = getShoppingCart(customer, deliveryMethod, errorLabel)
                    If Not IsCurrentCartSameType(cart) Then
                        If cart.Type <> ShoppingCart.enumCartType.Download Then
                            errorLabel.Text = "The item has been added to a cart for items to be physically shipped; your previous cart with items for electronic delivery has been saved."
                        Else
                            errorLabel.Text = "The item has been added to a cart for items to be electronically delivered; your previous cart with items for physical shipment has been saved."
                        End If
                    End If

                    scm.AddProductToCart(HttpContextManager.GlobalData, cart, software, ShoppingCartItem.ItemOriginationType.Search, deliveryMethod)
                    Session("carts") = cart ' Set the current cart as the one the item was added to
                End If
            Catch ex As Exception
                errorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Internal method of adding a Training item to the cart. Called by the generic AddToCart.
        ''' </summary>
        Private Sub AddTrainingToCart(ByRef training As Training, ByRef customer As Customer, ByRef errorLabel As Label, Optional ByVal quantity As String = "1")
            Try
                Dim cart As ShoppingCart
                Dim shoppingCartManager As New ShoppingCartManager
                Dim deliveryMethod As ShoppingCartItem.DeliveryMethod

                If (training.Download) Then
                    deliveryMethod = ShoppingCartItem.DeliveryMethod.Download
                Else
                    deliveryMethod = ShoppingCartItem.DeliveryMethod.Ship
                End If

                cart = getShoppingCart(customer, deliveryMethod, errorLabel)

                If Not IsCurrentCartSameType(cart) Then
                    If cart.Type <> ShoppingCart.enumCartType.Download Then
                        errorLabel.Text = "The item has been added to a cart for items to be physically shipped; your previous cart with items for electronic delivery has been saved."
                    Else
                        errorLabel.Text = "The item has been added to a cart for items to be electronically delivered; your previous cart with items for physical shipment has been saved."
                    End If
                End If

                shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, training, ShoppingCartItem.ItemOriginationType.Search, deliveryMethod)
                Session("carts") = cart
            Catch ex As Exception
                errorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        ''' <summary>
        ''' Internal method of adding a Part item to the cart. Called by the generic AddToCart.
        ''' </summary>
        Private Sub AddPartToCart(ByRef part As Part, ByRef customer As Customer, ByRef errorLabel As Label, Optional ByVal quantity As String = "1")
            Try
                Dim shoppingCartManager As New ShoppingCartManager
                Dim cart As ShoppingCart
                Dim deliveryMethod As ShoppingCartItem.DeliveryMethod

                ' If Cart and Product Type conflict, inform the user and create a new cart
                deliveryMethod = ShoppingCartManager.getDeliveryMethod(part.ProgramCode)
                cart = getShoppingCart(customer, deliveryMethod, errorLabel)

                If Not IsCurrentCartSameType(cart) Then
                    errorLabel.Visible = True
                    If cart.Type = ShoppingCart.enumCartType.PrintOnDemand Then
                        errorLabel.Text = "The Part has been put in a different cart from your print-on-demand items."
                    ElseIf cart.Type = ShoppingCart.enumCartType.Download Then
                        errorLabel.Text = "The Part has been put in a different cart from your downloadable items."
                    End If
                End If
                shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, part.PartNumber, quantity)
                Session("carts") = cart ' set this cart as the current one
            Catch ex As Exception
                errorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Internal method of adding a Kit item to the cart. Called by the generic AddToCart.
        ''' </summary>
        Private Sub AddKitToCart(ByRef kit As Kit, ByRef customer As Customer, ByRef errorLabel As Label, Optional ByVal quantity As String = "1")
            Try
                Dim shoppingCartManager As New ShoppingCartManager
                Dim cart As ShoppingCart
                Dim deliveryMethod As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship

                ' If Cart and Product Type conflict, inform the user and create a new cart
                cart = getShoppingCart(customer, deliveryMethod, errorLabel)

                If Not IsCurrentCartSameType(cart) Then
                    errorLabel.Visible = True
                    If cart.Type = ShoppingCart.enumCartType.PrintOnDemand Then
                        errorLabel.Text = "The Kit has been put in a different cart from your print-on-demand items."
                    ElseIf cart.Type = ShoppingCart.enumCartType.Download Then
                        errorLabel.Text = "The Kit has been put in a different cart from your downloadable items."
                    End If
                End If
                shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, kit, ShoppingCartItem.ItemOriginationType.Search)
                Session("carts") = cart ' set this cart as the current one
            Catch ex As Exception
                errorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Internal method of adding a Warranty item to the cart. Called by the generic AddToCart.
        ''' </summary>
        Private Sub AddWarrantyToCart(ByRef warranty As ExtendedWarrantyModel, ByRef customer As Customer, ByRef errorLabel As Label)
            Try
                Dim cart As ShoppingCart
                Dim shoppingCartManager As New ShoppingCartManager
                Dim deliveryMethod As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Download

                cart = getShoppingCart(customer, deliveryMethod, errorLabel)
                If Not IsCurrentCartSameType(cart) Then
                    If cart.Type = ShoppingCart.enumCartType.PrintOnDemand Then
                        errorLabel.Text = "The Warranty has been put in a different cart from your print-on-demand items."
                    ElseIf cart.Type = ShoppingCart.enumCartType.Other Then
                        errorLabel.Text = "The Warranty has been put in a different cart from your items that require shipping."
                    End If
                End If
                shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, warranty, ShoppingCartItem.ItemOriginationType.Search, deliveryMethod)
                Session("carts") = cart
            Catch ex As Exception
                errorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "     OLD ADD TO CART METHODS - Currently commented out    "
        'Protected Sub AddToCart(ByRef thisCustomer As Customer, ByRef thisProduct As Product, ByRef errorMessageLabel As Label, ByRef customer As Customer)
        '	Try
        '		If thisCustomer IsNot Nothing And thisProduct IsNot Nothing Then
        '			'-- if cart contains download only item then save cart and
        '			'-- create a new one for the part - dwd
        '			Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
        '			If TypeOf (thisProduct) Is Sony.US.ServicesPLUS.Core.Part Then
        '				Dim thisPart As Sony.US.ServicesPLUS.Core.Part = CType(thisProduct, Sony.US.ServicesPLUS.Core.Part)
        '				deliveryMethod = ShoppingCartManager.getDeliveryMethod(thisPart.ProgramCode)
        '			Else
        '				deliveryMethod = ShoppingCartItem.DeliveryMethod.Ship 'for kit, always shipped
        '			End If

        '			'-- if the cart is empty we don't need to do any checks on what's in the cart
        '			'If cart.NumberOfItems > 0 Then

        '			'Dim okToAddToCart As Boolean = False
        '			Dim cart As ShoppingCart = getShoppingCart(thisCustomer, deliveryMethod, errorMessageLabel)

        '			'-- if we can't add the item to the cart then we need to save the existing cart and create a new cart for the item
        '			If Not IsCurrentCartSameType(cart) Then
        '				errorMessageLabel.Visible = True
        '				If cart.Type <> ShoppingCart.enumCartType.PrintOnDemand Then
        '					errorMessageLabel.Text = "The print on demand item has been put in a different cart."
        '				Else
        '					errorMessageLabel.Text = "The item has been put in a different cart from your print on demand items."
        '				End If
        '			End If

        '			Dim sm As New ShoppingCartManager
        '			If TypeOf (thisProduct) Is Sony.US.ServicesPLUS.Core.Part Then
        '				sm.AddProductToCart(HttpContextManager.GlobalData, cart, thisProduct.PartNumber, "1")
        '			ElseIf TypeOf (thisProduct) Is Sony.US.ServicesPLUS.Core.Kit Then
        '				sm.AddProductToCart(HttpContextManager.GlobalData, cart, CType(thisProduct, Sony.US.ServicesPLUS.Core.Kit), ShoppingCartItem.ItemOriginationType.Search)
        '			End If

        '			Session("carts") = cart	' set this cart as the current one
        '		End If
        '	Catch ex As Exception
        '		'-- major send user to error page to give instrutions on how to order via phone.
        '		handleError(ex)
        '	End Try
        'End Sub

        'Protected Sub AddToCart(ByRef thisCustomer As Customer, ByRef thisSoftware As Software, ByVal deliveryMethod As ShoppingCartItem.DeliveryMethod, ByRef errorMessageLabel As Label)
        'End Sub

        'Protected Sub AddToCart(ByRef thisCustomer As Customer, ByRef thisTraining As Training, ByVal deliveryMethod As ShoppingCartItem.DeliveryMethod, ByRef errorMessageLabel As Label)
        '	Try
        '		If Not thisCustomer Is Nothing And Not thisTraining Is Nothing Then
        '			Dim cart As ShoppingCart
        '			Dim shoppingCartManager As New ShoppingCartManager

        '			cart = getShoppingCart(thisCustomer, deliveryMethod, errorMessageLabel)

        '			If Not IsCurrentCartSameType(cart) Then
        '				If cart.Type <> ShoppingCart.enumCartType.DownLoad Then
        '					errorMessageLabel.Text = "The item has been added to a cart for items to be physically shipped; your previous cart with items for electronic delivery has been saved."
        '				Else
        '					errorMessageLabel.Text = "The item has been added to a cart for items to be electronically delivered; your previous cart with items for physical shipment has been saved."
        '				End If
        '			End If

        '			shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, thisTraining, ShoppingCartItem.ItemOriginationType.Search, deliveryMethod)
        '			Session("carts") = cart
        '		End If
        '	Catch ex As Exception
        '		handleError(ex)
        '	End Try
        'End Sub

        'Protected Sub AddToCart(ByRef thisCustomer As Customer, ByRef thisExtendedWarranty As ExtendedWarrantyModel, ByVal deliveryMethod As ShoppingCartItem.DeliveryMethod, ByRef errorMessageLabel As Label)
        '	Try
        '		If thisCustomer IsNot Nothing And thisExtendedWarranty IsNot Nothing Then
        '			Dim cart As ShoppingCart
        '			Dim shoppingCartManager As New ShoppingCartManager
        '			cart = getShoppingCart(thisCustomer, deliveryMethod, errorMessageLabel)
        '			If Not IsCurrentCartSameType(cart) Then
        '				If cart.Type <> ShoppingCart.enumCartType.DownLoad Then
        '					errorMessageLabel.Text = "The item has been added to a cart for items to be physically shipped; your previous cart with items for electronic delivery has been saved."
        '				Else
        '					errorMessageLabel.Text = "The item has been added to a cart for items to be electronically delivered; your previous cart with items for physical shipment has been saved."
        '				End If
        '			End If
        '			shoppingCartManager.AddProductToCart(HttpContextManager.GlobalData, cart, thisExtendedWarranty, ShoppingCartItem.ItemOriginationType.Search, deliveryMethod)
        '			Session("carts") = cart
        '		End If
        '	Catch ex As Exception
        '		handleError(ex)
        '	End Try
        'End Sub
#End Region

        Protected Function DownloadFile(ByRef uri As String, ByRef numBytesToReadEachTime As Integer) As Byte()
            Dim objRequest As System.Net.HttpWebRequest = WebRequest.Create(uri)
            Dim objResponse As System.Net.WebResponse = objRequest.GetResponse
            Dim pdfFileStream As Stream = objResponse.GetResponseStream

            Dim numBytesRead As Integer = 0

            Dim n As Integer
            Dim pdf(numBytesToReadEachTime - 1) As Byte
            While True
                ' Read can return anything from 0 to numBytesToRead.
                'Dim tempPDF(numBytesToRead - 1) As Byte
                n = pdfFileStream.Read(pdf, numBytesRead, numBytesToReadEachTime)
                ' The end of the file has been reached.
                If n = 0 Then
                    Exit While
                End If
                ReDim Preserve pdf(pdf.Length + numBytesToReadEachTime - 1) 'still have more to be read,adjust the buf size
                'Array.Copy(tempPDF, 0, pdf, numBytesRead, n)
                numBytesRead += n
            End While
            ReDim Preserve pdf(numBytesRead - 1) 'adjust for the final length
            pdfFileStream.Close()
            Return pdf
        End Function

#End Region

        Public Sub handleError(ByVal ex As Exception)
            Try
                'Response.Write(ex.Message + vbCrLf + ex.StackTrace)
                'Exit Sub
                Session.Clear()
                Session("Exception") = ex
                Response.Redirect(errorHandlerPage + "?SoftwareError=True", False)
            Catch
                '-- expected error do nothing 
            End Try
        End Sub

        Public Function GetBrowserSessionID() As String
            Dim strHTTPCookie As String() = HttpContextManager.GetServerVariableValue("HTTP_COOKIE").Split(";")
            For Each strCookieValue As String In strHTTPCookie
                If strCookieValue.Contains("ASP.NET_SessionId") Then
                    strCookieValue = strCookieValue.Replace("ASP.NET_SessionId=", "").Trim()
                    Return strCookieValue
                End If
            Next
            Return String.Empty
        End Function

        ' 2016-07-19 ASleight - Moved this to the App_Code\HttpContextManager static class
        'Public Function GetServerVariableValue(ByVal Key As String) As String
        '	Dim loop1, loop2 As Integer
        '	Dim arr1(), arr2() As String
        '	Dim coll As NameValueCollection
        '	Dim value As String = String.Empty
        '	' Load ServerVariable collection into NameValueCollection object.
        '	coll = Request.ServerVariables
        '	' Get names of all keys into a string array.
        '	arr1 = coll.AllKeys
        '	For loop1 = 0 To arr1.GetUpperBound(0)
        '		If Key = arr1(loop1) Then
        '			arr2 = coll.GetValues(loop1) ' Get all values under this key.
        '			For loop2 = 0 To arr2.GetUpperBound(0)
        '				value = Server.HtmlEncode(arr2(loop2))
        '				Return value
        '				Exit Function
        '			Next loop2
        '		End If
        '	Next loop1
        '	Return value
        'End Function
#End Region

#Region "    SUB-CLASSES"
        Public Class AddToCartResult '6994
            Private _message As String
            Private _totalAmount As String
            Private _items As String
            Private _success As Integer
            Private _quantity As String
            Private _partnumber As String
            Private _differentCart As Boolean

            Property PartNumber() As String
                Get
                    Return _partnumber
                End Get
                Set(ByVal value As String)
                    _partnumber = value
                End Set
            End Property
            Property message() As String
                Get
                    Return _message
                End Get
                Set(ByVal value As String)
                    _message = value
                End Set
            End Property
            Property Quantity() As String
                Get
                    Return _quantity
                End Get
                Set(ByVal value As String)
                    _quantity = value
                End Set
            End Property
            ''' <summary>
            ''' 0-Failure, 1-Success 2-warning
            ''' </summary>
            Property success() As Integer
                Get
                    Return _success
                End Get
                Set(ByVal value As Integer)
                    _success = value
                End Set
            End Property
            Property totalAmount() As String
                Get
                    Return _totalAmount
                End Get
                Set(ByVal value As String)
                    _totalAmount = value
                End Set
            End Property
            Property items() As String
                Get
                    Return _items
                End Get
                Set(ByVal value As String)
                    _items = value
                End Set
            End Property

            Property differentCart() As Boolean
                Get
                    Return _differentCart
                End Get
                Set(ByVal value As Boolean)
                    _differentCart = value
                End Set
            End Property
        End Class

        Public Class XsltDateTime
            Protected m_strFormat As String = "MM/dd/yyyy"
            Public Sub New()

            End Sub
            Public Sub New(ByRef strFormat As String)
                m_strFormat = strFormat
            End Sub

            Public Function Format(ByRef strDateTime As String) As String
                Try
                    Dim dateTime As Date = Convert.ToDateTime(strDateTime)
                    If dateTime = NullDateTime Then
                        Return "TBD"
                    Else
                        Return dateTime.ToString(m_strFormat)
                    End If
                Catch ex As Exception
                    Return "TBD"
                End Try
            End Function
        End Class

        Public Class XsltCourse
            Public Function FormatPreRequisite(ByRef strPreRequisite As String) As String
                Try
                    If String.IsNullOrWhiteSpace(strPreRequisite) Then
                        Return "None"
                    Else
                        Return strPreRequisite
                    End If
                Catch ex As Exception
                    Return "None"
                End Try
            End Function
            Public Function FormatPrice(ByRef strPrice As String) As String
                Try
                    If String.IsNullOrWhiteSpace(strPrice) Then
                        Return ""
                    Else
                        Dim dPrice As Double = Convert.ToDouble(strPrice)
                        Return dPrice.ToString("C")
                    End If
                Catch ex As Exception
                    Return ""
                End Try
            End Function
            Public Function FormatWhiteSpace(ByVal length As Integer) As String
                Dim i As Integer
                Dim strWhiteSpace As String = ""
                For i = 1 To length
                    strWhiteSpace = strWhiteSpace + "&nbsp;"
                Next
                Return strWhiteSpace
            End Function
        End Class
#End Region

    End Class

End Namespace
