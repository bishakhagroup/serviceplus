﻿Imports Microsoft.VisualBasic
Imports Sony.US.AuditLog
Imports System

Public NotInheritable Class SingletonLogHelper

	Private Shared m_instance As SingletonLogHelper
	Private Shared syncRoot As Object = New [Object]()

	Private Sub New()
	End Sub

#Region "Properties"
	Public Shared ReadOnly Property Instance() As SingletonLogHelper
		Get
			If m_instance Is Nothing Then
				SyncLock syncRoot
					If m_instance Is Nothing Then
						m_instance = New SingletonLogHelper()
					End If
				End SyncLock
			End If

			Return m_instance
		End Get
	End Property
	Public Sub LogMember(ByVal eventOrigionMethod As String, ByVal message As String)
		Dim objPCILogger As New PCILogger()
		objPCILogger.EventOriginApplication = "ServicesPLUS"
		objPCILogger.EventOriginMethod = eventOrigionMethod
		objPCILogger.Message = message
		objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
		objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
		objPCILogger.OperationalUser = Environment.UserName
		objPCILogger.PushLogToMSMQ()
	End Sub

	Public Sub LogException(ByVal ex As Exception, ByVal eventOrigionMethod As String, ByVal message As String)
		Dim objPCILogger As New PCILogger()
		objPCILogger.IndicationSuccessFailure = "Failure"
		objPCILogger.EventOriginApplication = "ServicesPLUS"
		objPCILogger.EventOriginMethod = eventOrigionMethod
		objPCILogger.Message = message
		objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
		objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
		objPCILogger.OperationalUser = Environment.UserName
		objPCILogger.PushLogToMSMQ()
	End Sub

#End Region
End Class

