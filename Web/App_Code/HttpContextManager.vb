﻿Option Explicit On
Option Strict On

Imports System.Web.SessionState
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp

	''' <summary>
	'''    A public, static class for fetching and handling HttpRequest and HttpContext related variables.
	'''    Throws meaningful exceptions for pages/classes to handle and log.
	'''    NOTE: Request and Session CANNOT be accessed until Page_Load event begins.
	''' </summary>
	Public NotInheritable Class HttpContextManager

#Region "Private Constants"
		Private Const SESSION_GLOBALDATA As String = "GlobalData"
        Private Const SESSION_SELECTEDLANGUAGE As String = "SelectedLang"
        Private Const SESSION_CUSTOMER As String = "customer"
        Private Const SESSION_SELECTEDCUSTOMER As String = "SelectedCustomer"
		'Private Const SERVERVAR_FORWARDHOST As String = "HTTP_X_FORWARDED_HOST"
		Private Const ENV_DEVELOP As String = "Development"
		Private Const ERR_NOSESSION As String = "No HttpSessionState found. Do not reference before Page_Load event."
		Private Const ERR_NOREQUEST As String = "No HttpRequest found. Do not reference before Page_Load event."
		Private Const ERR_SESSIONREADONLY As String = "Session state has been set as Read-Only."
		'Private Const ERR_NOSERVERVAR As String = "Request does not contain the Server Variable: " + SERVERVAR_FORWARDHOST
		Private Const ERR_BADREQUESTURL As String = "Request.Url or Request.Url.Host are null."
#End Region

		' This prevents the class from being instantiated.
		Private Sub New()
		End Sub

#Region "Private Properties for HTTPContext (throws specific exceptions for pages to Try/Catch/Log)"
		''' <summary>
		'''    Returns the current HttpContext.Current.Session object, or throws an InvalidOperationException if not available, or NotSupportedException if it's read-only.
		''' </summary>
		Private Shared ReadOnly Property Session() As HttpSessionState
			Get
				If ((HttpContext.Current Is Nothing) OrElse (HttpContext.Current.Session Is Nothing)) Then
					Throw New InvalidOperationException(ERR_NOSESSION)
				ElseIf (HttpContext.Current.Session.IsReadOnly) Then
					Throw New NotSupportedException(ERR_SESSIONREADONLY)
				Else
					Return HttpContext.Current.Session
				End If
				Return Nothing
			End Get
		End Property

		''' <summary>
		'''    Returns the current HttpContext.Current.Request object, or throws an InvalidOperationException if Request doesn't exist.
		''' </summary>
		Private Shared ReadOnly Property Request() As HttpRequest
			Get
				If ((HttpContext.Current Is Nothing) OrElse (HttpContext.Current.Request Is Nothing)) Then
					Throw New InvalidOperationException(ERR_NOREQUEST)
				Else
					Return HttpContext.Current.Request
				End If
				Return Nothing
			End Get
        End Property
#End Region

#Region "Public Properties"
        ''' <summary>
        '''   Returns the Domain part of the current URL, or null/Nothing if it cannot get it from the server variables.
        ''' </summary>
        Public Shared ReadOnly Property DomainURL() As String
            Get
                If (Request.Url Is Nothing) OrElse (Request.Url.Authority Is Nothing) Then
                    Throw New InvalidOperationException(ERR_BADREQUESTURL)
                End If

                If (ConfigurationData.GeneralSettings.Environment = ENV_DEVELOP) Or (Request.IsLocal) Then
                    'Developer: Toggle between US and CA by switching the comments below
                    Return Request.Url.Authority.Trim.ToLower
                    'Return "https://servicesplus-qa.sony.ca/default.aspx"
                ElseIf RequestExists() Then
                    Return Request.ServerVariables("HTTP_X_FORWARDED_HOST")
                Else
                    Return Nothing
                End If
            End Get
        End Property

        ''' <summary>
        ''' When the user selects a language from the drop-down box, this will be set.
        ''' </summary>
        ''' <returns>Returns "en-US" as default, or "fr-CA"</returns>
        Public Shared Property SelectedLang() As String
            Get
                If Session(SESSION_SELECTEDLANGUAGE) Is Nothing Then
                    'If GlobalData IsNot Nothing Then
                    '    Session(SESSION_SELECTEDLANGUAGE) = GlobalData.Language
                    'Else
                    If (DomainURL IsNot Nothing) AndAlso (DomainURL.Contains("sony.ca")) Then
                        Session(SESSION_SELECTEDLANGUAGE) = "en-CA"
                    Else
                        Session(SESSION_SELECTEDLANGUAGE) = "en-US"
                    End If
                End If
                Return CType(Session(SESSION_SELECTEDLANGUAGE), String)
            End Get

            Set(ByVal value As String)
                If value Is Nothing Then
                    Session.Remove(SESSION_SELECTEDLANGUAGE)
                Else
                    Session(SESSION_SELECTEDLANGUAGE) = value
                    Dim gd = GlobalData
                    gd.Language = value
                    Session(SESSION_GLOBALDATA) = gd
                End If
            End Set
        End Property

        ''' <summary>
        '''   Publicly accessible instance of the GlobalData, as fetched from Session.Item. If none exists, it will create one.
        ''' </summary>
        Public Shared Property GlobalData() As GlobalData
            Get
                If (Not SessionExists()) Then
                    Return Nothing
                ElseIf (Session(SESSION_GLOBALDATA) Is Nothing) Then
                    CreateGlobalData()
                End If

                Return CType(Session(SESSION_GLOBALDATA), GlobalData)
            End Get

            Set(ByVal value As GlobalData)
                If value Is Nothing Then
                    Session.Remove(SESSION_GLOBALDATA)
                Else
                    Session(SESSION_GLOBALDATA) = value
                End If
            End Set
        End Property

        ''' <summary>
        '''   Publicly accessible instance of the Customer selected from the Admin pages. Returns Nothing/null if not yet set.
        ''' </summary>
        Public Shared Property SelectedCustomer() As Customer
            Get
                If (Not SessionExists()) OrElse (Session(SESSION_SELECTEDCUSTOMER) Is Nothing) Then
                    Return Nothing
                Else
                    Return CType(Session(SESSION_SELECTEDCUSTOMER), Customer)
                End If
            End Get

            Set(ByVal value As Customer)
                If value Is Nothing Then
                    Session.Remove(SESSION_SELECTEDCUSTOMER)
                Else
                    Session(SESSION_SELECTEDCUSTOMER) = value
                End If
            End Set
        End Property

        ''' <summary>
        '''   Publicly accessible instance of the logged-in Customer, as fetched from Session variables. Returns Nothing/null if not logged in.
        ''' </summary>
        Public Shared Property Customer() As Customer
            Get
                If (Not SessionExists()) OrElse (Session(SESSION_CUSTOMER) Is Nothing) Then
                    Return Nothing
                Else
                    Return CType(Session(SESSION_CUSTOMER), Customer)
                End If
            End Get

            Set(ByVal value As Customer)
                If value Is Nothing Then
                    Session.Remove(SESSION_CUSTOMER)
                Else
                    Session(SESSION_CUSTOMER) = value
                End If
            End Set
        End Property
#End Region

#Region "Private Helper Methods"
        ''' <summary>
        '''   Generate a new GlobalData and insert it into Session.Items("GlobalData")
        ''' </summary>
        Private Shared Sub CreateGlobalData()
            Dim gd = New GlobalData
            ' Set up default values. Language and SalesOrg will be overwritten later if needed.
            gd.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
            gd.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
            gd.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
            gd.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US

            If (DomainURL IsNot Nothing) AndAlso (DomainURL.Contains("sony.ca")) Then
                gd.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                gd.Language = SelectedLang
            End If
            'Utilities.LogMessages(DateTime.Now() & " | GDATA CREATE - DomainURL: " & DomainURL & ", SalesOrg: " & gd.SalesOrganization & ", Lang: " & gd.Language)

            Session(SESSION_GLOBALDATA) = gd
        End Sub
#End Region

#Region "Public Methods"
        ''' <summary>
        '''  Fetches the String value of a specified Server Variable. If the Variable doesn't exist, it returns String.Empty
        ''' </summary>
        ''' <param name="keyName">The name of the Server Variable to fetch.</param>
        ''' <returns>Variable value as String, or String.Empty</returns>
        Public Shared Function GetServerVariableValue(ByVal keyName As String) As String
            If (Request.ServerVariables(keyName) IsNot Nothing) Then
                Return Request.ServerVariables(keyName)
            Else
                Return String.Empty
            End If
        End Function

        ''' <summary>
        '''    Returns True if a valid Request object exists.
        ''' </summary>
        Public Shared Function SessionExists() As Boolean
            Return ((HttpContext.Current IsNot Nothing) AndAlso (HttpContext.Current.Session IsNot Nothing))
        End Function

        ''' <summary>
        '''    Returns True if a valid Session object exists.
        ''' </summary>
        Public Shared Function RequestExists() As Boolean
            Return ((HttpContext.Current IsNot Nothing) AndAlso (HttpContext.Current.Request IsNot Nothing))
        End Function

        Public Sub ChangeLocale(ByVal locale As String)
            Dim gd = GlobalData
            Dim oldSalesOrg = gd.SalesOrganization
            Dim oldLocale = gd.Language
            Dim url As String = ""

            If (String.IsNullOrEmpty(locale)) Then
                locale = ConfigurationData.GeneralSettings.GlobalData.Language.US
            End If

            gd.Language = locale
            If (locale = "en-US") Then
                gd.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
            Else
                gd.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
            End If

            ' Store the new values in the Session variable set
            GlobalData = gd
            SelectedLang = locale

            ' If we changed Countries, we have to change the Domain part of the URL and reload.
            If oldSalesOrg <> gd.SalesOrganization Then
                If gd.SalesOrganization = "US00" Then
                    url = Request.Url.AbsoluteUri.Replace("https://" + Request.Url.Host, ConfigurationData.Environment.URL.US)
                    Utilities.LogMessages("ContextManager - Redirecting to US site: " & url)
                ElseIf gd.SalesOrganization = "CA00" Then
                    url = Request.Url.AbsoluteUri.Replace("https://" + Request.Url.Host, ConfigurationData.Environment.URL.CA)
                    Utilities.LogMessages("ContextManager - Redirecting to CA site: " & url)
                End If
                HttpContext.Current.Response.Redirect(url)
            End If
        End Sub
#End Region
    End Class

End Namespace