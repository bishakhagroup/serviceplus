Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class Availability
        Inherits SSL

        Protected WithEvents PriceLabel As Label
        Protected WithEvents lblPromoPrice As Label
        Private conPriceUnavailableMsg As String = Resources.Resource.el_Unavailable '"unavailable"
        Private conNoPriceMessage As String = Resources.Resource.el_Unavailable_Msg '"Requested product not available for shipment. Please contact ServicesPLUS at 1-800-538-7550 and request status on release availability."
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
#End Region

#Region "Events"
        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            Page.ID = Request.Url.AbsolutePath
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            If HttpContextManager.GlobalData.IsCanada Then
                lblYourPriceTitle.Text = Resources.Resource.el_YourPrice_CAD + ":"
                lblListPriceTitle.Text = Resources.Resource.el_ListPrice_CAD + ":"
            Else
                lblYourPriceTitle.Text = Resources.Resource.el_YourPrice + ":"
                lblListPriceTitle.Text = Resources.Resource.el_ListPrice + ":"
            End If
            If Not Page.IsPostBack Then ControlMethod(sender, e)
        End Sub
#End Region

#Region "Methods"

        Private Sub ControlMethod(ByVal sender As Object, ByVal e As EventArgs)
            Try
                '' If Not Session.Item("customer") Is Nothing And TypeOf Session.Item("customer") Is Customer Then Customer = Session.Item("customer") 'Changes done towards Bug# 1212
                If Request.QueryString("softwareItem") IsNot Nothing Then  '-- get your price for software -- 
                    ShowSoftwarePrice(GetSoftwareToPrice())
                ElseIf Request.QueryString("promoKit") IsNot Nothing Then
                    Dim kit As String = Request.QueryString("promoKit")
                    ShowKitDetail(kit)
                Else 'get your price for parts
                    GetPartPrice()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ShowKitDetail(ByVal kit As String)
            Dim cust As Customer
            Dim custMgr As New CustomerManager
            Dim sap_services As New SAPHelper()
            Dim objPCILogger As New PCILogger() '6524
            Dim availibility As String
            Dim listPrice As String

            Try
                cust = custMgr.CreateCustomer()
                Dim parts As SAPProxyPart() = sap_services.PartInquiry(cust, kit, Nothing)
                For Each part As SAPProxyPart In parts
                    availibility = IIf(part.Availability > 0, "In stock", "No")
                    tdrecycle.Visible = (part.RecyclingFlag > 0)
                    listPrice = part.ListPrice.ToString("C")
                    PopulateLabelControls(part.Number, part.Description, listPrice, part.ListPrice, availibility, part.YourPrice, False)
                Next
            Catch ex As Exception
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "showKitDetail"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "showKitDetail Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Sub ShowSoftwarePrice(ByVal software As Software)
            Dim objPCILogger As New PCILogger()
            Try
                Dim custMan As New CustomerManager
                Dim catMan As New CatalogManager
                Dim promoMan As New PromotionManager
                Dim corePart As New Part()
                Dim cust As Customer = custMan.CreateCustomer()
                Dim IsSpecialCharacter As New Regex(ConfigurationData.GeneralSettings.SpecialCharacter.SpecialCharacterDetail)
                Dim partNumber As String
                Dim partDesc As String = ""
                Dim availibility As String = ""
                Dim listPrice As String = ""
                Dim hasPromo As Boolean = False
                Dim yourPrice As Double = 0
                Dim recycleflag As Double = 0

                If software Is Nothing Then ' 2018-10-09 ASleight - Added to prevent unhandled Null Reference Exceptions.
                    Throw New ArgumentNullException("software", "YourPrice.ShowSoftwarePrice - Software variable was null. Possible loss of session.")
                End If

                partDesc = software.KitDescription
                listPrice = software.ListPrice.ToString("C")
                If Not String.IsNullOrEmpty(software.KitPartNumber) Then
                    partNumber = software.KitPartNumber
                ElseIf Not String.IsNullOrEmpty(software.ReplacementPartNumber) Then
                    partNumber = software.ReplacementPartNumber
                ElseIf Not String.IsNullOrEmpty(software.PartNumber) Then
                    partNumber = software.PartNumber
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(New Exception("An error occurred while loading the Part Number from the selected software item. Please try again."))
                    Exit Sub
                End If

                If IsSpecialCharacter.IsMatch(partNumber) Then
                    ErrorLabel.Text = Resources.Resource.el_partNo_SpecialChar '"Part Number Contains Special Character."
                    Exit Sub
                End If
                availibility = catMan.Availability(software, cust, Product.ProductType.Software, corePart, yourPrice, HttpContextManager.GlobalData)

                If promoMan.IsPromotionPart(partNumber) Then
                    lblListPrice.Text = listPrice + "<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>" '7820
                    hasPromo = True
                End If
                tdLegend.Visible = hasPromo

                recycleflag = catMan.ISRecyclingFlagCheck(partNumber, cust, HttpContextManager.GlobalData)
                tdrecycle.Visible = recycleflag > 0
                PopulateLabelControls(partNumber, partDesc, listPrice, software.ListPrice, availibility, yourPrice, hasPromo) '7820

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.EventOriginMethod = "showSoftwarePrice"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "showSoftwarePrice Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function GetSoftwareToPrice() As Software
            If Session.Item("software") IsNot Nothing Then
                Return CType(Session.Item("software"), ArrayList)(Request.QueryString("softwareItem"))
            Else
                Return Nothing  ' Causing an immediate Null Reference Exception in the method that requests this value.
            End If
        End Function

        Private Sub PopulateLabelControls(ByRef partNumber As String, ByRef partDesc As String, ByRef listPrice As String, ByRef dlistPrice As Decimal, ByRef availibility As String, ByVal yourprice As Double, ByRef boolhaspromo As Boolean)
            Dim verifyYourPrice As String = yourprice.ToString("C")
            Dim discount As Integer = 0

            lblCurrentPartNumber.Text = partNumber
            lblDescription.Text = partDesc
            If listPrice <> verifyYourPrice Then
                lblDiscountTitle.Visible = True
                lblDiscount.Visible = True
            Else
                lblDiscountTitle.Visible = False
                lblDiscount.Visible = False
                lblYourPriceTitle.Visible = False
                lblYourPrice.Visible = False
            End If

            lblListPrice.Text = listPrice
            If boolhaspromo = True Then lblListPrice.Text &= "<span face=""wingdings"" color=""red"" size=""3px"">v</span>"

            If Not String.Equals(availibility.ToString, "In stock") Then
                Me.lblAvailibility.Height = 30
            End If

            lblAvailibility.Text = availibility
            lblYourPrice.Text = yourprice.ToString("C")
            If Not dlistPrice = 0 Then
                discount = ((dlistPrice - yourprice) / dlistPrice) * 100
                lblDiscount.Text = discount.ToString() + " %"
            Else
                lblDiscount.Text = "N/A"
            End If
        End Sub

#End Region

        Private Sub GetPartPrice()
            Dim pciLogger As New PCILogger()
            Dim debugMessage As String = ""
            Try
                ' ASleight - There is a Null exception thrown sometimes within this. It is NOT properly logged by our error handler.
                '    It is now logged as "Exception during ExceptionUtils.LogException". This is a long-standing bug, which I have
                '    slowly improved but still not resolved. Could be due to Session loss, but either way, it needs to be cleaned up.
                debugMessage &= $"Availability.getPartPrice - START at {Date.Now.ToShortTimeString}.{Environment.NewLine}"
                Dim custMan As New CustomerManager
                Dim catMan As New CatalogManager
                Dim promoMan As New PromotionManager
                Dim cust As Customer
                Dim corepart As New Part()
                Dim isKit As Boolean = False
                Dim promo_price = String.Empty
                Dim list_price = String.Empty
                Dim availibility = String.Empty
                Dim recycleflag As Double = 0
                Dim hasPromo As Boolean = False
                Dim IsSpecialCharacter As New Regex(ConfigurationData.GeneralSettings.SpecialCharacter.SpecialCharacterDetail)

                If Request.QueryString("isKit") IsNot Nothing Then isKit = (Request.QueryString("isKit") = "1")

                tdLegend.Visible = False
                If isKit Then
                    debugMessage &= $"- isKit = true.{Environment.NewLine}"
                    Dim kits As Kit() = Nothing
                    Dim kit As Kit = Nothing
                    Dim whichKit As Integer = -1
                    Dim yourprice As Double = 0

                    If Session.Item("kits") IsNot Nothing Then kits = Session.Item("kits")
                    If Request.QueryString("kitVal") IsNot Nothing Then whichKit = Request.QueryString("kitVal")

                    If (kits Is Nothing) OrElse (whichKit < 0) Then
                        ErrorLabel.Text = Resources.Resource.el_Price_Err ' "Error getting your price."
                        Exit Sub
                    End If
                    kit = kits.GetValue(whichKit)

                    If IsSpecialCharacter.IsMatch(kit.PartNumber) Then
                        ErrorLabel.Text = Resources.Resource.el_partNo_SpecialChar ' "Part Number Contains Special Character."
                        Exit Sub
                    End If

                    cust = custMan.CreateCustomer()
                    list_price = kit.ListPrice.ToString("C")
                    availibility = catMan.Availability(kit, cust, Product.ProductType.Kit, corepart, yourprice, HttpContextManager.GlobalData)
                    debugMessage &= $"- Kit Availability is: {availibility}.{Environment.NewLine}"

                    If promoMan.IsPromotionPart(kit.PartNumber) Then
                        lblListPrice.Text = list_price + "<span face=""wingdings"" color=""red"" size=""3px"">v</span>" '7820
                        hasPromo = False
                    End If
                    recycleflag = catMan.ISRecyclingFlagCheck(kit.PartNumber, cust, HttpContextManager.GlobalData)
                    tdrecycle.Visible = recycleflag > 0
                    PopulateLabelControls(kit.PartNumber, corepart.Description, corepart.ListPrice, kit.ListPrice, availibility, yourprice, hasPromo) '7820

                    'Added to inform user of core charge
                    If kit.HasCoreCharge Then
                        HasCoreChargeLabel.Text = "<span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB1 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB2 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB3
                    End If
                Else
                    debugMessage &= $"- isKit = false.{Environment.NewLine}"
                    Dim parts As Part() = Nothing
                    Dim the_part As Part = Nothing
                    Dim whichPart As Integer = -1
                    Dim partNumber As String = ""
                    Dim dListprice As Decimal = 0
                    Dim yourprice As Double = 0

                    If Session("partList") IsNot Nothing Then parts = Session.Item("partList")
                    If Request.QueryString("ArrayVal") IsNot Nothing Then whichPart = Request.QueryString("ArrayVal")

                    If (parts IsNot Nothing) AndAlso (parts.Length > 0) AndAlso (whichPart > -1) Then
                        the_part = parts.GetValue(whichPart)
                        debugMessage &= $"- Part fetched.  Parts List size: {IIf(parts Is Nothing, "null", parts?.Length)}, Part Number: {the_part?.PartNumber}, whichPart: {whichPart}.{Environment.NewLine}"
                    Else
                        debugMessage &= $"- Session Variable issue. Parts List size: {IIf(parts Is Nothing, "null", parts?.Length)}, whichPart: {whichPart}.{Environment.NewLine}"
                    End If

                    If the_part Is Nothing Then
                        ErrorLabel.Text = Resources.Resource.el_NoitemNoProvided '"No item number provided."
                        Exit Sub
                    End If

                    partNumber = IIf(Not String.IsNullOrEmpty(the_part.ReplacementPartNumber), the_part.ReplacementPartNumber, the_part.PartNumber)

                    If IsSpecialCharacter.IsMatch(partNumber) Then
                        ErrorLabel.Text = Resources.Resource.el_partNo_SpecialChar ' "Part Number Contains Special Character."
                        Exit Sub
                    End If

                    If (the_part.IsFRB Or the_part.HasCoreCharge) Then
                        dListprice = the_part.ListPrice + the_part.CoreCharge
                    Else
                        dListprice = the_part.ListPrice
                    End If
                    list_price = dListprice.ToString("C")
                    'Dim lprice As String = corepart.ListPrice.ToString("C")

                    If promoMan.IsPromotionPart(partNumber) Then
                        lblListPrice.Text = list_price + "<span face=""wingdings"" color=""red"" size=""3px"">v</span>"
                        hasPromo = True
                    End If

                    cust = custMan.CreateCustomer()
                    debugMessage &= $"- Availability fetch. Variables are: {the_part.PartNumber}, {cust.LdapID}, {corepart.PartNumber}, {yourprice}.{Environment.NewLine}"
                    availibility = catMan.Availability(the_part, cust, Product.ProductType.Software, corepart, yourprice, HttpContextManager.GlobalData)    '.ToString("C")
                    recycleflag = catMan.ISRecyclingFlagCheck(partNumber, cust, HttpContextManager.GlobalData)
                    tdrecycle.Visible = recycleflag > 0

                    PopulateLabelControls(partNumber, corepart.Description, list_price, dListprice, availibility, yourprice, hasPromo) '7820
                    'Added to inform user of core charge

                    If the_part.IsFRB Then
                        HasCoreChargeLabel.Text = "<span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB + Resources.Resource.el_Price_FRB1 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB2 + (corepart.CoreCharge).ToString("C") + Resources.Resource.el_Price_FRB3
                    ElseIf the_part.HasCoreCharge Then
                        HasCoreChargeLabel.Text = "<span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB1 + corepart.CoreCharge.ToString("C") + Resources.Resource.el_Price_FRB2 + (corepart.CoreCharge).ToString("C") + Resources.Resource.el_Price_FRB3
                    End If
                End If

                ' -- display legend only if there is at least one part with promotion
                tdLegend.Visible = hasPromo
                'Utilities.LogMessages("getPartPrice - END at " + DateTime.Now.ToShortTimeString)
            Catch ex As Exception
                Utilities.LogMessages(debugMessage & " | Exception Type: " & ex.GetType().ToString())
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                pciLogger.EventOriginApplication = "ServicesPLUS"
                pciLogger.EventOriginApplicationLocation = Page.GetType().Name
                pciLogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                pciLogger.OperationalUser = Environment.UserName
                pciLogger.EventDateTime = Date.Now.ToLongTimeString()
                pciLogger.HTTPRequestObjectValues = pciLogger.GetRequestContextValue()
                pciLogger.EventOriginMethod = "getPartPrice"
                pciLogger.EventType = EventType.View
                pciLogger.IndicationSuccessFailure = "Failure"
                pciLogger.Message = "getPartPrice Failure. " & ex.Message.ToString()
                pciLogger.PushLogToMSMQ()
            End Try
        End Sub

    End Class

End Namespace
