﻿Imports System.Globalization
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System.Data

Namespace ServicePLUSWebApp
    Partial Class Register_Email_Verification
        Inherits SSL

        Dim temp_user As TempCustomerValues = New TempCustomerValues
        Private usCulture As New CultureInfo("en-US")
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                AutogenerateCode()
            End If
        End Sub

        Protected Sub NextButton_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles NextButton.Click
            If txt_code.Text.Trim().Equals(Session("vcode")) Then
                Session("Isverified") = "1"   'To insert information into Users table 
                Response.Redirect("Register-New-User.aspx", False) ' changed by satheesh Email validation
                'Response.Redirect("Register-Email-Verification.aspx", False)
            Else
                ErrorLabel.Text = "Please enter valid verification code."
                ErrorLabel.Focus()
            End If
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Protected Sub btn_Resend_Code_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Resend_Code.Click
            AutogenerateCode()
            ErrorLabel.Text = "Please enter verification code sent to your email ID."
            ErrorLabel.Focus()
        End Sub

        Private Sub AutogenerateCode()
            ' code for generating verification code Start
            Try
                Dim cm As New CustomerManager
                Dim CustomerDB As TempCustomer = New TempCustomer
                Dim data As String = String.Empty
                Dim newUserData As String()
                Dim ds_TempUser As DataSet
                Dim existing_user_FirstName As String
                Dim existing_user_LastName As String
                Dim existing_user_EmailAddress As String
                Dim existing_user_VerificationCode As String
                Dim existing_user_createddate As DateTime
                Dim curdate As DateTime
                Dim duration As TimeSpan
                Dim verificationcode As String = String.Empty
                Dim EmailBody As New StringBuilder(5000)
                Dim Subject As String = "Welcome to Servicesplus. "

                Try
                    ' This causes a lot of errors, due to lost Session state. The culprit is often web spiders and bots. Check the User Agent in the logs.
                    data = Session.Item("NewUserData").ToString()
                    newUserData = data.Split("*"c)
                    temp_user.FirstName = newUserData(0)
                    temp_user.LastName = newUserData(1)
                    temp_user.EmailAddress = newUserData(2)
                Catch ex As Exception
                    If String.IsNullOrWhiteSpace(data) Then data = "null"
                    Utilities.LogMessages("Exception while parsing New User Data. UserData = " & data)
                    ErrorLabel.Text = "We are unable to fetch your User data; your Session may have expired. Please ensure you have cookies enabled in your browser, " &
                        "<a href=""/default.aspx"">return to the Home page</a>, then Log In with your new ID to resume registration.<br/><br/>" &
                        Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Focus()
                    Return
                End Try

                Try
                    ' Check if user is already in the Database
                    ds_TempUser = CustomerDB.GetCustomerDetails(temp_user)
                Catch ex As Exception
                    Utilities.LogMessages("Exception while GetCustomerDetails.")
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Focus()
                    Return
                End Try
                If (ds_TempUser IsNot Nothing) AndAlso (ds_TempUser.Tables(0).Rows.Count > 0) Then
                    Try
                        'User Exists, fetch their temporary user data including the emailed Verification Code
                        existing_user_FirstName = ds_TempUser.Tables(0).Rows(0).Item("FIRSTNAME").ToString()
                        existing_user_LastName = ds_TempUser.Tables(0).Rows(0).Item("LASTNAME").ToString()
                        existing_user_EmailAddress = ds_TempUser.Tables(0).Rows(0).Item("EMAILADDRESS").ToString()
                        existing_user_VerificationCode = ds_TempUser.Tables(0).Rows(0).Item("VERIFICATIONCODE").ToString()
                        existing_user_createddate = Convert.ToDateTime(ds_TempUser.Tables(0).Rows(0).Item("CREATEDDATE"), usCulture)
                    Catch ex As Exception
                        Utilities.LogMessages("Exception while parsing Customer Details.")
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        ErrorLabel.Focus()
                        Return
                    End Try

                    curdate = DateTime.Now
                    duration = curdate - existing_user_createddate
                    If duration.TotalHours > 24 Then
                        'Check if Code has expired. If so, generate a new one, update the DB and email customer
                        Try
                            temp_user.VerificationCode = AutoGeneratedCode.GetString()
                            Session("vcode") = temp_user.VerificationCode
                            'temp_user.VerificationCode = verificationcode
                            CustomerDB.AddCustomer(temp_user, True) ' Doesn't this generate duplicate records? May be the cause of all the unfinished registrations in the DB. Consider adding a Delete step.
                            buildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                            cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject)
                        Catch ex As Exception
                            Utilities.LogMessages("Exception while building email 1.")
                            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                            ErrorLabel.Focus()
                            Return
                        End Try
                    Else
                        'Try
                        ' Code is current and already in the database
                        Session("vcode") = Convert.ToString(ds_TempUser.Tables(0).Rows(0).Item("VERIFICATIONCODE"))
                        buildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                        cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject)
                        'Catch ex As Exception
                        '    Utilities.LogMessages("Exception while building email 2.")
                        '    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        'End Try
                    End If
                Else
                    Try
                        'If New user registering
                        verificationcode = AutoGeneratedCode.GetString()
                        Session("vcode") = verificationcode
                        temp_user.VerificationCode = verificationcode
                    Catch ex As Exception
                        Utilities.LogMessages("Is temp_user null? " & (temp_user Is Nothing).ToString & Environment.NewLine &
                                              "Is verificationcode null or empty? " & (String.IsNullOrEmpty(verificationcode)).ToString & Environment.NewLine &
                                              "Exception while managing verification code")
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        ErrorLabel.Focus()
                        Return
                    End Try
                    Try
                        CustomerDB.AddCustomer(temp_user, False)
                    Catch ex As Exception
                        Utilities.LogMessages("Is temp_user null? " & (temp_user Is Nothing).ToString & Environment.NewLine &
                                              "Exception while calling CustomerDB.AddCustomer")
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        ErrorLabel.Focus()
                        Return
                    End Try
                    Try
                        'Send Email 
                        buildMailBody(EmailBody, Subject, temp_user.FirstName, temp_user.LastName)
                        cm.SendRegisterWelcomeEmail(temp_user.EmailAddress, EmailBody.ToString(), Subject) 'Enhanc 
                    Catch ex As Exception
                        Utilities.LogMessages("Is temp_user null? " & (temp_user Is Nothing).ToString)
                        Utilities.LogMessages($"Exception while building email 3. FN = {temp_user.FirstName}, SN = {temp_user.LastName}, Email = {temp_user.EmailAddress}, UserData = {data}")
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        ErrorLabel.Focus()
                        Return
                    End Try
                End If
            Catch ex As Exception
                'Need to change.
                If (ex.Message.Contains(Resources.Resource.el_Err_TimeOut) Or ex.Message.Contains(Resources.Resource.el_Err_Exception) Or ex.Message.Contains(Resources.Resource.el_Err_Request)) Then
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    Dim er As String
                    er = ErrorLabel.Text.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace) 'Operation to register timed out. Please try again or")
                    ErrorLabel.Text = er
                    ErrorLabel.Focus()
                Else
                    Utilities.WrapExceptionforUI(ex)
                    'ErrorLabel.Text = "Registration has encountered an error: First Name, Last Name, Company, Email Address, and/or Password are not valid. Please try again."'6918
                    ErrorLabel.Text = "Registration process has encountered an error: Please try again." '6918
                    ErrorLabel.Focus()
                End If
            End Try
        End Sub

        Sub buildMailBody(ByRef Emailbody As StringBuilder, ByRef Subject As String, ByVal FirstName As String, ByVal Lastname As String)
            Dim privacy_policy_link = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
            Dim terms_and_conditions_link = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
            Dim contact_us_link = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
            Dim terms_and_conditions_link_ca = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link_ca
            Dim contact_us_link_ca = ConfigurationData.GeneralSettings.URL.Contact_Us_Link_ca

            Emailbody.Append(Resources.Resource_mail.ml_common_Dear_en + FirstName + " " + Lastname + "," + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_Verificationcode_en + " " + Session("vcode").ToString + "" + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line1_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line2_en)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line3_en)
            Emailbody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_newuserconfirm_body_line8_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_Sincerely_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_en + vbCrLf + vbCrLf)
            ' ASleight - French is not an option here, and URLs are all Canadian.
            Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link_ca + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link_ca + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_en + privacy_policy_link + vbCrLf)
            Subject = Resources.Resource_mail.ml_newuserconfirm_subject_en
        End Sub
    End Class
End Namespace
