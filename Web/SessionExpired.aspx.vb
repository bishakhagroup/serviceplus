Imports Sony.US.ServicesPLUS.Core


Namespace ServicePLUSWebApp

    Partial Class SessionExpired
        Inherits System.Web.UI.Page

        'Changes done towards Bug#1212
        Public objCustomer As Customer
        Dim redirPage As String = "default.aspx"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If (Not Page.IsPostBack) Then
                Dim errorType As String = ""
                Dim returnToCode As String = ""

                Response.CacheControl = "no-cache"
                Response.AddHeader("Pragma", "no-cache")
                Response.Expires = -1

                If HttpContextManager.Customer IsNot Nothing Then objCustomer = HttpContextManager.Customer

                '-- For Checkout process, clear the current page index since it's going to restart.
                Session.Remove("CurrentPageIndex")

                If Not (String.IsNullOrEmpty(Request.QueryString("error"))) Then
                    errorType = Request.QueryString("error")
                    If Not (String.IsNullOrEmpty(Request.QueryString("returnto"))) Then
                        returnToCode = Request.QueryString("returnto")
                    End If
                End If
                ProcessQueryValues(errorType, returnToCode)
            End If
        End Sub

        Private Sub ProcessQueryValues(ByVal errorType As String, ByVal returnToCode As String)
            Select Case errorType
                Case "searchexpired"
                    lblSubHdr.InnerText = "Session Expired"
                    litMessage.Text = "Your browser Session has expired, resulting in the loss of your search criteria and results. Please click below to begin a new search."
                    btnBack.ImageUrl = Resources.Resource.img_btnNewSearch
                    btnBack.AlternateText = Resources.Resource.el_newSearch
                Case "loginexpired"
                    lblSubHdr.InnerText = "Session Expired"
                    litMessage.Text = "Your browser Session has expired. Please click below to return to the homepage, then log in again."
                    btnBack.ImageUrl = Resources.Resource.img_btnLogin
                    btnBack.AlternateText = Resources.Resource.el_Login
                Case Else
                    lblSubHdr.InnerText = "Session Notice"
                    litMessage.Text = "The page you are trying to view cannot be accessed using the 'Back' button on your browser."
                    btnBack.ImageUrl = Resources.Resource.img_btnReturnToCart
                    btnBack.AlternateText = Resources.Resource.el_ReturnToCart
            End Select

            If returnToCode = "partsearch" Then
                redirPage = "sony-parts.aspx"
            ElseIf returnToCode = "cart" Then
                redirPage = "vc.aspx"
            Else
                redirPage = "default.aspx"
            End If
        End Sub

        'Private Sub cancelOpenOrder()
        '    If Not Session.Item("order") Is Nothing And Not Session.Item("carts") Is Nothing Then

        '        Try
        '            Dim order As Sony.US.ServicesPLUS.Core.Order = Session.Item("order")
        '            Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
        '            Dim om As New OrderManager

        '            '***************Changes done for Bug# 1212 Starts here***************
        '            '**********Kannapiran S****************
        '            objSISParameter.UserLocation = objCustomer.UserLocation
        '            om.CancelOrder(order, cart, objSISParameter)
        '            '***************Changes done for Bug# 1212 Ends here***************
        '        Catch ex As Exception
        '            ErrorLabel.Text = ex.Message
        '        End Try

        '        '-- clear CurrentPageIndex, currentProcessIndex - starting over again
        '        If Not Session.Item("CurrentPageIndex") Is Nothing Then
        '            Session.Remove("CurrentPageIndex")
        '        End If
        '    End If
        'End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
            Try
                Response.Redirect(redirPage, True)
            Catch ex As Threading.ThreadAbortException
                '-- do nothing
            End Try
        End Sub

    End Class

End Namespace
