<%@ Reference Page="~/YourPrice.aspx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.SoftwarePLUSSearch"
    EnableViewStateMac="true" CodeFile="sony-software.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.sftware_hdr_msg()%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <meta name="keywords" content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript">
        var element = document.getElementById('<%=focusField%>');
        if (element !== null && element.value === '') {
            element.focus();
        }
    </script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
        <table width="760" border="0" role="presentation">
            <tr>
                <td style="width: 25px; background: url(images/sp_left_bkgd.gif);">
                    &nbsp;
                </td>
                <td width="710" bgcolor="#ffffff">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0" role="presentation">
                                    <tr style="height: 82px; vertical-align: middle;">
                                        <td style="width: 464px; background: #363d45 url('images/software_firmware_banner.JPG') no-repeat; text-align: right; padding-right: 10px;">
                                            <h1 class="headerText"><%=Resources.Resource.hplnk_software_frmwre%></h1>
                                        </td>
                                        <td style="padding: 10px; background: #363d45;">
                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td style="width: 464px; padding-left: 20px; background: #f2f5f8;">
                                            <h2 class="headerTitle"><%=Resources.Resource.search_software_firmware%></h2>
                                            <table style="width: 440px; border: none;" role="presentation">
                                                <tr>
                                                    <td>
                                                        <label class="finderCopyDark" for="txtModelNumber"><%=Resources.Resource.el_ModelNumber%>:</label>
                                                    </td>
                                                    <td>
                                                        <label class="finderCopyDark" for="txtModelDesc"><%=Resources.Resource.el_ModelDescription%>:</label>
                                                    </td>
                                                    <td>
                                                        <label class="finderCopyDark" for="ddSoftwareCategory"><%=Resources.Resource.el_ProductCategory%>:</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <SPS:SPSTextBox ID="txtModelNumber" runat="server" Width="80px" MaxLength="30" ToolTip="Search by Model Number" />
                                                    </td>
                                                    <td>
                                                        <SPS:SPSTextBox ID="txtModelDesc" runat="server" Width="140px" MaxLength="100" ToolTip="Search by Model Description" />
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddSoftwareCategory" runat="server" Width="184px" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="background: #99a8b5; vertical-align: bottom;">
                                            <asp:ImageButton ID="btnSearch" runat="server" AlternateText="<%$ Resources:Resource,search_software_firmware%>"
                                                ImageUrl="<%$ Resources:Resource,img_btnSearchHeader%>" />
                                        </td>
                                    </tr>
                                    <tr style="height: 9px;">
                                        <td style="width: 464px; background: #f2f5f8 url(images/sp_int_header_btm_left_onepix.gif) repeat-x;">
                                            <img src="images/spacer.gif" alt="" />
                                        </td>
                                        <td style="width: 246px; background: #99a8b5 url(images/sp_int_header_btm_right.gif) repeat-x;">
                                            <img src="images/spacer.gif" alt="" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%-- Error Label and Pro Site link: "Can't find what you're looking for?" --%>
                        <tr>
                            <td style="padding-left: 20px;">
                                <span class="redAsterick"><%=Resources.Resource.search_cant_find%> <asp:HyperLink ID="lnkProSite" Target="_blank"
                                    NavigateUrl="https://pro.sony/ue_US/support/software" title="Click to visit the Sony Pro Site for more software downloads."
                                    runat="server"><%=Resources.Resource.tc_clickhre_msg %></asp:HyperLink>.</span>
                                <br />                                            
                                <asp:Label ID="errorMessageLabel" runat="server" Width="641px" ForeColor="Red" CssClass="tableData" EnableViewState="False" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 710px; margin: 0px; border: none; padding: 0px;" role="presentation">
                                    <tr style="padding-top: 5px; padding-left: 20px; vertical-align: top;">
                                        <td>
                                            <%-- Top Paging Navigation --%>
                                            <nav>
                                                <table id="tblPagerTop" style="border: none; margin: 0px; padding: 0px 5px;" role="navigation" runat="server">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="btnPreviousPage" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                            Height="9" Width="9" AlternateText="Previous Page" runat="server" /><asp:LinkButton ID="PreviousLinkButton"
                                                            CssClass="tableData" AlternateText="Previous Page" runat="server">
                                                            <%=Resources.Resource.el_Previous%></asp:LinkButton>
                                                    </td>
                                                    <td class="tableData" align="center">
                                                        &nbsp;<%=Resources.Resource.el_Page%>
                                                        <asp:Label ID="lblCurrentPage" Text="1" runat="server" />
                                                        <%=Resources.Resource.el_of%>
                                                        <asp:Label ID="lblEndingPage" Text="1" runat="server" />&nbsp;                                                        
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="NextLinkButton" CssClass="tableData" AlternateText="Next Page" runat="server">
                                                        <%=Resources.Resource.el_Next%></asp:LinkButton><asp:ImageButton ID="btnNextPage"
                                                            runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9" Width="9"
                                                            AlternateText="Next Page" />
                                                    </td>
                                                    <td class="tableData" style="text-align: right; padding-left: 25px;">
                                                        <label for="txtPageNumber"><%=Resources.Resource.el_Page%></label>
                                                    </td>
                                                    <td>
                                                        <SPS:SPSTextBox ID="txtPageNumber" Width="40" MaxLength="5" ToolTip="Page Number"
                                                            runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnGoToPage" ImageUrl="<%$ Resources:Resource,img_btnGoTo%>"
                                                            AlternateText="Go to specified result page" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </nav>
                                            <%-- Model List Table --%>
                                            <asp:Table ID="SoftwareSearchResultTable" Width="670" runat="server" Caption="Software search results.">
                                                <asp:TableHeaderRow Height="20" BackColor="#d5dee9">
                                                    <asp:TableHeaderCell Width="160" CssClass="tableHeader">
                                                        <asp:LinkButton ID="lnkSortByCategory" runat="server" CssClass="tableHeader" CommandArgument="ProductCategoryDESC"
                                                            title="Click to sort by Product Category"><%=Resources.Resource.el_ProductCategory%></asp:LinkButton>
                                                        <img id="imgPCSort" runat="server" src="images/arrow_up.gif" alt="Toggle ascending or descending sort order">
                                                    </asp:TableHeaderCell>
                                                    <asp:TableHeaderCell Width="110" CssClass="tableHeader">
                                                        <asp:LinkButton ID="lnkSortByModel" runat="server" CssClass="tableHeader" CommandArgument="ModelNumberASC"
                                                            title="Click to sort by Model Number"><%=Resources.Resource.el_ModelNumber%></asp:LinkButton>
                                                        <img id="imgModelSort" runat="server" src="images/arrow_up.gif" alt="Toggle ascending or descending sort order">
                                                    </asp:TableHeaderCell>
                                                    <asp:TableHeaderCell CssClass="tableHeader"><%=Resources.Resource.el_ModelDescription%></asp:TableHeaderCell>
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <%-- Bottom Paging Navigation --%>
                                            <nav>
                                                <table id="tblPagerBottom" style="border: none; margin: 0px; padding: 0px 5px;" role="navigation" runat="server">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="btnPreviousPage2" ImageUrl="images/sp_int_leftArrow_btn.gif" Height="9" Width="9"
                                                            AlternateText="Previous Page" runat="server" /><asp:LinkButton ID="PreviousLinkButton2"
                                                            CssClass="tableData" AlternateText="Previous Page" runat="server"><%=Resources.Resource.el_Previous%></asp:LinkButton>
                                                    </td>
                                                    <td class="tableData" align="center">
                                                        &nbsp;<%=Resources.Resource.el_Page%>
                                                        <asp:Label ID="lblCurrentPage2" Text="1" runat="server" />
                                                        <%=Resources.Resource.el_of%>
                                                        <asp:Label ID="lblEndingPage2" Text="1" runat="server" />&nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="NextLinkButton2" CssClass="tableData" AlternateText="Next Page" runat="server">
                                                        <%=Resources.Resource.el_Next%></asp:LinkButton><asp:ImageButton ID="btnNextPage2"
                                                            runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9" Width="9"
                                                            AlternateText="Next Page" />
                                                    </td>
                                                    <td class="tableData" style="text-align: right; padding-left: 25px;">
                                                        <label for="txtPageNumber2"><%=Resources.Resource.el_Page%></label>
                                                    </td>
                                                    <td>
                                                        <SPS:SPSTextBox ID="txtPageNumber2" Width="40px" MaxLength="5" ToolTip="Page Number"
                                                            runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnGoToPage2" ImageUrl="<%$ Resources:Resource,img_btnGoTo%>"
                                                            AlternateText="Goto specified result page" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            </nav>
                                            <%-- Go To Top --%>
                                            <div>
                                                <a class="tableHeaderLink" href="#top" title="Return to the top of this page.">
                                                    <img src="<%=Resources.Resource.repair_sny_type_3_img%>" alt="Return to the top of this page." /></a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 25px; background: url(images/sp_right_bkgd.gif);">
                    &nbsp;
                </td>
            </tr>
        </table>
    </center>
    </form>
</body>
</html>
