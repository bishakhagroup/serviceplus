<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Availability"
    EnableViewStateMac="true" CodeFile="Availability.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>
        <%=Resources.Resource.ttl_Availability%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0"
    marginheight="0" onkeydown="javascript:SetTheFocusButton(event, 'btnClose');">
    <form id="Form1" method="post" runat="server">
    <table width="400" border="0" role="presentation">
        <tr>
            <td style="background-image: url('images/sp_int_popup_login_hdrbkgd_onepix.gif');
                padding-left: 25px;">
                <h1 class="headerText"><%=Resources.Resource.el_Availability %></h1>
            </td>
        </tr>
        <tr>
            <td height="10" style="background-image: url('images/sp_int_popup_login_hdrbkgd_lowr.gif');">
                <h2 class="finderCopyDark">&nbsp;</h2>
            </td>
        </tr>
        <tr>
            <td height="1">
                <table width="100%" height="1" border="0" role="presentation">
                    <tr>
                        <td width="21" style="background-image: url('images/sp_int_header_btm_right.gif'); background-repeat: repeat-x;">
                            &nbsp;
                        </td>
                        <td width="378" style="background-image: url('images/sp_int_header_btm_left_onepix.gif'); background-repeat: repeat-x;">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table border="0" width="365" style="margin-left: 10px" role="presentation">
                    <tr>
                        <td width="350" align="left" colspan="3">
                            <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="18">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="bodyCopyBold">
                            <%=Resources.Resource.el_itemNumber%>
                        </td>
                        <td colspan="2" class="bodyCopyBold">
                            <%=Resources.Resource.el_Description%>:
                        </td>
                    </tr>
                    <tr style="background: #f2f5f8;">
                        <td height="18" class="bodyCopyBold">
                            <asp:Label ID="lblCurrentPartNumber" runat="server" CssClass="bodyCopyBold" />
                        </td>
                        <td colspan="2" class="bodyCopyBold">
                            <asp:Label ID="lblDescription" runat="server" CssClass="bodyCopyBold" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="bodyCopyBold">
                            <asp:Label class="bodyCopyBold" id="lblListPriceTitle" runat="Server" />
                        </td>
                        <td class="bodyCopyBold">
                            <asp:Label class="bodyCopyBold" id="lblYourPriceTitle" runat="Server" />
                        </td>
                        <td id="Discount" class="bodyCopyBold">
                            <asp:Label class="bodyCopyBold" id="lblDiscountTitle" runat="Server" />
                        </td>
                    </tr>
                    <tr style="background: #f2f5f8;">
                        <td class="bodyCopyBold" height="20">
                            <asp:Label ID="lblListPrice" runat="server" CssClass="bodyCopyBold" />
                        </td>
                        <td class="bodyCopyBold">
                            <asp:Label ID="lblYourPrice" runat="server" CssClass="bodyCopyBold" />
                        </td>
                        <td class="bodyCopyBold">
                            <asp:Label ID="lblDiscount" runat="server" CssClass="bodyCopyBold" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="bodyCopyBold">
                            <span>
                                <%=Resources.Resource.el_AvaliabilityStatus%>:</span>
                        </td>
                    </tr>
                    <tr style="background: #f2f5f8;">
                        <td colspan="3" class="bodyCopyBold">
                            <asp:Label ID="lblAvailibility" Height="18px" runat="server" CssClass="bodyCopyBold" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="18">
                            <asp:Label ID="HasCoreChargeLabel" runat="server" CssClass="legalCopy"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <a href="#" id="btnClose" onclick="javascript:window.close();">
                                <img src="<%=Resources.Resource.img_btnCloseWindow()%>" border="0" alt="Close Window"></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" id="tdLegend" runat="server">
                            <span style="font-family: Wingdings; color: red; font-size: 12px;">v</span><span class="bodyCopy"> &nbsp;
                                <%=Resources.Resource.el_LimitedPromoPrice%></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" id="tdrecycle" runat="server" visible="false">
                            <span class="bodyCopy">
                                <%=Resources.Resource.el_RecyclingFee%><a href="http://www.sony.com/recyclemodels"
                                    target='_blank'>http://www.sony.com/recyclemodels</a></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
