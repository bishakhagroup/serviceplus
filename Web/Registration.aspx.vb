﻿Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class New_Register
        Inherits SSL

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            If Not Page.IsPostBack Then
                Dim isAmerica = HttpContextManager.GlobalData.IsAmerica

                spnData.Visible = isAmerica
                spnData1.Visible = isAmerica
                spnData_CA.Visible = Not isAmerica
                spnData1_CA.Visible = Not isAmerica
            End If
        End Sub

        Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnNext.Click
            Try
                Dim admin As New SecurityAdministrator
                Dim custMgr As New CustomerManager
                Dim firstName As String = txtFirstName.Text.Trim()
                Dim lastName As String = txtLastName.Text.Trim()
                Dim emailAddress As String = txtEmail.Text.ToLower().Trim()
                Dim data As String = String.Empty
                Dim userCountInDb As Integer

                ' If the user entered invalid data, this function sets the error message(s).
                If (Not IsDataValid()) Then Exit Sub

                userCountInDb = custMgr.VALIDATECUSTOMERDATA(emailAddress.ToUpper())

                If userCountInDb = 1 Then   ' User exists in the database.
                    ErrorLabel.Text = ConfigurationData.GeneralSettings.SecurityManagement.UserNameUnAvailable  ' "Requested username is unavailable."
                    Exit Sub
                End If

                Try
                    ' Search LDAP to see if the email address has a login, but is not in our DB.
                    data = admin.SearchUser(emailAddress)
                Catch ex As Exception
                    Dim er As String = Utilities.WrapExceptionforUI(ex)
                    er = er.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace) ' "Operation to register timed out. Please try again or"
                    ErrorLabel.Text = er
                    Exit Sub
                End Try

                If Not String.IsNullOrEmpty(data) Then
                    ' User is in LDAP, we need to have them complete Registration for our site DB.
                    Utilities.LogMessages($"Registration.aspx - Existing user found. Data = {data}, Email = {emailAddress}")
                    Dim newUserData = data.Split("*"c)
                    Dim cust As New Customer With {
                        .FirstName = firstName,
                        .LastName = lastName,
                        .CompanyName = newUserData(2),
                        .LdapID = newUserData(3),
                        .EmailAddress = emailAddress
                    }

                    Session.Item("existingSMcustomer") = cust
                    Session.Item("NewUserData") = data

                    Response.Redirect($"SignIn.aspx?Lang={HttpContextManager.GlobalData.LanguageForIDP}&post=existinguser", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                Else
                    ' This is a new user to LDAP and DB
                    Dim newUserData As String = $"{firstName}*{lastName}*{emailAddress}"
                    'Utilities.LogMessages("Registration.aspx - New User Data = " & newUserData)
                    Session.Item("NewUserData") = newUserData

                    Response.Redirect("Register-Email-Verification.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
                'Catch protoEx As ServiceModel.ProtocolException
                'ErrorLabel.Text = "An error occurred while communicating with the Identity Access Manager. " + _
                'Utilities.WrapExceptionforUI(protoEx)
            Catch ex As Exception
                ' ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ' If (ex.Message.Contains("The operation has timed out") Or ex.Message.Contains("Exception of type 'System.Web.Services.Protocols.SoapHeaderException' was thrown") Or ex.Message.Contains("The request failed with HTTP status 502: Bad Gateway")) Then
                If (ex.Message.Contains(Resources.Resource.el_Err_TimeOut) Or ex.Message.Contains(Resources.Resource.el_Err_Exception) Or ex.Message.Contains(Resources.Resource.el_Err_Request)) Then
                    Dim er As String = Utilities.WrapExceptionforUI(ex)
                    er = er.Replace("Unexpected error encountered. For assistance, please", Resources.Resource.el_Err_Unexp_Replace) 'Operation to register timed out. Please try again or")
                    ErrorLabel.Text = er
                Else
                    Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Text = Resources.Resource.el_rgn_Validate '"Registration process has encountered an error: First Name, Last Name, and/or Email Address are not valid.</br>Please try again." '6918
                End If
            End Try
        End Sub

        'Private Function GetListofDomains() As List(Of String)
        '    Try
        '        domainnames.Clear()
        '        Dim xmldoc As New XmlDataDocument()
        '        Dim xmlnode As XmlNodeList
        '        Dim i As Integer

        '        Dim fs As New FileStream(Server.MapPath("Data.xml"), FileMode.Open, FileAccess.Read) 'need to change the path while moving to production
        '        xmldoc.Load(fs)
        '        xmlnode = xmldoc.GetElementsByTagName("DomainNames")
        '        For i = 0 To xmlnode.Count - 1
        '            For j = 0 To xmlnode(i).ChildNodes.Count - 1
        '                domainnames.Add(xmlnode(i).ChildNodes.Item(j).InnerText.Trim())
        '            Next
        '            ' str = xmlnode(i).ChildNodes.Item(0).InnerText.Trim() & "  " & xmlnode(i).ChildNodes.Item(1).InnerText.Trim() & "  " & xmlnode(i).ChildNodes.Item(2).InnerText.Trim()
        '            'MsgBox(str)
        '        Next
        '    Catch ex As Exception
        '    End Try

        '    Return domainnames
        'End Function

        Private Function IsDataValid() As Boolean
            Dim isValid = True
            Dim email = txtEmail.Text.Trim()
            Dim email2 = txtEmail2.Text.Trim()
            Dim fname = txtFirstName.Text.Trim()
            Dim lname = txtLastName.Text.Trim()
            ErrorLabel.Text = ""

            ' Check for empty Email boxes
            If String.IsNullOrEmpty(email) And String.IsNullOrEmpty(email2) Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_req_Email & "<br/>" ' "Please Enter Email and Confirm Email Address."
                isValid = False
            ElseIf String.IsNullOrEmpty(email) And Not String.IsNullOrEmpty(email2) Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_req_Email1 & "<br/>" ' "Please Enter Email Address."
                isValid = False
            ElseIf Not String.IsNullOrEmpty(email) And String.IsNullOrEmpty(email2) Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_req_Email2 & "<br/>" ' "Please Enter Confirm Email Address."
                isValid = False
            End If

            If String.IsNullOrEmpty(lname) Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_req_LastName & "<br/>" ' "Please Enter Last Name."
                isValid = False
            End If

            If String.IsNullOrEmpty(fname) Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_req_FirstName & "<br/>" ' "Please Enter First Name."
                isValid = False
            End If

            'If String.IsNullOrEmpty(txtFirstName.Text.Trim) And String.IsNullOrEmpty(txtLastName.Text.Trim) Then
            '    ErrorLabel.Text = Resources.Resource.el_rgn_req_Name ' "Please Enter First Name And Last Name."
            '    isValid = False
            'End If

            If email <> email2 Then
                ErrorLabel.Text &= Resources.Resource.el_rgn_Validate_Email1 & "<br/>" ' "Email Address values do not match."
                lblEmail.Attributes("class") = "redAsterick"
                lblEmail2.Attributes("class") = "redAsterick"
                isValid = False
            ElseIf Not String.IsNullOrEmpty(email) Then
                Dim sa As New SecurityAdministrator
                If Not sa.IsValidEmail(email) Then  ' Uses RegEx to see if the string is a valid email.
                    lblEmail.Attributes("class") = "redAsterick"
                    ErrorLabel.Text &= Resources.Resource.el_rgn_Validate_Email2 & "<br/>" ' "Email Address is in an invalid format."
                    isValid = False
                ElseIf email.ToLower.Contains("sony.com") Then
                    lblEmail.Attributes("class") = "redAsterick"
                    ErrorLabel.Text &= Resources.Resource.el_rgn_warning_Email1 & "<br/>" ' "Please use a non-Sony email address."
                    isValid = False
                End If
            End If

            Return isValid
        End Function

    End Class

End Namespace
