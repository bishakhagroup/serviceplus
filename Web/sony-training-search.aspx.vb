Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException


Namespace ServicePLUSWebApp

    Partial Class sony_training_search
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(False)
            InitializeComponent()
            FormFieldInitialization()
        End Sub

#End Region

#Region "Page members"


        '-- constants --
#Region "constants"
        'category drop down messages
        Private Const majorCategoryDropdownMessage As String = "All categories."
        Private Const modelNumberDropdownMessage As String = "All models."
        Private Const locationDropdownMessage As String = "All locations."
        Private Const titleDropdownMessage As String = "All titles."
        Private Const monthDropdownMessage As String = "All months."
        Private Const majorCategoryDropdownUnavailableMessage As String = "No categories are available as this time."
        Private Const locationDropdownUnavailableMessage As String = "No locations are available as this time."
        'search parameter array length
        Private Const conSearchParmsArrayLength As Int16 = 7
        Private Const conMaxResultsPerPage As Int16 = 50

        'use to tell if the addToCart icon was clicked.
        Private Const conRegister As String = "register"

#End Region

        '-- variables --
#Region "variables"
        'result page variables
        Private currentResultPage As Integer = 1
        Private endingResultPage As Integer = 1
        Private startingResultItem As Integer = 1
        Private resultPageRequested As Integer = 3
        Private totalSearchResults As Integer = 0
        'sort order variables
        Private sortOrder As enumSortOrder = enumSortOrder.None
        'default form field with focus
        Public focusField As String = "txtModelNumber"
        'Dim objUtilties As Utilities = New Utilities
#End Region

        '-- enumerators --
#Region "enumerators"

        '-- result page
        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
            Current = 5
        End Enum

        '-- sort order 
        Private Enum enumSortOrder
            None = -1
            MajorCategoryASC = 0
            MajorCategoryDESC = 1
            ModelNumberASC = 2
            ModelNumberDESC = 3
            CourseTitleASC = 4
            CourseTitleDESC = 5
            CourseLocationASC = 6
            CourseLocationDESC = 7
        End Enum

        '-- sort type
        Private Enum enumSortBy
            MajorCatagory = 0
            ModelNumber = 1
            CourseTitle = 2
            CourseLocation = 3
        End Enum
#End Region

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here

            FormFieldInitialization()

            errorMessageLabel.Text = ""     '-- clear the error label        
            '-- this is kind of jacked up but the only way i could make it work. - dwd
            '-- if not post back the we want to check the event target form field to see if the add to cart button was click.
            '-- if it was then we change the id of the send and call the control method (processPageRequest). The ID is change
            '-- to make it easy to determine what method need to be called on the control method. -- dwd
            If Not Page.IsPostBack Then
                If Not Request.QueryString(conRegister) Is Nothing Then
                    sender = New UserControl With {
                        .ID = conRegister
                    }
                    ProcessPageRequest(sender, e)
                ElseIf Not Request.QueryString("Model") Is Nothing Then
                    ddlModelNumber.SelectedValue = Request.QueryString("Model")
                End If
                ProcessPageRequest(sender, e)
            Else
                'If Not Request.Form("__EVENTTARGET") Is Nothing And Not Request.Form("__EVENTARGUMENT") Is Nothing Then
                'If Request.Form("__EVENTTARGET").ToString() = conRegister Then
                '    sender = New Web.UI.UserControl
                '    sender.ID = conRegister
                '   processPageRequest(sender, e)
                'End If
                'End If
            End If
        End Sub

#End Region

#Region "Methods"

        Private Sub FormFieldInitialization()
            Page.ID = Request.Url.AbsolutePath.ToString()
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage')"
            txtPageNumber2.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnGoToPage2')"
            errorMessageLabel.Text = ""
        End Sub

        Private Sub ProcessPageRequest(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim callingControl As String = String.Empty

                If Not sender.ID Is Nothing Then callingControl = sender.ID.ToString()

                Select Case (callingControl)
                    Case Page.ID.ToString()
                        PopulateModelNumberDropDown()
                        PopulateMajorCategoryDropDown()              '-- populate the ddl
                        PopulateLocationDropDown()
                        PopulateTitleDropDown()
                        PopulateMonthDropDown()
                    Case conRegister
                        ProcessRegisterRequest()                       '-- register
                End Select

                If IsPostBack Then
                    tdResult.Visible = True
                    resultPageRequested = GetRequestedPage(callingControl)  '-- set the result page requested             
                    SetSortOrder(callingControl)                            '-- set the sort order
                    SearchForTrainingClass()                                     '-- search for software

                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

#End Region

#Region "Search Functionality"
        Private Sub SearchForTrainingClass()
            BuildSearchResultsTable(GetSearchParms())
        End Sub

        Private Function GetSearchParms() As String()
            Dim returnSearchParms(conSearchParmsArrayLength) As String
            Dim modelNumber As String
            Dim courseNumber As String
            Dim categoryName As String
            Dim title As String
            Dim location As String
            Dim city As String = ""
            Dim state As String = ""
            Dim month As Integer

            Try
                'modelNumber = ddlModelNumber.SelectedValue.Trim().Replace("-", "")
                modelNumber = ddlModelNumber.SelectedValue.Trim()
                If modelNumber = modelNumberDropdownMessage Then modelNumber = ""

                'courseNumber = txtCourseNumber.Text.ToString().Trim().Replace("-", "")
                courseNumber = txtCourseNumber.Text.Trim()

                categoryName = ddMajorCategory.SelectedValue.Trim()
                If categoryName = majorCategoryDropdownMessage Then categoryName = ""

                title = ddTitle.SelectedValue.Trim()
                If title = titleDropdownMessage Then title = ""

                location = ddLocation.SelectedValue.ToString().Trim()
                If location <> locationDropdownMessage Then
                    Dim split As String() = location.Split(",")
                    city = If(split(0), "")
                    state = If(split(1), "")
                End If

                month = ddMonth.SelectedIndex
                If month = 0 Then month = -1

                '-- just a check to make sure we are not passing in to much data. --
                If modelNumber.Length > 30 Then modelNumber.Substring(0, 30)
                If categoryName.Length > 100 Then categoryName.Substring(0, 100)
                If categoryName = majorCategoryDropdownMessage Then categoryName = "" '-- if category name = "all categories" then clear category name to return all categories. -dwd

                returnSearchParms.SetValue(modelNumber, 0)
                returnSearchParms.SetValue(courseNumber, 1)
                returnSearchParms.SetValue(categoryName, 2)
                returnSearchParms.SetValue(title, 3)
                returnSearchParms.SetValue(city, 4)
                returnSearchParms.SetValue(state, 5)
                returnSearchParms.SetValue(month.ToString(), 6)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnSearchParms
        End Function

        Private Function BuildSearchResultsTable(ByVal searchParms As String()) As Table
            Dim courseResults() As CourseSchedule
            Dim courseResultsDisplayed As New ArrayList
            Dim resultTable As New Table
            Dim currentResultItem As Int32 = 1
            Dim x As Integer = 0

            Try
                Dim accountType As enumAccountType = SSL.enumAccountType.Unknown '6668
                If HttpContextManager.Customer IsNot Nothing Then accountType = getUsersAccountType(HttpContextManager.Customer.SIAMIdentity)

                '-- get search results collection
                courseResults = GetCourseResultCollection(searchParms)

                If courseResults?.Length > 0 Then
                    '-- calculate pagination 
                    totalSearchResults = courseResults.Length
                    CalcStaringResultItem(resultPageRequested)

                    'sort the collection of software if the order is set  
                    If sortOrder <> enumSortOrder.None Then
                        CourseSchedule.SortOrder = sortOrder
                        Array.Sort(courseResults)
                    End If

                    '-- loop through all the software 
                    For Each courseItem As CourseSchedule In courseResults
                        If currentResultItem >= startingResultItem Then
                            CourseSearchResultTable.Controls.Add(BuildCourseResultRow(courseItem, x, accountType))
                            courseResultsDisplayed.Add(courseItem)
                            x += 1
                        End If
                        currentResultItem += 1
                        If currentResultItem >= (startingResultItem + conMaxResultsPerPage) Then Exit For
                    Next

                    '-- add collection of displayed course to array - this is used for register functionality.
                    Session.Add("courseschedule", courseResultsDisplayed)
                Else
                    '-- no course found --
                    CourseSearchResultTable.Controls.Add(BuildCourseResultRow(totalSearchResults))
                End If

                '-- update the pagination controlsl                 
                UpdateResultPageNav()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return resultTable
        End Function

        Private Function GetCourseResultCollection(ByVal searchParms As String()) As CourseSchedule()
            Dim resultCollection As CourseSchedule() = Nothing
            Dim courseManager As New CourseManager

            Try
                resultCollection = courseManager.CourseSearch(searchParms(2), searchParms(0), searchParms(1), searchParms(3), searchParms(4), searchParms(5), "", searchParms(6))
            Catch ex As Exception
                Dim debugMessage As String = $"sony-training-search.GetCourseResultCollection- Search Params for error below:{Environment.NewLine}
                    Model: {If(searchParms(0), "null")}, Course: {If(searchParms(1), "null")}, Category: {If(searchParms(2), "null")}, 
                    Title: {If(searchParms(3), "null")}, City: {If(searchParms(4), "null")}, State: {If(searchParms(5), "null")}, Month: {If(searchParms(6), "null")}"
                Utilities.LogMessages(debugMessage)
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return resultCollection
        End Function


        Private Function GetRequestedPage(ByVal callingControl As String) As enumResultPageRequested
            Dim returnValue As enumResultPageRequested = enumResultPageRequested.Current
            Try
                Select Case (callingControl)
                    Case Page.ID.ToString(), conRegister
                        returnValue = enumResultPageRequested.Current
                    Case btnSearch.ID.ToString()
                        returnValue = enumResultPageRequested.FirstPage
                    Case btnGoToPage.ID.ToString, btnGoToPage2.ID.ToString
                        returnValue = enumResultPageRequested.UserEnteredPage
                    Case btnPreviousPage.ID.ToString, PreviousLinkButton.ID.ToString(), btnPreviousPage2.ID.ToString, PreviousLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.PreviousPage
                    Case btnNextPage.ID.ToString(), NextLinkButton.ID.ToString(), btnNextPage2.ID.ToString(), NextLinkButton2.ID.ToString()
                        returnValue = enumResultPageRequested.NextPage
                    Case ID.ToString, CategorySortLink.ID.ToString, ModelSortLink.ID.ToString, TitleSortLink.ID.ToString, LocationSortLink.ID.ToString
                        returnValue = enumResultPageRequested.UserEnteredPage
                End Select
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

#End Region

#Region "result table"

        Private Function BuildCourseResultRow(ByVal course As CourseSchedule, ByVal x As Integer, ByVal accountType As enumAccountType) As TableRow
            Dim tr As New TableRow

            Try
                Dim td As New TableCell
                Dim majorCategory As New Label
                Dim modelNumber As New Label
                Dim title As New HyperLink
                Dim location As New Label
                Dim courseNumber As New Label
                Dim startDate As New Label
                Dim listPrice As New Label
                Dim endDate As New Label

                majorCategory.ID = "majorCategory" + x.ToString
                modelNumber.ID = "modelNumber" + x.ToString
                title.ID = "title" + x.ToString
                location.ID = "location" + x.ToString
                courseNumber.ID = "courseNumber" + x.ToString
                startDate.ID = "startDate" + x.ToString
                listPrice.ID = "listPrice" + x.ToString
                endDate.ID = "endDate" + x.ToString

                majorCategory.CssClass = "tableData"
                modelNumber.CssClass = "tableData"
                title.CssClass = "tableData"
                title.ForeColor = System.Drawing.Color.Blue
                location.CssClass = "tableData"
                courseNumber.CssClass = "tableData"
                startDate.CssClass = "tableData"
                listPrice.CssClass = "tableData"
                endDate.CssClass = "tableData"

                majorCategory.Text = "&nbsp;"
                modelNumber.Text = "&nbsp;"
                title.Text = "&nbsp;"
                location.Text = "&nbsp;"
                courseNumber.Text = "&nbsp;"
                startDate.Text = "&nbsp;"
                listPrice.Text = "&nbsp;"
                endDate.Text = "&nbsp;"
                title.NavigateUrl = "sony-training-search.aspx?" + conRegister + "=" + x.ToString()

                If Not course.Course.MinorCategory Is Nothing Then majorCategory.Text = course.Course.MajorCategory.Description
                If Not course.Course.Model Is Nothing Then modelNumber.Text = course.Course.Model
                If Not course.Course.Title Is Nothing Then title.Text = course.Course.Title
                If Not course.Location Is Nothing Then location.Text = course.Location.State
                If Not course.Course.Number Is Nothing Then courseNumber.Text = course.Course.Number
                startDate.Text = course.StartDate.ToString("MM/dd/yyyy")
                If startDate.Text = NullDateTime Then
                    startDate.Text = "TBD"
                End If
                endDate.Text = course.EndDate.ToString("MM/dd/yyyy")
                If endDate.Text = NullDateTime Then
                    endDate.Text = "TBD"
                End If


                ' added course type to check for non classroom training
                ' if the type is non class room training, then the start and end date should not be displayed
                ' added by Deepa V Feb 16,2006
                Dim cm As New CourseManager
                cm.GetCourseVacancy(course, True)
                Dim courseType As New CourseType With {
                    .PartNumber = course.PartNumber
                }
                cm.GetCourseType(courseType)

                If courseType.Code <> 0 Then
                    startDate.Visible = False
                    endDate.Visible = False
                End If

                If course.Course.ListPrice > 0 Then listPrice.Text = String.Format("{0:c}", course.Course.ListPrice).ToString()

                'register.CommandArgument = x.ToString()
                'register.ImageUrl = "images/sp_int_add2Cart_btn.gif"

                ' -- first search result row
                ' -- major category
                td.Controls.Add(New LiteralControl(vbCrLf + "<table width=""665"" cellpadding=""0"" cellspacing=""0"" border=""0"">" + vbCrLf + "<tr>" + vbCrLf))

                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""20"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""110"">&nbsp;&nbsp;"))
                td.Controls.Add(majorCategory)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- model number
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""80"">&nbsp;&nbsp;"))
                td.Controls.Add(modelNumber)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- title
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""320"">&nbsp;&nbsp;"))
                td.Controls.Add(title)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                ' -- location 
                'display location only when it is class room training
                ' added by Deepa V, feb 17,2006

                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" align=""center"" width=""35"">&nbsp;&nbsp;"))
                If Not (courseType.Code <> 0) Then
                    td.Controls.Add(location)
                End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                ' -- course number
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""115"">&nbsp;&nbsp;"))
                td.Controls.Add(courseNumber)
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- instructor
                'td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                'td.Controls.Add(New LiteralControl(vbTab + "<td class=""tableData"" width=""193"">&nbsp;&nbsp;"))
                'td.Controls.Add(courseInstructor)
                'td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                ' -- spacer
                td.Controls.Add(New LiteralControl(vbTab + "<td width=""1"" bgColor=""#d5dee9""><IMG height=""20"" src=""images/spacer.gif"" width=""1""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                '-- second search result row
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                'td.Controls.Add(New LiteralControl(vbTab + "<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "<td colspan=""11"" bgcolor=""F2F5F8"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "<table width=""665"" border=""0"" cellspacing=""0"" cellpadding=""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""40"" height=""4""></td>" + vbCrLf))

                ' check if the class type is non class room training 
                ' if so start and end date needs to be hidden
                ' added by Deepa V Feb 16,2006
                If Not (course.Course.Type.TypeCode <> 0) Then
                    ' start date -- 
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"">Start Date</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td class=""tableData"" width=""80"">&nbsp;"))
                    td.Controls.Add(startDate)
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    ' -- end date
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"">End Date</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td class=""tableData"" width=""80"">&nbsp;"))
                    td.Controls.Add(endDate)
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                Else
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"">&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td class=""tableData"" width=""80"">&nbsp;"))
                    td.Controls.Add(New LiteralControl("</td>"))
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"">&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td class=""tableData"" width=""80"">&nbsp;"))
                    td.Controls.Add(New LiteralControl("</td>"))
                End If

                If course.Course.Type.TypeCode <> 0 Then
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""20"" height=""4""></td>" + "<td class=""tableHeader"" Width=""30""></td>" + vbCrLf))

                    ' -- list price
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData"" width=""60"">&nbsp;"))
                    'td.Controls.Add(listPrice)
                Else
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""20"" height=""4""></td>" + "<td class=""tableHeader"" Width=""30"">Price:</td>" + vbCrLf))

                    ' -- list price
                    td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td class=""tableData"" width=""60"">&nbsp;"))
                    td.Controls.Add(listPrice)
                End If

                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                ' -- register        
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""10"" height=""4""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td width=""5"">"))
                'If Session.Item("customer") Is Nothing Then
                'td.Controls.Add(Register2)
                'Else
                'td.Controls.Add(register)
                'End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))

                ' -- spacers
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td><img src=""images/spacer.gif"" width=""1"" height=""4""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "<tr bgcolor=""D5DEE9"">" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + vbTab + "<td colspan=""11""><img src=""images/spacer.gif"" width=""1"" height=""3""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + vbTab + "</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + vbTab + "</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl(vbTab + "</td>" + vbCrLf))
                'td.Controls.Add(New LiteralControl(vbTab + "<td bgcolor=""D5DEE9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf + "</table>" + vbCrLf))

                tr.Controls.Add(td)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        '-- this method is a override used when no results are found.
        Private Function BuildCourseResultRow(ByVal totalResults As Integer) As TableRow
            Dim tr As New TableRow

            Dim td As New TableCell

            '-- no result for search - show search parameters
            td.Controls.Add(New LiteralControl(vbCrLf + "<table cellpadding=""0"" cellspacing=""0"" border=""0"">" + vbTab + "<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>No match found for</strong></td>"))
            td.Controls.Add(New LiteralControl("</tr>"))
            If ddMajorCategory.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Category: </strong>" + ddMajorCategory.SelectedValue + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If ddlModelNumber.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Model Number: </strong>" + ddlModelNumber.SelectedValue + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If ddTitle.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Course Title: </strong>" + ddTitle.SelectedValue.ToString() + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If ddLocation.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Course Location: </strong>" + ddLocation.SelectedValue.ToString() + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If txtCourseNumber.Text <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Course Number: </strong>" + txtCourseNumber.Text.ToString() + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            If ddMonth.SelectedValue <> "" Then
                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td colspan=""11""><span class=""bodyCopy""><strong>Month: </strong>" + ddMonth.SelectedValue.ToString() + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr
        End Function

#End Region

#Region "Pagination"
        Private Sub CalcCurrentResultPage()
            Try
                currentResultPage = Int(startingResultItem / conMaxResultsPerPage) + IIf((startingResultItem Mod conMaxResultsPerPage) = 0, 0, 1)
                If currentResultPage > endingResultPage Then currentResultPage = endingResultPage
            Catch ex As Exception
                currentResultPage = 1
            End Try
        End Sub

        Private Sub CalcEndingResultPage()
            Try
                endingResultPage = Int(totalSearchResults / conMaxResultsPerPage) + IIf((totalSearchResults Mod conMaxResultsPerPage) = 0, 0, 1)
            Catch ex As Exception
                endingResultPage = 1
            End Try
        End Sub

        Private Sub CalcStaringResultItem(ByVal thisPage As enumResultPageRequested)
            Dim tempPageNumber As Integer = 1
            Dim tempCurrentPageNumber As Integer = 1
            CalcEndingResultPage()

            Try
                If txtPageNumber.Text.ToString().Trim() <> "" Or txtPageNumber2.Text.ToString().Trim() <> "" Then
                    If IsNumeric(txtPageNumber.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber.Text.ToString())
                    ElseIf IsNumeric(txtPageNumber2.Text.ToString()) Then
                        tempPageNumber = Integer.Parse(txtPageNumber2.Text.ToString())
                    End If
                    If tempPageNumber > endingResultPage Then tempPageNumber = endingResultPage
                End If

                If lblCurrentPage.Text.ToString().Trim() <> "" Then
                    If IsNumeric(lblCurrentPage.Text.ToString()) Then tempCurrentPageNumber = Integer.Parse(lblCurrentPage.Text.ToString())
                End If

                Select Case (thisPage)
                    Case enumResultPageRequested.FirstPage
                        startingResultItem = 1
                    Case enumResultPageRequested.LastPage
                        If endingResultPage > 1 Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.NextPage
                        If (tempCurrentPageNumber + 1) > endingResultPage Then
                            startingResultItem = (((endingResultPage - 1) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = (((tempCurrentPageNumber) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.PreviousPage
                        If tempCurrentPageNumber > 2 Then
                            startingResultItem = (((tempCurrentPageNumber - 2) * conMaxResultsPerPage) + 1)
                        Else
                            startingResultItem = 1
                        End If

                    Case enumResultPageRequested.UserEnteredPage
                        If tempPageNumber <= 1 Then
                            startingResultItem = 1
                        Else
                            startingResultItem = (((tempPageNumber - 1) * conMaxResultsPerPage) + 1)
                        End If

                    Case enumResultPageRequested.Current
                        startingResultItem = (((tempCurrentPageNumber - 1) * conMaxResultsPerPage) + 1)

                End Select

                CalcCurrentResultPage()

            Catch ex As Exception
                startingResultItem = 1
            End Try
        End Sub

        Private Sub UpdateResultPageNav()
            Try
                Dim hidePrevButtons As Boolean = False
                Dim hideNextButtons As Boolean = False

                PreviousLinkButton.Visible = True
                btnPreviousPage.Visible = True
                PreviousLinkButton2.Visible = True
                btnPreviousPage2.Visible = True
                NextLinkButton.Visible = True
                btnNextPage.Visible = True
                NextLinkButton2.Visible = True
                btnNextPage2.Visible = True

                Select Case resultPageRequested
                    Case enumResultPageRequested.FirstPage
                        hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.LastPage
                        hideNextButtons = True

                        lblCurrentPage.Text = endingResultPage.ToString()
                        lblCurrentPage2.Text = endingResultPage.ToString()
                    Case enumResultPageRequested.NextPage
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.PreviousPage
                        If currentResultPage = 1 Then hidePrevButtons = True


                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case enumResultPageRequested.UserEnteredPage, enumResultPageRequested.Current
                        If currentResultPage = 1 Then hidePrevButtons = True
                        If endingResultPage = currentResultPage Then hideNextButtons = True

                        lblCurrentPage.Text = currentResultPage.ToString()
                        lblCurrentPage2.Text = currentResultPage.ToString()
                    Case Else
                        lblCurrentPage.Text = "1"
                End Select

                If hidePrevButtons Then
                    PreviousLinkButton.Visible = False
                    btnPreviousPage.Visible = False
                    PreviousLinkButton2.Visible = False
                    btnPreviousPage2.Visible = False
                End If
                If hideNextButtons Then
                    NextLinkButton.Visible = False
                    btnNextPage.Visible = False
                    NextLinkButton2.Visible = False
                    btnNextPage2.Visible = False
                End If

                lblEndingPage.Text = endingResultPage.ToString()
                lblEndingPage2.Text = endingResultPage.ToString()
                txtPageNumber.Text = ""
                txtPageNumber2.Text = ""

                ' added to hide the go to buttons and text boxes when the search result is not > 1
                ' added by Deepa V Feb 16,2006
                If Convert.ToInt32(endingResultPage.ToString()) <= 1 Then
                    btnGoToPage.Visible = False
                    btnGoToPage2.Visible = False
                    txtPageNumber.Visible = False
                    txtPageNumber2.Visible = False
                    lblPg1.Visible = False
                    lblPg2.Visible = False
                Else
                    btnGoToPage.Visible = True
                    btnGoToPage2.Visible = True
                    txtPageNumber.Visible = True
                    txtPageNumber2.Visible = True
                    lblPg1.Visible = True
                    lblPg2.Visible = True
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "training collection sorting"
        Private Sub SetSortOrder(ByVal callingControl As String)

            Select Case (callingControl)
                Case CategorySortLink.ID.ToString()
                    If CategorySortLink.CommandArgument.ToString() = enumSortOrder.MajorCategoryASC.ToString() Or CategorySortLink.CommandArgument.ToString() = enumSortOrder.MajorCategoryASC.ToString() Then
                        sortOrder = enumSortOrder.MajorCategoryDESC
                        CategorySortLink.CommandArgument = enumSortOrder.MajorCategoryDESC.ToString()
                    Else
                        sortOrder = enumSortOrder.MajorCategoryASC
                        CategorySortLink.CommandArgument = enumSortOrder.MajorCategoryASC.ToString()
                    End If

                    btnSearch.CommandArgument = enumSortBy.MajorCatagory.ToString()

                Case ModelSortLink.ID.ToString()

                    If ModelSortLink.CommandArgument.ToString() = enumSortOrder.ModelNumberASC.ToString() Then
                        sortOrder = enumSortOrder.ModelNumberDESC
                        ModelSortLink.CommandArgument = enumSortOrder.ModelNumberDESC.ToString()
                    Else
                        sortOrder = enumSortOrder.ModelNumberASC
                        ModelSortLink.CommandArgument = enumSortOrder.ModelNumberASC.ToString()
                    End If

                    btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString()

                Case TitleSortLink.ID.ToString()

                    If TitleSortLink.CommandArgument.ToString() = enumSortOrder.CourseTitleASC.ToString() Then
                        sortOrder = enumSortOrder.CourseTitleDESC
                        TitleSortLink.CommandArgument = enumSortOrder.CourseTitleDESC.ToString()
                    Else
                        sortOrder = enumSortOrder.CourseTitleASC
                        TitleSortLink.CommandArgument = enumSortOrder.CourseTitleASC.ToString()
                    End If

                    btnSearch.CommandArgument = enumSortBy.CourseTitle.ToString()

                Case LocationSortLink.ID.ToString()

                    If LocationSortLink.CommandArgument.ToString() = enumSortOrder.CourseLocationASC.ToString() Then
                        sortOrder = enumSortOrder.CourseLocationDESC
                        LocationSortLink.CommandArgument = enumSortOrder.CourseLocationDESC.ToString()
                    Else
                        sortOrder = enumSortOrder.CourseLocationASC
                        LocationSortLink.CommandArgument = enumSortOrder.CourseLocationASC.ToString()
                    End If

                    btnSearch.CommandArgument = enumSortBy.CourseLocation.ToString()

                Case btnSearch.ID.ToString()
                    ' we need to sort my model number if in a Major Category Search and "All Categories" is not selected -- dwd
                    'If ddMajorCategory.SelectedValue.ToString() <> "" Then
                    'sortOrder = enumSortOrder.ModelNumberASC
                    'ModelSortLink.CommandArgument = enumSortOrder.ModelNumberASC.ToString()
                    'btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString()
                    'Else
                    '   sortOrder = enumSortOrder.MajorCategoryASC
                    '   CategorySortLink.CommandArgument = enumSortOrder.MajorCategoryASC.ToString()
                    '   CategorySortLink.CommandArgument = enumSortOrder.MajorCategoryASC.ToString()

                    '   btnSearch.CommandArgument = enumSortBy.MajorCatagory.ToString()
                    'End If
                Case btnGoToPage.ID.ToString(), btnNextPage.ID.ToString(), btnPreviousPage.ID.ToString(), NextLinkButton.ID.ToString(), PreviousLinkButton.ID.ToString(), btnGoToPage2.ID.ToString(), btnNextPage2.ID.ToString(), btnPreviousPage2.ID.ToString(), NextLinkButton2.ID.ToString(), PreviousLinkButton2.ID.ToString(), conRegister.ToString()
                    If btnSearch.CommandArgument = enumSortBy.MajorCatagory.ToString() Then
                        If CategorySortLink.CommandArgument.ToString() = enumSortOrder.MajorCategoryASC.ToString() Then
                            sortOrder = enumSortOrder.MajorCategoryASC
                        Else
                            sortOrder = enumSortOrder.MajorCategoryDESC
                        End If
                        btnSearch.CommandArgument = enumSortBy.MajorCatagory.ToString()
                    ElseIf btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString() Then
                        If ModelSortLink.CommandArgument.ToString() = enumSortOrder.ModelNumberASC.ToString() Then
                            sortOrder = enumSortOrder.ModelNumberASC
                        Else
                            sortOrder = enumSortOrder.ModelNumberDESC
                        End If
                        btnSearch.CommandArgument = enumSortBy.ModelNumber.ToString()
                    ElseIf btnSearch.CommandArgument = enumSortBy.CourseTitle.ToString() Then
                        If TitleSortLink.CommandArgument.ToString() = enumSortOrder.CourseTitleASC.ToString() Then
                            sortOrder = enumSortOrder.CourseTitleASC
                        Else
                            sortOrder = enumSortOrder.CourseTitleDESC
                        End If
                        btnSearch.CommandArgument = enumSortBy.CourseTitle.ToString()
                    ElseIf btnSearch.CommandArgument = enumSortBy.CourseLocation.ToString() Then
                        If LocationSortLink.CommandArgument.ToString() = enumSortOrder.CourseLocationASC.ToString() Then
                            sortOrder = enumSortOrder.CourseLocationASC
                        Else
                            sortOrder = enumSortOrder.CourseLocationDESC
                        End If
                        btnSearch.CommandArgument = enumSortBy.CourseLocation.ToString()
                    End If

            End Select
        End Sub
#End Region

#Region "add to cart"

        Private Sub ProcessRegisterRequest()
            Try
                'If Not Request.Form("__EVENTARGUMENT") Is Nothing And Not Session.Item("courseschedule") Is Nothing Then
                'Dim itemNumber As Integer = Integer.Parse(Request.Form("__EVENTARGUMENT"))
                Dim itemNumber As Integer = Integer.Parse(Request.QueryString(conRegister))
                Dim courseClass As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)

                '-- make sure we have a price for item 
                If courseClass.Course.ListPrice > 0 Then
                    Response.Redirect("sony-training-class-details.aspx?ItemNumber=" + itemNumber.ToString)
                Else
                    '-- not price display message to user on what to do.
                    errorMessageLabel.Text = "Requested course not available. Please contact ServicesPLUS at 1-800-538-7550 and request status on availability."
                End If
                'End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "populate dropdown list"
        Private Sub PopulateMajorCategoryDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim categories() As String = courseDataManager.GetMajorCategoryDescritions(majorCategoryDropdownMessage)
                ddMajorCategory.DataSource = categories
                ddMajorCategory.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub PopulateModelNumberDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim modelnumbers() As String = courseDataManager.GetModelNumber(modelNumberDropdownMessage)
                ddlModelNumber.DataSource = modelnumbers
                ddlModelNumber.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub PopulateTitleDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim titles() As String = courseDataManager.GetCourseTitles(titleDropdownMessage)
                ddTitle.DataSource = titles
                ddTitle.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub PopulateLocationDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim location() As CourseLocation = courseDataManager.GetLocations()
                ddLocation.Items.Add(New ListItem(locationDropdownMessage, locationDropdownMessage))
                For Each cl As CourseLocation In location
                    ddLocation.Items.Add(New ListItem(cl.City + ", " + cl.State, cl.City + "," + cl.State))
                Next
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub PopulateMonthDropDown()
            Dim courseManager As New CourseManager
            Dim monthes() As String = {"All months.", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

            ddMonth.DataSource = monthes
            ddMonth.DataBind()
        End Sub

#End Region

        Private Sub allImage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPreviousPage.Click, btnNextPage.Click, btnSearch.Click, btnGoToPage.Click, btnPreviousPage2.Click, btnNextPage2.Click, btnGoToPage2.Click
            ProcessPageRequest(sender, e)
        End Sub

        Private Sub allLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModelSortLink.Click, PreviousLinkButton.Click, NextLinkButton.Click, CategorySortLink.Click, PreviousLinkButton2.Click, NextLinkButton2.Click, TitleSortLink.Click, LocationSortLink.Click
            ProcessPageRequest(sender, e)
        End Sub

    End Class

End Namespace
