Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Text.RegularExpressions


Namespace ServicePLUSWebApp
    Partial Class PrintRegisteredProduct
        Inherits System.Web.UI.Page

        Public bColor As Boolean = False
        Dim objProductRegistration As New Sony.US.ServicesPLUS.Core.ProductRegistration

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Session("RegisterProduct") Is Nothing Then
                objProductRegistration = CType(Session("RegisterProduct"), Sony.US.ServicesPLUS.Core.ProductRegistration)
                If objProductRegistration Is Nothing Then
                    Session.Remove("RegisterProduct")
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "ClosePrintPage", "<script language=javascript>window.close();</script>")
                End If
                bodyDiv.InnerHtml = buildBody()
            End If
        End Sub
        
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "PrintPage", "<script language=javascript>PrintPage();</script>")
        End Sub
        Private Function buildBody() As String
            If objProductRegistration Is Nothing Then
                Return String.Empty
            End If
            Dim sAddr As String = String.Empty
            Dim sb As StringBuilder = New StringBuilder()
            Dim bgColorProduct As Boolean = False

            sb.Append("<table border=""0"" cellspacing=""0"" cellpadding=""0"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">")
            sb.Append("<tr>")
            sb.Append("<table align=""center"" cellpadding=""3"" cellspacing=""1"" border=""0"" style=""width: 99%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width:1px; border-right-color:666666; border-right-style:solid; border-right-width:1px; border-left-color:666666; border-left-style:solid; border-left-width:1px; border-top-color:666666; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:666666"">")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin;""><img src=""https://www.servicesplus.sel.sony.com/images/sp_int_sonylogo.gif""/></td>")
            sb.Append("<td style=""border-bottom-style:solid; border-bottom-width:thin;"" align=""right"" valign=""middle"">&nbsp;</td>")
            sb.Append("</tr>")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td colspan=""2"" style=""border-bottom-style:solid; border-bottom-width:thin;"">")
            sb.Append("<table border=""0"" cellpadding=""1"" cellspacing=""1"" style=""width: 100%"">")
            sb.Append("<tr>")
            sb.Append("<td colspan=""2"" style=""height: 16px"" align=""center""><strong>ServicesPLUS -" + vbCrLf + "Service Agreement</strong></td>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("<td style=""width: 100px""></td>")
            sb.Append("</tr>")
            sb.Append("</table>")
            sb.Append("</td>")
            sb.Append("</tr>")
            sb.Append("<tr valign=""top"">")
            sb.Append("<td style=""width: 173px;"">Date:</td>")
            sb.Append($"<td style=""width: 376px;"">{DateTime.Now.ToString("MM/dd/yyyy")}</td>")
            sb.Append("</tr>")

            sb.Append("<tr valign=""top""  bgColor=""#d5dee9"">")
            sb.Append("<td style=""width: 173px;"">Purchased From:</td>")
            sb.Append("<td style=""width: 376px;"">" + objProductRegistration.Dealer + "</td>")
            sb.Append("</tr>")
            bColor = True

            If Len(Trim(objProductRegistration.CustomerAddress.Company)) > 0 Then
                sAddr = objProductRegistration.CustomerAddress.Company
            End If

            If Len(Trim(objProductRegistration.CustomerAddress.FirstName)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.CustomerAddress.FirstName
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.LastName)) > 0 Then
                sAddr = sAddr + " " + objProductRegistration.CustomerAddress.LastName
            End If

            If Len(Trim(objProductRegistration.CustomerAddress.Address1)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.CustomerAddress.Address1
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.Address2)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.CustomerAddress.Address2
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.City)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.CustomerAddress.City
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.State)) > 0 Then
                sAddr = sAddr + ", " + objProductRegistration.CustomerAddress.State
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.ZipCode)) > 0 Then
                sAddr = sAddr + " " + objProductRegistration.CustomerAddress.ZipCode
            End If

            If Len(Trim(objProductRegistration.CustomerAddress.EmailAddress)) > 0 Then
                sAddr = sAddr + "<br/><br/>" + "Email: " + objProductRegistration.CustomerAddress.EmailAddress
            End If

            If Len(Trim(objProductRegistration.CustomerAddress.Phone)) > 0 Then
                sAddr = sAddr + "<br/><br/>Phone: " + objProductRegistration.CustomerAddress.Phone
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.PhoneExtn)) > 0 Then
                sAddr = sAddr + " (" + objProductRegistration.CustomerAddress.PhoneExtn + ")"
            End If
            If Len(Trim(objProductRegistration.CustomerAddress.Fax)) > 0 Then
                sAddr = sAddr + "<br/>Fax: " + objProductRegistration.CustomerAddress.Fax
            End If

            If bColor = False Then
                sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                bColor = True
                sb.Append("<td colspan=""2"" class=bodycopy ><table cellpadding=""3"" cellspacing=""1"" border=""1"" style=""width: 100%;font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666; border-bottom-width:1px; border-right-color:666666; border-right-style:solid; border-right-width:1px; border-left-color:666666; border-left-style:solid; border-left-width:1px; border-top-color:666666; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-bottom-color:666666"">")
                sb.Append("<tr style=""font-weight:normal;font-style:normal;text-decoration:none;"" width=""100%"" ><td valign=top width=""50%"" valign=""top"">")
                sb.Append("<u>Customer Information:</u><br/>" + vbCrLf + sAddr + "</td>")
                'sb.Append("<td>" + sAddr + "</td>")
                'sb.Append("</tr>")
                '<td></tr></td>
            Else
                sb.Append("<tr valign=""top"">")
                bColor = False
                sb.Append("<td colspan=""2"" class=bodycopy ><table cellpadding=""3"" cellspacing=""1"" border=""0"" width= 100%>")
                sb.Append("<tr style=""font-weight:normal;font-style:normal;text-decoration:none;"" width=""100%"" ><td valign=top width=""50%"" valign=""top"">")
                sb.Append("<u><span class=tableheader>Customer Information:</span></u><br/>" + vbCrLf + "<span class=bodycopy>" + sAddr + "</span></td>")
                'sb.Append("<td>" + sAddr + "</td>")
                'sb.Append("</tr>")
                '<td></tr></td>

            End If

            If Len(Trim(objProductRegistration.LocationAddress.Company)) > 0 Then
                sAddr = objProductRegistration.LocationAddress.Company
            End If

            If Len(Trim(objProductRegistration.LocationAddress.FirstName)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.LocationAddress.FirstName
            End If
            If Len(Trim(objProductRegistration.LocationAddress.LastName)) > 0 Then
                sAddr = sAddr + " " + objProductRegistration.LocationAddress.LastName
            End If

            If Len(Trim(objProductRegistration.LocationAddress.Address1)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.LocationAddress.Address1
            End If
            If Len(Trim(objProductRegistration.LocationAddress.Address2)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.LocationAddress.Address2
            End If
            If Len(Trim(objProductRegistration.LocationAddress.City)) > 0 Then
                sAddr = sAddr + "<br/>" + objProductRegistration.LocationAddress.City
            End If
            If Len(Trim(objProductRegistration.LocationAddress.State)) > 0 Then
                sAddr = sAddr + ", " + objProductRegistration.LocationAddress.State
            End If
            If Len(Trim(objProductRegistration.LocationAddress.ZipCode)) > 0 Then
                sAddr = sAddr + " " + objProductRegistration.LocationAddress.ZipCode
            End If

            If Len(Trim(objProductRegistration.LocationAddress.EmailAddress)) > 0 Then
                sAddr = sAddr + "<br/><br/>" + "Email: " + objProductRegistration.LocationAddress.EmailAddress
            End If

            If Len(Trim(objProductRegistration.LocationAddress.Phone)) > 0 Then
                sAddr = sAddr + "<br/><br/>Phone: " + objProductRegistration.LocationAddress.Phone
            End If
            If Len(Trim(objProductRegistration.LocationAddress.PhoneExtn)) > 0 Then
                sAddr = sAddr + " (" + objProductRegistration.LocationAddress.PhoneExtn + ")"
            End If
            If Len(Trim(objProductRegistration.LocationAddress.Fax)) > 0 Then
                sAddr = sAddr + "<br/>Fax: " + objProductRegistration.LocationAddress.Fax
            End If

            sb.Append("<td valign=top width=""50%"" valign=""top""><u><span class=tableheader>Product Location:</span></u><br/>" + vbCrLf + "<span class=bodycopy>" + sAddr + "</span></td></tr></table></td>")

            'If bColor = False Then
            '    sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
            '    bColor = True
            '    sb.Append("<td style=""width: 173px;"" valign=""top"">Product Location:" + vbCrLf + "</td>")
            '    sb.Append("<td>" + sAddr + "</td>")
            '    sb.Append("</tr>")
            'Else
            '    sb.Append("<tr valign=""top"">")
            '    bColor = False
            '    sb.Append("<td style=""width: 173px;"" valign=""top"">Product Location:" + vbCrLf + "</td>")
            '    sb.Append("<td>" + sAddr + "</td>")
            '    sb.Append("</tr>")
            'End If

            If Session.Item("customer") Is Nothing Then
                If objProductRegistration.OptIN Then

                    If bColor = False Then
                        sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                        bColor = True
                        'sb.Append("<td colspan=""2""><table><tr><td valign=top><input type=checkbox id=optin " + IIf(objProductRegistration.OptIN, "checked=checked", "") + " ></td>")
                        sb.Append("<td colspan=""2""><table><tr><td valign=top><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" /></td>")
                        'sb.Append("<td><span class=bodycopy>Yes, I would like to receive information from Sony Electronics Inc. about products, services, promotions, contests and offerings that may be of interest to me.</span></td>")
                        sb.Append("<td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">Yes, I would like to receive information from Sony Electronics Inc. about products, services, promotions, contests and offerings that may be of interest to me.</span></td>")
                        sb.Append("</tr></table></td>")
                        sb.Append("</tr>")
                    Else
                        sb.Append("<tr valign=""top"">")
                        bColor = False
                        'sb.Append("<td colspan=""2""><table><tr><td valign=top><input type=checkbox id=optin " + IIf(objProductRegistration.OptIN, "checked=checked", "") + " ></td>")
                        sb.Append("<td colspan=""2""><table><tr><td valign=top><img src=""https://www.servicesplus.sel.sony.com/images/checkbox.jpg"" border=""0"" /></td>")
                        'sb.Append("<td><span class=bodycopy>Yes, I would like to receive information from Sony Electronics Inc. about products, services, promotions, contests and offerings that may be of interest to me.</span></td>")
                        sb.Append("<td><span style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">Yes, I would like to receive information from Sony Electronics Inc. about products, services, promotions, contests and offerings that may be of interest to me.</span></td>")
                        sb.Append("</tr></table></td>")
                        sb.Append("</tr>")
                    End If
                    If bColor = False Then
                        sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                        bColor = True
                        sb.Append("<td colspan=""2"">&nbsp;</td>")
                        sb.Append("</tr>")
                    Else
                        sb.Append("<tr valign=""top"">")
                        bColor = False
                        sb.Append("<td colspan=""2"">&nbsp;</td>")
                        sb.Append("</tr>")
                    End If
                End If
            End If

                If bColor = False Then
                    sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                    bColor = True
                    sb.Append("<td colspan = ""2""  style=""width: 173px;"" valign=""top"">Product List" + vbCrLf + "</td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr valign=""top"">")
                    bColor = False
                    sb.Append("<td colspan = ""2"" style=""width: 173px;"" valign=""top"">Product List" + vbCrLf + "</td>")
                    sb.Append("</tr>")
                End If

                sb.Append("<tr valign=""top"">")
                bColor = False
                sb.Append("<td colspan=""2"">")
                sb.Append("<table cellspacing=""0"" class=""bodycopy"" align=""center"" valign=""middle"" cellpadding=""4"" border=""0"" id=""dgProduct"" style=""color:#333333;width:90%;border-collapse:collapse; font-family: Arial, Helvetica, sans-serif; font-size: 11px; "">")
                sb.Append("<tr class=""tableHeader"" style=""background-color:#D5DEE9;font-weight:bold;"">")
                sb.Append("<td style=""width:25%;"">Certificate #</td><td style=""width:15%;"">Product Code</td><td style=""width:15%;"">Model Number</td><td style=""width:15%;"">Purchase Date</td><td style=""width:25%;"">Serial Number</td>")
                sb.Append("</tr>")

                For Each objProduct As Sony.US.ServicesPLUS.Core.ProductRegistration.Product In objProductRegistration.RegisteredProducts
                    If bgColorProduct Then
                        sb.Append("<tr style=""font-weight:normal;font-style:normal;text-decoration:none;"">")
                        bgColorProduct = False
                    Else
                        sb.Append("<tr style=""background-color:#EFF3FB;font-weight:normal;font-style:normal;text-decoration:none;"">")
                        bgColorProduct = True
                    End If
                    sb.Append("<td style=""width:25%;"">" + objProduct.CertificateNumber + "</td><td style=""width:15%;"">" + objProduct.ProductCode + "</td><td style=""width:15%;"">" + objProduct.ModelNumber + "</td><td style=""width:15%;"">" + objProduct.PurchaseDate + "</td><td style=""width:25%;"">" + objProduct.SerialNumber + "</td>")
                    sb.Append("</tr>")
                Next
                sb.Append("</table>")

                sb.Append("</td>")
                sb.Append("</tr>")


                If bColor = False Then
                    sb.Append("<tr valign=""top"" bgColor=""#d5dee9"">")
                    bColor = True
                    sb.Append("<td colspan=""2"">&nbsp;</td>")
                    sb.Append("</tr>")
                Else
                    sb.Append("<tr valign=""top"">")
                    bColor = False
                    sb.Append("<td colspan=""2"">&nbsp;</td>")
                    sb.Append("</tr>")
                End If

            sb.Append("</table>")
            sb.Append("</tr>")
            sb.Append("<tr>")
            sb.Append("<p style=""font-family: Arial, Helvetica, sans-serif; font-size: 11px; color: 666666;"">&nbsp;&nbsp;In the event your product requires service, please contact Sony at 866-766-9272. At that time you will be instructed on where to send or bring your Sony <br />&nbsp;&nbsp;product for repair.</p>")
            sb.Append("</tr>")
            sb.Append("</table>")

                Return sb.ToString()

        End Function

    End Class
End Namespace

