<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="Message" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.SoftwareFreeDownload"
    CodeFile="SoftwareFreeDownload.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Download Free Software</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                height="40" align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td style="padding: 5px; vertical-align: middle; background: #363d45;">
                                                <ServicePLUSWebApp:Message ID="Message1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.header_DownloadSoftware%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="154">
                                    <table width="710" border="0" role="presentation">
                                        <tbody>
                                            <tr>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td width="162" height="20">
                                                    <asp:Label ID="errorMessageLabel" runat="server" ForeColor="Red" CssClass="tableData"></asp:Label>
                                                </td>
                                                <td width="20" height="20">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20" height="93">
                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                </td>
                                                <td height="93">
                                                    <table width="670" border="0" role="presentation">
                                                        <tr style="text-align: center; vertical-align: middle; background: #d5dee9; padding: 0px 3px 0px 3px;">
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="35" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="130">
                                                                <asp:LinkButton ID="ProductCategorySortLink1" runat="server" CssClass="Body" ToolTip="Sort By Product Category"
                                                                    CommandArgument="ProductCategoryASC"><%=Resources.Resource.el_ProductCategory%></asp:LinkButton>
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="70">
                                                                <asp:LinkButton ID="ModelSortLink" runat="server" CssClass="Body" ToolTip="Sort By Model"><%=Resources.Resource.el_ModelNumber%></asp:LinkButton>
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="145">
                                                                <%=Resources.Resource.el_ModelDescription%>
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="55">
                                                                Version
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="70">
                                                                <%=Resources.Resource.el_PartNumber%>
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="1" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                            <td class="tableHeader" width="193">
                                                                <%=Resources.Resource.el_Description%>
                                                            </td>
                                                            <td width="1" bgcolor="#d5dee9">
                                                                <img height="35" src="images/spacer.gif" width="1" alt="">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="13">
                                                                <asp:Table ID="FreeSoftwareDownloadTable" runat="server" CellSpacing="0"
                                                                    Width="650" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="middle" align="center" height="23">
                                    <table role="presentation">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="btnDownload" runat="server" ToolTip="Download free software now."
                                                    AlternateText="Download free software now." ImageUrl="<%$Resources:Resource,sna_svc_img_4 %>" />
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="btnSearch" runat="server" ToolTip="Return to Software Search"
                                                    AlternateText="Start SoftwarePLUS Search" ImageUrl="<%$Resources:Resource,sna_svc_img_73 %>" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
