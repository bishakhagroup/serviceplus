Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS

Namespace ServicePLUSWebApp

    Partial Class MyProfile
        Inherits SSL

        'Public ChangePassword As String
#Region " Web Form Designer Generated Code "
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            isProtectedPage(True)
            InitializeComponent()
        End Sub
#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

			'Dim globalData As New Core.GlobalData
			'If Not Session.Item("GlobalData") Is Nothing Then
			'    globalData = Session.Item("GlobalData")
			'End If

            'If globalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US Then
            '    ChangePassword = ConfigurationData.Environment.ChangePassword.ChangePasswordURL + "&Lang=" + ConfigurationData.GeneralSettings.GlobalData.Language.US.Substring(0, 2).ToUpper
            'Else
            '    ChangePassword = ConfigurationData.Environment.ChangePassword.ChangePasswordURL + "&Lang=" + ConfigurationData.GeneralSettings.GlobalData.Language.CA.Substring(0, 2).ToUpper
            'End If

			' 2016-06-09 ASleight - Removing Change Password link; Shibboleth IDP doesn't allow external links to this functionality.
			'  Users should change their password from the Login screen.
			'If globalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
			'    If globalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US Then
			'        ChangePassword = ConfigurationData.Environment.ChangePassword.ChangePasswordURL + "&Lang=" + ConfigurationData.GeneralSettings.GlobalData.Language.CA_en.Replace("-", "_")
			'    Else
			'        ChangePassword = ConfigurationData.Environment.ChangePassword.ChangePasswordURL + "&Lang=" + ConfigurationData.GeneralSettings.GlobalData.Language.CA.Replace("-", "_")
			'    End If
			'Else
			'    ChangePassword = ConfigurationData.Environment.ChangePassword.ChangePasswordURL + "&Lang=" + ConfigurationData.GeneralSettings.GlobalData.Language.US.Replace("-", "_")
			'End If

        End Sub

#End Region

    End Class

End Namespace
