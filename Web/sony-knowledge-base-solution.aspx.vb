Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class sony_knowledge_base_solution
        Inherits SSL
        Private recTechBulletin As TechBulletin = New TechBulletin()
        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        Public pagetitle As String = String.Empty
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            isProtectedPage(False)
            InitializeComponent()
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim isValidSolution As Boolean = False   ' ASleight - By assuming a value of False, we don't need 5 "Else" statements repeating the same code.
                Dim solutionID As Integer
                Dim techManager As New TechBulletinManager
                Dim dsSolution As DataSet
                Dim strSolutionId As String = ""

                If HttpContextManager.Customer IsNot Nothing Then Response.Redirect("au-sony-knowledge-base-solution.aspx?filenum=" + Request.QueryString("filenum"))

                If Not String.IsNullOrEmpty(Request.QueryString("filenum")) Then
                    If Integer.TryParse(Convert.ToString(Request.QueryString("filenum")), solutionID) Then
                        strSolutionId = solutionID.ToString()
                        lblSolutionHeader.InnerText = "Knowledge Base ID " + strSolutionId

                        dsSolution = techManager.GetKnowledgeBaseSolution(strSolutionId)
                        If (dsSolution IsNot Nothing) AndAlso (dsSolution.Tables.Count > 0) AndAlso (dsSolution.Tables(0).Rows.Count > 0) Then
                            isValidSolution = True
                            solutiontable.Visible = True

                            lblDatePublishedValue.Text = CType(dsSolution.Tables(0).Rows(0)("DATEPUBLISHED"), Date).ToString("MM/dd/yyyy")
                            lblModelValuse.Text = dsSolution.Tables(0).Rows(0)("MODEL").ToString()
                            lblProblemQuestionValue.Text = dsSolution.Tables(0).Rows(0)("SUBJECTPROBLEM").ToString()
                            lblSolutionAnswerValue.Text = dsSolution.Tables(0).Rows(0)("SOLUTION").ToString()
                            pagetitle = lblProblemQuestionValue.Text
                            ViewState.Add("BulletinNo", strSolutionId)
                            ViewState.Add("Subject", pagetitle)
                        End If
                    End If
                End If

                If isValidSolution Then
                    If Not IsPostBack Then
                        SaveKBView()
                    End If
                Else   ' ASleight - The below code was repeated 5 times in Else statements. Makes more sense like this.
                    solutiontable.Visible = False
                    pagetitle = "Knowledge Base"
                    errorMessageLabel.Text = "No solution for this search."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region
        Private Sub SaveKBView()
            Try
                LogTransaction(0, -1, "")
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try

        End Sub

        Private Sub LogTransaction(ByVal TransactionType As Int16, ByVal FeedBackAnswer As Int16, ByVal Suggestion As String)
            Dim xCUSTOMERID As Integer = "-1"
            Dim xCUSTOMERSEQUENCENUMBER As Integer = "-1"
            If Not Session.Item("customer") Is Nothing Then
                xCUSTOMERID = Convert.ToInt32(CType(Session.Item("customer"), Customer).CustomerID)
                xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(CType(Session.Item("customer"), Customer).SequenceNumber)
            End If
            Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
            Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
            Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
            Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")

            If xHTTP_URL = "" Then
                xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
            End If

            Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

            Try
                Dim intFeedBackID As Int64 = 0
                If Not ViewState("FeedBackID") Is Nothing Then
                    intFeedBackID = Convert.ToInt64(ViewState("FeedBackID").ToString())
                End If
                recTechBulletin.Type = 1
                recTechBulletin.BulletinNo = ViewState("BulletinNo").ToString()
                recTechBulletin.Subject = ViewState("Subject").ToString()
                If Not recTechBulletin Is Nothing Then
                    Dim tbManager As TechBulletinManager = New TechBulletinManager()
                    intFeedBackID = (tbManager.SaveFeedBack(recTechBulletin, FeedBackAnswer, intFeedBackID, TransactionType, Suggestion, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT))
                    ViewState.Add("FeedBackID", intFeedBackID)
                End If
            Catch ex As Exception
                Response.Redirect("Member.aspx")
            End Try

        End Sub

        Protected Sub imgYes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgYes.Click
            Try
                LogTransaction(1, 1, "")
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Protected Sub ImgNo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgNo.Click
            Try
                LogTransaction(1, 0, "")
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
            FeedBackResponse()
        End Sub

        Private Sub FeedBackResponse()
            feedbackTr.Visible = False
            Dim lblFBresponse As New Label()
            lblFBresponse.CssClass = "TableHeader"
            '------------------------------------------------------
            lblFBresponse.Text = "Thank you for your feedback. Do you have any suggestions for improving the above information?"
            btnSubmit.Visible = True
            txtSuggestion.Visible = True
            txtSuggestion.Focus()
            '------------------------------------------------------
            feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub

        Private Sub SuggestionResponse()
            feedbackTr.Visible = False
            Dim lblFBresponse As New Label()
            lblFBresponse.CssClass = "TableHeader"
            '------------------------------------------------------
            lblFBresponse.Text = "Thank you for your suggestions."
            btnSubmit.Visible = False
            txtSuggestion.Visible = False
            '------------------------------------------------------
            feedbackPlaceHolder.Controls.Clear()
            feedbackPlaceHolder.Controls.Add(lblFBresponse)
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            ImgNo.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
            imgYes.Attributes.Add("onclick", "javascript:document.getElementById('feedbackTr').style.visibility='hidden';")
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmit.Click
            Try
                If txtSuggestion.Text <> String.Empty Then
                    LogTransaction(2, 0, txtSuggestion.Text)
                    SuggestionResponse()
                Else
                    Dim lblFBresponse As New Label()
                    lblFBresponse.CssClass = "redAsterick"
                    '------------------------------------------------------
                    lblFBresponse.Text = "Please enter your suggestions to submit."
                    btnSubmit.Visible = True
                    txtSuggestion.Visible = True
                    '------------------------------------------------------
                    feedbackPlaceHolder.Controls.Clear()
                    feedbackPlaceHolder.Controls.Add(lblFBresponse)
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                Response.Redirect("Member.aspx")
            End Try
        End Sub
    End Class

End Namespace
