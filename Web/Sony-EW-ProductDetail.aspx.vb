Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class Sony_EW_ProductDetail
        Inherits SSL
        Public cm As New CustomerManager
        Public customer As Customer
        Public SearchType As String
        Dim PageNum As Integer
        Public ProductCode As String
        Public EWProductCode As String
        Public EWModel As String
        Public SISPARTNUMBER As Double
        Dim ew As ExtendedWarrantyManager
        Dim ewmodels As DataSet
        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ModelDescription As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    ProductCode = Request.QueryString("PC")
                    EWProductCode = Request.QueryString("EW")
                    EWModel = Request.QueryString("mdl")
                    ViewState.Add("querystring", "ewc=" + EWProductCode + "&pc=" + ProductCode + "&mdl=" + EWModel)

                    'Modified by prasad for 2523
                    If ProductCode.Trim() = String.Empty Or EWProductCode.Trim() = String.Empty Then Response.Redirect("sony-extended-warranties.aspx")
                    LoadProductDetails(ProductCode, EWProductCode)
                Catch ex As Exception
                    'ErrorLabel.Text = "Error in displaying Product Details. Please search again for your model. <a href='sony-extended-warranties.aspx'>Click here</a> to go to search page."
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    spnPrice.Visible = False
                    btnOrderEW.Visible = False
                End Try

            End If
        End Sub

        Private Sub LoadProductDetails(ByVal ProductCode As String, ByVal EWProductCode As String)
            Try
                ew = New ExtendedWarrantyManager
                ewmodels = ew.LoadEWProductDetails(ProductCode, EWProductCode)
                If ewmodels.Tables.Count > 0 Then
                    If ewmodels.Tables(0).Rows.Count > 0 Then
                        lblModel.Text = ewmodels.Tables(0).Rows(0)("PARENT_COMPRESSED_MODEL_NAME").ToString()
                        SISPARTNUMBER = ewmodels.Tables(0).Rows(0)("SISPARTNUMBER").ToString()

                        If ewmodels.Tables(0).Rows(0)("PRICE").ToString() = "-1" Or ewmodels.Tables(0).Rows(0)("PRICE").ToString() = "0" Then
                            spnPrice.Visible = False
                            spnCall1800.InnerText = "Call " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail + " for information"
                            spnCall1800.Visible = True
                            btnOrderEW.Visible = False
                        ElseIf (ewmodels.Tables(0).Rows(0)("SISPARTNUMBER").ToString() = "00" Or ewmodels.Tables(0).Rows(0)("SISLISTPRICE").ToString() = "-1") Then
                            'Or SISPARTNUMBER = "00"
                            'Call at 1-800
                            'Show SAP Price
                            lblPrice.Text = "$" + ewmodels.Tables(0).Rows(0)("PRICE").ToString()
                            spnCall1800.InnerText = "Call " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail + " for information"
                            spnCall1800.Visible = True
                            btnOrderEW.Visible = False
                        Else
                            lblPrice.Text = "$" + ewmodels.Tables(0).Rows(0)("SISLISTPRICE").ToString()
                        End If
                        lblShortDescription.Text = ewmodels.Tables(0).Rows(0)("ShortDescription").ToString()
                        lblDesc.Text = "<P><table class=finderCopyDark width=100%><tr width=2%><td></td><td width=98%>" + ewmodels.Tables(0).Rows(0)("LongDescription").ToString() + "</td></tr></table></P>"
                        lblHighlights.Text = ""
                        lblHighlights.Visible = False
                        lblHighlights.Text = "<ul>"
                        For Each Row In ewmodels.Tables(0).Rows
                            lblHighlights.Text = lblHighlights.Text + "<li>" + Row("Highlights") + "</li>"
                        Next Row
                        lblHighlights.Text = lblHighlights.Text + "</ul>"

                    End If
                Else
                    ErrorLabel.Text = "Error showing Extended Warranty item. This Extended Warranty is not available. Please search again for your model. <a href='sony-extended-warranties.aspx'>Click here</a> to go to search page."
                End If
            Catch ex As Exception
                'ErrorLabel.Text = "Error showing Extended Warranty item. This Extended Warranty is not available. Please search again for your model. <a href='sony-extended-warranties.aspx'>Click here</a> to go to search page."
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub mnTab_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles mnTab.MenuItemClick
            If e.Item.Value = 0 Then
                mnTab.Items(0).ImageUrl = "images/Description.gif"
                lblDesc.Visible = True
                mnTab.Items(1).ImageUrl = "images/Highlights.gif"
                lblHighlights.Visible = False
                ErrorLabel.Text = ""
            Else
                mnTab.Items(1).ImageUrl = "images/Highlightslight.gif"
                lblHighlights.Visible = True
                mnTab.Items(0).ImageUrl = "images/DescriptionDark.gif"
                lblDesc.Visible = False
                ErrorLabel.Text = ""
            End If
        End Sub

        Protected Sub btnOrderEW_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOrderEW.Click
            Server.Transfer("Sony-EW-PurchaseInfo.aspx?" + ViewState.Item("querystring").ToString())
        End Sub
    End Class

End Namespace

