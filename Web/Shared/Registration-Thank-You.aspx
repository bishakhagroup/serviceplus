<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Registration_Thank_You"
    CodeFile="Registration-Thank-You.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>
        <%=Resources.Resource.ttl_ThankYou%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="../includes/ServicesPLUS.js" type="text/
			"></script>
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" cellpadding="0">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" cellpadding="0">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <img src="<%=Resources.Resource.svc_rgstr_thnku_img()%>" width="710" height="29" alt="ServicesPlus Registration">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" cellpadding="0">
                                        <tr>
                                            <td width="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                            <td width="670" valign="top">
                                                <table width="670" border="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top">
                                                            <img src="../images/spacer.gif" width="544" height="1" alt="">
                                                            <table>
                                                                <tr>
                                                                    <td height="20" width="670">
                                                                        <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
                                                                        <p class="bodyCopyBold">
                                                                            <%=Resources.Resource.el_rgn_thankContent1%>
                                                                            <sup>
                                                                                <%=Resources.Resource.el_rgn_thankContent2%>
                                                                            </sup>
                                                                            <%=Resources.Resource.el_rgn_thankContent3%>
                                                                            <br />
                                                                            <br />
                                                                            <asp:Label ID="TaxExemptMsgLabel" runat="server" CssClass="body"></asp:Label>
                                                                            <br />
                                                                            <br />
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trPendingCA" runat="server" visible="false">
                                                                    <td height="20" width="670">
                                                                        <p class="bodyCopyBold">
                                                                            <%=Resources.Resource.el_rgn_AccountReqd_CAN%>
                                                                            <br />
                                                                            <br />
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20" width="670">
                                                                        <p class="bodyCopyBold">
                                                                            <%=Resources.Resource.el_rgn_thankContent4%>
                                                                            <br />
                                                                            <br />
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20" width="670">
                                                                        <p class="bodyCopyBold">
                                                                            <br />
                                                                            <%=Resources.Resource.el_rgn_thankContent5%>
                                                                            <br />
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" align="center">
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="<%$Resources:Resource,svc_rgstr_thnku_nxt_img%>"
                                                                AlternateText="Next"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img src="../images/spacer.gif" width="20" height="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
