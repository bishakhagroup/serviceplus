﻿Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class Registration_Thank_You
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        'Dim objUtilties As Utilities = New Utilities
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session.Item("customer") Is Nothing Then
                    Response.Redirect("~/SignIn.aspx?post=rtu", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
                Session.Remove("newSIAMUser")
                Session.Remove("newCustomer")

                Dim customer As New Sony.US.ServicesPLUS.Core.Customer
                If Not HttpContext.Current.Session.Item("customer") Is Nothing Then
                    customer = HttpContext.Current.Session.Item("customer")
                End If

				'Dim objGlobalData As New GlobalData
				'If Not Session.Item("GlobalData") Is Nothing Then
				'    objGlobalData = Session.Item("GlobalData")
				'End If

				If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA And customer.SAPBillToAccounts.Length = 0 Then
					trPendingCA.Visible = True
				Else
					trPendingCA.Visible = False
					If customer.UserType = "P" Then
						TaxExemptMsgLabel.Text = ("<br />" + Resources.Resource.el_rgn_thankContent6)
					Else
						TaxExemptMsgLabel.Text = ("")
					End If
				End If

                'If Session("custusertype") = "P" Then
                '    'TaxExemptMsgLabel.Text = ("If you have submitted a tax exempt status we will verify the data for security reasons and you will be able to get your tax exempt invoice within 5 days.")
                '    TaxExemptMsgLabel.Text = ("<br />" + Resources.Resource.el_rgn_thankContent6)
                '    Session.Remove("custusertype")
                'Else
                '    TaxExemptMsgLabel.Text = ("")
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            If Not Session("RegisterReturnPage") Is Nothing Then
                Dim strSessionValue As String = Session("RegisterReturnPage").ToString()
                Session("RegisterReturnPage") = Nothing
                If Not Session("RegisterReturnPageQS") Is Nothing Then
                    strSessionValue = strSessionValue + Session("RegisterReturnPageQS").ToString()
                    Session("RegisterReturnPageQS") = Nothing
                End If
                Response.Redirect("../" & strSessionValue, True)
            Else
                Response.Redirect("../Member.aspx", False)
            End If
            'Response.Redirect("Member.aspx")
        End Sub

    End Class

End Namespace
