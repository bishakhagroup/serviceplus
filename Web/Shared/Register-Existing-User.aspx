<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Register_Existing_User"
    EnableViewStateMac="true" CodeFile="Register-Existing-User.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.ttl_RegisterExistingUser%> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" rel="stylesheet" href="../includes/ServicesPLUS_style.css">

    <script type="text/javascript" src="../includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="../includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../includes/svcplus_globalization.js"></script>
    <style type="text/css">
        .style1 {
            width: 19px;
        }

        .style2 {
            width: 20px;
        }
    </style>
</head>
<body bgcolor="#5d7180" text="#000000" leftmargin="0" topmargin="0" marginwidth="0"
    marginheight="0">
    <form id="formid" method="post" runat="server">
        <center>
            <table width="760" border="0">
                <tr>
                    <td width="25" background="../images/sp_left_bkgd.gif">
                        <img height="25" src="../images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<%=Resources.Resource.svc_rgstr_thnku_img%>" border="0" alt="Thank You">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0">
                                        <tr>
                                            <td width="17" height="20">
                                                <img height="20" src="../images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678" height="20">
                                                <img height="20" src="../images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="../images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="20" src="../images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678">
                                                <table width="670" border="0">
                                                    <tr bgcolor="#ffffff">
                                                        <td class="style2">
                                                            <img height="1" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td width="127">
                                                            <img height="1" src="../images/spacer.gif" width="125" alt="">
                                                        </td>
                                                        <td width="542">
                                                            <img height="1" src="../images/spacer.gif" width="542" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" class="bodyCopy">
                                                            <span class="tableHeader">Welcome <asp:Label ID="lblwelcome" runat="server" Text="Label" /></span>
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <% If Session("customer") Is Nothing Then%>
                                                    <tr>
                                                        <td height="18" class="style2">
                                                            <img height="15" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" class="bodyCopy">
                                                            <br />
                                                            <b>The ServicesPLUS page you requested requires that you register for the ServicesPLUS
                                                            site.</b>
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <% Else%>
                                                    <tr bgcolor="#d5dee9">
                                                        <td height="18" class="style2">
                                                            <img height="15" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" height="18">
                                                            <span class="tableHeader"><%=Resources.Resource.el_rgn_RgnSubHdl1 %></span><span class="redAsterick">&nbsp;*&nbsp;</span>
                                                            <span class="tableHeader"><%=Resources.Resource.el_rgn_RgnSubHdl2 %></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td width="127">
                                                            <img height="7" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick" Text="<%$ Resources:Resource, el_validate_RequireWarning%>" />
                                                            <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                                            <asp:Label ID="lblPhoneError" runat="server" CssClass="redAsterick" Visible="false" />
                                                            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" CssClass="redAsterick"
                                                                ErrorMessage="</br>Please enter five digit zip code." ControlToValidate="txtZip"
                                                                ForeColor=" " EnableClientScript="False" />
                                                            <asp:Label ID="lblEtxErr" runat="server" CssClass="redAsterick" Visible="false" />
                                                            <asp:Label ID="Label1" runat="server" CssClass="redAsterick" />
                                                            <asp:Label ID="lblFaxError" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblFirstName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_FirstName%>" />&nbsp;
                                                        </td>
                                                        <td>
                                                            <table width="460" border="0">
                                                                <tr>
                                                                    <td class="bodyCopy" style="padding-left: 2px;">
                                                                        &nbsp;<SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="bodyCopy" Enabled="false" />
                                                                    </td>
                                                                    <td class="bodyCopy">
                                                                        <asp:Label ID="lblLastName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_LastName%>" />
                                                                    </td>
                                                                    <td class="bodyCopy">
                                                                        &nbsp;<SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="bodyCopy" Enabled="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCompany" runat="server" Text="<%$ Resources:Resource, el_rgn_CompanyName%>" />&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtCompany" runat="server" CssClass="bodyCopy" MaxLength="50" /><asp:Label
                                                            ID="lblCompanyRequired" runat="server" CssClass="redAsterick" Text="*" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLine1" runat="server" Text="<%$ Resources:Resource, el_rgn_StreetAddress%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="txtLine1" runat="server" CssClass="bodyCopy" MaxLength="50" />
                                                            <asp:Label ID="lblLine1Star" runat="server" CssClass="redAsterick" Text="*" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLine2" runat="server" Text="<%$ Resources:Resource, el_rgn_2ndAddress%>" />&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="txtLine2" runat="server" CssClass="bodyCopy" MaxLength="50" />
                                                            <asp:Label ID="lblLine2Star" runat="server" CssClass="redAsterick" />
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCity" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_City %>"></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="bodyCopy" MaxLength="50" /><asp:Label
                                                                ID="lblCityStar" runat="server" CssClass="redAsterick" Text="*" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblState" runat="server" Text="<%$ Resources:Resource, el_rgn_State%>" />
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                            <asp:DropDownList ID="ddlState" runat="server" Width="130px" />
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblZip" runat="server" Text="<%$ Resources:Resource, el_rgn_ZipCode%>" />&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="bodyCopy" MaxLength="5" /><span
                                                                class="redAsterick">*</span>
                                                            <asp:RegularExpressionValidator ID="zipRegularExpressionValidator" runat="server"
                                                                CssClass="bodyCopy" ErrorMessage="You have entered an invalid zip code." ControlToValidate="txtZip"
                                                                EnableClientScript="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblPhone" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Phone%>"></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            <table width="460" border="0">
                                                                <tr>
                                                                    <td class="bodyCopy" style="padding-left: 2px;">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox><span
                                                                        class="redAsterick">*</span>&nbsp;&nbsp;xxx-xxx-xxxx
                                                                    </td>
                                                                    <%--<td class="bodyCopy">xxx-xxx-xxxx</td>--%>
                                                                    <td class="bodyCopy">
                                                                        <asp:Label ID="lblExt" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Extension%>"></asp:Label>
                                                                    </td>
                                                                    <td class="bodyCopy">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="txtExtension" runat="server" CssClass="bodyCopy" MaxLength="6"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblFax1" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Fax%>" />&nbsp;
                                                        </td>
                                                        <td class="bodyCopy">
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Fax" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox>
                                                            &nbsp;&nbsp;&nbsp;xxx-xxx-xxxx
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <img height="30" src="../images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" style="text-align: center;">
                                                            <asp:ImageButton ID="NextButton" runat="server" ImageUrl="<%$ Resources:Resource,svc_rgstr_thnku_nxt_img%>"
                                                                AlternateText="Next" />
                                                        </td>
                                                    </tr>
                                                    <% End If%>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="../images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="../images/sp_right_bkgd.gif">
                        <img height="20" src="../images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
