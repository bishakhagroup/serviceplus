﻿//'******************************************************************************************************************
//'Module name               : SAP Tokenization
//'Program name              : IFrames.aspx.vb
//'Program Description       : Load the Paymetric control using IFrame
//'Created by                : Venkata Srinivas Dwara
//'Created date              : 07-JUN-2011
//'Change history            :
//'Changed by                : 
//'Changed date              : 
//'Change description        : 
//'*******************************************************************************************************************

using System;
using System.Web;
using ServicesPlusException;

public partial class IFrames : System.Web.UI.Page {
    Utilities objUtilties = new Utilities();

    protected void Page_Load(object sender, EventArgs e) {
        try {
            if (HttpContext.Current.Session["customer"] == null) {
                Response.Redirect("~/SignIn.aspx?post=iframe", false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            PMRespone.Visible = false;
            if (IsPostBack) {
                lblResult.Text = "Checking";
                if (this.CCToken.Value != "") {
                    lblResult.Text = "Checking";
                    PMFrame.Visible = false;
                    PMRespone.Visible = true;
                    String tokenno = this.CCToken.Value;
                    String Expirydate = Session["ExpirationMonth"].ToString() + "/" + Session["ExpirationYear"].ToString();
                    String CardType = Session["CreditCardType"].ToString();
                    String[] str = tokenno.Split('-');
                    lblResult.Text = "<center><Table border=0><TR align =left><TD><b>" + Resources.Resource.cc_frame_msg + "</b></TD></TR><TR><TD></TD></TR><TR><TD></TD></TR></Table>" +
                        "<Table border=0><TR align =left> <TD><b>" + Resources.Resource.cc_frame_msg_1 + "</b></TD><TD>" + CardType + "</TD></TR><TR align =left><TD><b>" +
                        Resources.Resource.cc_frame_msg_2 + "</b></TD><TD> XXXXXXXXXXXX" + str[2].ToString() + "</TD</TR><TR align =left><TD><b>" + Resources.Resource.cc_frame_msg_3 +
                        "</b></TD><TD>" + Expirydate + "</TD></TR></Table><center>";
                } else {
                    RegisterStartupScript("TEST", "<script type=\"text/javascript\" language=\"javascript\">this.window.close();</script>");
                }

            }
        } catch (Exception ex) {
            lblResult.Text = ex.Message;
            ErrorLabel.Text = Utilities.WrapExceptionforUI(ex);
        }
    }
}
