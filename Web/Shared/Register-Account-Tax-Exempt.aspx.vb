﻿Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp


    Partial Class Register_Account_Tax_Exempt
        Inherits SSL
		Dim customer As New Sony.US.ServicesPLUS.Core.Customer
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

		Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
			Dim blVisible As Boolean = True
			Dim rbTax As RadioButtonList

			Try
				If Session.Item("customer") IsNot Nothing Then
					customer = Session.Item("customer")
					LabelSiamID.Text = GenerateSiamIDImageString(customer.SIAMIdentity.ToString())
				End If

				'Kayal - No need to check customer session values
				pnlCCTaxExemptDetails.Visible = False
				divWarningMsg.Visible = False 'Warning message for CAN customer if they dont have sony account

				If rbSonyAccount.SelectedValue = "1" Then
					tblCart.Rows.Add(populateAccountNumberData(buildAccountNumberTextBox()))
					AddMoreAccountTextBoxs()
				ElseIf rbSonyAccount.SelectedValue = "0" Then
					If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
						blVisible = False
					End If
					divWarningMsg.Visible = Not blVisible  ' Hide the tax message and show warning msg for CAN customer 
					If blVisible Then
						tblCart.Rows.Add(populateTaxData(buildTaxExempt()))
						If Page.FindControl("rbTaxExempt") IsNot Nothing Then
							rbTax = (Page.FindControl("rbTaxExempt"))
							If rbTax IsNot Nothing Then
								If rbTax.SelectedValue = 1 Then
									If Session.Item("customer") IsNot Nothing Then
										divCustomerID.Visible = True
									Else
										divCustomerID.Visible = False
									End If
									pnlCCTaxExemptDetails.Visible = True
								End If
							End If
						End If
					End If
				End If

				If Session("Language_Changed") IsNot Nothing AndAlso Session("Language_Changed") = "1" Then
					fnSetSession()
				End If

			Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub AddMore2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddMore2.Click
            controlMethod(sender, e)
        End Sub

        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim caller As String = String.Empty

                If Not sender.ID Is Nothing Then caller = sender.ID.ToString()
                Select Case caller
                    Case Page.ID.ToString()
                        DisplaySonyAccount()
                    Case ImageButton1.ID.ToString()
                        processRegistrationForm()
                    Case AddMore2.ID.ToString()
                        AddMoreAccountTextBoxs()
                        AddMore2.CommandArgument = "ShowAll"
                    Case rbSonyAccount.ID.ToString()
                        populateAccount_TaxTable()
                        If rbSonyAccount.SelectedValue = 1 Then
                            AddMoreAccountTextBoxs()
                        End If
                        AddMore2.CommandArgument = ""
                End Select
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Sub rbSonyAccount_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSonyAccount.SelectedIndexChanged
            ErrorSonyACC.Text = ""
            LabelSonyAct.CssClass = "bodyCopy"
            controlMethod(sender, e)
        End Sub

        Private Sub DisplaySonyAccount()
            If Not Session("PunchOutRequestData") Is Nothing Then
                trCBSAccount.Visible = True
                trSonyAccount.Visible = False
                tblCart.Rows.Clear()
                tblCart.Rows.Add(buildAccountNumberTextBox())
                AddMoreAccountTextBoxs()
            End If
        End Sub

        Private Sub processRegistrationForm()
            Dim custmgr As CustomerManager = New CustomerManager()
            Dim isVerified As Boolean = True
            Dim addedAccount As Boolean = False

            Try
                If Session("newCustomer") IsNot Nothing Then
                    customer = Session.Item("newCustomer")
                Else
                    customer = New Sony.US.ServicesPLUS.Core.Customer
                End If

                If Session("PunchOutRequestData") IsNot Nothing Then
                    customer.StatusCode = 3
                    ' customer.UserType = ???
                Else
                    customer.StatusCode = 1
                    If rbSonyAccount.SelectedIndex = 0 Then
                        customer.UserType = "P"
                    ElseIf rbSonyAccount.SelectedIndex = 1 Then
                        ' customer.UserType = ???
                    Else
                        ' In case they pick the third of two possible responses for "Do you have an open Sony account?", we tell them their account number is invalid?
                        Throw New Exception(Resources.Resource.el_rgn_Err_AccntValidate)
                    End If
                End If

                If (isVerified) Then
                    If Request.Form("rbTaxExempt") = "1" Then
                        Session.Add("isTaxExempt", True)
                        customer.Tax_Exempt = True
                    Else
                        If Session.Item("isTaxExempt") IsNot Nothing Then Session.Remove("isTaxExempt")
                        customer.Tax_Exempt = False
                    End If

                    If ((Session("PunchOutRequestData") IsNot Nothing) Or (rbSonyAccount.SelectedIndex = 0)) Then
                        Dim tbArray() As String = {(Request.Form("tb1")), (Request.Form("tb2")), (Request.Form("tb3")), (Request.Form("tb4")), (Request.Form("tb5")), (Request.Form("tb6")), (Request.Form("tb7")), (Request.Form("tb8")), (Request.Form("tb9")), (Request.Form("tb10")), (Request.Form("tb11")), (Request.Form("tb12"))}
                        For i As Int32 = 0 To 11
                            If Not tbArray(i) = "" Then
                                customer.AddSAPBillToAccount(tbArray(i))
                                addedAccount = True
                            End If
                        Next
                    End If

                    If Session("newCustomer") IsNot Nothing Then
                        Dim cm As New CustomerManager
                        cm.UpdateCustomerSubscription(customer)
                        cm.AddCustomerAccount(customer)
                        Session.Add("customer", customer)
                        Session.Add("siamID", customer.SIAMIdentity)
                    Else
                        Session.Add("newCustomer", customer)
                    End If
                Else
                    If Session("PunchOutRequestData") IsNot Nothing Then
                        trCBSAccount.Visible = True
                        trSonyAccount.Visible = False
                        tblCart.Rows.Clear()
                        tblCart.Rows.Add(buildAccountNumberTextBox())
                        AddMoreAccountTextBoxs()
                    Else
                        If rbSonyAccount.SelectedValue = "1" Then
                            tblCart.Rows.Add(populateAccountNumberData(buildAccountNumberTextBox()))
                            AddMoreAccountTextBoxs()
                        ElseIf rbSonyAccount.SelectedValue = "0" Then
                            tblCart.Rows.Add(populateTaxData(buildTaxExempt()))
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub populateAccount_TaxTable()
            tblCart.Rows.Clear()
            If rbSonyAccount.SelectedValue = 1 Then
                tblCart.Rows.Add(buildAccountNumberTextBox())
                pnlCCTaxExemptDetails.Visible = False
            Else
                Dim blVisible As Boolean = True
                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    blVisible = False
                End If
                divWarningMsg.Visible = Not blVisible  ' Hide the tax message and show warning msg for CAN customer 
                If blVisible Then
                    tblCart.Rows.Add(buildTaxExempt())
                End If
            End If
        End Sub

        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String
            Dim strImg As String = "<img src='images/ImgName.JPG' style='border:0;' alt='' />"
            Dim strValue As String = ""

            For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                strValue += strImg.Replace("ImgName", strChar.ToString())
            Next
            Return strValue
        End Function

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            Dim cm As New CustomerManager
            Try
                If IsAccountNosLengthOk() Then
                    controlMethod(sender, e)
                    If (Not Request.QueryString("type") Is Nothing) Then
                        Response.Redirect("Register-Notifications.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Response.Redirect("~/Registration.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If

                If Session.Item("customer") IsNot Nothing Then
                    customer = Session.Item("customer")
                    If IsAccountNosLengthOk() Then
                        controlMethod(sender, e)
                        If (Request.QueryString("type") IsNot Nothing) Then
                            Response.Redirect("Register-Notifications.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("~/Registration.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function IsAccountNosLengthOk() As Boolean
            Dim tbArray() As String = {(Request.Form("tb1")), (Request.Form("tb2")), (Request.Form("tb3")), (Request.Form("tb4")), (Request.Form("tb5")), (Request.Form("tb6")), (Request.Form("tb7")), (Request.Form("tb8")), (Request.Form("tb9")), (Request.Form("tb10")), (Request.Form("tb11")), (Request.Form("tb12"))}
			Dim altxtTemp As New ArrayList

			lblAccountNoErrors.Text = ""
			lblAccountNoErrors.Visible = False

			For i As Int32 = 0 To 11
				If Not tbArray(i) = "" Then
					If tbArray(i).Length() > 10 Then
						lblAccountNoErrors.Text = Resources.Resource.el_rgn_Err_AccntNoValidate	' "All Sony open account numbers should be 10 characters or less."
						lblAccountNoErrors.Visible = True
						Return False
					End If
					For j As Int32 = 11 To (i + 1) Step -1
						If tbArray(j) <> "" Then
							If tbArray(i) = tbArray(j) Then
								lblAccountNoErrors.Text = Resources.Resource.el_rgn_Err_AcctDuplicate ' "Duplicate account numbers."
								lblAccountNoErrors.Visible = True
								Return False
							End If
						End If
					Next
				End If
			Next
			Return True
        End Function
        Private Sub AddMoreAccountTextBoxs()
            tblCart.Rows.Clear()
            Dim TR As New TableRow
            Dim TD As New TableCell

			Dim tb As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb.ID = "tb1"
            tb.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb.CssClass = "bodyCopy"
            tb.MaxLength = 10
            tb.Text = (Request.Form("tb1"))

            Dim tb2 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb2.ID = "tb2"
            tb2.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb2.CssClass = "bodyCopy"
            tb2.MaxLength = 10
            tb2.Text = (Request.Form("tb2"))

            Dim tb3 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb3.ID = "tb3"
            tb3.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb3.CssClass = "bodyCopy"
            tb3.MaxLength = 10
            tb3.Text = (Request.Form("tb3"))

            Dim tb4 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb4.ID = "tb4"
            tb4.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb4.CssClass = "bodyCopy"
            tb4.MaxLength = 10
            tb4.Text = (Request.Form("tb4"))

            Dim tb5 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb5.ID = "tb5"
            tb5.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb5.CssClass = "bodyCopy"
            tb5.MaxLength = 10
            tb5.Text = (Request.Form("tb5"))

            Dim tb6 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb6.ID = "tb6"
            tb6.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb6.CssClass = "bodyCopy"
            tb6.MaxLength = 10
            tb6.Text = (Request.Form("tb6"))

            Dim tb7 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb7.ID = "tb7"
            tb7.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb7.CssClass = "bodyCopy"
            tb7.MaxLength = 10
            tb7.Text = (Request.Form("tb7"))

            Dim tb8 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb8.ID = "tb8"
            tb8.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb8.CssClass = "bodyCopy"
            tb8.MaxLength = 10
            tb8.Text = (Request.Form("tb8"))

            Dim tb9 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb9.ID = "tb9"
            tb9.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb9.CssClass = "bodyCopy"
            tb9.MaxLength = 10
            tb9.Text = (Request.Form("tb9"))

            Dim tb10 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb10.ID = "tb10"
            tb10.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb10.CssClass = "bodyCopy"
            tb10.MaxLength = 10
            tb10.Text = (Request.Form("tb10"))

            Dim tb11 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb11.ID = "tb11"
            tb11.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb11.CssClass = "bodyCopy"
            tb11.MaxLength = 10
            tb11.Text = (Request.Form("tb11"))

            Dim tb12 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb12.ID = "tb12"
            tb12.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb12.CssClass = "bodyCopy"
            tb12.MaxLength = 10
            tb12.Text = (Request.Form("tb12"))

            AddMore2.Visible = False
            tb7.Visible = True
            tb8.Visible = True
            tb9.Visible = True
            tb10.Visible = True
            tb11.Visible = True
            tb12.Visible = True
            'Commeted by Prasad for 2738

            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">Enter your Sony open account number(s) below. If you don't know your account number(s), you can leave the boxes blank.</td></tr>"))
            TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">" + Resources.Resource.el_rgn_AcctHdl + "</td></tr>"))

            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">If you have a Sony Account Number(s), please enter it in a text box below. Each textbox is designed to hold a unique account number. You can add up to 12 Sony Account numbers on this page. Sony Account numbers are a 7 digit number, if you don't know your number you can leave it blank.</td></tr>"))
            TD.Controls.Add(New LiteralControl("<tr><td width=""10""><img src=""images/spacer.gif"" width=""10"" alt=""""></td></tr>"))
            'TD.Controls.Add(New LiteralControl("<tr><td width=""20""><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">Your Sony Account(s): &nbsp;</td></tr>"))

            TD.Controls.Add(New LiteralControl("<tr><td width=""10""><img src=""images/spacer.gif"" width=""10"" alt=""""></td><td colspan=""2"">"))
            TD.Controls.Add(tb)
            TD.Controls.Add(tb2)
            TD.Controls.Add(tb3)
            TD.Controls.Add(tb4)
            TD.Controls.Add(tb5)
            TD.Controls.Add(tb6)
            TD.Controls.Add(New LiteralControl("</td></tr>"))
            TD.Controls.Add(New LiteralControl("<tr><td width=""20""><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td colspan=""2"">"))

            TD.Controls.Add(tb7)
            TD.Controls.Add(tb8)
            TD.Controls.Add(tb9)
            TD.Controls.Add(tb10)
            TD.Controls.Add(tb11)
            TD.Controls.Add(tb12)
            TD.Controls.Add(New LiteralControl("</td></tr>"))

            TR.Cells.Add(TD)
            tblCart.Rows.Add(TR)
            focusFormField = tb.ID.ToString
        End Sub

        Private Function buildTaxExempt() As TableRow
            Dim TR As New TableRow
            Dim TD As New TableCell
            Dim rb As New RadioButtonList

            rb.ID = "rbTaxExempt"
            rb.AutoPostBack = True
            rb.CssClass = "bodyCopy"
            rb.SelectedValue = "Yes"
            rb.RepeatDirection = RepeatDirection.Horizontal
            rb.Items.Add(New ListItem("Yes", "1"))
            rb.Items.Add(New ListItem("No", "0"))
            rb.SelectedIndex = 1

            AddMore2.Visible = False
            Dim strMsg As String = Resources.Resource.el_rgn_TaxExempt
            TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">" + strMsg))
            TD.Controls.Add(rb)
            TD.Controls.Add(New LiteralControl("</td></tr>"))
            focusFormField = rbSonyAccount.ID.ToString

            TR.Cells.Add(TD)

            Return TR
        End Function


        Private Function populateTaxData(ByRef thisTR As TableRow) As TableRow
            tblCart.Rows.Clear()

            If Not Request.Form("rbTaxExempt") Is Nothing Then
                If Request.Form("rbTaxExempt") = "0" Then
                    CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).SelectedIndex = 1
                    'CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).Items(0).Selected = True
                ElseIf Request.Form("rbTaxExempt") = "1" Then
                    CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).SelectedIndex = 0
                    'CType(thisTR.Cells(0).Controls.Item(1), RadioButtonList).Items(1).Selected = True
                End If
            End If

            Return thisTR
        End Function

        Private Function buildAccountNumberTextBox() As TableRow
            Dim TR As New TableRow
            Dim TD As New TableCell

            Dim tb As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb.ID = "tb1"
            tb.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb.CssClass = "bodyCopy"
            tb.MaxLength = 15

            Dim tb2 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb2.ID = "tb2"
            tb2.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb2.CssClass = "bodyCopy"
            tb2.MaxLength = 15

            Dim tb3 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb3.ID = "tb3"
            tb3.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb3.CssClass = "bodyCopy"
            tb3.MaxLength = 15

            Dim tb4 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb4.ID = "tb4"
            tb4.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb4.CssClass = "bodyCopy"
            tb4.MaxLength = 15


            Dim tb5 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb5.ID = "tb5"
            tb5.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb5.CssClass = "bodyCopy"
            tb5.MaxLength = 15

            Dim tb6 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb6.ID = "tb6"
            tb6.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb6.CssClass = "bodyCopy"
            tb6.MaxLength = 15

            Dim tb7 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb7.ID = "tb7"
            tb7.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb7.CssClass = "bodyCopy"
            tb7.Visible = False
            tb7.MaxLength = 15

            Dim tb8 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb8.ID = "tb8"
            tb8.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb8.CssClass = "bodyCopy"
            tb8.Visible = False
            tb8.MaxLength = 15

            Dim tb9 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb9.ID = "tb9"
            tb9.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb9.CssClass = "bodyCopy"
            tb9.Visible = False
            tb9.MaxLength = 15

            Dim tb10 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb10.ID = "tb10"
            tb10.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb10.CssClass = "bodyCopy"
            tb10.Visible = False
            tb10.MaxLength = 15


            Dim tb11 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb11.ID = "tb11"
            tb11.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb11.CssClass = "bodyCopy"
            tb11.Visible = False
            tb11.MaxLength = 15

            Dim tb12 As New Sony.US.ServicesPLUS.Controls.SPSTextBox
            tb12.ID = "tb12"
            tb12.Width = System.Web.UI.WebControls.Unit.Pixel(90)
            tb12.CssClass = "bodyCopy"
            tb12.Visible = False
            tb12.MaxLength = 15

            AddMore2.Visible = False

            'If AddMore2.CommandArgument.Length = 0 Then
            '    AddMore2.Visible = True
            'Else
            AddMore2.Visible = False
            'End If

            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">If you have a Sony Account Number(s), please enter it in a text box below. Each text box is designed to hold a unique 7 digit account number. You can add up to 12 Sony Account numbers on this page. Sony Account numbers are a 7 digit number, if you don't know your number you can leave it blank.</td></tr>"))
            'TD.Controls.Add(New LiteralControl("<tr><td><img src=""images/spacer.gif"" width=""20"" alt=""""></td><td class=""bodyCopy"" align=""left"" colspan=""2"">Your Sony Account(s): &nbsp;"))
            TD.Controls.Add(tb)
            TD.Controls.Add(tb2)
            TD.Controls.Add(tb3)
            TD.Controls.Add(tb4)
            TD.Controls.Add(tb5)
            TD.Controls.Add(tb6)
            TD.Controls.Add(New LiteralControl("<br/>"))
            TD.Controls.Add(tb7)
            TD.Controls.Add(tb8)
            TD.Controls.Add(tb9)
            TD.Controls.Add(tb10)
            TD.Controls.Add(tb11)
            TD.Controls.Add(tb12)
            TD.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
            TD.Controls.Add(New LiteralControl("</td></tr>"))

            focusFormField = tb.ID.ToString
            TR.Cells.Add(TD)

            Return TR
        End Function


        Private Function populateAccountNumberData(ByRef thisTR As TableRow) As TableRow
			Dim thisControl As Control
            Dim showAll As Boolean = False

            If AddMore2.CommandArgument = "ShowAll" Then showAll = True

            For Each thisControl In thisTR.Cells(0).Controls
                If thisControl.GetType.Name = "SPSTextBox" Then
                    If Not Request.Form(thisControl.ID.ToString()) Is Nothing Then
                        CType(thisControl, Sony.US.ServicesPLUS.Controls.SPSTextBox).Text = Request.Form(thisControl.ID)
                    End If

                    If showAll Then CType(thisControl, Sony.US.ServicesPLUS.Controls.SPSTextBox).Visible = True

                End If
            Next

            Return thisTR
        End Function

		Private Sub ShowDynamicData()
			'Added by Prasad for 2738
			If Not Session("PunchOutRequestData") Is Nothing Then
				trCBSAccount.Visible = True
				trSonyAccount.Visible = False
				tblCart.Rows.Clear()
				tblCart.Rows.Add(buildAccountNumberTextBox())
				AddMoreAccountTextBoxs()
			Else
				If rbSonyAccount.SelectedValue = "1" Then
					tblCart.Rows.Add(populateAccountNumberData(buildAccountNumberTextBox()))
					AddMoreAccountTextBoxs()
				ElseIf rbSonyAccount.SelectedValue = "0" Then
					tblCart.Rows.Add(populateTaxData(buildTaxExempt()))
				End If
			End If
		End Sub

#Region " get and Set Session values to control for language change"
		Public Sub fnGetSession()
			'Session.Add("CultureData", ObjCst)
		End Sub

		Public Sub fnSetSession()
			If Not Session("CultureData") Is Nothing Then

				Session.Remove("CultureData")
				Session("Language_Changed") = "0"
			End If
		End Sub

#End Region


        Public focusFormField As String
        Protected WithEvents TaxExempt As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents isTaxExempt As System.Web.UI.WebControls.CheckBox
        Protected WithEvents chkBoxTaxExempt As System.Web.UI.WebControls.CheckBox
        Protected WithEvents AddMore As System.Web.UI.WebControls.Button
    End Class

End Namespace
