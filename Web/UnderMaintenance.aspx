<%@ Page Language="vb" AutoEventWireup="false" CodeFile="UnderMaintenance.aspx.vb" Inherits="ServicePLUSWebApp.UnderMaintenance" EnableViewStateMac="true" %>

<%@ Register TagPrefix="ServicePLUSMaintanence" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Professional Services Contact Information</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>

<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" background="images/sp_left_bkgd.gif">
                        <img height="25" src="images/spacer.gif" width="25"></td>
                    <td width="720" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSMaintanence:TopSonyHeader ID="TopSonyHeader" ShowDefaultIcon="false" runat="server"></ServicePLUSMaintanence:TopSonyHeader>
                                </td>
                            </tr>

                            <tr>
                                <td background="images/sp_navbar_bkgd.gif" colspan="5">
                                    <img src="images/spacer.gif" width="10" height="10" alt=""></td>
                            </tr>

                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Under Maintenance</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>

                            <tr>
                                <td valign="top">
                                    <div id="tableContact" runat="server"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="20" src="images/spacer.gif" width="25" alt=""></td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
