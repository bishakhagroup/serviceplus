Imports System.Net
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class logout
        Inherits Page

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Dim ex As Exception = Nothing
            Dim logout As String = String.Empty
            Dim requestURL As String = String.Empty

            ' Set the Logout URL based on CA/US
            requestURL = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_HOST")
            'If requestURL.Contains("sony.ca") Then
            If HttpContextManager.GlobalData.IsCanada Then
                If HttpContextManager.GlobalData.IsFrench Then
                    logout = ConfigurationData.Environment.IdentityURLs.LogoutFRCA
                Else
                    logout = ConfigurationData.Environment.IdentityURLs.LogoutENCA
                End If
            Else
                logout = ConfigurationData.Environment.IdentityURLs.LogoutUS
            End If

            If Session("HOMEPAGE") IsNot Nothing Then ex = Session("HOMEPAGE")

            Session.Clear()
            Session.RemoveAll()
            Session.Abandon()
            If (ex IsNot Nothing) Then Session("HOMEPAGE") = ex ' Re-add the Exception details after clearing the Session variables.

            ' Force all of the Cookies to expire.
            For Each cookie In Response.Cookies
                Response.Cookies(cookie.ToString()).Expires = DateAdd("d", -1, Now())
            Next

            If ConfigurationData.GeneralSettings.Environment = "Development" Then
                Response.Redirect("default.aspx", True)
            End If

            Response.Redirect(logout, False)
        End Sub
    End Class
End Namespace
