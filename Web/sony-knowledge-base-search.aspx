<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_knowledge_base_search" CodeFile="sony-knowledge-base-search.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.knowldge_base_hdr_msg()%></title>
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Technical Bulletins"
        name="keywords">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />

    <link href="includes/CalendarControl.css" rel="stylesheet" type="text/css" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/CalendarControl.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        function showModalPopUp(cnt, models) {
            var lstModels = new Array(models.split(','));
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");
            $("#divMessage").dialog({ // dialog box
                title: '<span class="modalpopup-title">Model Renamed</span>',
                height: 285,
                width: 560,
                modal: true,
                position: 'top',
                close: function () {
                    redirectTo();
                }
            });
            var content = "<span class='modalpopup-content'>Your Search criteria for Model matches models which have been renamed." + "<br/>" +
                "Knowledge Base articles may refer to either old model names or new model names." + "<br/>" +
                "Your search results will include any records which match either a new model name or an" + "<br/>" +
                "old model name listed below</span>"
            content += "<br/>" + "<table class='modalpopup-content' style='margin-left:100px;'>"
            content += '<tr><td width="100px"><u>Old Model Name</u></td><td width="100px"><u>New Model Name</u></td></tr>';
            for (i = 0; i < cnt; i++) {
                var lstmodel = new Array(lstModels[0][i].split(':'));
                content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1] + '</td></tr>';
            }
            content += "</table>"
            $("#divMessage").append(content); //message to display in divmessage div
            $("#divMessage").append('<br/> <br/><a id="btnClose"><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
            hideControls();
            return false;
        }

        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }

        function redirectTo() {
            showControls();
        }

        function showControls() {
            $("#dgTechBulletinResults").show();
            $("#lblCurrentPage").show();
            $("#lblEndingPage").show();
            $("#lblCurrentPage2").show();
            $("#lblEndingPage2").show();
            $("#btnGoToPage").show();
            $("#btnGoToPage2").show();
            $("#btnNextPage").show();
            $("#btnNextPage2").show();
            $("#btnPreviousPage").show();
            $("#btnPreviousPage2").show();
            $("#txtPageNumber").show();
            $("#txtPageNumber2").show();
            $("#PreviousLinkButton").show();
            $("#PreviousLinkButton2").show();
            $("#NextLinkButton").show();
            $("#NextLinkButton2").show();
            $("#lblof1").show();
            $("#lblof2").show();
            $("#lblPage1").show();
            $("#lblPage2").show();
            $("#trBackToTop").show();
            $("#errorMessageLabel").show();
        }
        function hideControls() {
            $("#dgTechBulletinResults").hide();
            $("#lblCurrentPage").hide();
            $("#lblEndingPage").hide();
            $("#lblCurrentPage2").hide();
            $("#lblEndingPage2").hide();
            $("#btnGoToPage").hide();
            $("#btnGoToPage2").hide();
            $("#btnNextPage").hide();
            $("#btnNextPage2").hide();
            $("#btnPreviousPage").hide();
            $("#btnPreviousPage2").hide();
            $("#txtPageNumber").hide();
            $("#txtPageNumber2").hide();
            $("#PreviousLinkButton").hide();
            $("#PreviousLinkButton2").hide();
            $("#NextLinkButton").hide();
            $("#NextLinkButton2").hide();
            $("#lblof1").hide();
            $("#lblof2").hide();
            $("#lblPage1").hide();
            $("#lblPage2").hide();
            $("#trBackToTop").hide();
            $("#errorMessageLabel").hide();
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="frmTechBulletinSearch" method="post" runat="server">
            <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto; font-weight: bold;"></div>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                            <table width="710" border="0" role="presentation">
                                <tr>
                                    <td>
                                        <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nav role="navigation"><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td valign="top" align="right" width="464" background="images/Knowledge_Base_banner.jpg"
                                                    bgcolor="#363d45" height="82">
                                                    <br/>
                                                    <h1 class="headerText"><%=Resources.Resource.menu_knowldgebase%></h1>&nbsp;&nbsp;
                                                </td>
                                                <td valign="middle" bgcolor="#363d45">
                                                    <ServicePLUSWebApp:PersonalMessage ID="PersonalMessage2" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="hiddenKB" runat="server">
                                            <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td width="466" bgcolor="#f2f5f8">
                                                        <table width="464" border="0" role="presentation">
                                                            <tr>
                                                                <td width="20">
                                                                </td>
                                                                <td>
                                                                    <span class="bodycopy"><span class="bodyCopySM"><%=Resources.Resource.contact_canada_support %><a href="mailto:<%=Resources.Resource.contact_canada_support_email%>"><%=Resources.Resource.contact_canada_support_email%></a></span></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td bgcolor="#99a8b5" valign="bottom">
                                                    </td>
                                                </tr>
                                                <tr style="height: 9px;">
                                                    <td width="466" bgcolor="#f2f5f8">
                                                        <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                    </td>
                                                    <td bgcolor="#99a8b5">
                                                        <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </div>
                                    </td>
                                </tr>
                                <div id="DefaultKB" runat="server">
                                    <tr>
                                        <td>
                                            <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td width="466" bgcolor="#f2f5f8">
                                                        <table width="464" border="0" role="presentation">
                                                            <tr>
                                                                <td width="20">
                                                                </td>
                                                                <td>
                                                                    <h2 class="headerTitle">Search Knowledge Base</h2><br />
                                                                    <img height="10" src="images/spacer.gif" width="20" alt="">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20">
                                                                </td>
                                                                <td>
                                                                    <span class="bodycopy">Search the Knowledge Base for information about Sony Professional Products.<br />
                                                                        <br />
                                                                        Access to technical bulletins is available to subscribers with an <a href="http://www.sony.com/bbsc-tb" target="_blank"
                                                                            aria-label="Leave this site and go to Sony's Technical Bulletin site.">Internet-only technical bulletin access</a>
                                                                        subscription. Subscribers can search for technical bulletins by logging into this site.<br />
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20">
                                                                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                                </td>
                                                                <td valign="top">
                                                                    <br />
                                                                    <table width="100%" border="0" role="presentation">
                                                                        <tr style="height: 18px;">
                                                                            <td style="width: 155px">
                                                                                <label class="tableData" for="txtModelNo">Model Number:</label>
                                                                            </td>
                                                                            <td style="width: 153px">
                                                                                <label class="tableData" for="txtSerialNo">Serial Number:</label>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr style="height: 22px;">
                                                                            <td>
                                                                                <SPS:SPSTextBox ID="txtModelNo" CssClass="tableData" runat="server" MaxLength="100" />
                                                                            </td>
                                                                            <td>
                                                                                <SPS:SPSTextBox ID="txtSerialNo" CssClass="tableData" runat="server" MaxLength="12" />
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr style="height: 18px;">
                                                                            <td colspan="3">
                                                                                <label class="tableData" for="txtSubject">Keywords:</label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="height: 22px;">
                                                                            <td colspan="3">
                                                                                <SPS:SPSTextBox ID="txtSubject" CssClass="tableData" runat="server" Width="284px" MaxLength="300" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="height: 5px;">
                                                                            <td colspan="3">
                                                                                <img height="5" src="images/spacer.gif" width="20" alt="">
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="height: 18px;">
                                                                            <td>
                                                                                <label class="tableData" for="txtBulletinNo">KB ID Number:</label>
                                                                            </td>
                                                                            <td colspan="2">&nbsp;</td>
                                                                        </tr>
                                                                        <tr style="height: 22px;">
                                                                            <td>
                                                                                <SPS:SPSTextBox ID="txtBulletinNo" CssClass="tableData" runat="server" MaxLength="11" />
                                                                            </td>
                                                                            <td colspan="2">&nbsp;</td>
                                                                        </tr>
                                                                        <tr style="height: 5px;">
                                                                            <td colspan="3">
                                                                                <img height="5" src="images/spacer.gif" width="20" alt="">
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="height: 18px;">
                                                                            <td>
                                                                                <label class="tableData" for="txtDateFrom">Date From:</label>
                                                                            </td>
                                                                            <td>
                                                                                <label class="tableData" for="txtDateTo">Date To:</label>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr style="height: 18px;">
                                                                            <td style="width: 155px">
                                                                                <input id="txtDateFrom" type="date" class="tableData" runat="server" required />
                                                                                <%--<asp:Calendar ID="calDateFrom" CssClass="tableData" SelectionMode="Day" runat="server" />--%>
                                                                                <%--<SPS:SPSTextBox ID="txtDateFrom" CssClass="tableData" runat="server" MaxLength="10" TextMode="Date" />--%>
                                                                                <%--<a href="#" onclick="showCalendarControl(txtDateFrom)">
                                                                                    <img alt="Click to select a date" src="images/cal.gif" border="0"></a>--%>
                                                                            </td>
                                                                            <td style="width: 153px; vertical-align: top;">
                                                                                <input id="txtDateTo" type="date" class="tableData" runat="server" required />
                                                                                <%--<asp:Calendar ID="calDateTo" CssClass="tableData" SelectionMode="Day" runat="server" />--%>
                                                                                <%--<SPS:SPSTextBox ID="txtDateTo" CssClass="tableData" runat="server" MaxLength="10" TextMode="Date" />--%>
                                                                                <%--<a onclick="showCalendarControl(txtDateTo)" href="#">
                                                                                    <img alt="Click to select a date" src="images/cal.gif" border="0"></a>--%>
                                                                            </td>
                                                                            <td align="center">
                                                                                <%--<asp:Label ID="dtFormat" CssClass="tableData" runat="server" Width="125px">Date Format: mm/dd/yyyy</asp:Label>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td bgcolor="#99a8b5" valign="bottom">
                                                        <table width="100%" border="0" role="presentation">
                                                            <tr align="center">
                                                                <td style="height: 32px">
                                                                    <asp:ImageButton ID="btnSearch" ImageUrl="images/sp_int_KBSearchHdr_btn.gif" runat="server"
                                                                        AlternateText="Search Knowledge Base index" />
                                                                    <%--&nbsp;<asp:ImageButton ID="lnkWhatsNew" ImageUrl="images/sp_int_WhatsNewHdr_btn.GIF" runat="server"
                                                                        AlternateText="What's New: Search Knowledge Base for all articles published in the last month." />--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="height: 9px;">
                                                    <td width="466" bgcolor="#f2f5f8">
                                                        <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                    </td>
                                                    <td bgcolor="#99a8b5">
                                                        <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                    </td>
                                                </tr>
                                            </table>
                                            <br/>
                                            <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="errorMessageLabel" runat="server" CssClass="redAsterick" EnableViewState="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="710" border="0" role="presentation">
                                                <tr>
                                                    <td width="20" height="5">
                                                        <img height="5" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                    <td width="670" height="5">
                                                        <img height="5" src="images/spacer.gif" width="670" alt="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                    <td valign="top" width="670">
                                                        <table id="tblPagination1" style="border: none;" role="navigation" aria-label="Pagination navigation" runat="server" caption="Search results pagination">
                                                            <tr style="vertical-align: bottom;">
                                                                <td style="width: 65px;">
                                                                    <a href="#" id="lnkPreviousPage1" onclick="FetchPreviousResultSet()" class="tableData" runat="server"
                                                                        aria-label="Go to previous page of search results"><img src="images/sp_int_leftArrow_btn.gif" alt="" /> Previous</a>
                                                                    <%--<asp:ImageButton ID="btnPreviousPage" runat="server" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                        Height="9" Width="9" ToolTip="Previous Result Page"></asp:ImageButton><asp:LinkButton
                                                                            ID="PreviousLinkButton" runat="server" CssClass="tableData" ToolTip="Previous Result Page">Previous</asp:LinkButton>--%>
                                                                </td>
                                                                <td class="tableData" style="width: 50px;">
                                                                    <asp:Label ID="lblCurrentPage" runat="server">1</asp:Label><asp:Label ID="lblof1"
                                                                        runat="server">&nbsp;of&nbsp;</asp:Label><asp:Label ID="lblEndingPage" runat="server">1</asp:Label>
                                                                </td>
                                                                <td style="width: 45px;">
                                                                    <a href="#" id="lnkNextPage1" onclick="FetchNextResultSet()" class="tableData" runat="server"
                                                                        aria-label="Go to next page of search results">Next <img src="images/sp_int_rightArrow_btn.gif" alt="" /></a>
                                                                    <%--<asp:LinkButton ID="NextLinkButton" runat="server" CssClass="tableData" ToolTip="Next Result Page">Next</asp:LinkButton>
                                                                    <asp:ImageButton ID="btnNextPage" runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9"
                                                                        Width="9" ToolTip="Next Result Page" />--%>
                                                                </td>
                                                                <td style="width: 25px;">
                                                                    <img height="4" src="images/spacer.gif" width="25" alt="">
                                                                </td>
                                                                <td class="tableData" style="width: 40px; text-align: right;">
                                                                    <label id="lblPage1" for="txtPageNumber" runat="server">Page:</label>
                                                                </td>
                                                                <td style="width: 40px;">
                                                                    <SPS:SPSTextBox ID="txtPageNumber" runat="server" CssClass="tableData" Width="40px"
                                                                        MaxLength="7" ToolTip="Specify search result page number to go to." />
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="btnGoToPage" ImageUrl="images/sp_int_go_btn.gif" runat="server"
                                                                        AlternateText="Go to specified search result page number" Height="25" Width="40"
                                                                        ToolTip="Go to specified search result page number" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table width="670" border="0" role="presentation">
                                                            <tr>
                                                                <td>
                                                                    <asp:DataGrid ID="dgTechBulletinResults" Width="680" AutoGenerateColumns="False" runat="server">
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderText="Access" ItemStyle-Width="10%" Visible="false" ItemStyle-Height="100%">
                                                                                <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                                <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                                <ItemTemplate>
                                                                                    <a class="tableData" id="lblAccess" runat="server"></a>
                                                                                    <br />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="KB ID<br/>Number" ItemStyle-Width="10%" ItemStyle-Height="100%">
                                                                                <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                                <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblTBulletinNo" CssClass="tableData" runat="server" BackColor="White" Width="100%"></asp:Label><br/>
                                                                                    <asp:Label ID="lblNullspace" runat="server" CssClass="tableData" Width="100%">&nbsp;</asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Subject" ItemStyle-Width="80%" ItemStyle-Height="100%">
                                                                                <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                                <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="lnkBulletin" CommandName="BulletinClick" CssClass="tableData" BackColor="white" runat="server" Width="100%">lnkBulletin</asp:LinkButton><br/>
                                                                                    <asp:Label ID="lblModel" CssClass="tableData" runat="server" Width="12%">
																					    <strong>Model(s):</strong>
                                                                                    </asp:Label><br/>
                                                                                    <asp:Label ID="lblModelNullSpace" CssClass="tableData" runat="server" Width="12%">
																					    &nbsp;
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblModelList" runat="server" CssClass="tableData" Width="88%"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderText="Date Published" ItemStyle-Width="10%" ItemStyle-Height="100%">
                                                                                <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                                <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblPrint" CssClass="tableData" runat="server" BackColor="White" Width="100%"></asp:Label><br/>
                                                                                    <asp:Label ID="lblNullspace1" CssClass="tableData" runat="server" Width="100%">&nbsp;</asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="tblPagination2" style="border: none;" role="navigation" aria-label="Pagination navigation" runat="server">
                                                            <tr style="vertical-align: bottom;">
                                                                <td style="width: 65px;">
                                                                    <a href="#" id="lnkPreviousPage2" onclick="FetchPreviousResultSet()" class="tableData" runat="server"
                                                                        aria-label="Go to previous page of search results"><img src="images/sp_int_leftArrow_btn.gif" alt="" /> Previous</a>
                                                                    <%--<asp:ImageButton ID="btnPreviousPage2" runat="server" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                        Height="9" Width="9" ToolTip="Previous Results Page"></asp:ImageButton><asp:LinkButton
                                                                            ID="PreviousLinkButton2" runat="server" CssClass="tableData" ToolTip="Previous Results Page">Previous</asp:LinkButton>--%>
                                                                </td>
                                                                <td class="tableData" style="width: 50px;">
                                                                    <asp:Label ID="lblCurrentPage2" runat="server">1</asp:Label><asp:Label ID="lblof2"
                                                                        runat="server">&nbsp;of&nbsp;</asp:Label><asp:Label ID="lblEndingPage2" runat="server">1</asp:Label>
                                                                </td>
                                                                <td style="width: 45px;">
                                                                    <a href="#" id="lnkNextPage2" onclick="FetchNextResultSet()" class="tableData" runat="server"
                                                                        aria-label="Go to next page of search results">Next <img src="images/sp_int_rightArrow_btn.gif" alt="" /></a>
                                                                    <%--<asp:LinkButton ID="NextLinkButton2" CssClass="tableData" ToolTip="Next Result Page"
                                                                        runat="server">Next</asp:LinkButton><asp:ImageButton ID="btnNextPage2"
                                                                            runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif"
                                                                            Height="9" Width="9" ToolTip="Next Result Page" />--%>
                                                                </td>
                                                                <td style="width: 25px;">
                                                                    <img height="4" src="images/spacer.gif" width="25" alt="">
                                                                </td>
                                                                <td class="tableData" style="width: 40px; text-align: right;">
                                                                    <label id="lblPage2" for="txtPageNumber2" runat="server">Page:</label>
                                                                </td>
                                                                <td style="width: 40px;">
                                                                    <SPS:SPSTextBox ID="txtPageNumber2" runat="server" CssClass="tableData" Width="40px"
                                                                        MaxLength="7" ToolTip="Page Number" />
                                                                </td>
                                                                <td>
                                                                    <asp:ImageButton ID="btnGoToPage2" runat="server" AlternateText="Goto specified result page"
                                                                        ImageUrl="images/sp_int_go_btn.gif" Height="25" Width="40" ToolTip="Goto Page" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table id="tblBackToTop" style="width: 283px; border: none;" role="navigation" runat="server">
                                                            <tr>
                                                                <td>
                                                                    <a href="#top">
                                                                        <img src="images/sp_int_back2top_btn.gif" height="28" width="78" alt="Go back to top of the results page"
                                                                            aria-label="Click to go back to the top of the page."></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                </div>
                                <tr>
                                    <td>
                                        <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>

    </center>
</body>
</html>
