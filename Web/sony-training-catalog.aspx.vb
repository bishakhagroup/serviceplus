Imports System.IO
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Data


Namespace ServicePLUSWebApp

    Partial Class sony_training_catalog
        Inherits SSL

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub
#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If HttpContextManager.GlobalData.IsCanada Then
                    DefaultSonyTraining.Visible = False
                    hiddenSonyTraining.Visible = True
                    Return
                Else
                    DefaultSonyTraining.Visible = True
                    hiddenSonyTraining.Visible = False
                End If

                If Not IsPostBack Then
                    Dim sb As New StringBuilder()
                    Dim sw As New StringWriter(sb)
                    Dim cm As New CourseDataManager()
                    Dim transform As New XslTransform()

                    Dim xmlDoc As XmlDataDocument = cm.GetMajorMinorCategories()
                    Dim xslPath As String = Server.MapPath("MajorMinorCategory.xslt")
                    Dim styleSheetDoc As New XPathDocument(xslPath)
                    Dim nav As XPathNavigator = styleSheetDoc.CreateNavigator()

                    transform.Load(nav, Nothing, [GetType]().Assembly.Evidence)

                    transform.Transform(xmlDoc, Nothing, sw, Nothing)
                    tableCategory.InnerHtml = sb.ToString()
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class

End Namespace
