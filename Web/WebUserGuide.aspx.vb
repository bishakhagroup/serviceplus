Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS.Core


Namespace ServicePLUSWebApp


    Partial Class WebUserGuide
        Inherits SSL


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            'Dim thisCH As New ConfigHandler

            'If thisCH.GetCurrentEnvironment.ToString.ToLower = "staging" Then
            '    ServicesPlusLabel.Text = "Home page: <a href=""https://servicesplus-stage.us.sony.biz"">https://servicesplus-stage.us.sony.biz</a>"
            'Else
            '    ServicesPlusLabel.Text = "Home page: <a href=""https://www.servicesplus.sel.sony.com"">https://www.servicesplus.sel.sony.com</a>"
            'End If

            '2016-05-11 ASleight Modifying for US/CA site root
			'Dim objGlobalData As New GlobalData
			'If Session.Item("GlobalData") IsNot Nothing Then
			'    objGlobalData = Session.Item("GlobalData")
			'End If
            Dim rootURL As String
			If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
				rootURL = ConfigurationData.Environment.URL.CA
			Else
				rootURL = ConfigurationData.Environment.URL.US
			End If
            'ServicesPlusLabel.Text = GetGlobalResourceObject("Resource", "web_usr_gd_hmpg_msg") + "<a href=""" + ConfigurationData.Environment.URL.HomePage + """>" + ConfigurationData.Environment.URL.HomePage + "</a>"
            ServicesPlusLabel.Text = GetGlobalResourceObject("Resource", "web_usr_gd_hmpg_msg") + String.Format("<a href=""{0}"">{0}</a>", rootURL)
        End Sub

    End Class

End Namespace
