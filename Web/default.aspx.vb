Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class _default
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            Try
                'CODEGEN: This method call is required by the Web Form Designer
                'Do not modify it using the code editor.
                InitializeComponent()
                Promotions1.DisplayPage = "default"
                RHSPromos.DisplayPage = "default"
            Catch ex As Exception

            End Try
        End Sub

#End Region
        Public LoginURL As String = ""
        Public isCanada As Boolean = False
        Public isFrench As Boolean = False
        Public ForgotPassword As String
        Protected WithEvents IMG2 As HtmlImage
        Protected WithEvents IMG3 As HtmlImage
        Protected WithEvents tbUserName As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents tbEmailAddress As Sony.US.ServicesPLUS.Controls.SPSTextBox
        Protected WithEvents btnNext As Button
        Private sm As New SecurityManager
        Private sa As New SecurityAdministrator
        'Dim objUtilties As Utilities = New Utilities

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Try
                ' Set the Country and Language booleans for use in this method and in the page's markup
                isCanada = HttpContextManager.GlobalData.IsCanada
                isFrench = HttpContextManager.GlobalData.IsFrench

                Session.Remove("RegisterReturnPage")
                Session.Remove("RegisterReturnPageQS")
                hdnSessionId.Value = Session.SessionID.ToString()
                Dim isUniqueNumberFromUI As Boolean = False

                ' 2016-05-20 ASleight - Modifying to set up the Login URL and change how the ForgotPassword is set up
                If isCanada Then
                    If isFrench Then  ' fr-CA
                        LoginURL = ConfigurationData.Environment.IdentityURLs.LoginFRCA
                    Else  ' en-CA
                        LoginURL = ConfigurationData.Environment.IdentityURLs.LoginENCA
                    End If
                Else    ' en-US
                    LoginURL = ConfigurationData.Environment.IdentityURLs.LoginUS
                End If

                ForgotPassword = ConfigurationData.Environment.ForgotPassword.ForgotPasswordURL + "&Lang=" + HttpContextManager.GlobalData.LanguageForIDP
                ' 2016-05-20 ASleight - End changes

                If (Not Page.IsPostBack) Then
                    Response.CacheControl = "no-cache"
                    Response.AddHeader("Pragma", "no-cache")
                    Response.Expires = -1
                End If

                If Session("EmailIdNotMatched") IsNot Nothing Then
                    'Utilities.LogMessages("Default - Email ID mismatch triggered.")
                    Dim uniqueNumber As String = Utilities.GetUniqueKey()
                    Try
                        Dim ht As Hashtable = CType((Session("EmailIdNotMatched")), Hashtable)
                        Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                        ErrorLabel.Text = Resources.Resource.home_eror_msg + "<a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + Resources.Resource.home_eror_msg_1 + " </a>" + Resources.Resource.home_eror_msg_2 + uniqueNumber
                        Throw New ApplicationException(Resources.Resource.home_eror_msg_3 + ht.Item("ldapId").ToString() + Resources.Resource.home_eror_msg_4 + ht.Item("emailAddress").ToString() + Resources.Resource.home_eror_msg_5)
                    Catch ex As Exception
                        Utilities.WrapExceptionforUINumber(ex, uniqueNumber)
                    Finally
                        Session.Remove("EmailIdNotMatched")
                    End Try
                End If

                If (Session("HOMEPAGE")) IsNot Nothing Then
                    Try
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(Session("HOMEPAGE"))
                    Catch ex As Exception
                        Utilities.LogMessages("Default - Exception while processing exception from another page.")
                    Finally
                        Session.Remove("HOMEPAGE")
                    End Try
                End If

                If Not Request.QueryString.Item("logout") = "true" And HttpContextManager.Customer IsNot Nothing Then
                    Response.Redirect("Member.aspx", True)
                End If

                If Request.QueryString.Item("error") IsNot Nothing Then
                    Select Case Request.QueryString.Item("error")
                        Case "ThirdIdFailed"
                            ErrorLabel.Text = "&nbsp;&nbsp;" + Resources.Resource.home_eror_lgnfld_msg
                    End Select
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Function RandomNumber(ByVal low As Integer, ByVal high As Integer) As Integer
            Static RandomNumGen As New System.Random
            Return RandomNumGen.Next(low, high + 1)
        End Function

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function GetCulture(ByVal selectedLocale As String) As String
            Try
                ' Fetch a copy of GlobalData from the Session. We will store it again later.
                Dim globalData = HttpContextManager.GlobalData

                If (String.IsNullOrEmpty(selectedLocale)) Then
                    selectedLocale = ConfigurationData.GeneralSettings.GlobalData.Language.US
                End If

                globalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                globalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                globalData.Language = selectedLocale
                If (selectedLocale = "en-US") Then
                    globalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                Else
                    globalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                End If

                HttpContextManager.SelectedLang = selectedLocale
                HttpContextManager.GlobalData = globalData
                HttpContext.Current.Session.Add("Language_Changed", "1")
            Catch ex As Exception

            End Try
            Return selectedLocale
        End Function
    End Class

End Namespace
