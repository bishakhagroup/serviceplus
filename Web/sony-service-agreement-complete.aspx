<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-service-agreement-complete.aspx.vb" Inherits="sony_service_agreement_complete" %>

<%@ Import Namespace="Sony.US.ServicesPLUS.Core" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=PageLanguage %>">
<head runat="server">
    <title><%=Resources.Resource.ttl_ProfesstionalProduct%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
    
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
    <form id="form1" runat="server">
        <table width="760" border="0" role="presentation">
            <tr>
                <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                    <img height="25" src="images/spacer.gif" width="25" alt="">
                </td>
                <td width="689" bgcolor="#ffffff">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 10px;">
                                <asp:Label ID="lblError" CssClass="redAsterick" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <% If agreement IsNot Nothing Then %>
                                    <p class="bodyCopy" style="padding: 5px;">Your Reseller Agreement has been submitted, and an email has been sent to the
                                        registered address with the information below. Please keep it for your records.</p>

                                    <%-- AGREEMENT INFORMATION --%>
                                    <center>
                                        <table class="tableData" style="width: 93%; vertical-align: top; color: #666; border: solid 1px #666; line-height: 20px;" role="presentation">
                                        <tr>
                                            <td style="border-bottom: solid thin #666">
                                                <img src="images/sp_int_sonylogo.gif" alt="Sony Logo" />
                                            </td>
                                            <td style="border-bottom: solid thin #666">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="background: #d5dee9; font-weight: bold; text-align: center; border-bottom: solid 1px #666; border-top: solid 1px #666; ">
                                                ServicesPLUS - <%=Resources.Resource_mail.ml_ProductRegistration_body_ServiceAgreement_en %>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top; <%= IIf(isColorRow, " background-color:#d5dee9;", "") %>">
                                            <td style="width: 175px;"><%=Resources.Resource_mail.ml_ProductRegistration_body_RegistrationDate_en %></td>
                                            <td><%=DateTime.Now.ToString("MMM dd, yyyy") %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <tr style="vertical-align: top; <%= IIf(isColorRow, " background-color:#d5dee9;", "") %>">
                                            <td>Service Agreement Number:</td>
                                            <td><%=agreement.AgreementNumber %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>

                                        <%-- End-User Information --%>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_RARequest_companyname_en %></td>
                                            <td><%=agreement.Company %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_common_Name_en %>:</td>
                                            <td><%=agreement.FirstName %> <%=agreement.LastName %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_common_Email_en %>:</td>
                                            <td><%=agreement.Email %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_common_Address_en %>:</td>
                                            <td><%=userAddress %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_RARequest_phone_en %></td>
                                            <td><%=agreement.Phone & IIf(String.IsNullOrWhiteSpace(agreement.Extension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.Extension}") %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <% If Not String.IsNullOrWhiteSpace(agreement.Fax) Then %>
                                        <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                            <td><%=Resources.Resource_mail.ml_ProductRegistration_body_fax_en %>:</td>
                                            <td><%=agreement.Fax %></td>
                                        </tr>
                                        <% isColorRow = Not isColorRow %>
                                        <% End If %>

                                        <%-- Reseller Information --%>
                                        <% If agreement.HasReseller() Then %>
                                            <tr style="background-color: #404040;">
                                                <td colspan="2" style="color: white; font-weight: bold;">Reseller Information:</td>
                                            </tr>
                                            <% If Not String.IsNullOrWhiteSpace(agreement.ResellerCompany) Then %>
                                            <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                                <td>Reseller <%=Resources.Resource_mail.ml_RARequest_companyname_en %></td>
                                                <td><%=agreement.ResellerCompany %></td>
                                            </tr>
                                            <% isColorRow = Not isColorRow %>
                                            <% End If %>
                                            <% If Not String.IsNullOrWhiteSpace(agreement.ResellerName) Then %>
                                            <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                                <td>Reseller <%=Resources.Resource_mail.ml_common_Name_en %>:</td>
                                                <td><%=agreement.ResellerName %></td>
                                            </tr>
                                            <% isColorRow = Not isColorRow %>
                                            <% End If %>
                                            <% If Not String.IsNullOrWhiteSpace(agreement.ResellerPhone) Then %>
                                            <tr <%=IIf(isColorRow, " style=""background-color:#d5dee9;""", "") %>>
                                                <td><%=Resources.Resource_mail.ml_RARequest_phone_en %></td>
                                                <td><%=agreement.ResellerPhone & IIf(String.IsNullOrWhiteSpace(agreement.ResellerExtension), "", $"&nbsp;&nbsp;&nbsp;Ext: {agreement.ResellerExtension}") %></td>
                                            </tr>
                                            <% isColorRow = Not isColorRow %>
                                            <% End If %>
                                        <% End If %>

                                        <%-- PRODUCT LIST --%>
                                        <tr style="background-color: #404040;">
                                            <td colspan="2" style="color: white; font-weight: bold;">Product List:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table cellpadding="4" role="presentation"
                                                    style="vertical-align: middle; text-align: center; color: #333333; width: 90%; border: none; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
                                                    <tr style="background-color: #d5dee9; font-weight: bold;">
                                                        <th><%=Resources.Resource_mail.ml_ProductRegistration_body_ModelNumber_en %></th>
                                                        <th><%=Resources.Resource_mail.ml_ProductRegistration_body_SerialNumber_en %></th>
                                                        <th><%=Resources.Resource_mail.ml_ProductRegistration_body_PurchaseDate_en %></th>
                                                    </tr>

                                                    <% For Each item As ServiceAgreementItem In agreement.EquipmentList %>
                                                        <tr <%=IIf(isColorProductRow, " style=""background-color:#d5dee9;""", "") %>>
                                                            <td><%=item.Model%></td>
                                                            <td><%=item.Serial%></td>
                                                            <td><%=item.DatePurchased.ToString("MMM dd, yyyy") %></td>
                                                        </tr>
                                                        <% isColorProductRow = Not isColorProductRow %>
                                                    <% Next %>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    </center>
                                <% End If %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                    <img src="images/spacer.gif" width="25" height="20" alt="">
                </td>
            </tr>
        </table>
    </form>
    </center>
</body>
</html>
