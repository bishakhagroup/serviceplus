<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.PunchOutSignIn"
    EnableViewStateMac="true" CodeFile="PunchOutSignIn.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS - Login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">    
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script language="javascript">

        function SetFocus() {

            document.getElementById("UName").focus();
        }

        function DoCallback(url, params) {
            var pageUrl = url + "?callback=true&param=" + params;
            var xmlRequest;
            if (window.XMLHttpRequest) {
                xmlRequest = new XMLHttpRequest();
            }

            else if (window.ActiveXObject) {
                xmlRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlRequest.open("POST", pageUrl, false);
            xmlRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlRequest.send(null);
            return xmlRequest;
        }
        function SetTarget() {
            var opennerPage = window.opener.location;
            //var xmlRequest = DoCallback("SignIn.aspx", opennerPage);
            var xmlRequest = DoCallback("SignIn-Register.aspx", opennerPage);
            //window.opener.location = "Register.aspx";
            window.opener.location = "Register-Account-Tax-Exempt.aspx";
            window.close();
        }
        function OpenContactUs() {
            var opennerPage = window.opener.location;
            //var xmlRequest = DoCallback("SignIn.aspx", opennerPage);
            var xmlRequest = DoCallback("SignIn-Register.aspx", opennerPage);
            window.opener.location = "sony-service-contacts.aspx";
            window.close();
        }
            
    </script>

</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" onload="SetFocus();">
    <center>
        <form id="SignIn" method="post" runat="server">
        <table width="760" border="0">
            <tr>
                <td width="25" background="images/sp_left_bkgd.gif">
                    <img height="25" src="images/spacer.gif" width="25">
                </td>
                <td width="710" bgcolor="#ffffff">
                    <table width="710" border="0">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0">
                                    <tr>
                                        <td width="464" bgcolor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                            height="57" align="right" valign="top">
                                            <br/>
                                            <h1 class="headerText">ServicesPLUS PunchOut&nbsp;&nbsp;</h1>
                                        </td>
                                        <td valign="middle" bgcolor="#363d45">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="466" bgcolor="#f2f5f8">
                                            <table width="464" border="0">
                                                <tr>
                                                    <td width="20">
                                                    </td>
                                                    <td>
                                                        <h2 class="headerTitle">Login</h2>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td bgcolor="#99a8b5" valign="bottom">
                                        </td>
                                    </tr>
                                    <tr height="9">
                                        <td width="466"  bgcolor="#f2f5f8"><img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464"></td>
                                        <td bgcolor="#99a8b5"><img height="9" src="images/sp_int_header_btm_right.gif" width="246"></td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div id="divLogin" runat="server" visible="true">
                                    <table width="100%" border="0" cellpadding="2" class="redAsterick">
                                        <tr>
                                            <td width="10%">
                                                <img src="images/spacer.gif" width="20" height="4">
                                            </td>
                                            <td width="90%">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellpadding="2" class="redAsterick">
                                        <tr>
                                            <td width=5%>
                                                <img src="images/spacer.gif" width="20" height="4">
                                            </td>
                                            <td width=55%>
                                                <table width="45%" border="0"  cellpadding="2" class="redAsterick">
                                                       <tr>
                                                            <td class=tableData>
                                                                Put PunchOut Text here
                                                            </td>
                                                       </tr>
                                                </table>
                                            </td>
                                            <td width=40%>
                                                <table width="100%" border="0"  cellpadding="2" class="redAsterick">
                                                    <tr>
                                                        <td width="90%" class="bodyCopy">
                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                <asp:Label ID="lnkcontact" runat="server" Visible="false"><a id="aLnkContact" href="#" onclick="OpenContactUs()" class="redAsterick">Contact Us</a></asp:Label>
                                                <asp:Label ID="TechError" runat="server" CssClass="redAsterick" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="bodyCopy">
                                                            User Name:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" valign="top">
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="UName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                                class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="12" class="bodyCopy" width="90%">
                                                            Password:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="tableHeader" height="25">
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="Password" runat="server" CssClass="bodyCopy" TextMode="Password"></SPS:SPSTextBox><span
                                                                class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="12" class="bodyCopy" width="90%">
                                                            <asp:Label ID="QuestionLabel" runat="server" CssClass="bodyCopy" Visible="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="tableHeader" height="25">
                                                            &nbsp;
                                                            <SPS:SPSTextBox ID="DOB" runat="server" CssClass="bodyCopy" TextMode="Password"></SPS:SPSTextBox><span
                                                                class="redAsterick">*</span>
                                                            <asp:Label ID="DateFormatLabel" runat="server" CssClass="bodyCopy" Visible="true"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="tableHeader" height="25">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="tableHeader">
                                                            <asp:ImageButton ID="Submit" runat="server" ImageUrl="./images/sp_int_login_btn.gif"
                                                                AlternateText="Login"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="90%" class="bodyCopy">
                                                            <img src="images/spacer.gif" width="4" height="10"><br/>
                                                            &nbsp;<a runat=server id="frgtPassLink" href="#">Forgot your password?</a><br/>
                                                            <br/>
                                                            <%--&nbsp;<a href="register.aspx">Register as a New ServicesPLUS User</a>--%>
                                                            &nbsp;<a href="Register-Account-Tax-Exempt.aspx">Register as a New ServicesPLUS User</a>
                                                            <br />
                                                            <span class="Bodycopy">(<a href="#" class="Bodycopy" onclick="window.open('sony-service-agreement-info.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=no,resize=no,width=400,height=475'); return false;">Why
                                                                should I become a ServicesPLUS user?</a>)</span>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </table>
                                </div>
  <div id="divPromptPassword" runat="server" visible="false">
                                        <br/>
                                        <table width="100%" border="0" cellpadding="2" class="redAsterick">
                                            <tr>
                                                <td height="16" width="10%">
                                                </td>
                                                <td height="16" width="90%">
                                                    <asp:Label ID="lblPrompt" runat="server" CssClass="redAsterick" Text="Your old password does not confirm to the security policies. <br/> Please enter the new password. Passwords must be alpha numeric and are case sensitive."></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td width="10%" height="16">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" height="16">
                                                    <asp:Label ID="lblSecurityQuestion" runat="server" CssClass="bodyCopy"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td width="10%" valign="top">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" valign="top">
                                                    &nbsp;
                                                    <SPS:SPSTextBox ID="txtSecurityQuestion" runat="server" CssClass="bodyCopy" TextMode="Password"
                                                        MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%" height="16">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" height="16" class="bodyCopy">
                                                    New password:</td>
                                            </tr>
                                            <tr>
                                                <td width="10%" valign="top">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" valign="top">
                                                    &nbsp;
                                                    <SPS:SPSTextBox ID="txtNewPassword1" runat="server" CssClass="bodyCopy" TextMode="Password"
                                                        MaxLength="15"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="12" width="10%">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" height="16" class="bodyCopy">
                                                    Confirm New password:</td>
                                            </tr>
                                            <tr>
                                                <td width="10%" class="tableHeader" height="25">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" class="tableHeader" height="25">
                                                    &nbsp;
                                                    <SPS:SPSTextBox ID="txtNewPassword2" runat="server" CssClass="bodyCopy" TextMode="Password"
                                                        MaxLength="15"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%" class="tableHeader">
                                                    <img src="images/spacer.gif" width="20" height="4"></td>
                                                <td width="90%" class="tableHeader">
                                                    <asp:ImageButton ID="btnUpdatePwd" runat="server" ImageUrl="<%=Resources.Resource.img_btnSubmit()%>"
                                                        AlternateText="Submit"></asp:ImageButton></td>
                                            </tr>
                                        </table>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" background="images/sp_right_bkgd.gif">
                    <img height="5" src="images/spacer.gif" width="25">
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html>
