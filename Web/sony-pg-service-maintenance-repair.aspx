<%@ Page Language="VB" AutoEventWireup="false" SmartNavigation="False" CodeFile="sony-pg-service-maintenance-repair.aspx.vb"
    Inherits="ServicePLUSWebApp.pg_service_maintenance_repair" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.repair_pg_svc_msg()%> <%=sModelName%> <%=Resources.Resource.repair_pg_svc_msg_1()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function ShowPopupDialog(pID) {
            if (pID === '1') {
                window.open("http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml", "mywindow");
            } else if (pID === '2') {
                window.open("http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-supportnet.shtml", "mywindow");
            } else if (pID === '3') {
                window.open("http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-sendinrepair.shtml", "mywindow");
            } else if (pID === '4') {
                window.open("http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-fieldservice.shtml", "mywindow");
            }
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <%-- Do not remove below line; it gets rid of a compiler bug that throws IntelliSense errors --%>
        <%="" %>
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background-image: url('images/sp_left_bkgd.gif')">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" style="background-image: url('images/repair_lrg.gif'); background-color: #363d45; width: 465px; height: 82px;">
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4 %></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td bgcolor="#f2f5f8" style="width: 465px">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server"></h2>
                                                <%If thruSearchModel = True Then%>
                                                    <br /><span class="finderSubhead"><%=Resources.Resource.repair_pg_mdlst_msg_1 %></span>
                                                <%End If%>
                                            </td>
                                            <td width="238" style="padding-left: 3px; background: #99a8b5">
                                                <span style="color: black; font-family:Arial, Helvetica, sans-serif; font-size: 12px;">
                                                    <%=Resources.Resource.repair_pg_mdlst_msg_2%></span><br />
                                                <a href="sony-repair.aspx"><img src="<%=Resources.Resource.repair_svc_img_5%>" border="0" alt="Change model" /></a>
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="width: 465px; background: #f2f5f8;">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="width: 238px; background: #99a8b5;">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <%If fOverallLayout = SETFLAG Then%>
                                        <tr>
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">
                                                <p class="promoCopyBold"><%=Resources.Resource.repair_pg_dptfld_msg()%> <%=sModelName%> <%=Resources.Resource.repair_pg_unt_msg()%></p>
                                            </td>
                                            <td style="width: 436px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">
                                                &nbsp;
                                            </td>
                                            <td align="center" style="width: 436px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">
                                                <table width="100%" cellpadding="3" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <p class="promoCopyBold"><%=Resources.Resource.repair_dptsvc_msg()%></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodyCopy">
                                                            <%If fDeportContactMsg = 0 And fDepotRequest = SETFLAG Then%>
                                                            <%=Resources.Resource.repair_pg_sndur_msg()%> <%=sModelName%> <%=Resources.Resource.repair_pg_sndur_msg_1()%>
                                                            <%ElseIf fDeportContactMsg = 1 And fDepotRequest = 0 Then%>
                                                            <%=Resources.Resource.repair_pg_tosndur_msg()%>
                                                            <%End If%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <%If fDepotRequest = SETFLAG Then%>
                                                                <%If fCustomerSession = True Then%>
                                                                    <a href="sony-service-sn.aspx">
                                                                <%Else%>
                                                                    <a href="SignIn-Register.aspx?SPGSModel=<%=sModelName%>">
                                                                <%End If%>
                                                                <img src="<%=Resources.Resource.repair_snypg_mdl_type_3_img%>"
                                                                    border="0" alt="Request depot service" /></a><br />
                                                            <%End If%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 443px">
                                                            <span class="redAsterick"><%=Resources.Resource.repair_newchksts_msg()%></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="sony-repair-status.aspx">
                                                                <img src="<%= Resources.Resource.repair_snypg_mdl_type_2_img() %>"
                                                                    border="0" alt="Check Service Status" id="imbSearchRepairStatus" /></a><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 438px">
                                                            <p class="promoCopyBold"><%=Resources.Resource.repair_fs_msg()%></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="bodyCopy">
                                                            <%=Resources.Resource.repair_pg_torqst_msg()%> <%=sModelName%> <%=Resources.Resource.repair_pg_torqst_msg_1()%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label for="ddlStates" class="bodyCopy"><strong><%=Resources.Resource.repair_state_msg()%>&nbsp;</strong></label>
                                                            <asp:DropDownList ID="ddlStates" runat="server" Style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 13px; color: black"
                                                                ValidationGroup="ValidateState" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<%$Resources:Resource,repair_svccntrss_msg%>"
                                                                ControlToValidate="ddlStates" CssClass="bodyCopy" Font-Bold="True" ValidationGroup="ValidateState" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:ImageButton ID="imgBtnGo" runat="server" ImageUrl="<%$ Resources:Resource,repair_snypg_mdl_type_1_img %>"
                                                                ImageAlign="AbsBottom" AlternateText="Request Field Service" ValidationGroup="ValidateState" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%--Sneha on 24/03/2012:Width of the below td is changed to 34% for bug 8994(bugzilla)--%>
                                            <%If divsvcrprShow = True Then%>
                                            <td width="34%">
                                                <uc1:SPSPromotion ID="RHSPromn1" DisplayPage="sony-pg-service-maintenance-repair"
                                                    Type="1" runat="server" />
                                            </td>
                                            <%End If%>
                                        </tr>
                                        <%End If%>

                                        <%If fServiceLocResult = SETFLAG Then%>
                                        <%If sStateName.Trim() <> "" Then%>
                                        <tr valign="top">
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">
                                                <p class="promoCopyBold">
                                                    <asp:Label ID="lblResult" runat="server" Visible="False"></asp:Label>
                                                </p>
                                                <br />
                                                <span class="bodyCopy"><%=Resources.Resource.repair_pg_frsvc_msg()%>
                                                    <a href='javascript:ShowPopupDialog(1);'><%=Resources.Resource.repair_pg_wrnty_msg()%></a>
                                                    or <a href='javascript:ShowPopupDialog(2);'><%=Resources.Resource.repair_pg_svccnt_msg()%></a></span>
                                                <ul>
                                                    <li><span class="bodyCopy">
                                                        <%=Resources.Resource.repair_pg_fldsvc_msg()%> $<%=nRateField%> <%=Resources.Resource.repair_pg_fldsvc_msg_1()%></span> </li>
                                                    <li><span class="bodyCopy">
                                                        <%=Resources.Resource.repair_pg_minfld_msg()%> <%=nMinLabourTm%> <%=Resources.Resource.repair_pg_minfld_msg_1()%></span> </li>
                                                </ul>
                                                <%If Not String.IsNullOrEmpty(sModelName) Then%>
                                                <p class="promoCopyBold">
                                                    <span color="blue"><a href='sony-pg-service-maintenance-repair.aspx?model=<%=sModelName%>'>
                                                        <%=Resources.Resource.repair_pg_vw_msg()%> <%=sModelName %> <%=Resources.Resource.repair_pg_dptsvc_msg()%></a></span>
                                                </p>
                                                <%Else%>
                                                <p class="promoCopyBold">
                                                    <span color="blue"><a href='sony-repair.aspx'>
                                                        <%=Resources.Resource.repair_pg_vw_msg()%> <%=sModelName %> <%=Resources.Resource.repair_pg_dptsvc_msg()%></a></span>
                                                </p>
                                                <%End If%>
                                            </td>
                                            <%-- By Sneha on 25/03/2014: The following row width is changed to 34% from 436px to fix bug 8994(bugzilla) --%>
                                            <%If divsvcrprShow = True Then%><td style="width: 34%;">
                                                <uc1:SPSPromotion ID="RHSPromn2" DisplayPage="sony-pg-service-maintenance-repair"
                                                    Type="1" runat="server" />
                                            </td>
                                            <%End If%>
                                        </tr>
                                        <%Else%>
                                        <tr valign="top">
                                            <td style="width: 14px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 7790px">
                                                <p class="promoCopyBold"><span color="red"><%=Resources.Resource.repair_pg_nosvcdtl_msg()%></span></p>
                                                <br />
                                                <a href='javascript:history.back();'>
                                                    <span class="promoCopyBold" color="blue"><%=Resources.Resource.repair_pg_vw_msg()%> <%=sModelName%> <%=Resources.Resource.repair_pg_dptsvc_msg()%></span>
                                                </a>
                                            </td>
                                            <%If divsvcrprShow = True Then%>
                                            <td style="width: 512px">
                                                <uc1:SPSPromotion ID="RHSPromn3" DisplayPage="sony-pg-service-maintenance-repair"
                                                    Type="1" runat="server" />
                                            </td>
                                            <%End If%>
                                        </tr>
                                        <%End If%>
                                        <%End If%>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                            <td colspan="2">
                                                <%If divsvcrprShow = True Then%>
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <uc1:SPSPromotion ID="Promotions1" DisplayPage="sony-pg-service-maintenance-repair"
                                                                Type="2" runat="server" />
                                                        </td>
                                                        <td width="16">
                                                            <img src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <uc1:SPSPromotion ID="SPSPromotionBottomRight" DisplayPage="sony-pg-service-maintenance-repair"
                                                                Type="3" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%End If%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
