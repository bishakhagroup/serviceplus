<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SPSPromotion" Src="~/UserControl/SPSPromotion.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Member"
    EnableViewStateMac="true" CodeFile="Member.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.memeber_wcmem_msg%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <link href="includes/svcplus_globalization.css" rel="stylesheet" />

    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <style type="text/css">
        .style1 {
            height: 56px;
        }

        .style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 15px;
            color: #666666;
            height: 56px;
        }

        .style3 {
            height: 21px;
        }

        .style4 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 11px;
            line-height: 15px;
            color: #666666;
            height: 21px;
        }
    </style>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="Form1" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 710px; height: 123px;" role="presentation">
                                        <tr>
                                            <td style="background: #000 url('images/Homepage_Banner.gif')">
                                                <table role="presentation">
                                                    <tr>
                                                        <td width="140">
                                                            &nbsp;
                                                        </td>
                                                        <td colspan="3">
                                                            <h1 id="lblWelcome" class="headerText" runat="server"><%=Resources.Resource.memeber_wc_msg%></h1>
                                                        </td>
                                                        <td>
                                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="140">
                                                            &nbsp;
                                                        </td>
                                                        <td width="10">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="LabelMemberName" runat="server" CssClass="memberName" Width="170"></asp:Label>
                                                        </td>
                                                        <td width="140">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="PasswordExpirationLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 17px;">
                                <td style="background: url('images/sp_hdr_btm_bar.gif')">
                                    <%-- <img style="WIDTH: 710px; HEIGHT: 17px" height="17" src="images/sp_hdr_btm_bar.jpg" width="710">--%>
                                    <img height="17" src="images/spacer.gif" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img height="20" src="images/spacer.gif" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td>
                                                <table border="0" role="presentation">
                                                    <tr valign="top">
                                                        <td width="430">
                                                            <table border="0" role="presentation">
                                                                <tr>
                                                                    <td class="style3">
                                                                        <%--<a href="./sony-knowledge-base-search.aspx">--%>
                                                                            <img src="images/sp_int_home_techbulletin.gif" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.menu_knowldgebase--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10" class="style3">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-knowledge-base-search.aspx" title="Visit the Knowledge Base page.">
                                                                            <%=Resources.Resource.menu_knowldgebase%></a>&nbsp;- <%=Resources.Resource.hplnk_kngbse_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 20px;">
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-operation-manual.aspx">--%>
                                                                        <img src="images/Manuals_Button.jpg" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.menu_manuals--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-operation-manual.aspx" title="Visit the Operation Manuals page.">
                                                                            <%=Resources.Resource.menu_manuals%></a>&nbsp;- <%=Resources.Resource.hplnk_manuals_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-parts.aspx">--%>
                                                                        <img src="images/parts_button.jpg" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.hplnk_parts_acc--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-parts.aspx" title="Visit the Parts page.">
                                                                            <%=Resources.Resource.hplnk_parts_acc%></a>&nbsp;- <%=Resources.Resource.hplnk_parts_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./Sony-repair.aspx">--%>
                                                                        <img src="images/repair_button.jpg" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.hplnk_depot_repair--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./Sony-repair.aspx" title="Visit the Repair page.">
                                                                            <%=Resources.Resource.hplnk_depot_repair%></a>&nbsp;- <%=Resources.Resource.hplnk_depot_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-software.aspx">--%>
                                                                        <img src="images/Software_and_Firmware_Button.jpg" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.hplnk_software_frmwre--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-software.aspx" title="Visit the Software page.">
                                                                            <%=Resources.Resource.hplnk_software_frmwre%></a>&nbsp;- <%=Resources.Resource.hplnk_software_frmwre_msg%>
                                                                        <% If isCanada Then%>
                                                                            <br />
                                                                            <% If isFrench Then %>
                                                                                <span class="redAsterick"><strong>NOUVEAU!</strong> - Maintenant disponible au Canada.</span>
                                                                            <% Else%>
                                                                                <span class="redAsterick"><strong>NEW!</strong> - Now available in Canada.</span>
                                                                            <% End If%>
                                                                        <% End If%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-training-catalog.aspx">--%>
                                                                        <img src="images/STI_Button.jpg" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.hplnk_sony_training_inst--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-training-catalog.aspx" title="Visit the Sony Training page.">
                                                                            <%=Resources.Resource.hplnk_sony_training_inst%></a>&nbsp;- <%=Resources.Resource.hplnk_sony_training_inst_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <%--<a href="./sony-service-agreements.aspx">--%>
                                                                        <img src="images/sp_int_home_serv-agrmnts.gif" style="width: 70px; height: 50px" alt="<%--=Resources.Resource.hplnk_svc_agreement--%>">
                                                                        <%--</a>--%>
                                                                    </td>
                                                                    <td width="10">
                                                                        <img src="images/spacer.gif" width="10" alt="">
                                                                    </td>
                                                                    <td class="bodyCopySM">
                                                                        <a class="promoCopyBold" href="./sony-service-agreements.aspx" title="Visit the Service Agreements page.">
                                                                            <%=Resources.Resource.hplnk_svc_agreement%></a>&nbsp;- <%=Resources.Resource.hplnk_svc_agreement_msg%><br />
                                                                        <a class="promoCopyBold" href="./sony-service-agreements.aspx" title="Visit the Product Registration page.">
                                                                            <%=Resources.Resource.hplnk_productrstn%></a>&nbsp;- <%=Resources.Resource.hplnk_productrstn_msg%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <img height="20" src="images/spacer.gif" alt="">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="17">
                                                            <img src="images/spacer.gif" width="17" alt="">
                                                        </td>
                                                        <td width="230" height="260">
                                                            <%If Not isCanada Then%>
                                                            <table role="presentation">
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="RHSPromos" Type="1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="SPSPromotion1" Type="4" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="Promotions1" Type="2" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 127px;">
                                                                    <td>
                                                                        <ServicePLUSWebApp:SPSPromotion ID="SPSPromotionBottomRight" Type="3" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%End If%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <a class="promoCopyBold" href="sony-service-repair-maintenance.aspx" runat="server" visible="false"><%=Resources.Resource.hplnk_sonysvc_modellst%></a>
                                    <a class="promoCopyBold" href="sony-part-catalog.aspx" runat="server" visible="false">Sony Part Catalog</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
