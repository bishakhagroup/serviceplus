﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OrderUploadHelp.aspx.vb"
    Inherits="OrderUploadHelp" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>
<%@ Reference Page="~/SSL.aspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=PageLanguage %>">
<head runat="server">
    <title>
        <%=Resources.Resource.ttl_UploadHelper%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <style type="text/css">
        p
        {
            margin: .5em;
            line-height: 1.5em;
        }
        b
        {
            font-weight: bold;
        }
    </style>
</head>
<body bgcolor="#ffffff" text="#000000" onload="javascript:SetTheFocusButton(event, 'btnClose');">
    <form id="Form1" method="post" runat="server">
    <table width="400" style="padding: 0px; margin: 0px; border: none;" role="presentation">
        <tr>
            <td style="background: url(images/sp_int_popup_login_hdrbkgd_onepix.gif)">
                <h1 class="headerText">&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.el_OrderUploadHelp%></h1>
            </td>
        </tr>
        <tr>
            <td style="background: url(images/sp_int_popup_login_hdrbkgd_lowr.gif)">
                <h2 class="finderCopyDark">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
            </td>
        </tr>
        <tr height="9">
            <td style="background: url(images/sp_int_header_btm_left.gif)">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="bodyCopyBold" bgcolor="silver" style="height: 18px">
                &nbsp;&nbsp;What is a CSV file?
            </td>
        </tr>
        <tr>
            <td class="bodyCopy" style="height: 18px">
                <p>
                    CSV is a simple file format used to store tabular data, such as a spreadsheet or
                    database. Files in the CSV format can be imported to and exported from programs
                    that store data in tables, such as Microsoft Excel or OpenOffice Calc. CSV stands
                    for "Comma-Separated Values".</p>
            </td>
        </tr>
        <tr>
            <td class="bodyCopyBold" bgcolor="silver" style="height: 18px">
                &nbsp;&nbsp;How to do I upload a formatted CSV file?
            </td>
        </tr>
        <tr>
            <td class="bodyCopy" style="height: 18px">
                <p>
                    In a spreadsheet program such as Excel, the first column (column A), the cell's
                    value should contain a <strong>Parts Number</strong>. The next column over (column B) should
                    contain the part's <strong>Quantity</strong>. Once everything is completed, save the document
                    as CSV (Comma Delimited) in the "Save as Type" drop-down menu.</p>
                <p>
                    Note that Excel may be very insistent that you save as another file type to preserve
                    formatting. Don't listen to it and force it to save as a .CSV file.</p>
            </td>
        </tr>
        <tr>
            <td class="bodyCopyBold" bgcolor="silver" style="height: 18px">
                &nbsp;&nbsp;Where can I get a sample CSV template?
            </td>
        </tr>
        <tr>
            <td class="bodyCopy" style="height: 18px">
                <p>
                    You can download a sample CSV file here: <a mimetype="application/octet-stream" href="Shared/sample_parts_template.csv">
                        Download Link</a></p>
            </td>
        </tr>
        <tr>
            <td height="20">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right">
                <a href="#" id="btnClose" onclick="javascript:window.close();">
                    <img src="<%=Resources.Resource.img_btnCloseWindow%>" border="0" alt="Close Window"></a>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
