<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="Message" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.RASuccess" EnableViewStateMac="true" CodeFile="RASuccess.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS - Parts Return Request Form Sucess</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form id="form4" method="post" runat="server">
        <center>
		    <table width="710" border="0" role="presentation">
                <tr>
                    <td style="height: 400px" width="25" background="images/sp_left_bkgd.gif">
                        <img height="25" src="images/spacer.gif" width="25">
                    </td>
                    <td style="height: 400px" width="710" bgcolor="#ffffff">
                        <table width="650" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 21px" height="21">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="650" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br/>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Parts and Software Return Request Form</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 12px" align="left">
                                </td>
                            </tr>
                            <tr>
								<td>
									<table width="710" border="0" role="presentation">
										<tr>
											<td width="20" height="20"><img height="20" src="images/spacer.gif" width="20"></td>
											<td width="670" height="20"><img height="20" src="images/spacer.gif" width="670"></td>
											<td width="20" height="20"><img height="20" src="images/spacer.gif" width="20"></td>
										</tr>
										<tr>
											<td width="20"><img height="20" src="images/spacer.gif" width="20"></td>
											<td width="670">													
												<table border="0" role="presentation">
													<tr>
														<td colSpan="4" height=15><span class="tableData"><asp:label id="LabelHeader" runat="server" CssClass="tableHeader">Request has been submitted.<br/>A customer service representative will contact you.</asp:label>&nbsp;</span></td>
													</tr>														
												</table>
												<img src="images/spacer.gif" width="670" height="5"><br/>
												<img src="images/spacer.gif" width="670" height="5"><br/>
											</td>
											<td width="20"><img src="images/spacer.gif" width="20" height="20"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>                                   
                        </table>                        
                    </td>
                    <td width="25" background="images/sp_right_bkgd.gif">
                        <img height="20" src="images/spacer.gif" width="25">
                    </td>
                </tr>
			</table>
		</center>
    </form>
</body>
</html>
