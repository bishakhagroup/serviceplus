<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.au_sony_knowledge_base_solution"
    CodeFile="au-sony-knowledge-base-solution.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=pagetitle%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Technical Bulletins"
        name="keywords">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0">
    <center>
        <form id="frmTechBulletinSearch" method="post" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <table width="760" border="0" role="presentation">
            <tr>
                <td width="25" background="images/sp_left_bkgd.gif">
                    <img height="25" src="images/spacer.gif" width="25" alt="">
                </td>
                <td width="710" bgcolor="#ffffff">
                    <table width="710" border="0" role="presentation">
                        <tr>
                            <td>
                                <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td valign="top" align="right" width="464" background="images/sp_int_subhd_technical_bulletins.jpg"
                                            bgcolor="#363d45" height="82">
                                            <br/>
                                            <h1 class="headerText">Knowledge Base</h1>
                                            &nbsp;&nbsp;
                                        </td>
                                        <td valign="middle" bgcolor="#363d45">
                                            <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="466" bgcolor="#f2f5f8">
                                            <table width="464" border="0" role="presentation">
                                                <tr>
                                                    <td width="20">
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSolutionHeader" CssClass="headerTitle" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td bgcolor="#99a8b5" valign="bottom">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#f2f5f8">
                                            <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                        </td>
                                        <td bgcolor="#99a8b5">
                                            <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                        </td>
                                    </tr>
                                </table>
                                <table width="710" border="0" role="presentation">
                                    <tr>
                                        <td>
                                            <img src="images/spacer.gif" width="5%" alt="">
                                        </td>
                                        <td width="90%">
                                            <asp:Label ID="errorMessageLabel" runat="server" CssClass="redAsterick" Width="100%" EnableViewState="False" />
                                        </td>
                                        <td>
                                            <img src="images/spacer.gif" width="5%" alt="">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="710" border="0" runat="server" id="solutiontable" role="presentation" visible="false">
                                    <tr>
                                        <td>
                                            <img src="images/spacer.gif" width="5%" alt="">
                                        </td>
                                        <td width="90%">
                                            <table width="100%" border="0" align="center" role="presentation">
                                                <tr>
                                                    <td width="20%" align="left" valign="top">
                                                        <asp:Label ID="lblDatePublished" runat="server" CssClass="tableHeader" Width="100%"
                                                            Text="Date Published:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <img src="images/spacer.gif" width="5" alt="">
                                                    </td>
                                                    <td width="80%" align="left" valign="top">
                                                        <asp:Label ID="lblDatePublishedValue" runat="server" CssClass="tableData" Width="100%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" align="left" valign="top">
                                                        <asp:Label ID="lblModel" runat="server" CssClass="tableHeader" Width="100%" Text="Model(s):"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <img src="images/spacer.gif" width="5" alt="">
                                                    </td>
                                                    <td width="80%" align="left" valign="top">
                                                        <asp:Label ID="lblModelValuse" runat="server" CssClass="tableData" Width="100%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" colspan="3" align="left" valign="top">
                                                        <img src="images/spacer.gif" width="5" alt=""><br />
                                                        <asp:Label ID="lblProblemQuestionValue" runat="server" CssClass="headerTitle" Width="100%"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" colspan="3" align="left" valign="top">
                                                        <img src="images/spacer.gif" width="5" alt=""><br />
                                                        <asp:Label ID="lblSolutionAnswerValue" runat="server" CssClass="tableData" Width="100%"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <img src="images/spacer.gif" width="5%" alt="">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="images/spacer.gif" width="5%" alt="">
                                        </td>
                                        <td colspan="2">
                                            <img src="images/spacer.gif" height="20" width="25" alt=""><asp:UpdateProgress ID="UpdateProgress1"
                                                runat="server">
                                                <ProgressTemplate>
                                                    <span id="Span4" class="bodyCopy">Please wait...</span>
                                                    <img src="images/progbar.gif" width="100" alt="" />
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td width="90%" align="left">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <table width="100%" border="0" align="left" role="form">
                                                        <tr>
                                                            <td align="left" colspan="2" width="100%">
                                                                <asp:Label ID="lblFeedbackMessage" runat="server" CssClass="redAsterick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" width="100%">
                                                                <SPS:SPSTextBox Visible="false" Width="60%" Height="60" ID="txtSuggestion" TextMode="MultiLine"
                                                                    CssClass="tableData" runat="server" MaxLength="2000"></SPS:SPSTextBox>
                                                                <img src="images/spacer.gif" width="20" alt="">
                                                                <asp:ImageButton Visible="false" ID="btnSubmit" runat="server" AlternateText="Submit Suggestion"
                                                                    ImageUrl="<%$Resources:Resource,img_btnSubmit%>" />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" id="feedbackTr">
                                                            <td align="left" width="35%">
                                                                <span class="tableHeader">Was this information helpful to you?</span>
                                                                <img src="images/spacer.gif" width="10" alt="">
                                                            </td>
                                                            <td align="left" valign="bottom">
                                                                <asp:ImageButton ID="imgYes" runat="server" ImageUrl="<%$Resources:Resource,img_btnYes%>"
                                                                    AlternateText="<%$Resources:Resource,el_yes%>" />
                                                                <img src="images/spacer.gif" width="5" alt="">
                                                                <asp:ImageButton ID="ImgNo" runat="server" ImageUrl="<%$Resources:Resource,img_btnNo%>"
                                                                    AlternateText="<%$Resources:Resource,el_no%>"></asp:ImageButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25" background="images/sp_right_bkgd.gif">
                    <img src="images/spacer.gif" height="5" width="25" alt="">
                </td>
            </tr>
        </table>
        </form>
    </center>
</body>
</html>
