Imports System.IO
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace ServicePLUSWebApp



    Partial Class TechBulletinGenInfo
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private downloadURL As String
        Private downloadPath As String
        Private conTechBulletinSubscriptionForm As String
        'Dim objUtilties As Utilities = New Utilities
        Public LanguageSM As String = "EN"


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Pass language to siteminder 
			If (HttpContextManager.SelectedLang IsNot Nothing) Then
				LanguageSM = HttpContextManager.SelectedLang.Substring(0, 2).ToUpper()
			End If


            If Not IsPostBack Then
                Try
                    errorMessageLabel.Text = ""
                    setFileDownloadPaths()
                    'check for file's physical presence only if the path was resolved successfully
                    If errorMessageLabel.Text = "" Then
                        Try
                            If conTechBulletinSubscriptionForm <> "" And File.Exists(downloadPath + "/" + conTechBulletinSubscriptionForm) Then
                                Me.refSubscriptionform.HRef = downloadURL + "/" + conTechBulletinSubscriptionForm
                                Me.refSubscriptionform.Target = "_blank"
                            End If
                        Catch ex As Exception
                            Me.errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                            Me.errorMessageLabel.Visible = True
                        End Try
                    End If

                    If Session.Item("customer") Is Nothing Then
                        Response.Redirect("sony-knowledge-base-search.aspx")
                    Else
                        If Not Session.Item("IsValidSubscription") Is Nothing Then
                            If CType(Session.Item("IsValidSubscription"), Boolean) = True Then
                                CloseAndRedirect()
                                Response.End()
                            End If
                        End If
                        Me.divContent.Visible = False
                        Me.divAltContent.Visible = True
                        Me.txtSubscriptionID.Visible = True
                        Me.txtPassword.Visible = True
                        Me.lblSubscriptionID.Visible = True
                        Me.lblPassword.Visible = True
                        Me.btnSearch.Visible = True
                        Me.lblSpace.Visible = True
                    End If
                Catch exThreas As System.Threading.ThreadAbortException
                    'do nothing, expected for response.redirect
                Catch ex As Exception
                    Me.errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                    Me.errorMessageLabel.Visible = True
                End Try
            End If
        End Sub
        Private Sub setFileDownloadPaths()
            Try
                downloadURL = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Url
                downloadPath = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.Physical
                conTechBulletinSubscriptionForm = Sony.US.SIAMUtilities.ConfigurationData.Environment.Download.TechBulletInform
            Catch ex As Exception
                Me.errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Me.errorMessageLabel.Visible = True
            End Try
        End Sub
        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            ' the validation for the subscription id is 2 fold
            ' 1. the id/password is available in the SubscriptionAuthorization table
            ' 2. the subscription id is in the current customer's profile only
            ' after successful validation update customer with the subscription id
            ' redirect to tech bulletin search page

            Try
                Dim customer As Customer = New Customer
                If Not Session.Item("customer") Is Nothing Then
                    customer = Session.Item("customer")
                Else
                    'simply return 
                    'general information page for users who have not logged in
                    Return
                End If
                Dim custMgr As CustomerManager = New CustomerManager
                Dim strSubscription As String
                Dim strPassword As String
                Dim IsValidSubscription As Boolean
                Dim redir As Boolean = False

                strSubscription = Me.txtSubscriptionID.Text.Trim()
                strPassword = Me.txtPassword.Text.Trim()

                If Not (strSubscription = "" Or strPassword = "") Then
                    Select Case (custMgr.IsValidSubscriptionID(customer.CustomerID, strSubscription))
                        Case 1
                            'OK
                        Case 2
                            Me.errorMessageLabel.Text = "The maximum number of ServicesPLUS users permitted for this subscription ID have already been registered. Please try another subscription ID."
                            Me.errorMessageLabel.Visible = True
                            Return
                        Case 3
                            Me.errorMessageLabel.Text = "Invalid subscription ID. Please try another subscription ID."
                            Me.errorMessageLabel.Visible = True
                            Return
                        Case Else

                    End Select
                    IsValidSubscription = custMgr.IsValidSubscriptionID(customer.CustomerID, strSubscription, strPassword)
                    If IsValidSubscription Then
                        customer.SubscriptionID = strSubscription
                        Try
                            custMgr.UpdateCustomerSubscription(customer)
                            Session.Item("IsValidSubscription") = "True"
                            'Response.Redirect("sony-techbulletin.aspx")
                            CloseAndRedirect()
                            Response.End()
                        Catch threadEx As System.Threading.ThreadAbortException
                            'do nothing
                        Catch ex As Exception
                            Me.errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                            Me.errorMessageLabel.Visible = True
                        End Try
                    Else
                        Me.errorMessageLabel.Text = "Invalid subscription details. Please retry."
                        Me.errorMessageLabel.Visible = True
                    End If
                End If
            Catch exThreadAbort As System.Threading.ThreadAbortException
                'do nothing
            Catch ex As Exception
                Me.errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Me.errorMessageLabel.Visible = True
            End Try

        End Sub
        Private Sub CloseAndRedirect()
            If Not Request.QueryString("post") Is Nothing Then
                Select Case Request.QueryString("post")
                    Case "kbs"
                        Response.Redirect("au-sony-knowledge-base-search.aspx")
                End Select
            ElseIf Not Session("LoginFrom") Is Nothing Then
                Select Case Session("LoginFrom").ToString()
                    Case "Bulletin"
                        If Not Session("RedirectTo") Is Nothing Then
                            Response.Redirect(Session("RedirectTo").ToString())
                        Else
                            Response.Redirect("au-sony-knowledge-base-search.aspx")
                        End If
                    Case Else
                        Exit Sub
                End Select
            Else
                Response.Redirect("member.aspx")
            End If
        End Sub
    End Class

End Namespace
