<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RepairStatus.ascx.vb"
    Inherits="ServicePLUSWebApp.RepairStatus" %>
<div style="border-style: solid; width: 100%; border-color: White">
    <table width="100%" border="0" role="presentation">
        <tr>
            <td>
                <table width="100%" border="0" role="presentation">
                    <tr>
                        <td id="tdHeader" runat="server" bgcolor="#d5dee9" width="30%">
                            <img src="images/spacer.gif" width="2" alt="">
                            <asp:Label ID="lblHeader" CssClass="tableHeader" runat="server" Text=""></asp:Label>
                        </td>
                        <td width="80%">
                        </td>
                    </tr>
                    <%--<tr>
                    <td bgcolor="#d5dee9" colspan="2" height="2" width="100%">
                    </td>
                </tr>--%>
                </table>
            </td>
            <tr>
                <td>
                    <div style="border-style: solid; border-color: #d5dee9; width: 100%;">
                        <table width="100%" border="0" role="presentation">
                            <tr>
                                <td width="5">
                                    <img src="images/spacer.gif" width="5" alt="" />
                                </td>
                                <td width="50%">
                                    <asp:Label ID="lblModel" CssClass="tableHeader" runat="server" Text="Model Number:"></asp:Label>
                                    <asp:Label ID="lblModelText" CssClass="bodycopy" runat="server" Text=""></asp:Label>
                                </td>
                                <td width="50%">
                                    <asp:Label ID="lblSerial" CssClass="tableHeader" runat="server" Text="Serial Number:"></asp:Label>
                                    <asp:Label ID="lblSerialText" CssClass="bodycopy" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="5">
                                    <img src="images/spacer.gif" width="5" alt="" />
                                </td>
                                <td width="100%" colspan="2">
                                    <asp:Label ID="lblStatus" CssClass="tableHeader" runat="server" Text="Repair Status:"></asp:Label>
                                    <asp:Label ID="lblStatusText" CssClass="bodycopy" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr id="trShipping" runat="server">
                                <td width="5">
                                    <img src="images/spacer.gif" width="5" alt="" />
                                </td>
                                <td width="100%" colspan="2">
                                    <%--<asp:Label ID="lblShipingInfo" CssClass="tableHeader" runat="server" Text="Shipping Info:"></asp:Label>--%>
                                    <%--8060--%>
                                    <asp:Label ID="lblShipingInfo" CssClass="tableHeader" runat="server" Text="Shipping Information:"></asp:Label>
                                    <asp:Label ID="lblShipingInfoText" CssClass="bodycopy" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="5">
                                    <img src="images/spacer.gif" width="5" alt="" />
                                </td>
                                <td width="100%" colspan="2" style="height: 19px">
                                    <asp:Label ID="lblEnquire" CssClass="bodycopy" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
    </table>
</div>
