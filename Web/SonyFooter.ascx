<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.SonyFooter"
    CodeFile="SonyFooter.ascx.vb" %>
<%-- The lines below help Visual Studio properly render CSS within controls, without including it in the final markup. --%>
<% If False Then %>
<link href = "includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
<% End If %>

<table style="width: 706px; text-align: center;" role="navigation">
    <tr>
        <td colspan="2" style="width: 710px; background: url('images/sp_int_footer_bkgrd.gif'); height: 42px;">
            <nav>
                &nbsp;<a class="legalLink" href="<%=urlProSite %>" target="_blank"
                    aria-label="Leave this site and visit the Sony Pro website."><%=Resources.Resource.footer_sony_profsnl_svs%></a>
                <% If isAmerica = True Then%>
                    &nbsp;|&nbsp;<a class="legalLink" href="http://products.sel.sony.com/SEL/legal/privacy.html" target="_blank"
                        aria-label="Visit the Privacy Policy page.">Privacy Policy/Your California Privacy Rights</a>
                <% End If %> 
                &nbsp;|&nbsp;<a class="legalLink" href="http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-other-legal.shtml" target="_blank"
                    aria-label="Visit the Legal and Trademark information page."><%=Resources.Resource.footer_legal_lm%></a>
                <span id="hideDuringMaintenance" runat="server">
                    &nbsp;|&nbsp;<a class="legalLink" href="TC2.aspx"
                        aria-label="Visit the Terms and Conditions page."><%=Resources.Resource.footer_terms_condition%></a>
                    &nbsp;|&nbsp;<a class="legalLink" href="WebUserGuide.aspx"
                        aria-label="Visit the website guide page."><%=Resources.Resource.footer_site_guide%></a>
                    &nbsp;|&nbsp;<a class="legalLink" href="Help.aspx"
                        aria-label="Visit the Help page for more information about how to use this site."><%=Resources.Resource.footer_help%></a>
                    &nbsp;|&nbsp;<a class="legalLink" href="sony-service-contacts.aspx"
                        aria-label="Visit the Contact page for information on who to contact for further support."><%=Resources.Resource.footer_contactus%></a>
                </span>
            </nav>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: bottom; text-align: left;">
            <span class="legalCopy"><%=Resources.Resource.footer_copy_right%>&nbsp;<asp:Label ID="lblYear" runat="server" class="legalCopy" />
                &nbsp;<%=Resources.Resource.footer_copy_msg%></span>
        </td>        
        <td style="vertical-align: bottom; text-align: right;">
            <span class="legalCopy"><%=Resources.Resource.footer_buildno%>
                <asp:Label ID="lblBuildNumber" runat="server" class="legalCopy" />
            </span>
        </td>
    </tr>
</table>
