<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_extended_warranties"
    EnableViewStateMac="true" CodeFile="sony-extended-warranties.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Extended Warranties for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <style type="text/css">
        .style1 {
            width: 100%;
            height: 5px;
        }

        .style2 {
            width: 201px;
            height: 5px;
        }
    </style>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="700">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="right" width="464" background="images/EWPageHeader.jpg"
                                                    bgcolor="#363d45" height="82">
                                                </td>
                                                <td valign="top" width="174" bgcolor="#363d45">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#f2f5f8">
                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Shop Extended Warranty</h2>
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                    <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">--%>
                                                </td>
                                                <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                    <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" colspan="2" width="100%" bgcolor="#f2f5f8">
                                                    <table width="100%" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                            <td>
                                                                <img height="5" src="images/spacer.gif" width="20" alt="">
                                                                <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" />
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true">
                                                                    <ContentTemplate>
                                                                        <table width="100%" border="0" role="presentation">
                                                                            <tr>
                                                                                <td valign="top" width="40%">
                                                                                    <table width="100%" border="0" role="presentation">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <span class="finderSubhead">Search by Sony Model Number</span><br />

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="15">
                                                                                                <asp:Label ID="Search1ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="12">
                                                                                                <span class="finderCopyDark">Full or partial Model Number:</span><%--<img height="12" src="images/sp_int_model_number_label.gif" width="64" alt="Full or partial Model Number:">--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <SPS:SPSTextBox ID="ewModelNumber" runat="server"></SPS:SPSTextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                                                                <asp:ImageButton ID="SearchModel" runat="server" ImageUrl="images/sp_int_submitGreybkgd_btn.gif"
                                                                                                    AlternateText="Search by Sony Model Number"></asp:ImageButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server"
                                                                                                    AssociatedUpdatePanelID="UpdatePanel2">
                                                                                                    <ProgressTemplate>
                                                                                                        <span class="bodyCopy">
                                                                                                            <br />
                                                                                                            Please wait...<img src="images/progbar.gif" alt="Please Wait..." /></span>
                                                                                                    </ProgressTemplate>
                                                                                                </asp:UpdateProgress>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td width="2%">
                                                                                    <img height="1" src="images/spacer.gif" width="9" alt="">
                                                                                </td>
                                                                                <td valign="top" width="60%">
                                                                                    <table width="100%" border="0" role="presentation">
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <span class="finderSubhead">Search by Extended Warranty Item Number or 
                                                                                            Description</span>

                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="15" class="style1">
                                                                                                <asp:Label ID="Search2ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <span class="finderCopyDark">Item number:</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <SPS:SPSTextBox ID="ewItemNumber" runat="server" CssClass="finderCopyDark"></SPS:SPSTextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style2">
                                                                                                <span class="finderCopyDark"></span>
                                                                                                <%--<img height="12" src="images/sp_int_partnumber_label.gif" width="58" alt="Part Number">--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <span class="finderCopyDark">Description or Keyword:</span><%--<img height="12" src="images/sp_int_description_label.gif" width="50" alt="Description or Keyword:">--%>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <SPS:SPSTextBox ID="ewDescription" runat="server" CssClass="finderCopyDark"></SPS:SPSTextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="style1">
                                                                                                <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                                                                <asp:ImageButton ID="SearchEW" runat="server" ImageUrl="images/sp_int_submitGreybkgd_btn.gif"
                                                                                                    AlternateText="Search by Extended Warranty Item Number or Description"></asp:ImageButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td width="418" bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td width="174" bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%--<asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label><asp:Label
                                    ID="NotFoundLabel" runat="server" CssClass="redAsterick"></asp:Label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                &nbsp;
                                            </td>
                                            <td width="670" height="20">
                                                <asp:UpdateProgress ID="UpdateProgress2" runat="server"
                                                    AssociatedUpdatePanelID="UpdatePanel1">
                                                    <ProgressTemplate>
                                                        <span class="bodyCopy">Please wait...<img src="images/progbar.gif" alt="Please Wait..." /></span>&nbsp;
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                            </td>
                                            <td width="20" height="20">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DataGrid ID="dgEW" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                            ForeColor="#333333" GridLines="None" Width="100%" AllowPaging="True" AllowSorting="True"
                                                            PageSize="8">
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                            <EditItemStyle BackColor="#2461BF" />
                                                            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="#333333" />
                                                            <PagerStyle BackColor="#D5DEE9" CssClass="tableHeader" HorizontalAlign="Left" Mode="NumericPages" />
                                                            <AlternatingItemStyle BackColor="White" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" />
                                                            <ItemStyle BackColor="#EFF3FB" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                Font-Strikeout="False" Font-Underline="False" />
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Model Number" SortExpression="Model_Name">
                                                                    <ItemTemplate>
                                                                        <a href="#" class="bodycopy" onclick="window.location.href='Sony-EW-ProductDetail.aspx?EW=<%#DataBinder.Eval(Container, "DataItem.EW_Code") %>&PC=<%#DataBinder.Eval(Container, "DataItem.MODEL_CODE") %>&MDL=<%#DataBinder.Eval(Container, "DataItem.MODEL_NAME") %>';return false;">
                                                                            <%#DataBinder.Eval(Container, "DataItem.Model_Name")%></a>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" HorizontalAlign="Left" />
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Item Number" SortExpression="EW_Code">
                                                                    <ItemTemplate>
                                                                        <a href="#" class="bodycopy" onclick="window.location.href='Sony-EW-ProductDetail.aspx?EW=<%#DataBinder.Eval(Container, "DataItem.EW_Code") %>&PC=<%#DataBinder.Eval(Container, "DataItem.MODEL_CODE") %>&MDL=<%#DataBinder.Eval(Container, "DataItem.MODEL_NAME") %>';return false;">
                                                                            <%#DataBinder.Eval(Container, "DataItem.EW_Code")%></a>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="20%" HorizontalAlign="Left" />
                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                        Font-Underline="False" HorizontalAlign="Left" />
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Short_Description" HeaderText="Description">
                                                                    <HeaderStyle Width="50%" />
                                                                    <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="List_Price" HeaderText="List Price" DataFormatString="{0:c}">
                                                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                                    <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="right" VerticalAlign="Middle" />
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                            <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" Font-Bold="True" />
                                                        </asp:DataGrid>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="SearchEW" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="SearchModel" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
