Imports System.Xml
Imports System.Xml.Xsl
Imports System.Text
Imports System.IO
Imports ServicesPlusException

Namespace ServicePLUSWebApp

    Partial Class ContactUs
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim transform As New XslTransform
            Dim xmlDoc As New XmlDocument()
            Dim sb As New StringBuilder
            Dim sw As New StringWriter(sb)
            Dim strSesion As String
            Dim xslPath As String
            Dim strBuildNum As String

            Try
                strSesion = IIf(HttpContextManager.Customer IsNot Nothing, "1", "0")
                strBuildNum = Application("Build").ToString()
                xmlDoc.LoadXml("<NewDataSet><CONTACT><MAINTENANCE>0</MAINTENANCE><SESSION>" + strSesion + "</SESSION><BUILDNUM>" + strBuildNum + "</BUILDNUM></CONTACT></NewDataSet>")
                If (HttpContextManager.SelectedLang = "fr-CA") Then
                    xslPath = Server.MapPath("ContactCA.xslt")
                Else
                    xslPath = Server.MapPath("Contact.xslt")
                End If
                transform.Load(xslPath)
                transform.Transform(xmlDoc, Nothing, sw, Nothing)
                tableContact.InnerHtml = sb.ToString()
                sw.Flush()
                sb.Length = 0
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
			End Try
        End Sub

    End Class

End Namespace
