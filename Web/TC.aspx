<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TC" EnableViewStateMac="true"
    CodeFile="TC.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Terms and Conditions</title>
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkSession() {
            if (document.getElementById("txtHidden").value === "load") {
                document.getElementById("txtHidden").value = "done";
            } else {
                location.href = "SessionExpired.aspx";
            }
        }
    </script>
    <style type="text/css">
        .style1 {
            height: 8px;
        }

        .style2 {
            height: 4px;
        }

        .style3 {
            height: 1024px;
        }
    </style>
</head>
<body style="margin: 0px; background: #5d7180; color: Black;" onload="javascript:checkSession();">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                bgcolor="#363d45" height="57">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.TC_hdl%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="txtHidden" type="hidden" value="load">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="600" border="0" class="bodyCopy" role="presentation">
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <br />
                                                <p style="line-height: 20px;" class="bodyCopyBold">
                                                    <%=Resources.Resource.tc_limpw_msg%>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td class="style1">
                                                <img height="25" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td class="style2">
                                                <p>
                                                    <a target="_blank" href="http://pro.sony.com/bbsc/ssr/services.servicesprograms.bbsccms-services-servicesprograms-warrantyinformation.shtml">
                                                        <%=Resources.Resource.help_cnt_hdrp_msg_12%></a>
                                                        <%=Resources.Resource.TC_Cnt1%>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <span class="bodyCopyBold">
                                                    <%=Resources.Resource.tc_part_msg%></span>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_Cnt2%></span>
                                                    <%=Resources.Resource.TC_Cnt3%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_Cnt4%></span>
                                                    &nbsp;<%=Resources.Resource.TC_Cnt5%>&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_Cnt6%></span>
                                                    &nbsp;<%=Resources.Resource.TC_Cnt7%>&nbsp;
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_Cnt8%></span>
                                                    <%=Resources.Resource.TC_Cnt9%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_Cnt10%></span>
                                                    <%=Resources.Resource.TC_Cnt11%>
                                                </p>
                                                <span class="legalCopy"><a class="legalLink"
                                                    href="http://pro.sony.com/bbsccms/services/files/servicesprograms/WARNTY2f.pdf"
                                                    target="_blank"><%=Resources.Resource.tc_lmtprtswrnty_msg%></a> </span>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <span class="bodyCopyBold"><%=Resources.Resource.tc_sftr_msg%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style3">
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td class="style3">
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_sftragrmnt_msg%></span>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_imprdcar_msg%></span>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_sftlic_msg%>&nbsp;</span>
                                                    <%=Resources.Resource.tc_sftlic_msg_1%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_grntoflic_msg%></span>
                                                    <%=Resources.Resource.tc_licrghts_msg%><br />
                                                    - <%=Resources.Resource.tc_licrghts_msg_1%><br />
                                                    - <%=Resources.Resource.tc_licrghts_msg_2%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_mpeg_msg%></span>
                                                    <%=Resources.Resource.tc_mpeg_msg_1%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_desclim_msg%></span><br />
                                                    - <%=Resources.Resource.tc_desclim_msg_1%><br />
                                                    - <%=Resources.Resource.tc_desclim_msg_2%><br />
                                                    - <%=Resources.Resource.tc_desclim_msg_3%><br />
                                                    - <%=Resources.Resource.tc_desclim_msg_4%><br />
                                                    - <%=Resources.Resource.tc_desclim_msg_5%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_cpyrght_msg%></span>
                                                    &nbsp;<%=Resources.Resource.tc_cpyrght_msg_1%>&nbsp;
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_prsprt_msg%></span>
                                                    <%=Resources.Resource.tc_prsprt_msg_1%>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_prsprt_msg_2%></span>
                                                    <%=Resources.Resource.tc_prsprt_msg_3%>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_exwarsft_msg%></span>
                                                    &nbsp;<%=Resources.Resource.tc_exwarsft_msg_1%>&nbsp;
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_limlia_msg%></span>
                                                    <%=Resources.Resource.tc_limlia_msg_1%>
                                                </p>
                                                <p>
                                                    &nbsp;
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td align="center">
                                                <span class="bodyCopyBold"><%=Resources.Resource.tc_training_msg%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img height="29" src="images/spacer.gif" width="15" alt="">
                                            </td>
                                            <td>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.tc_htltra_msg%>&nbsp;</span>
                                                    <%=Resources.Resource.tc_htltra_msg_1%>
                                                </p>
                                                <p>
                                                    <span class="bodyCopyBold"><%=Resources.Resource.TC_cnt12%>&nbsp;</span>
                                                    <%=Resources.Resource.tc_htltra_msg_2%>
                                                </p>
                                                <p>
                                                    <%=Resources.Resource.tc_htltra_msg_3%>
                                                </p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <br>
                                    <asp:ImageButton ID="Accept" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_17%>"
                                        AlternateText="Accept" />&nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="Decline" runat="server" ImageUrl="images/sp_int_decline_btn.gif"
                                        AlternateText="Decline" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
