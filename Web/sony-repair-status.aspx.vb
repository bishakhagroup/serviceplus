Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.Page
Imports System.Web.UI.WebControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System.Collections.Generic

Namespace ServicePLUSWebApp

    Partial Class sony_repair_status
        Inherits SSL
        'Dim objUtilties As Utilities = New Utilities
        Private lstConfModel As List(Of String)

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If ConfigurationManager.AppSettings("RepairSearchLoginRequired").ToString() = "1" Then
                    If Not Session.Item("customer") Is Nothing Then
                        Response.Redirect("sony-repair.aspx")
                    End If
                End If
                If Not IsPostBack Then
                    ScriptManager1.RegisterAsyncPostBackControl(btnNewSearch)
                    ScriptManager1.RegisterAsyncPostBackControl(btnSearch)
                    Dim blnIsContextAvailable As Boolean = False
                    If Not Context.Items("Model") Is Nothing Then
                        ModelNumber.Text = Context.Items("Model").ToString()
                        blnIsContextAvailable = True
                    End If
                    If Not Context.Items("Serial") Is Nothing Then
                        SerialNumber.Text = Context.Items("Serial").ToString()
                        blnIsContextAvailable = True
                    End If
                    If Not Context.Items("Phone") Is Nothing Then
                        PhoneNumber.Text = Context.Items("Phone").ToString()
                        blnIsContextAvailable = True
                    End If
                    If Not Context.Items("Notification") Is Nothing Then
                        NotificationNumber.Text = Context.Items("Notification").ToString()
                        blnIsContextAvailable = True
                    End If
                    If blnIsContextAvailable = True Then
                        btnSearch_Click(sender, Nothing)
                    End If
                    'If Not Session("SesisonSearchResultCollection") Is Nothing Then
                    '    BindSearchResult(CType(Session("SesisonSearchResultCollection"), ArrayList))
                    'End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
            Try
                Dim objSearchResultCollection As ArrayList
                If ValidateSearchCriteria() = False Then Exit Sub
                objSearchResultCollection = GetSearchResult()
                If Not objSearchResultCollection Is Nothing Then
                    BindSearchResult(objSearchResultCollection)
                    'Changes made by Sneha on 16/07/2014 
                    'checking the count of resultant records before showing the popup that shows conflicted models. i.e. show pop up only if we have data in parent table
                    If objSearchResultCollection.Count > 0 Then
                        callPopUp(lstConfModel)
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub callPopUp(ByVal lstConflicts As List(Of String))
            If lstConflicts.Count > 0 Then
                Dim strModels As String = String.Empty
                For Each item In lstConflicts
                    strModels = strModels + item + ","
                Next
                If Not String.IsNullOrEmpty(strModels) Then strModels = strModels.Remove(strModels.Length - 1, 1)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), _
                "showPopup", "javascript:OrderLookUp(" + _
                String.Format("{0},'{1}'", lstConflicts.Count, strModels) + ");", True)
            End If
        End Sub

        Private Function ValidateSearchCriteria() As Boolean
			lblError.Visible = False

			If (Not IsValidString(ModelNumber.Text) OrElse (Not IsValidString(SerialNumber.Text)) OrElse (Not IsValidString(NotificationNumber.Text)) OrElse (Not IsValidString(PhoneNumber.Text))) Then
				lblError.Visible = True
				lblError.Text = "Illegal characters detected. Please try a different search."
				Return False
			End If

			If ModelNumber.Text = String.Empty And SerialNumber.Text = String.Empty And NotificationNumber.Text = String.Empty And PhoneNumber.Text = String.Empty Then
				lblError.Visible = True
				lblError.Text = "Please enter search criteria."
				Return False
			End If

			If (ModelNumber.Text <> String.Empty And SerialNumber.Text = String.Empty) Then	'And (NotificationNumber.Text <> String.Empty Or PhoneNumber.Text <> String.Empty) Then
				lblError.Visible = True
				lblError.Text = GetGlobalResourceObject("Resource", "repair_stus_val_msg_1")
				If NotificationNumber.Text <> String.Empty Or PhoneNumber.Text <> String.Empty Then
					Return True
				Else
					Return False
				End If
			End If

			If (ModelNumber.Text = String.Empty And SerialNumber.Text <> String.Empty) Then	' And (NotificationNumber.Text <> String.Empty Or PhoneNumber.Text <> String.Empty) Then
				lblError.Visible = True
				lblError.Text = GetGlobalResourceObject("Resource", "repair_stus_val_msg_2")
				If NotificationNumber.Text <> String.Empty Or PhoneNumber.Text <> String.Empty Then
					Return True
				Else
					Return False
				End If
			End If

			Return True
		End Function

        Private Function GetSearchResult() As ArrayList
			Dim xModelNumber As String = ModelNumber.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()
			Dim xSerialNumber As String = SerialNumber.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()
			Dim xNotificationNumber As String = NotificationNumber.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()
			Dim xCustomerPhoneNumber As String = PhoneNumber.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()

			While xModelNumber.StartsWith("0")
				xModelNumber = xModelNumber.Substring(1, xModelNumber.Length - 1)
			End While
			While xSerialNumber.StartsWith("0")
				xSerialNumber = xSerialNumber.Substring(1, xSerialNumber.Length - 1)
			End While
			While xNotificationNumber.StartsWith("0")
				xNotificationNumber = xNotificationNumber.Substring(1, xNotificationNumber.Length - 1)
			End While
			While xCustomerPhoneNumber.StartsWith("0")
				xCustomerPhoneNumber = xCustomerPhoneNumber.Substring(1, xCustomerPhoneNumber.Length - 1)
			End While
			For iCounter As Int16 = 0 To xCustomerPhoneNumber.Length - 1
				If IsNumeric(xCustomerPhoneNumber.Substring(iCounter, 1)) = False Then xCustomerPhoneNumber = xCustomerPhoneNumber.Replace(xCustomerPhoneNumber.Substring(iCounter, 1), "~")
			Next
			xCustomerPhoneNumber = xCustomerPhoneNumber.Replace("~", "")
			PhoneNumber.Text = xCustomerPhoneNumber
			If xCustomerPhoneNumber.Length > 10 Then
				xCustomerPhoneNumber = xCustomerPhoneNumber.Substring(0, 10)
			End If

			Dim xCUSTOMERID As Integer = "-1"
			Dim xCUSTOMERSEQUENCENUMBER As Integer = "-1"
			If Not Session.Item("customer") Is Nothing Then
				xCUSTOMERID = Convert.ToInt32(CType(Session.Item("customer"), Customer).CustomerID)
				xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(CType(Session.Item("customer"), Customer).SequenceNumber)
			End If
			Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
			Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
			Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
			Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")
			If xHTTP_URL = "" Then
				xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
			End If

			Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

			'Dim objGlobalData As New GlobalData

			'If Not Session.Item("GlobalData") Is Nothing Then
			'    objGlobalData = Session.Item("GlobalData")
			'End If

			Dim objCatalogManager As New CatalogManager()
			Return objCatalogManager.SearchRepairStatus(xModelNumber, xSerialNumber, xNotificationNumber, xCustomerPhoneNumber, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT, lstConfModel, HttpContextManager.GlobalData)
		End Function

        Private Sub BindSearchResult(ByVal objSearchResultCollection As ArrayList)
            Dim objSesisonSearchResultCollection As ArrayList = New ArrayList()
            'objSesisonSearchResultCollection = CType(Session("SesisonSearchResultCollection"), ArrayList)

            Dim objRepairControl As ServicePLUSWebApp.RepairStatus

            'If Not objSesisonSearchResultCollection Is Nothing Then
            '    For Each objRepairStatus As Sony.US.ServicesPLUS.Core.RepairStatus In objSesisonSearchResultCollection
            '        objRepairControl = CType(Page.LoadControl("RepairStatus.ascx"), ServicePLUSWebApp.RepairStatus)
            '        objRepairControl.Model = objRepairStatus.MODELNUMBER
            '        objRepairControl.Serial = objRepairStatus.SERIALNUMBER
            '        objRepairControl.Status = objRepairStatus.STATUS
            '        objRepairControl.Enquire = "For More Information call on " + objRepairStatus.PLANTPHONE + " or email to <A href='mailto:" + objRepairStatus.PLANTEMAILADDRESS + "'>" + objRepairStatus.PLANTEMAILADDRESS + "</A>"
            '        objRepairControl.Header = "Notification Number: " + objRepairStatus.NOTIFICATIONNUMBER
            '        objRepairControl.CarrierCode = objRepairStatus.CARRIERCODE
            '        objRepairControl.TrackingNumber = objRepairStatus.TRACKINGNUMBER
            '        objRepairControl.CarrierURL = objRepairStatus.TRACKINGURL.Replace("TRACKINGNUMBER", objRepairControl.TrackingNumber)
            '        statusControlPlaceHolder.Controls.AddAt(0, objRepairControl)
            '    Next
            'End If

            If objSearchResultCollection.Count > 0 Then
                btnNewSearch.Visible = True
                statusControlPlaceHolder.Controls.Clear()
                For Each objRepairStatus As Sony.US.ServicesPLUS.Core.RepairStatus In objSearchResultCollection
                    objRepairControl = CType(Page.LoadControl("RepairStatus.ascx"), ServicePLUSWebApp.RepairStatus)
                    objRepairControl.Model = IIf(objRepairStatus.OLDMODELNUMBER IsNot String.Empty, objRepairStatus.MODELNUMBER + String.Format("<strong>&nbsp;(formerly known as {0})</strong>", objRepairStatus.OLDMODELNUMBER), objRepairStatus.MODELNUMBER)
                    objRepairControl.Serial = objRepairStatus.SERIALNUMBER
                    objRepairControl.Status = objRepairStatus.STATUS
                    objRepairControl.Enquire = Resources.Resource.repair_stus_info_msg + objRepairStatus.PLANTPHONE + Resources.Resource.repair_stus_info_msg_1 + "<A href='mailto:" + objRepairStatus.PLANTEMAILADDRESS + "'>" + objRepairStatus.PLANTEMAILADDRESS + "</A>"
                    objRepairControl.Header = Resources.Resource.repair_stus_notino_msg + objRepairStatus.NOTIFICATIONNUMBER
                    objRepairControl.CarrierCode = objRepairStatus.CARRIERCODE.Replace("-", "")
                    objRepairControl.TrackingNumber = objRepairStatus.TRACKINGNUMBER
                    objRepairControl.CarrierURL = objRepairStatus.TRACKINGURL.Replace("TRACKINGNUMBER", objRepairControl.TrackingNumber)
                    'objRepairControl.LatestSearch = True
                    statusControlPlaceHolder.Controls.AddAt(0, objRepairControl)
                    If objSesisonSearchResultCollection Is Nothing Then objSesisonSearchResultCollection = New ArrayList()
                    objSesisonSearchResultCollection.Add(objRepairStatus)
                Next
            Else
                lblError.Visible = True
                lblError.Text = Resources.Resource.repair_error_msg
            End If
            'Session("SesisonSearchResultCollection") = objSesisonSearchResultCollection
        End Sub

        'Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSearch.Click
        '    statusControlPlaceHolder.Controls.Clear()
        '    Session("SesisonSearchResultCollection") = Nothing
        'End Sub

        Protected Sub btnNewSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
            ModelNumber.Text = String.Empty
            SerialNumber.Text = String.Empty
            NotificationNumber.Text = String.Empty
            PhoneNumber.Text = String.Empty
            statusControlPlaceHolder.Controls.Clear()
            ModelNumber.Focus()
            lblError.Visible = False
        End Sub
    End Class
End Namespace
