<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Online-Product-Registration.aspx.vb"
    Inherits="ServicePLUSWebApp.Online_Product_Registration" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="~/ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SPSPromotion" Src="~/UserControl/SPSPromotion.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Product registration </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/s_code.js" type="text/javascript"></script>
    <script src="includes/Prepopulate.js" type="text/javascript"></script>
    <script src="includes/Block_Array_HideShowClear.js" type="text/javascript"></script>

    <style type="text/css">
        .f12 {
            font-size: 12px;
            color: #474747;
            font-family: arial,helvetica,verdana,sans-serif;
        }

        .style1 {
            height: 27px;
        }
    </style>

    <script type="text/javascript">
        s.pageName = "bbsc:prodreg:step1";
        s.channel = "bbsc:prodreg";
        s.prop2 = "bbsc:prodreg:step1";
        s.prop3 = "model:registration";
        s.eVar1 = "bbsc:prodreg";
        s.eVar2 = "bbsc:prodreg:step1";

        var s_code = s.t(); if (s_code) document.write(s_code);
    </script>

    <script type="text/javascript">
        var isNN = (navigator.appName.indexOf("Netscape") !== -1);
        function autoTab(input, len, e) {
            var keyCode = (isNN) ? e.which : e.keyCode;
            var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];
            if (input.value.length >= len && !containsElement(filter, keyCode)) {
                input.value = input.value.slice(0, len);
                input.form[(getIndex(input) + 1) % input.form.length].focus();
            }
            function containsElement(arr, ele) {
                var found = false, index = 0;
                while (!found && index < arr.length)
                    if (arr[index] === ele)
                        found = true;
                    else
                        index++;
                return found;
            }
            function getIndex(input) {
                var index = -1, i = 0, found = false;
                while (i < input.form.length && index === -1)
                    if (input.form[i] === input) index = i;
                    else i++;
                return index;
            }
            return true;
        }

        function dynamic_OCCO(val) {
            if (val === "Other") {
                Block_Array_HideShowClear(new Array("OCCOid"), new Array(""), "show");
            } else {
                Block_Array_HideShowClear(new Array("OCCOid"), new Array("OCCO"), "hide");
            }
        }
    </script>

    <script type="text/javascript">
        function list_from_arrays(VisibleArray, ValueArray, ListName, FormName) {
            if (FormName === null) {
                FormName = "forms[0]";
            } else {
                FormName = "forms[" + FormName + "]";
            }
            ShortName = eval("document." + FormName + "." + ListName);
            if (VisibleArray.length !== ValueArray.length) {
                alert("Visible and Value arrays are not of equal size");
            } else {
                ShortName.options.length = 0;
                for (i = 0; i < ValueArray.length; i++) {
                    ShortName.options[i] = new Option(VisibleArray[i], ValueArray[i]);
                }
            }
        }

        function list_from_assoc_arrays(Assoc_Array, ListName, FormName) {
            //uses an associative array for create dropdowns
            if (FormName === null) {
                FormName = "forms[0]";
            } else {
                FormName = "forms['" + FormName + "']";
            }
            ShortName = eval("document." + FormName + "." + ListName);
            ShortName.options.length = 0;
            var i = 0;
            for (var assoc_name in Assoc_Array) {
                ShortName.options[i] = new Option(assoc_name, Assoc_Array[assoc_name]);
                i++;
            }
        }
    </script>

    
    <script type="text/javascript">
    //global variable declaration
    var ff = 0;   //used as a flag
    var submitcount = 0;  //used to keep the form from being submitted multiple times
    var req_edt = "The selected field must be filled in.";
    var req_sel = "A selection from the list must be made.";
    var req_gen = "All required fields must be completed before moving to the next page.";
    var req_num_only = "The selected field accepts numbers only.";
    var req_min_size = "The selected field requires at least 5 digits.";
    var req_min_is_max = "The selected field must be completely filled in.";
    var req_valid_email = "A valid E-mail address is required. Example: someone@mydomain.com";
    var req_matched_email = "Please enter and confirm your e-mail address and make sure they match.";
    var req_date = "The date you selected is invalid. Please select a valid date.";
    var req_good_zip = "Please enter a valid ZIP code.  Example: NNNNN or NNNNN-NNNN";
    var req_good_phone = "Please enter your 10 digit phone number.";
    var req_good_fax = "Your fax number should look like NNN-NNN-NNNN.";
    var req_other_edit = "You selected other.  Please specify.";

    var click_to_select = new Object();
    click_to_select["Select a primary use above before proceeding."] = "";
    var BroadcastCable = new Object();
    BroadcastCable["Click to Select"] = "";
    BroadcastCable["Broadcast/Cable - General"] = "A0 Broadcast/Cable - General";
    BroadcastCable["Broadcast/Cable - Broadcast Networks"] = "A1 Broadcast/Cable - Broadcast Networks";
    BroadcastCable["Broadcast/Cable - Broadcast Stations"] = "A2 Broadcast/Cable - Broadcast Stations";
    BroadcastCable["Broadcast/Cable - Cable"] = "A3 Broadcast/Cable - Cable";
    BroadcastCable["Broadcast/Cable - IPTV"] = "A4 Broadcast/Cable - IPTV";
    BroadcastCable["Broadcast/Cable - PBS"] = "A5 Broadcast/Cable - PBS";

    var Corporate = new Object();
    Corporate["Click to Select"] = "";
    Corporate["Corporate - General"] = "B0 Corporate - General";
    Corporate["Corporate - Advertising"] = "B1 Corporate - Advertising";
    Corporate["Corporate - Entertainment"] = "B2 Corporate - Entertainment";
    Corporate["Corporate - Financial"] = "B3 Corporate - Financial";
    Corporate["Corporate - Healthcare"] = "B4 Corporate - Healthcare";
    Corporate["Corporate - Manufacturing"] = "B5 Corporate - Manufacturing";
    Corporate["Corporate - Real Estate"] = "B6 Corporate - Real Estate";
    Corporate["Corporate - Enterprize 500+"] = "B7 Corporate - Enterprize 500+";
    Corporate["Corporate - med 25-500"] = "B8 Corporate - med 25-500";
    Corporate["Corporate - small 0-25"] = "B9 Corporate - small 0-25";

    var DigitalCinema = new Object();
    DigitalCinema["Click to Select"] = "";
    DigitalCinema["Digital Cinema - General"] = "V0 Digital Cinema - General";
    DigitalCinema["Digital Cinema - Exhibition"] = "V1 Digital Cinema - Exhibition";
    DigitalCinema["Digital Cinema - Studios"] = "V2 Digital Cinema - Studios";
    DigitalCinema["Digital Cinema - Installers"] = "V3 Digital Cinema - Installers";
    DigitalCinema["Digital Cinema - Industry associations and Affiliates"] = "V4 Digital Cinema - Industry associations and Affiliates";
    DigitalCinema["Digital Cinema - Service Providers"] = "V5 Digital Cinema - Service Providers";

    var DigitalCinematography = new Object();
    DigitalCinematography["Click to Select"] = "";
    DigitalCinematography["Digital Cinematography - General"] = "C0 Digital Cinematography - General";
    DigitalCinematography["Digital Cinematography - Cinema Rental Company"] = "C1 Digital Cinematography - Cinema Rental Company";
    DigitalCinematography["Digital Cinematography - Independent Producers"] = "C2 Digital Cinematography - Independent Producers";
    DigitalCinematography["Digital Cinematography - Major Studios"] = "C3 Digital Cinematography - Major Studios";

    var DigitalPhotography = new Object();
    DigitalPhotography["Click to Select"] = "";
    DigitalPhotography["Digital Photography - General"] = "D0 Digital Photography - General";
    DigitalPhotography["Digital Photography -Event Photography"] = "D1 Digital Photography -Event Photography";
    DigitalPhotography["Digital Photography -Passport & Photo Printing"] = "D2 Digital Photography -Passport & Photo Printing";
    DigitalPhotography["Digital Photography -Portrait Photography"] = "D3 Digital Photography -Portrait Photography";
    DigitalPhotography["Digital Photography -Retail Photo"] = "D4 Digital Photography -Retail Photo";

    var Education = new Object();
    Education["Click to Select"] = "";
    Education["Education - General"] = "E0 Education - General";
    Education["Education - Film School"] = "E1 Education - Film School";
    Education["Education - Higher Education"] = "E2 Education - Higher Education";
    Education["Education - K-12"] = "E3 Education - K-12";

    var EventVideography = new Object();
    EventVideography["Click to Select"] = "";
    EventVideography["Event Videography - General"] = "F0 Event Videography - General";

    var Government = new Object();
    Government["Click to Select"] = "";
    Government["Government - General"] = "G0 Government - General";
    Government["Government - Federal"] = "G1 Government - Federal";
    Government["Government - Local"] = "G2 Government - Local";
    Government["Government - State"] = "G3 Government - State";

    var Hospitality = new Object();
    Hospitality["Click to Select"] = "";
    Hospitality["Hospitality - General"] = "H0 Hospitality - General";
    Hospitality["Hospitality - Casino Hotels"] = "H1 Hospitality - Casino Hotels";
    Hospitality["Hospitality - Casinos w/o Hotel"] = "H2 Hospitality - Casinos w/o Hotel";
    Hospitality["Hospitality - Cruise Ships"] = "H3 Hospitality - Cruise Ships";
    Hospitality["Hospitality - Hotels/Resorts"] = "H4 Hospitality - Hotels/Resorts";
    Hospitality["Hospitality - Restaurants/Bars"] = "H5 Hospitality - Restaurants/Bars";

    var Medical = new Object();
    Medical["Click to Select"] = "";
    Medical["Medical - General"] = "I0 Medical - General";
    Medical["Medical - Surgical"] = "I1 Medical - Surgical";
    Medical["Medical -Radiology"] = "I2 Medical -Radiology";

    var OutsideBroadcast = new Object();
    OutsideBroadcast["Click to Select"] = "";
    OutsideBroadcast["Outside Broadcast - General"] = "J0 Outside Broadcast - General";

    var PostProduction = new Object();
    PostProduction["Click to Select"] = "";
    PostProduction["Post Production - General"] = "K0 Post Production - General";
    PostProduction["Post Production - Hollywood Post"] = "K1 Post Production - Hollywood Post";
    PostProduction["Post Production - Non Hollywood Post"] = "K2 Post Production - Non Hollywood Post";

    var Production = new Object();
    Production["Click to Select"] = "";
    Production["Production - General"] = "L0 Production - General";

    var Religious = new Object();
    Religious["Click to Select"] = "";
    Religious["Religious - General"] = "M0 Religious - General";
    Religious["Religious - Broadcasters"] = "M1 Religious - Broadcasters";
    Religious["Religious - HOW large 800-2000 attendees"] = "M2 Religious - HOW large 800-2000 attendees";
    Religious["Religious - HOW mega >2000 attendees"] = "M3 Religious - HOW mega >2000 attendees";
    Religious["Religious - HOW small/med  <800 attendees"] = "M4 Religious - HOW small/med  <800 attendees";

    var Reseller = new Object();
    Reseller["Click to Select"] = "";
    Reseller["Reseller - General"] = "N0 Reseller - General";
    Reseller["Reseller - One Shot only"] = "N1 Reseller - One Shot only business";
    Reseller["Reseller - Distribution"] = "N2 Reseller - Distribution";

    var Security = new Object();
    Security["Click to Select"] = "";
    Security["Security - General"] = "O0 Security - General";
    Security["Security - Commercial "] = "O1 Security - Commercial ";
    Security["Security - Retail"] = "O2 Security - Retail";
    Security["Security - Transportation"] = "O3 Security - Transportation";

    var Sports = new Object();
    Sports["Click to Select"] = "";
    Sports["Sports - General"] = "P0 Sports - General";
    Sports["Sports - Leagues"] = "P1 Sports - Leagues";
    Sports["Sports - Teams: Broadcast Solutions"] = "P2 Sports - Teams: Broadcast Solutions";
    Sports["Sports - Teams:Coaching Solutions"] = "P3 Sports - Teams:Coaching Solutions";
    Sports["Sports - Venues"] = "P4 Sports - Venues";

    var SXRD = new Object();
    SXRD["Click to Select"] = "";
    SXRD["SXRD- General"] = "Q0 SXRD- General";
    SXRD["SXRD -Automotive "] = "Q1 SXRD -Automotive ";
    SXRD["SXRD- Energy"] = "Q2 SXRD- Energy";
    SXRD["SXRD- Entertainment/Attractions"] = "Q3 SXRD- Entertainment/Attractions";
    SXRD["SXRD- Museums/Planetariums "] = "Q4 SXRD- Museums/Planetariums ";
    SXRD["SXRD- Science/Research"] = "Q5 SXRD- Science/Research";
    SXRD["SXRD- Simulation"] = "Q6 SXRD- Simulation";

    var SystemsIntegrator = new Object();
    SystemsIntegrator["Click to Select"] = "";
    SystemsIntegrator["Systems Integrator - General"] = "R0 Systems Integrator - General";

    var VideoRentalCompanies = new Object();
    VideoRentalCompanies["Click to Select"] = "";
    VideoRentalCompanies["Video Rental Companies - General"] = "S0 Video Rental Companies - General";

    var Videoconferencing = new Object();
    Videoconferencing["Click to Select"] = "";
    Videoconferencing["Videoconferencing - General"] = "T0 Videoconferencing - General";

    var VisualImaging = new Object();
    VisualImaging["Click to Select"] = "";
    VisualImaging["Visual Imaging - General"] = "U0 Visual Imaging - General";
    VisualImaging["Visual Imaging - Bioscience/Microscopy"] = "U1 Visual Imaging - Bioscience/Microscopy";
    VisualImaging["Visual Imaging - Industrial Automation"] = "U2 Visual Imaging - Industrial Automation";
    VisualImaging["Visual Imaging - Lab Automation"] = "U3 Visual Imaging - Lab Automation";
    VisualImaging["Visual Imaging - Machine Vision"] = "U4 Visual Imaging - Machine Vision";
    VisualImaging["Visual Imaging - Robotics"] = "U5 Visual Imaging - Robotics";
    VisualImaging["Visual Imaging - Traffic & Mobile Police"] = "U6 Visual Imaging - Traffic & Mobile Police";

    function change_occu(occu) {
        switch (occu) {
            case "": list_from_assoc_arrays(click_to_select, 'OCCU'); break;
            case "Broadcast/Cable": list_from_assoc_arrays(BroadcastCable, 'OCCU'); break;
            case "Corporate": list_from_assoc_arrays(Corporate, 'OCCU'); break;
            case "Digital Cinema": list_from_assoc_arrays(DigitalCinema, 'OCCU'); break;
            case "Digital Cinematography": list_from_assoc_arrays(DigitalCinematography, 'OCCU'); break;
            case "Digital Photography": list_from_assoc_arrays(DigitalPhotography, 'OCCU'); break;
            case "Education": list_from_assoc_arrays(Education, 'OCCU'); break;
            case "Event Videography": list_from_assoc_arrays(EventVideography, 'OCCU'); break;
            case "Government": list_from_assoc_arrays(Government, 'OCCU'); break;
            case "Hospitality": list_from_assoc_arrays(Hospitality, 'OCCU'); break;
            case "Medical": list_from_assoc_arrays(Medical, 'OCCU'); break;
            case "Outside Broadcast": list_from_assoc_arrays(OutsideBroadcast, 'OCCU'); break;
            case "Post Production": list_from_assoc_arrays(PostProduction, 'OCCU'); break;
            case "Production": list_from_assoc_arrays(Production, 'OCCU'); break;
            case "Religious": list_from_assoc_arrays(Religious, 'OCCU'); break;
            case "Reseller": list_from_assoc_arrays(Reseller, 'OCCU'); break;
            case "Security": list_from_assoc_arrays(Security, 'OCCU'); break;
            case "Sports": list_from_assoc_arrays(Sports, 'OCCU'); break;
            case "SXRD": list_from_assoc_arrays(SXRD, 'OCCU'); break;
            case "Systems Integrator": list_from_assoc_arrays(SystemsIntegrator, 'OCCU'); break;
            case "Video Rental Companies": list_from_assoc_arrays(VideoRentalCompanies, 'OCCU'); break;
            case "Videoconferencing": list_from_assoc_arrays(Videoconferencing, 'OCCU'); break;
            case "Visual Imaging": list_from_assoc_arrays(VisualImaging, 'OCCU'); break;
        }
        selectDropDown('OCCU', occu, 'form1');
    }

    function vishideshow(id, act) {
        if (document.getElementById && document.getElementById(id)) { // is W3C-DOM
            if (act === 'show') eval("document.getElementById(id).style.visibility='visible';");
            if (act === 'hide') eval("document.getElementById(id).style.visibility='hidden';");
        }
        else if (document.all) { // is IE
            if (act === 'show') eval("document.all." + id + ".style.visibility='visible';");
            if (act === 'hide') eval("document.all." + id + ".style.visibility='hidden';");
        }
        else if (document.layers) { // is NS
            if (act === 'show') eval("document.layers['" + id + "'].visibility='show';");
            if (act === 'hide') eval("document.layers['" + id + "'].visibility='hide';");
        }
    }

    function blockhideshow(id, act) {
        if (document.getElementById && document.getElementById(id)) { // is W3C-DOM
            if (act === 'show') eval("document.getElementById(id).style.visibility='visible';document.getElementById(id).style.display='';");
            if (act === 'hide') eval("document.getElementById(id).style.visibility='hidden';document.getElementById(id).style.display='none';");
        }
        else if (document.all) { // is IE
            if (act === 'show') eval("document.all." + id + ".style.visibility='visible';document.all." + id + ".style.display='block';");
            if (act === 'hide') eval("document.all." + id + ".style.visibility='hidden';document.all." + id + ".style.display='none';");
        }
        else if (document.layers) { // is NS
            if (act === 'show') eval("document.layers['" + id + "'].visibility='show';");
            if (act === 'hide') eval("document.layers['" + id + "'].visibility='hide';");
        }
    }

    //isWhitespace() gets called in the validator function. It checks to see if whitespace was entered into the edit field.
    function isWhitespace(s) {
        var i;
        var whitespace = " \t\n\r";

        // Search through string's characters one by one
        // until we find a non-whitespace character.
        // When we do, return false; if we don't, return true.

        for (i = 0; i < s.length; i++) {
            // Check that current character isn't whitespace.
            var c = s.charAt(i);

            if (whitespace.indexOf(c) === -1) return false;
        }

        // All characters are whitespace.
        return true;
    }

    function hasWhitespace(s) {
        var whitespace = " \t\n\r";
        for (var i = 0; i < s.length; i++) {
            // See if the current character is whitespace.
            if (whitespace.indexOf(s.charAt(i)) !== -1) return true;
        }
        return false;
    }

    var fields = {
        FNAM: '',
        LNAM: '',
        COMP: '',
        ADD1: '',
        CITY: '',
        STAT: '',
        ZIPC: '',
        PHAC: '',
        PHPR: '',
        PHNM: '',
        FXAC: '',
        FXPR: '',
        FXNM: '',
        EMAL: '',
        CMAL: '',
        PUSE: '',
        OCCU: '',
        OCCO: ''
    };

    function validate(form1) {
        if (submitcount === 0) {
            ++submitcount;
            var obj;
            var error = '';
            var br = "\n";

            for (var field in fields) {
                eval("obj = document.form1.elements[field]");
                if (typeof (obj) === "undefined") {
                    continue;
                } else if (obj.type === "text") {
                    if (field === "OCCO") {
                        if (document.form1.OCCU.options[document.form1.OCCU.selectedIndex].value === "Other" && obj.value === "") error = 'empty_other_edit';
                    } else if (field === "FXAC" || field === "FXPR" || field === "FXNM") {
                        if (document.form1.FXAC.value !== "" || document.form1.FXPR.value !== "" || document.form1.FXNM.value !== "") {
                            if ((field === "FXAC" || field === "FXPR") && obj.value.length !== 3) error = 'bad_fax';
                            if (field === "FXNM" && obj.value.length !== 4) error = 'bad_fax';
                            if ((field === "FXAC" || field === "FXPR" || field === "FXNM") && isNaN(obj.value) === true) error = 'nonnumber';
                            if (field === "FXAC" && obj.value.length !== 3 && document.form1.FXPR.value !== "" && document.form1.FXNM.value !== "") error = 'bad_fax';
                            if (field === "FXPR" && obj.value.length !== 3 && document.form1.FXAC.value !== "" && document.form1.FXNM.value !== "") error = 'bad_fax';
                            if (field === "FXNM" && obj.value.length !== 4 && document.form1.FXAC.value !== "" && document.form1.FXPR.value !== "") error = 'bad_fax';
                        }
                    } else if ((field === "PHAC" || field === "PHPR") && obj.value.length !== 3) error = 'bad_phone';
                    else if (field === "ZIPC" && (obj.value.length !== 5 && obj.value.length !== 10)) error = 'bad_zip';
                    else if (field === "ZIPC" && obj.value.length === 5 && isNaN(obj.value) === true) error = 'bad_zip';
                    else if (field === "ZIPC" && obj.value.length === 10 && obj.value.indexOf("-", 5) === -1) error = 'bad_zip';
                    else if (field === "ZIPC" && obj.value.length === 10 && isNaN(obj.value.substring(0, 4)) === true) error = 'bad_zip';
                    else if (field === "ZIPC" && obj.value.length === 10 && isNaN(obj.value.substring(6)) === true) error = 'bad_zip';
                    else if (field === "PHNM" && obj.value.length !== 4) error = 'bad_phone';
                    else if ((field === "PHAC" || field === "PHPR" || field === "PHNM") && isNaN(obj.value) === true) error = 'nonnumber';
                    else if (field === "EMAL" && (obj.value.indexOf("@", 0) === -1 || obj.value.indexOf(".", 0) === -1 || hasWhitespace(obj.value))) error = 'bademail';
                    else if (field === "CMAL" && (obj.value !== document.form1.EMAL.value)) error = 'nonmatchedemail';
                    else if (obj.value === "") {
                        switch (field) {
                            default:
                                {
                                    error = 'missingedt';
                                    break;
                                }
                        }
                    }
                } else if (obj.type === "select-one") {
                    if (obj.selectedIndex <= -1 || obj.options[obj.selectedIndex].value === "") {
                        //alert(obj.name);
                        error = 'missingsel';
                    }
                } else if (obj[0] && obj[0].type === "radio") {
                    var foundchecked = 0;
                    for (var i = 0; i < obj.length; i++) {
                        if (obj[i].checked) {
                            foundchecked = 1;
                            break;
                        }
                    }
                    if (!foundchecked) {
                        switch (field) {
                            default:
                                {
                                    error = 'missingradio';
                                    break;
                                }
                        }
                    }
                }

                if (error) {
                    //alert(document.getElementById('errorMessageLabel').innerHTML);
                    if (error === 'empty_other_edit') {
                        //alert(req_other_edit);
                        document.getElementById('errorMessageLabel').innerHTML = req_other_edit;
                    } else if (error === 'nonnumber') {
                        //alert(req_num_only);
                        document.getElementById('errorMessageLabel').innerHTML = req_num_only;
                    } else if (error === 'bad_zip') {
                        //alert(req_good_zip);
                        document.getElementById('errorMessageLabel').innerHTML = req_good_zip;
                    } else if (error === 'bad_phone') {
                        //alert(req_good_phone);
                        document.getElementById('errorMessageLabel').innerHTML = req_good_phone;
                    } else if (error === 'bad_fax') {
                        //alert(req_good_fax);
                        document.getElementById('errorMessageLabel').innerHTML = req_good_fax;
                    } else if (error === 'bademail') {
                        //alert(req_valid_email);
                        document.getElementById('errorMessageLabel').innerHTML = req_valid_email;
                    } else if (error === 'nonmatchedemail') {
                        //alert(req_matched_email);
                        document.getElementById('errorMessageLabel').innerHTML = req_matched_email;
                    } else if (error === 'missingsel') {
                        //alert(req_sel + br + req_gen);
                        document.getElementById('errorMessageLabel').innerHTML = req_sel + br + req_gen;
                    } else {
                        //alert(req_edt + br + req_gen);
                        document.getElementById('errorMessageLabel').innerHTML = req_edt + br + req_gen;
                    }
                    //window.scroll(0, 0);
                    (error === 'missingradio') ? obj[0].focus() : obj.focus();
                    submitcount--;
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    </script>


</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
    <form id="form1" method="post" runat="server">
        <center>
            <table width="710" border="0" align="center" role="presentation">
                <tr>
                    <td width="710" bgcolor="#ffffff" style="height: 714px">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="ServicePlusNavDisplay" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/pghdr_prodreg1.jpg"
                                                bgcolor="#363d45" height="82">
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">--%>
                                            </td>
                                            <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <img src="images/spacer.gif" width="16" alt=""><span id="errorMessageLabel" runat="server" class="redAsterick"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="1" height="18">
                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                            </td>
                                            <td height="18">
                                                <span class="tableHeader">To register your product, please fill out the 
                                            information below. Fields marked with a <span class="redAsterick">*</span> are 
                                            mandatory.</span>
                                            </td>
                                        </tr>
                                        <%-- <tr>
												<td width="17" height="15"><img height="15" src="images/spacer.gif" width="20" alt=""></td>
						                        <td width="1" height="18"><img height="15" src="images/spacer.gif" width="3" alt=""></td>
						                        <td height="18"><span class="tableHeader">If you are signed in to ServicesPLUS, please review the information below and correct as necessary.</td>
					                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678" height="15">
                                                <img height="15" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" height="18">
                                                &nbsp;
                                            </td>
                                            <td width="670" height="30" align="left" valign="middle">
                                                <span style="font-size: 24px;">
                                                    <input type="HIDDEN" name="VERS" value="0100" />
                                                    <input type="HIDDEN" name="PRNM" value="Sony Pro Registration" />
                                                    <input type="HIDDEN" name="RgVr" value="I21105" />
                                                    <input type="HIDDEN" name="MFGR" value="SOPR" />
                                                    <input type="HIDDEN" name="BlVr" value="I60111" />
                                                    <input type="HIDDEN" name="PROD" value="SOP1" />
                                                    <input type="HIDDEN" name="RSrc" value="Web" />
                                                    <input type="HIDDEN" name="VRST" value="0100 (EN)" />
                                                    <input type="HIDDEN" name="LANG" value="EN" />
                                                    <input type="HIDDEN" escape="HTML" name="PRDP" />
                                                    <table border="0" cellspacing="1" width="670px" role="presentation">
                                                        <tr>
                                                            <td width="430px">
                                                                <table border="0" cellspacing="1" width="100%" role="form">
                                                                    <div id="formed">
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="FNAM" class="bodyCopy">
                                                                                First Name:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%">
                                                                            <input type="text" name="FNAM" runat="server" id="FNAM" size="30" maxlength="25"
                                                                                style="width: 200px" class="bodyCopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="LNAM" class="bodyCopy">
                                                                                &nbsp;Last Name:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="LNAM" runat="server" id="LNAM" size="30" maxlength="25"
                                                                                style="width: 200px" class="bodycopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="COMP" class="bodyCopy">
                                                                                &nbsp;Company:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="COMP" id="COMP" runat="server" size="30" maxlength="25"
                                                                                style="width: 200px" class="bodycopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" align="CENTER">
                                                                            <label for="COMP_SE" class="bodyCopy">
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If self-employed, please enter &quot;Independent&quot;</label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right" class="style1">
                                                                            <label for="ADD1" class="bodyCopy">Street Address:</label>
                                                                        </td>
                                                                        <td width="2%" class="style1">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="ADD1" id="ADD1" runat="server" size="30" maxlength="50"
                                                                                style="width: 200px" class="bodycopy" /><asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick" Text="*"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="ADD2" class="bodyCopy">Address 2nd Line:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="ADD2" id="ADD2" size="30" runat="server" maxlength="50"
                                                                                style="width: 200px" class="bodycopy" /><asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr visible="false" runat="server">
                                                                        <td width="31%" align="right">
                                                                            <label for="ADD3" class="bodyCopy">Address 3rd Line:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="ADD3" id="ADD3" size="30" runat="server" maxlength="50"
                                                                                style="width: 200px" class="bodycopy" /><asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="CITY0" class="bodyCopy">&nbsp;City:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="CITY" id="CITY0" size="30" runat="server" maxlength="50"
                                                                                style="width: 200px" class="bodycopy" /><asp:Label ID="LabelLine4Star" runat="server" CssClass="redAsterick" Text="*"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="STATrow">
                                                                        <td width="31%" align="right">
                                                                            <label for="STAT" class="bodyCopy">&nbsp;State:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <select name="STAT" runat="server" id="STAT" style="width: 125px" class="bodycopy">
                                                                                <option value="">Click to Select</option>
                                                                                <option value="AL">Alabama</option>
                                                                                <option value="AK">Alaska</option>
                                                                                <option value="AZ">Arizona</option>
                                                                                <option value="AR">Arkansas</option>
                                                                                <option value="CA">California</option>
                                                                                <option value="CO">Colorado</option>
                                                                                <option value="CT">Connecticut</option>
                                                                                <option value="DE">Delaware</option>
                                                                                <option value="FL">Florida</option>
                                                                                <option value="GA">Georgia</option>
                                                                                <option value="">Hawaii (N/A) **</option>
                                                                                <option value="ID">Idaho</option>
                                                                                <option value="IL">Illinois</option>
                                                                                <option value="IN">Indiana</option>
                                                                                <option value="IA">Iowa</option>
                                                                                <option value="KS">Kansas</option>
                                                                                <option value="KY">Kentucky</option>
                                                                                <option value="LA">Louisiana</option>
                                                                                <option value="ME">Maine</option>
                                                                                <option value="MD">Maryland</option>
                                                                                <option value="MA">Massachusetts</option>
                                                                                <option value="MI">Michigan</option>
                                                                                <option value="MN">Minnesota</option>
                                                                                <option value="MS">Mississippi</option>
                                                                                <option value="MO">Missouri</option>
                                                                                <option value="MT">Montana</option>
                                                                                <option value="NE">Nebraska</option>
                                                                                <option value="NV">Nevada</option>
                                                                                <option value="NH">New Hampshire</option>
                                                                                <option value="NJ">New Jersey</option>
                                                                                <option value="NM">New Mexico</option>
                                                                                <option value="NY">New York</option>
                                                                                <option value="NC">North Carolina</option>
                                                                                <option value="ND">North Dakota</option>
                                                                                <option value="OH">Ohio</option>
                                                                                <option value="OK">Oklahoma</option>
                                                                                <option value="OR">Oregon</option>
                                                                                <option value="PA">Pennsylvania</option>
                                                                                <option value="RI">Rhode Island</option>
                                                                                <option value="SC">South Carolina</option>
                                                                                <option value="SD">South Dakota</option>
                                                                                <option value="TN">Tennessee</option>
                                                                                <option value="TX">Texas</option>
                                                                                <option value="UT">Utah</option>
                                                                                <option value="VT">Vermont</option>
                                                                                <option value="VA">Virginia</option>
                                                                                <option value="WA">Washington</option>
                                                                                <option value="DC">Washington, D.C.</option>
                                                                                <option value="WV">West Virginia</option>
                                                                                <option value="WI">Wisconsin</option>
                                                                                <option value="WY">Wyoming</option>
                                                                            </select><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right" class="bodycopy">
                                                                            <label for="ZIPC" class="bodyCopy">&nbsp;Zip or Postal Code:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="ZIPC" runat="server" id="ZIPC" size="15" maxlength="10"
                                                                                style="width: 200px" class="bodycopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="PHON" class="bodyCopy">&nbsp;Phone Number:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <label class="bodyCopy">
                                                                                (
                                                                <input type="text" name="PHAC" runat="server" id="PHAC" size="3" maxlength="3" onkeyup="return autoTab(this, 3, event);"
                                                                    class="bodycopy" />
                                                                                ) -
                                                                <input type="text" name="PHPR" id="PHPR" runat="server" size="3" maxlength="3" onkeyup="return autoTab(this, 3, event);"
                                                                    class="bodycopy" />
                                                                                -
                                                                <input type="text" name="PHNM" id="PHNM" runat="server" size="4" maxlength="4" class="bodycopy" /></label><em
                                                                    class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="FAXX" class="bodyCopy">Fax Number:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <label class="bodyCopy">
                                                                                (
                                                                <input type="text" name="FXAC" id="FXAC" runat="server" size="3" maxlength="3" onkeyup="return autoTab(this, 3, event);"
                                                                    class="bodycopy" />
                                                                                ) -
                                                                <input type="text" name="FXPR" id="FXPR" runat="server" size="3" maxlength="3" onkeyup="return autoTab(this, 3, event);"
                                                                    class="bodycopy" />
                                                                                -
                                                                <input type="text" name="FXNM" id="FXNM" runat="server" size="4" maxlength="4" class="bodycopy" /></label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="EMAL" class="bodyCopy">&nbsp;E-Mail Address:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" name="EMAL" runat="server" id="EMAL" size="30" maxlength="100"
                                                                                style="width: 200px" class="bodycopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="CMAL" class="bodyCopy">&nbsp;Confirm E-Mail Address:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <input type="text" runat="server" name="CMAL" id="CMAL" size="30" maxlength="100"
                                                                                style="width: 200px" class="bodycopy" /><em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right">
                                                                            <label for="PUSE" class="bodyCopy">Industry:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <SPS:SPSDropDownList Width="200" ID="PUSE" CssClass="bodyCopy" runat="server" />
                                                                            <em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="31%" align="right" class="bodycopy">
                                                                            &nbsp;Occupation/Title:
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%" class="bodycopy">
                                                                            <SPS:SPSDropDownList Width="250" ID="OCCU" CssClass="bodyCopy" runat="server" />
                                                                            <em class="redAsterick">*</em>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="OCCOid" style="display: none; visibility: hidden;">
                                                                        <td width="31%" align="right">
                                                                            <label for="OCCO" class="bodyCopy"><em>*</em> Other, please specify:</label>
                                                                        </td>
                                                                        <td width="2%">
                                                                        </td>
                                                                        <td width="67%">
                                                                            <input type="text" name="OCCO" id="OCCO" size="30" maxlength="30" style="width: 200px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <br />
                                                                            <p>
                                                                                <input type="checkbox" name="NEMA" id="NEMA" value="1" checked><label for="NEMA"
                                                                                    class="bodyCopy">
                                                                                    Yes, I would like to receive information from Sony Electronics Inc. about 
                                                                products, services, promotions, contests and offerings that may be of interest 
                                                                to me.</label>
                                                                                <br />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <input type="hidden" name="PAGE" value="1">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="240px" valign="top">
                                                                <ServicePLUSWebApp:SPSPromotion ID="RHSPromos" Type="3" runat="server" />
                                                            </td>
                                                        </tr>
                                                <td colspan="3">
                                                    <br />
                                                    <label for="GREETING2" class="bodyCopy">
                                                        &nbsp;<strong><em>**</em>Product Registration at this site is valid only in the 
                                                continental United States, excluding Hawaii and territories.</strong></label>
                                                </td>
                                        </tr>
                                        </div>
                                    </table>

                                    <script type="text/javascript">
                                        var formLT = document.forms.form1;
                                        i = formLT.OCCU.selectedIndex;
                                        //alert("i:"+i);
                                        if (formLT.PUSE !== null) {
                                            change_occu(formLT.PUSE.options[formLT.PUSE.selectedIndex].value);
                                            //change_occu(formLT.PUSE.options[i].value);
                                            formLT.OCCU.selectedIndex = i;
                                            //alert("PUSE:"+formLT.PUSE.options[formLT.PUSE.selectedIndex].value+"\n"+"OCCU:"+formLT.OCCU.options[formLT.OCCU.selectedIndex].value);

                                            //dynamic_OCCO(document.forms[0].OCCU.options[i].value);
                                            dynamic_OCCO(document.forms[0].OCCU.options[document.forms[0].OCCU.selectedIndex].value);
                                        }


                                    </script>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" height="18" align="center">
                                    <asp:ImageButton ID="imgNext" ImageUrl="images/sp_int_next_btn.gif" runat="server"
                                        AlternateText="Next" />
                                </td>
                            </tr>
                            <tr>
                                <td width="3" colspan="2" height="18">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="3" colspan="2" height="18">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                    </td>
                </tr>
            </table>
            </td> </tr> </table>
        </center>
        <input type="hidden" runat="server" id="hdnNavigator" />
    </form>
</body>
</html>
