<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.fileUnavailableOnline" CodeFile="fileUnavailableOnline.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Technical Bulletin Unavailable Online</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

    <script type="text/javascript">
        function ReSize() {
            window.resizeTo(410, 500);
        }
    </script>
</head>
<body leftmargin="0" topmargin="0" onload="javascript:ReSize();">
    <form id="frmfileUnavailableOnline" method="post" runat="server">
        <table width="100%" align="left" border="0">
            <tr>
                <td>
                    <img src="images/sp_popup_technical_bulletins.jpg" alt="Technical Bulletin"></td>
            </tr>
            <tr>
                <td style="height: 201px">
                    <img height="10" src="images/spacer.gif" width="4" alt="">
                    <table cellpadding="2" width="370" border="0">
                        <tr>
                            <td width="20" height="3" style="height: 3px">
                                <img height="4" src="images/spacer.gif" width="20" alt=""></td>
                            <td width="341" height="3" style="width: 341px; height: 3px"></td>
                        </tr>
                        <tr>
                            <td valign="top" width="20">
                                <img height="4" src="images/spacer.gif" width="20" alt=""></td>
                            <td valign="top" width="341" class="tableData" style="width: 365px">
                                This Technical 
									Bulletin you requested  is currently unavailable online. Please call 
									1-408-352-4500 for further details.
                            </td>
                        </tr>
                        <tr>
                            <td width="20">
                                <img height="4" src="images/spacer.gif" width="20" alt=""></td>
                            <td width="341" style="width: 341px" align="center"><a href="#" onclick="javascript:window.close();">
                                <img src="<%=Resources.Resource.img_btnCloseWindow()%>" border="0" alt="Close Window"></a></td>
                        </tr>
                        <tr>
                            <td width="20">
                                <img height="4" src="images/spacer.gif" width="20" alt=""></td>
                            <td width="341" style="width: 341px">
                                <img height="10" src="images/spacer.gif" width="4" alt=""></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
