<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-Pg-ModelSearch-Result.aspx.vb" Inherits="ServicePLUSWebApp.PgModelSearchResult" %>

<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register Src="~\UserControl\SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.header_serviceplus()%> - <%=Resources.Resource.repair_svcacc_hdr_msg()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Repair, Maintenance, Services, Sony Models, Sony Professional Models, Sony Broadcast Models, Sony Business Models, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <meta http-equiv="X-UA-Compatible" content="IE=5" />
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function Show() {
            alert('Sai');
        }
        function GoDepot() {
            if (document.Form1.ModelNumber.value === "") {
                alert('<%=Resources.Resource.repair_sny_pg_mdlsrch_msg()%>');
            }
        }
        function ShowLogin(SPGSModel) {
            window.location.href = 'SignIn.aspx?Lang=' + '<%=LanguageSM%>' + '&post=spmsr&SPGSModel=' + SPGSModel;
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background-image: url('images/sp_left_bkgd.gif')">
                        <img src="images/spacer.gif" height="25" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="582" height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr style="height: 82px;">
                                            <td style="width: 465px; text-align: right; background: #363d45 url('images/repair_lrg.gif');">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.repair_snysvc_srl_msg_4%></h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8" style="width: 465px" valign="top">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.repair_sny_pg_svcrqst_msg%></h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5" valign="top">
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="width: 465px; background: #f2f5f8;">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td style="width: 238px; background: #99a8b5;">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 14px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="height: 22px; width: 5731px;">&nbsp;</td>
                                            <td style="width: 436px; height: 22px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 12px; height: 24px;">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 3773px; height: 24px;">
                                            </td>
                                            <td align="center" style="width: 436px; height: 24px;">
                                                &nbsp;&nbsp;
								                    <img height="20" src="images/spacer.gif" width="20" alt="">
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td style="width: 12px">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td style="width: 3773px">
                                                <p class="promoCopy">
                                                    <table cellpadding="3" width="100%" role="presentation">
                                                        <tr>
                                                            <td>
                                                                <p class="promoCopyBold">
                                                                    <%=Resources.Resource.repair_dptsvc_msg()%>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span class="bodyCopy"><%=Resources.Resource.repair_sny_pg_slmdlblw_msg()%></span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%-- 2018-08-15 ASleight - There appears to be no use for this, and it's causing a WCAG compliance issue
                                                                    <a href="#" onclick="mywin=window.open('NotLoggedInAddToCart.aspx','','toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=400,height=400');"></a>--%>
                                                                <asp:GridView ID="GridPartsGroup" runat="server" AutoGenerateColumns="False">
                                                                    <Columns>
                                                                        <asp:BoundField AccessibleHeaderText="Group ID" DataField="ModelName" HeaderText="Group ID"
                                                                            Visible="False" />
                                                                        <asp:TemplateField AccessibleHeaderText="From" HeaderText="Model Number">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkModel" CommandName="ModelClick" CommandArgument='<%# Eval("ModelName") %>' runat="server" PostBackUrl="sony-service-sn.aspx" Text='<%# Eval("ModelName") %>'></asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:TemplateField>
                                                                        <asp:HyperLinkField AccessibleHeaderText="To" DataTextField="ModelDescription" HeaderText="Description">
                                                                            <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:HyperLinkField>
                                                                    </Columns>
                                                                    <RowStyle BackColor="#F2F5F8" CssClass="tableData" />
                                                                    <HeaderStyle BackColor="#D5DEE9" CssClass="tableHeader" />
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <a href="#top"><img src="<%= Resources.Resource.repair_sny_type_3_img() %>" border="0" alt="Back to Top"></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </p>
                                            </td>
                                            <%-- By Sneha on 25/03/2014: The following row width is changed from 228px to 34% to fix bug 8994 (bugzilla-similar)--%>
                                            <%If divrprmsrShow = True Then%>
                                            <td width="34%" valign="top" align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<uc1:SPSPromotion ID="RHSPromn1" DisplayPage="sony-Pg-ModelSearch-Result" Type="1" runat="server" />
                                            </td>
                                            <%End If%>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                            <td colspan="2">
                                                <%If divrprmsrShow = True Then%>
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <uc1:SPSPromotion ID="Promotions1" DisplayPage="sony-Pg-ModelSearch-Result" Type="2" runat="server" />
                                                        </td>
                                                        <td width="16">
                                                            <img src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <uc1:SPSPromotion ID="SPSPromotionBottomRight" DisplayPage="sony-Pg-ModelSearch-Result" Type="3" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <%End If%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
