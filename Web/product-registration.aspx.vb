Imports System.Data
Imports System.Web.Services
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class ProductRegistrationAddSerial
        Inherits SSL


#Region " Web Form Designer Generated Code "
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'If Request.UrlReferrer Is Nothing Then Response.Redirect("ProductRegistrationWelcome.aspx")
        End Sub
#End Region

#Region "   Page Variabls   "
        Public bColor As Boolean = False
        Private dsRegisteredProduct As New DataSet()
        Private objProductRegistration As ProductRegistration
#End Region

#Region "   Events  "

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            txtCertificate.Attributes.Add("onkeypress", "javascript:toUpper('txtCertificate');")
            txtSerialNumber.Attributes.Add("onkeypress", "javascript:toUpper('txtSerialNumber');")
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                If Session("DUPCERT") IsNot Nothing Then
                    ErrorLabel.Text = Session("DUPCERT")
                End If

                If Session("RegisterProduct") Is Nothing Then
                    Session.Remove("RegisterProduct")   ' If it's null in C#, it gets Removed. Not sure how VB handles this, so will leave it as-is.
                    Response.Redirect("sony-service-agreements.aspx", True)
                    Exit Sub
                Else
                    objProductRegistration = CType(Session("RegisterProduct"), ProductRegistration)
                    If objProductRegistration.Completed Then
                        Session.Remove("RegisterProduct")
                        Response.Redirect("sony-service-agreements.aspx", True)
                        Exit Sub
                    End If
                End If

                'Added code to read the data from database by Rashmi
                If ConfigurationData.GeneralSettings.SIAMOperations.EnableServAgreementDatabase = "True" Then
                    Dim objSIAMOperation = New SIAMOperation()
                    dsRegisteredProduct = objSIAMOperation.GETServiceAgreementModel(HttpContextManager.GlobalData)
                Else
                    dsRegisteredProduct.ReadXml(Server.MapPath("ProductRegistrationList.xml"))
                End If


                If Not IsPostBack Then
                    ddlModelNumber.Attributes.Add("onchange", "javascript: return ModelPopup();")
                    If objProductRegistration.RegisteredProducts.Length = 0 Then
                        objProductRegistration.AddProduct("", "", "", "", "", "", True)
                    End If
                    dgProduct.DataSource = objProductRegistration.RegisteredProducts
                    dgProduct.DataBind()

                    Dim dView As New DataView()
                    Dim dTable As New DataTable
                    'Added code to read the data from database by Rashmi
                    If ConfigurationData.GeneralSettings.SIAMOperations.EnableServAgreementDatabase = "True" Then
                        dView = dsRegisteredProduct.Tables(0).DefaultView
                        dView.Sort = "Category"
                        dTable = dView.ToTable(True, "Category", "PRODUCTGROUP")
                        'ddlProductCategory.DataSource = dView.Table
                        ddlProductCategory.DataSource = dTable
                        ddlProductCategory.DataTextField = "Category"
                        ddlProductCategory.DataValueField = "PRODUCTGROUP"
                    Else
                        dView = dsRegisteredProduct.Tables("Schema").DefaultView
                        dView.Sort = "CategoryID"
                        ddlProductCategory.DataSource = dView.Table
                        ddlProductCategory.DataTextField = "CategoryID"
                        ddlProductCategory.DataValueField = "PRODGRP"
                    End If

                    ddlProductCategory.DataBind()
                    ddlProductCategory.Items.Insert(0, " Select Product Category")
                    ddlProductCategory.SelectedIndex = 0
                    FillProductCode(ddlProductCategory.SelectedItem.Text)
                End If
                'If (objProductRegistration.RegisteredProducts.Length = 0) Then
                '    imgNext.Enabled = False
                '    imgNext.AlternateText = "Add at least one product."
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub imgNext_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgNext.Click
            If (objProductRegistration.RegisteredProducts.Length = 0 Or objProductRegistration.RegisteredProducts(0).IsDefault = True) Then
                ErrorValidation.Text = Resources.Resource.el_ValidProduct ' "Add at least one product."
                Exit Sub
            End If

            If chkTC.Checked = False Then
                ErrorValidation.Text = Resources.Resource.el_ValidTermsCond ' "Please accept terms and conditions."
                Exit Sub
            End If
            Session.Remove("DUPCERT")
            Response.Redirect("ProductRegistrationThankYou.aspx", False)
        End Sub

        Protected Sub dgProduct_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgProduct.ItemCommand
            Dim ireduceItemId As Int16 = 0
            Dim productReg As New ProductRegistration

            Try
                If e.CommandName = "Delete" Then
                    If Session("RegisterProduct") IsNot Nothing Then
                        productReg = CType(Session("RegisterProduct"), ProductRegistration)
                        For Each product In productReg.RegisteredProducts
                            If product.ItemID.ToString() = e.Item.Cells(6).Text.Trim() Then
                                productReg.RemoveProduct(product)
                                ireduceItemId = 1
                            End If
                            product.ItemID = product.ItemID - ireduceItemId
                        Next
                        'If productReg.RegisteredProducts.Length > 0 Then
                        '    imgNext.Enabled = True
                        'Else
                        '    imgNext.Enabled = False
                        '    imgNext.AlternateText = "Add atleast one product to submit"
                        'End If
                        If productReg.RegisteredProducts.Length = 0 Then
                            productReg.AddProduct("", "", "", "", "", "", True)
                            Session("RegisterProduct") = productReg
                        End If
                        dgProduct.DataSource = productReg.RegisteredProducts
                        dgProduct.DataBind()
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgProduct_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgProduct.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim objProduct As ProductRegistration.Product
                objProduct = CType(e.Item.DataItem(), ProductRegistration.Product)
                If objProduct.CertificateStatus <> CertificateUseType.Valid_NotInUse Then
                    e.Item.Cells(0).CssClass = "redAsterick"
                    e.Item.Cells(1).CssClass = "redAsterick"
                    e.Item.Cells(2).CssClass = "redAsterick"
                    e.Item.Cells(3).CssClass = "redAsterick"
                    e.Item.Cells(4).CssClass = "redAsterick"
                    e.Item.Cells(5).CssClass = "redAsterick"
                    Select Case (objProduct.CertificateStatus)
                        Case (CertificateUseType.UsedBySameCustomer)
                            ErrorValidation.Text = Resources.Resource.Err_ProductReg_1 '"You have previously registered a Service Agreement with this Certificate Number. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669.<br/>"
                            e.Item.ToolTip = Resources.Resource.Err_ProductReg_2 '"You have previously registered a Service Agreement with this Certificate Number. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669."
                        Case (CertificateUseType.UsedByAnotherCustomer)
                            ErrorValidation.Text = Resources.Resource.Err_ProductReg_3 '"The Certificate Number for the Service Agreement you are trying to register has previously been registered by another Customer. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669.<br/>"
                            e.Item.ToolTip = Resources.Resource.Err_ProductReg_4 '"The Certificate Number for the Service Agreement you are trying to register has previously been registered by another Customer. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669."
                        Case (CertificateUseType.NotAValidSerialNumber)
                            ErrorValidation.Text = Resources.Resource.Err_ProductReg_5 ' "The Certificate Number for the Service Agreement you are trying to register does not match the format for this Service Agreement. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669. </br>"
                            e.Item.ToolTip = Resources.Resource.Err_ProductReg_6 '"The Certificate Number for the Service Agreement you are trying to register does not match the format for this Service Agreement. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669. </br>"
                    End Select
                End If
            End If
        End Sub

        Protected Sub btnSaveAndAdd_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSaveAndAdd.Click
            Try
                If IsItemValid() Then
                    If AddProduct() Then
                        ClearField()
                        RefreshGrid()
                    Else
                        ErrorValidation.Text = Resources.Resource.Valid_ProductReg_1 ' "Model number and serial number already exists on products list."
                        ErrorValidation.Visible = True
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlProductCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProductCategory.SelectedIndexChanged
            Try
                FillProductCode(ddlProductCategory.SelectedItem.Text)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlProductCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProductCode.SelectedIndexChanged
            Try
                FillModel(ddlProductCode.Text)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

#Region "Private Methods"

        Private Sub FillModel(ByVal ProductCode As String)
            Dim dView As New DataView()
            'Dim dTable As New System.Data.DataTable
            'Added code to read the data from database
            If ConfigurationData.GeneralSettings.SIAMOperations.EnableServAgreementDatabase = "True" Then
                dView = dsRegisteredProduct.Tables(0).DefaultView()
                dView.RowFilter = "Productcode = '" + ProductCode + "'"
                dView.Sort = "Model"
                'dTable = dView.ToTable(True, "Model")
                ddlModelNumber.DataSource = dView.Table()
                'ddlModelNumber.DataSource = dTable
                ddlModelNumber.DataTextField = "Model"
                ddlModelNumber.DataValueField = "Model"
            Else
                dView = dsRegisteredProduct.Tables("ModelNumber").DefaultView()
                dView.RowFilter = "ProductID = '" + ProductCode + "'"
                dView.Sort = "ModelID"
                ddlModelNumber.DataSource = dView.Table()
                ddlModelNumber.DataTextField = "ModelID"
                ddlModelNumber.DataValueField = "ModelID"
            End If

            ddlModelNumber.DataBind()
            If (ddlProductCode.SelectedIndex > 0) Then
                ddlModelNumber.Items.Insert(0, " Select Model Number")
                ddlModelNumber.SelectedIndex = 0
            End If
        End Sub

        Private Sub FillProductCode(ByVal ProductCategory As String)
            Dim dView As New DataView()
            Dim dTable As New System.Data.DataTable
            'Added code to read the data from database Rashmi
            If ConfigurationData.GeneralSettings.SIAMOperations.EnableServAgreementDatabase = "True" Then
                dView = dsRegisteredProduct.Tables(0).DefaultView()
                dView.RowFilter = "Category = '" + ProductCategory + "'"
                dView.Sort = "Productcode"
                dTable = dView.ToTable(True, "Productcode")
                'ddlProductCode.DataSource = dView.Table()
                ddlProductCode.DataSource = dTable
                ddlProductCode.DataTextField = "Productcode"
                ddlProductCode.DataValueField = "Productcode"
            Else
                dView = dsRegisteredProduct.Tables("ProductCode").DefaultView()
                dView.RowFilter = "CategoryID = '" + ProductCategory + "'"
                dView.Sort = "ProductID"
                ddlProductCode.DataSource = dView.Table()
                ddlProductCode.DataTextField = "ProductID"
                ddlProductCode.DataValueField = "ProductID"
            End If

            ddlProductCode.DataBind()

            If (ddlProductCategory.SelectedIndex > 0) Then
                ddlProductCode.Items.Insert(0, " Select Product Code")
                ddlProductCode.SelectedIndex = 0
            End If
            ddlModelNumber.Items.Clear()
        End Sub

        Private Function IsItemValid() As Boolean
            Dim retVal As Boolean = True
            ErrorValidation.Text = String.Empty

            If (ddlProductCategory.SelectedIndex = 0) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_2 & "<br/>" '"Please select Product Code and Model Number."
                retVal = False
            End If

            If (ddlProductCode.SelectedIndex = 0) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_2 & "<br/>" '"Please select Product Code and Model Number."
                retVal = False
            ElseIf String.IsNullOrWhiteSpace(ddlProductCode.Text) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_4 & "<br/>" '"Please select Product Code."
                retVal = False
            End If

            If (ddlModelNumber.SelectedIndex = 0) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_3 & "<br/>" ' "Please select Model Number."
                retVal = False
            ElseIf String.IsNullOrWhiteSpace(ddlModelNumber.Text) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_3 & "<br/>" '"Please select Model Number."
                retVal = False
            End If

            If String.IsNullOrWhiteSpace(txtDate.Text) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_5 & "<br/>" '"Please enter purchase date."
                retVal = False
            Else
                If IsDate(txtDate.Text) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not Regex.IsMatch(txtDate.Text.ToString(), thisPattern) Then
                        ErrorValidation.Text = Resources.Resource.Valid_ProductReg_6 & "<br/>" ' "Please enter date in mm/dd/yyyy format."
                        retVal = False
                    Else
                        If Convert.ToDateTime(txtDate.Text) > Now.Date Then
                            ErrorValidation.Text = Resources.Resource.Valid_ProductReg_7 & "<br/>" ' "Please enter date before today."
                            retVal = False
                        End If
                    End If
                Else
                    ErrorValidation.Text = Resources.Resource.Valid_ProductReg_8 & "<br/>" ' "Please enter valid date."
                    retVal = False
                End If
            End If

            If String.IsNullOrWhiteSpace(txtSerialNumber.Text) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_9 & "<br/>" ' "Please enter Serial Number."
                retVal = False
            End If
            If String.IsNullOrWhiteSpace(txtCertificate.Text) Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_10 & "<br/>" ' "Please enter Service Pack Certificate Number."
                retVal = False
            ElseIf txtCertificate.Text.ToUpper().StartsWith("EW") Then
                If Not CheckCertificateNumber() Then
                    ErrorValidation.Text = Resources.Resource.Valid_ProductReg_11 & "<br/>" '"You do not need to register certificate XXXXXX; it was automatically registered when you purchased your Extended Warranty on this ServicesPLUS site. If you need assistance, please contact Sony Professional Services Contracts Administration at 877-398-7669. </br>"
                    retVal = False
                Else
                    ErrorValidation.Text = Resources.Resource.Valid_ProductReg_12 & "<br/>" '"The Certificate Number for the Service Agreement you are trying to register does not match the format for this Service Agreement. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669. </br>"
                    retVal = False
                End If
            ElseIf Not ValidateCertificateNumber() Then
                ErrorValidation.Text = Resources.Resource.Valid_ProductReg_12 & "<br/>" '"The Certificate Number for the Service Agreement you are trying to register does not match the format for this Service Agreement. Please check the number and re-enter it. If an error persists, please contact Sony Professional Service Contracts Administration at 877-398-7669. </br>"
                retVal = False
            End If

            Return retVal
        End Function

        Private Function AddProduct() As Boolean
            Dim objProductRegistration As New ProductRegistration
            If Session("RegisterProduct") Is Nothing Then Return False
            objProductRegistration = CType(Session("RegisterProduct"), ProductRegistration)

            For Each objProduct As ProductRegistration.Product In objProductRegistration.RegisteredProducts
                If objProduct.ModelNumber = ddlModelNumber.SelectedValue And objProduct.SerialNumber = txtSerialNumber.Text.Trim().ToUpper() Then
                    Return False
                End If
            Next
            objProductRegistration.AddProduct(ddlProductCategory.SelectedItem.Text, txtCertificate.Text.Trim().ToUpper(), ddlProductCode.Text, ddlModelNumber.Text, txtDate.Text.Trim(), txtSerialNumber.Text.Trim().ToUpper(), False)
            Session("RegisterProduct") = objProductRegistration

            Return True
        End Function

        Private Sub ClearField()
            ddlProductCode.SelectedIndex = 0
            ddlModelNumber.SelectedIndex = 0
            txtDate.Text = String.Empty
            txtSerialNumber.Text = String.Empty
            ErrorValidation.Text = String.Empty
            txtCertificate.Text = String.Empty
            ddlModelNumber.Items.Clear()
            ddlProductCode.Items.Clear()
            ddlProductCategory.SelectedIndex = 0
            ddlProductCategory.Focus()
        End Sub

        Private Sub RefreshGrid()
            If Session("RegisterProduct") IsNot Nothing Then
                Dim objProductRegistration = CType(Session("RegisterProduct"), ProductRegistration)
                dgProduct.DataSource = objProductRegistration.RegisteredProducts
                dgProduct.DataBind()
                If objProductRegistration.RegisteredProducts.Length > 0 Then
                    imgNext.Enabled = True
                    imgNext.AlternateText = "Register"
                End If
            End If
        End Sub

        ''' <summary>
        ''' Ensure the text in txtCertificate matches the expected Certificate Number
        ''' format: AA1???1111???, where A = Alpha, 1 = Numeric, ? = AlphaNumeric
        ''' </summary>
        Private Function ValidateCertificateNumber() As Boolean
            Dim isNotAlpha As New Regex("[\w]")
            Dim isNotAlphaNumeric As New Regex("[\d\w]")
            Dim isNotNumeric As New Regex("[\d]")
            Dim strCertNum As String = txtCertificate.Text.Trim()

            If strCertNum.Length <> 13 Then
                'ErrorValidation.Text = "Certificate number should be of length 13 charecter."
                Return False
            ElseIf IsNumeric(strCertNum.Substring(0, 1)) Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            ElseIf IsNumeric(strCertNum.Substring(1, 1)) Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            ElseIf isNotNumeric.Replace(strCertNum.Substring(2, 1), "").Length > 0 Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            ElseIf isNotAlphaNumeric.Replace(strCertNum.Substring(3, 3), "").Length > 0 Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            ElseIf isNotNumeric.Replace(strCertNum.Substring(6, 4), "").Length > 0 Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            ElseIf isNotAlphaNumeric.Replace(strCertNum.Substring(10, 3), "").Length > 0 Then
                'ErrorValidation.Text = "Incorrect certificate number."
                Return False
            End If

            Return True
        End Function

        Public Function CheckCertificateNumber() As Boolean
            Dim Retvalue As Integer
            Dim ew = New ExtendedWarrantyManager()
            Retvalue = ew.CheckCertificateNumber(txtCertificate.Text.Trim())
            If Retvalue > 0 Then
                Return False
            Else
                Return True
            End If
        End Function
#End Region

        ''' <summary>
        ''' Added to get the conflicted model number
        ''' </summary>
        ''' <remarks>By Sneha on 25/11/2013</remarks>
        <WebMethod()> _
		Public Shared Function CheckConflicts(ByVal ModelNo As String) As String
            Try
                Dim objSIAMOperation = New SIAMOperation()
                Return objSIAMOperation.GetConflictedModel(ModelNo, HttpContextManager.GlobalData)
            Catch ex As Exception
                Return ex.Message
			End Try
		End Function
    End Class
End Namespace
