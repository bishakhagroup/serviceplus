Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace ServicePLUSWebApp

    'Imports PayFlowPro
    'Imports PFProCOMLib

    Partial Class NotLoggedInAddToCart
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        'Dim objUtilties As Utilities = New Utilities
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Try catch is added by praveen to handle Exception for Bug 2170
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "Page_Load"
                objPCILogger.Message = "Page_Load Success."
                objPCILogger.EventType = EventType.View
                '6524 end

                'Dim responseurl As String = "SignIn.aspx"
                Dim responseurl As String = "SignIn-Register.aspx"
                If Not Me.IsPostBack Then
                    If Not String.IsNullOrWhiteSpace(Request.QueryString("PartNumber")) Then
                        Dim strKitPartNumber = Request.QueryString("PartNumber")
                        'Dim sModel As String = Request.QueryString("ModelNumber")
                        Dim dm As New SoftwarePlusDataManager
                        Dim sw As SelectedSoftware = dm.SearchSoftwareByID(strKitPartNumber)
                        'commented by chandra for bug 456 [------- Comment #18 From Rick Upton 2008-02-27 18:21:13 pst [reply] ------- ]
                        If (Not sw Is Nothing) Then  'And (sw.FreeOfCharge = Software.FreeOfChargeType.NotFree) Then 'only add those are not free
                            If sw.Type = Product.ProductType.Certificate Or sw.Type = Product.ProductType.ClassRoomTraining Or sw.Type = Product.ProductType.OnlineTraining Then
                                sw.Download = True
                                'sw.DownloadFileName = "NA"
                            Else
                                sw.Download = sw.BillingOnly
                                sw.Ship = Not sw.BillingOnly
                            End If
                            Dim selectedItemsBeforeLoggedIn As ArrayList
                            If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                                selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                            Else
                                selectedItemsBeforeLoggedIn = New ArrayList
                                Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                            End If
                            selectedItemsBeforeLoggedIn.Add(sw)
                        End If
                        'If Not sModel Is Nothing Then
                        '    responseurl = responseurl + "?SWMNumber=" + sModel
                        'End If
                        ' added by praveen  for Bug 2170
                        'Session.Item("partList") is not null
                    ElseIf Not Request.QueryString("PartLineNo") Is Nothing Then
                        ''added by praveen to handle session timeout 
                        If (Session.Item("partList") Is Nothing) Then
                            Session.Abandon()
                            Response.Redirect("default.aspx", True)
                        End If
                        Dim parts() As Sony.US.ServicesPLUS.Core.Part = Session.Item("partList")

                        Dim part As Sony.US.ServicesPLUS.Core.Part = parts(Short.Parse(Request.QueryString("PartLineNo")))
                        Dim selectedItemsBeforeLoggedIn As ArrayList
                        If Not Session("SelectedItemsBeforeLoggedIn") Is Nothing Then
                            selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                        Else
                            selectedItemsBeforeLoggedIn = New ArrayList
                            Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                        End If
                        selectedItemsBeforeLoggedIn.Add(part)
                        ' added by praveen  for Bug 2170
                        'Session.Item("kits") is not null
                    ElseIf Not Request.QueryString("KitLineNo") Is Nothing Then
                        ''added by praveen to handle session timeout 
                        ''If session is nothing
                        ''due to session timeout then remove customer session information and redirecting to default.aspx page
                        If (Session.Item("kits") Is Nothing) Then
                            Session.Abandon()
                            Response.Redirect("default.aspx", True)
                        End If
                        Dim kits() As Sony.US.ServicesPLUS.Core.Kit = Session.Item("kits")
                        Dim kit As Sony.US.ServicesPLUS.Core.Kit = kits(Short.Parse(Request.QueryString("KitLineNo")))
                        Dim selectedItemsBeforeLoggedIn As ArrayList
                        If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                            selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                        Else
                            selectedItemsBeforeLoggedIn = New ArrayList
                            Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                        End If
                        selectedItemsBeforeLoggedIn.Add(kit)
                    ElseIf Request.QueryString("PromoPartLineNo") IsNot Nothing Then
                        'Dim parts() As Sony.US.ServicesPLUS.Core.Part = Session.Item("partList")
                        Dim part As Sony.US.ServicesPLUS.Core.Part
                        Dim promotionManager As New PromotionManager
                        part = promotionManager.GetPartInPromotion(Request.QueryString("PromoPartLineNo"))
                        'For Each t_part As Part In parts
                        '    If t_part.PartNumber = Request.QueryString("PromoPartLineNo") Then
                        '        part = t_part
                        '        Exit For
                        '    End If
                        'Next
                        Dim selectedItemsBeforeLoggedIn As ArrayList
                        If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                            selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                        Else
                            selectedItemsBeforeLoggedIn = New ArrayList
                            Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                        End If
                        selectedItemsBeforeLoggedIn.Add(part)
                    ElseIf Request.QueryString("PromoKitLineNo") IsNot Nothing Then
                        'Dim kits() As Sony.US.ServicesPLUS.Core.Kit = Session.Item("kits")
                        Dim promotionManager As PromotionManager = New PromotionManager
                        Dim kit As Sony.US.ServicesPLUS.Core.Kit
                        kit = promotionManager.GetKitInPromotion(Request.QueryString("PromoKitLineNo"))
                        'For Each t_kit As Kit In kits
                        '    If t_kit.Model = Request.QueryString("PromoKitLineNo") Then
                        '        kit = t_kit
                        '        Exit For
                        '    End If
                        'Next
                        Dim selectedItemsBeforeLoggedIn As ArrayList
                        If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                            selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                        Else
                            selectedItemsBeforeLoggedIn = New ArrayList
                            Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                        End If
                        selectedItemsBeforeLoggedIn.Add(kit)
                        promotionManager = Nothing
                    ElseIf Request.QueryString("SPGSModel") IsNot Nothing Then
                        responseurl = responseurl + "?SPGSModel=" + Request.QueryString("SPGSModel")
                        Response.Redirect(responseurl)
                    End If
                    btnClose.Visible = False
                End If

                Dim strParamaters As String = String.Empty
                strParamaters = ResolveURLParameter()
                responseurl = responseurl + IIf(strParamaters = String.Empty, String.Empty, "?" + strParamaters)
                Response.Redirect(responseurl)
            Catch ex As Threading.ThreadAbortException
                '-- do nothing    
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Page_Load Failed. " & ex.Message.ToString() '6524
                MessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
            Response.Write("<script language='javascript'> { window.close() }</script>")
        End Sub
    End Class

End Namespace
