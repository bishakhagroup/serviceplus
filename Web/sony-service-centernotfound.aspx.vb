Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Text.RegularExpressions

Namespace ServicePLUSWebApp

    Partial Class sony_service_centernotfound
        Inherits SSL

        Public sModelNumber As String = String.Empty
        Public sShipState As String = String.Empty
        Public sError As String = String.Empty

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Session("snServiceInfo") Is Nothing Then
                Dim oServiceInfo As New ServiceItemInfo
                oServiceInfo = Session("snServiceInfo")
                lnkChangeRequest.HRef = "sony-service-sn.aspx?model=" + oServiceInfo.ModelNumber
                sModelNumber = oServiceInfo.ModelNumber
                sError = oServiceInfo.DepotCenterAddress
                sShipState = oServiceInfo.ShipStateAbbr
            Else
                Response.Redirect("sony-repair.aspx")
            End If
        End Sub
    End Class
End Namespace