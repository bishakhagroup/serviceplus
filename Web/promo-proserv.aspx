<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="promo-proserv.aspx.vb" Inherits="promo_proserv" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.footer_sony_profsnl_svs%></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.footer_sony_profsnl_svs%> &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" bgcolor="#363d45">
                                                <table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img height="24" src="images/spacer.gif" width="246" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">&nbsp;</td>
                                                        <td width="236">
                                                            <span class="memberName">
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server"></ServicePLUSWebApp:PersonalMessage>
                                                            </span>
                                                        </td>
                                                        <td width="5">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Introduction to Sony Professional Services</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="bodyCopy" style="vertical-align: top; padding: 10px;">
                                    <span style="font-size: 13px; line-height: 17px;">Providing maximum up time for our customers is our primary mission at Sony Professional Services. With Regional Service Centers (RSCs)
									     located on each coast of the US, multiple satellite Field Stocking Locations (FSL), and unmatched technical experience, Sony has the resources
									     to provide service centered on your needs. Our factory-trained team of engineers and technical specialists offers superb service both in our
									     RSCs and on-site at customers&#8217; facilities.   In addition to repair and maintenance services, Sony offers our customers 24 Hour SystemWatch&copy;
									     Remote Systems Support, Technical Telephone Support, Software Support, and genuine Sony Repair Parts.</span><br />
                                    <hr />
                                    <div>
                                        <div class="headerTitle" style="float: left;">Service Agreement Offerings:</div>
                                        <div class="bodyCopyBold" style="float: right;">
                                            For more information Email <a href="mailto:SupportNET@am.sony.com">SupportNET@am.sony.com</a>
                                            , or call <a href="tel:1-877-398-7669">1-877-398-7669</a>.
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <div class="promoCopyBold">Extended Warranty</div>
                                    Extended Warranties extends the standard limited product warranty.<br />
                                    <br />
                                    <div class="promoCopyBold">SupportNET Depot</div>
                                    SupportNET Depot offers priority turn-around time with remedial repairs and periodic maintenance performed by Sony qualified engineers.<br />
                                    <br />
                                    <div class="promoCopyBold">SupportNET Onsite</div>
                                    SupportNET Onsite provides customers with on site repairs performed by Sony qualified field service engineers.<br />
                                    <br />
                                    <div class="promoCopyBold">SupportNET24</div>
                                    SupportNET24 provides onsite support with extended hours 24X7X365.<br />
                                    <br />
                                    <div class="promoCopyBold">Module Advanced eXchange (MAX)</div>
                                    Advance exchange of defective components with a new or refurbished replacement units next business day in most cases.<br />
                                    <br />
                                    <div class="promoCopyBold">Support Pack</div>
                                    For select software products a Support Pack is offered to keep the customer software current with the latest releases.<br />
                                    <br />
                                    <div class="promoCopyBold">SystemWatch&copy;</div>
                                    SystemWatch for business critical applications requiring up to 24 hour remote system support.  SystemWatch provides a single point of contact
                                         for a multivendor system environment.  SystemWatch may include Sony remote monitoring software configured specifically for your system as
                                         well as backup and restore software for rapid recoveries.<br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
