<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.SessionExpired"
    CodeFile="SessionExpired.aspx.vb" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Sony Parts for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>

    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                bgcolor="#363d45" colspan="1" height="57">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td bgcolor="#363d45" colspan="1">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" bgcolor="#f2f5f8" colspan="1">
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td width="427">
                                                            <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server">&nbsp;</h2>
                                                            <br />
                                                            <table height="200" width="427" border="0" role="presentation">
                                                                <tr>
                                                                    <td valign="middle" colspan="1" class="tableData">
                                                                        <br />
                                                                        <asp:Literal ID="litMessage" runat="server" Text="" />
                                                                        <br />
                                                                        <p class="bodyCopyBold">
                                                                            <span class="redAsterick">Please Note:</span> This site
                                                                     requires the use of cookies to maintain your Session. Please ensure your browser
                                                                     and computer security settings do not block them.
                                                                        </p>
                                                                        <p>
                                                                            We do not use these cookies to identify or track you. If you experience repeated
                                                                     Session loss, try restarting your browser or clearing the cookie for this site.
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnBack" runat="server" ImageUrl="images/sp_int_returnToCart_btn.gif"
                                                                            AlternateText="Return To Cart" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" width="10" alt="">
                                                        </td>
                                                        <td>
                                                            <img height="400" src="images/spacer.gif" width="100" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" width="290" bgcolor="#99a8b5" colspan="1">
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
