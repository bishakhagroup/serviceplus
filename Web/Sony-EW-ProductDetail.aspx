<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Sony_EW_ProductDetail"
    EnableViewStateMac="true" CodeFile="Sony-EW-ProductDetail.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register Src="~/UserControl/SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Extended Warranties for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet" />

    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="Form1" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="700">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="ServicePlusNavDisplay" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tbody>
                                            <tr>
                                                <td valign="top" align="right" width="464" background="images/EWPageHeader.jpg"
                                                    bgcolor="#363d45" height="82">
                                                </td>
                                                <td valign="top" width="174" bgcolor="#363d45">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#f2f5f8">
                                                    <h2 id="Label1" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server">Product Details</h2>
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                    <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9">--%>
                                                </td>
                                                <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                    <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9">--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" colspan="2" width="100%" bgcolor="#f2f5f8">
                                                    <table width="100%" border="0" role="presentation">
                                                        <tr>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                            <td>
                                                                &nbsp;<table width="100%" border="0" role="presentation">
                                                                    <tr>
                                                                        <td valign="top" width="100%" bgcolor="#f2f5f8">
                                                                            <table width="100%" border="0" role="presentation">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="100%">
                                                                                        <table width="100%" border="0" role="presentation">
                                                                                            <tr>
                                                                                                <td valign="top">
                                                                                                    <table border="0" width="100%" role="presentation">
                                                                                                        <tr width="100%">
                                                                                                            <td width="90%" valign="top">
                                                                                                                <table align="left" border="0" width="100%" role="presentation">
                                                                                                                    <tr>
                                                                                                                        <td width="100%">
                                                                                                                            <asp:Label ID="lblShortDescription" runat="server" CssClass="headerTitle"></asp:Label>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            &nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <span class="tableHeader">
                                                                                                                                <asp:Label ID="lblModel" runat="server"
                                                                                                                                    CssClass="finderCopyDark"></asp:Label></span>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td width="100%">
                                                                                                                            &nbsp;&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td width="100%">
                                                                                                                            <span id="spnPrice" runat="server" class="tableHeader">Price: &nbsp;<asp:Label ID="lblPrice" runat="server"
                                                                                                                                CssClass="finderCopyDark"></asp:Label><br />
                                                                                                                            </span>
                                                                                                                            <span id="spnCall1800" runat="server" class="redAsterick" visible="false">Call number here.</span>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td>
                                                                                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td class="headerTitle" width="100%">

                                                                                                                            <asp:ImageButton ID="btnOrderEW" runat="server" ImageAlign="Top" ImageUrl="images/sp_int_order_btn.gif"
                                                                                                                                ToolTip="Add to Cart" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                            <td width="10%" align="right" valign="top">
                                                                                                                <img src="images/SonyExtendedWarranty.jpg" runat="server" id="iProductDetails" alt="Sony Extended Warranty" />
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    &nbsp;
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr valign="top">
                                                                                                <td width="100%" valign="top">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                                                                                                        <ContentTemplate>
                                                                                                            <div>
                                                                                                                <asp:Menu ID="mnTab" runat="server" BorderStyle="Solid" Orientation="Horizontal"
                                                                                                                    StaticEnableDefaultPopOutImage="False" OnMenuItemClick="mnTab_MenuItemClick">
                                                                                                                    <Items>
                                                                                                                        <asp:MenuItem ImageUrl="images/Description.gif" Text=" " Value="0"></asp:MenuItem>
                                                                                                                        <asp:MenuItem ImageUrl="images/Highlights.gif" Text=" " Value="1"></asp:MenuItem>
                                                                                                                    </Items>
                                                                                                                </asp:Menu>
                                                                                                                <div style="border-style: none; border-width: thin; border-color: inherit; overflow: auto; height: 150px; background-color: #FFFFFF;">
                                                                                                                    <asp:Label ID="lblDesc" runat="server" CssClass="finderCopyDark" Width="100%"></asp:Label>
                                                                                                                    <asp:Label ID="lblHighlights" runat="server" CssClass="finderCopyDark" Width="100%"
                                                                                                                        Visible="false"></asp:Label>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </ContentTemplate>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="20">
                                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td width="418" bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td width="174" bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
