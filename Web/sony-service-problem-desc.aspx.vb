Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports System.Text.RegularExpressions

Namespace ServicePLUSWebApp

    Partial Class sony_service_problem_desc
        Inherits SSL
        'Inherits System.Web.UI.Page
        Dim oServiceInfo As New ServiceItemInfo
        Dim sModelName As String

        Protected Sub imgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnNext.Click

            If validateForm() = False Then
                Return
            End If

            If rdProbInter.SelectedValue = "1" Then
                oServiceInfo.isIntermittentProblem = True
            Else
                oServiceInfo.isIntermittentProblem = False
            End If
            If rdPrevntMaint.SelectedValue = "1" Then
                oServiceInfo.isPreventiveMaintenance = True
            Else
                oServiceInfo.isPreventiveMaintenance = False
            End If

            oServiceInfo.ProblemDescription = txtProbDescr.Text
            FindDepotCenterAddress(oServiceInfo.ShipStateAbbr.ToString())

            Session("snServiceInfo") = oServiceInfo
            If oServiceInfo.DepotCenterAddress = String.Empty Then
                'TODO: Put a blank page and show message "For depot service for models not found here, please contact the Sony national service line at 866-766-9272, then select option 2."
            End If
            Response.Redirect("sony-service-confirmation.aspx") '?model=" + sModelName)
            'Response.Redirect("sony-service-confirmation.aspx?model=" + sModelName)
        End Sub

        Private Sub FindDepotCenterAddress(ByVal pState As String)

            Dim cm As New CatalogManager
            Dim sAddr As String

            If pState.Length > 0 Then
                sAddr = cm.GetDepotServiceCenter(pState, sModelName)
                If sAddr <> String.Empty Then
                    sAddr = sAddr.Replace("!", "<br>")
                    oServiceInfo.DepotCenterAddress = sAddr.Split(("#").ToCharArray)(0)
                    oServiceInfo.DepotCenterAddressID = Convert.ToInt32(sAddr.Split(("#").ToCharArray)(1))
                Else
                    'oServiceInfo.DepotCenterAddress = "For depot service for models not found here, please contact the Sony national service line at 866-766-9272, then select option 2."
                    oServiceInfo.DepotCenterAddress = String.Empty
                End If
            Else
                oServiceInfo.DepotCenterAddress = "NA"
            End If
        End Sub

        Private Function validateForm() As Boolean

            Dim isValid As Boolean = False
            Dim isPrevSel As Boolean = False
            Dim isProbIntSel As Boolean = False

            If rdProbInter.SelectedValue = "" Then
                lblProbInter.CssClass = "redAsterick"
                isProbIntSel = True
            Else
                lblProbInter.CssClass = "tableData"
                isProbIntSel = False
            End If

            If rdPrevntMaint.SelectedValue = "" Then
                lblProbMaint.CssClass = "redAsterick"
                isPrevSel = True
            Else
                lblProbMaint.CssClass = "tableData"
                isPrevSel = False
            End If

            If Trim(txtProbDescr.Text) = "" Then
                lblDescr.CssClass = "redAsterick"
                isValid = True
            Else
                lblDescr.CssClass = "tableData"
                isValid = False
            End If

            If isValid = True Or isPrevSel = True Or isProbIntSel = True Then
                ErrorValidation.Visible = True
                Return False
            Else
                Return True
            End If

        End Function

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            'If Not Session.Item("customer") Is Nothing Then
            '    Response.Redirect("sony-service-sn.aspx")
            'End If

            If Not Session.Item("ServiceModelNumber") Is Nothing Then
                sModelName = Session.Item("ServiceModelNumber").ToString()
            Else
                Response.Redirect("sony-repair.aspx", True)
                Response.End()
            End If

            'If Not Request.QueryString("model") Is Nothing Then
            '    sModelName = Request.QueryString("model").Trim()
            'End If

            lblSubHdr.InnerText = sModelName + " " + GetGlobalResourceObject("Resource", "repair_svcacc_dept_msg")

            oServiceInfo = Session("snServiceInfo")

            If Trim(oServiceInfo.ProblemDescription) <> "" Then
                loadDatatoControl()
            End If

        End Sub
        Private Sub loadDatatoControl()

            txtProbDescr.Text = oServiceInfo.ProblemDescription

            If oServiceInfo.isIntermittentProblem = True Then
                rdProbInter.SelectedValue = "1"
            Else
                rdProbInter.SelectedValue = "0"
            End If
            If oServiceInfo.isPreventiveMaintenance = True Then
                rdPrevntMaint.SelectedValue = "1"
            Else
                rdPrevntMaint.SelectedValue = "0"
            End If

        End Sub
    End Class
End Namespace
