Imports System.IO
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class SoftwareTC
        Inherits SSL

        Public isLoggedIn As Boolean = False
        Public bTrack As Boolean = False
        Public isEnglish As Boolean = False
        Public sKitPartNumber As String = ""
        'Dim objUtilties As Utilities = New Utilities
        Public LanguageForIDP As String


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(False)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim software As Software
            Dim itemNumber As Integer

            Try
                Session.Add("Accept", False)
                LanguageForIDP = HttpContextManager.GlobalData.LanguageForIDP

                isLoggedIn = (HttpContextManager.Customer IsNot Nothing)
                isEnglish = HttpContextManager.GlobalData.IsEnglish

                If Not Page.IsPostBack Then
                    If (Request.QueryString("ln") Is Nothing) Or (Session.Item("software") Is Nothing) Then
                        Response.Redirect("SessionExpired.aspx", True)
                    Else
                        itemNumber = Convert.ToInt16(Request.QueryString("ln"))
                        If (itemNumber >= 0) And (Session.Item("software") IsNot Nothing) Then
                            software = CType(Session.Item("software"), ArrayList)(itemNumber)
                            Session.Add("FreeDownloadSoftware", software)
                        ElseIf (Session("SelectedFreeSoftwareBeforeLoggedIn") IsNot Nothing) And (Session("FreeDownloadSoftware") IsNot Nothing) Then
                            software = CType(Session("FreeDownloadSoftware"), Software)
                            If software.FreeOfCharge And software.Tracking And isLoggedIn Then
                                Session.Item("Accept") = True
                                Response.Redirect("SoftwareFreeDownload.aspx", False)
                            End If
                        End If
                    End If
                End If

                If Session("FreeDownloadSoftware") IsNot Nothing Then
                    software = CType(Session("FreeDownloadSoftware"), Software)
                    sKitPartNumber = software.KitPartNumber
                    If software.FreeOfCharge And software.Tracking Then
                        bTrack = True
                    Else
                        bTrack = False
                    End If
                End If

                'If bTrack = True And bCustomer = False Then
                '	Accept.Visible = False
                '	Accept.Visible = True
                '	lnkLogin.Visible = True
                '	lnkLogin.HRef = "javascript:checkLogin(" + Request.QueryString("ln") + ");"
                '	Dim responseurl As String = "SignIn-Register.aspx"
                '	Response.Redirect(responseurl + "?post=ssmfree")
                'Else
                lnkLogin.Visible = False
                Accept.Visible = True
                'End If

                If Request.QueryString("readTC") IsNot Nothing And Session("SelectedFreeSoftwareBeforeLoggedIn") Is Nothing Then
                    lblErrorMsg.Text = "Please accept the Terms and Conditions.  "
                Else
                    lblErrorMsg.Text = ""
                End If
            Catch exThreadAbort As System.Threading.ThreadAbortException
                'Do nothing '6708
            Catch fex As FormatException    ' Occurs if user modifies query string "&ln=" to be something other than an integer
                lblErrorMsg.Text = "Modified URL detected. Please go back and try again."
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub Decline_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Decline.Click
            Try
                Response.Redirect("TCDeclined.aspx")
            Catch ex As Threading.ThreadAbortException
                '-- do nothing
            End Try

        End Sub

        Private Sub Accept_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Accept.Click
            If Not chkRead.Checked Then
                lblErrorMsg.Text = "Please accept the Terms and Conditions."
                Exit Sub
            Else
                If bTrack And HttpContextManager.Customer Is Nothing Then
                    If Session("FreeDownloadSoftware") IsNot Nothing Then
                        Response.Redirect("SignIn-Register.aspx?post=ssmfree")
                    Else
                        Accept.Visible = False
                        lnkLogin.Visible = True
                        lnkLogin.HRef = "javascript:checkLogin(" + Request.QueryString("ln") + ");"
                    End If
                Else
                    lblErrorMsg.Text = ""
                    If Request.QueryString("ln") IsNot Nothing Then
                        If Session("FreeDownloadSoftware") IsNot Nothing Then
                            Response.Redirect("SoftwareFreeDownload.aspx")
                        End If
                    End If
                End If
            End If

        End Sub

    End Class

End Namespace
