Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class traininmg_student
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private m_cr As CourseReservation
        Private m_order As Sony.US.ServicesPLUS.Core.Order
        Private customer As Customer

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    Dim cp As CourseParticipant = GetParticipant()
                    If Not cp Is Nothing And Not m_order Is Nothing Then
                        BuildDDLs()

                        Me.lnkOrderNumber.NavigateUrl = "training-order.aspx?OrderNumber=" + m_order.OrderNumber
                        Me.lnkOrderNumber.Text = m_order.OrderNumber
                        Me.labelOrderDate.Text = m_order.OrderDate.ToString("MM/dd/yyyy")
                        Me.labelUpdateBy.Text = m_order.Customer.SIAMIdentity
                        Me.labelUpdateDate.Text = m_cr.UpdateDate.ToString("MM/dd/yyyy")
                        Me.labelClassNumber.Text = m_cr.CourseSchedule.PartNumber
                        Me.labelCourseNumber.Text = m_cr.CourseSchedule.Course.Number
                        Me.labelCourseTitle.Text = m_cr.CourseSchedule.Course.Title
                        Me.labelStartDate.Text = m_cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy")
                        Me.labelEndDate.Text = m_cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy")
                        Me.labelLocation.Text = m_cr.CourseSchedule.Location.Name

                        Me.txtCompany.Text = cp.Company
                        Me.txtFirstName.Text = cp.FirstName
                        Me.txtLastName.Text = cp.LastName
                        Me.txtAddress1.Text = cp.Address1
                        Me.txtAddress2.Text = cp.Address2
                        Me.txtAddress3.Text = cp.Address3
                        Me.txtCity.Text = cp.City
                        Me.ddlState.SelectedValue = cp.State
                        Me.txtCountryCode.Text = cp.Country
                        Me.txtPostalCode.Text = cp.Zip
                        Me.txtEMail.Text = cp.EMail
                        Me.txtPhone.Text = cp.Phone
                        Me.txtExtension.Text = cp.Extension
                        Me.txtFax.Text = cp.Fax
                        Me.labelDateReserved.Text = cp.DateReservedDisplay
                        Me.labelDateCancelled.Text = cp.DateCanceledDisplay
                        Me.labelDateWaited.Text = cp.DateWaitedDisplay
                        Me.txtNotes.Text = cp.Notes
                        Me.ddlStatus.SelectedValue = cp.Status
                        Me.labelStudentUpdatedBy.Text = cp.UpdatedSIAMID
                        Me.labelStudentUpdateDate.Text = cp.UpdateDate.ToString("MM/dd/yyyy")

                    End If


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Private Sub BuildDDLs()
            ddlState.Items.Add(New ListItem("Select One", ""))
            ddlState.Items.Add(New ListItem("AK", "AK"))
            ddlState.Items.Add(New ListItem("AL", "AL"))
            ddlState.Items.Add(New ListItem("AR", "AR"))
            ddlState.Items.Add(New ListItem("AZ", "AZ"))
            ddlState.Items.Add(New ListItem("CA", "CA"))
            ddlState.Items.Add(New ListItem("CO", "CO"))
            ddlState.Items.Add(New ListItem("CT", "CT"))
            ddlState.Items.Add(New ListItem("DC", "DC"))
            ddlState.Items.Add(New ListItem("DE", "DE"))
            ddlState.Items.Add(New ListItem("FL", "FL"))
            ddlState.Items.Add(New ListItem("GA", "GA"))
            ddlState.Items.Add(New ListItem("HI", "HI"))
            ddlState.Items.Add(New ListItem("IA", "IA"))
            ddlState.Items.Add(New ListItem("ID", "ID"))
            ddlState.Items.Add(New ListItem("IL", "IL"))
            ddlState.Items.Add(New ListItem("IN", "IN"))
            ddlState.Items.Add(New ListItem("KS", "KS"))
            ddlState.Items.Add(New ListItem("KY", "KY"))
            ddlState.Items.Add(New ListItem("LA", "LA"))
            ddlState.Items.Add(New ListItem("MA", "MA"))
            ddlState.Items.Add(New ListItem("MD", "MD"))
            ddlState.Items.Add(New ListItem("ME", "ME"))
            ddlState.Items.Add(New ListItem("MI", "MI"))
            ddlState.Items.Add(New ListItem("MN", "MN"))
            ddlState.Items.Add(New ListItem("MO", "MO"))
            ddlState.Items.Add(New ListItem("MS", "MS"))
            ddlState.Items.Add(New ListItem("MT", "MT"))
            ddlState.Items.Add(New ListItem("NC", "NC"))
            ddlState.Items.Add(New ListItem("ND", "ND"))
            ddlState.Items.Add(New ListItem("NE", "NE"))
            ddlState.Items.Add(New ListItem("NH", "NH"))
            ddlState.Items.Add(New ListItem("NJ", "NJ"))
            ddlState.Items.Add(New ListItem("NM", "NM"))
            ddlState.Items.Add(New ListItem("NV", "NV"))
            ddlState.Items.Add(New ListItem("NY", "NY"))
            ddlState.Items.Add(New ListItem("OH", "OH"))
            ddlState.Items.Add(New ListItem("OK", "OK"))
            ddlState.Items.Add(New ListItem("OR", "OR"))
            ddlState.Items.Add(New ListItem("PA", "PA"))
            ddlState.Items.Add(New ListItem("RI", "RI"))
            ddlState.Items.Add(New ListItem("SC", "SC"))
            ddlState.Items.Add(New ListItem("SD", "SD"))
            ddlState.Items.Add(New ListItem("TN", "TN"))
            ddlState.Items.Add(New ListItem("TX", "TX"))
            ddlState.Items.Add(New ListItem("UT", "UT"))
            ddlState.Items.Add(New ListItem("VA", "VA"))
            ddlState.Items.Add(New ListItem("VT", "VT"))
            ddlState.Items.Add(New ListItem("WA", "WA"))
            ddlState.Items.Add(New ListItem("WI", "WI"))
            ddlState.Items.Add(New ListItem("WV", "WV"))
            ddlState.Items.Add(New ListItem("WY", "WY"))

            ddlStatus.Items.Add(New ListItem("Reserved", "R"))
            ddlStatus.Items.Add(New ListItem("Wait Listed", "W"))
            ddlStatus.Items.Add(New ListItem("Cancelled", "C"))
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            Dim objPCILogger As New PCILogger() '6524
            Try
                If Not ValidateForm() Then
                    ErrorLabel.Visible = True
                    ErrorLabel.Text = "Please make sure all input is valid."
                    Return
                End If

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                objPCILogger.Message = "Update Training Student Success."
                objPCILogger.EventType = EventType.Update
                '6524 end

                Dim lineNo As Integer = Convert.ToInt16(Request.QueryString("LineNo"))
                m_cr = Session.Item("current_trainingreservation")
                m_order = Session.Item("current_trainingorder")
                If Not m_cr Is Nothing Then
                    Dim cp As CourseParticipant = m_cr.Participants(lineNo)
                    Dim cm As CourseManager = New CourseManager
                    Dim oldStatus As Char = cp.Status
                    cp.Company = Me.txtCompany.Text
                    cp.FirstName = Me.txtFirstName.Text
                    cp.LastName = Me.txtLastName.Text
                    cp.Address1 = Me.txtAddress1.Text
                    cp.Address2 = Me.txtAddress2.Text
                    cp.Address3 = Me.txtAddress3.Text
                    cp.City = Me.txtCity.Text
                    cp.State = Me.ddlState.SelectedValue
                    cp.Zip = Me.txtPostalCode.Text
                    cp.Country = Me.txtCountryCode.Text
                    cp.EMail = Me.txtEMail.Text
                    cp.Phone = Me.txtPhone.Text
                    cp.Extension = Me.txtExtension.Text
                    cp.Fax = Me.txtFax.Text
                    cp.Notes = Me.txtNotes.Text
                    cp.Status = Me.ddlStatus.SelectedValue
                    If oldStatus <> "R" And cp.Status = "R" Then
                        cm.ProcessTrainingStudent(cp, m_cr.CourseSchedule, True) 'need to check vacancy
                    Else
                        cm.ProcessTrainingStudent(cp, m_cr.CourseSchedule, False)
                    End If
                End If


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Update Training Student Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
        Private Function GetParticipant() As CourseParticipant
            Dim cp As CourseParticipant = Nothing
            Dim lineNo As Integer = -1


            If Not Request.QueryString("LineNo") Is Nothing Then
                lineNo = Convert.ToInt16(Request.QueryString("LineNo"))
                m_cr = Session.Item("current_trainingreservation") 'already known
                m_order = Session.Item("current_trainingorder")
                cp = m_cr.Participants(lineNo)
            ElseIf Not Request.QueryString("SearchedLineNo") Is Nothing Then
                lineNo = Convert.ToInt16(Request.QueryString("SearchedLineNo"))
                cp = CType(Session.Item("searchedparticipants"), CourseParticipant())(lineNo)
                'Now need to get the order and reservation for the participant
                Dim cm As New CourseManager
                m_cr = cm.GetReservation(cp.ReservationNumber)
                Session.Add("current_trainingreservation", m_cr)
                Dim om As OrderManager = New OrderManager
                m_order = om.GetOrder(cp.ReservationNumber, False)
                If Not m_order Is Nothing Then
                    om.GetOrderInvoices(m_order)
                    Session.Add("current_trainingorder", m_order)
                Else
                    ErrorLabel.Text = "Order " + cp.ReservationNumber + " does not exist."
                End If
                '***************Changes done for Bug# 1212 Ends here***************

            End If
            Return cp
        End Function

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            If Me.txtCompany.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtFirstName.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtLastName.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtAddress1.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtCity.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtPostalCode.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtEMail.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtPhone.Text Is Nothing Then
                bValid = False
            End If
            If Me.txtCountryCode.Text Is Nothing Then
                bValid = False
            End If

            If txtAddress1.Text.Length > 35 Then
                ErrorLabel.Visible = True
                LabelLine1Star.Text = "Street Address must be 35 characters or less in length."
                bValid = False
                'txtAddress1.Focus()
            Else
                LabelLine1Star.Text = ""
            End If

            If txtAddress2.Text.Length > 35 Then
                ErrorLabel.Visible = True
                LabelLine2Star.Text = "Address 2nd Line must be 35 characters or less in length."
                bValid = False
                'txtAddress2.Focus()
            Else
                LabelLine2Star.Text = ""
            End If

            If txtAddress3.Text.Length > 35 Then
                ErrorLabel.Visible = True
                LabelLine3Star.Text = "Address 3rd Line must be 35 characters or less in length."
                bValid = False
                'txtAddress3.Focus()
            Else
                LabelLine3Star.Text = ""
            End If

            If txtCity.Text.Length > 35 Then
                ErrorLabel.Visible = True
                LabelLine4Star.Text = "City must be 35 characters or less in length."
                bValid = False
                'txtCity.Focus()
            Else
                LabelLine4Star.Text = ""
            End If

            Return bValid
        End Function

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-student-search.aspx")
        End Sub
    End Class

End Namespace
