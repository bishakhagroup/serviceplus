﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CancelOrder.ascx.vb"
    Inherits="e_CancelOrder" %>
<table>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSOrderNumber" CssClass="BodyHead" runat="server">Order Number</SPS:SPSLabel>
        </td>
       </tr>
      
       <tr> 
       <td>
            <SPS:SPSTextBox ID="txtOrderNumber" CssClass="BodyHead" runat="server">0010775640</SPS:SPSTextBox>
        </td>
       </tr>
        <tr>
        <td>
            <SPS:SPSLabel ID="lblReasonforRejection" CssClass="BodyHead" runat="server">Reason for Rejection</SPS:SPSLabel>
        </td>
       </tr>
    <tr>       
        <td>
            <SPS:SPSTextBox ID="txtReasonforRejection" CssClass="BodyHead" runat="server">04</SPS:SPSTextBox>

        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="lblLineNumbers" CssClass="BodyHead" runat="server">Line Number</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtLineNumbers" CssClass="BodyHead" runat="server" Height="50px" TextMode="MultiLine" >00010
00020</SPS:SPSTextBox>

        </td>
    </tr>
</table>
