Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class Role_Function_Mapping
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()

            thisPage = "CustomerInfo.aspx"

            Try
                If Not Request.QueryString("IsTraining") Is Nothing Then
                    If CType(Request.QueryString("IsTraining"), Boolean) Then
                        thisPage = "training-class-search.aspx"
                    Else
                        thisPage = "CustomerInfo.aspx"
                    End If
                Else
                    thisPage = "CustomerInfo.aspx"
                End If
            Catch ex As Exception
                thisPage = "CustomerInfo.aspx"
            End Try
        End Sub




#End Region

#Region "   Page Variables  "

        Private sa As New SecurityAdministrator
        Private roleList As Role()
        Private functionalityList As Sony.US.siam.Functionality()
        Private r As SiamResponse
        Public thisPage As String = String.Empty

#End Region

#Region "   Events  "

        Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ErrorLabel.Visible = False
            lblMessage.Text = ""
            Try
                If Not IsPostBack Then
                    PopulateRoles()
                    PopulateFunctionalities()
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSave_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            ''Praveen kumar N.B added on 14-12-2009
            'Event logging code block added for Role/Function
            Dim objPCILogger As New PCILogger()
            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
            objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
            objPCILogger.EventOriginMethod = "btnSave_click"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = System.Environment.UserName ''HttpContext.Current.User.Identity.Name
            objPCILogger.EventType = EventType.Update
            If Not Session.Item("siamuser") Is Nothing Then
                'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                'objPCILogger.B2B_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId'6524
                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
            End If


            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

            Dim strAddFun As String = "0"
            Dim strDelFun As String = "0"
            Dim lstItem As ListItem
            For Each lstItem In lstSelected.Items
                If lstItem.Value.IndexOf("~") > 0 Then
                    strAddFun = strAddFun + "," + lstItem.Value.Substring(0, lstItem.Value.Length - 1)
                End If
            Next lstItem

            For Each lstItem In lstAvailable.Items
                If lstItem.Value.IndexOf("~") <= 0 Then
                    strDelFun = strDelFun + "," + lstItem.Value
                End If
            Next lstItem
            Try
                r = sa.UpdateRoleFunctionalityMapping(ddlRole.SelectedItem.Value, strAddFun, strDelFun)

                If r.SiamResponseMessage = Messages.UPDATE_ROLEFUNMAP_COMPLETED.ToString() Then
                    For Each lstItem In lstSelected.Items
                        If lstItem.Value.IndexOf("~") > 0 Then
                            lstItem.Value = lstItem.Value.Substring(0, lstItem.Value.Length - 1)
                        End If
                    Next lstItem

                    lblMessage.Text = "Role updated succesfully"
                    ''Praveen kumar N.B added on 18-12-2009
                    'Event logging code block added for Role/Function
                    objPCILogger.Message = ddlRole.SelectedItem.Text & " role is updated."
                    objPCILogger.IndicationSuccessFailure = "Success"
                Else
                    lblMessage.Text = "Role updated failed"
                    objPCILogger.Message = ddlRole.SelectedItem.Text & "Role updated failed"
                    objPCILogger.IndicationSuccessFailure = "Failure"
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Dim lstItem As ListItem
                Dim lstItemToCopy As ListItem
                For Each lstItem In lstSelected.Items
                    'If lstItem.Selected = True Then
                    lstItemToCopy = New ListItem()
                    lstItemToCopy.Text = lstItem.Text
                    lstItemToCopy.Value = lstItem.Value
                    lstAvailable.Items.Add(lstItemToCopy)
                    'lstItem.Enabled = False
                    'End If
                Next lstItem

                lstSelected.Items.Clear()
                For Each lstItem In lstAvailable.Items
                    If lstItem.Value.IndexOf("~") <= 0 Then
                        lstItemToCopy = New ListItem()
                        lstItemToCopy.Text = lstItem.Text
                        lstItemToCopy.Value = lstItem.Value
                        lstSelected.Items.Add(lstItemToCopy)
                        lstItem.Enabled = False
                    End If
                Next lstItem

                Dim j, k As Int16
                k = lstAvailable.Items.Count
                While k > 0
                    For j = 0 To k - 1
                        If lstAvailable.Items(j).Enabled = False Then
                            lstAvailable.Items.Remove(lstAvailable.Items(j))
                            Exit For
                        End If
                        'j += 1
                    Next
                    j = 0
                    k -= 1
                End While
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
            Try
                Dim lstItem As ListItem
                Dim lstItemToCopy As ListItem
                For Each lstItem In lstAvailable.Items
                    If lstItem.Selected = True Then
                        lstItemToCopy = New ListItem()
                        lstItemToCopy.Text = lstItem.Text
                        lstItemToCopy.Value = lstItem.Value
                        lstSelected.Items.Add(lstItemToCopy)
                        'lstItem.Enabled = False
                    End If
                Next lstItem

                Dim j, k As Int16
                k = lstAvailable.Items.Count
                While k > 0
                    For j = 0 To k - 1
                        If lstAvailable.Items(j).Selected = True Then
                            lstAvailable.Items.Remove(lstAvailable.Items(j))
                            Exit For
                        End If
                        'j += 1
                    Next
                    j = 0
                    k -= 1
                End While
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnDeSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeSelect.Click
            Try
                Dim lstItem As ListItem
                Dim lstItemToCopy As ListItem
                For Each lstItem In lstSelected.Items
                    If lstItem.Selected = True Then
                        lstItemToCopy = New ListItem()
                        lstItemToCopy.Text = lstItem.Text
                        lstItemToCopy.Value = lstItem.Value
                        lstAvailable.Items.Add(lstItemToCopy)
                        'lstItem.Enabled = False
                    End If
                Next lstItem
                Dim j, k As Int16
                k = lstSelected.Items.Count
                While k > 0
                    For j = 0 To k - 1
                        If lstSelected.Items(j).Selected = True Then
                            lstSelected.Items.Remove(lstSelected.Items(j))
                            Exit For
                        End If
                        'j += 1
                    Next
                    j = 0
                    k -= 1
                End While
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.SelectedIndexChanged
            Try
                PopulateFunctionalities()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub dgRoleFunctionalityMapping_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRoleFunctionalityMapping.ItemCommand
            'Try
            '    If e.CommandName = "Delete" Then
            '        Dim objCustomer As Customer = New Customer()
            '        objCustomer.SIAMIdentity = e.CommandArgument.ToString()
            '        Dim objCustomerManager As CustomerManager = New CustomerManager()
            '        objCustomerManager.DeleteCustomer(objCustomer)
            '        If Not objCustomer.SequenceNumber Is System.DBNull.Value Then
            '            Dim mySA As New SecurityAdministrator
            '            mySA.DeleteUser(e.CommandArgument.ToString())
            '            If hdnLastOpr.Value = "A" Then
            '                showAllUsers()
            '            Else
            '                r = sa.GetUsersShallow(hdnLastOpr.Value)
            '                If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
            '                    userlist = r.SiamPayLoad
            '                    dgRoleFunctionalityMapping.Visible = True
            '                    PopulateDataGridWithUsers()
            '                Else
            '                    lblFoundResut.Text = "No Record Found "
            '                    dgRoleFunctionalityMapping.Visible = False
            '                    ErrorLabel.Text = "No records returned."
            '                End If
            '            End If
            '        End If
            '        objCustomer = Nothing
            '        objCustomerManager = Nothing
            '    End If
            'Catch ex As Exception
            '    'ErrorLabel.Text = ex.Message.ToString()
            'End Try
        End Sub

        Protected Sub dgRoleFunctionalityMapping_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRoleFunctionalityMapping.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgRoleFunctionalityMapping_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRoleFunctionalityMapping.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                    If Not lnkbtn Is Nothing Then
                        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub pager(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgRoleFunctionalityMapping.PageIndexChanged
            'If Session.Item("SearchPattern") Is Nothing Then
            '    r = sa.GetUsersShallow()
            '    userlist = r.SiamPayLoad
            'Else
            '    r = sa.GetUsersShallow(Session.Item("SearchPattern").ToString())
            '    userlist = r.SiamPayLoad
            'End If
            'If r.SiamResponseCode = 147 Then
            '    dgRoleFunctionalityMapping.CurrentPageIndex = e.NewPageIndex
            '    PopulateDataGridWithUsers()
            'Else
            '    Session.Remove("LastError")
            '    Session.Add("LastError", r.SiamResponseMessage)
            '    Response.Redirect("ErrorPage.aspx")
            'End If
        End Sub

        Private Sub sortlist(ByVal sender As System.Object, ByVal e As DataGridSortCommandEventArgs) Handles dgRoleFunctionalityMapping.SortCommand
            'If Session.Item("SearchPattern") Is Nothing Then
            '    userlist = sa.GetUsersShallow().SiamPayLoad
            'Else
            '    userlist = sa.GetUsersShallow(Session.Item("SearchPattern").ToString()).SiamPayLoad
            'End If
            'If e.SortExpression = "Name" Then
            '    Array.Sort(userlist)
            'End If
            'If e.SortExpression = "EmailAddress" Then
            '    Array.Sort(userlist, New ShallowUser.EmailComparer)
            'End If
            'If e.SortExpression = "Company" Then
            '    Array.Sort(userlist, New ShallowUser.CompanyComparer)
            'End If
            'dgRoleFunctionalityMapping.DataSource = userlist
            'dgRoleFunctionalityMapping.DataBind()
        End Sub

#End Region

#Region "   Methods     "

        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            dgRoleFunctionalityMapping.Columns(0).Visible = True

            Try
                Dim user As User = Session(MyBase.CustomerSessionVariable)
                Dim caller As String = String.Empty

                'If Not Request.QueryString("Identity") Is Nothing Then DeleteUser(Request.QueryString("Identity"))
                If Not sender.ID Is Nothing Then caller = sender.ID.ToString()


                Select Case caller
                    Case Page.ID.ToString()
                        'Case SearchButton_SPLS.ID.ToString()
                        '    doSearchForUser_SPLS()
                    Case btnSave.ID.ToString()
                        'doSearchForUser()
                    Case btnCancel.ID.ToString()
                        'showAllUsers()
                    Case dgRoleFunctionalityMapping.ID.ToString()

                End Select

                Me.writeScriptsToPage()




            Catch ex As Exception
            End Try
        End Sub

#Region "       Populate List Controls         "
        Private Sub PopulateRoles()
            r = (New SecurityAdministrator).GetRoles()
            If r.SiamResponseMessage = Messages.GET_ROLES_COMPLETED.ToString() Then
                roleList = r.SiamPayLoad
                ddlRole.Visible = True
                ddlRole.DataTextField = "Name"
                ddlRole.DataValueField = "RoleId"
                ddlRole.DataSource = roleList
                ddlRole.DataBind()
            Else
                ErrorLabel.Text = "No records returned."
            End If
        End Sub
        Private Function AvailablaValue(ByVal objFunctionality As Sony.US.siam.Functionality) As Boolean
            Return Not objFunctionality.AvailableStatus
        End Function
        Private Function SelectedValue(ByVal objFunctionality As Sony.US.siam.Functionality) As Boolean
            Return objFunctionality.AvailableStatus
        End Function

        Private Sub PopulateFunctionalities()
            'Dim array As Functionality()
            'Dim match As Predicate(Of Functionality)
            'Dim returnValue As Functionality()
            lstAvailable.Items.Clear()
            lstSelected.Items.Clear()
            If ddlRole.Items.Count > 0 Then
                ddlRole.AutoPostBack = False

                r = (New SecurityAdministrator).GetFunctionalities(Convert.ToInt32(ddlRole.SelectedValue))
                ddlRole.AutoPostBack = True
                If r.SiamResponseMessage = Messages.GET_FUNCTIONALITIES_COMPLETED.ToString() Then
                    functionalityList = r.SiamPayLoad

                    'lstAvailable.Visible = True
                    'lstAvailable.DataTextField = "Name"
                    'lstAvailable.DataValueField = "FunctionalityId"

                    'match = New System.Predicate(Of Functionality)(AddressOf AvailablaValue)
                    'returnValue = array.FindAll(array, match)
                    'lstAvailable.DataSource = returnValue
                    'lstAvailable.DataBind()
                    Dim lstFuncItem As Sony.US.siam.Functionality
                    Dim lstItem As ListItem
                    For Each lstFuncItem In functionalityList
                        If (Not lstFuncItem Is Nothing) Then
                            If lstFuncItem.AvailableStatus = 0 Then
                                lstItem = New ListItem(lstFuncItem.Name, lstFuncItem.FunctionalityId.ToString() + "~")
                                lstAvailable.Items.Add(lstItem)
                            Else
                                lstItem = New ListItem(lstFuncItem.Name, lstFuncItem.FunctionalityId)
                                lstSelected.Items.Add(lstItem)
                            End If
                        End If
                    Next lstFuncItem

                    If lstAvailable.Items.Count < 1 Then
                        btnSelect.Enabled = False
                    Else
                        btnSelect.Enabled = True
                    End If
                Else
                    ErrorLabel.Text = "No records returned."
                    btnSelect.Enabled = False
                End If
            End If
        End Sub
#End Region

#Region "       Show All Users      "
        'Private Sub doSearchForUser()
        '    Dim srchPattern As String = String.Empty

        '    If txtSubscriptionID.Text.Trim().Length > 0 Then
        '        srchPattern = srchPattern + " and LOWER(subscriptionid) like '%" + txtSubscriptionID.Text.ToLower() + "%'"
        '        Session.Remove("SearchPattern")
        '        Session.Add("SearchPattern", srchPattern)
        '        r = sa.GetUsersShallow(srchPattern, True)
        '        dgRoleFunctionalityMapping.Columns(0).Visible = False
        '    Else
        '        If txtUserID.Text.Length > 0 Then
        '            srchPattern = srchPattern + " and (LOWER(username) like '%" + txtUserID.Text.Trim().ToLower() + "%')"
        '        End If
        '        If NameTextBox.Text.Length > 0 Then
        '            srchPattern = srchPattern + " and (LOWER(LastName) like '%" + NameTextBox.Text.ToLower() + "%' or LOWER(FirstName) like '%" + NameTextBox.Text.ToLower() + "%')"
        '        End If

        '        If EmailTextBox.Text.Length > 0 Then
        '            srchPattern = srchPattern + " and LOWER(emailaddress) like '%" + EmailTextBox.Text.ToLower() + "%'"
        '        End If

        '        If CompanyTextBox.Text.Length > 0 Then
        '            srchPattern = srchPattern + " and LOWER(companyname) like '%" + CompanyTextBox.Text.ToLower() + "%'"
        '        End If

        '        ' included user id in the search criterion
        '        ' Added by Deepa V, Mar 23,2006
        '        If txtSiamId.Text.Trim().Length > 0 Then
        '            If IsNumeric(Convert.ToInt64(txtSiamId.Text.Trim())) Then
        '                srchPattern = srchPattern + " and LOWER(userid) like '%" + txtSiamId.Text.Trim() + "%'"
        '            Else
        '                ErrorLabel.Text = "Please Enter only numeric value for User ID."
        '                ErrorLabel.Visible = True
        '                Response.End()
        '            End If

        '        End If

        '        'Added for CR Issue E496 by Satyam
        '        'Commented by chandra on July, 06 2007
        '        'If txtSubscriptionID.Text.Trim().Length > 0 Then
        '        '    If IsNumeric(Convert.ToInt64(txtSubscriptionID.Text.Trim())) Then
        '        '        srchPattern = srchPattern + " and LOWER(userid) like '%" + txtSubscriptionID.Text.Trim() + "%'"
        '        '    Else
        '        '        ErrorLabel.Text = "Please Enter valid subscription ID."
        '        '        ErrorLabel.Visible = True
        '        '        Response.End()
        '        '    End If
        '        'End If
        '        Session.Remove("SearchPattern")
        '        Session.Add("SearchPattern", srchPattern)
        '        r = sa.GetUsersShallow(srchPattern)
        '    End If

        '    If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
        '        userlist = r.SiamPayLoad
        '        'If Not userlist Is Nothing Then
        '        dgRoleFunctionalityMapping.Visible = True
        '        PopulateDataGridWithUsers()
        '        hdnLastOpr.Value = srchPattern
        '        'Else
        '        'End If
        '    Else
        '        lblFoundResut.Text = "No Record Found "
        '        dgRoleFunctionalityMapping.Visible = False
        '        ErrorLabel.Text = "No records returned."
        '    End If

        'End Sub

        'Private Sub doSearchForUser_SPLS()
        '    Dim srchPattern As String = String.Empty

        '    If txtSubscriptionID.Text.Length > 0 Then
        '        srchPattern = srchPattern + " and LOWER(subscriptionid) like '%" + txtSubscriptionID.Text.ToLower() + "%'"
        '    End If


        '    Session.Remove("SearchPattern")
        '    Session.Add("SearchPattern", srchPattern)
        '    r = sa.GetUsersShallow(srchPattern, True)
        '    If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
        '        userlist = r.SiamPayLoad
        '        'If Not userlist Is Nothing Then
        '        dgRoleFunctionalityMapping.Visible = True
        '        PopulateDataGridWithUsers()
        '        'Else
        '        'End If
        '    Else
        '        ErrorLabel.Text = "No records returned."
        '    End If

        'End Sub

        'Private Sub showAllUsers()
        '    Session.Remove("SearchPattern")
        '    r = sa.GetUsersShallow()
        '    If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
        '        userlist = r.SiamPayLoad
        '        PopulateDataGridWithUsers()
        '        hdnLastOpr.Value = "A"
        '    Else
        '        Session.Remove("LastError")
        '        Session.Add("LastError", r.SiamResponseMessage)
        '        Response.Redirect("ErrorPage.aspx")
        '    End If
        'End Sub

#End Region

        'Private Sub PopulateDataGridWithUsers()
        '    lblFoundResut.Visible = True
        '    If Not userlist Is Nothing Then
        '        lblFoundResut.Text = "Found " + userlist.Length.ToString() + " Records"
        '    Else
        '        lblFoundResut.Text = "No Record Found "
        '    End If
        '    dgRoleFunctionalityMapping.Visible = True
        '    dgRoleFunctionalityMapping.DataSource = userlist
        '    dgRoleFunctionalityMapping.AllowPaging = False
        '    If (Not userlist Is Nothing) Then
        '        If (userlist.Length > dgRoleFunctionalityMapping.PageSize) Then
        '            dgRoleFunctionalityMapping.AllowPaging = True
        '        End If
        '    End If
        '    dgRoleFunctionalityMapping.DataBind()
        'End Sub


#Region "       Helper Methods      "
        Private Sub ApplyDataGridStyle()

            dgRoleFunctionalityMapping.ApplyStyle(GetStyle(500))
            dgRoleFunctionalityMapping.PageSize = 30
            dgRoleFunctionalityMapping.AutoGenerateColumns = False
            dgRoleFunctionalityMapping.AllowPaging = False
            dgRoleFunctionalityMapping.GridLines = GridLines.Horizontal
            dgRoleFunctionalityMapping.AllowSorting = True
            dgRoleFunctionalityMapping.CellPadding = 3
            dgRoleFunctionalityMapping.PagerStyle.Mode = PagerMode.NumericPages
            dgRoleFunctionalityMapping.HeaderStyle.Font.Size = FontUnit.Point(8)
            dgRoleFunctionalityMapping.HeaderStyle.ForeColor = Color.GhostWhite
            dgRoleFunctionalityMapping.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgRoleFunctionalityMapping.ItemStyle.Font.Size = FontUnit.Point(8)
            dgRoleFunctionalityMapping.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub


        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

#End Region


#End Region


    End Class

End Namespace


