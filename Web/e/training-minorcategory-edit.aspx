<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_minorcategory_edit" CodeFile="training-minorcategory-edit.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>training_minorcategory_edit</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 521px; POSITION: absolute; TOP: 8px; HEIGHT: 248px"
				align="center" border="0">
				<TR>
					<TD class="PageHead" style="HEIGHT: 1px" align="center" colSpan="1" height="1" rowSpan="1">Training 
						Course Minor Category</TD>
				</TR>
                <tr>
                    <td align="left" class="PageHead" style="height: 3px">
                        &nbsp;</td>
                </tr>
				<TR>
					<TD class="PageHead" style="HEIGHT: 3px" align="left">
						<asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 12px">
                        &nbsp;</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 107px" vAlign="top" align="left">
						<TABLE id="Table2" style="WIDTH: 560px; HEIGHT: 88px" cellPadding="0"
							width="560" border="0">
							<%--<TR>
          <TD style="WIDTH: 84px; HEIGHT: 1px; align: " vAlign=top align=right 
          right?>
<asp:label id=labelCode runat="server" CssClass="Body"> Code:</asp:label></TD>
          <TD style="HEIGHT: 1px" vAlign=top>
<SPS:SPSTextBox id=txtCode runat="server" CssClass="Body"></SPS:SPSTextBox>
<asp:label id=LabelCodeStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD>
							</TR>--%>
        <TR>
								<TD style="WIDTH: 84px; HEIGHT: 29px"vAlign="top" align="right">
									<asp:label id="labelDescription" runat="server" CssClass="Body">Description:</asp:label></TD>
								<TD style="HEIGHT: 29px" vAlign="top">
									<SPS:SPSTextBox id="txtDescription" runat="server" CssClass="Body" Width="464px" Height="26px" ></SPS:SPSTextBox>
<asp:label id=LabelDescriptionStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD></TR>
							<TR>
								<TD style="WIDTH: 84px; HEIGHT: 20px" align="right" vAlign=top colSpan="1" rowSpan="1">
									<asp:label id="labelCapacity" runat="server" CssClass="Body"> Major Category:</asp:label></TD>
								<TD style="HEIGHT: 20px" vAlign=top colSpan="1" rowSpan="1">
									<asp:DropDownList id="ddlMajorCategory" runat="server" CssClass="Body" Width="176px"></asp:DropDownList>
<asp:label id=LabelMajorCategoryStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD>
							</TR>
                            <tr>
                                <td align="right" colspan="1" rowspan="1" style="width: 84px; height: 20px" valign="top">
                                </td>
                                <td colspan="1" rowspan="1" style="height: 20px" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right" colspan="1" rowspan="1" style="width: 84px; height: 20px" valign="top">
                                </td>
                                <td align="right" colspan="1" rowspan="1" style="height: 20px" valign="top">
									<asp:button id="btnUpdate" runat="server" Text="Update"></asp:button>&nbsp; &nbsp;<asp:button id="btnCancel" runat="server" Text="Back"></asp:button>&nbsp; &nbsp;</td>
                            </tr>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 50px" align="center">
						<TABLE id="Table1" style="WIDTH: 564px; HEIGHT: 24px" width="564"
							border="0">
							<TR>
								<TD align="center">
									</TD>
								<TD align="center">
									</TD>
							</TR>
						</TABLE>
					</TD>
				</TR><input type=hidden id=txtCode runat=server />
			</TABLE>
		</form>
	</body>
</HTML>
