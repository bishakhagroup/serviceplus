<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_majorcategory_list" CodeFile="training-majorcategory-list.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<%@ Register TagPrefix="uc1" TagName="TrainingDataSubNavigation" Src="TrainingDataSubNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_majorcategory_list</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="0"
				width="520" align="left" border="0">
				<TR>
					<TD class="PageHead" align="center" height="30">Manage&nbsp;Training&nbsp;Major 
						Category</TD>
				</TR>
				<TR>
					<TD align="center" height="10"><asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left" height="10">
						<uc1:TrainingDataSubNavigation id="TrainingDataSubNavigation" runat="server"></uc1:TrainingDataSubNavigation></TD>
				</TR>
				<TR>
					<TD align="center" height="10"></TD>
				</TR>
				<TR>
					<TD align="center"><span class="BodyHead"><asp:label id="Label1" runat="server">Current Major Category List</asp:label></span></TD>
				</TR>
				<TR>
					<TD height="10">&nbsp;</TD>
				</TR>
				<TR>
					<TD height="5"><asp:datagrid id="majorcategoryDataGrid" runat="server" OnPageIndexChanged="majorcategoryDataGrid_Paging">
							<Columns>
								<%--<asp:TemplateColumn HeaderText="Code">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Code") %>' NavigateUrl='' ID="lnkMajorCategory">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>--%>
								<asp:TemplateColumn HeaderText="Description">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Description")) %>' ID="lnkDescription">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px" align="center"><asp:button id="btnAdd" runat="server" Text="Add"></asp:button></TD>
				</TR>
				<TR>
					<TD colSpan="5">&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
