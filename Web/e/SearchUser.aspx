<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SearchUser.aspx.vb" Inherits="SIAMAdmin.SearchUser" CodeFileBaseClass="SIAMAdmin.UtilityClass"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
<head>
    <title>Search User</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <script language=javascript>
		function ConfirmDelete()
		{
		    window.event.returnValue = confirm("This activity will delete selected user...");		   
		}
	</script>
    <style type="text/css">
        .style1
        {
            width: 170px;
        }
    </style>
</head>
<body>
<form id="form1" method="post" runat="server">
<table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px; height: 19px;">
            <asp:Label ID="lblHeader" runat="server">Search Internal User</asp:Label></td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr height="25">
				    <td ><a class="Nav3" href="appsUser.aspx">New</a></td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td ><a class="Nav3">Search</a></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
    <tr>
        <td colspan=2>
          <div id="div1" style="width:100%; border-top-width: thin; border-left-width: thin; border-left-color: gray; border-bottom-width: thin; border-bottom-color: gray; border-top-color: gray; border-right-width: thin; border-right-color: gray;">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td style="width: 170px; text-align: right; height: 19px;">&nbsp;
                    </td>
                    <td style="height: 19px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 170px; height: 26px; text-align: right">
                        <asp:Label ID="lblSIAMUserID" runat="server" CssClass="Body">SIAM User ID:</asp:Label></td>
                    <td style="height: 26px">
                        <SPS:SPSTextBox ID="txtSiamId" runat="server" CssClass="Body" MaxLength="16"></SPS:SPSTextBox></td>
                </tr>
          <tr> 
            <td style="width: 170px; text-align: right; height: 26px;">
                <asp:Label ID="lblFirstName" runat="server" CssClass="Body">First Name:</asp:Label></td>
            <td style="height: 26px"><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></span></td>
          </tr>
                <tr>
                    <td style="width: 170px; height: 24px; text-align: right">
                        <asp:Label ID="lblLastName" runat="server" CssClass="Body">Last Name:</asp:Label></td>
                    <td style="height: 24px">
                        <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></td>
                </tr>
                <tr>
                    <td style="text-align: right" class="style1">
                        <asp:Label ID="lblCompanyName" runat="server" CssClass="Body">Company Name:</asp:Label></td>
                    <td>
                        <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></td>
                </tr>
                <tr>
                    <td style="width: 180px; height: 23px; text-align: left">
                        <asp:Label ID="lblUserNmae" runat="server" CssClass="Body">User Name (10 character
Global ID):&nbsp;&nbsp;</asp:Label></td>
                    <td style="height: 23px">
                        <SPS:SPSTextBox ID="txtUserName" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></td>
                </tr>
                <tr>
                    <td style="width: 170px; height: 23px; text-align: right">
                        <asp:Label ID="lblEmail" runat="server" CssClass="Body">Email:</asp:Label></td>
                    <td style="height: 23px">
                        <SPS:SPSTextBox ID="txtEmail" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></td>
                </tr>
          <tr> 
            <td style="width: 170px; height: 20px;"><div align="right">
                &nbsp;<asp:Label ID="lblRole" runat="server" CssClass="Body">Role:</asp:Label></div></td>
            <td style="height: 20px">
                <asp:DropDownList ID="ddlRole" runat="server" CssClass="Body" Width="131px">
                </asp:DropDownList></td>
          </tr>
          <tr> 
            <td style="width: 170px; height: 20px;"><div align="right">
                &nbsp;<asp:Label ID="lblStatus" runat="server" CssClass="Body">Status:</asp:Label></div></td>
            <td style="height: 20px">
                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="Body" Height ="18px" Width="131px">
                    <asp:ListItem>Active</asp:ListItem>
                    <asp:ListItem>Inactive</asp:ListItem>
                    <asp:ListItem>Active and Inactive</asp:ListItem>
                </asp:DropDownList>
            </td>
          </tr>
          <tr> 
            <td style="width: 299px; height: 18px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="btnSearch" runat="server" CssClass="Body" Text="Search" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="Body" Text="Cancel" />
                &nbsp;
              </div></td>
          </tr>
          </table>&nbsp;
          </div>        
        </td>
    </tr>
    <tr>
    <td colspan=2>
    <asp:datagrid id="dgSearchUser" runat="server" AutoGenerateColumns="False" Width=100% AllowPaging="True" PageSize="3">
		<Columns>
           <asp:TemplateColumn SortExpression="Identity" HeaderText="SIAM UserID">
				<ItemTemplate>
						<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserId") %>' NavigateUrl='<%# "modifyUser.aspx?siamid="+DataBinder.Eval(Container.DataItem, "UserId").ToString()%>' ID="Hyperlink1">
						</asp:HyperLink>
						</ItemTemplate>
			</asp:TemplateColumn>
			<asp:BoundColumn DataField="UserName" HeaderText="User Name"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="First Name" DataField="FirstName"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Last Name" DataField="LastName"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Role" DataField="RoleName"></asp:BoundColumn>
		<%--	<asp:TemplateColumn>
				<ItemTemplate>
					<asp:HyperLink runat="server" Text="Modify" NavigateUrl='<%# "modifyUser.aspx?siamid="+DataBinder.Eval(Container.DataItem, "UserId").ToString()%>' ID="Hyperlink2"></asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
					<asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID")%>' CommandName="Delete" Text="Delete" ID="lnkBtnDelete"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateColumn>--%>
		</Columns>
        <ItemStyle BackColor="SlateGray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="ControlDarkDark" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        </asp:datagrid>    
    </td>
    </tr>
  </table>
</form>
</body>
</html>
