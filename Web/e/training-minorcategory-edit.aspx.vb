Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_minorcategory_edit
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    populateMajorCategoryDropDown()
                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-minorcategory-list") Is Nothing) Then
                        Dim categorySelected As CourseMinorCategory
                        Dim itemNumber As Integer = -1
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        categorySelected = CType(Session.Item("training-minorcategory-list"), Array)(itemNumber)
                        Me.txtCode.Value = categorySelected.Code
                        Me.txtDescription.Text = categorySelected.Description
                        Me.ddlMajorCategory.SelectedValue = categorySelected.MajorCategoryCode
                    End If




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try
            End If
        End Sub

        Private Sub populateMajorCategoryDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim categories() As CourseMajorCategory = courseDataManager.GetMajorCategories()
                For Each cl As CourseMajorCategory In categories
                    Me.ddlMajorCategory.Items.Add(New ListItem(cl.Description, cl.Code))
                Next




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering minor category info:" + ErrorLabel.Text
                Return
            End If
            Dim objPCILogger As New PCILogger() '6524
            Dim bUpdate As Boolean = False
            Dim categorySelected As CourseMinorCategory
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    bUpdate = True
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    categorySelected = CType(Session.Item("training-minorcategory-list"), Array)(itemNumber)
                Else
                    categorySelected = New CourseMinorCategory
                End If
                If Me.txtCode.Value.Trim() = String.Empty Then
                    txtCode.Value = "0"
                End If
                categorySelected.Code = Convert.ToInt32(txtCode.Value)

                categorySelected.Description = Me.txtDescription.Text
                categorySelected.MajorCategoryCode = Me.ddlMajorCategory.SelectedValue
                Dim cm As New CourseManager
                If bUpdate Then
                    objPCILogger.Message = "UpdateCourseMinorCategory Success."
                    objPCILogger.EventType = EventType.Update
                    cm.UpdateCourseMinorCategory(categorySelected)
                Else
                    objPCILogger.Message = "AddCourseMinorCategory Success."
                    objPCILogger.EventType = EventType.Add
                    cm.AddCourseMinorCategory(categorySelected)
                End If
                ErrorLabel.Text = "Saved Successfully"





            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Add/Update CourseMinorCategory Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-minorcategory-list.aspx")
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            ErrorLabel.Text = ""

            'Me.labelCode.CssClass = "Body"
            Me.txtDescription.CssClass = "Body"

            'Me.LabelCodeStar.Text = ""
            Me.LabelDescriptionStar.Text = ""

            'If Me.txtCode.Text = "" Then
            '    Me.labelCode.CssClass = "redAsterick"
            '    Me.LabelCodeStar.Text = "<span class=""redAsterick"">*</span>"
            '    bValid = False
            'End If
            If Me.txtDescription.Text = "" Then
                Me.labelDescription.CssClass = "redAsterick"
                Me.LabelDescriptionStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            ElseIf Me.txtDescription.Text.Length > 40 Then
                ErrorLabel.Text = "Description can not be longer than 40"
                bValid = False
            End If
            Return bValid
        End Function
    End Class

End Namespace
