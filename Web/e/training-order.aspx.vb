Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin


    Partial Class training_order
        Inherits UtilityClass
        Public Auditcustomer As Customer 'Added by Narayanan July 31 for AuditTrail
        'Dim objUtilties As Utilities = New Utilities
        'Changes done towards Bug# 1212
        'Public objSISParameter As New Sony.US.ServicesPLUS.Process.SISParameter
        'Public customer As Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private m_ccUC As CreditCardPayment = Nothing
        Private m_aUC As IAccountPayment
        Private m_invUC As InvoicePayment = Nothing
        Private m_order As Order
        Private m_cr As CourseReservation


        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            Try

                If Not IsPostBack Then
                    Session.Item("DDLStatusChanged") = False
                    Dim orderNumber As String = Request.QueryString("OrderNumber")
                    If Not orderNumber Is Nothing Then
                        Dim om As OrderManager = New OrderManager
                        '***************Changes done for Bug# 1212 Starts here***************
                        '**********Kannapiran S****************
                        'objSISParameter.UserLocation = customer.UserLocation
                        m_order = om.GetOrder(orderNumber, False, True, ServicePLUSWebApp.HttpContextManager.GlobalData)
                        If m_order Is Nothing Then Throw New ApplicationException("Order Not found")
                        '***************Changes done for Bug# 1212 Ends here***************
                        om.GetOrderInvoices(m_order)

                        Dim cm As CourseManager = New CourseManager
                        m_cr = cm.GetReservation(m_order.OrderNumber)

                        Session.Add("current_trainingorder", m_order) 'save for use later
                        Session.Add("current_trainingreservation", m_cr)

                        labelNumber.Text = m_order.OrderNumber


                        labelOrderDate.Text = m_order.OrderDate.ToString("MM/dd/yyyy")
                        labelBy.Text = m_order.Customer.SIAMIdentity
                        labelUpdateDate.Text = m_cr.UpdateDate.ToString("MM/dd/yyyy")
                        labelLastName.Text = m_order.Customer.LastName
                        labelFirstName.Text = m_order.Customer.FirstName
                        labelPhone.Text = m_order.Customer.PhoneNumber
                        labelExtension.Text = m_order.Customer.PhoneExtension
                        labelFax.Text = m_order.Customer.FaxNumber
                        labelEMail.Text = m_order.Customer.EmailAddress
                        labelClassNumber.Text = m_cr.CourseSchedule.PartNumber
                        labelCourseNumber.Text = m_cr.CourseSchedule.Course.Number
                        labelCourseTitle.Text = m_cr.CourseSchedule.Course.Title
                        lblPONoValue.Text = m_order.POReference.ToString()

                        Dim courseType As CourseType = New CourseType
                        courseType.PartNumber = m_cr.CourseSchedule.PartNumber()
                        cm.GetCourseType(courseType)

                        'for Non Class room training dont display the start and end date information
                        If Not courseType.Code <> 0 Then
                            labelStartDate.Text = m_cr.CourseSchedule.StartDateDisplay
                            labelEndDate.Text = m_cr.CourseSchedule.EndDateDisplay
                            labelLocation.Text = m_cr.CourseSchedule.Location.Name
                            labelPrice.Text = String.Format("{0:c}", m_cr.CourseSchedule.Course.ListPrice)
                            labelNoStudent.Text = m_cr.Participants.Length
                            labelTotal.Text = String.Format("{0:c}", m_order.AllocatedGrandTotal)
                        Else
                            lblStartDate.Visible = False
                            labelStartDate.Visible = False
                            lblEndDate.Visible = False
                            labelEndDate.Visible = False
                            lblLocation.Visible = False
                            labelLocation.Visible = False
                            lblNoStudent.Visible = False
                            labelNoStudent.Visible = False
                            lblTotal.Visible = False
                            labelTotal.Visible = False
                            lblStudentInfo.Visible = False
                            lblPrice.Text = "Price:"
                            labelPrice.Text = String.Format("{0:c}", m_cr.CourseSchedule.Course.ListPrice)
                            studentDataGrid.Visible = False

                        End If


                        'Payment
                        'Modified by Prasad for 45
                        If m_cr.Sentinvoiceorquote = 0 Then
                            If Not m_order.CreditCard Is Nothing Then 'credit payment info
                                m_ccUC = CType(Page.LoadControl("CreditCardPayment.ascx"), CreditCardPayment)

                                '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                                m_ccUC.GetReqContextValue = GetRequestContextValue()
                                '********************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                                m_ccUC.Order = m_order
                                m_ccUC.TrainingStartDate = m_cr.CourseSchedule.StartDateDisplay
                                phPayment.Controls.Add(m_ccUC)
                            Else 'account payment info
                                Dim user As User = Session(MyBase.CustomerSessionVariable)
                                'always the order is considered to be placed by the customer
                                'even if it is placed by a rep in ServicesPLUS Admin
                                'If m_order.Customer.SIAMIdentity = user.Identity Then 'the order was created by training admin
                                '    m_aUC = CType(Page.LoadControl("AccountPaymentReg.ascx"), IAccountPayment)
                                'Else
                                '    m_aUC = CType(Page.LoadControl("AccountPayment.ascx"), IAccountPayment)
                                'End If
                                m_aUC = CType(Page.LoadControl("AccountPayment.ascx"), IAccountPayment)
                                m_aUC.Order = m_order
                                phPayment.Controls.Add(m_aUC)
                            End If
                        Else 'send invoice without notification e-mail
                            m_invUC = CType(Page.LoadControl("InvoicePayment.ascx"), InvoicePayment)
                            m_invUC.Reservation = m_cr
                            m_invUC.Order = m_order
                            phPayment.Controls.Add(m_invUC)
                        End If

                        If (Not m_order.Invoices Is Nothing) And (m_order.Invoices.Length > 0) Then
                            txtInvoiceNumber.Text = m_order.Invoices(m_order.Invoices.Length - 1).InvoiceNumber 'work!!! assume the last (latest) invoice is the latest one
                            txtInvoiceNumber.ReadOnly = True  ' Temporary Fix for bug 314 discription
                        End If
                        txtOrderNotes.Text = m_cr.Notes

                        studentDataGrid.DataSource = m_cr.Participants
                        studentDataGrid.DataBind()
                    End If
                Else 'still need to load the user control for post back
                    m_cr = Session.Item("current_trainingreservation")
                    m_order = Session.Item("current_trainingorder")
                    If (Not m_cr Is Nothing) Then
                        'Modified by Prasad for 45
                        If m_cr.Sentinvoiceorquote = 0 Then
                            If Not m_order.CreditCard Is Nothing Then 'credit payment info
                                m_ccUC = CType(Page.LoadControl("CreditCardPayment.ascx"), CreditCardPayment)
                                m_ccUC.Order = m_order
                                m_ccUC.TrainingStartDate = m_cr.CourseSchedule.StartDateDisplay
                                phPayment.Controls.Add(m_ccUC)
                            Else 'account payment info
                                Dim user As User = Session(MyBase.CustomerSessionVariable)
                                If m_order.Customer.SIAMIdentity = user.Identity Then 'the order was created by training admin
                                    m_aUC = CType(Page.LoadControl("AccountPaymentReg.ascx"), IAccountPayment)
                                Else
                                    m_aUC = CType(Page.LoadControl("AccountPayment.ascx"), IAccountPayment)
                                End If
                                m_aUC.Order = m_order
                                phPayment.Controls.Add(m_aUC)
                            End If
                        Else 'send invoice without notification e-mail
                            m_invUC = CType(Page.LoadControl("InvoicePayment.ascx"), InvoicePayment)
                            m_invUC.Order = m_order
                            m_invUC.Reservation = m_cr
                            phPayment.Controls.Add(m_invUC)
                        End If
                    End If

                End If



                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
            If Not m_ccUC Is Nothing Then m_ccUC.EnableDDLType = False
        End Sub
        Public Sub DDLStatus_SelectedIndexChangeed(ByVal sender As Object, ByVal e As EventArgs)
            Session.Item("DDLStatusChanged") = True
        End Sub

        Private Function ValidateCreditCardInfo() As Boolean
            Try
                If m_ccUC IsNot Nothing Then
                    Dim tbox As TextBox
                    Dim ddl As DropDownList

                    Dim ControlsArray(6) As String

                    ControlsArray = New String() {"txtName", "txtStreet1", "TextBoxCity", "txtZip", "txtExpireationDate", "txtCCNumber"}

                    For Each obj In ControlsArray
                        tbox = m_ccUC.FindControl(obj)
                        If String.IsNullOrWhiteSpace(tbox.Text) Then
                            Return False
                        End If
                    Next

                    ControlsArray = New String() {"DDLState", "ddlType"}

                    For Each obj In ControlsArray
                        ddl = m_ccUC.FindControl(obj)
                        If (ddl.SelectedIndex = 0) Then
                            Return False
                        End If
                    Next
                    ' 2018-10-04 ASleight - We never return "True". Ever. Not my place to rewrite such logic, though.
                End If
                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Function

        Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
            Dim objPCILogger As New PCILogger()
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                objPCILogger.Message = "Training Order Update Success."
                objPCILogger.EventType = EventType.Update

                If m_ccUC IsNot Nothing Then
                    m_ccUC.GetReqContextValue = GetRequestContextValue()

                    If ValidateCreditCardInfo() Then
                        If Not m_ccUC.Save(m_order.Customer) Then
                            ErrorLabel.Visible = True
                            ErrorLabel.Text = "Make sure the credit card info is input correctly."
                            Return
                        End If
                    Else
                        ErrorLabel.Visible = True
                        ErrorLabel.Text = "Make sure the credit card info is input correctly."
                    End If
                End If

                If m_aUC IsNot Nothing Then
                    If Not m_aUC.Save() Then
                        ErrorLabel.Visible = True
                        ErrorLabel.Text = "Make sure the account info is input correctly."
                        Return
                    End If
                End If

                'If Not ValidateForm() Then
                '    Me.ErrorLabel.Visible = True
                '    Me.ErrorLabel.Text = "Make sure all input is correct."
                '    Return
                'End If


                m_cr.Notes = txtOrderNotes.Text

                Dim index = 0
                Dim studentDataGridItem As DataGridItem
                Dim ddlStatus As DropDownList
                For Each cp As CourseParticipant In m_cr.Participants
                    studentDataGridItem = studentDataGrid.Items(index)
                    ddlStatus = CType(studentDataGridItem.Cells(3).FindControl("DDLStatus"), DropDownList)
                    cp.Status = ddlStatus.SelectedValue
                Next

                If Not Session("TheCurrentUser") Is Nothing Then
                    Dim oUser As User = Session("TheCurrentUser")
                    m_cr.AdminSIAMID = oUser.Identity
                End If

                Dim cm As CourseManager = New CourseManager

                If (Not m_order.Invoices Is Nothing) And (m_order.Invoices.Length > 0) Then
                    If txtInvoiceNumber.Text <> m_order.Invoices(m_order.Invoices.Length - 1).InvoiceNumber Then
                        cm.ProcessTrainingOrder(m_order, m_cr, txtInvoiceNumber.Text) 'invoice number is changed, add it
                    Else
                        cm.ProcessTrainingOrder(m_order, m_cr, "")
                    End If
                    txtInvoiceNumber.ReadOnly = True ' Temporary Fix for bug 314 discription
                Else
                    If txtInvoiceNumber.Text.Trim() <> String.Empty Then
                        cm.ProcessTrainingOrder(m_order, m_cr, txtInvoiceNumber.Text) 'invoice number is changed, add it
                    Else
                        cm.ProcessTrainingOrder(m_order, m_cr, "")
                    End If
                    'cm.ProcessTrainingOrder(m_order, m_cr, Me.txtInvoiceNumber.Text) 'new invoice number save it
                End If


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Training Order Update Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
        Private Sub studentDataGrid_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles studentDataGrid.ItemCreated
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim ddlStatus As DropDownList = CType(e.Item.Cells(3).FindControl("DDLStatus"), DropDownList)
                    ddlStatus.Items.Add(New ListItem("Reserved", "R"))
                    ddlStatus.Items.Add(New ListItem("Wait Listed", "W"))
                    ddlStatus.Items.Add(New ListItem("Cancelled", "C"))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub

        Private Sub studentDataGrid_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles studentDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim ddlStatus As DropDownList = CType(e.Item.Cells(3).FindControl("DDLStatus"), DropDownList)
                    Dim cp As CourseParticipant = CType(e.Item.DataItem, CourseParticipant)
                    ddlStatus.SelectedValue = cp.Status
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(1).FindControl("HyperlinkFirstName"), HyperLink)
                    hyperLink.ForeColor = Color.Black
                    hyperLink.Text = cp.FirstName
                    'e.Item.Cells(1).Text = cp.FirstName
                    hyperLink.NavigateUrl = "training-student.aspx?LineNo=" + e.Item.ItemIndex().ToString
                    hyperLink = CType(e.Item.Cells(1).FindControl("HyperlinkLastName"), HyperLink)
                    'e.Item.Cells(2).Text = cp.LastName
                    hyperLink.ForeColor = Color.Black
                    hyperLink.Text = cp.LastName
                    hyperLink.NavigateUrl = "training-student.aspx?LineNo=" + e.Item.ItemIndex().ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub


        Private Function ValidateForm() As Boolean
            Dim bValid As Boolean = False
            If Session.Item("DDLStatusChanged") = True Then 'chaging status, don't need invoice/order info
                bValid = True
                Return bValid
            End If
            'To fix bug 314 commented this check
            'If Me.txtInvoiceNumber.Text <> "" Then
            '    bValid = True
            'End If
            If txtOrderNotes.Text <> "" Then
                bValid = True
            End If
            Return bValid
        End Function

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click
            Response.Redirect("training-open-orders.aspx")
        End Sub

        Public Function GetAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = ""

            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Short = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If

            Return returnValue
        End Function

        Public Function SelectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String = ""
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)

            Return creditNo
        End Function
    End Class
End Namespace
