Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports ServicesPlusException

Namespace SIAMAdmin


    Partial Class ViewOrderDetail
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
        'Public objSISParameter As New Sony.US.ServicesPLUS.Process.SISParameter
        'Public customer As Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)

            'MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub

#End Region

#Region "   Events  "
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            controlMethod(sender, e)
        End Sub

#End Region

#Region "   Methods     "
        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As EventArgs)
            Try
                Dim orderNumber As String = String.Empty
                Dim caller As String = String.Empty

                If Not Request.QueryString("on") Is Nothing Then orderNumber = Request.QueryString("on")

                If Not orderNumber Is Nothing Or orderNumber <> String.Empty Then
                    Dim thisOrder As Order = getOrder(orderNumber)

                    If Not thisOrder Is Nothing Then
                        If sender.Id IsNot Nothing Then caller = sender.Id.ToString()

                        Select Case caller
                            Case Page.ID.ToString()
                                PopulateOrderDetailTable(thisOrder)
                        End Select
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function getOrder(ByVal thisOrderNumber As String) As Order
            Dim thisOrderManager As New OrderManager
            Dim returnValue As Order = Nothing

            Try
                If thisOrderManager IsNot Nothing Then
                    Dim tempOrder As Order = thisOrderManager.GetOrder(thisOrderNumber, True)
                    If tempOrder IsNot Nothing Then returnValue = tempOrder
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function

        ' 2018-10-04 ASleight - No references to this method
        'Private Sub PopulateCustomerDetails(ByRef thisCustomer As Customer)
        '    Try
        '        If thisCustomer IsNot Nothing Then

        '        End If
        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

        Private Sub PopulateOrderDetailTable(ByRef thisOrder As Order)
            Try
                If thisOrder IsNot Nothing Then
                    '-- build header row
                    OrderDetailsTable.Controls.Add(BuildOrderDetailsHeader())
                    '-- build details row
                    OrderDetailsTable.Controls.Add(BuildOrderDetails(thisOrder))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ' 2018-10-04 ASleight - No references to this function
        'Private Function BuildInvoiceTrackingAndStatusTable(ByRef thisOrder As Order) As TableRow
        '    Dim returnValue As New TableRow

        '    Try
        '        If thisOrder IsNot Nothing Then
        '            Dim td As New TableCell

        '            td.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing-""0"">" + vbCrLf))
        '            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
        '            td.Controls.Add(New LiteralControl("</tr>"))
        '            td.Controls.Add(New LiteralControl("</table>"))
        '            '-- return the table row 
        '            returnValue.Controls.Add(td)
        '        End If
        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        '    Return returnValue
        'End Function

        Private Function BuildOrderDetailsHeader() As TableRow
            Dim returnValue As New TableRow
            Dim td As New TableCell

            Try
                td.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing-""0"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))

                '-- part number
                td.Controls.Add(New LiteralControl("<td class=""body"">Part Number</td>"))
                '-- description 
                td.Controls.Add(New LiteralControl("<td class=""body"">Description</td>"))
                '-- qty
                td.Controls.Add(New LiteralControl("<td class=""body"">QTY</td>"))
                '-- item price
                td.Controls.Add(New LiteralControl("<td class=""body"">Item Price</td>"))
                '-- total price
                td.Controls.Add(New LiteralControl("<td class=""body"">Total Price</td>"))
                '-- cancel
                td.Controls.Add(New LiteralControl("<td class=""body"">Cancel</td>"))

                td.Controls.Add(New LiteralControl("</tr>"))
                td.Controls.Add(New LiteralControl("</table>"))

                '-- return the table row 
                returnValue.Controls.Add(td)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function

        Private Function BuildOrderDetails(ByRef thisOrder As Order) As TableRow
            Dim returnValue As New TableRow
            Dim td As New TableCell

            Try
                If thisOrder IsNot Nothing Then
                    td.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing-""0"">" + vbCrLf))

                    For Each thisLineItem As OrderLineItem In thisOrder.LineItems
                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))

                        '-- part number
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">" + thisLineItem.Product?.PartNumber + "</span></td>"))
                        '-- description 
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">" + thisLineItem.Product?.Description + "</span></td>"))
                        '-- qty
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">" + thisLineItem.Quantity.ToString() + "</span></td>"))
                        '-- item price
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">$" + thisLineItem.Product?.ListPrice.ToString("C") + "</span></td>"))
                        '-- total price
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">$" + (thisLineItem.Product?.ListPrice * thisLineItem.Quantity).ToString("C") + "</span></td>"))
                        '-- cancel
                        td.Controls.Add(New LiteralControl("<td><span class=""body"">N/A</span></td>"))

                        td.Controls.Add(New LiteralControl("</tr>"))
                    Next
                    td.Controls.Add(New LiteralControl("</table>"))

                    '-- return the table row 
                    returnValue.Controls.Add(td)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function

        Private Sub ApplyDataGridStyle()
            DataGrid1.ApplyStyle(GetStyle(520))
            DataGrid1.PageSize = 10
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = False
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Height = Unit.Pixel(30)
            DataGrid1.PagerStyle.Height = Unit.Pixel(10)
            DataGrid1.HeaderStyle.Font.Size = FontUnit.Point(10)
            DataGrid1.HeaderStyle.ForeColor = Color.GhostWhite
            DataGrid1.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(10)
            DataGrid1.ItemStyle.ForeColor = Color.GhostWhite
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            DataGrid1.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub
#End Region

    End Class

End Namespace
