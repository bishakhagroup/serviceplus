<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.AccountPaymentReg" CodeFile="AccountPaymentReg.ascx.vb" %>
<LINK href="includes/style2.css" type="text/css" rel="stylesheet">
<TABLE id="Table1" style="WIDTH: 312px; HEIGHT: 207px" cellPadding="0"
	width="312" border="0">
	<TR>
		<TD style="HEIGHT: 16px" colSpan="2"><asp:label id="Label19" runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Account Payment Information:</asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 21px" align="right"><asp:label id="Label1" runat="server" CssClass="Body">SAP Bill to Accnt #:</asp:label></TD>
		<TD style="HEIGHT: 21px">
			<SPS:SPSTextBox id="txtSAPAccntNumber" CssClass="Body" runat="server" Width="152px"></SPS:SPSTextBox>
			<asp:Button id="btnEnter" CssClass="Body" runat="server" Width="56px" Text="Enter"></asp:Button></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 23px" align="right" colSpan="1" rowSpan="1"><asp:label id="Label2" runat="server" CssClass="Body">Bill to Name:</asp:label></TD>
		<TD style="HEIGHT: 23px"><asp:label id="labelName" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 23px" align="right"><asp:label id="Label3" runat="server" CssClass="Body">Bill to Street1:</asp:label></TD>
		<TD style="HEIGHT: 23px"><asp:label id="labelStreet1" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 24px" align="right"><asp:label id="Label4" runat="server" CssClass="Body">Bill to Street 2:</asp:label></TD>
		<TD style="HEIGHT: 24px"><asp:label id="labelStreet2" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 24px" align="right">
			<asp:Label id="Label5" runat="server" CssClass="Body">Bill to Street 3:</asp:Label></TD>
		<TD style="HEIGHT: 24px">
			<asp:Label id="labelStreet3" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 24px" align="right">
			<asp:Label id="Label7" runat="server" CssClass="Body">Bill to State:</asp:Label></TD>
		<TD style="HEIGHT: 24px">
			<asp:Label id="labelState" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 97px; HEIGHT: 25px" align="right">
			<asp:Label id="Label8" runat="server" CssClass="Body">Bill to Postal Code:</asp:Label></TD>
		<TD style="HEIGHT: 25px">
			<asp:Label id="labelZip" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD colspan="2">
			<asp:Label id="lblErrorMsg" runat="server" CssClass="Body" Visible="False" Font-Size="Smaller"
				ForeColor="Red"></asp:Label></TD>
	</TR>
</TABLE>
