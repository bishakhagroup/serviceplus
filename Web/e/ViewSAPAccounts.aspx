<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="uc" TagName="CustomerMenu" Src="~/e/CustomerDataSubNavigation.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="SIAMAdmin.ViewSAPAccounts"
    CodeFile="ViewSAPAccounts.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>View SAP Accounts</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%" align="center" border="0">
        <%--<asp:UpdateProgress ID="updProgress"
        AssociatedUpdatePanelID="UpdatePanel1"
        runat="server">
            <ProgressTemplate>            
            <img alt="progress" src="../images/progbar.gif"/>
               Processing...            
            </ProgressTemplate>
        </asp:UpdateProgress>

				  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
				  <ContentTemplate>--%>
        <tr>
            <td align="left">
                <uc:CustomerMenu ID="ucCustomerMenu" runat="server" />
            </td>
        </tr>
        <tr bgcolor="#5d7180">
            <td class="PageHead" align="center" style="color: white;" colspan="1" height="30" rowspan="1">
                SAP Accounts
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="ErrorLabel" runat="server" CssClass="body" EnableViewState="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:DataGrid ID="DataGrid1" Width="100%" CssClass="Body" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Validated">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Validated") %>' ID="Label4">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Account">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNumber") %>' ID="Label1">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Company">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Name") %>' ID="Label2">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Address">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Line1") + " " + DataBinder.Eval(Container, "DataItem.Address.Line2") %>'
                                    ID="Label3">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="State">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.State") %>' ID="Label6">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Zip">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.PostalCode").ToString() %>'
                                    ID="Label7">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Delete">
                            <ItemTemplate>
                                <asp:CheckBox ID="deleteCheckbox" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td>
                <table id="Table1" width="100%" border="0">
                    <tr>
                        <td style="height: 30px" width="30%" align="right">
                        </td>
                        <td style="height: 30px" width="70%" align="right">
                            <asp:Button ID="btnModifyAccount" runat="server" Width="139px" Text="Modify Account(s)"></asp:Button>
                            <asp:Button ID="DeleteSAPAccounts" runat="server" Width="139px" Text="Delete Account(s)"></asp:Button>
                            <asp:Button ID="btnReloadAccount" runat="server" Text="Reload Account Information"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%--<tr>
					<td align="center" style="HEIGHT: 13px">
						<b>
							<asp:Label id="AddSAPLabel" runat="server" EnableViewState="False" CssClass="body">Add new SAP account(s) number</asp:Label></b>
					</td>
				</tr>--%>
        <%--<tr>
					<td align="center">
						<table border="0" cellspacing="0">
							<tr>
								<td><SPS:SPSTextBox id="SAPAccount1TextBox" runat="server" Width="80px"></SPS:SPSTextBox></td>
								<td><SPS:SPSTextBox id="SAPAccount2TextBox" runat="server" Width="80px"></SPS:SPSTextBox></td>
								<td><SPS:SPSTextBox id="SAPAccount3TextBox" runat="server" Width="80px"></SPS:SPSTextBox></td>
								<td><SPS:SPSTextBox id="SAPAccount4TextBox" runat="server" Width="80px"></SPS:SPSTextBox></td>
							</tr>
							<tr>
								<td colspan="4" align="right">
									<asp:Button id="AddNewSAPAccounts" runat="server" Text="Add Account(s)"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="10">
					<td></td>
				</tr>--%>
        <%--	</ContentTemplate>
				<Triggers  >
				<asp:AsyncPostBackTrigger ControlID="btnReloadAccount" EventName="Click" ></asp:AsyncPostBackTrigger> 
				</Triggers>
				  </asp:UpdatePanel>--%>
    </table>
    </form>
</body>
</html>
