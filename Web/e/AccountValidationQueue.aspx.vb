Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam

Namespace SIAMAdmin

    Partial Class AccountValidationQueue
        Inherits UtilityClass

        Protected WithEvents TextBox1 As SPSTextBox
        Protected WithEvents AddApplicationButton As System.Web.UI.WebControls.ImageButton
        Private sa As New SecurityAdministrator
        Private users As ShallowUser()
        Private strsiamIdentity As String '7086
        Private LDapID As String '7086

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub
#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not Page.IsPostBack Then controlMethod(sender, e)
        End Sub

        Private Sub DataGrid1_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemCreated
            Try
                e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then e.Item.Font.Size = FontUnit.Point(8)
            Catch ex As Exception
                Label1.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub pager(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles DataGrid1.PageIndexChanged
            Try
                DataGrid1.CurrentPageIndex = e.NewPageIndex
                controlMethod(sender, e)
            Catch ex As Exception
                Label1.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


#End Region

#Region "   Methods     "
        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Dim custMan As New CustomerManager

            Try
                '-- make sure we don't need to remove a customer from the list before we generate it. 
                If Request.QueryString("LdapId") IsNot Nothing Then
                    LDapID = Request.QueryString("LdapId")
                    strsiamIdentity = custMan.GetSiamIdByLdapId(LDapID)
                    removeUser(strsiamIdentity)
                End If

                Dim user As User = Session(CustomerSessionVariable)
                Dim customers As Customer() = custMan.GetAllNonValidatedAccountHolders()


                If customers Is Nothing Then
                    Label1.Text = "There are no pending account holders to validate."
                    Label1.Visible = True
                Else
                    DataGrid1.Visible = True
                    DataGrid1.DataSource = customers
                    DataGrid1.AllowPaging = False
                    If customers.Length > DataGrid1.PageSize Then DataGrid1.AllowPaging = True
                    DataGrid1.DataBind()
                End If

                writeScriptsToPage()
            Catch ex As Exception
                Label1.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub removeUser(ByVal SIAMIdentity As String)
            Dim objPCILogger As New PCILogger() '6524
            Try
                Dim myCM As New CustomerManager
                Dim myCustomer As Customer = myCM.GetCustomer(SIAMIdentity)

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.CustomerID = myCustomer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = mycustomer.SequenceNumber
                'objPCILogger.UserName = myCustomer.UserName
                objPCILogger.EmailAddress = myCustomer.EmailAddress '6524
                objPCILogger.SIAM_ID = myCustomer.SIAMIdentity
                objPCILogger.LDAP_ID = myCustomer.LdapID '6524
                objPCILogger.EventOriginMethod = "removeUser"
                objPCILogger.Message = "removeUser Success."
                objPCILogger.EventType = EventType.Delete
                '6524 end

                For Each myAccount As Account In myCustomer.SAPBillToAccounts
                    myCustomer.RemoveAccount(myAccount)
                Next

                myCM.UpdateCustomerSubscription(myCustomer)
                myCustomer.ClearAllSAPAccounts()
            Catch ex As Exception
                Label1.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "removeUser Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub



        Private Sub ApplyDataGridStyle()
            DataGrid1.ApplyStyle(GetStyle(450))
            DataGrid1.PageSize = 15
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = False
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Height = Unit.Pixel(30)
            DataGrid1.PagerStyle.Height = Unit.Pixel(8)
        End Sub

        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

#End Region

    End Class

End Namespace
