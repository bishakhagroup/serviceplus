<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ModifyFunctionality.aspx.vb" Inherits="SIAMAdmin.ModifyFunctionality" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
<head>
    <title>Modify Functionality</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<body>
    <form id="form1" method="post" runat="server">
<table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px;height: 19px;">
            <asp:Label ID="lblHeader" runat="server">Modify Functionality</asp:Label></td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr height="25">
				    <td class="Nav3"><A href="FunctionalityMaster.aspx">New</A></td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td class=Nav3><A href="SearchFunctionality.aspx">Search</A></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
  
    <tr>
        <td colspan="2">
          <div id="div1" style="width:100%; border-top-width: thin; border-left-width: thin; border-left-color: gray; border-bottom-width: thin; border-bottom-color: gray; border-top-color: gray; border-right-width: thin; border-right-color: gray;">
            <table width="100%" border="0" cellspacing="0">
                <tr>
                    <td style="width: 170px; text-align: right">&nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFunctionalityId" runat="server" CssClass="BodyHead">Functionality ID:</asp:Label></td>
                    <td style="height: 22px">
                        <SPS:SPSTextBox ID="txtFunctionalityId" runat="server" CssClass="Body" MaxLength="25"
                            ReadOnly="True" Width="50%"></SPS:SPSTextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFunctionalityName" runat="server" CssClass="BodyHead">Functionality Name:</asp:Label></td>
                    <td>
                        <SPS:SPSTextBox ID="txtFunctionalityName" runat="server" CssClass="Body" 
                            MaxLength="25" Width="50%"></SPS:SPSTextBox></td>
                </tr>
          <tr> 
            <td>
                <asp:Label ID="lblURLMapped" runat="server" CssClass="BodyHead">URL Mapped:</asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtURLMapped" runat="server" CssClass="Body" MaxLength="50" 
                    Width="50%"></SPS:SPSTextBox></span></td>
          </tr>
          <tr> 
            <td>
                        <asp:Label ID="lblGroupName" runat="server" CssClass="BodyHead">Group Name:</asp:Label></td>
            <td>
                        <SPS:SPSDropDownList ID="drpGroupName" runat="server" Width="50%">
                            <asp:ListItem Value="1" Selected=True>Customer</asp:ListItem>
                            <asp:ListItem Value="2">Order</asp:ListItem>
                            <asp:ListItem Value="3">Exception</asp:ListItem>
                            <asp:ListItem Value="4">Training</asp:ListItem>
                            <asp:ListItem Value="5">Admin</asp:ListItem>
                        </SPS:SPSDropDownList>
                    </td>
          </tr>
          <tr> 
            <td style="width: 170px">
                <asp:Label ID="lblIsActive" runat="server" CssClass="BodyHead">Is Active:</asp:Label></td>
            <td><asp:CheckBox ID="chkIsActive" runat="server" CssClass="Body" /></td>
          </tr>
          <tr> 
            <td align="left" class="style1"><asp:Label ID="lblIsActive0" runat="server" 
                    CssClass="BodyHead">Show function when site is:</asp:Label></td>
            <td class="style1">
                        <SPS:SPSDropDownList ID="drpLive" runat="server" Width="60%">
                            <asp:ListItem Value="1" Selected=True>Live or under maintenance</asp:ListItem>
                            <asp:ListItem Value="2">Under maintenance only</asp:ListItem>
                            <asp:ListItem Value="3">Live only</asp:ListItem>
                        </SPS:SPSDropDownList>
                    </td>
          </tr>
          <tr> 
            <td style="width: 299px; height: 19px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="btnUpdate" runat="server" CssClass="Body" Text="Update" />
                &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="Body" Text="Cancel" />&nbsp;
              </div></td>
          </tr>
          </table>&nbsp;
          </div>        
        </td>
    </tr>
  </table>
 </form>
</body>
</html>
