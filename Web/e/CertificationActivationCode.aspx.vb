Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Globalization
Imports Sony.US.ServicesPLUS.Controls
Imports System.Data
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin
    Partial Class CertificationActivationCode
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCourseNumber As SPSTextBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    PopulateCertificationCourseDropDown()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            Dim objPCILogger As New PCILogger() '6524
            If Not ValidateForm() Then
                ErrorLabel.Text = "Select Training Course Number and enter Activation Code."
                Return
            Else
                Try
                    '6524 start
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.EventOriginMethod = "btnUpdate_Click"
                    objPCILogger.Message = "btnUpdate_Click Success."
                    objPCILogger.EventType = EventType.Update
                    '6524 end

                    Dim strCertificate As String = String.Empty
                    Dim strReturnCertificate As String = String.Empty
                    strCertificate = txtActivationCode.Text.Replace(vbCrLf, "~").Replace(" ", "").ToUpper()
                    While strCertificate.IndexOf("~~") >= 0
                        strCertificate = strCertificate.Replace("~~", "~")
                    End While
                    If strCertificate <> String.Empty Then
                        Dim objCourseManager As CourseManager = New CourseManager()
                        strReturnCertificate = objCourseManager.SaveCertificateForCourse(ddlTrainingCourse.SelectedValue, strCertificate)
                    End If
                    PopulateGrid()
                    If Not strReturnCertificate Is Nothing Then
                        ErrorLabel.Text = "Certificates Updated Succesfully with duplicate certificate(s) (" + strReturnCertificate.Substring(1, strReturnCertificate.Length - 1) + ") already exist in database."
                    Else
                        ErrorLabel.Text = "Certificates Updated Succesfully."
                    End If




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "btnUpdate_Click Failed. " & ex.Message.ToString() '6524
                Finally
                    objPCILogger.PushLogToMSMQ() '6524
                End Try
            End If
        End Sub


        Private Function ValidateForm() As Boolean
            Dim bValid = True

            lblActivationCode.CssClass = "BodyCopy"
            labelCourseNumber.CssClass = "BodyCopy"

            If Me.ddlTrainingCourse.SelectedItem.Text = "Select Course" Then
                Me.labelCourseNumber.CssClass = "redAsterick"
                Me.LabelCourseNumberStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If txtActivationCode.Text = "" Then
                lblActivationCode.CssClass = "redAsterick"
                bValid = False
            End If

            Return bValid
        End Function

        Private Sub PopulateCertificationCourseDropDown()
            Dim objCourseManager As CourseManager
            Try
                objCourseManager = New CourseManager()
                Dim courseData As DataSet = objCourseManager.GetCertificateCourses()
                ddlTrainingCourse.DataValueField = "Coursenumber"
                ddlTrainingCourse.DataTextField = "Coursenumber"
                ddlTrainingCourse.DataSource = courseData.Tables(0)

                ddlTrainingCourse.DataBind()
                ddlTrainingCourse.Items.Insert(0, "Select Course")




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objCourseManager = Nothing
            End Try
        End Sub

        Protected Sub ddlTrainingCourse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrainingCourse.SelectedIndexChanged
            PopulateGrid()
        End Sub
        Private Sub PopulateGrid()
            Dim objCourseManager As CourseManager
            Try
                objCourseManager = New CourseManager()
                Dim dtCertificateData As DataSet = objCourseManager.GetCertificateCodeForCourse(ddlTrainingCourse.SelectedValue)
                dgCertificate.DataSource = dtCertificateData.Tables(0)
                dgCertificate.DataBind()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objCourseManager = Nothing
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        End Sub
    End Class

End Namespace
