<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.MaintainShippingMethod"
    CodeFile="MaintainShippingMethod.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Maintain Shipping Method</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <%--<link href="includes/style.css" type="text/css" rel="stylesheet">--%>
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript">
        function ConfirmDelete() {
            window.event.returnValue = confirm("This activity will delete selected mapping...");
        }
        window.parent.status = "Maintain Shipping Method is open.";
    </script>

    <style type="text/css">
        Body
        {
            filter: chroma(color=#FFFFFF);
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center" border="0">
        <tr>
            <td align="center" height="20">
                <asp:Label ID="lblHeader" CssClass="headerTitle" runat="server" Text="Find ServicesPLUS User">Maintain Shipping Method</asp:Label>
            </td>
        </tr>
        <tr>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    <tr height="20">
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="FieldUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="div1" style="border-right: #c9d5e6 thin solid; width: 100%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                        <table width="100%" cellspacing="0">
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblTrackingURL" CssClass="bodyCopyBold" runat="server">TRACKINGURL</asp:Label>
                                                </td>
                                                <td width="40%" colspan="3">
                                                    <SPS:SPSTextBox ID="txtTRACKINGURL" CssClass="bodyCopy" Width="95%" runat="server"
                                                        Text="" MaxLength="300"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblCountry" CssClass="bodyCopyBold" runat="server">COUNTRY</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <asp:DropDownList ID="ddlcountry" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblLanguage" runat="server" CssClass="bodyCopyBold">LANGUAGE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <asp:DropDownList ID="ddllanguage" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">
                                                    <asp:Label ID="lblDesc" runat="server" CssClass="bodyCopyBold"> DESCRIPTION</asp:Label>
                                                </td>
                                                <td width="40%" colspan="3">
                                                    <SPS:SPSTextBox ID="txtDESCRIPTION" runat="server" Width="95%" CssClass="bodyCopy"
                                                        Text="" MaxLength="100"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%"">
                                                    <asp:Label ID="lblBACKORDEROVERRIDE" CssClass="bodyCopyBold" runat="server">BACKORDEROVERRIDE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSDropDownList ID="drpBACKORDEROVERRIDE" CssClass="bodyCopy" Width="50%" runat="server">
                                                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                    </SPS:SPSDropDownList>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblSISBOPICKGROUPCODE" CssClass="bodyCopyBold" runat="server">SISBOPICKGROUPCODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtSISBOPICKGROUPCODE" CssClass="bodyCopy" runat="server" Text=""
                                                        Width="40%" MaxLength="2"></SPS:SPSTextBox>
                                                    <%--<span class="redAsterick">*</span>--%>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblSISCARRIERCODE" CssClass="bodyCopyBold" runat="server">SISCARRIERCODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtSISCARRIERCODE" CssClass="bodyCopy" runat="server" Text=""
                                                        MaxLength="2" Width="40%"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblSISPICKGROUPCODE" runat="server" CssClass="bodyCopyBold">SISPICKGROUPCODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtSISPICKGROUPCODE" runat="server" CssClass="bodyCopy" Text=""
                                                        Width="40%" MaxLength="2"></SPS:SPSTextBox>
                                                    <%--<span class="redAsterick">*</span>--%>
                                                </td>
                                            </tr>
                                            <tr height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblSISBOCARRIERCODE" runat="server" CssClass="bodyCopyBold">SISBO CARRIER CODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtSISBOCARRIERCODE" runat="server" CssClass="bodyCopy" Text=""
                                                        MaxLength="20"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblPRSVCARRIERCODE" CssClass="bodyCopyBold" runat="server">PRSV CARRIER CODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtPRSVCARRIERCODE" CssClass="bodyCopy" runat="server" Text=""
                                                        Width="90%" MaxLength="20"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblDISPLAYORDER" CssClass="bodyCopyBold" runat="server">DISPLAY ORDER</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSTextBox ID="txtDISPLAYORDER" CssClass="bodyCopy" runat="server" Text=""></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblStatusCode" runat="server" CssClass="bodyCopyBold">STATUS CODE</asp:Label>
                                                </td>
                                                <td width="15%">
                                                    <SPS:SPSDropDownList ID="drpStatusCode" runat="server" CssClass="bodyCopy" Width="100%">
                                                       <asp:ListItem Text="All" Selected="True" ></asp:ListItem>
                                                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="InActive" Value="0"></asp:ListItem>         
                                                    </SPS:SPSDropDownList>
                                                </td>
                                            </tr>
                                            <tr height="20px">
                                                <td colspan="8">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="8" align="right" width="25%">
                                                    <asp:Label ID="lblShippingMethodID" runat="server" Visible="false"></asp:Label>
                                                       <asp:Button ID="btnSearch" runat="server" CssClass="bodyCopyBold" Text="Search"
                                                        Width="70px"></asp:Button> 
                                                        &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnAddNew" runat="server" CssClass="bodyCopyBold" Text="Add New"
                                                        Width="70px"></asp:Button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnSave" runat="server" CssClass="bodyCopyBold" Text="Save" Width="70px">
                                                    </asp:Button>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="bodyCopyBold" Text="Cancel" Width="70px">
                                                    </asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dgShippingMethod" EventName="ItemCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:UpdatePanel ID="GridUpdatePanel" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dgShippingMethod" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="8" Width="100%">
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditItemStyle BackColor="#999999" />
                                        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="#284775" CssClass="BodyCopy" ForeColor="White" HorizontalAlign="Center"
                                            Mode="NumericPages" />
                                        <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="BodyCopy" />
                                        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="BodyCopy" />
                                        <HeaderStyle BackColor="#284775" CssClass="TableHeader" ForeColor="White" />
                                         
                                        
                                        <Columns>
                                            <asp:BoundColumn DataField="SALESORGANIZATION" HeaderText="COUNTRY"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LANGUAGEID" HeaderText="LANGUAGE"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                                                <HeaderStyle Width="12%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TRACKINGURL" HeaderText="TRACKING URL">
                                                <HeaderStyle Width="35%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SISBOCARRIERCODE" HeaderText="SIS BO CARRIER CODE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SISBOPICKGROUPCODE" HeaderText="SIS BO PICK GROUP CODE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="BACKORDEROVERRIDE" HeaderText="BACK ORDER OVERRIDE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SISCARRIERCODE" HeaderText="SIS CARRIER CODE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SISPICKGROUPCODE" HeaderText="SIS PICK GROUP CODE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PRSVCARRIERCODE" HeaderText="PRSV CARRIER CODE">
                                                <HeaderStyle Width="7%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DISPLAYORDER" HeaderText="DISPLAY ORDER" Visible="True">
                                                <HeaderStyle Width="6%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STATUSCODE" HeaderText="Status" Visible="True">
                                                <HeaderStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="SHIPPINGMETHODID" Visible="False"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SHIPPINGMETHODID")%>'
                                                        CommandName="Edit" CssClass="bodyCopySM" Text="Edit">
                                                    </asp:LinkButton>
                                                    <itemstyle cssclass="bodycopy" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SHIPPINGMETHODID")%>'
                                                        CommandName="Delete" CssClass="bodyCopySM" Text="Delete">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                           
                                            <asp:BoundColumn DataField="SALESORG" HeaderText="SALESORG" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LANGUAGE_ID" HeaderText="LANGUAGE_ID" Visible="False">
                                            </asp:BoundColumn>
                                            
                                        </Columns>
                                    </asp:DataGrid>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnLastOpr" runat="server" />
    </form>
</body>
</html>
