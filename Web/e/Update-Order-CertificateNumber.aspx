<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" CodeFile="Update-Order-CertificateNumber.aspx.vb"
    Inherits="SIAMAdmin.UpdateOrderCertificateNumber" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Certification Code</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

    <style type="text/css">
        .style1
        {
            height: 19px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="Table3" align="center" border="0" width="100%">
        <tr>
            <td class="PageHead" align="center" colspan="1" height="25" rowspan="1">
                Update Order Certification Activation Code<br />
            </td>
        </tr>
        <tr>
            <td class="PageHead" style="height: 1px" align="left">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red"
                            CssClass="Body"></asp:Label><br />
                        <br />
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <table id="Table2" width="100%" border="0">
                    <tr>
                        <td align="right" colspan="1" rowspan="1">
                            <asp:Label ID="lblOrderNumber" runat="server" CssClass="BodyCopy">Order Number</asp:Label>
                        </td>
                        <td style="height: 7px">
                            <SPS:SPSTextBox ID="txtOrderNumber" runat="server" CssClass="Body" Width="30%" Height="25px"></SPS:SPSTextBox>
                            <asp:Button ID="btnSearch" runat="server" Text="Search Order Line Item" Height="25px">
                            </asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" width="20%">
                            &nbsp;
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" width="20%">
                            &nbsp;
                        </td>
                        <td align="right">
                            </br>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" align="LEFT" width="100%">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <div style="overflow: auto; width: 100%; height: 200px">
                                        <asp:DataGrid ID="dgCertificate" runat="server" PageSize="5000" AutoGenerateColumns="False"
                                            CellPadding="4" ForeColor="#333333" GridLines="None" Width="97%" >
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                            <EditItemStyle BackColor="#999999" />
                                            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                            <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" CssClass="Body" ForeColor="White" />
                                            <Columns>
                                                <asp:BoundColumn HeaderText="Certificate Course" DataField="PARTNUMBER">
                                                    <ItemStyle CssClass="bodyCopy" VerticalAlign="Top" />
                                                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Quantity" HeaderText="Ordered Quantity">
                                                    <ItemStyle CssClass="bodyCopy"  HorizontalAlign="Center" VerticalAlign="Top"  />
                                                    <HeaderStyle Width="10%" />
                                                </asp:BoundColumn>
                                                
                                                <asp:TemplateColumn HeaderText="Activation Code">
                                                    <ItemStyle VerticalAlign="Top" CssClass="bodyCopy" Width=30% />
                                                    <ItemTemplate>
                                                        <div style="overflow: auto; width: 200px; height: 40px">
                                                        <asp:Label runat="server" ID="lblActivationCode"  
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.TRAININGCERTIFICATIONCODE") %>'></asp:Label>
                                                            </div>
                                                    </ItemTemplate>
                                                     <HeaderStyle Width="30%"  />
                                                </asp:TemplateColumn>
                                                
                                                <asp:TemplateColumn HeaderText="Add New ',' delimited Activation Code(s)">
                                                    <ItemStyle CssClass="bodyCopy" VerticalAlign="Top" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID=imgPicCode AlternateText="Show Available Code" runat="server" Width=10px ImageUrl="~/e/images/plus.gif"/><SPS:SPSTextBox runat="server" CssClass="Body" ID="txtActivationCode" BorderWidth=1 BorderColor=Black Text="" Width=90%></SPS:SPSTextBox>
                                                        <span runat="server" id="listCodeSpan" visible="false"><br /><img height="25" runat="server" id="spacer" src="images/spacer.gif" width="13"><asp:ListBox ID="lstAvailableCode" style="border-width:1;border-color:Black; background-color:AliceBlue"  Width=50% runat="server" Visible="true" AutoPostBack="false"></asp:ListBox></span>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50%"  HorizontalAlign="Center"/>
                                                </asp:TemplateColumn>
                                                <asp:ButtonColumn Text="Update" HeaderText="Update">
                                                    <ItemStyle CssClass="bodyCopy" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    <HeaderStyle Width="10%" />
                                                </asp:ButtonColumn>
                                                <asp:BoundColumn DataField="OrderNumber" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="SEQUENCENUMBER" Visible="false"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" width="20%" class="style1">
                        </td>
                        <td align="left" class="style1">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right" width="20%" colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
