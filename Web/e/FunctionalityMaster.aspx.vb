Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports System.Data
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class FunctionalityMaster
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

            If btnSave.Text = "New" Then
                txtFunctionalityName.Text = ""
                txtURLMapped.Text = ""
                chkIsActive.Checked = False
                btnSave.Text = " Save "
                Exit Sub
            End If
            Dim objPCILogger As New PCILogger()
            Try
                'prasad added on 16-12-2009
                'Event logging code added for Add Function. BugId #2100
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "btnSave_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Add
                If Not Session.Item("siamuser") Is Nothing Then
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                    objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                    objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                    objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                    objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                If ValidateUsrInput() = True Then
                    Dim oUsrRoleMgr As New UserRoleSecurityManager
                    Dim sFunctionalityName As String = txtFunctionalityName.Text.Trim()
                    Dim sURLMapped As String = txtURLMapped.Text.Trim()
                    Dim iStatus As Integer

                    If chkIsActive.Checked = True Then
                        iStatus = 0
                    Else
                        iStatus = 1
                    End If
                    Dim FunctionalityId As Integer
                    FunctionalityId = oUsrRoleMgr.AddFunctionality(sFunctionalityName, sURLMapped, iStatus, Convert.ToInt16(drpGroupName.SelectedValue), Convert.ToInt16(drpLive.SelectedItem.Value))
                    If FunctionalityId <> vbNull Then
                        ErrorLabel.Text = "Functionality added successfully"
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.Message = "FunctionalityName " & sFunctionalityName & " with FunctionalityId " & FunctionalityId & " is added successfully"
                        btnSave.Text = "New"
                    Else
                        ErrorLabel.Text = "Error on adding new Functionality"
                        objPCILogger.IndicationSuccessFailure = "Failure"
                        objPCILogger.Message = "Error on adding new FunctionalityName " & sFunctionalityName
                    End If
                Else
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = "Invalid inputs"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function ValidateUsrInput() As Boolean
            If txtFunctionalityName.Text.Trim.Length <= 0 Then
                ErrorLabel.Text = "Please enter Functionality Name"
                'prasad added on 16-12-2009
                'Added below line to Exit from this fucntion when Functionality name is empty.
                ' 2018-10-04 ASleight - ...why? Now the function can just return nothing, which is an invalid Boolean value.
                '                       If you want to create an error, throw one. Don't just force-exit when a return value
                '                       is expected...
                'Exit Function
                Return False
            ElseIf txtURLMapped.Text.Trim.Length <= 0 Then
                ErrorLabel.Text = "Please enter URL Mapped"
                Return False
            Else
                ErrorLabel.Text = ""
                Return True
            End If
        End Function

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            txtFunctionalityName.Text = ""
            txtURLMapped.Text = ""
            chkIsActive.Checked = False
            drpLive.SelectedIndex = -1
            drpLive.SelectedIndex = 0
        End Sub

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'TODO: Get Group Name List
        End Sub
    End Class

End Namespace
