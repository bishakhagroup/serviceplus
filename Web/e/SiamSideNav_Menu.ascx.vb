Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports Sony.US.SIAMUtilities
Imports System.Xml
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls

Namespace SIAMAdmin
    Partial Class SiamSideNav_Menu
        Inherits System.Web.UI.UserControl
        Dim array_funct() As Sony.US.siam.Functionality
        Dim strCurrentGroupName As String = String.Empty
        Dim strPreviousGroupName As String = String.Empty

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            'loadMenu("3875600601006838")
        End Sub

        Public Function loadMenu(ByVal lstFunctionality As Sony.US.siam.Functionality()) As Boolean

            'Dim oUsrSecMgr As New UserRoleSecurityManager
            'array_funct = oUsrSecMgr.GetLoggedUserFunctionality(pSIAMID)
            ''Array.Sort(array_funct)
            'If array_funct.Length > 0 Then
            '    dgMenu.DataSource = array_funct
            '    dgMenu.DataBind()
            '    dgMenu.Visible = True
            '    Return True
            'Else
            '    dgMenu.Visible = False
            '    Return False
            'End If

            '********************Changes done by Mujahid E Azam for Bug# 2633 Starts here***************
            If lstFunctionality Is Nothing Then
                Return False
            End If
            '********************Changes done by Mujahid E Azam for Bug# 2633 ends here***************

            array_funct = lstFunctionality
            If lstFunctionality.Length > 0 Then
                dgMenu.DataSource = lstFunctionality
                dgMenu.DataBind()
                dgMenu.Visible = True
                Return True
            Else
                dgMenu.Visible = False
                Return False
            End If
        End Function

        Protected Sub dgMenu_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgMenu.ItemDataBound
            'Dim cnfgHandler As New ConfigHandler()
            Dim lnkbtn As LinkButton
            Dim objFunctionality As Sony.US.siam.Functionality
            If (e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item) Then
                objFunctionality = array_funct(e.Item.ItemIndex)
                strCurrentGroupName = objFunctionality.GroupName
                If strPreviousGroupName <> strCurrentGroupName Then
                    Dim lblGroup As SPSLabel
                    lblGroup = e.Item.FindControl("lblGroup")
                    lblGroup.Text = "<br/>" + strCurrentGroupName + "<br/>"
                    lblGroup.Visible = True
                    strPreviousGroupName = strCurrentGroupName
                End If
                lnkbtn = e.Item.FindControl("lnkFunction")
                If (Not lnkbtn Is Nothing) Then
                    'If ((objFunctionality.URLMapped.ToString().ToUpper = "DBSANITIZER.ASPX")) And ConfigurationData.GeneralSettings.Environment.ToUpper() = "PRODUCTION" Then
                    '    lnkbtn.Attributes.Add("onclick", "javascript:alert('This functionality is not available in Production.'); return false;")
                    'Else
                    '    lnkbtn.Attributes.Add("onclick", "javascript:LoadContentFrame('" + objFunctionality.URLMapped.ToString() + "'); return false;")
                    'End If
                    lnkbtn.Attributes.Add("onclick", "javascript:LoadContentFrame('" + objFunctionality.URLMapped.ToString() + "'); return false;")
                End If
            End If
        End Sub
    End Class
End Namespace