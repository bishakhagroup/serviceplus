<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_student_report" CodeFile="training-student-report.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_student_report</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/style2.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="0"
				width="300" border="0">
				<TR>
					<TD class="PageHead" style="HEIGHT: 27px" align="center">Training Participant 
						Report</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 27px" align="center">
						<asp:label id="errorMessageLabel" runat="server" Font-Size="Smaller" ForeColor="Red" EnableViewState="False"
							CssClass="Body"></asp:label></TD>
				</TR>
				<TR>
					<TD>
						<TABLE id="Table1" style="WIDTH: 792px; HEIGHT: 184px" cellPadding="0"
							width="792" border="0">
							<TR>
								<TD style="WIDTH: 162px">
									<asp:Label id="labelCourseNumber" runat="server" Width="144px" Font-Names="Times New Roman"
										Font-Size="10pt">COURSENUMBER</asp:Label></TD>
								<TD style="WIDTH: 291px">
									<asp:Label id="labelTitle" runat="server" Width="320px" Font-Names="Times New Roman" Font-Size="10pt">TITLE</asp:Label></TD>
								<TD style="WIDTH: 69px">
									<asp:Label id="Label1" runat="server" Font-Names="Times New Roman" Font-Size="10pt">From</asp:Label></TD>
								<TD style="WIDTH: 136px">
									<asp:Label id="labelStartDate" runat="server" Width="120px" Font-Names="Times New Roman" Font-Size="10pt">STARTDATE</asp:Label></TD>
								<TD style="WIDTH: 56px">
									<asp:Label id="Label2" runat="server" Font-Names="Times New Roman" Font-Size="10pt">To</asp:Label></TD>
								<TD>
									<asp:Label id="labelEndDate" runat="server" Width="120px" Font-Names="Times New Roman" Font-Size="10pt">ENDDATE</asp:Label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 162px; HEIGHT: 8px" colSpan="6"></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 162px" colSpan="6" rowSpan="1">
									<asp:DataGrid id="participantGrid" OnPageIndexChanged="participantGrid_Paging" runat="server"
										Width="784px" Font-Names="Times New Roman" Font-Size="10pt" AllowPaging="True" PageSize="15"
										AutoGenerateColumns="False" GridLines="None">
										<Columns>
											<asp:BoundColumn DataField="FIRSTNAME" HeaderText="First Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="LASTNAME" HeaderText="Last Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="EMAIL" HeaderText="EMail"></asp:BoundColumn>
											<asp:BoundColumn DataField="PHONE" HeaderText="Phone"></asp:BoundColumn>
											<asp:BoundColumn DataField="STATUS" HeaderText="Status"></asp:BoundColumn>
										</Columns>
										<PagerStyle Mode="NumericPages"></PagerStyle>
									</asp:DataGrid></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 162px" colSpan="6">
									<TABLE id="Table2" style="WIDTH: 648px; HEIGHT: 24px" width="648"
										border="0">
										<TR>
											<TD align="center" colSpan="1" rowSpan="1">
												<asp:Button id="btnBack" runat="server" Text="Back"></asp:Button></TD>
											<TD align="center">
												<asp:Button id="btnExport" runat="server" Text="Export"></asp:Button></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
