Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports System.Data

Namespace SIAMAdmin

    Partial Class MaintainShippingMethod
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub




#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorLabel.Visible = False
                ErrorLabel.Text = String.Empty
                If Not IsPostBack Then

                    'Populate DataGrid
                    InitializeControls(False)
                    PopulateDataGrid()
                    bindCountryDDL()
                    bindLanguageDDL()
                    btnCancel.Enabled = False
                    btnSave.Enabled = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnSave_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Dim objSIAMOperation As New SIAMOperation()
            Dim objPCILogger As New PCILogger() '6524
            'Dim objGlobalData As New GlobalData
            Dim intDisplayOrder As Int16
            Dim salesOrg As String
            Dim language As String

            If Valid() = False Then
                ErrorLabel.Text = "Please correct following red marked field(s)."
                Exit Sub
            End If

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnSave_click"
                objPCILogger.Message = "btnSave_click Success."

                If txtDISPLAYORDER.Text <> String.Empty Then
                    intDisplayOrder = Convert.ToInt16(txtDISPLAYORDER.Text)
                End If

                language = ddllanguage.SelectedItem.Value
                If ddlcountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                    salesOrg = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                    ' ASleight - Not sure what the purpose of below is. If we're adding American shipping methods, add "French" to the language dropdown during save? Why?
                    Dim newListItem2 As New ListItem()
                    newListItem2.Text = ConfigurationData.GeneralSettings.GlobalData.Language.CA
                    newListItem2.Value = ConfigurationData.GeneralSettings.GlobalData.Language.CA
                    ddllanguage.Items.Add(newListItem2)
                Else
                    salesOrg = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA
                End If

                If btnSave.Text = "Save" Then
                    objPCILogger.EventType = EventType.Add
                    objSIAMOperation.StoreShippingMethod(-1, Convert.ToInt16(drpStatusCode.SelectedValue), intDisplayOrder, txtSISBOCARRIERCODE.Text, txtTRACKINGURL.Text, txtSISBOPICKGROUPCODE.Text, Convert.ToInt16(drpBACKORDEROVERRIDE.SelectedValue), txtDESCRIPTION.Text, txtSISCARRIERCODE.Text, txtSISPICKGROUPCODE.Text, txtPRSVCARRIERCODE.Text, salesOrg, language, 1)
                ElseIf btnSave.Text = "Update" Then
                    objPCILogger.EventType = EventType.Update
                    objSIAMOperation.StoreShippingMethod(Convert.ToInt32(lblShippingMethodID.Text), Convert.ToInt16(drpStatusCode.SelectedValue), intDisplayOrder, txtSISBOCARRIERCODE.Text, txtTRACKINGURL.Text, txtSISBOPICKGROUPCODE.Text, Convert.ToInt16(drpBACKORDEROVERRIDE.SelectedValue), txtDESCRIPTION.Text, txtSISCARRIERCODE.Text, txtSISPICKGROUPCODE.Text, txtPRSVCARRIERCODE.Text, salesOrg, language, 2)
                End If
                ErrorLabel.Text = "Saved successfully."

                dgShippingMethod.SelectedIndex = -1
                btnSave.Enabled = False
                btnAddNew.Enabled = True
                btnCancel.Enabled = False
                btnSearch.Enabled = True
                btnSave.Text = "Save"
                PopulateDataGrid()
                InitializeControls(False)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "btnSave_click Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            dgShippingMethod.SelectedIndex = -1
            InitializeControls(False)

            btnSave.Enabled = False
            btnAddNew.Enabled = True
            btnSearch.Enabled = True
            btnCancel.Enabled = False
            bindLanguageDDL()
            btnSave.Text = "Save"
        End Sub

        Private Sub AddNewUSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
            dgShippingMethod.SelectedIndex = -1
            InitializeControls(True)
            btnSave.Enabled = True
            btnCancel.Enabled = True
            btnSave.Text = "Save"
            btnAddNew.Enabled = False
            btnSearch.Enabled = False
            txtTRACKINGURL.Focus()
        End Sub

        Protected Sub dgShippingMethod_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingMethod.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    dgShippingMethod.SelectedIndex = e.Item.ItemIndex

                    InitializeControls(True)

                    drpBACKORDEROVERRIDE.SelectedIndex = -1
                    drpBACKORDEROVERRIDE.Items.FindByValue(e.Item.Cells(6).Text).Selected = True

                    drpStatusCode.SelectedIndex = -1
                    drpStatusCode.Items.FindByValue(e.Item.Cells(11).Text).Selected = True



                    If e.Item.Cells(15).Text = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US Then
                        ddlcountry.SelectedIndex = 1
                    Else
                        ddlcountry.SelectedIndex = 2
                    End If
                    bindLanguageDDL()

                    ddllanguage.SelectedIndex = -1

                    ddllanguage.Items.FindByValue(e.Item.Cells(16).Text).Selected = True

                    txtDESCRIPTION.Text = e.Item.Cells(2).Text
                    txtTRACKINGURL.Text = e.Item.Cells(3).Text
                    txtSISBOCARRIERCODE.Text = e.Item.Cells(4).Text
                    txtSISBOPICKGROUPCODE.Text = e.Item.Cells(5).Text
                    txtSISCARRIERCODE.Text = e.Item.Cells(7).Text
                    txtSISPICKGROUPCODE.Text = e.Item.Cells(8).Text
                    txtPRSVCARRIERCODE.Text = e.Item.Cells(9).Text
                    txtDISPLAYORDER.Text = e.Item.Cells(10).Text
                    lblShippingMethodID.Text = e.Item.Cells(12).Text
                    btnSave.Enabled = True
                    btnSave.Text = "Update"
                    btnAddNew.Enabled = False
                    btnSearch.Enabled = False
                    btnCancel.Enabled = True
                ElseIf e.CommandName = "Delete" Then
                    Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
                    objSIAMOperation.StoreShippingMethod(Convert.ToInt32(e.Item.Cells(12).Text), 0, 0, "", "", "", 0, "", "", "", "", "", "", 3)
                    ErrorLabel.Text = "Deleted successfully."

                    InitializeControls(False)

                    btnSave.Enabled = False
                    btnAddNew.Enabled = True
                    btnSearch.Enabled = True
                    btnCancel.Enabled = False
                    btnSave.Text = "Save"
                    PopulateDataGrid()
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function Valid() As Boolean
            'If Validation.IsUrl(txtTRACKINGURL.Text) = False Then
            '    ErrorLabel.Text = "Tracking URL is incorrect."
            '    Return False
            'Else
            Dim blnValid As Boolean = True
            lblTrackingURL.CssClass = "bodyCopyBold"
            lblDesc.CssClass = "bodyCopyBold"
            lblSISBOPICKGROUPCODE.CssClass = "bodyCopyBold"
            lblSISCARRIERCODE.CssClass = "bodyCopyBold"
            lblSISPICKGROUPCODE.CssClass = "bodyCopyBold"
            lblSISBOCARRIERCODE.CssClass = "bodyCopyBold"
            lblDISPLAYORDER.CssClass = "bodyCopyBold"
            lblPRSVCARRIERCODE.CssClass = "bodyCopyBold"
            lblCountry.CssClass = "bodyCopyBold"
            lblLanguage.CssClass = "bodyCopyBold"
            lblStatusCode.CssClass = "bodyCopyBold"

            If txtTRACKINGURL.Text = "" Then
                lblTrackingURL.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtDESCRIPTION.Text = "" Then
                lblDesc.CssClass = "redAsterick"
                blnValid = False
            End If
            'If txtSISBOPICKGROUPCODE.Text = "" Then
            '    lblSISBOPICKGROUPCODE.CssClass = "redAsterick"
            '    blnValid = False
            'End If
            If txtSISCARRIERCODE.Text = "" Then
                lblSISCARRIERCODE.CssClass = "redAsterick"
                blnValid = False
            End If
            'If txtSISPICKGROUPCODE.Text = "" Then
            '    lblSISPICKGROUPCODE.CssClass = "redAsterick"
            '    blnValid = False
            'End If
            If txtSISBOCARRIERCODE.Text = "" Then
                lblSISBOCARRIERCODE.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtDISPLAYORDER.Text = String.Empty Or IsNumeric(txtDISPLAYORDER.Text) = False Then
                lblDISPLAYORDER.CssClass = "redAsterick"
                blnValid = False
            End If
            If txtPRSVCARRIERCODE.Text = "" Then
                lblPRSVCARRIERCODE.CssClass = "redAsterick"
                blnValid = False
            End If

            If ddlcountry.Items.FindByText("ALL").Selected = True Then
                lblCountry.CssClass = "redAsterick"
                blnValid = False
            End If


            If ddlcountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                'lblCountry.CssClass = "redAsterick"
                'blnValid = False
                If ddllanguage.Items.FindByText("ALL").Selected = True Then
                    lblLanguage.CssClass = "redAsterick"
                    blnValid = False
                End If
                'ElseIf ddllanguage.Items.FindByText("ALL").Selected = True Then
                '    lblLanguage.CssClass = "redAsterick"
                '    blnValid = False
            End If

            If drpStatusCode.Items.FindByText("All").Selected = True Then
                lblStatusCode.CssClass = "redAsterick"
                blnValid = False
            End If

            Return blnValid
        End Function

        Protected Sub dgShippingMethod_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShippingMethod.ItemDataBound
            Dim lnkbtn As LinkButton
            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                lnkbtn = e.Item.FindControl("lnkBtnDelete")
                If Not lnkbtn Is Nothing Then
                    lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                End If
            End If
        End Sub

        Private Sub dgShippingMethod_PageIndexChanged(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles dgShippingMethod.PageIndexChanged
            Try
                dgShippingMethod.CurrentPageIndex = e.NewPageIndex
                If Session("SearchDataTable") Is Nothing Then
                    PopulateDataGrid()
                Else
                    fnPopulate()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateDataGrid()

            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsShippingMethod As DataSet = objSIAMOperation.GETShippingMethods()
            Session("ShippingMethods") = dsShippingMethod
            dgShippingMethod.SelectedIndex = -1
            dgShippingMethod.DataSource = dsShippingMethod.Tables(0)
            dgShippingMethod.DataBind()
        End Sub

        Private Sub fnPopulate()
            Dim dtResult As DataTable = New DataTable()
            If Not Session("SearchDataTable") Is Nothing Then
                dtResult = Session("SearchDataTable")
                dgShippingMethod.SelectedIndex = -1
                dgShippingMethod.DataSource = dtResult
                dgShippingMethod.DataBind()
            Else
                fnSearch()
            End If
        End Sub

        Private Sub fnGetShippingMethod()
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsShippingMethod As DataSet = objSIAMOperation.GETShippingMethods()
            Session("ShippingMethods") = dsShippingMethod
        End Sub

        Private Sub InitializeControls(ByVal Enable As Boolean)
            txtDESCRIPTION.Text = ""
            txtDISPLAYORDER.Text = ""
            txtPRSVCARRIERCODE.Text = ""
            txtSISBOCARRIERCODE.Text = ""
            txtSISBOPICKGROUPCODE.Text = ""
            txtSISCARRIERCODE.Text = ""
            txtSISPICKGROUPCODE.Text = ""
            txtTRACKINGURL.Text = ""

            drpBACKORDEROVERRIDE.SelectedIndex = 0
            drpStatusCode.SelectedIndex = 0

            bindLanguageDDL()
            ddlcountry.SelectedIndex = 0
            ddllanguage.SelectedIndex = 0

            txtDESCRIPTION.Enabled = Enable
            txtDISPLAYORDER.Enabled = Enable
            txtPRSVCARRIERCODE.Enabled = Enable
            txtSISBOCARRIERCODE.Enabled = Enable
            txtSISBOPICKGROUPCODE.Enabled = Enable
            txtSISCARRIERCODE.Enabled = Enable
            txtSISPICKGROUPCODE.Enabled = Enable
            txtTRACKINGURL.Enabled = Enable
            drpBACKORDEROVERRIDE.Enabled = Enable
            'drpStatusCode.Enabled = Enable
            'ddlcountry.Enabled = Enable
            'ddllanguage.Enabled = Enable

            lblTrackingURL.CssClass = "bodyCopyBold"
            lblDesc.CssClass = "bodyCopyBold"
            lblSISBOPICKGROUPCODE.CssClass = "bodyCopyBold"
            lblSISCARRIERCODE.CssClass = "bodyCopyBold"
            lblSISPICKGROUPCODE.CssClass = "bodyCopyBold"
            lblSISBOCARRIERCODE.CssClass = "bodyCopyBold"
            lblDISPLAYORDER.CssClass = "bodyCopyBold"
            lblPRSVCARRIERCODE.CssClass = "bodyCopyBold"
            lblCountry.CssClass = "bodyCopyBold"
            lblLanguage.CssClass = "bodyCopyBold"
            lblStatusCode.CssClass = "bodyCopyBold"

        End Sub
#End Region

#Region "       Helper Methods      "
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

#End Region



        Private Sub bindCountryDDL()
            Try
                'Fetch from DB 
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                Dim countrydetail As DataSet = data_store.GetCountryDetails()

                If countrydetail.Tables(0).Rows.Count > 0 Then
                    With countrydetail.Tables(0)
                        For i As Integer = 0 To countrydetail.Tables(0).Rows.Count - 1
                            ddlcountry.Items.Add(New ListItem(countrydetail.Tables(0).Rows(i)("COUNTRYNAME"), countrydetail.Tables(0).Rows(i)("COUNTRYCODE")))
                        Next
                    End With
                End If

                ddlcountry.Items.Insert(0, "ALL")
                'DropDownList1.Items.Insert(0, "Select");

                'bind card type array to dropdown list control   
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Sub bindLanguageDDL()
            Try
                ddllanguage.Items.Clear()

                ddllanguage.Items.Insert(0, "ALL")
                Dim newListItem1 As New ListItem()
                newListItem1.Text = "English"
                newListItem1.Value = ConfigurationData.GeneralSettings.GlobalData.Language.US ' en-US
                ddllanguage.Items.Add(newListItem1)

                Dim newListItem2 As New ListItem()
                newListItem2.Text = "French"
                newListItem2.Value = ConfigurationData.GeneralSettings.GlobalData.Language.CA ' fr-CA
                ddllanguage.Items.Add(newListItem2)

                'DropDownList1.Items.Insert(0, "Select");
                'bind card type array to dropdown list control   
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlcountry_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged

            Try
                ddllanguage.Items.Clear()

                If ddlcountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Or ddlcountry.SelectedValue = "ALL" Then

                    ddllanguage.Items.Insert(0, "ALL")
                    Dim newListItem1 As New ListItem()
                    newListItem1.Text = "English"
                    newListItem1.Value = ConfigurationData.GeneralSettings.GlobalData.Language.US
                    ddllanguage.Items.Add(newListItem1)

                    Dim newListItem2 As New ListItem()
                    newListItem2.Text = "French"
                    newListItem2.Value = ConfigurationData.GeneralSettings.GlobalData.Language.CA
                    ddllanguage.Items.Add(newListItem2)
                Else
                    Dim newListItem1 As New ListItem()
                    newListItem1.Text = "English"
                    newListItem1.Value = ConfigurationData.GeneralSettings.GlobalData.Language.US
                    ddllanguage.Items.Add(newListItem1)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub


        Private Sub fnSearch()
            If Session("ShippingMethods") Is Nothing Then
                fnGetShippingMethod()
            End If

            Dim dsShippingMethod As DataSet = New DataSet()
            dsShippingMethod = Session("ShippingMethods")

            Dim blSearch As Boolean = False
            Dim sbSrarch As StringBuilder = New StringBuilder()
            If Not ddlcountry.SelectedValue = "ALL" Then
                blSearch = True
                sbSrarch.Append(" SALESORGANIZATION = '" + ddlcountry.SelectedItem.Text + "'")
            End If

            If Not ddllanguage.SelectedValue = "ALL" Then
                blSearch = True
                If sbSrarch.ToString().Trim().Length > 0 Then
                    sbSrarch.Append(" And ")
                End If
                sbSrarch.Append(" LANGUAGE_ID  = '" + ddllanguage.SelectedValue + "'")
            End If

            If Not drpStatusCode.SelectedValue.ToUpper() = "ALL" Then
                blSearch = True
                If sbSrarch.ToString().Trim().Length > 0 Then
                    sbSrarch.Append(" And ")
                End If
                sbSrarch.Append(" StatusCode  = '" + drpStatusCode.SelectedValue + "'")
            End If

            If blSearch Then
                Dim dtResult As DataTable = New DataTable()
                'dtResult = dsShippingMethod.Tables(0).Select(" SALESORGANIZATION = 'Canada'").CopyToDataTable()

                Dim str As String = sbSrarch.ToString()
                Try
                    dtResult = dsShippingMethod.Tables(0).Select(sbSrarch.ToString()).CopyToDataTable()

                Catch ex As Exception
                    dtResult = dsShippingMethod.Tables(0).Clone()
                End Try

                Session("SearchDataTable") = dtResult
                dgShippingMethod.SelectedIndex = -1
                dgShippingMethod.DataSource = dtResult
                dgShippingMethod.DataBind()

            Else
                Session.Remove("SearchDataTable")
                dgShippingMethod.SelectedIndex = -1
                dgShippingMethod.DataSource = dsShippingMethod.Tables(0)
                dgShippingMethod.DataBind()
            End If

        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                fnSearch()

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class

End Namespace


