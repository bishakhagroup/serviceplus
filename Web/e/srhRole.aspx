<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="srhRole.aspx.vb" Inherits="SIAMAdmin.srhRole" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Search User Role</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK href="includes/style.css" type="text/css" rel="stylesheet">
</head>

<body>
<form id="Form1" name="frmRole" runat=server>
<table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px;">Search Role</td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr height="25">
				    <td class="Nav3"><a href="userRole.aspx">New</a></td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td class=Nav3>Search</td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
    <tr>
        <td colspan=2>
          <div id="div1" style="BORDER-RIGHT: gray thin solid; BORDER-TOP: gray thin solid; BORDER-LEFT: gray thin solid; BORDER-BOTTOM: gray thin solid; width:100%">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td style="width: 170px; text-align: right">&nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
          <tr> 
            <td style="width: 170px; text-align: right;">
                <asp:Label ID="lblRole" runat="server" CssClass="body" Text="Role :"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">&nbsp;<SPS:SPSTextBox ID="txtRoleName" runat="server" CssClass="body" MaxLength="50"></SPS:SPSTextBox></span></td>
          </tr>
          <tr> 
            <td style="width: 170px"><div align="right"><asp:Label ID="lblActive" runat="server" CssClass="body" Text="Is Active :"></asp:Label></div></td>
            <td><asp:CheckBox ID="chkActive" Checked="true" runat="server" CssClass="body" /></td>
          </tr>
          <tr> 
            <td style="width: 299px; height: 19px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="cmdSearch" runat="server" CssClass="body" Text="Search" ToolTip="Click to Search the Role" />
                <asp:Button ID="cmdCancel" runat="server" CssClass="body" Text=" Cancel " ToolTip="Click to Cancel the entry" />&nbsp;
              </div></td>
          </tr>
          </table>&nbsp;
          </div>        
        </td>
    </tr>
    <tr>
    <td colspan=2>
    <asp:datagrid id="dgSearchRole" runat="server" AutoGenerateColumns="False" Width=100%>
		<Columns>
			<asp:BoundColumn DataField="RoleID" HeaderText="Role ID"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Role" DataField="RoleName"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Is Active" DataField="IsDeleted"></asp:BoundColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
					<asp:HyperLink runat="server" Text="Modify" NavigateUrl='<%# "modifyRole.aspx?Identity="+DataBinder.Eval(Container.DataItem, "RoleID").ToString() + "&rn=" + DataBinder.Eval(Container.DataItem, "RoleName").ToString() + "&as=" + DataBinder.Eval(Container.DataItem, "IsDeleted").ToString() %>' ID="Hyperlink2"></asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
        <ItemStyle BackColor="SlateGray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="ControlDarkDark" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        </asp:datagrid>    
    </td>
    </tr>
  </table>
</form>
</body>
</html>