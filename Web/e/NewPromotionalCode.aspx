<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="False" 
CodeFile="NewPromotionalCode.aspx.vb" Inherits="SIAMAdmin.NewPromotionalCode" 
CodeFileBaseClass="SIAMAdmin.UtilityClass"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Add New Promotional Code</title>
    <style type="text/css">
        .bodycopy
        {
            margin-left: 0px;
        }
    </style>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <script language=javascript>
		function ConfirmDelete()
		{
		    window.event.returnValue = confirm("This activity will delete selected user...");		   
		}
	</script>
       
</head>
<body>
    <form id="form1" runat="server">
    
    <table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px;">
            Promotional Code Manager</td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr style="height:25">
				    <td class=Nav3>New</td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td><A class=Nav3 href="SearchPromotionalCode.aspx">Search</A></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
            <tr>
        <td colspan=2 align="left" >
        <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
          <div id="div1" style="BORDER-RIGHT: gray thin solid; BORDER-TOP: gray thin solid; BORDER-LEFT: gray thin solid; BORDER-BOTTOM: gray thin solid; width:98%; ">
            <table width=100% border=0 cellpadding=0 cellspacing=0 >
                <tr>
                    <td style="width: 170px; text-align: right">&nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
                <tr bgcolor="#f2f5f8" > 
            <td style="text-align: right" >
                <asp:Label ID="lblpromocodenumber" runat="server" CssClass="Body" Text="Promo Code Number:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtpromocodenumber" runat="server" CssClass="Body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span></td>
          </tr>
                <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblPromoStartdate" runat="server" CssClass="Body" Text="Promo Start Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtPromoStartdate" runat="server" CssClass="Body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span><asp:Label ID="Label3" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
          </tr>
                <tr bgcolor="#f2f5f8"> 
            <td style="text-align: right" >
                <asp:Label ID="lblpromoenddate" runat="server" CssClass="Body" Text="Promo End Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtpromoenddate" runat="server" CssClass="Body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span><asp:Label ID="Label4" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
          </tr>
                <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblproregenddate" runat="server" CssClass="Body" Text="Product Registration End Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtproregenddate" runat="server" CssClass="Body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span><asp:Label ID="Label5" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
             </tr>
             
                <tr  bgcolor="#f2f5f8">   
                <td style="text-align: right">
                <asp:Label ID="lblmessage" runat="server" CssClass="Body" Text="Success Message:"></asp:Label></td>
            <td>
                 <SPS:SPSTextBox ID="txtmessage" runat="server" CssClass="Body" Height = "60px" width= "250px"
                     TextMode="MultiLine"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span></td>
             </tr>
              <tr>
                <td style="text-align: right"><asp:Label ID="lblindustry" runat="server" CssClass="Body" Text="Industry:"></asp:Label>
                    </td>
                <td>
                    <asp:ListBox  ID="PUSH" SelectionMode ="Multiple" CssClass="Body" Width="250px" runat="server"></asp:ListBox>
                   
                                                 
                    </td>
            </tr>
              <tr style ="height :30px" bgcolor="#f2f5f8">
                <td style="text-align: right">                    
                        <asp:Label ID="lblModelPrefix" runat="server" CssClass="Body" Text="Model Prefix:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlModelPrefix" CssClass="Body" runat="server" AutoPostBack="True"
                                                            Width="70px">
                                                        </asp:DropDownList></td>
            </tr>
             <tr bgcolor="#f2f5f8"  >
                <td style="text-align: right;" valign="top">
                    <asp:Label ID="lblModel" runat="server" CssClass="Body" Text="Model:"></asp:Label></td>
               <%-- <td valign="top">--%>
                    <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>--%>
                                                                                                              
                                                            <%--</ContentTemplate>--%>
                                                           <%-- <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlModelPrefix" EventName="SelectedIndexChanged" />
                                                         </Triggers>--%>
               <%-- </asp:UpdatePanel>--%><%--</td>--%>
            </tr>
            <tr>
                                                                 <td style="width: 200px" bgcolor="#f2f5f8">
                                        </td>
                                                <td  >
                                                    <table style="vertical-align:top;"   border="0" >
                                                        <tr valign="top" >
                                                            <td >
                                                               <table border="0">
                                                            <tr>
                                                            <td align="left"> <asp:label id="Label1" runat="server" CssClass="Body">Available Models:</asp:label></td>
                                                            </tr>
                                                              <tr>
                                                            <td align="left"> <asp:ListBox ID="lstBoxAvailable" Width="125px" CssClass="Body"  SelectionMode="Multiple" runat="server"></asp:ListBox>
                                                           </td>
                                                            </tr>
                                                            </table>
                                                           
                                                                </td>
                                                           
											                <td valign="middle" align ="center" style="padding-left:4px;">
											                <table align="center" border="0">
											                <tr>
											                <td><asp:Button ID="btnSelect" runat="server" CssClass="Body" Text=">>" />&nbsp;</td>
											                </tr>  <tr>
											                <td> <asp:Button ID="btnDeSelect" runat="server" CssClass="Body" Text="<<" />&nbsp;</td>
											                </tr>
											                </table>
											                
                                                                
                                                               
                                                            </td>
                                                            <td  >
                                                             <table  border="0">
                                                            <tr>
                                                            <td align="left"> <asp:label id="Label2" runat="server" CssClass="Body">Selected Models:</asp:label></td>
                                                            </tr>
                                                              <tr>
                                                            <td align="left"><asp:ListBox ID="lstSelected" CssClass="Body" Width="125px"  runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                           </td>
                                                            </tr>
                                                            </table>
                                                            
                                                        
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                 </tr>                
            <tr style ="height :30px" style="padding-top:10px;">
                <td style="text-align: right">
                    <asp:Label ID="lblResellerName" runat="server" CssClass="Body" Text="Reseller Name:"></asp:Label></td>
                <td>
                    <asp:ListBox ID="lstResellerName" CssClass="Body" SelectionMode ="Multiple"  runat="server" Width="300px"></asp:ListBox>
                   <%-- <asp:DropDownList ID="ddlResellerName" CssClass="bodycopy" runat="server" Width="300px">--%>
                                                                            <%--</asp:DropDownList>--%></td>
                                                                            
            </tr>  
            <tr> 
            <td style="width: 299px; height: 19px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="cmdSave" runat="server" CssClass="Body" Text="  Save  "  />
                <asp:Button ID="cmdCancel" runat="server" CssClass="Body" Text=" Cancel " ToolTip="Click to Cancel the entry" />&nbsp;
              </div></td>
          </tr>
              </table>
          </td> 
          </tr>
          
            

                                                    
           
            
            
            
            
                     
           
            
          
           </table>&nbsp;
    
    </form>
</body>
</html>
