<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_class_search" CodeFile="training-class-search.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_participant_report</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style2.css" type="text/css" rel="stylesheet">
		<script language=javascript type="text/javascript">
		    function ShowReport(lineNumber,hide,delstatus,type)
		    {
		        window.open('training-participant-report.aspx?LineNo=' + lineNumber + '&hide=' + hide + '&delstatus=' + delstatus + '&type=' + type,'','toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=yes,width=' + screen.width + ',height=' + screen.height);
		        return false;
		    }
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; TOP: 16px; HEIGHT: 143px"
				width="70%" border="0" align="center">
				<TR>
					<TD class="PageHead" align="center" colSpan="4">Training 
						Search</TD>
				</TR>
				<TR>
					<TD style="WIDTH: 520px; HEIGHT: 14px" align="left" colSpan="4"><asp:label id="errorMessageLabel" runat="server" CssClass="Body"  EnableViewState="False"
							ForeColor="Red"></asp:label></TD>
				</TR>
				<TR>
					<TD width=10%><asp:label id="Label1" runat="server" CssClass="Body" Font-Bold="True">Category:</asp:label></TD>
					<TD  width=40%><asp:dropdownlist id="ddMajorCategory" runat="server" Width="100%" CssClass="Body"></asp:dropdownlist></TD>
					<TD width=10% align="right"><asp:label id="Label11" runat="server" CssClass="Body" Font-Bold="True">Model #:</asp:label></TD>
					<TD width=40%><asp:dropdownlist id="ddlModelNumber" runat="server" Width="100%" CssClass="Body"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD ><asp:label id="Label2" runat="server" CssClass="Body" Font-Bold="True">Title:</asp:label></TD>
					<TD colSpan="3"><asp:dropdownlist id="ddTitle" runat="server" Width="100%" CssClass="Body"></asp:dropdownlist></TD>
				</TR>
				<TR>
					<TD ><asp:label id="Label13" runat="server" CssClass="Body" Font-Bold="True">Location:</asp:label></TD>
					<TD style="width: 286px" ><asp:dropdownlist id="ddLocation" runat="server" Width="100%" CssClass="Body"></asp:dropdownlist></TD>
					<TD align="right" ><asp:label id="Label12" runat="server" CssClass="Body" Font-Bold="True">Course#:</asp:label></TD>
					<TD ><SPS:SPSTextBox id="txtCourseNumber" runat="server" Width="100%" ToolTip="Search by Course Number"
							MaxLength="30" CssClass="Body"></SPS:SPSTextBox></TD>
				</TR>
                <tr>
              		<TD ><asp:label id="Label3" runat="server" CssClass="Body" Font-Bold="True">Month:</asp:label></TD>
					<TD style="width: 286px" ><asp:dropdownlist id="ddMonth" runat="server" Width="106px" CssClass="Body"></asp:dropdownlist></TD>
					<TD align="right" ><asp:label id="Label15" runat="server" CssClass="Body" Font-Bold="True">Year:</asp:label></TD>
					<TD ><asp:dropdownlist id="ddYear" runat="server" Width="106px" CssClass="Body"></asp:dropdownlist></TD>
                </tr>
                <TR>
                    <TD align="left" colSpan="1">
                        <asp:Label ID="Label18" runat="server" CssClass="Body" Font-Bold="True" Width="82px">Record Status:</asp:Label></TD>					
                    <TD align="left" colSpan="2">
                        <asp:RadioButtonList ID="rdoStatus" runat="server" CssClass="body" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                            <asp:ListItem Selected="True" Value="1">Active</asp:ListItem>
                            <asp:ListItem Value="2">Deleted</asp:ListItem>
                            <asp:ListItem Value="3">Hidden</asp:ListItem>
                        </asp:RadioButtonList></TD>
                        <td align="left" colspan="3">
                        <asp:CheckBox ID="chkFutureItem" runat="server" CssClass="body" Text="Show future classes only" /></td>
				</TR>
				 <TR>
                    <TD align="left" colSpan="1"></TD>					
                    <td align="left" colspan="5">
                        <asp:CheckBox ID="chkShowReservationOnly" runat="server" CssClass="body" Text="Show only classes with reservations, waitlists, or orders" />
                    </td>
				</TR>
                <tr>
                    <td align="right" colspan="4">
                        &nbsp; &nbsp;</td>
                </tr>
				<TR>
					<TD align="right" colSpan="4">
					<asp:button id="btnSearch" runat="server" Text="Search"></asp:button></TD>
				</TR>
				<TR>
					<TD align="left" colSpan="4" style="height: 19px">
                        &nbsp;</TD>
				</TR>
			</TABLE>
			<TABLE id="Table2" width=100%  border="0">
				<TR width=100%>
					<TD style="WIDTH: 100%">
					<asp:datagrid id="classDataGrid" runat="server" Width="100%" Font-Size="Smaller" AllowPaging="True"
							AutoGenerateColumns="False" OnPageIndexChanged="classDataGrid_Paging">
							<SelectedItemStyle BackColor="Lime"></SelectedItemStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Category">
									<HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Course.MajorCategory.Description") %>' ID="Label4" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Model">
									<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Course.Model") %>' ID="Label5">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Title">
									<HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Course.Title") %>' ID="Label6">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location">
									<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Location.Name")%>' ID="Label7">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Course#">
									<HeaderStyle HorizontalAlign="Center" Width="5px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center"></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink runat="server" ID="hlkCourseNumber" Text='<%# DataBinder.Eval(Container, "DataItem.Course.Number") %>' >
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Start Date">
									<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDateDisplay") %>' ID="Label9">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="End Date">
									<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDateDisplay") %>' ID="Label10">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="PartNumber">
									<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink runat="server" ID="hlkPartNumber" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber") %>' >
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink id="HyperlinkRegister" runat="server" CssClass="body" NavigateUrl="">Details</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn>
									<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" ></ItemStyle>
									<ItemTemplate>
										<asp:HyperLink runat="server" ImageUrl='images/sp_int_reporting_btn.gif'  Visible="False" NavigateUrl='' ID="HyperlinkReport"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
                                <asp:BoundColumn DataField="StatusCode" HeaderText="StatusCode" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Hide" HeaderText="Hide" Visible="False"></asp:BoundColumn>
							</Columns>
						</asp:datagrid></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
