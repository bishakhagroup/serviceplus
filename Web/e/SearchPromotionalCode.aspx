<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SearchPromotionalCode.aspx.vb" Inherits="SIAMAdmin.SearchPromotionalCode" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Promotional Code</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <script language=javascript>
		function ConfirmDelete()
		{
		    window.event.returnValue = confirm("This activity will delete selected user...");		   
		}
	</script>
    <style type="text/css">
        .style1
        {
            width: 170px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px; height: 19px;">
            <asp:Label ID="lblHeader" runat="server">Promotional Code Manager</asp:Label></td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr height="25">
				    <td ><a class="Nav3" href="NewPromotionalCode.aspx">New</a></td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td ><a class="Nav3">Search</a></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
    <tr>
        <td colspan=2>
          <div id="div1" style="width:100%; border-top-width: thin; border-left-width: thin; border-left-color: gray; border-bottom-width: thin; border-bottom-color: gray; border-top-color: gray; border-right-width: thin; border-right-color: gray;">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td style="width: 170px; text-align: right; height: 19px;">&nbsp;
                    </td>
                    <td style="height: 19px">
                    </td>
                </tr>
                <tr bgcolor="#f2f5f8"> 
            <td style="text-align: right" >
                <asp:Label ID="lblpromocodenumber" runat="server" CssClass="Body" Text="Promo Code Number:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtpromocodenumber" runat="server" CssClass="Body"></SPS:SPSTextBox>
                </td>
          </tr>
                <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblPromoStartdate" runat="server" CssClass="Body" Text="Promo Start Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtPromoStartdate" runat="server" CssClass="Body"></SPS:SPSTextBox>
                </span><asp:Label ID="Label1" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
          </tr>
                <tr bgcolor="#f2f5f8"> 
            <td style="text-align: right" >
                <asp:Label ID="lblpromoenddate" runat="server" CssClass="Body" Text="Promo End Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtpromoenddate" runat="server" CssClass="Body"></SPS:SPSTextBox>
               </span><asp:Label ID="Label2" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
          </tr>
          
                <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblproregenddate" runat="server" CssClass="Body" Text="Product Registration End Date:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtproregenddate" runat="server" CssClass="Body"></SPS:SPSTextBox>
               </span><asp:Label ID="Label3" runat="server" CssClass="Body" Text="MM/DD/YYYY"></asp:Label></td>
             </tr>
         <tr>
                <td style="text-align: right"><asp:Label ID="lblindustry" runat="server" CssClass="Body" Text="Industry:"></asp:Label>
                    </td>
                <td>
                    
                    <SPS:SPSDropDownList Width="200" ID="PUSH"   CssClass="bodyCopy" runat="server">
                                                            </SPS:SPSDropDownList>
                                                           
                    </td>
            </tr>
            <tr style ="height :30px" bgcolor="#f2f5f8">
                <td style="text-align: right">                    
                        <asp:Label ID="lblModelPrefix" runat="server" CssClass="Body" Text="Model Prefix:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlModelPrefix" CssClass="Body" runat="server" AutoPostBack="True"
                                                            Width="70px">
                                                        </asp:DropDownList></td>
            </tr>
             <tr bgcolor="#f2f5f8">
                <td style="text-align: right; height: 22px;">
                    <asp:Label ID="lblModel" runat="server" CssClass="Body" Text="Model:"></asp:Label></td>
                <td  >
                    <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>--%>
                                                                <asp:DropDownList ID="ddlModel" CssClass="Body" runat="server" AutoPostBack="True"
                                                                    Width="300px">
                                                                </asp:DropDownList>  
                                                                                                        
                                                            <%--</ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlModelPrefix" EventName="SelectedIndexChanged" />
                                                         </Triggers>
                </asp:UpdatePanel>--%></td>
            </tr>
            <tr style ="height :30px">
                <td style="text-align: right">
                    <asp:Label ID="lblResellerName" runat="server" CssClass="Body" Text="Reseller Name:"></asp:Label></td>
                <td>                    
                   <asp:DropDownList ID="ddlResellerName" CssClass="Body" runat="server" Width="300px">
                                                                           </asp:DropDownList></td>
                                                                            
            </tr>               
          <tr> 
            <td style="width: 299px; height: 18px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="btnSearch" runat="server" CssClass="Body" Text="Search" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="Body" Text="Cancel" />
                &nbsp;
              </div></td>
          </tr>
          </table>&nbsp;
          </div>        
        </td>
    </tr>
    <tr Width=100%>
    <td colspan=2>
    <asp:datagrid id="dgSearchPromo" runat="server" AutoGenerateColumns="False" Width=100% AllowPaging="True" PageSize="3">
		<Columns>
			<asp:BoundColumn DataField="PROMO_CODE_NUMBER" HeaderText="Promo Code Number"></asp:BoundColumn>
            <asp:BoundColumn DataField="PROMO_START_DATE" DataFormatString="{0:M-dd-yyyy}"     HeaderText="Promo Start Date"></asp:BoundColumn>
            <asp:BoundColumn DataField="PROMO_END_DATE" DataFormatString = "{0:M-dd-yyyy}"    HeaderText="Promo End Date"></asp:BoundColumn>
            <asp:BoundColumn DataField="PROD_REG_END_DATE" DataFormatString = "{0:M-dd-yyyy}"  HeaderText="Product Registration End Date"></asp:BoundColumn>
            <asp:BoundColumn DataField="MODELNUMBER"  ItemStyle-Width ="20%" ItemStyle-Wrap ="false"  HeaderText="Model Number"></asp:BoundColumn>
            <asp:BoundColumn DataField="INDUSTRY" HeaderText="Industry" ItemStyle-Width ="15%" ItemStyle-Wrap ="false" ></asp:BoundColumn>
            <asp:BoundColumn DataField="RESELLERNAME" ItemStyle-Wrap ="false"  HeaderText="Reseller Name">
            <ItemStyle Width="15%" /> 
            </asp:BoundColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
					<asp:HyperLink runat="server" Text="Modify" NavigateUrl='<%# "ModifyPromotionalCode.aspx?PromoCodeNumber="+DataBinder.Eval(Container.DataItem, "PROMO_CODE_NUMBER").ToString()%>' ID="Hyperlink2"></asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
					<asp:LinkButton runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PROMO_CODE_NUMBER")%>' CommandName="Delete" Text="Delete" ID="lnkBtnDelete"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
        <ItemStyle BackColor="SlateGray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="ControlDarkDark" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        </asp:datagrid>    
    </td>
    </tr>
  </table>
    </form>
</body>
</html>
