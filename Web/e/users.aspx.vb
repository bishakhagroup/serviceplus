Imports System.Drawing
Imports ServicePLUSWebApp
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.siam

Namespace SIAMAdmin

    Partial Class users
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()

            thisPage = "CustomerInfo.aspx"

            'If Not Request.QueryString("customer") Is Nothing Then
            '    If CType(Request.QueryString("customer"), Boolean) Then
            '        thisPage = "CustomerInfo.aspx"
            '    Else
            '        thisPage = "ManageUser.aspx"
            '    End If
            'Else
            '    thisPage = "ManageUser.aspx"
            'End If

            Try

                If Not Request.QueryString("Customer") Is Nothing Then
                    thisPage = "CustomerInfo.aspx"
                ElseIf Not Request.QueryString("synchdata") Is Nothing Then
                    thisPage = "Fix-Account.aspx"
                End If

            Catch ex As Exception
                thisPage = "CustomerInfo.aspx"
            End Try
        End Sub




#End Region

#Region "   Page Variables  "

        Private sa As New SecurityAdministrator
        Private userlist As ShallowUser()
        Private r As SiamResponse
        Public thisPage As String = String.Empty

#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ErrorLabel.Visible = False
            AddNewUserButton.Visible = False
            HttpContextManager.SelectedCustomer = Nothing   ' ASleight - Bug fix for a selected Customer staying in memory.
            If Session.Item("LastOpr") Is Nothing Then Session.Item("LastOpr") = ""

            Try
                If Not Request.QueryString("IsTraining") Is Nothing Then
                    If CType(Request.QueryString("IsTraining"), Boolean) Then
                        lblHeader.Text = "Purchase Training"
                        Me.AddNewUserButton.Text = "Add New B2C User"
                        AddNewUserButton.Visible = True
                    Else
                        lblHeader.Text = "Find ServicesPLUS User"
                    End If
                Else
                    lblHeader.Text = "Find ServicesPLUS User"
                End If
            Catch ex As Exception
                Utilities.WrapExceptionforUI(ex)
                lblHeader.Text = "Find ServicesPLUS User"
            End Try
            If Not Page.IsPostBack Then controlMethod(sender, e)
        End Sub

        Private Sub SearchButton_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchButton.Click
            controlMethod(sender, e)
        End Sub

        Private Sub ShowAllUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowAllUsersButton.Click
            controlMethod(sender, e)
        End Sub

        Private Sub AddNewUSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddNewUserButton.Click
            controlMethod(sender, e)
        End Sub

        Protected Sub DataGrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.ItemCommand
            Dim objPCILogger As New PCILogger()
            Try
                If e.CommandName = "Delete" Then
                    ' modified by Manish for the bug  -2098 PCI Event Log on 18 DEC 2009
                    ' modified by Manish for 2146 on 28 Dec 2009
                    If Not Session.Item("siamuser") Is Nothing Then
                        'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                        objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                        objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                        objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                        objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                        'objPCILogger.B2B_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId'6524
                        objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524

                    End If
                    objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginMethod = "DataGrid1_ItemCommand(Delete User)"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    'objPCILogger.UserIdentity = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.EventType = EventType.Delete
                    objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                    objPCILogger.Message = "User deleted with SIAM Identity " & e.CommandArgument.ToString()
                    objPCILogger.IndicationSuccessFailure = "Failure"

                    Dim mySA As New SecurityAdministrator
                    Dim oUser As Sony.US.siam.User
                    oUser = Session(CurrentUserSessionVariables)
                    mySA.DeleteUser(e.CommandArgument.ToString(), oUser)
                    objPCILogger.IndicationSuccessFailure = "Success"
                    'If hdnLastOpr.Value = "A" Then
                    If Session.Item("CheckCondition") = "A" Then
                        showAllUsers()
                    Else
                        'r = sa.GetUsersShallow(hdnLastOpr.Value)
                        r = sa.GetUsersShallow(Session.Item("LastOpr"))
                        If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
                            userlist = r.SiamPayLoad
                            DataGrid1.Visible = True
                            PopulateDataGridWithUsers()
                        Else
                            Label1.Text = "No Record Found "
                            DataGrid1.Visible = False
                            ErrorLabel.Text = "No records returned."
                        End If
                    End If
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub
        Private Sub DataGrid1_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        '7086 Starts
        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String

            Dim strImg As String = "<img src='images/ImgName.JPG' style='border:0;' alt='' />"
            Dim strValue As String = ""

            Try
                For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                    strValue += strImg.Replace("ImgName", strChar.ToString())
                Next
            Catch ex As Exception
                Return strValue
            End Try

            Return strValue

        End Function
        '7086 Ends

        Protected Sub DataGrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                Dim lbldgsiam As Label '7086
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                    If Not lnkbtn Is Nothing Then
                        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                    End If
                    '7086 Starts
                    lbldgsiam = e.Item.FindControl("lbldgsiamid")
                    If Not lbldgsiam.Text Is Nothing Then
                        lbldgsiam.Text = GenerateSiamIDImageString(lbldgsiam.Text)
                        lbldgsiam.BackColor = Color.Transparent
                    End If
                    '7086 Ends
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub



        Private Sub pager(ByVal sender As System.Object, ByVal e As DataGridPageChangedEventArgs) Handles DataGrid1.PageIndexChanged
            Try
                If Session.Item("LastOpr") Is Nothing Then
                    r = sa.GetUsersShallow()
                    userlist = r.SiamPayLoad
                Else
                    r = sa.GetUsersShallow(Session.Item("LastOpr").ToString())
                    userlist = r.SiamPayLoad
                End If
                If r.SiamResponseCode = 147 Then
                    DataGrid1.CurrentPageIndex = e.NewPageIndex
                    PopulateDataGridWithUsers()
                Else
                    Session.Remove("LastError")
                    Session.Add("LastError", r.SiamResponseMessage)
                    Response.Redirect("ErrorPage.aspx")
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


        Private Sub sortlist(ByVal sender As System.Object, ByVal e As DataGridSortCommandEventArgs) Handles DataGrid1.SortCommand
            Try
                If Session.Item("LastOpr") Is Nothing Then '6650
                    userlist = sa.GetUsersShallow().SiamPayLoad
                Else
                    userlist = sa.GetUsersShallow(Session.Item("LastOpr").ToString()).SiamPayLoad '6650
                End If
                If e.SortExpression = "Name" Then
                    Array.Sort(userlist)
                End If
                If e.SortExpression = "EmailAddress" Then
                    Array.Sort(userlist, New ShallowUser.EmailComparer)
                End If
                If e.SortExpression = "Company" Then
                    Array.Sort(userlist, New ShallowUser.CompanyComparer)
                End If
                '6650 starts
                If e.SortExpression = "LdapId" Then
                    Array.Sort(userlist, New ShallowUser.LdapIdComparer)
                End If
                '6650 ends
                DataGrid1.DataSource = userlist
                DataGrid1.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub


#End Region

#Region "   Methods     "

        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            DataGrid1.Columns(0).Visible = True

            Try
                Dim user As User = Session(CustomerSessionVariable)
                Dim caller As String = String.Empty

                'If Not Request.QueryString("Identity") Is Nothing Then DeleteUser(Request.QueryString("Identity"))
                If Not sender.ID Is Nothing Then caller = sender.ID.ToString()


                Select Case caller
                    Case Page.ID.ToString()
                        'Case SearchButton_SPLS.ID.ToString()
                        '    doSearchForUser_SPLS()
                    Case SearchButton.ID.ToString()
                        doSearchForUser()
                    Case ShowAllUsersButton.ID.ToString()
                        showAllUsers()
                    Case AddNewUserButton.ID.ToString()
                        addNewUser()
                    Case DataGrid1.ID.ToString()

                End Select

                Me.writeScriptsToPage()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub doSearchForUser()
            Dim srchPattern As String = String.Empty

            '6756
            Dim tempStatusCode As Integer
            If ddlUserStatus.Text.ToUpper() = "ACTIVE" Then
                tempStatusCode = 1
            ElseIf ddlUserStatus.Text.ToUpper() = "INACTIVE" Then
                tempStatusCode = 3
            ElseIf ddlUserStatus.Text.ToUpper() = "ACTIVE AND INACTIVE" Then
                'just a temporary reference value by which we can determine search type for 'Active and Inactive' users
                tempStatusCode = 4
            End If

            ' dal.GetUsers(firstName, lastName,email,company,ldapId,samIdentity,subscriptionId
            'If (NameTextBox.Text = [String].Empty AndAlso txtLastName.Text = [String].Empty AndAlso EmailTextBox.Text = [String].Empty AndAlso CompanyTextBox.Text = [String].Empty AndAlso txtLDAPID.Text = [String].Empty AndAlso txtSiamId.Text = [String].Empty AndAlso txtSubscriptionID.Text = [String].Empty) Then
            '   r = sa.GetUsersShallow("")
            'Else
            '    r = sa.GetUsersShallow(NameTextBox.Text + "*" + txtLastName.Text + "*" + EmailTextBox.Text + "*" + CompanyTextBox.Text + "*" + txtLDAPID.Text.Trim() + "*" + txtSiamId.Text.Trim() + "*" + txtSubscriptionID.Text.Trim())
            'End If
            srchPattern = NameTextBox.Text + "*" + txtLastName.Text + "*" + EmailTextBox.Text + "*" + CompanyTextBox.Text + "*" + txtLDAPID.Text.Trim() + "*" + txtSiamId.Text.Trim() + "*" + txtSubscriptionID.Text.Trim() + "*" + txtUserID.Text.ToString() + "*" + tempStatusCode.ToString()
            r = sa.GetUsersShallow(srchPattern)

            'AND ROWNUM < (9*10 + 1) AND  LOWER(SIAMUSER.username) like '%test%' ORDER BY ROWNUM DESC) WHERE RNM > ((9-1)*10) ORDER BY RNM ASC
            If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
                userlist = r.SiamPayLoad
                DataGrid1.Visible = True
                PopulateDataGridWithUsers()
                'hdnLastOpr.Value = srchPattern
                Session.Add("LastOpr", srchPattern)
            Else
                Label1.Text = "No Record Found "
                DataGrid1.Visible = False
                ErrorLabel.Text = "No records returned."
            End If
        End Sub

        Private Sub doSearchForUser_SPLS()
            Dim srchPattern As String = String.Empty

            If txtSubscriptionID.Text.Length > 0 Then
                srchPattern = srchPattern + " and LOWER(subscriptionid) like '%" + txtSubscriptionID.Text.ToLower() + "%'"
            End If

            Session.Remove("SearchPattern")
            Session.Add("SearchPattern", srchPattern)
            r = sa.GetUsersShallow(srchPattern, True)
            If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
                userlist = r.SiamPayLoad
                'If Not userlist Is Nothing Then
                DataGrid1.Visible = True
                PopulateDataGridWithUsers()
                'Else
                'End If
            Else
                ErrorLabel.Text = "No records returned."
            End If
        End Sub

        Private Sub showAllUsers()

            r = sa.GetUsersShallow()
            If r.SiamResponseMessage = Messages.GET_USERS_COMPLETED.ToString() Then
                userlist = r.SiamPayLoad
                PopulateDataGridWithUsers()
                'hdnLastOpr.Value = "A"
                Session.Add("CheckCondition", "A")
            Else
                Session.Remove("LastError")
                Session.Add("LastError", r.SiamResponseMessage)
                Response.Redirect("ErrorPage.aspx")
            End If
        End Sub

        Private Sub addNewUser()
            Try
                Dim toURL As String = ""
                Try
                    If Not Request.QueryString("IsTraining") Is Nothing Then
                        If CType(Request.QueryString("IsTraining"), Boolean) Then
                            toURL = "ManageUser.aspx?IsTraining=true"
                        Else
                            toURL = "ManageUser.aspx"
                        End If
                    Else
                        toURL = "ManageUser.aspx"
                    End If
                Catch ex As Exception
                    toURL = "ManageUser.aspx"
                End Try

                Response.Redirect(toURL, True)
            Catch ex1 As System.Threading.ThreadAbortException
                '-- do nothing expected exception
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateDataGridWithUsers()
            Label1.Visible = True
            If Not userlist Is Nothing Then
                Label1.Text = "Found " + userlist.Length.ToString() + " Records"
            Else
                Label1.Text = "No Record Found "
            End If
            DataGrid1.Visible = True
            DataGrid1.DataSource = userlist
            DataGrid1.AllowPaging = False
            If (Not userlist Is Nothing) Then
                If (userlist.Length > DataGrid1.PageSize) Then
                    DataGrid1.AllowPaging = True
                End If
            End If
            Try
                DataGrid1.DataBind()
            Catch ex As Exception
                DataGrid1.CurrentPageIndex = 0
                DataGrid1.DataBind()
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            DataGrid1.ApplyStyle(GetStyle(0))
            DataGrid1.PageSize = 10
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = False
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.HeaderStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.HeaderStyle.ForeColor = Color.GhostWhite
            DataGrid1.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub

        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

#End Region

    End Class

End Namespace


