Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_type_edit
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-type-list") Is Nothing) Then
                        Dim typeSelected As CourseType
                        Dim itemNumber As Integer = -1
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        typeSelected = CType(Session.Item("training-type-list"), Array)(itemNumber)
                        Me.txtCode.Text = typeSelected.Code
                        Me.txtDescription.Text = typeSelected.Description
                    End If


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering type info:" + ErrorLabel.Text
                Return
            End If
            Dim bUpdate As Boolean = False
            Dim typeSelected As CourseType
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then 'Update
                    bUpdate = True
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    typeSelected = CType(Session.Item("training-type-list"), Array)(itemNumber)
                Else
                    typeSelected = New CourseType
                End If

                typeSelected.Code = Me.txtCode.Text
                typeSelected.Description = Me.txtDescription.Text
                Dim cm As New CourseManager
                If bUpdate Then
                    objPCILogger.Message = "UpdateCourseType Success."
                    objPCILogger.EventType = EventType.Update
                    cm.UpdateCourseType(typeSelected)
                Else
                    objPCILogger.Message = "AddCourseType Success."
                    objPCILogger.EventType = EventType.Add
                    cm.AddCourseType(typeSelected)
                End If


                ErrorLabel.Visible = True


                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Add/Update CourseType Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-type-list.aspx")
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            ErrorLabel.Text = ""

            Me.labelCode.CssClass = "Body"
            Me.labelDescription.CssClass = "Body"

            Me.LabelCodeStar.Text = ""
            Me.LabelDescriptionStar.Text = ""

            If Me.txtCode.Text = "" Then
                Me.labelCode.CssClass = "redAsterick"
                Me.LabelCodeStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If Me.txtDescription.Text = "" Then
                Me.labelDescription.CssClass = "redAsterick"
                Me.LabelDescriptionStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            ElseIf Me.txtDescription.Text.Length > 50 Then
                ErrorLabel.Text = "Description can not be longer than 50"
                bValid = False
            End If
            Return bValid
        End Function
    End Class

End Namespace
