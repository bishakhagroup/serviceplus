<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.AccountPayment" CodeFile="AccountPayment.ascx.vb" %>
<LINK href="includes/style2.css" type="text/css" rel="stylesheet">
<TABLE id="Table1" style="WIDTH: 344px; HEIGHT: 207px" cellPadding="0"
	width="344" border="0">
	<TR>
		<TD style="HEIGHT: 16px" colSpan="2"><asp:label id="Label19" runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Account Payment Information:</asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 21px" align="right"><asp:label id="Label1" runat="server" CssClass="Body">SAP Bill to Accnt #:</asp:label></TD>
		<TD style="HEIGHT: 21px"><asp:dropdownlist id="ddlAccntNumber" runat="server" CssClass="Body" Width="192px" AutoPostBack="True"></asp:dropdownlist></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 23px" align="right" colSpan="1" rowSpan="1"><asp:label id="Label2" runat="server" CssClass="Body">Bill to Name:</asp:label></TD>
		<TD style="HEIGHT: 23px"><asp:label id="labelName" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 23px" align="right"><asp:label id="Label3" runat="server" CssClass="Body">Bill to Address 1:</asp:label></TD>
		<TD style="HEIGHT: 23px"><asp:label id="labelStreet1" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label4" runat="server" CssClass="Body">Bill to Address 2:</asp:label></TD>
		<TD style="HEIGHT: 24px"><asp:label id="labelStreet2" runat="server" CssClass="Body"></asp:label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 24px" align="right">
			<asp:Label id="Label5" runat="server" CssClass="Body">Bill to Address 3:</asp:Label></TD>
		<TD style="HEIGHT: 24px">
			<asp:Label id="labelStreet3" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 24px" align="right">
			<asp:Label id="Label6" runat="server" CssClass="Body">Bill to Address 4:</asp:Label></TD>
		<TD style="HEIGHT: 24px">
			<asp:Label id="lblAddress4" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 24px" align="right">
			<asp:Label id="Label7" runat="server" CssClass="Body">Bill to State:</asp:Label></TD>
		<TD style="HEIGHT: 24px">
			<asp:Label id="labelState" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 133px; HEIGHT: 25px" align="right">
			<asp:Label id="Label8" runat="server" CssClass="Body">Bill to Postal Code:</asp:Label></TD>
		<TD style="HEIGHT: 25px">
			<asp:Label id="labelZip" runat="server" CssClass="Body"></asp:Label></TD>
	</TR>
</TABLE>
