<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns="urn:schemas-microsoft-com:office:spreadsheet" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="urn:my-scripts" 
	xmlns:o="urn:schemas-microsoft-com:office:office" 
	xmlns:x="urn:schemas-microsoft-com:office:excel" 
	xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">

<xsl:template match="CourseParticipant">

<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 
 <!--<Styles>
  <Style ss:ID="Default" ss:Name="Normal">
	<Alignment ss:Vertical="Bottom"/>
	<Borders/>
	<span/>
	<Interior/>
	<NumberFormat/>
	<Protection/>
  </Style>
  <Style ss:ID="s21">
	<span ss:Bold="1"/>
	<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
  </Style>
 <Style ss:ID="s22">
	<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
	<span ss:Bold="1"/>
	<Interior ss:Color="#99CCFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s23" ss:Name="Currency">
	<NumberFormat
		ss:Format="_(&amp;quot;$&amp;quot;* #,##0.00_);_(&amp;quot;$&amp;quot;* \(#,##0.00\);_(&amp;quot;$&amp;quot;* &amp;quot;-&amp;quot;??_);_(@_)"/>
  </Style>
  <Style ss:ID="s24">
	<NumberFormat ss:Format="_(* #,##0.00_);_(* \(#,##0.00\);_(* &amp;quot;-&amp;quot;??_);_(@_)"/>
  </Style>
  <Style ss:ID="s25">
	<Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
  </Style>
 </Styles>-->

 <Worksheet>
	<xsl:attribute name="ss:Name">
		<xsl:value-of select='concat("Part #", Class/PARTNUMBER)'/>
	</xsl:attribute>
	<Table ss:ExpandedColumnCount="5">
		<xsl:attribute name="ss:ExpandedRowCount">
			<xsl:value-of select="count(Class) + count(Participants)+ 1"/>
	</xsl:attribute>

   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Column ss:AutoFitWidth="0" ss:Width="75"/>
   <Column ss:AutoFitWidth="0" ss:Width="100"/>

   <xsl:apply-templates select="Class"/>

   <Row>
		<Cell><Data ss:Type="String">First Name</Data></Cell>
		<Cell><Data ss:Type="String">Last Name</Data></Cell>
		<Cell><Data ss:Type="String">Email</Data></Cell>
		<Cell><Data ss:Type="String">Phone</Data></Cell>
		<Cell><Data ss:Type="String">Status</Data></Cell>
   </Row>
   
   <xsl:apply-templates select="Participants"/>

   </Table>
  </Worksheet>
 </Workbook>
</xsl:template>

<xsl:template match="Class">
   <Row>
    <Cell><Data ss:Type="String"><xsl:value-of select="COURSENUMBER"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="TITLE"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select='concat("From ",STARTDATE)'/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select='concat("To ",ENDDATE)'/></Data></Cell>
    <Cell><Data ss:Type="String"> </Data></Cell>
   </Row>
</xsl:template>

<xsl:template match="Participants">
   <Row>
    <Cell><Data ss:Type="String"><xsl:value-of select="FIRSTNAME"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="LASTNAME"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="EMAIL"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="PHONE"/></Data></Cell>
    <Cell><Data ss:Type="String"><xsl:value-of select="STATUS"/></Data></Cell>
   </Row>
</xsl:template>

</xsl:stylesheet>

  