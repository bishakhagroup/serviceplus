<%@ Reference Control="~/e/CustomerDataSubNavigation.ascx" %>
<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.ViewSISAccounts" CodeFile="ViewSISAccounts.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<%@ Register TagPrefix="SIAMCustomerService" TagName="CustomerSubNav" Src="CustomerDataSubNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>View SIS Accounts</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table>				
				<tr>
					<td align="center">
						<table width="500" border="0">
							<tr>
								<td><SIAMCUSTOMERSERVICE:CUSTOMERSUBNAV id="CustomerSubNav" runat="server"></SIAMCUSTOMERSERVICE:CUSTOMERSUBNAV></td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label id="ErrorLabel" runat="server" CssClass="body" EnableViewState="False" ForeColor="Red"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>
						<asp:datagrid id="DataGrid1" CssClass="Body" Runat="server">
							<Columns>
								<asp:TemplateColumn HeaderText="Validated">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Validated") %>' ID="Label4">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Account">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountNumber") %>' ID="Label1">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Company">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Name") %>' ID="Label2">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Address">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.Line1") + " " + DataBinder.Eval(Container, "DataItem.Address.Line2") %>' ID="Label3">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="State">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.State") %>' ID="Label6">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Zip">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address.PostalCode").ToString() %>' ID="Label7">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
