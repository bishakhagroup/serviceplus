<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="uc" TagName="CustomerMenu" Src="~/e/CustomerDataSubNavigation.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="SIAMAdmin.CustomerInfo"
    CodeFile="CustomerInfo.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Customer Info</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <style type="text/css">
        .style1
        {
            height: 12px;
            width: 291px;
        }
        .style2
        {
            width: 291px;
        }
        .style5
        {
            width: 477px;
        }
        .style6
        {
            height: 12px;
            width: 313px;
        }
        .style7
        {
            width: 313px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%" border="0">
        <tr>
            <td align="left">
                <uc:CustomerMenu ID="ucCustomerMenu" runat="server" />
            </td>
        </tr>
        <tr bgcolor="#5d7180">
            <td class="PageHead" align="center" style="color: white;" colspan="1" height="30"
                rowspan="1">
                Manage User
            </td>
        </tr>
        <tr>
            <td style="height: 19px" height="19">
                <asp:Label ID="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <table align="center" width="100%">
                    <tr>
                        <td>
                            <div id="div1" style="border: gray thin solid; width: 100%;">
                                <table width="100%">
                                    <tr>
                                        <td valign="top" class="style5">
                                            <div id="div2" style="border-right: gray thin solid; border-top: medium none; border-left: medium none;
                                                border-bottom: medium none">
                                                <table border="0" width="100%">
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblUserName" runat="server" CssClass="body" EnableViewState="False">User Name</asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style7">
                                                            <SPS:SPSTextBox ID="txtUserName" runat="server" CssClass="body" Width="122px" MaxLength="50"
                                                                Enabled="false"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblEmailAddress" runat="server" CssClass="body" EnableViewState="False">Email Address</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <SPS:SPSTextBox ID="txtEmailAddress" runat="server" CssClass="body" Width="220px"
                                                                MaxLength="100"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblUserType" runat="server" CssClass="body">User Type</asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <asp:Label ID="lblUserStatus" runat="server" CssClass="body">User Status</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style7">
                                                            <SPS:SPSDropDownList ID="ddlUserType" runat="server" Width="220px" CssClass="body">
                                                            </SPS:SPSDropDownList>
                                                        </td>
                                                        <td class="style7">
                                                            <SPS:SPSDropDownList ID="ddlUserStatus" runat="server" Width="220px" CssClass="body">
                                                                <asp:ListItem Value="1" Text="Active" />
                                                                <asp:ListItem Value="3" Text="Inactive" />
                                                            </SPS:SPSDropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblSIAMID" runat="server" CssClass="body" EnableViewState="False">SIAM Identity</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblsiamidimg" runat="server" CssClass="body" Width="220px"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblCustomerID" runat="server" CssClass="body" EnableViewState="False">Customer ID</asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <asp:Label ID="lblLDAPID" runat="server" CssClass="body" EnableViewState="False">LDAP ID</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style7">
                                                            <SPS:SPSTextBox ID="txtCustomerID" runat="server" CssClass="body" Width="120px" Enabled="False"></SPS:SPSTextBox>&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtLDAPID" runat="server" CssClass="body" Width="120px" Enabled="False"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblSubscriptionID" runat="server" CssClass="body" EnableViewState="False">Subscription ID</asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style7">
                                                            <SPS:SPSTextBox ID="txtSubscriptionID" runat="server" CssClass="body" Width="120px"></SPS:SPSTextBox>&nbsp;&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="isSubscriptionActive" CssClass="body" Enabled="false" runat="server"
                                                                TextAlign="left" Text="Subscription Active?" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblSendInfo" runat="server" CssClass="body" EnableViewState="False">OK to Send Information : </asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <asp:RadioButtonList ID="rdbtnSendInfo" RepeatDirection="Horizontal" runat="server"
                                                                CssClass="body">
                                                                <asp:ListItem Value="True">Yes</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="False">No</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <asp:Label ID="lblCaliforniaTax" runat="server" CssClass="body" EnableViewState="False">Ask customer if section 6378 California partial tax exemption<br/>is applicable when placing order shipping to California?If yes, then use eNable document ID below: </asp:Label>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style6">
                                                            <SPS:SPSTextBox ID="txtEnableDocumentIDNumber" runat="server" CssClass="body" Width="120px"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td>
                                            <div id="div3" style="border-right: medium none; width: 100%; border-top: medium none;
                                                border-left: medium none; border-bottom: medium none">
                                                <table border="0" width="100%">
                                                    <tr>
                                                        <td class="style1">
                                                            <asp:Label ID="lblFirstName" runat="server" CssClass="body" EnableViewState="False">First Name</asp:Label>
                                                        </td>
                                                        <td style="height: 14px">
                                                            <asp:Label ID="lblLastName" runat="server" CssClass="body" EnableViewState="False">Last Name</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="body" Width="100px" MaxLength="50"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="body" Width="100px" MaxLength="50"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblCompanyName" runat="server" CssClass="body" EnableViewState="False">Company Name</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <SPS:SPSTextBox ID="txtCompanyName" runat="server" CssClass="body" Width="220px"
                                                                MaxLength="50"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 11px">
                                                            <asp:Label ID="lblAddressLine1" runat="server" CssClass="body" EnableViewState="False">Street Address</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <SPS:SPSTextBox ID="txtAddressLine1" runat="server" CssClass="body" Width="220px"
                                                                MaxLength="50"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblAddressLine2" runat="server" CssClass="body" EnableViewState="False">Address 2nd Line</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2">
                                                            <SPS:SPSTextBox ID="txtAddressLine2" runat="server" CssClass="body" Width="220px"
                                                                MaxLength="50"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="CountryLabel" runat="server" CssClass="body" EnableViewState="False">Country</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="body" Width="78px" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblCity" runat="server" CssClass="body" EnableViewState="False">City</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblState" runat="server" CssClass="body" EnableViewState="False">State</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblZipCode" runat="server" CssClass="body" EnableViewState="False">Zip Code</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="body" Width="112px" MaxLength="50"></SPS:SPSTextBox>&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="body">
                                                            </asp:DropDownList>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtZipCode" runat="server" CssClass="body" Width="55px" MaxLength="7"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblPhoneNumber" runat="server" CssClass="body" EnableViewState="False">Phone Number</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblExtension" runat="server" CssClass="body" EnableViewState="False">Ext</asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblFaxNumber" runat="server" CssClass="body" EnableViewState="False">Fax Number</asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtPhoneNumber" runat="server" CssClass="body" Width="92px" MaxLength="20"></SPS:SPSTextBox>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtExtension" runat="server" CssClass="body" Width="47px" MaxLength="6"></SPS:SPSTextBox>&nbsp;
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtFaxNumber" runat="server" CssClass="body" Width="92px" MaxLength="20"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1" class="style2">
                                                            <asp:Label ID="lblDateReg" runat="server" CssClass="body">Last Account Update</asp:Label>
                                                        </td>
                                                        <td colspan="1">
                                                            <asp:Label ID="lblLastLoginTime" runat="server" CssClass="body">Last Login Time</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="1" class="style2">
                                                            <SPS:SPSTextBox ID="txtDateReg" runat="server" CssClass="body" Width="120px" Enabled="False"></SPS:SPSTextBox>
                                                        </td>
                                                        <td colspan="1">
                                                            <SPS:SPSTextBox ID="txtLastLoginTime" runat="server" CssClass="body" Width="120px"
                                                                Enabled="False"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="3">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="style2">
                                                            <asp:Button ID="btnUpdateUser" runat="server" CssClass="body" Text="Update User">
                                                            </asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <SPS:SPSTextBox ID="htxtStatusId" Visible="False" runat="server"></SPS:SPSTextBox>
                            <SPS:SPSTextBox ID="htxtSiamIdentity" Visible="False" runat="server"></SPS:SPSTextBox>
                            <SPS:SPSTextBox ID="htxtLDAPID" Visible="False" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
