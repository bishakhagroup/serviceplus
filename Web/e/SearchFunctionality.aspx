<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SearchFunctionality.aspx.vb" Inherits="SIAMAdmin.SearchFunctionality" CodeFileBaseClass="SIAMAdmin.UtilityClass"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html>
<head>
    <title>Search Functionality</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
	<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
	<meta content="JavaScript" name="vs_defaultClientScript">
	<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<LINK href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
</head>
<body>
    <form id="form1" method="post" runat="server">
<table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px; height: 19px;">
            <asp:Label ID="lblHeader" runat="server">Search Functionality</asp:Label></td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr height="25">
				    <td ><a class="Nav3" href="FunctionalityMaster.aspx">New</a></td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td ><a class="Nav3">Search</a></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
    <tr>
        <td colspan=2>
          <div id="div1" style="width:100%; border-top-width: thin; border-left-width: thin; border-left-color: gray; border-bottom-width: thin; border-bottom-color: gray; border-top-color: gray; border-right-width: thin; border-right-color: gray;">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td style="width: 170px; text-align: right; height: 19px;">&nbsp;
                    </td>
                    <td style="height: 19px">
                    </td>
                </tr>
          <tr> 
            <td style="width: 170px; text-align: right; height: 26px;">
                <asp:Label ID="lblFunctionalityName" runat="server" CssClass="Body">Functionality Name:</asp:Label></td>
            <td style="height: 26px"><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtFunctionalityName" runat="server" CssClass="Body" MaxLength="25"></SPS:SPSTextBox></span></td>
          </tr>
                <tr>
                    <td style="width: 170px; height: 24px; text-align: right">
                        <asp:Label ID="lblURLMapped" runat="server" CssClass="Body">URL Mapped:</asp:Label></td>
                    <td style="height: 24px">
                        <SPS:SPSTextBox ID="txtURLMapped" runat="server" CssClass="Body" MaxLength="50"></SPS:SPSTextBox></td>
                </tr>
          <tr> 
            <td style="width: 170px; height: 20px;"><div align="right">
                &nbsp;<asp:Label ID="lblIsActive" runat="server" CssClass="Body">Is Active:</asp:Label></div></td>
            <td style="height: 20px"><asp:CheckBox ID="chkIsActive" Checked="true" runat="server" CssClass="Body" /></td>
          </tr>
          <tr> 
            <td style="width: 299px; height: 18px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td colspan="2"><div align="center"> 
                <asp:Button ID="btnSearch" runat="server" CssClass="Body" Text="Search" />&nbsp;
                <asp:Button ID="btnCancel" runat="server" CssClass="Body" Text="Cancel" />
                &nbsp;
              </div></td>
          </tr>
          </table>&nbsp;
          </div>        
        </td>
    </tr>
    <tr>
    <td colspan=2>
    <asp:datagrid id="dgSearchFunctionality" runat="server" AutoGenerateColumns="False" Width=100% AllowSorting="True">
		<Columns>
			<asp:BoundColumn DataField="FUNCTIONALITYID" HeaderText="ID"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Functionality Name" DataField="FUNCTIONALITYNAME"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="URL Mapped" DataField="URLMapped"></asp:BoundColumn>
            <asp:BoundColumn HeaderText="Active" DataField="IsDeleted"></asp:BoundColumn>
			<asp:TemplateColumn>
				<ItemTemplate>
					<asp:HyperLink runat="server" Text="Modify" NavigateUrl='<%# "ModifyFunctionality.aspx?Identity="+DataBinder.Eval(Container.DataItem, "FUNCTIONALITYID").ToString() + "&fname=" + DataBinder.Eval(Container.DataItem, "FUNCTIONALITYNAME").ToString() + "&URLMapped=" + DataBinder.Eval(Container.DataItem, "URLMapped").ToString()+ "&Active=" + DataBinder.Eval(Container.DataItem, "IsDeleted").ToString()+ "&GroupID=" + DataBinder.Eval(Container.DataItem, "GroupID").ToString()+ "&sitestatus=" + DataBinder.Eval(Container.DataItem, "SITESTATUS").ToString()%>' ID="Hyperlink2"></asp:HyperLink>
				</ItemTemplate>
			</asp:TemplateColumn>
		</Columns>
        <ItemStyle BackColor="SlateGray" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        <HeaderStyle BackColor="ControlDarkDark" Font-Bold="False" Font-Italic="False" Font-Overline="False"
            Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
        </asp:datagrid>    
    </td>
    </tr>
  </table>
    </form>
</body>
</html>
