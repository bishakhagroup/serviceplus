<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.OrderDetails" CodeFile="OrderDetails.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>View Order Details</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <style type="text/css">
        .style1 {
            height: 19px;
        }

        .style2 {
            height: 3px;
        }

        .style3 {
            height: 11px;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table width="100%" align="center" border="0">
            <tr>
                <td align="center" colspan="2" height="20">
                    <asp:Label ID="ErrorLabel" runat="server" ForeColor="Red" CssClass="body"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2" height="5">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table width="100%" border="0">
                        <tr>
                            <td align="center" colspan="3">
                                <span class="BodyHead">&nbsp;Order Number <span class="BodyHead">
                                    <asp:Label ID="OrderNumberLabel" runat="server" CssClass="body"
                                        EnableViewState="False"></asp:Label>
                                    <br />
                                </span></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span class="BodyHead">Order Information from&nbsp;<asp:Label ID="Lblsource" runat="server" Text=""></asp:Label>
                                    <br />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span class="Body">Order Date: 
                                <asp:Label ID="lblOrderDate" runat="server" CssClass="body" EnableViewState="False"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><span class="Body">PO Number:&nbsp;<asp:Label ID="PoNumberLabel" runat="server" CssClass="body"></asp:Label></span></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span class="Body">SIAM Identity :
                                <asp:Label ID="SIAMIdentityLabel" runat="server" CssClass="body"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <span class="Body">Account Number:
                                <asp:Label ID="CCOrOnAccountLabel" runat="server" CssClass="body"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style3">
                                <span class="Body">Credit Card Number Last Four Digits: &nbsp;<asp:Label ID="lblCreditCard" runat="server" CssClass="body" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="Body">Expiration Date: &nbsp;
                                <asp:Label ID="lblExpiryDate" runat="server" CssClass="body"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="250">
                                <span class="Body">Item Subtotal: </span>
                                <asp:Label ID="lblPartSubTotal" runat="server" CssClass="body"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="250">
                                <span class="Body">Shipping and Handling: </span>
                                <asp:Label ID="lblshippinghandling" runat="server" CssClass="body"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="300" class="style1">
                                <span class="Body">Tax: </span>
                                <asp:Label ID="LblTax" runat="server" CssClass="body"></asp:Label>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="300" colspan="2">
                                <span class="Body">Recycle Fees: </span>
                                <asp:Label ID="lblRecyclingfees" runat="server" CssClass="body"></asp:Label>
                                &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="300" colspan="2">
                                <span class="Body">Total Order Amount: </span>
                                <asp:Label ID="lblAmount" runat="server" CssClass="body"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="250"><span class="Body"><b>Billing Information:</b></span></td>
                <td width="250"><span class="Body"><b>Shipping Information:</b></span></td>
            </tr>
            <tr>
                <td width="250">
                    <asp:Label ID="BillingInfoLabel" runat="server" CssClass="body"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="ShippingInfoLabel" runat="server" CssClass="body"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="BodyHead">Order Information from ServicesPLUS Database<br /></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="Body">
                        <%-- Modified By Sneha on 30/12/2013 DIEN replaced with ZDIE--%>
                        <asp:Label ID="lblMateriaType" runat="server" />
                        <asp:Label ID="label1" runat="server" Text="" /><br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="Body">
                        <b>Tax Exemption Information</b><br />
                        <br />
                        Alt. Tax Classification: &nbsp;<asp:Label ID="Lblalttax" runat="server" Text="" /></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="Body">Enable Document ID Number: &nbsp;<asp:Label ID="Lblenabledocidnum" runat="server" Text="" /><br />
                        <br />
                        <br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <div id="displayGoBtn" runat="server">
                        <a id="refFindOrders" href="FindOrders.aspx"><img src="images/sp_int_back_btn.gif" alt="Go Back" border="0"></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="BodyHead"><asp:Label ID="Lblorderdetail" runat="server" Text="" /></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DataGrid ID="DataGrid2" Width="100%" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="10"
                        GridLines="Horizontal" runat="server">
                        <HeaderStyle HorizontalAlign="Center" Height="30" ForeColor="GhostWhite" BackColor="#666666" />
                        <PagerStyle Mode="NumericPages" Height="10" />
                        <ItemStyle ForeColor="GhostWhite" Font-Size="10" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Item Number">
                                <ItemStyle Width="15%" HorizontalAlign="Left" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PartNumber") %>' ID="ItemNumber" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Description">
                                <ItemStyle Width="35%" HorizontalAlign="Left" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Description") %>' ID="Description" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="QTY">
                                <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Quantity") %>' ID="Quantity" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Your Price">
                                <ItemStyle Width="10%" HorizontalAlign="Right" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatCurrency(Eval("Item_Price").ToString(), , , TriState.True, TriState.True) %>' ID="ListPrice" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total Price">
                                <ItemStyle Width="10%" HorizontalAlign="Right" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#  FormatCurrency((Eval("Item_Price") * Eval("Quantity")).ToString(), , , TriState.True, TriState.True) %>' ID="TotalPrice" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0">
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Table ID="InvoicesTable" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
