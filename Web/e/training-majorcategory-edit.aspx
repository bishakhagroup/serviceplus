<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_majorcategory_edit" CodeFile="training-majorcategory-edit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <title>training_majorcategory_edit</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="includes/style.css" type="text/css" rel="stylesheet">
  </HEAD>
  <body>

    <form id="Form1" method="post" runat="server">
<TABLE id=Table3 
style="Z-INDEX: 101; LEFT: 8px; WIDTH: 549px; POSITION: absolute; TOP: 8px; HEIGHT: 216px" 
cellSpacing=0 cellPadding=0 align=center border=0>
  <TR>
    <TD class=PageHead style="HEIGHT: 1px" align=center colSpan=1 
    rowSpan=1>Training Course Major Category</TD></TR>
    <tr>
        <td align="left" class="PageHead">
        </td>
    </tr>
  <TR>
    <TD class=PageHead align=left>
            <asp:label id=ErrorLabel runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label>&nbsp;</TD></TR>
  <%--<TR>
    <TD style="HEIGHT: 12px">
        <asp:label id=Label23 runat="server" Font-Size="Smaller" CssClass="Body" Font-Bold="True"> Course Major Category Information:</asp:label></TD></TR>--%>
  <TR>
    <TD  vAlign=top align=left>
      <TABLE id=Table2 style="WIDTH: 560px; " cellSpacing=0   cellPadding=0 width=560 border=0>
             <tr><td colspan=2>&nbsp;</td></tr> 
        <TR>
          <TD style="WIDTH: 84px; HEIGHT: 5px" vAlign=top align=right>
                <asp:label id=labelDescription runat="server" CssClass="Body">Description:</asp:label></TD>
          <TD style="HEIGHT: 5px" vAlign=top>
            <SPS:SPSTextBox id=txtDescription runat="server" CssClass="Body" Height="33px" Width="432px"></SPS:SPSTextBox>
            <asp:label id=LabelDescriptionStar runat="server" CssClass="redAsterick"><span class="redAsterick">*</span></asp:label></TD></TR>
          <tr>
              <td align="right" style="width: 84px; height: 5px" valign="top">
              </td>
              <td style="height: 5px" valign="top">
              </td>
          </tr>
          <tr>
              <td align="right" style="width: 84px; height: 5px" valign="top">
              </td>
              <td align="right" style="height: 5px" valign="top">
                    <asp:button id=btnUpdate runat="server" Text="Update"></asp:button>&nbsp; &nbsp;<asp:button id=btnCancel runat="server" Text="Back"></asp:button>
                  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
              </td>
          </tr>
      </TABLE></TD></TR>
        <TR>
        <TD  align=center>
          <TABLE id=Table1 style="WIDTH: 562px; HEIGHT: 24px" cellSpacing=0   cellPadding=0 width=562 border=0>
            <TR>
                <TD align=center>
                    &nbsp;<input type=hidden id=txtCode runat=server />
                </TD>
                 <TD align=center>
                     &nbsp; &nbsp;&nbsp;
                 </TD></TR>
          </TABLE>
          
        </TD>
        </TR></TABLE>
                   
    </form>

  </body>
</HTML>
