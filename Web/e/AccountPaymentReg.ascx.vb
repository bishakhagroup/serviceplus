Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class AccountPaymentReg
        Inherits System.Web.UI.UserControl
        Implements IAccountPayment

        Private cm As New CustomerManager
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected m_order As Sony.US.ServicesPLUS.Core.Order


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                lblErrorMsg.Visible = False
                If Not m_order Is Nothing Then
                    Me.txtSAPAccntNumber.Text = m_order.SAPBillToAccountNumber
                    Me.labelName.Text = m_order.BillTo.Name
                    Me.labelStreet1.Text = m_order.BillTo.Line1
                    Me.labelStreet2.Text = m_order.BillTo.Line2
                    Me.labelStreet3.Text = m_order.BillTo.Line3
                    Me.labelZip.Text = m_order.BillTo.PostalCode
                    Me.labelState.Text = m_order.BillTo.State
                End If
            End If
        End Sub

        Private Sub btnEnter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
            Dim adm As AccountDataManager = New AccountDataManager
            Try
                'Dim current_bill_to As Account = adm.GetCustomerAccountByNumber(Me.txtSAPAccntNumber.Text)
                ''get all address details per sap bill_to
                '    Throw New NotImplementedException()
                '    'sis.SAPPayorInquiry(current_bill_to, True)
                'Me.labelName().Text = current_bill_to.Address.Name
                'Me.labelStreet1.Text = current_bill_to.Address.Line1
                'Me.labelStreet2.Text = current_bill_to.Address.Line2
                'Me.labelStreet3.Text = current_bill_to.Address.Line3
                'Me.labelState.Text = current_bill_to.Address.State
                'Me.labelZip.Text = current_bill_to.Address.PostalCode
                Dim objAccount As New Account()
                objAccount.AccountNumber = Me.txtSAPAccntNumber.Text
                ''changed for bug 6219
                Dim current_bill_to As Account = cm.PopulateCustomerDetailWithSAPAccount(objAccount)
                Me.labelName().Text = current_bill_to.Address.Name
                Me.labelStreet1.Text = current_bill_to.Address.Line1
                Me.labelStreet2.Text = current_bill_to.Address.Line2
                Me.labelStreet3.Text = current_bill_to.Address.Line3
                Me.labelState.Text = current_bill_to.Address.State
                Me.labelZip.Text = current_bill_to.Address.PostalCode
                Session.Add("current_trainingbill_to", current_bill_to)
            Catch ex As Exception
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Property Order() As Sony.US.ServicesPLUS.Core.Order Implements IAccountPayment.Order
            Get
                Return m_order
            End Get
            Set(ByVal Value As Sony.US.ServicesPLUS.Core.Order)
                m_order = Value
            End Set
        End Property
        Private Function ValidateForm() As Boolean
            If Me.labelName().Text = "" Then
                Return False
            End If
            Return True
        End Function

        Public Function Save(Optional ByRef customer As Customer = Nothing) As Boolean Implements IAccountPayment.Save
            If Not ValidateForm() Then
                Return False
            End If

            Dim tm As New CourseManager
            Dim current_bill_to As Account = Session.Item("current_trainingbill_to")
            If Not customer Is Nothing Then 'creating order
                m_order = tm.Checkout(customer, current_bill_to)
            End If
            m_order.SAPBillToAccountNumber = current_bill_to.AccountNumber
            m_order.BillTo = current_bill_to.Address
            Return True
        End Function
    End Class

End Namespace
