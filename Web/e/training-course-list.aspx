<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_course_list" CodeFile="training-course-list.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<%@ Register TagPrefix="uc1" TagName="TrainingDataSubNavigation" Src="TrainingDataSubNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_course_list</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" cellSpacing="0"
				width="100%" align="left" border="0">
				<TR>
					<TD class="PageHead" align="center" height="30">Manage&nbsp;Training&nbsp;Courses</TD>
				</TR>
				<TR>
					<TD align="center" height="10">
						<asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left" height="10">
						<uc1:TrainingDataSubNavigation id="TrainingDataSubNavigation" runat="server"></uc1:TrainingDataSubNavigation></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 2px" align="center" height="2"></TD>
				</TR>
				<TR>
					<TD align="center"><span class="BodyHead">
							<asp:Label id="Label1" runat="server">Current Course List</asp:Label></span></TD>
				</TR>
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
				
				<tr>
					<td height="10">&nbsp;&nbsp;<asp:Label id="Label2" runat="server" CssClass="BodyHead">Course Number</asp:Label><SPS:SPSTextBox ID="txtCourseNumber"  CssClass="body" runat=server></SPS:SPSTextBox><asp:Button id="btnSearchCourseNumber" CssClass="body" runat=server Text="Search" /> <asp:label id="lblCourseSearch" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
				<tr width=100%>
					<td style="height: 10px" width=100%>
					        <table>
					            <tr>
					                <td>
					                    &nbsp;<asp:Label id="Label4" runat="server" CssClass="BodyHead">Course Status</asp:Label>
					                </td>
					                <td>
					                    <asp:RadioButtonList ID="rdoStatus" AutoPostBack=true  CssClass="Body" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                            <asp:ListItem Value="2">Hidden</asp:ListItem>
                                            <asp:ListItem Value="3">Deleted</asp:ListItem>
                                        </asp:RadioButtonList>
					                </td>
					            </tr>
					        </table>
                    </td>
				</tr>
				<TR width=100%>
					<TD width=100%>
						<asp:datagrid id="coursesDataGrid" runat="server" Width=100% OnPageIndexChanged="coursesDataGrid_Paging">
							<Columns>
								<asp:TemplateColumn HeaderText="Course#">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Number") %>' NavigateUrl='' ID="lnkCourse">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Title">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Title")) %>' ID="labelTitle">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Price">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.ListPrice")) %>' ID="labelPrice">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<%--<asp:TemplateColumn HeaderText="PreRequisite">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.PreRequisite")) %>' ID="labelPreRequisite">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>--%>
							</Columns>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px" align="center">
						<asp:Button id="btnAdd" runat="server" Text="Add"></asp:Button></TD>
				</TR>
				<TR>
					<TD colSpan="5">&nbsp;</TD>
				</TR>
			</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
