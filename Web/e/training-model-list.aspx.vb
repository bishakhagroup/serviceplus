Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class training_model_list
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    writeScriptsToPage()
                    Dim cdm As New CourseDataManager
                    Dim css As CourseModel() = cdm.GetModels(String.Empty)
                    Session.Add("training-model-list", css)
                    Me.modelDataGrid.DataSource = css
                    Me.modelDataGrid.DataBind()


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Sub modelDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                modelDataGrid.CurrentPageIndex = e.NewPageIndex
                Me.modelDataGrid.DataSource = Session("training-model-list")
                Me.modelDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            modelDataGrid.ApplyStyle(GetStyle(520))
            modelDataGrid.PageSize = 10
            modelDataGrid.AllowPaging = True
            modelDataGrid.AutoGenerateColumns = False
            modelDataGrid.GridLines = GridLines.Horizontal
            modelDataGrid.AllowSorting = True
            modelDataGrid.CellPadding = 3
            modelDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            modelDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            modelDataGrid.PagerStyle.Height = Unit.Pixel(10)
            modelDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            modelDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            modelDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            modelDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            modelDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            modelDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            modelDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub modelDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles modelDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub modelDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles modelDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(0).FindControl("lnkModel"), HyperLink)
                    hyperLink.NavigateUrl = "training-model-edit.aspx?LineNo=" + (e.Item.ItemIndex() + modelDataGrid.CurrentPageIndex * modelDataGrid.PageSize).ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub


        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Response.Redirect("training-model-edit.aspx")
        End Sub

        Protected Sub btnSearchModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchModel.Click
            Try
                Dim cdm As New CourseDataManager
                Dim css As CourseModel() = cdm.GetModels(txtModel.Text.Trim())
                Session.Remove("training-model-list")
                Session.Add("training-model-list", css)

                modelDataGrid.CurrentPageIndex = 0
                'Dim css As CourseModel() = Session.Item("training-model-list")
                'Dim item_list As New ArrayList
                'Dim iCount As Integer = 0
                'Dim bfound As Boolean = False
                'For Each crs As CourseModel In css
                '    'If crs.ReturnDeletedObjects = True Then crs.ReturnDeletedObjects = Not crs.ReturnDeletedObjects
                '    If crs.Model.ToUpper() = txtModel.Text.Trim().ToUpper() Then
                '        item_list.Add(crs)
                '        bfound = True
                '    End If
                '    'crs.ReturnDeletedObjects = Not crs.ReturnDeletedObjects
                '    iCount += 1
                'Next
                'Dim items(item_list.Count - 1) As CourseModel
                'item_list.CopyTo(items)
                'If items.Length > 0 Then
                '    Session.Remove("training-model-list")
                '    Session.Add("training-model-list", items)
                'End If

                If css.Length > 1 Then
                    Me.modelDataGrid.DataSource = css
                    Me.modelDataGrid.DataBind()
                ElseIf css.Length = 1 Then
                    Response.Redirect("training-model-edit.aspx?LineNo=0")
                Else
                    lblModelSearch.Text = "Model " + txtModel.Text.Trim() + " does not exist."
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
    End Class

End Namespace
