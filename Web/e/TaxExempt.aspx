﻿<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="uc" TagName="CustomerMenu" Src="~/e/CustomerDataSubNavigation.ascx" %>

<%@ Page Language="VB" AutoEventWireup="true" Inherits="SIAMAdmin.TaxExempt" CodeFile="TaxExempt.aspx.vb"
    CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Tax Exempt</title>
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        $( function() {
            $( "[id$=txtDocumentDate]" ).datepicker();
            $( "[id$=txtExpirationDate]" ).datepicker();
            $( "[id$=txtNewDocumentDate]" ).datepicker();
            $( "[id$=txtNewExpirationDate]" ).datepicker();
        } );
    </script>

</head>
<body class="Body">
    <form id="form1" runat="server">
    <table width="100%" border="0" class="Body">
        <tr>
            <td align="left">
                <uc:CustomerMenu ID="ucCustomerMenu" runat="server" />
            </td>
        </tr>
        <tr bgcolor="#5d7180">
            <td class="PageHead" align="center" style="color: white;" height="30">
                Tax Exemptions
            </td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="5" width="100%" class="Body">
                    <tr>
                        <th colspan="6">Selected Customer Details</th>                        
                    </tr>
                    <tr>
                        <th>Customer's Name:</th>
                        <th>Company Name:</th>
                        <th>Company State:</th>
                        <th>Email:</th>
                        <th>LDAP ID:</th>
                        <th>SIAM ID:</th>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lblCustomerName" runat="server" /></td>
                        <td><asp:Label ID="lblCustomerCompany" runat="server" /></td>
                        <td><asp:Label ID="lblCustomerState" runat="server" /></td>
                        <td><asp:Label ID="lblCustomerEmail" runat="server" /></td>
                        <td><asp:Label ID="lblCustomerLdap" runat="server" /></td>
                        <td><asp:Label ID="lblCustomerSiam" runat="server" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr height="40">
            <td>
                <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red" />
                <asp:Label ID="lblUserMessage" runat="server" ForeColor="Blue" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grdTaxExempt" Width="100%" CssClass="Body" runat="server" AutoGenerateColumns="False"
                    OnRowEditing="grdTaxExempt_RowEditing" OnRowCancelingEdit="grdTaxExempt_RowCancelingEdit"
                    OnRowUpdating="grdTaxExempt_RowUpdating" ShowFooter="true" EnableViewState="true">
                    <HeaderStyle Font-Bold="true" Height="20px" HorizontalAlign="Center" BackColor="LightSteelBlue" />
                    <FooterStyle CssClass="Body" BackColor="PaleGreen" />
                    <EmptyDataTemplate>
                        <p>Customer has no tax exemptions.</p>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField HeaderText="Row ID" DataField="RowID" Visible="false" />
                        <asp:BoundField HeaderText="SIAM ID" DataField="SiamID" Visible="false" />
                        <asp:TemplateField HeaderText="Company Name">
                            <ItemTemplate>
                                <%#Eval("Name")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName" Text='<%# Eval("Name")%>' runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewName" Text="" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company City">
                            <ItemTemplate>
                                <%#Eval("City")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCity" Text='<%# Eval("City")%>' runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewCity" Text="" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company State">
                            <ItemTemplate>
                                <%#Eval("State")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <%--<asp:TextBox ID="txtState" Text='<%# Eval("State")%>' Width="30" runat="server" />--%>
                                <asp:DropDownList ID="ddlState" SelectedValue='<%# Eval("State")%>' runat="server"></asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox ID="txtState" Text='<%# Eval("State")%>' Width="30" runat="server" />--%>
                                <asp:DropDownList ID="ddlNewState" runat="server"></asp:DropDownList>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exempt State">
                            <ItemTemplate>
                                <%#Eval("DocumentState")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <%--<asp:TextBox ID="txtDocumentState" Text='<%# Eval("DocumentState")%>' Width="30" runat="server" />--%>
                                <asp:DropDownList ID="ddlDocumentState" SelectedValue='<%# Eval("DocumentState")%>' runat="server"></asp:DropDownList>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <%--<asp:TextBox ID="txtDocumentState" Text='<%# Eval("DocumentState")%>' Width="30" runat="server" />--%>
                                <asp:DropDownList ID="ddlNewDocumentState" runat="server"></asp:DropDownList>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exemption Begins">
                            <ItemTemplate>
                                <%#Eval("DocumentDate", "{0:MM/dd/yyyy}")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <input id="txtDocumentDate" type="text" value='<%# Eval("DocumentDate")%>' style="width: 100px;" runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <input id="txtNewDocumentDate" type="text" style="width: 100px;" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Exemption Ends">
                            <ItemTemplate>
                                <%#Eval("ExpirationDate", "{0:MM/dd/yyyy}")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <input id="txtExpirationDate" type="text" value='<%# Eval("ExpirationDate")%>' style="width: 100px;" runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <input id="txtNewExpirationDate" type="text" style="width: 100px;" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deloitte Document ID">
                            <ItemTemplate>
                                <%#Eval("DocumentIDNumber")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDocumentIDNumber" Text='<%# Eval("DocumentIDNumber")%>' Width="80" runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewDocumentIDNumber" Width="80" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Update Date" DataField="UpdateDate" ReadOnly="true" DataFormatString="{0:MM/dd/yyyy}" />
                        
                        <asp:TemplateField ControlStyle-Height="20">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnEdit" ImageUrl="~/Shared/images/Edit.jpg" AlternateText="Edit" CommandName="Edit" runat="server" />
                                <asp:ImageButton ID="btnDelete" ImageUrl="~/Shared/images/delete.jpg" AlternateText="Delete" CommandName="Delete" runat="server" OnClientClick="return confirm('Are you sure you want to delete this exemption?');" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:ImageButton ID="btnUpdate" ImageUrl="~/Shared/images/update.jpg" AlternateText="Update" CommandName="Update" runat="server" />
                                <asp:ImageButton ID="btnCancel" ImageUrl="~/Shared/images/Cancel.jpg" AlternateText="Cancel" CommandName="Cancel" runat="server" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="btnAdd" Text="Add Exemption" OnClick="btnAdd_Click" ForeColor="Blue" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <%--<asp:CommandField ButtonType="Image" ControlStyle-ForeColor="Black" ControlStyle-Height="16px" ItemStyle-Wrap="false"
                            ShowEditButton="true" EditImageUrl="~/Shared/images/Edit.jpg"
                            ShowDeleteButton="true" DeleteImageUrl="~/Shared/images/delete.jpg"
                            ShowCancelButton="true" CancelImageUrl="~/Shared/images/Cancel.jpg"
                            UpdateImageUrl="~/Shared/images/update.jpg" />--%>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr height="60">
            <td>
                <p>NOTE: Tax Exemptions are based on the State the user will be SHIPPING their Order to; not the billing address they order from.
                     If they require exemptions in multiple States, create one entry per State.</p>
                <p>Dates should all be in USA format: mm/dd/yyyy</p>
                <p><b>Deloitte Document ID</b> is generated by the Deloitte system, and should be copied here as-is.</p>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
