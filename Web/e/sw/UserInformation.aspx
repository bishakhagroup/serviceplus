<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true" CodeFile="UserInformation.aspx.cs"
Inherits="sw_UserInformation" Theme="SkinFile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="Updatepnluserinfo" runat="server">
<ContentTemplate>
<table border="0" style="width: 100%">
<tr>
<td style="width: 4px">
</td>
<td>
<table border="0" style="width: 100%">
<tr><td><asp:Label ID="lblMessage" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td></tr>
<tr>
<td>
<asp:TabContainer ID="tabcontainuserinfo" runat="server" ActiveTabIndex="1" 
        Width="900px" AutoPostBack="True">
       
         <asp:TabPanel ID="tabpaneluserinfo" runat="server" HeaderText="User Information">
                    <HeaderTemplate>
                        User Information
                    </HeaderTemplate>
               <ContentTemplate>
                   <table class="style1">
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lbluserid" runat="server" Text="User ID:"></asp:Label>
                                   </td>
                                
                                   <td class="style9" >
                                       <cc1:SPSTextBox ID="txtuserid" runat="server"></cc1:SPSTextBox>
                                       <asp:DropDownList ID="ddluserid" runat="server" AutoPostBack="True" 
                                           OnSelectedIndexChanged="ddluserid_SelectedIndexChanged">
                                       </asp:DropDownList>
                                   </td>
                                  
                                   <td class="style4">
                                       <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                   </td>
                                   <td>
                                       <cc1:SPSTextBox ID="txtEmail" runat="server"></cc1:SPSTextBox>
                                      </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblfname" runat="server" Text="First Name:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <cc1:SPSTextBox ID="txtfname" runat="server"></cc1:SPSTextBox>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lbllname" runat="server" Text="Last Name:"></asp:Label>
                                   </td>
                                   <td>
                                       <cc1:SPSTextBox ID="txtlname" runat="server"></cc1:SPSTextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblrole" runat="server" Text="Role:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <asp:DropDownList ID="ddlrole" runat="server">
                                       </asp:DropDownList>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lbluserstatus" runat="server" Text="User Status:"></asp:Label>
                                   </td>
                                   <td>
                                       <asp:DropDownList ID="ddluserstatus" runat="server">
                                           <asp:ListItem>Active</asp:ListItem>
                                           <asp:ListItem>Inactive</asp:ListItem>
                                       </asp:DropDownList>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lbloffphone" runat="server" Text="Office Phone:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <cc1:SPSTextBox ID="txtoffphone" runat="server"></cc1:SPSTextBox>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lbloffphext" runat="server" Text="Extension:"></asp:Label>
                                   </td>
                                   <td>
                                       <cc1:SPSTextBox ID="txtoffphext" runat="server"></cc1:SPSTextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblchstate" runat="server" Text="State/Province:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <asp:DropDownList ID="ddlchstate" runat="server">
                                       </asp:DropDownList>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lblchcountry" runat="server" Text="Country:"></asp:Label>
                                   </td>
                                   <td>
                                       <asp:DropDownList ID="ddlchcountry" runat="server">
                                       </asp:DropDownList>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblcreatedby" runat="server" Text="Created By:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <cc1:SPSTextBox ID="txtcreatedby" runat="server" Enabled="False"></cc1:SPSTextBox>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lbldatecreated" runat="server" Text="Date Created"></asp:Label>
                                   </td>
                                   <td>
                                       <cc1:SPSTextBox ID="txtdatecreated" runat="server" Enabled="False"></cc1:SPSTextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblupdatedby" runat="server" Text="Updated By:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <cc1:SPSTextBox ID="txtupdatedby" runat="server" Enabled="False"></cc1:SPSTextBox>
                                   </td>
                                   <td class="style4">
                                       <asp:Label ID="lbllastupdated" runat="server" Text="Last Updated:"></asp:Label>
                                   </td>
                                   <td>
                                       <cc1:SPSTextBox ID="txtlastupdated" runat="server" Enabled="False"></cc1:SPSTextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblnotes" runat="server" Text="Notes:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <cc1:SPSTextBox ID="txtnotes" runat="server" Height="50px" TextMode="MultiLine" 
                                           Width="250px"></cc1:SPSTextBox>
                                   </td>
                                   <td class="style4">
                                       &nbsp;</td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                               <tr>
                                   <td class="style10">
                                       <asp:Label ID="lblPasword0" runat="server" Text="Password:"></asp:Label>
                                   </td>
                                   <td class="style9">
                                       <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                       &nbsp;&nbsp;&nbsp; </td>
                                   <td class="style4">
                                       <asp:Label ID="lblConfirmPassword" runat="server" Text="ConfirmPassword"></asp:Label>
                                   </td>
                                   <td>
                                       <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                   </td>
                               </tr>
                             </table>
                   <table class="style1" ID="tblbtn" align="center">
                       <tr>
                           <td class="style8" align="right">
                               <asp:ImageButton ID="btnNewuser" runat="server" CausesValidation="False" 
                                   ImageUrl="~/Shared/images/sw_Add.JPG" OnClick="btnNewuser_Click" />
                           </td>
                           <td class="style7" align="left">
                               <asp:ImageButton ID="btnEdituser" runat="server" CausesValidation="False" 
                                   Height="80%" ImageUrl="~/Shared/images/sw_Edit.JPG" onclick="btnEdituser_Click" 
                                   style="margin-left: 0px" />
                           </td>
                           <td align="left" class="style6">
                               <asp:ImageButton ID="btnSaveuser" runat="server" ImageAlign="AbsMiddle" 
                                   ImageUrl="~/Shared/images/sw_Save.JPG" OnClick="btnSaveuser_Click" />
                           </td>
                           <td align="left">
                               <asp:ImageButton ID="btnDeleteuser" runat="server" 
                                   ImageUrl="~/Shared/images/sw_Delete.JPG" onclick="btnDeleteuser_Click" />
                           </td>
                       </tr>
                   </table>
               </ContentTemplate>
                </asp:TabPanel>
                 <asp:TabPanel ID="tabpanelpersonal" runat="server" HeaderText="Personal">
                    <ContentTemplate>
                        <table class="style1">
                            <tr>
                                <td>
                                    <asp:Label ID="lblchaddress" runat="server" Text="Address:"></asp:Label>
                                </td>
                                <td class="style6">
                                    <cc1:SPSTextBox ID="txtchaddress" runat="server" Height="50px" 
                                        TextMode="MultiLine" Width="250px"></cc1:SPSTextBox>
                                </td>
                                <td class="style4">
                                    </td>
                                <td>
                                    </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="lblchcity" runat="server" Text="City:"></asp:Label>
                                </td>
                                <td class="style6">
                                    <cc1:SPSTextBox ID="txtchcity" runat="server"></cc1:SPSTextBox>
                                </td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="lblchzip" runat="server" Text="Zip/Postal Code:"></asp:Label>
                                </td>
                                <td class="style6">
                                    <cc1:SPSTextBox ID="txtchzip" runat="server"></cc1:SPSTextBox>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="lblfax0" runat="server" Text="Fax:"></asp:Label>
                                </td>
                                <td>
                                    <cc1:SPSTextBox ID="txtfax" runat="server"></cc1:SPSTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="lblchhomephone" runat="server" Text="Home Phone:"></asp:Label>
                                </td>
                                <td class="style6">
                                    <cc1:SPSTextBox ID="txtchhomephone" runat="server"></cc1:SPSTextBox>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="lblchvoicemail" runat="server" Text="Voice Mail:"></asp:Label>
                                </td>
                                <td>
                                    <cc1:SPSTextBox ID="txtchvoicemail" runat="server"></cc1:SPSTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="lblchpager" runat="server" Text="Pager:"></asp:Label>
                                </td>
                                <td class="style6">
                                    <cc1:SPSTextBox ID="txtchpager" runat="server"></cc1:SPSTextBox>
                                </td>
                                <td class="style4">
                                    <asp:Label ID="lblchcellular" runat="server" Text="Cellular:"></asp:Label>
                                </td>
                                <td>
                                    <cc1:SPSTextBox ID="txtchcellular" runat="server"></cc1:SPSTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    &nbsp;</td>
                                <td class="style6">
                                    &nbsp;</td>
                                <td class="style4">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                        <table class="style1" id="Table1" align="center">
                               <tr>
                                   <td align="right">
                                       &nbsp;</td>
                                   <td align="right" class="style2">
                                       <asp:ImageButton ID="btnchsave" runat="server" 
                                           ImageUrl="~/Shared/images/sw_Save.JPG" onclick="btnchsave_Click"
                                            />
                                   </td>
                                   <td align="left" class="style3">
                                       &nbsp;</td>
                                   <td align="left">
                                       <asp:ImageButton ID="btnchdelete" runat="server" 
                                           ImageUrl="~/Shared/images/sw_Delete.JPG" onclick="btnchdelete_Click"  />
                                   </td>
                               </tr>
                           </table>
                    </ContentTemplate>
                </asp:TabPanel>
                  </asp:TabContainer>
</td>
</tr>
</table>
</td>
<td style="width: 4px">
</td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

