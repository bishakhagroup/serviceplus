﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;

public partial class sw_SystemMaster : System.Web.UI.Page {

    #region Properties
    DataSet ds;
    LookupTables lt;
    SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
    XmlErrorLog errorlog;
    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        try {
            lblerror.Text = "";
            if (Session["UserName"] == null) {
                Response.Redirect("Default.aspx?post=SysMaster");
            } else if (!IsPostBack) {
                BindLookuptables();
            }

        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    #region Categories
    private void LoadLookuptablesbycategory() {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            ds = lt.GetLookuptablesbycategory();
            if (ds.Tables[0].Rows.Count > 0) {
                gvcategories.DataSource = ds;
                gvcategories.DataBind();
            } else {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvcategories.DataSource = ds;
                gvcategories.DataBind();
                int columncount = gvcategories.Rows[0].Cells.Count;
                gvcategories.Rows[0].Cells.Clear();
                gvcategories.Rows[0].Cells.Add(new TableCell());
                gvcategories.Rows[0].Cells[0].ColumnSpan = columncount;
                gvcategories.Rows[0].Cells[0].Text = "No Records Found";
            }
        } catch (Exception ex) {
            lblerror.Text = "";
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcategories_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvcategories.EditIndex = -1;
            LoadLookuptablesbycategory();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcategories_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strPRODUCT_CATEGORY = gvcategories.DataKeys[e.RowIndex].Values["PRODUCT_CATEGORY"].ToString();
            lt.deleteLookuptablesbycategory(strPRODUCT_CATEGORY);
            LoadLookuptablesbycategory();
            lblerror.ForeColor = Color.Red;
            lblerror.Text = strPRODUCT_CATEGORY + " details deleted successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Unable to delete Product Category.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated")) {
                lblerror.Text = "Unable to delete Product Category.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }

    }
    protected void gvcategories_RowEditing(object sender, GridViewEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvcategories.EditIndex = e.NewEditIndex;
            LoadLookuptablesbycategory();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcategories_PreRender(object sender, EventArgs e) {
        if (this.gvcategories.EditIndex != -1) {
            TextBox txtcatfocus = gvcategories.Rows[gvcategories.EditIndex].FindControl("txteditcategory") as TextBox;
            txtcatfocus.Focus();
        }
    }
    protected void gvcategories_RowUpdating(object sender, GridViewUpdateEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strprodcategory = gvcategories.DataKeys[e.RowIndex].Values["PRODUCT_CATEGORY"].ToString();
            TextBox txteditcat = (TextBox)gvcategories.Rows[e.RowIndex].FindControl("txteditcategory");
            lt.UpdateLookuptablesbycategory(strprodcategory, txteditcat.Text.Trim());
            gvcategories.EditIndex = -1;
            LoadLookuptablesbycategory();
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strprodcategory + " Details Updated successful.";
        } catch (Exception ex) {
            lblerror.Text = "";
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated")) {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvcategories_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            lblerror.Text = "";
            if (gvcategories.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                gvcategories.PageIndex = e.NewPageIndex;
                LoadLookuptablesbycategory();

            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcategories_RowCommand(object sender, GridViewCommandEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            if (e.CommandName.Equals("AddNew")) {
                TextBox txtnewcat = (TextBox)gvcategories.FooterRow.FindControl("txtnewcategory");
                lt.insertLookuptablesbycategory(txtnewcat.Text);
                LoadLookuptablesbycategory();
                lblerror.ForeColor = Color.Green;
                lblerror.Text = txtnewcat.Text + " Details inserted successful.";
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Duplicate Product Category is not allowed.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated")) {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvcategories_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.Row.RowType == DataControlRowType.DataRow) {
                //getting username from particular row
                string strPRODUCT_CATEGORY = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PRODUCT_CATEGORY"));
                //identifying the control in gridview
                ImageButton lnkbtnresult = (ImageButton)e.Row.FindControl("imgbtnDelete");
                //raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtnresult != null) {
                    lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + strPRODUCT_CATEGORY + "')");
                }
                //User Roles Starts
                ImageButton lnkbtnedit = (ImageButton)e.Row.FindControl("imgbtnEdit");
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                }
                //else if ((Session["Role"] == "SoftwareEngineer"))
                //{
                //    lnkbtndelete.Visible = false;
                //    this.gvcategories.ShowFooter = false;
                //    lblerror.Visible = false;

                //}
                else if ((Session["Role"].ToString() == "SoftwareReadOnly") || (Session["Role"].ToString() == "SoftwareEngineer")) {
                    lnkbtnedit.Visible = false;
                    lnkbtndelete.Visible = false;
                    gvcategories.ShowFooter = false;
                    lblerror.Visible = false;
                }
                //User role Ends

            }
        } catch (Exception) {
            //errorlog = new XmlErrorLog();
            //errorlog.LogError(ex, severity.Error.ToString());
            //lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    #endregion

    #region Countries
    private void LoadLookuptablesbycountry() {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            ds = lt.GetLookuptablesbycountry();
            if (ds.Tables[0].Rows.Count > 0) {
                gvcountries.DataSource = ds;
                gvcountries.DataBind();
            } else {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvcountries.DataSource = ds;
                gvcountries.DataBind();
                int columncount = gvcountries.Rows[0].Cells.Count;
                gvcountries.Rows[0].Cells.Clear();
                gvcountries.Rows[0].Cells.Add(new TableCell());
                gvcountries.Rows[0].Cells[0].ColumnSpan = columncount;
                gvcountries.Rows[0].Cells[0].Text = "No Records Found";
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    protected void gvcountries_RowEditing(object sender, GridViewEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvcountries.EditIndex = e.NewEditIndex;
            LoadLookuptablesbycountry();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcountries_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            lblerror.Text = "";
            if (gvcountries.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                gvcountries.PageIndex = e.NewPageIndex;
                LoadLookuptablesbycountry();

            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcountries_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvcountries.EditIndex = -1;
            LoadLookuptablesbycountry();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcountries_RowCommand(object sender, GridViewCommandEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            if (e.CommandName.Equals("AddNew")) {
                TextBox txtnewcon = (TextBox)gvcountries.FooterRow.FindControl("txtnewcountry");
                lt.insertLookuptablesbycountry(txtnewcon.Text);

                LoadLookuptablesbycountry();
                lblerror.ForeColor = Color.Green;
                lblerror.Text = txtnewcon.Text + " Details inserted successful.";
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_COUNTRIES) violated")) {
                lblerror.Text = "Duplicate records are not allowed.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK1COUNTRIES) violated"))//SOFTWAREPLUS
              {
                lblerror.Text = "Unable to add/modify Country.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvcountries_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.Row.RowType == DataControlRowType.DataRow) {
                //getting username from particular row
                string strCOUNTRY = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "COUNTRY"));
                //identifying the control in gridview
                ImageButton lnkbtnresult = (ImageButton)e.Row.FindControl("imgbtnDelete");
                //raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtnresult != null) {
                    lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBox('" + strCOUNTRY + "')");
                }

                //User Roles Starts
                ImageButton lnkbtnedit = (ImageButton)e.Row.FindControl("imgbtnEdit");
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                }
                //else if ((Session["Role"] == "SoftwareEngineer"))
                //{
                //    lnkbtndelete.Visible = false;
                //    this.gvcountries.ShowFooter = false;
                //    lblerror.Visible = false;

                //}
                else if ((Session["Role"].ToString() == "SoftwareReadOnly") || (Session["Role"].ToString() == "SoftwareEngineer")) {
                    lnkbtnedit.Visible = false;
                    lnkbtndelete.Visible = false;
                    gvcountries.ShowFooter = false;
                    lblerror.Visible = false;
                }
                //User role Ends

            }
        } catch (Exception) {
            //errorlog = new XmlErrorLog();
            //errorlog.LogError(ex, severity.Error.ToString());
            //lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvcountries_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strCOUNTRY = gvcountries.DataKeys[e.RowIndex].Values["COUNTRY"].ToString();
            lt.deleteLookuptablesbycountry(strCOUNTRY);
            LoadLookuptablesbycountry();
            lblerror.ForeColor = Color.Red;
            lblerror.Text = strCOUNTRY + " details deleted successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_COUNTRIES) violated")) {
                lblerror.Text = "Unable to delete Country.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK1COUNTRIES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to delete Country.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvcountries_PreRender(object sender, EventArgs e) {
        if (this.gvcountries.EditIndex != -1) {
            TextBox txtconfocus = gvcountries.Rows[gvcountries.EditIndex].FindControl("txteditcountry") as TextBox;
            txtconfocus.Focus();
        }
    }
    protected void gvcountries_RowUpdating(object sender, GridViewUpdateEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strCOUNTRY = gvcountries.DataKeys[e.RowIndex].Values["COUNTRY"].ToString();
            TextBox txteditcon = (TextBox)gvcountries.Rows[e.RowIndex].FindControl("txteditcountry");
            lt.UpdateLookuptablesbycountry(strCOUNTRY, txteditcon.Text.Trim());
            gvcountries.EditIndex = -1;
            LoadLookuptablesbycountry();
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strCOUNTRY + " Details Updated successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_COUNTRIES) violated")) {
                lblerror.Text = "Unable to add/modify Country.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK1COUNTRIES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify Country.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    #endregion

    #region Model Product Categories
    private void LoadLookuptablesbymodelprodcat() {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            ds = lt.GetLookuptablesbymodelprodcat();
            if (ds.Tables[0].Rows.Count > 0) {
                gvmodelprodcat.DataSource = ds;
                gvmodelprodcat.DataBind();
            } else {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvmodelprodcat.DataSource = ds;
                gvmodelprodcat.DataBind();
                int columncount = gvmodelprodcat.Rows[0].Cells.Count;
                gvmodelprodcat.Rows[0].Cells.Clear();
                gvmodelprodcat.Rows[0].Cells.Add(new TableCell());
                gvmodelprodcat.Rows[0].Cells[0].ColumnSpan = columncount;
                gvmodelprodcat.Rows[0].Cells[0].Text = "No Records Found";
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    protected void gvmodelprodcat_PreRender(object sender, EventArgs e) {
        if (this.gvmodelprodcat.EditIndex != -1) {
            TextBox txtmodelidfocus = gvmodelprodcat.Rows[gvmodelprodcat.EditIndex].FindControl("txteditmodelid") as TextBox;
            txtmodelidfocus.Focus();
        }
    }
    protected void gvmodelprodcat_RowEditing(object sender, GridViewEditEventArgs e) {
        try {

            lblerror.Text = "";
            gvmodelprodcat.EditIndex = e.NewEditIndex;
            LoadLookuptablesbymodelprodcat();

        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvmodelprodcat_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            lblerror.Text = "";
            if (gvmodelprodcat.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                gvmodelprodcat.PageIndex = e.NewPageIndex;
                LoadLookuptablesbymodelprodcat();

            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvmodelprodcat_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvmodelprodcat.EditIndex = -1;
            LoadLookuptablesbymodelprodcat();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvmodelprodcat_RowCommand(object sender, GridViewCommandEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            if (e.CommandName.Equals("AddNew")) {
                TextBox _txtnewmodelid = (TextBox)gvmodelprodcat.FooterRow.FindControl("txtnewmodelid");
                TextBox _txtnewprodcat = (TextBox)gvmodelprodcat.FooterRow.FindControl("txtnewprodcat");
                lt.insLookuptblbymodelprodcat(_txtnewmodelid.Text, _txtnewprodcat.Text);
                LoadLookuptablesbymodelprodcat();
                lblerror.ForeColor = Color.Green;
                lblerror.Text = _txtnewmodelid.Text + " " + _txtnewprodcat.Text + " Details inserted successful.";
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            lblerror.Text = "";
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());

            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_MODEL_PRODUCT_CATEGORIES) violated"))//SERVICESPLUS
            {
                lblerror.Text = "Unable to add/modify Model ID";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESDATM) violated"))//SERVICESPLUS
               {
                lblerror.Text = "Unable to add/modify Model ID";
            } else if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvmodelprodcat_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.Row.RowType == DataControlRowType.DataRow) {
                //getting username from particular row
                string strMODEL_ID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "MODEL_ID"));
                string strPRODUCT_CATEGORY = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PRODUCT_CATEGORY"));
                //identifying the control in gridview
                ImageButton lnkbtnresult = (ImageButton)e.Row.FindControl("imgbtnDelete");
                //raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtnresult != null) {
                    lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBoxmodelprodcat('" + strMODEL_ID + "')");
                }

                //User Roles Starts
                ImageButton lnkbtnedit = (ImageButton)e.Row.FindControl("imgbtnEdit");
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                }
                //else if ((Session["Role"] == "SoftwareEngineer"))
                //{
                //    lnkbtndelete.Visible = false;
                //    this.gvmodelprodcat.ShowFooter = false;
                //    lblerror.Visible = false;

                //}
                else if ((Session["Role"].ToString() == "SoftwareReadOnly") || (Session["Role"].ToString() == "SoftwareEngineer")) {
                    lnkbtnedit.Visible = false;
                    lnkbtndelete.Visible = false;
                    gvmodelprodcat.ShowFooter = false;
                    lblerror.Visible = false;
                }
                //User role Ends

            }
        } catch (Exception) {
            //errorlog = new XmlErrorLog();
            //errorlog.LogError(ex, severity.Error.ToString());
            //lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvmodelprodcat_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strMODEL_ID = gvmodelprodcat.DataKeys[e.RowIndex].Values["MODEL_ID"].ToString();
            string strPRODUCT_CATEGORY = gvmodelprodcat.DataKeys[e.RowIndex].Values["PRODUCT_CATEGORY"].ToString();
            lt.delLookuptblbymodelprodcat(strMODEL_ID, strPRODUCT_CATEGORY);
            LoadLookuptablesbymodelprodcat();
            lblerror.ForeColor = Color.Red;
            lblerror.Text = strMODEL_ID + " details deleted successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_MODEL_PRODUCT_CATEGORIES) violated"))//SERVICESPLUS
            {
                lblerror.Text = "Unable to delete the record.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESDATM) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to delete the record";
            } else if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Unable to delete the record.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to delete the record";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvmodelprodcat_RowUpdating(object sender, GridViewUpdateEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strMODEL_ID = gvmodelprodcat.DataKeys[e.RowIndex].Values["MODEL_ID"].ToString();
            string strPRODUCT_CATEGORY = gvmodelprodcat.DataKeys[e.RowIndex].Values["PRODUCT_CATEGORY"].ToString();
            TextBox _txteditmodelid = (TextBox)gvmodelprodcat.Rows[e.RowIndex].FindControl("txteditmodelid");
            TextBox _txteditprodcat = (TextBox)gvmodelprodcat.Rows[e.RowIndex].FindControl("txteditprodcat");
            lt.UpdLookuptblbymodelprodcat(_txteditmodelid.Text, _txteditprodcat.Text, strMODEL_ID);
            gvmodelprodcat.EditIndex = -1;
            LoadLookuptablesbymodelprodcat();
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strMODEL_ID + " Details Updated successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_MODEL_PRODUCT_CATEGORIES) violated"))//SERVICESPLUS
            {
                lblerror.Text = "Unable to add/modify Model ID";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESDATM) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify Model ID";
            } else if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK1_CATEGORIES) violated")) {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2MODELPRODUCTCATEGORIESLK1C) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify Product Category.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    #endregion

    #region States
    private void LoadLookuptablesbystates() {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            ds = lt.GetLookuptablesbystates();
            if (ds.Tables[0].Rows.Count > 0) {
                gvstates.DataSource = ds;
                gvstates.DataBind();
            } else {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvstates.DataSource = ds;
                gvstates.DataBind();
                int columncount = gvstates.Rows[0].Cells.Count;
                gvstates.Rows[0].Cells.Clear();
                gvstates.Rows[0].Cells.Add(new TableCell());
                gvstates.Rows[0].Cells[0].ColumnSpan = columncount;
                gvstates.Rows[0].Cells[0].Text = "No Records Found";
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    protected void gvstates_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            lblerror.Text = "";
            if (gvstates.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                gvstates.PageIndex = e.NewPageIndex;
                LoadLookuptablesbystates();

            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvstates_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvstates.EditIndex = -1;
            LoadLookuptablesbystates();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvstates_RowCommand(object sender, GridViewCommandEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            if (e.CommandName.Equals("AddNew")) {
                TextBox _txtnewststate = (TextBox)gvstates.FooterRow.FindControl("txtnewststate");
                TextBox _txtnewstatename = (TextBox)gvstates.FooterRow.FindControl("txtnewstatename");
                lt.insLookuptblbystates(_txtnewststate.Text, _txtnewstatename.Text);

                LoadLookuptablesbystates();
                lblerror.ForeColor = Color.Green;
                lblerror.Text = _txtnewststate.Text + " " + _txtnewstatename.Text + " Details inserted successful.";
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());

            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_STATES) violated")) {
                lblerror.Text = "Unable to add/modify State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2REGIONSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify State details.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvstates_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.Row.RowType == DataControlRowType.DataRow) {
                //getting username from particular row
                string strstSTATE = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE"));
                string strSTATE_NAME = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "STATE_NAME"));
                //identifying the control in gridview
                ImageButton lnkbtnresult = (ImageButton)e.Row.FindControl("imgbtnDelete");
                //raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtnresult != null) {
                    lnkbtnresult.Attributes.Add("onclick", "javascript:return ConfirmationBoxstate('" + strstSTATE + "')");
                }
                //User Roles Starts
                ImageButton lnkbtnedit = (ImageButton)e.Row.FindControl("imgbtnEdit");
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                }
                //else if ((Session["Role"] == "SoftwareEngineer"))
                //{
                //    lnkbtndelete.Visible = false;
                //    this.gvstates.ShowFooter = false;
                //    lblerror.Visible = false;

                //}
                else if ((Session["Role"].ToString() == "SoftwareReadOnly") || (Session["Role"].ToString() == "SoftwareEngineer")) {
                    lnkbtnedit.Visible = false;
                    lnkbtndelete.Visible = false;
                    gvstates.ShowFooter = false;
                    lblerror.Visible = false;
                }
                //User role Ends
            }
        } catch (Exception) {
            //errorlog = new XmlErrorLog();
            //errorlog.LogError(ex, severity.Error.ToString());
            //lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gvstates_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strSTATE = gvstates.DataKeys[e.RowIndex].Values["STATE"].ToString();
            string strSTATE_NAME = gvstates.DataKeys[e.RowIndex].Values["STATE_NAME"].ToString();
            lt.delLookuptblbystates(strSTATE, strSTATE_NAME);
            LoadLookuptablesbystates();
            lblerror.ForeColor = Color.Red;
            lblerror.Text = strSTATE + " details deleted successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_STATES) violated")) {
                lblerror.Text = "Unable to delete State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to delete State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2REGIONSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to delete State details.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvstates_RowUpdating(object sender, GridViewUpdateEventArgs e) {
        try {
            lblerror.Text = "";
            lt = new LookupTables();
            string strSTATE = gvstates.DataKeys[e.RowIndex].Values["STATE"].ToString();
            string strSTATE_NAME = gvstates.DataKeys[e.RowIndex].Values["STATE_NAME"].ToString();
            TextBox _txteditststate = (TextBox)gvstates.Rows[e.RowIndex].FindControl("txteditststate");
            TextBox _txteditstatename = (TextBox)gvstates.Rows[e.RowIndex].FindControl("txteditstatename");
            lt.UpdLookuptblbystates(_txteditststate.Text, _txteditstatename.Text, strSTATE);
            gvstates.EditIndex = -1;
            LoadLookuptablesbystates();
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strSTATE + " Details Updated successful.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            if (ex.Message.Contains("unique constraint (SERVICESPLUS.PLK2_STATES) violated")) {
                lblerror.Text = "Unable to add/modify State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FSYSUSERSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify State details.";
            } else if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FLK2REGIONSLK2STATES) violated"))//SERVICESPLUS
              {
                lblerror.Text = "Unable to add/modify State details.";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void gvstates_PreRender(object sender, EventArgs e) {
        if (this.gvstates.EditIndex != -1) {
            TextBox txtstatefocus = gvstates.Rows[gvstates.EditIndex].FindControl("txteditststate") as TextBox;
            txtstatefocus.Focus();
        }
    }
    protected void gvstates_RowEditing(object sender, GridViewEditEventArgs e) {
        try {
            lblerror.Text = "";
            gvstates.EditIndex = e.NewEditIndex;
            LoadLookuptablesbystates();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    #endregion

    private void BindLookuptables() {
        try {
            lblerror.Text = "";
            SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
            ddlllokup.DataSource = softwareConfigMgr.SystemLookupTables();
            ddlllokup.DataBind();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void ddlllokup_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            lblerror.Text = "";
            if (ddlllokup.SelectedItem.Text == "Categories") {
                MultiView1.ActiveViewIndex = 0;
                LoadLookuptablesbycategory();
            } else if (ddlllokup.SelectedItem.Text == "Countries") {
                MultiView1.ActiveViewIndex = 1;
                LoadLookuptablesbycountry();
            } else if (ddlllokup.SelectedItem.Text == "Model Product Categories") {
                MultiView1.ActiveViewIndex = 2;
                LoadLookuptablesbymodelprodcat();
            } else if (ddlllokup.SelectedItem.Text == "States") {
                MultiView1.ActiveViewIndex = 3;
                LoadLookuptablesbystates();
            } else /*if (ddlllokup.SelectedItem.Text == "")*/ {
                MultiView1.ActiveViewIndex = -1;
            }

        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
}
