﻿using System;
using System.Web.UI;
using Sony.US.SIAMUtilities;
using System.Web.UI.HtmlControls;

/// <summary>
/// This page is no longer used. All Admin file uploads are handled by FileUpload.aspx
/// </summary>
public partial class e_sw_ReleaseFileUpload : Page {
    public bool ReleaseLog { get; set; }

    protected void Page_Load(object sender, EventArgs e) {
        if (Request.QueryString["ReleaseLog"] != null) {
            ReleaseLog = bool.Parse(Convert.ToString(Request.QueryString["ReleaseLog"]));
        }
    }

    protected void btnAddImage_Click(object sender, EventArgs e) {
        string strFileName;
        HtmlInputFile htmlFile = ImageFileUpload;
        if (htmlFile.PostedFile.ContentLength > 0) {
            string sFormat = String.Format("{0:#.##}", (float)htmlFile.PostedFile.ContentLength / 2048);
            ViewState["ImageName"] = htmlFile.PostedFile.FileName.Substring(htmlFile.PostedFile.FileName.LastIndexOf("\\") + 1);
            strFileName = ViewState["ImageName"].ToString();

            if ((strFileName.EndsWith(".pdf") == true) || (strFileName.EndsWith(".txt") == true) || (strFileName.EndsWith(".zip") == true)) {
                if (ReleaseLog) {
                    htmlFile.PostedFile.SaveAs(ConfigurationData.Environment.SoftwarePlusConfiguration.SoftwarePLUSUserManualsPath + "\\" + strFileName);
                    //Session["ReleaseLog"] = null;
                    //Session["ReleaseLog"] = strFileName;
                }
                //htmlFile.PostedFile.SaveAs(Server.MapPath(strFileName));
                lblError.Visible = false;
                System.Threading.Thread.Sleep(2000);

                //labelFileName.Text = strFileName.ToString();
                //labelFileName=" + labelFileName.Text);
                Label1.Visible = true;
                Label1.Text = "Upload successfull!";

                if (ReleaseLog)
                    ScriptManager.RegisterStartupScript(this, GetType(), "GetRefershParentPage", "GetRefershParentPage('" + strFileName + "');", true);
            } else {
                Label1.Visible = false;
                lblError.Visible = true;
                lblError.Text = "Please Check your file";
            }
        } else {
            lblError.Visible = true;
            lblError.Text = "Please Select any file to upload";
        }
    }
}
