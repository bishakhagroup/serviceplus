﻿<%@ Page Title="" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master" AutoEventWireup="true" CodeFile="LoginAlertReports.aspx.cs" Inherits="sw_LoginAlertReports" %>

<%@ Register Assembly="CrystalDecisions.Web" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>  <%--, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304--%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<%--    <asp:UpdatePanel runat="server" ID="UpdateCRReport">
        <ContentTemplate>--%>
            <br />
            <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            <div>
                <asp:Panel ID="pnlLoginAlretReport" runat="server" BorderStyle="Groove" Width="600px">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:ListBox ID="lstReportName" runat="server" Height="180px" 
                                    onselectedindexchanged="lstReportName_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text ="System Delete Log" Value="SystemDeleteLog"  />
                                    <asp:ListItem Text ="System Error Log" Value="SystemErrorLog" />
                                </asp:ListBox>
                            </td>
                            <td>
                                <table>
                                    <asp:Panel ID="pnlLog" runat="server">
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblStartDate" runat="server" Text="Start Date :"></asp:Label>
                                             <asp:RegularExpressionValidator ID="rgeddate" runat="server" ControlToValidate="txtStartDate"
                                                                        Display="Dynamic" ErrorMessage="Enter Start Date in mm/dd/yyyy format" ToolTip="Type in mm/dd/yyyy format"
                                                                        ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$"
                                                                        SetFocusOnError="True">*</asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                            <SPS:SPSTextBox ID="txtStartDate" runat="server" ></SPS:SPSTextBox>
                                           <asp:FilteredTextBoxExtender ID="txtStartDate_FilteredTextBoxExtender" runat="server"
                                                                        Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtStartDate"
                                                                        ValidChars="/">
                                                                    </asp:FilteredTextBoxExtender>
                                                                    <asp:CalendarExtender ID="txtStartDate_CalendarExtender" runat="server" CssClass="cal_Theme1"
                                                                        Enabled="True" TargetControlID="txtStartDate" PopupButtonID="imgcalStartDate">
                                                                    </asp:CalendarExtender>
                                                                    <asp:Image ID="imgcalStartDate" runat="server" ImageUrl="~/Shared/Images/Calendar.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                             <asp:Label ID="lblEndDate" runat="server" Text="End Date :"></asp:Label>
                                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEndDate"
                                                                        Display="Dynamic" ErrorMessage="Enter End Date in mm/dd/yyyy format" ToolTip="Type in mm/dd/yyyy format"
                                                                        ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$"
                                                                        SetFocusOnError="True">*</asp:RegularExpressionValidator>
                                        </td>
                                        <td>
                                             <SPS:SPSTextBox ID="txtEndDate" runat="server"></SPS:SPSTextBox>
                                             <asp:FilteredTextBoxExtender ID="txtEndDate_FilteredTextBoxExtender" runat="server"
                                                                        Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtEndDate"
                                                                        ValidChars="/">
                                                                    </asp:FilteredTextBoxExtender>
                                                                    <asp:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" CssClass="cal_Theme1"
                                                                        Enabled="True" TargetControlID="txtEndDate" PopupButtonID="imgcalEndDate">
                                                                    </asp:CalendarExtender>
                                                                    <asp:Image ID="imgcalEndDate" runat="server" ImageUrl="~/Shared/Images/Calendar.png" />
                                        </td>
                                    </tr>
                                    </asp:Panel>
                                </table>
                            </td>
                            <td align="right">
                                 <asp:ImageButton ID="btnSubmit" runat="server" 
                                           ImageUrl="~/Shared/Images/sw_submit.jpg" 
                                           ImageAlign="AbsMiddle" onclick="btnSubmit_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            <br />
<%--            <asp:Panel ID="panelUpdateProgress" runat="server">
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdateCRReport" DisplayAfter="0">
                    <ProgressTemplate>
                        <div style="position: relative; top: 30%; text-align: center;">
                        <img src="../../Shared/Images/progbar.gif"  style="vertical-align: middle" alt="Processing" />
                        Processing ...
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </asp:Panel>--%>
             <asp:ValidationSummary ID="ValidationSummary1" 
    runat="server" ShowMessageBox="True" ShowSummary="False" />
            <asp:Label ID="lblReportStatus" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            <div id="divReport">
                <asp:Panel ID="pnlCRReport" runat="server">
                    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />  <%--Obsolete: DisplayGroupTree="False"--%>
                </asp:Panel>
            </div>
<%--        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

