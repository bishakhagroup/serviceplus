﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;

/// <summary>
/// If this page starts having issues with Crystal Reports, ensure you install the 32-bit edition of the
///     Crystal Reports runtime. 64-bit edition will cause errors.
/// You also need to ensure the web directory includes a Virtual Directory to c:\inetpub\wwwroot\aspnet_client\,
///     because CR installs a bunch of prerequisites there, assuming everyone publishes their sites to that folder.
/// </summary>
public partial class sw_TestReports : Page {

    #region Properties

    Reports reports = new Reports();
    XmlErrorLog errorlog;
    ReportDocument rd = new ReportDocument();
    SoftwarePlusConfigManager configManager = new SoftwarePlusConfigManager();

    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        lblerror.Text = "";
        lblReportStatus.Text = "";

        if (Session["UserName"] == null) {
            Response.Redirect("Default.aspx?post=textreports");
        } else {
            if (!Page.IsPostBack) {
                try {
                    LoadEngineerName();
                    LoadKitPartNoDesc();
                    LoadProductManager();
                    BindData();

                    pnlchkItems.Visible = false;
                    pnlReleaseDates.Visible = false;
                    pnlEngineerName.Visible = false;
                    pnlProductManager.Visible = false;
                    pnlKitPartNo.Visible = false;
                    pnlVersion.Visible = false;
                    txtStartReleaseDate.Text = DateTime.Now.AddYears(-1).ToShortDateString();
                    txtEndReleaseDate.Text = DateTime.Now.ToShortDateString();
                } catch {

                }
            } else {
                ReportSelection();
            }
        }
    }

    private void BindData() {
        ddlReleaseType.DataSource = configManager.ReleaseType();
        ddlReleaseType.DataBind();
        ddlReleaseType.SelectedIndex = 1;

        ddlDistribution.DataSource = configManager.ReleaseDistribution();
        ddlDistribution.DataBind();
        //ddlDistribution.SelectedIndex = 1;

        ddlVisibility.DataSource = configManager.ReleaseVisibility();
        ddlVisibility.DataBind();
        //ddlVisibility.SelectedIndex = 1;
    }

    private void ReportSelection() {
        try {
            string[] con = configManager.SoftwarePlusDBConnectionString().Split(new char[] { ';' });
            string UserName, Password;
            UserName = con[1].Split(new char[] { '=' }).GetValue(1).ToString();
            Password = con[2].Split(new char[] { '=' }).GetValue(1).ToString();
            if (lstReportName.SelectedValue == "CurrentVersion") {
                rd.Load(Server.MapPath("~/Report/CurrentVersion.rpt"));
                rd.SetDataSource(CurrentVersionData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();
                if (CurrentVersionData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "EngineerModel") {
                rd.Load(Server.MapPath("~/Report/EngineerModels.rpt"));
                rd.SetDataSource(EngineerModelsData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (EngineerModelsData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "EPROMKits") {
                rd.Load(Server.MapPath("~/Report/EPROMKits.rpt"));
                rd.SetDataSource(EPROMKitsData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (EPROMKitsData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "KitAvailability") {
                rd.Load(Server.MapPath("~/Report/KitAvailability.rpt"));
                rd.SetDataSource(KitAvailabilityData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (KitAvailabilityData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "ModelsNotAssigned") {
                rd.Load(Server.MapPath("~/Report/ModelsNotAssigned.rpt"));
                rd.SetDataSource(ModelsNotAssignedData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (ModelsNotAssignedData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "ProductManagerModels") {
                rd.Load(Server.MapPath("~/Report/ProductManagerModels.rpt"));
                rd.SetDataSource(ProductManagerModelsData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (ProductManagerModelsData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "SoftwareRegistration") {
                //rd.Load(Server.MapPath("~/Report/SoftwareRegistration.rpt"));
                //rd.SetDataSource(SoftwareRegistrationData(SoftwareRegistrationReport.SoftwareRegistration.ToString()));
                //rd.OpenSubreport("SoftwareRegistrationItem.rpt").SetDataSource(SoftwareRegistrationData(SoftwareRegistrationReport.SoftwareRegistrationItem.ToString()));
                //rd.OpenSubreport("SoftwareRegistrationRelease.rpt").SetDataSource(SoftwareRegistrationData(SoftwareRegistrationReport.SoftwareRegistrationRelease.ToString()));


                rd.Load(Server.MapPath("~/Report/SoftwareRegistration.rpt"));
                rd.SetDataSource(SoftwareRegistrationData());
                rd.OpenSubreport("SoftwareRegistrationRelease.rpt").SetDataSource(SWRegistrationReleaseData());
                rd.OpenSubreport("SoftwareRegistrationItem.rpt").SetDataSource(SWRegistrationItemData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                if (SoftwareRegistrationData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }


            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void lstReportName_SelectedIndexChanged(object sender, EventArgs e) {
        if (lstReportName.SelectedValue == "CurrentVersion") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = true;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = false;
        } else if (lstReportName.SelectedValue == "EngineerModel") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = true;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = false;
        } else if (lstReportName.SelectedValue == "EPROMKits") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = false;
        } else if (lstReportName.SelectedValue == "KitAvailability") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = true;
        } else if (lstReportName.SelectedValue == "ModelsNotAssigned") {
            pnlchkItems.Visible = false;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = false;
            pnlchkItems.Visible = false;

        } else if (lstReportName.SelectedValue == "ProductManagerModels") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = true;
            pnlKitPartNo.Visible = false;
            pnlVersion.Visible = false;
        } else if (lstReportName.SelectedValue == "SoftwareRegistration") {
            pnlchkItems.Visible = true;
            pnlReleaseDates.Visible = false;
            pnlEngineerName.Visible = false;
            pnlProductManager.Visible = false;
            pnlKitPartNo.Visible = true;
            pnlVersion.Visible = false;
            pnlchkItems.Visible = false;
        }
    }

    private void LoadEngineerName() {
        try {
            ddlEngineerName.Items.Clear();
            ddlEngineerName.Items.Insert(0, new ListItem("All", "All"));
            ddlEngineerName.AppendDataBoundItems = true;
            DataSet dsEngineerName = reports.GetEngineerName();
            ddlEngineerName.DataSource = dsEngineerName.Tables[0];
            ddlEngineerName.DataTextField = dsEngineerName.Tables[0].Columns["ENGINEER"].ToString();
            ddlEngineerName.DataBind();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void LoadProductManager() {
        try {
            ddlProductManager.Items.Clear();
            ddlProductManager.Items.Insert(0, new ListItem("All", "All"));
            ddlProductManager.AppendDataBoundItems = true;
            DataSet dsProductManager = reports.GetProductManager();
            ddlProductManager.DataSource = dsProductManager.Tables[0];
            ddlProductManager.DataTextField = dsProductManager.Tables[0].Columns["PRODUCT_MANAGER"].ToString();
            ddlProductManager.DataBind();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void LoadKitPartNoDesc() {
        try {
            DataSet dsKitPartNoDesc = reports.GetKitPartNoDesc();
            DataTable dt = dsKitPartNoDesc.Tables[0];
            string KitPartNo = string.Empty;
            string Description = string.Empty;
            string KitPartNoDescription = string.Empty;
            if (dt.Rows.Count > 0) {
                for (int i = 0; i < dt.Rows.Count; i++) {
                    KitPartNo = dt.Rows[i]["KIT_PART_NUMBER"].ToString();
                    Description = dt.Rows[i]["KIT_DESCRIPTION"].ToString();
                    KitPartNoDescription = KitPartNo + "  -  " + Description; // Known issue.
                    ddlKitPartNo.Items.Add(KitPartNoDescription);
                }
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private List<clsCurrentVersion> CurrentVersionData() {
        DataSet dsRPTModelsNotAssigned = reports.RPTCurrentVersion(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue, DateTime.Parse(txtStartReleaseDate.Text), DateTime.Parse(txtEndReleaseDate.Text));
        DataTable dt = dsRPTModelsNotAssigned.Tables[0];

        List<clsCurrentVersion> lsm = new List<clsCurrentVersion>();
        foreach (DataRow r in dt.Rows) {
            clsCurrentVersion cv = new clsCurrentVersion();

            cv.ModelId = r["MODEL_ID"].ToString();
            cv.ModelVersion = r["VERSION"].ToString();
            cv.ReleaseDate = DateTime.Parse(r["RELEASE_DATE"].ToString());
            //cv.Beta = Convert.ToInt32(r["BETA"].ToString());
            //cv.Shipping = Convert.ToInt32(r["SHIPPING"].ToString());
            //cv.Upgrade = Convert.ToInt32(r["UPGRADE"].ToString());
            //cv.Special = Convert.ToInt32(r["SPECIAL"].ToString());
            cv.TechnicalReference = r["TECHNICAL_REFERENCE"].ToString();
            cv.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            cv.SupercededKit = r["SUPERCEDED_KIT"].ToString();
            cv.KitDescription = r["KIT_DESCRIPTION"].ToString();
            cv.ReleaseType = r["RELEASE_TYPE"].ToString();
            cv.DistributionType = r["DISTRIBUTION_TYPE"].ToString();
            cv.VisibilityType = r["VISIBILITY_TYPE"].ToString();
            cv.StartDate = DateTime.Parse(txtStartReleaseDate.Text);
            cv.Enddate = DateTime.Parse(txtEndReleaseDate.Text);
            lsm.Add(cv);

        }
        return lsm;

    }

    private List<clsEngineerModels> EngineerModelsData() {
        DataSet dsRPTEngineerModels = reports.RPTEngineerModels(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue, ddlEngineerName.SelectedValue);
        DataTable dt = dsRPTEngineerModels.Tables[0];

        List<clsEngineerModels> lsEM = new List<clsEngineerModels>();
        foreach (DataRow r in dt.Rows) {
            clsEngineerModels em = new clsEngineerModels();

            em.ModelId = r["MODEL_ID"].ToString();
            em.ModelVersion = r["VERSION"].ToString();
            em.ReleaseDate = DateTime.Parse(r["RELEASE_DATE"].ToString());
            //em.Beta = Convert.ToInt32(r["BETA"].ToString());
            //em.Shipping = Convert.ToInt32(r["SHIPPING"].ToString());
            //em.Upgrade = Convert.ToInt32(r["UPGRADE"].ToString());
            //em.Special = Convert.ToInt32(r["SPECIAL"].ToString());
            em.TechnicalReference = r["TECHNICAL_REFERENCE"].ToString();
            em.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            em.SupercededKit = r["SUPERCEDED_KIT"].ToString();
            em.KitDescription = r["KIT_DESCRIPTION"].ToString();
            em.ReleaseType = r["RELEASE_TYPE"].ToString();
            em.DistributionType = r["DISTRIBUTION_TYPE"].ToString();
            em.VisibilityType = r["VISIBILITY_TYPE"].ToString();
            em.engineerName = ddlEngineerName.SelectedValue;
            lsEM.Add(em);
        }
        return lsEM;

    }

    private List<clsEPROMKits> EPROMKitsData() {

        DataSet dsRPTEPROMKits = reports.RPTEPROMKits(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue);
        DataTable dt = dsRPTEPROMKits.Tables[0];

        List<clsEPROMKits> lse = new List<clsEPROMKits>();
        foreach (DataRow r in dt.Rows) {
            clsEPROMKits ep = new clsEPROMKits();

            ep.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            ep.KitDescription = r["KIT_DESCRIPTION"].ToString();
            ep.ReleaseType = r["RELEASE_TYPE"].ToString();
            ep.DistributionType = r["DISTRIBUTION_TYPE"].ToString();
            ep.VisibilityType = r["VISIBILITY_TYPE"].ToString();
            lse.Add(ep);
        }
        return lse;
    }

    private List<clsKitAvailability> KitAvailabilityData() {

        DataSet dsRPTclsKitAvailability = reports.RPTKitAvailability(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue, ddlVersion.SelectedValue, txtVersion.Text);
        DataTable dt = dsRPTclsKitAvailability.Tables[0];

        List<clsKitAvailability> lska = new List<clsKitAvailability>();
        foreach (DataRow r in dt.Rows) {
            clsKitAvailability ka = new clsKitAvailability();

            ka.ModelID = r["MODEL_ID"].ToString();
            ka.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            ka.KitVersion = r["VERSION"].ToString();
            ka.Engineer = r["ENGINEER"].ToString();
            ka.SupercededKit = r["SUPERCEDED_KIT"].ToString();
            ka.ManufacturerRemarks = r["MANUFACTURER_REMARKS"].ToString();
            ka.ReleaseType = r["RELEASE_TYPE"].ToString();
            ka.DistributionType = r["DISTRIBUTION_TYPE"].ToString();
            ka.VisibilityType = r["VISIBILITY_TYPE"].ToString();
            ka.ReleaseDate = DateTime.Parse(r["RELEASE_DATE"].ToString());
            ka.KitDiscription = r["KIT_DESCRIPTION"].ToString();
            lska.Add(ka);
        }
        return lska;

    }

    private List<ClsModelsNotAssigned> ModelsNotAssignedData() {
        DataSet dsRPTModelsNotAssigned = reports.RPTModelsNotAssigned();
        DataTable dt = dsRPTModelsNotAssigned.Tables[0];

        List<ClsModelsNotAssigned> lsm = new List<ClsModelsNotAssigned>();
        foreach (DataRow r in dt.Rows) {
            ClsModelsNotAssigned ma = new ClsModelsNotAssigned();

            ma.ModelId = r["MODEL_ID"].ToString();
            ma.ModelStatus = Convert.ToInt32(r["MODEL_STATUS"]);
            ma.ModelDescription = r["MODEL_DESCRIPTION"].ToString();
            ma.Engineer = r["ENGINEER"].ToString();
            lsm.Add(ma);
        }
        return lsm;
    }

    private List<clsProductManagerModels> ProductManagerModelsData() {
        DataSet dsRPTProductManagerModels = reports.RPTProductManagerModels(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue, ddlProductManager.SelectedValue);
        DataTable dt = dsRPTProductManagerModels.Tables[0];

        List<clsProductManagerModels> lspm = new List<clsProductManagerModels>();
        foreach (DataRow r in dt.Rows) {
            clsProductManagerModels pm = new clsProductManagerModels();

            pm.ModelID = r["MODEL_ID"].ToString();
            pm.Version = r["VERSION"].ToString();
            pm.ProductManager = r["PRODUCT_MANAGER"].ToString();
            pm.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            pm.ReleaseStatus = r["RELEASE_STATUS"].ToString();
            pm.Engineer = r["ENGINEER"].ToString();
            pm.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            pm.ModelDescription = r["MODEL_DESCRIPTION"].ToString();
            pm.ReleaseType = r["RELEASE_TYPE"].ToString();
            pm.DistributionType = r["DISTRIBUTION_TYPE"].ToString();
            pm.VisibilityType = r["VISIBILITY_TYPE"].ToString();
            lspm.Add(pm);
        }
        return lspm;
    }

    private List<clsSoftwareRegistration> SoftwareRegistrationData() {
        string[] KitNo = ddlKitPartNo.SelectedValue.Split(new char[] { '-' });
        string KitPartNo;
        KitPartNo = KitNo[0].ToString().Trim();

        DataSet dsRPTSoftwareRegistration = reports.RPTSoftwareRegistration(KitPartNo);
        DataTable dt = dsRPTSoftwareRegistration.Tables[0];

        List<clsSoftwareRegistration> lss = new List<clsSoftwareRegistration>();

        clsSoftwareRegistration sr = new clsSoftwareRegistration();

        foreach (DataRow r in dt.Rows) {

            sr.KitPartNumber = r["KIT_PART_NUMBER"].ToString();
            sr.KitDescription = r["KIT_DESCRIPTION"].ToString();
            sr.SupercedKit = r["SUPERCEDED_KIT"].ToString();
            sr.ManufacturerRemarks = r["MANUFACTURER_REMARKS"].ToString();
            sr.DownloadFile = r["DOWNLOAD_FILE"].ToString();
            sr.ReleaseNoteFiles = r["RELEASE_NOTES_FILE"].ToString();
            sr.UserManualFile = r["USER_MANUAL_FILE"].ToString();

            lss.Add(sr);
        }

        return lss;
    }

    private List<clsSWRegistrationRelease> SWRegistrationReleaseData() {
        string[] KitNo = ddlKitPartNo.SelectedValue.Split(new char[] { '-' });
        string KitPartNo;
        KitPartNo = KitNo[0].ToString().Trim();

        DataSet dsRPTSoftwareRegistrationReleases = reports.RPTSoftwareRegistrationReleases(KitPartNo);
        DataTable dtR = dsRPTSoftwareRegistrationReleases.Tables[0];



        List<clsSWRegistrationRelease> lsrr = new List<clsSWRegistrationRelease>();

        clsSWRegistrationRelease srr = new clsSWRegistrationRelease();

        foreach (DataRow r1 in dtR.Rows) {

            srr.Sub2_ModelID = r1["MODEL_ID"].ToString();
            srr.Sub2_Version = r1["VERSION"].ToString();
            srr.Sub2_ReleaseQuantity = Convert.ToInt32(r1["RELEASE_QUANTITY"]);
            srr.Sub2_TechnicalReferance = r1["TECHNICAL_REFERENCE"].ToString();
            srr.Sub2_KitPartNumber = r1["KIT_PART_NUMBER"].ToString();

            lsrr.Add(srr);
        }

        return lsrr;
    }

    private List<clsSWRegistrationItem> SWRegistrationItemData() {
        string[] KitNo = ddlKitPartNo.SelectedValue.Split(new char[] { '-' });
        string KitPartNo;
        KitPartNo = KitNo[0].ToString().Trim();


        DataSet dsRPTSoftwareRegistrationItems = reports.RPTSoftwareRegistrationItems(KitPartNo);
        DataTable dtI = dsRPTSoftwareRegistrationItems.Tables[0];


        List<clsSWRegistrationItem> lsri = new List<clsSWRegistrationItem>();

        clsSWRegistrationItem sri = new clsSWRegistrationItem();

        foreach (DataRow r2 in dtI.Rows) {

            sri.Item_KitPartNumber = r2["KIT_PART_NUMBER"].ToString();
            sri.Item_Description = r2["ITEM_DESCRIPTION"].ToString();
            sri.Item_Type = r2["ITEM_TYPE"].ToString();
            sri.Item_SubAssembly = r2["SUB_ASSEMBLY"].ToString();
            sri.Item_Location = r2["LOCATION"].ToString();
            sri.Item_Version = r2["VERSION"].ToString();
            sri.Item_Speed = r2["SPEED"].ToString();
            sri.Item_CheckSum = r2["CHECK_SUM"].ToString();
            sri.Item_Manufacturer = r2["MANUFACTURER"].ToString();
            sri.Item_LabelContents = r2["LABEL_CONTENTS"].ToString();
            sri.Item_Packaging = r2["PACKAGING"].ToString();

            lsri.Add(sri);
        }
        return lsri;
    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e) {

    }
}
