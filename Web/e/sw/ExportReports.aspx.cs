﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using SoftwarePlus.BusinessAccess;

public partial class sw_ExportReports : Page {
    #region Properties

    Reports reports = new Reports();
    XmlErrorLog errorlog;
    ReportDocument rd = new ReportDocument();
    SoftwarePlusConfigManager configManager = new SoftwarePlusConfigManager();

    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        lblerror.Text = "";
        lblReportStatus.Text = "";

        if (Session["UserName"] == null) {
            Response.Redirect("Default.aspx?post=exportreports");
        } else {
            if (!Page.IsPostBack) {
                pnlInternet.Visible = false;
                BindData();
            } else {
                ReportSelection();
            }
        }
    }

    private void ReportSelection() {
        try {
            string[] con = configManager.SoftwarePlusDBConnectionString().Split(new char[] { ';' });
            string UserName, Password;
            UserName = con[1].Split(new char[] { '=' }).GetValue(1).ToString();
            Password = con[2].Split(new char[] { '=' }).GetValue(1).ToString();
            if (lstReportName.SelectedValue == "InternetModels") {
                rd.Load(Server.MapPath("~/Report/InternetModels.rpt"));
                rd.SetDataSource(InternetModelsData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();
                if (InternetModelsData().Count == 0) {
                    CrystalReportViewer1.Visible = false;
                    lblReportStatus.Text = "No Records Found";
                } else {
                    CrystalReportViewer1.Visible = true;
                }
            } else if (lstReportName.SelectedValue == "InternetKitItems") {
                rd.Load(Server.MapPath("~/Report/IntranetKitItems.rpt"));
                rd.SetDataSource(IntranetKitItemsData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();
                if (IntranetKitItemsData().Count == 0) {
                    CrystalReportViewer1.Visible = false;
                    lblReportStatus.Text = "No Records Found";
                } else {
                    CrystalReportViewer1.Visible = true;
                }
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void lstReportName_SelectedIndexChanged(object sender, EventArgs e) {
        if (lstReportName.SelectedValue == "InternetModels") {
            pnlInternet.Visible = true;
        } else if (lstReportName.SelectedValue == "InternetKitItems") {
            pnlInternet.Visible = true;
        }
    }

    private void BindData() {
        ddlReleaseType.DataSource = configManager.ReleaseType();
        ddlReleaseType.DataBind();
        ddlReleaseType.SelectedIndex = 1;

        ddlDistribution.DataSource = configManager.ReleaseDistribution();
        ddlDistribution.DataBind();
        ddlDistribution.SelectedIndex = 1;

        ddlVisibility.DataSource = configManager.ReleaseVisibility();
        ddlVisibility.DataBind();
        ddlVisibility.SelectedIndex = 1;
    }

    private List<clsInternetModels> InternetModelsData() {
        DataSet dsRPTInternetModels = reports.RPTInternetModels(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue);
        DataTable dt = dsRPTInternetModels.Tables[0];
        List<clsInternetModels> lsim = new List<clsInternetModels>();

        foreach (DataRow r in dt.Rows) {
            clsInternetModels im = new clsInternetModels {
                ModelID = r["MODEL_ID"].ToString(),
                ModelDescription = r["MODEL_DESCRIPTION"].ToString(),
                Version = r["VERSION"].ToString(),
                ReleaseDate = DateTime.Parse(r["RELEASE_DATE"].ToString())
            };
            lsim.Add(im);
        }
        return lsim;

    }

    private List<clsIntranetKitItems> IntranetKitItemsData() {
        DataSet dsRPTIntranetKitItems = reports.RPTIntranetKitItems(ddlReleaseType.SelectedValue, ddlDistribution.SelectedValue, ddlVisibility.SelectedValue);
        DataTable dt = dsRPTIntranetKitItems.Tables[0];
        List<clsIntranetKitItems> lsiki = new List<clsIntranetKitItems>();

        foreach (DataRow r in dt.Rows) {
            clsIntranetKitItems iki = new clsIntranetKitItems {
                ModelID = r["MODEL_ID"].ToString(),
                ModelDescription = r["MODEL_DESCRIPTION"].ToString(),
                Comments = r["COMMENTS"].ToString(),
                Version = r["VERSION"].ToString(),
                ReleaseDate = DateTime.Parse(r["RELEASE_DATE"].ToString()),
                KitpartNumber = r["KIT_PART_NUMBER"].ToString(),
                KitDescription = r["KIT_DESCRIPTION"].ToString(),
                ItemDescription = r["ITEM_DESCRIPTION"].ToString(),
                Subassembly = r["SUB_ASSEMBLY"].ToString(),
                Location = r["LOCATION"].ToString(),
                KitVersion = r["KitVersion"].ToString()
            };
            lsiki.Add(iki);
        }
        return lsiki;

    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e) {

    }
}
