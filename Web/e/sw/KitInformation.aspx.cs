﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;
using System.Data;
using System.Collections;
using System.IO;
using ServicesPlusException;

public partial class sw_KitInformation_cm : Page {
    #region Members
    KitInfo kitinfo = new KitInfo();
    XmlErrorLog errorlog = new XmlErrorLog();
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e) {
        try {
            lblerror.Text = "";
            lblmsgrelnotefile.Text = "";
            lblmsgupload.Text = "";

            if (Session["UserName"] == null) {
                Response.Redirect("Default.aspx?post=Kit");
            }
            if (!Page.IsPostBack) {
                if (Request.QueryString["Identity"] != null) {
                    LoadDataFromModel(Request.QueryString["Identity"].ToString());
                } else {
                    //DisableChilds(this.Page, false);
                    //panelLabelContent.Visible = false;
                    btnnew.Visible = true;
                    btnupdate.Visible = true;
                    btnDelete.Visible = true;
                    btnedit.Visible = false;
                    btnsave.Visible = false;
                    UserAccess(string.Empty);
                    BindDefaultData();
                    FtpDownloadData();
                    FtpReleaseNotesData();
                    //FtpUserManualData();
                    BindsprceedKitPartNumber();
                    LoadData();
                    FillKitInformationGrid();
                }
                //tabcontainkitinfo.ActiveTabIndex = 0;
                //FileNameTextRel.Text = Request.QueryString["KitlabelFileName"];       
            } else
                AddUploadedFiledtoDropdown(); //It will bind the lasted file name to Dropdown if there is any upload done.
        } catch (Exception ex) {
            lblerror.Visible = true;
            lblerror.Text = Utilities.WrapExceptionforUI(ex);
        }
    }

    protected void btnedit_Click(object sender, ImageClickEventArgs e) {
        lblerror.Text = "";
        txtkitpartno.Text = "";
        txtkitpartno.Visible = false;
        ddlkitpartno.Visible = true;
        UserAccess("Cancel");
        ddlkitpartno.Visible = true;
        gvitem.Visible = true;
        LoadData();
    }

    protected void btnnew_Click(object sender, ImageClickEventArgs e) {
        DefaultData();
        gvitem.Visible = false;
        // lblerror.Text = "Record inserted Successfully";
        hdnDownloadFile.Value = string.Empty;
        hdnReleaseNote.Value = string.Empty;
    }

    protected void ddlkitpartno_SelectedIndexChanged(object sender, EventArgs e) {
        //DataSet dskitinfodata = kitinfo.GetKitInfoByKitPartNo(ddlkitpartno.SelectedItem.Text);
        // LoadKitInfoData(dskitinfodata);
        lblerror.Text = "";
        KitInfo kitInfo = new KitInfo();
        DataSet ds = kitInfo.GetKitInfoByKitPartNo(ddlkitpartno.SelectedValue);
        KitInfoData(ds);
        FillKitInformationGrid();
    }

    protected void btnsave_Click(object sender, ImageClickEventArgs e) {
        try {
            lblerror.Text = "";
            if (IsValidate()) {
                int downloadFreeCharge;
                int tracking;
                // ASleight - WHY were we setting it to negative values in the first place?!
                //if (chkDownloaFreeCharge.Checked)
                //    downloadFreeCharge = -1;
                //else
                //    downloadFreeCharge = 0;

                //if (chkTracking.Checked)
                //    tracking = -1;
                //else
                //    tracking = 0;
                Session.Remove("KitList");   // Force the other pages to refresh their Kit List so we get the new one.
                downloadFreeCharge = Convert.ToInt32(chkDownloaFreeCharge.Checked);
                tracking = Convert.ToInt32(chkTracking.Checked);
                kitinfo.AddKitInfo(txtkitpartno.Text.ToUpper(), txtkitdesc.Text, ddlsprceedkit.Text, txtmfrremarks.Text, Session["userId"].ToString(), DateTime.Now, ddldownfile.SelectedValue, ddlrelnotefile.SelectedValue, ddlRequiredKey.SelectedValue, downloadFreeCharge, tracking, txtnotes.Text);
                FillKitInformationGrid();
                lblerror.Visible = true;
                lblerror.Text = "Record inserted Successfully";
                //btnnew.Visible = true;
                //btnupdate.Visible = false;
                //btnDelete.Visible = false;
                //btnedit.Visible = true;
                //btnsave.Visible = true;
                DefaultData();
                lblerror.Visible = true;
                lblerror.Text = "Record inserted Successfully";
                gvitem.Visible = true;
                hdnDownloadFile.Value = string.Empty;
                hdnReleaseNote.Value = string.Empty;
            } else {
                //btnnew.Visible = true;
                //btnupdate.Visible = false;
                //btnDelete.Visible = false;
                //btnedit.Visible = true;
                //btnsave.Visible = true;
                UserAccess("Save");
            }
        } catch (Exception ex) {
            lblerror.Visible = true;
            lblerror.Text = ex.Message;
        }
    }

    protected void btnupdate_Click(object sender, ImageClickEventArgs e) {
        lblerror.Text = "";
        int downloadFreeCharge;
        int tracking;
        // ASleight - WHY were we setting this to negative values?!
        //if (chkDownloaFreeCharge.Checked) {
        //    downloadFreeCharge = -1;
        //} else {
        //    downloadFreeCharge = 0;
        //}
        //if (chkTracking.Checked) {
        //    tracking = -1;
        //} else {
        //    tracking = 0;
        //}
        downloadFreeCharge = Convert.ToInt32(chkDownloaFreeCharge.Checked);
        tracking = Convert.ToInt32(chkTracking.Checked);

        kitinfo.UpdateKitInfo(ddlkitpartno.SelectedValue, txtkitdesc.Text, ddlsprceedkit.Text, txtmfrremarks.Text, Session["userId"].ToString(), DateTime.Now, ddldownfile.SelectedValue, ddlrelnotefile.SelectedValue, ddlRequiredKey.SelectedValue, downloadFreeCharge, tracking, txtnotes.Text);

        FillKitInformationGrid();
        lblerror.Visible = true;
        lblerror.Text = "Record updated Successfully";
        //btnsave.Visible = false;
        UserAccess("Update");
        hdnDownloadFile.Value = string.Empty;
        hdnReleaseNote.Value = string.Empty;
    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e) {
        try {
            lblerror.Text = "";
            KitInfo kitinfo = new KitInfo();
            kitinfo.DeleteKitInfo(ddlkitpartno.SelectedValue);
            lblerror.Visible = true;
            LoadData();
            lblerror.Text = "Record Deleted Successfully";
            hdnDownloadFile.Value = string.Empty;
            hdnReleaseNote.Value = string.Empty;
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Visible = true;
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    //protected void btnUploadDownloadFile_Click1(object sender, EventArgs e) {
    //    try {
    //        Response.Redirect("FileUpload.aspx?DownloadFilep=true");
    //    } catch (IOException ex) {
    //        lblerror.Visible = true;
    //        errorlog = new XmlErrorLog();
    //        errorlog.LogError(ex, severity.Error.ToString());
    //        if (ex.Message.Contains("Auth fail")) {
    //            lblerror.Text = "SFTP Credentials are invalid to upload the file.";
    //        } else {
    //            lblerror.Text = "The following exception occurs, " + ex.Message;
    //        }
    //    }
    //}

    //protected void btnUploadReleaseNote_Click(object sender, EventArgs e) {
    //    try {
    //        Response.Redirect("FileUpload.aspx?ReleaseNotes=true");
    //    } catch (IOException ex) {
    //        lblerror.Visible = true;
    //        errorlog = new XmlErrorLog();
    //        errorlog.LogError(ex, severity.Error.ToString());
    //        if (ex.Message.Contains("Auth fail")) {
    //            lblerror.Text = "SFTP Credentials are invalid to upload the file.";
    //        } else {
    //            lblerror.Text = "The following exception occurs, " + ex.Message;
    //        }
    //    } catch (Exception ex) {
    //        lblerror.Visible = true;
    //        errorlog = new XmlErrorLog();
    //        errorlog.LogError(ex, severity.Error.ToString());
    //        if (ex.Message.Contains("Auth fail")) {
    //            lblerror.Text = "SFTP Credentials are invalid to upload the file.";
    //        } else {
    //            lblerror.Text = "The following exception occurs, " + ex.Message;
    //        }
    //    }
    //}

    protected void gvitem_PreRender(object sender, EventArgs e) {
        if (this.gvitem.EditIndex != -1) {
            if (Session["CopyRightEdit"] != null) {
                TextBox b = gvitem.Rows[gvitem.EditIndex].FindControl("ddlLabelContents") as TextBox;
                Button btnLabelContent = gvitem.Rows[gvitem.EditIndex].FindControl("btnCopyRightEdit") as Button;
                ((TextBox)gvitem.Rows[gvitem.EditIndex].FindControl("ddlLabelContents")).Text = b.Text + btnLabelContent.Text;
            } else if (Session["RSymbol"] != null) {
                TextBox b = gvitem.Rows[gvitem.EditIndex].FindControl("ddlLabelContents") as TextBox;
                Button btnLabelContent = gvitem.Rows[gvitem.EditIndex].FindControl("btnRSymbo") as Button;
                ((TextBox)gvitem.Rows[gvitem.EditIndex].FindControl("ddlLabelContents")).Text = b.Text + btnLabelContent.Text;
            }
        }
        Session.Remove("CopyRightEdit");
        Session.Remove("RSymbol");
    }

    protected void gvitem_RowDataBound(object sender, GridViewRowEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.Row.RowType == DataControlRowType.DataRow) {
                //getting username from particular row
                // string strITEM_ID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ITEM_ID"));
                //identifying the control in gridview
                ImageButton lnkbtndelete = (ImageButton)e.Row.FindControl("imgbtnDelete");
                ImageButton lnkbtnedit = (ImageButton)e.Row.FindControl("imgbtnEdit");
                //raising javascript confirmationbox whenver user clicks on link button
                if (lnkbtndelete != null) {
                    lnkbtndelete.Attributes.Add("onclick", "javascript:return ConfirmationBox()");
                }
                //User Roles Starts
                if (Session["Role"].ToString() == "SoftwareAdmin") {
                } else if ((Session["Role"].ToString() == "SoftwareEngineer")) {
                    lnkbtndelete.Visible = false;
                    gvitem.ShowFooter = false;
                    lblerror.Visible = false;
                } else if ((Session["Role"].ToString() == "SoftwareReadOnly")) {
                    lnkbtnedit.Visible = false;
                    lnkbtndelete.Visible = false;
                    gvitem.ShowFooter = false;
                    lblerror.Visible = false;
                }
            }
        } catch (Exception) {
            //errorlog = new XmlErrorLog();
            //errorlog.LogError(ex, severity.Error.ToString());
            //lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            lblerror.Text = "";
            if (gvitem.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;
            } else {
                gvitem.PageIndex = e.NewPageIndex;
                FillKitInformationGrid();
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_RowCommand(object sender, GridViewCommandEventArgs e) {
        try {
            lblerror.Text = "";
            if (e.CommandName.Equals("AddNew")) {
                //txtNewItemId
                //  TextBox txtItemId = (TextBox)gvitem.FooterRow.FindControl("txtNewItemId");
                TextBox txtItemDescription = (TextBox)gvitem.FooterRow.FindControl("txtNewItemDescription");
                DropDownList ddlitemType = (DropDownList)gvitem.FooterRow.FindControl("ddlNewItemType");
                TextBox txtSubAssembly = (TextBox)gvitem.FooterRow.FindControl("txtNewSubAssembly");
                TextBox txtLocation = (TextBox)gvitem.FooterRow.FindControl("txtNewLocation");
                TextBox txtVersion = (TextBox)gvitem.FooterRow.FindControl("txtNewVersion");
                TextBox txtSpeed = (TextBox)gvitem.FooterRow.FindControl("txtNewSpeed");
                TextBox txtCheckSum = (TextBox)gvitem.FooterRow.FindControl("txtNewCheckSum");
                TextBox txtManufacturer = (TextBox)gvitem.FooterRow.FindControl("txtNewManufacturer");
                TextBox txtFileName = (TextBox)gvitem.FooterRow.FindControl("txtNewFileName");
                TextBox txtFileFormat = (TextBox)gvitem.FooterRow.FindControl("txtNewFileFormat");
                Button btnCopyRight = (Button)gvitem.FooterRow.FindControl("Button1");
                TextBox ddlLabelContents = (TextBox)gvitem.FooterRow.FindControl("ddlNewLabelContents");
                DropDownList ddlPckaging = (DropDownList)gvitem.FooterRow.FindControl("ddlNewPckaging");
                KitsItems items = new KitsItems();
                if (txtkitpartno.Text.Length > 0) {
                    items.AddKitItems(txtkitpartno.Text.ToUpper(), txtItemDescription.Text, ddlitemType.SelectedValue, txtSubAssembly.Text, txtLocation.Text, txtVersion.Text, txtSpeed.Text, txtCheckSum.Text, txtManufacturer.Text, txtFileFormat.Text, txtFileName.Text, ddlLabelContents.Text, ddlPckaging.SelectedValue);
                } else {
                    items.AddKitItems(ddlkitpartno.SelectedValue, txtItemDescription.Text, ddlitemType.SelectedValue, txtSubAssembly.Text, txtLocation.Text, txtVersion.Text, txtSpeed.Text, txtCheckSum.Text, txtManufacturer.Text, txtFileFormat.Text, txtFileName.Text, ddlLabelContents.Text, ddlPckaging.SelectedValue);
                }
                FillKitInformationGrid();
                lblerror.Text = "Record Inserted Sucessfully";
            } else if (e.CommandName.Equals("CopyRight")) {
                TextBox ddlLabelContents = (TextBox)gvitem.FooterRow.FindControl("ddlNewLabelContents");
                Button btnCopyRight = (Button)gvitem.FooterRow.FindControl("btnNewCopyRight");
                ((TextBox)gvitem.FooterRow.FindControl("ddlNewLabelContents")).Text = ddlLabelContents.Text + btnCopyRight.Text;
            } else if (e.CommandName.Equals("RSymbol")) {
                TextBox ddlLabelContents = (TextBox)gvitem.FooterRow.FindControl("ddlNewLabelContents");
                Button btnCopyRight = (Button)gvitem.FooterRow.FindControl("btnNewRSymbo");
                ((TextBox)gvitem.FooterRow.FindControl("ddlNewLabelContents")).Text = ddlLabelContents.Text + btnCopyRight.Text;
            } else if (e.CommandName.Equals("CopyRightEdit")) {
                Session.Add("CopyRightEdit", "true");
            } else if (e.CommandName.Equals("RSymbolEdit")) {
                Session.Add("RSymbol", "true");
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_RowDeleting(object sender, GridViewDeleteEventArgs e) {
        try {
            //TextBox txtItemID = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtItemId");
            string itemid = gvitem.DataKeys[e.RowIndex].Values["ITEM_ID"].ToString();
            string strKIT_PART_NUMBER = gvitem.DataKeys[e.RowIndex].Values["KIT_PART_NUMBER"].ToString();
            KitsItems kitsItems = new KitsItems();
            kitsItems.DeleteKitItems(strKIT_PART_NUMBER, itemid);
            FillKitInformationGrid();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_RowEditing(object sender, GridViewEditEventArgs e) {
        lblerror.Text = "";
        try {
            gvitem.EditIndex = e.NewEditIndex;
            FillKitInformationGrid();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_RowUpdating(object sender, GridViewUpdateEventArgs e) {
        try {
            lblerror.Text = "";
            TextBox txtItemID = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtItemId");
            TextBox txtItemDescription = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtItemDescription");
            DropDownList ddlitemType = (DropDownList)gvitem.Rows[e.RowIndex].FindControl("ddlItemType");
            TextBox txtSubAssembly = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtSubAssembly");
            TextBox txtLocation = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtLocation");
            TextBox txtVersion = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtVersion");
            TextBox txtSpeed = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtSpeed");
            TextBox txtCheckSum = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtCheckSum");
            TextBox txtManufacturer = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtManufacturer");
            TextBox txtFileName = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtFileName");
            TextBox txtFileFormat = (TextBox)gvitem.Rows[e.RowIndex].FindControl("txtFileFormat");
            TextBox ddlLabelContents = (TextBox)gvitem.Rows[e.RowIndex].FindControl("ddlLabelContents");
            DropDownList ddlPckaging = (DropDownList)gvitem.Rows[e.RowIndex].FindControl("ddlPckaging");

            KitsItems items = new KitsItems();
            if (txtkitpartno.Text.Length > 0) {
                items.UpdateKitItems(txtkitpartno.Text.ToUpper(), txtItemDescription.Text, ddlitemType.SelectedValue, txtSubAssembly.Text, txtLocation.Text, txtVersion.Text, txtSpeed.Text, txtCheckSum.Text, txtManufacturer.Text, txtFileFormat.Text, txtFileName.Text, ddlLabelContents.Text, ddlPckaging.SelectedValue);
            } else {
                items.UpdateKitItems(ddlkitpartno.SelectedValue, txtItemDescription.Text, ddlitemType.SelectedValue, txtSubAssembly.Text, txtLocation.Text, txtVersion.Text, txtSpeed.Text, txtCheckSum.Text, txtManufacturer.Text, txtFileFormat.Text, txtFileName.Text, ddlLabelContents.Text, ddlPckaging.SelectedValue);
            }
            gvitem.EditIndex = -1;
            FillKitInformationGrid();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void gvitem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) {
        try {
            gvitem.EditIndex = -1;
            FillKitInformationGrid();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    #endregion

    private void AddUploadedFiledtoDropdown() {
        if (!string.IsNullOrEmpty(hdnDownloadFile.Value)) {
            string strDownloadFile = hdnDownloadFile.Value;
            if (ddldownfile.Items.FindByValue(strDownloadFile) != null) {
                ddldownfile.SelectedValue = strDownloadFile;
            } else {
                ddldownfile.Items.Add(strDownloadFile);
                ddldownfile.SelectedValue = strDownloadFile;
            }
        }
        if (!string.IsNullOrEmpty(hdnReleaseNote.Value)) {
            string releaseFile;
            string docext = Path.GetExtension(hdnReleaseNote.Value).ToLower();
            if (txtkitpartno.Text.Length > 0) {
                releaseFile = "Release-Notes-" + txtkitpartno.Text + docext;
            } else {
                releaseFile = "Release-Notes-" + ddlkitpartno.SelectedValue + docext;
            }
            if (ddlrelnotefile.Items.FindByValue(releaseFile) != null) {
                ddlrelnotefile.SelectedValue = releaseFile;
            } else {
                ddlrelnotefile.Items.Add(releaseFile);
                ddlrelnotefile.SelectedValue = releaseFile;
            }
        }
    }

    private void UserAccess(string button) {
        if (Session["Role"].ToString() == "SoftwareAdmin") {
            if (button == "New") {
                btnnew.Visible = false;
                btnsave.Visible = true;
                btnupdate.Visible = false;
                btnDelete.Visible = false;
                btnedit.Visible = true;
            } else if (button == "Save") {
                btnnew.Visible = true;
                btnsave.Visible = true;
                btnupdate.Visible = false;
                btnDelete.Visible = false;
                btnedit.Visible = true;
            } else if (button == "Cancel") {
                btnnew.Visible = true;
                btnsave.Visible = false;
                btnupdate.Visible = true;
                btnDelete.Visible = true;
            } else if (button == "Update") {
                btnsave.Visible = false;
            } else if (button == "Delete") {

            }
        } else if (Session["Role"].ToString() == "SoftwareEngineer") {
            btnnew.Visible = false;
            btnsave.Visible = false;
            btnupdate.Visible = true;
            btnDelete.Visible = false;
            btnedit.Visible = true;
        } else if (Session["Role"].ToString() == "SoftwareReadOnly") {
            btnnew.Visible = false;
            btnsave.Visible = false;
            btnupdate.Visible = false;
            btnDelete.Visible = false;
            btnedit.Visible = false;
            btnUploadDownloadFile.Visible = false;
            btnUploadReleaseNote.Visible = false;
        }
    }

    private void LoadData() {
        ddlkitpartno.Visible = true;
        txtkitpartno.Text = "";
        txtkitpartno.Visible = false;
        DataSet dsKitPartNumbers = kitinfo.GetKitPartNumbers();
        ddlkitpartno.DataSource = dsKitPartNumbers.Tables[0];
        ddlkitpartno.DataTextField = dsKitPartNumbers.Tables[0].Columns["KIT_PART_NUMBER"].ToString();
        ddlkitpartno.DataBind();

        KitInfo kitInfo = new KitInfo();
        DataSet ds = kitInfo.GetKitInfoByKitPartNo(ddlkitpartno.SelectedValue);
        KitInfoData(ds);
    }

    private void LoadDataFromModel(string kitPartNumber) {
        btnnew.Visible = true;
        btnupdate.Visible = true;
        btnDelete.Visible = true;
        btnedit.Visible = false;
        btnsave.Visible = false;
        // UserAccess(string.Empty);
        BindDefaultData();
        FtpDownloadData();
        FtpReleaseNotesData();
        //FtpUserManualData();
        BindsprceedKitPartNumber();

        // LoadData();

        ddlkitpartno.Visible = true;
        txtkitpartno.Text = "";
        txtkitpartno.Visible = false;
        DataSet dsKitPartNumbers = kitinfo.GetKitPartNumbers();
        ddlkitpartno.DataSource = dsKitPartNumbers.Tables[0];
        ddlkitpartno.DataTextField = dsKitPartNumbers.Tables[0].Columns["KIT_PART_NUMBER"].ToString();
        ddlkitpartno.DataBind();
        ddlkitpartno.SelectedValue = kitPartNumber;
        KitInfo kitInfo = new KitInfo();
        DataSet ds = kitInfo.GetKitInfoByKitPartNo(kitPartNumber);
        KitInfoData(ds);
    }

    private void KitInfoData(DataSet ds) {
        try {
            if (ds != null) {
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows) {
                    ddlsprceedkit.SelectedValue = row["SUPERCEDED_KIT"].ToString();
                    txtkitdesc.Text = row["KIT_DESCRIPTION"].ToString();
                    txtmfrremarks.Text = row["MANUFACTURER_REMARKS"].ToString();
                    if (row["REQUIRED_KEY"] != null) {
                        ddlRequiredKey.SelectedValue = row["REQUIRED_KEY"].ToString();
                    } else {
                        ddlRequiredKey.SelectedValue = "";
                    }

                    ddldownfile.SelectedValue = row["DOWNLOAD_FILE"].ToString();
                    ddlrelnotefile.SelectedValue = row["RELEASE_NOTES_FILE"].ToString();
                    txtnotes.Text = row["NOTES"].ToString();
                    txtdatecreated.Text = row["DATE_CREATED"].ToString();
                    txtcreatedby.Text = row["CREATED_BY"].ToString();
                    txtupdatedby.Text = row["UPDATED_BY"].ToString();
                    txtlastupdated.Text = row["LAST_UPDATED"].ToString();
                    if (row["FREE_OF_CHARGE"].ToString() == "0") {
                        chkDownloaFreeCharge.Checked = false;
                    } else {
                        chkDownloaFreeCharge.Checked = true;
                    }
                    if (row["TRACKING"].ToString() == "0") {
                        chkTracking.Checked = false;
                    } else {
                        chkTracking.Checked = true;
                    }
                }
                FillKitInformationGrid();
            }
        } catch (Exception ex) {
            FillKitInformationGrid();
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void BindDefaultData() {
        ArrayList keyList = new ArrayList();
        KitsItems items = new KitsItems();
        DataSet dsRequiredKeys = items.GetRequiredKeys();
        keyList.Add("");
        if (dsRequiredKeys != null) {
            foreach (DataRow r in dsRequiredKeys.Tables[0].Rows) {
                keyList.Add(r["REQUIRED_KEY"].ToString());
            }
        }
        ddlRequiredKey.DataSource = keyList;
        //   ddlRequiredKey.DataTextField = dsRequiredKeys.Tables[0].Columns["REQUIRED_KEY"].ToString();
        ddlRequiredKey.DataBind();
    }

    private void FtpDownloadData() {
        ArrayList downloadFilesList = new ArrayList();
        downloadFilesList.Add("");
        DataSet ds = kitinfo.GetFTPDownloadFile();
        if (ds != null) {
            foreach (DataRow row in ds.Tables[0].Rows) {
                if (!String.IsNullOrWhiteSpace(row["DOWNLOAD_FILE"]?.ToString())) {
                    downloadFilesList.Add(row["DOWNLOAD_FILE"].ToString());
                }
            }
            downloadFilesList.Sort();
        }
        ddldownfile.DataSource = downloadFilesList;
        ddldownfile.DataBind();
    }

    private void FtpReleaseNotesData() {
        ArrayList releaseNoteFilesList = new ArrayList();
        releaseNoteFilesList.Add("");
        DataSet ds = kitinfo.GetFTPReleaseFile();
        if (ds != null) {
            foreach (DataRow row in ds.Tables[0].Rows) {
                if (!String.IsNullOrWhiteSpace(row["RELEASE_NOTES_FILE"]?.ToString())) {
                    releaseNoteFilesList.Add(row["RELEASE_NOTES_FILE"].ToString());
                }
            }
            releaseNoteFilesList.Sort();
        }
        ddlrelnotefile.DataSource = releaseNoteFilesList;
        ddlrelnotefile.DataBind();
    }

    private void FillKitInformationGrid() {
        KitsItems kitItems = new KitsItems();
        DataSet ds;
        //if (txtkitpartno.Text.Length > 0)
        //{
        //    ds = kitItems.GetKitItemsInfoByKitPartNo(txtkitpartno.Text);
        //}
        //else
        //{
        ds = kitItems.GetKitItemsInfoByKitPartNo(ddlkitpartno.SelectedValue);
        // }
        // DataTable dtCustomer = customer.Fetch();
        if (ds.Tables[0].Rows.Count > 0) {
            gvitem.DataSource = ds;
            gvitem.DataBind();
        } else {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            gvitem.DataSource = ds;
            gvitem.DataBind();
            int TotalColumns = gvitem.Rows[0].Cells.Count;
            gvitem.Rows[0].Cells.Clear();
            gvitem.Rows[0].Cells.Add(new TableCell());
            gvitem.Rows[0].Cells[0].ColumnSpan = TotalColumns;
            gvitem.Rows[0].Cells[0].Text = "No Record Found";
        }
    }

    private void FillKitInformationGrid(string newrecord) {
        KitsItems kitItems = new KitsItems();
        DataSet ds;
        if (txtkitpartno.Text.Length > 0) {
            ds = kitItems.GetKitItemsInfoByKitPartNo(txtkitpartno.Text.ToUpper());
        } else {
            ds = kitItems.GetKitItemsInfoByKitPartNo(newrecord);
        }
        // DataTable dtCustomer = customer.Fetch();
        if (ds.Tables[0].Rows.Count > 0) {
            gvitem.DataSource = ds;
            gvitem.DataBind();
        } else {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            gvitem.DataSource = ds;
            gvitem.DataBind();
            int TotalColumns = gvitem.Rows[0].Cells.Count;
            gvitem.Rows[0].Cells.Clear();
            gvitem.Rows[0].Cells.Add(new TableCell());
            gvitem.Rows[0].Cells[0].ColumnSpan = TotalColumns;
            gvitem.Rows[0].Cells[0].Text = "No Record Found";
        }
    }

    private void BindsprceedKitPartNumber() {
        ArrayList ls = new ArrayList();
        DataSet dsKitPartNumbers = kitinfo.GetKitPartNumbers();
        if (dsKitPartNumbers != null) {
            DataTable dt = dsKitPartNumbers.Tables[0];
            ls.Add("");
            foreach (DataRow row in dt.Rows) {
                ls.Add(row["KIT_PART_NUMBER"].ToString());
            }
        }
        ddlsprceedkit.DataSource = ls;
        ddlsprceedkit.DataBind();
    }

    private void BindKitPartNumber() {
        DataSet dsKitPartNumbers = kitinfo.GetKitPartNumbers();
        ddlkitpartno.DataSource = dsKitPartNumbers.Tables[0];
        ddlkitpartno.DataTextField = dsKitPartNumbers.Tables[0].Columns["KIT_PART_NUMBER"].ToString();
        ddlkitpartno.DataBind();
    }

    private void BindDownLoadData() {

    }

    private void DefaultData() {
        //DisableChilds(this.Page, true);
        lblerror.Text = "";
        ClearFieldsAndAddCreatedUser();
        lblerror.Visible = false;
        txtkitpartno.Visible = true;
        ddlkitpartno.Visible = false;
        ddlkitpartno.Visible = false;
        BindDefaultData();
        UserAccess("New");
        chkDownloaFreeCharge.Checked = false;
        chkTracking.Checked = false;
        FillKitInformationGrid(string.Empty);
        ddlRequiredKey.SelectedValue = "No Key Required";
    }

    private void LoadKitInfoData(DataSet ds) {
        if (ds != null) {
            BindDownLoadData();
            DataTable dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows) {
                ddlsprceedkit.SelectedValue = row["SUPERCEDED_KIT"].ToString();
                txtkitdesc.Text = row["KIT_DESCRIPTION"].ToString();
                txtmfrremarks.Text = row["MANUFACTURER_REMARKS"].ToString();
            }
        }
    }

    private void ClearFieldsAndAddCreatedUser() {
        txtcreatedby.Text = Session["userId"].ToString();
        txtdatecreated.Text = DateTime.Now.ToString();
        txtkitdesc.Text = "";
        txtkitpartno.Text = "";
        txtlastupdated.Text = "";
        txtmfrremarks.Text = "";
        txtnotes.Text = "";
        txtupdatedby.Text = "";
        ddlsprceedkit.SelectedValue = "";
        ddldownfile.SelectedValue = "";
        ddlrelnotefile.SelectedValue = "";
        txtnotes.Text = "";
    }

    private bool IsValidate() {
        if (txtkitpartno.Text.Length > 0) {
            if (ddlsprceedkit.Items.Count > 0) {
                if (ddlsprceedkit.Items.FindByText(txtkitpartno.Text) != null) {
                    lblerror.Visible = true;
                    lblerror.Text = "KitPart number already exists.";
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            lblerror.Visible = true;
            lblerror.Text = "Please enter the KitPart number.";
            return false;
        }
        //if (ddlsprceedkit.Items.Count > 0)
        //{
        //   string value =  ddlsprceedkit.Items.FindByText(txtkitpartno.Text).Value;
        //    lblerror.Text = "KitPart number already exists.";
        //    return false;
        //}
        //else
        //{
        //    return true;
        //}
    }
}

