﻿using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;

public partial class sw_ModelInformation_cm : Page {
    XmlErrorLog errorlog;

    #region events
    protected void Page_Load(object sender, EventArgs e) {
        if (Session["UserName"] == null) {
            Response.Redirect("Default.aspx?post=Model");
        } else {
            lblerror.Text = "";

            if (!Page.IsPostBack) {
                try {
                    // btDeleteChildModels.Visible = false;
                    pageloaddata();
                    TabContainer1.ActiveTabIndex = 0;
                    UserAccess();
                } catch (Exception ex) {
                    lblerror.ForeColor = Color.Red;
                    errorlog = new XmlErrorLog();
                    errorlog.LogError(ex, severity.Error.ToString());
                    lblerror.Text = "The following exception occurs, " + ex.Message;
                }
            }
        }
    }

    private void UserAccess() {
        var role = Session["Role"]?.ToString() ?? "";
        //if (role == "SoftwareAdmin") {

        //}
        //else if (role == "SoftwareEngineer")
        //{
        //    btnNewModel.Visible = false;
        //    btnDeleteChildInfo.Visible = false;
        //    btnDeleteModel.Visible = false;
        //}
        //else
        if (role == "SoftwareReadOnly" || role == "SoftwareEngineer") {
            btnNewModel.Visible = false;
            btnupdatemodel.Visible = false;
            btncancelModel.Visible = false;
            btnSaveModel.Visible = false;
            btnDeleteModel.Visible = false;

            btnchupdate.Visible = false;
            btnDeleteChildInfo.Visible = false;
            btnchcancel.Visible = false;
            SaveChildModel.Visible = false;
            btDeleteChildModels.Visible = false;
        }
    }

    private void childtoparentdata() {
        txtmodelid.Text = txtchmodelid.Text.ToUpper();
        txtmodeldesc.Text = txtchmodeldesc.Text;
        ddlmainassembly.SelectedValue = ddlchmainassembly.SelectedValue;
        txtupgradeest.Text = txtchupgest.Text;
        ddlprversreq.SelectedValue = ddlchprversreq.SelectedValue;
        ddlmodelstatus.SelectedValue = ddlchmodelstatus.SelectedValue;
        ddltype.SelectedValue = ddlchtype.SelectedValue;
        ddltracking.SelectedValue = ddlchtracking.SelectedValue;
        ddlsource.SelectedValue = ddlchsource.SelectedValue;
        //ddlmodelId.SelectedValue = ddlchmodelid.SelectedValue;
    }

    private void parenttochilddata() {
        txtchmodelid.Text = txtmodelid.Text.ToUpper();
        txtchmodeldesc.Text = txtmodeldesc.Text;
        ddlchmainassembly.SelectedValue = ddlmainassembly.SelectedValue;
        txtchupgest.Text = txtupgradeest.Text;
        ddlchprversreq.SelectedValue = ddlprversreq.SelectedValue;
        ddlchmodelstatus.SelectedValue = ddlmodelstatus.SelectedValue;
        ddlchtype.SelectedValue = ddltype.SelectedValue;
        ddlchtracking.SelectedValue = ddltracking.SelectedValue;
        ddlchsource.SelectedValue = ddlsource.SelectedValue;
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e) {
        try {
            if (TabContainer1.ActiveTabIndex == 0) {
                txtchildModelSearch.Text = "";

                if (txtchmodelid.Visible && txtchmodelid.Text.Length > 0) {
                    childtoparentdata();
                } else {
                    btnSaveModel.Visible = false;
                    LoadDefaultData();
                    LoadModelIDs();
                    LoadModelIDData();
                    childtoparentdata();
                    ddlmodelId.SelectedValue = ddlchmodelid.SelectedValue;
                    BindReleaseInfobyModelId();
                    BindKitInfoByModelNo();
                    BindReleaseKitID();
                    //TabContainer1.ActiveTabIndex = 0;
                    btnupdatemodel.Visible = true;
                    btnNewModel.Visible = true;
                    btncancelModel.Visible = false;
                    btnDeleteModel.Visible = true;
                    UserAccess();
                }
            } else {
                Session.Remove("Search");
                Session.Remove("SearchBasedOnModelID");
                Session.Remove("SaveChildModel");

                if (txtmodelid.Visible) {
                    txtchmodelid.Visible = true;
                    ddlchmodelid.Visible = false;
                    txtchmodelid.Text = txtmodelid.Text.ToUpper();
                    txtchmodeldesc.Text = txtmodeldesc.Text;
                    ddlchmainassembly.SelectedValue = ddlmainassembly.SelectedValue;
                    txtchupgest.Text = txtupgradeest.Text;
                    ddlchprversreq.SelectedValue = ddlprversreq.SelectedValue;
                    ddlchmodelstatus.SelectedValue = ddlmodelstatus.SelectedValue;
                    ddlchtype.SelectedValue = ddltype.SelectedValue;
                    ddlchtracking.SelectedValue = ddltracking.SelectedValue;
                    ddlchsource.SelectedValue = ddlsource.SelectedValue;
                    txtchcomments.Text = "";
                    txtchnotes.Text = "";
                    txtchcreatedby.Text = Session["userId"].ToString();
                    txtchdatecreated.Text = System.DateTime.Now.ToString();
                    txtchupdatedby.Text = "";
                    txtchlastupdated.Text = "";

                    gviewchmodel.Visible = false;
                    SaveChildModel.Visible = true;
                    btnchcancel.Visible = true;
                    btnchupdate.Visible = false;
                    btnDeleteChildInfo.Visible = false;
                    UserAccess();
                } else {
                    LoadChildModelIDs();
                    ddlchmodelid.SelectedValue = ddlmodelId.SelectedValue;
                    ddlchmodelid.Visible = true;
                    txtchmodelid.Visible = false;
                    DataSet dsmodelInfo = model.GetModelInfo(ddlmodelId.SelectedValue);
                    LoadChildModelData(dsmodelInfo);
                    BindChildModelDataByid();
                    SaveChildModel.Visible = false;
                    btnchupdate.Visible = true;
                    btnchcancel.Visible = false;
                    btnDeleteChildInfo.Visible = true;
                    parenttochilddata();
                    UserAccess();
                }
                if (gviewchmodel.Rows.Count > 0) {
                    btDeleteChildModels.Visible = true;
                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Visible = true;
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void btnDeleteChildInfo_Click(object sender, ImageClickEventArgs e) {
        try {
            foreach (GridViewRow gvRow in gviewchmodel.Rows) {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chkSelect");
                //if (chkSel.Checked == true)
                //{
                Label lblChildModel = (Label)gvRow.FindControl("lblChildControl");

                if (txtchmodelid.Text.Length > 0) {
                    model.DeleteChildModel(txtchmodelid.Text.ToUpper(), lblChildModel.Text.ToUpper());
                    model.DeleteModel(txtchmodelid.Text.ToUpper());
                } else {
                    model.DeleteChildModel(ddlchmodelid.Text, lblChildModel.Text.ToUpper());
                    model.DeleteModel(ddlchmodelid.Text);
                }
                //}
            }
            //BindChildModelDataByid();
            childpageloaddata();
            UserAccess();
            lblerror.Visible = true;
            lblerror.Text = "Model is deleted.";
            lblerror.ForeColor = Color.Red;
            if (gviewchmodel.Rows.Count > 0) {
                btDeleteChildModels.Visible = true;
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());

            if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FDATRELEASESKITSDATRELEASES) violated - child record found")) {
                lblerror.Text = "Unable to delete the selected MODEL ID-child record found";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e) {

    }

    protected void gviewchmodel_PageIndexChanging(Object sender, GridViewPageEventArgs e) {
        try {
            SaveChildModelDataOnPageIndexChange();
            if (gviewchmodel.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                if ((Session["Search"]) != null) {

                    if (txtchmodelid.Text.Length > 0) {
                        //if ((Session["Search"]) != null)
                        //{
                        //    ds = model.GetChildModels(txtchmodelid.Text);
                        //}
                        if ((Session["SearchBasedOnModelID"]) != null) {
                            ds = model.GetChildModels(txtchildModelSearch.Text);
                        } else if ((Session["SaveChildModel"]) != null) {
                            ds = model.GetChildModels(txtchildModelSearch.Text);
                        } else {
                            ds = model.GetChildModels(string.Empty);
                        }

                    } else {

                        if ((Session["SearchBasedOnModelID"]) != null) {
                            ds = model.GetChildModels(txtchildModelSearch.Text);
                        } else if ((Session["SaveChildModel"]) != null) {
                            ds = model.GetChildModels(ddlchmodelid.SelectedValue);
                        } else {
                            ds = model.GetChildModels(string.Empty);
                        }
                    }
                } else {
                    ds = model.GetChildModelsByModelID(ddlchmodelid.SelectedValue);
                }
                gviewchmodel.PageIndex = e.NewPageIndex;
                gviewchmodel.DataSource = ds;
                gviewchmodel.DataBind();

                //  Session.Remove("SaveChildModel");
                // Session.Remove("SearchBasedOnModelID");
            }

        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void gviewmodelinfo_PageIndexChanging(Object sender, GridViewPageEventArgs e) {
        try {
            if (gviewmodelinfo.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                ds = model.GetReleaseInfoForModel(ddlmodelId.SelectedValue);
                pnlmodelinfo.Visible = true;
                gviewmodelinfo.PageIndex = e.NewPageIndex;
                gviewmodelinfo.DataSource = ds;
                gviewmodelinfo.DataBind();

            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void ddlmodelId_SelectedIndexChanged(object sender, EventArgs e) {
        try {

            DataSet dsmodelInfo = model.GetModelInfo(ddlmodelId.SelectedValue);
            LoadModelData(dsmodelInfo);
            BindReleaseInfobyModelId();
            BindKitInfoByModelNo();
            BindReleaseKitID();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void ddlchmodelid_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            DataSet dsmodelInfo = model.GetModelInfo(ddlchmodelid.SelectedValue);
            LoadChildModelData(dsmodelInfo);
            BindChildModelDataByid();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void btnNewModel_Click(object sender, ImageClickEventArgs e) {
        try {
            Session.Remove("IsAdded");
            btnSaveModel.Visible = true;
            txtmodelid.Visible = true;
            ddlmodelId.Visible = false;
            btncancelModel.Visible = true;
            btnDeleteModel.Visible = false;
            btnNewModel.Visible = false;
            btnupdatemodel.Visible = false;
            ddltype.SelectedIndex = 0;
            ddlsource.SelectedIndex = 0;
            SetControlsDefaultValues();
            gviewkitinfo.Visible = false;
            pnlgviewkitinfo.Height = 0;
            pnlmodelinfo.Height = 0;
            txtupgradeest.Text = "0";
            lblkitrelease.Visible = false;
            ddlrelkits.Visible = false;
            UserAccess();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void UpdateModelInfo() {
        try {
            int mainassembly;
            int priorversion;
            int tracking;
            int modelStatus;

            if (ddlmodelstatus.Text == "Active") {
                modelStatus = -1;
            } else {
                modelStatus = 0;
            }
            if (ddlmainassembly.Text == "Yes") {
                mainassembly = -1;
            } else {
                mainassembly = 0;
            }
            if (ddlprversreq.Text == "Yes") {
                priorversion = -1;
            } else {
                priorversion = 0;
            }
            if (ddltracking.Text == "Yes") {
                tracking = -1;
            } else {
                tracking = 0;
            }
            model.UpdateParentModel(ddlmodelId.SelectedValue.ToString(), modelStatus, ddltype.Text, txtmodeldesc.Text, mainassembly, priorversion, tracking, ddlsource.Text, Convert.ToInt32(txtupgradeest.Text));
            string strupdmodelid = ddlmodelId.SelectedValue;
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strupdmodelid + " details are updated.";
            //pageload data method starts
            btnSaveModel.Visible = false;
            // LoadDefaultData();
            LoadModelIDs();
            ddlmodelId.SelectedValue = strupdmodelid;
            LoadModelIDData();
            BindReleaseInfobyModelId();
            BindKitInfoByModelNo();
            BindReleaseKitID();
            //TabContainer1.ActiveTabIndex = 0;
            btnupdatemodel.Visible = true;
            btnNewModel.Visible = true;
            btncancelModel.Visible = false;
            btnDeleteModel.Visible = true;
            //pageload data method ends
            UserAccess();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }


    protected void btnSaveModel_Click(object sender, ImageClickEventArgs e) {
        try {
            DataSet dscheckmodelid = model.GetModelInfo(txtmodelid.Text.ToUpper());
            if (dscheckmodelid.Tables[0].Rows.Count > 0) {
                lblerror.ForeColor = Color.Red;
                lblerror.Text = "Model ID already exists.";
            } else {


                int mainassembly;
                int priorversion;
                int tracking;
                int modelStatus;
                if (ddlmodelstatus.Text == "Active") {
                    modelStatus = -1;
                } else {
                    modelStatus = 0;
                }
                if (ddlmainassembly.Text == "Yes") {
                    mainassembly = -1;
                } else {
                    mainassembly = 0;
                }
                if (ddlprversreq.Text == "Yes") {
                    priorversion = -1;
                } else {
                    priorversion = 0;
                }
                if (ddltracking.Text == "Yes") {
                    tracking = -1;
                } else {
                    tracking = 0;
                }
                model.AddModel(txtmodelid.Text.ToUpper(), modelStatus, ddltype.Text, txtmodeldesc.Text, mainassembly, priorversion, tracking, ddlsource.Text, txtupgradeest.Text, string.Empty, string.Empty, Session["userId"].ToString(), DateTime.Now);
                Session.Remove("ModelList");   // Clear out the in-memory Model List so we force a refresh from the database.
                // btnNewModel.Visible = true;
                //  btnDeleteModel.Visible = true;
                //btnSaveModel.Visible = false;
                Session.Add("IsAdded", true);
                lblerror.ForeColor = Color.Green;
                lblerror.Text = "Record Inserted Sucessful.";
                //pageload data method starts
                btnSaveModel.Visible = false;
                //LoadDefaultData();
                LoadModelIDs();
                ddlmodelId.SelectedValue = txtmodelid.Text.ToUpper();
                LoadModelIDData();
                BindReleaseInfobyModelId();
                BindKitInfoByModelNo();
                BindReleaseKitID();
                //TabContainer1.ActiveTabIndex = 0;
                btnupdatemodel.Visible = true;
                btnNewModel.Visible = true;
                btncancelModel.Visible = false;
                btnDeleteModel.Visible = true;
                //pageload data method ends
                UserAccess();

            }

        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void btnChildModelSearch_Click(object sender, EventArgs e) {
        try {
            btDeleteChildModels.Visible = false;
            gviewchmodel.Visible = true;
            if (txtchildModelSearch.Text.Length > 0) {
                gviewchmodel.Visible = true;
                ds = model.GetChildModels(txtchildModelSearch.Text);
                if (ds.Tables.Count > 0) {

                    gviewchmodel.DataSource = ds;
                    gviewchmodel.AllowPaging = true;
                    gviewchmodel.DataBind();

                }

                Session.Add("SearchBasedOnModelID", "SearchBasedOnModelID");
                // Session.Remove("Search");
                Session.Remove("SaveChildModel");
            } else {
                Session.Add("Search", "true");
                Session.Remove("SearchBasedOnModelID");
                Session.Remove("SaveChildModel");
                BindChildModelData();
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void btnDeleteModel_Click(object sender, ImageClickEventArgs e) {
        try {
            model.DeleteModel(ddlmodelId.SelectedValue);
            LoadModelIDs();
            LoadModelIDData();
            lblerror.ForeColor = Color.Red;
            lblerror.Visible = true;
            lblerror.Text = "Model is deleted.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());

            if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FDATRELEASESKITSDATRELEASES) violated - child record found")) {
                lblerror.Text = "Unable to delete the selected MODEL ID-child record found";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void SaveChildModel_Click(object sender, ImageClickEventArgs e) {
        try {


            //reqchmodelid.Visible = true;
            DataSet dscheckmodelid = model.GetModelInfo(txtchmodelid.Text.ToUpper());
            if (dscheckmodelid.Tables[0].Rows.Count > 0) {
                lblerror.ForeColor = Color.Red;
                lblerror.Text = "Model ID already exists.";
            } else {
                // Session.Remove("Search");
                int mainassembly;
                int priorversion;
                int tracking;
                int modelStatus;
                if (ddlchmodelstatus.Text == "Active") {
                    modelStatus = -1;
                } else {
                    modelStatus = 0;
                }
                if (ddlchmainassembly.Text == "Yes") {
                    mainassembly = -1;
                } else {
                    mainassembly = 0;
                }
                if (ddlchprversreq.Text == "Yes") {
                    priorversion = -1;
                } else {
                    priorversion = 0;
                }
                if (ddlchtracking.Text == "Yes") {
                    tracking = -1;
                } else {
                    tracking = 0;
                }
                //childtoparentdata();
                if (txtchmodelid.Text.Length > 0) {
                    if (Session["IsAdded"] != null) {
                        //model.UpdateNewModel(txtmodelid.Text, modelStatus, ddlchtype.Text, txtchmodeldesc.Text, mainassembly, priorversion, tracking, ddlchsource.Text, Convert.ToInt32(txtchupgest.Text), txtchcomments.Text, txtchnotes.Text, txtchcreatedby.Text, DateTime.Now);
                        model.UpdateNewModel(txtchmodelid.Text.ToUpper(), modelStatus, ddlchtype.Text, txtchmodeldesc.Text, mainassembly, priorversion, tracking, ddlchsource.Text, Convert.ToInt32(txtchupgest.Text), txtchcomments.Text, txtchnotes.Text, Session["userId"].ToString(), DateTime.Now);
                    } else {
                        //model.AddModel(txtmodelid.Text, modelStatus, ddltype.Text, txtmodeldesc.Text, mainassembly, priorversion, tracking, ddlsource.Text, txtupgradeest.Text, string.Empty, string.Empty, Session["userId"].ToString(), DateTime.Now, string.Empty, DateTime.Now);
                        model.AddModel(txtchmodelid.Text.ToUpper(), modelStatus, ddlchtype.Text, txtchmodeldesc.Text, mainassembly, priorversion, tracking, ddlchsource.Text, txtchupgest.Text, txtchcomments.Text, txtchnotes.Text, Session["userId"].ToString(), DateTime.Now);
                        Session.Add("IsAdded", true);
                    }
                    lblerror.ForeColor = Color.Green;
                    lblerror.Text = "Record Saved Sucessfully";
                }
                SaveChildModelData();
                // BindChildModelDataByid();
                LoadChildModelIDs();
                ddlchmodelid.Visible = true;
                txtchmodelid.Visible = false;
                btnchupdate.Visible = true;
                btnDeleteChildInfo.Visible = true;
                SaveChildModel.Visible = false;
                btnchcancel.Visible = false;
                ddlchmodelid.SelectedValue = txtchmodelid.Text.ToUpper();
                DataSet dsmodelInfo = model.GetModelInfo(ddlchmodelid.SelectedValue);
                LoadChildModelData(dsmodelInfo);
                BindChildModelDataByid();
                UserAccess();
                if (gviewchmodel.Rows.Count > 0) {
                    btDeleteChildModels.Visible = true;
                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    #endregion

    #region PrivateMembers

    /// <summary>
    /// 
    /// </summary>
    private void BindChildModelDataByid() {
        try {
            gviewchmodel.Visible = true;
            if (txtmodelid.Text.Length > 0) {
                ds = model.GetChildModelsByModelID(txtmodelid.Text.ToUpper());
            } else {

                ds = model.GetChildModelsByModelID(ddlchmodelid.SelectedValue);
            }

            if (ds.Tables.Count > 0) {
                gviewchmodel.DataSource = ds;
                gviewchmodel.AllowPaging = true;
                gviewchmodel.DataBind();

            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void BindReleaseKitID() {
        DataSet dskitIds = model.GetReleaseKitInfoByModelNo(ddlmodelId.SelectedValue);
        DataTable dtkitids = dskitIds.Tables[0];
        string id = string.Empty;
        string name = string.Empty;
        string strreldate = string.Empty;
        string strkitpartno = string.Empty;
        string newName = string.Empty;
        ddlrelkits.Items.Clear();
        if (dtkitids.Rows.Count > 0) {
            lblkitrelease.Visible = true;
            ddlrelkits.Visible = true;
            for (int i = 0; i < dtkitids.Rows.Count; i++) {
                id = dtkitids.Rows[i]["MODEL_ID"].ToString();
                name = dtkitids.Rows[i]["VERSION"].ToString();
                strreldate = dtkitids.Rows[i]["RELEASE_DATE"].ToString();
                strreldate = Convert.ToDateTime(strreldate).ToShortDateString();
                strkitpartno = dtkitids.Rows[i]["KIT_PART_NUMBER"].ToString();
                newName = id + "-" + name + "-" + strreldate + "-" + strkitpartno;
                // ddlchreleaseid.Items.Add(newName);
                // myDic.Add(" V" + name,id);
                ddlrelkits.Items.Add(newName);
            }
            ddlrelkits.Items.Add("All Kits Related to This Model");

        } else {
            lblkitrelease.Visible = false;
            ddlrelkits.Visible = false;
        }
    }
    private void BindKitInfoByModelNo() {
        try {
            ds = model.GetKitInfoByModelNo(ddlmodelId.SelectedValue);

            if (ds.Tables[0].Rows.Count > 0) {
                pnlgviewkitinfo.Visible = true;
                pnlgviewkitinfo.Height = 100;
                gviewkitinfo.Visible = true;
                gviewkitinfo.DataSource = ds;
                gviewkitinfo.AllowPaging = true;
                gviewkitinfo.DataBind();
            } else {
                pnlgviewkitinfo.Height = 100;
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gviewkitinfo.Visible = true;
                gviewkitinfo.DataSource = ds;
                gviewkitinfo.DataBind();
                int columncount = gviewkitinfo.Rows[0].Cells.Count;
                gviewkitinfo.Rows[0].Cells.Clear();
                gviewkitinfo.Rows[0].Cells.Add(new TableCell());
                gviewkitinfo.Rows[0].Cells[0].ColumnSpan = columncount;
                gviewkitinfo.Rows[0].Cells[0].Text = "No Kit Information Details found for the selected Model ID.";
                gviewkitinfo.Rows[0].Cells[0].ForeColor = Color.Red;
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void BindChildModelDataByModelId(string modelId) {
        try {
            ds = model.GetChildModelsByModelID(modelId);

            if (ds.Tables.Count > 0) {
                gviewchmodel.DataSource = ds;
                gviewchmodel.AllowPaging = true;
                gviewchmodel.DataBind();
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void BindReleaseInfobyModelId() {
        try {


            ds = model.GetReleaseInfoForModel(ddlmodelId.SelectedValue);

            if (ds.Tables[0].Rows.Count > 0) {
                pnlmodelinfo.Visible = true;
                pnlmodelinfo.Height = 100;
                gviewmodelinfo.Visible = true;
                gviewmodelinfo.DataSource = ds;
                gviewmodelinfo.AllowPaging = true;
                gviewmodelinfo.DataBind();
            } else {
                pnlmodelinfo.Height = 100;
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gviewmodelinfo.Visible = true;
                gviewmodelinfo.DataSource = ds;
                gviewmodelinfo.DataBind();
                int columncount = gviewmodelinfo.Rows[0].Cells.Count;
                gviewmodelinfo.Rows[0].Cells.Clear();
                gviewmodelinfo.Rows[0].Cells.Add(new TableCell());
                gviewmodelinfo.Rows[0].Cells[0].ColumnSpan = columncount;
                gviewmodelinfo.Rows[0].Cells[0].Text = "No ReleaseLog Data found for the selected Model ID.";
                gviewmodelinfo.Rows[0].Cells[0].ForeColor = Color.Red;
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    private void LoadModelData(DataSet ds) {
        try {
            if (ds != null) {
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows) {
                    // foreach (DataColumn col in dt.Columns)
                    //Console.WriteLine(row["MODEL_STATUS"]);
                    if (row["MODEL_STATUS"].ToString().Contains("0")) {
                        ddlmodelstatus.SelectedValue = "Inactive";
                    } else {
                        ddlmodelstatus.SelectedValue = "Active";
                    }
                    ddltype.SelectedValue = row["MODEL_TYPE"].ToString();
                    txtmodeldesc.Text = row["MODEL_DESCRIPTION"].ToString();
                    if (row["MAIN_ASSEMBLY"].ToString().Contains("0")) {
                        ddlmainassembly.SelectedValue = "No";
                    } else {
                        ddlmainassembly.SelectedValue = "Yes";
                    }
                    if (row["PRIOR_VERSION_REQUIRED"].ToString().Contains("0")) {
                        ddlprversreq.SelectedValue = "No";
                    } else {
                        ddlprversreq.SelectedValue = "Yes";
                    }
                    if (row["CUSTOMER_TRACKING"].ToString().Contains("0")) {
                        ddltracking.SelectedValue = "No";
                    } else {
                        ddltracking.SelectedValue = "Yes";
                    }

                    if (row["PRODUCTION_SOURCE"].ToString() == "") {
                        ddlsource.SelectedIndex = 0;
                    } else {
                        ddlsource.SelectedValue = row["PRODUCTION_SOURCE"].ToString();
                    }
                    txtupgradeest.Text = row["UPGRADE_ESTIMATE"].ToString();
                }

            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void LoadChildModelData(DataSet ds) {
        try {
            if (ds != null) {
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows) {
                    // foreach (DataColumn col in dt.Columns)
                    //Console.WriteLine(row["MODEL_STATUS"]);
                    if (row["MODEL_STATUS"].ToString().Contains("0")) {
                        ddlchmodelstatus.SelectedValue = "Inactive";
                    } else {
                        ddlchmodelstatus.SelectedValue = "Active";
                    }

                    if (row["MAIN_ASSEMBLY"].ToString().Contains("0")) {
                        ddlchmainassembly.SelectedValue = "No";
                    } else {
                        ddlchmainassembly.SelectedValue = "Yes";
                    }
                    if (row["PRIOR_VERSION_REQUIRED"].ToString().Contains("0")) {
                        ddlchprversreq.SelectedValue = "No";
                    } else {
                        ddlchprversreq.SelectedValue = "Yes";
                    }
                    if (row["CUSTOMER_TRACKING"].ToString().Contains("0")) {
                        ddlchtracking.SelectedValue = "No";
                    } else {
                        ddlchtracking.SelectedValue = "Yes";
                    }
                    if (row["MODEL_TYPE"].ToString().Contains(" ")) {
                        ddlchtype.SelectedIndex = 0;
                    } else {
                        ddlchtype.SelectedValue = row["MODEL_TYPE"].ToString();
                    }
                    txtchmodeldesc.Text = row["MODEL_DESCRIPTION"].ToString();

                    if (row["PRODUCTION_SOURCE"].ToString() == "") {
                        ddlchsource.SelectedIndex = 0;
                    } else {
                        ddlchsource.SelectedValue = row["PRODUCTION_SOURCE"].ToString();
                    }
                    txtchupgest.Text = row["UPGRADE_ESTIMATE"].ToString();
                    txtchcomments.Text = row["COMMENTS"].ToString();
                    txtchnotes.Text = row["NOTES"].ToString();
                    txtchcreatedby.Text = row["CREATED_BY"].ToString();
                    txtchdatecreated.Text = row["DATE_CREATED"].ToString();
                    txtchupdatedby.Text = row["UPDATED_BY"].ToString();
                    txtchlastupdated.Text = row["LAST_UPDATED"].ToString();


                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void SetControlsDefaultValues() {
        txtmodelid.Text = "";
        ddlmodelstatus.SelectedValue = "Active";
        txtmodeldesc.Text = "";
        ddlmainassembly.SelectedValue = "Yes";
        ddlprversreq.SelectedValue = "No";
        ddltracking.SelectedValue = "Yes";
        gviewmodelinfo.Visible = false;
    }

    private void LoadModelIDs() {
        try {
            txtmodelid.Visible = false;
            ddlmodelId.Visible = true;
            DataSet dsmodelIds = model.GetModelIds();
            ddlmodelId.DataSource = dsmodelIds.Tables[0];
            ddlmodelId.DataTextField = dsmodelIds.Tables[0].Columns["MODEL_ID"].ToString();
            ddlmodelId.DataBind();

            DataSet dsmodelInfo = model.GetModelInfo(ddlmodelId.SelectedValue);
            LoadModelData(dsmodelInfo);
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void LoadModelIDData() {
        try {
            DataSet dsmodelInfo = model.GetModelInfo(ddlmodelId.SelectedValue);
            LoadModelData(dsmodelInfo);
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void LoadChildModelIDs() {

        try {
            DataSet dsmodelIds = model.GetModelIds();

            ddlchmodelid.DataSource = dsmodelIds.Tables[0];
            ddlchmodelid.DataTextField = dsmodelIds.Tables[0].Columns["MODEL_ID"].ToString();
            ddlchmodelid.DataBind();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    private void LoadDefaultData() {
        try {
            SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
            ddltype.DataSource = softwareConfigMgr.ModelType();
            ddltype.DataBind();
            ddlsource.DataSource = softwareConfigMgr.ModelSource();
            ddlsource.DataBind();
            ddlmodelstatus.DataSource = softwareConfigMgr.ModelStatus();
            ddlmodelstatus.DataBind();

            //Child Model

            //ddlchtype.DataSource = softwareConfigMgr.ModelType();
            //ddlchtype.DataBind();
            //ddlchsource.DataSource = softwareConfigMgr.ModelSource();
            //ddlchsource.DataBind();
            //ddlchmodelstatus.DataSource = softwareConfigMgr.ModelStatus();
            //ddlchmodelstatus.DataBind();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    private void LoadChildDefaultData() {
        try {
            SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
            //ddltype.DataSource = softwareConfigMgr.ModelType();
            //ddltype.DataBind();
            //ddlsource.DataSource = softwareConfigMgr.ModelSource();
            //ddlsource.DataBind();
            //ddlmodelstatus.DataSource = softwareConfigMgr.ModelStatus();
            //ddlmodelstatus.DataBind();

            //Child Model

            ddlchtype.DataSource = softwareConfigMgr.ModelType();
            ddlchtype.DataBind();
            ddlchsource.DataSource = softwareConfigMgr.ModelSource();
            ddlchsource.DataBind();
            ddlchmodelstatus.DataSource = softwareConfigMgr.ModelStatus();
            ddlchmodelstatus.DataBind();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    private void BindChildModelData() {
        try {
            ds = model.GetChildModels(string.Empty);

            if (ds.Tables.Count > 0) {
                gviewchmodel.DataSource = ds;
                gviewchmodel.AllowPaging = true;
                gviewchmodel.DataBind();
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void SaveChildModelData() {
        Session.Add("SaveChildModel", "true");
        Session.Remove("Search");
        Session.Remove("SearchBasedOnModelID");
        DataSet childInfoDataSet = model.GetChildModelInfo(ddlchmodelid.SelectedValue);
        ArrayList childList = new ArrayList();
        if (childInfoDataSet != null) {

            DataTable dt = childInfoDataSet.Tables[0];
            foreach (DataRow row in dt.Rows) {
                childList.Add(row["CHILD_MODEL"].ToString());
            }
        }

        try {
            foreach (GridViewRow gvRow in gviewchmodel.Rows) {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chkSelect");
                if (chkSel.Checked == true) {
                    Label lblChildModel = (Label)gvRow.FindControl("lblChildControl");

                    if (txtchmodelid.Text.Length > 0) {
                        if (!childList.Contains(lblChildModel.Text)) {
                            model.AddChildModel(txtchmodelid.Text.ToUpper(), lblChildModel.Text.ToUpper());
                        }
                    } else {
                        if (!childList.Contains(lblChildModel.Text)) {
                            model.AddChildModel(ddlchmodelid.Text, lblChildModel.Text.ToUpper());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    private void SaveChildModelDataOnPageIndexChange() {

        DataSet childInfoDataSet = model.GetChildModelInfo(ddlchmodelid.SelectedValue);
        ArrayList childList = new ArrayList();
        if (childInfoDataSet != null) {

            DataTable dt = childInfoDataSet.Tables[0];
            foreach (DataRow row in dt.Rows) {
                childList.Add(row["CHILD_MODEL"].ToString());
            }
        }

        try {
            foreach (GridViewRow gvRow in gviewchmodel.Rows) {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chkSelect");
                if (chkSel.Checked == true) {
                    Label lblChildModel = (Label)gvRow.FindControl("lblChildControl");

                    if (txtchmodelid.Text.Length > 0) {
                        if (!childList.Contains(lblChildModel.Text)) {
                            model.AddChildModel(txtchmodelid.Text.ToUpper(), lblChildModel.Text.ToUpper());
                        }
                    } else {
                        if (!childList.Contains(lblChildModel.Text)) {
                            model.AddChildModel(ddlchmodelid.Text, lblChildModel.Text);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    #endregion

    #region Properties
    Model model = new Model();
    DataSet ds;
    #endregion

    protected void gviewkitinfo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            if (gviewkitinfo.EditIndex != -1) {
                // Use the Cancel property to cancel the paging operation.
                e.Cancel = true;

                // Display an error message.
                int newPageNumber = e.NewPageIndex + 1;

            } else {
                ds = model.GetKitInfoByModelNo(ddlmodelId.SelectedValue);
                gviewkitinfo.PageIndex = e.NewPageIndex;
                pnlgviewkitinfo.Visible = true;
                gviewkitinfo.DataSource = ds;
                gviewkitinfo.DataBind();

            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    private void childpageloaddata() {

        // ddlchmodelid.SelectedValue = ddlmodelId.SelectedValue;

        ddlchmodelid.Visible = true;
        txtchmodelid.Visible = false;
        LoadChildModelIDs();
        DataSet dsmodelInfo = model.GetModelInfo(ddlchmodelid.SelectedValue);
        LoadChildModelData(dsmodelInfo);
        BindChildModelDataByid();
        btnchupdate.Visible = true;
        btnDeleteChildInfo.Visible = true;
        SaveChildModel.Visible = false;
        btnchcancel.Visible = false;
    }
    private void pageloaddata() {
        btnSaveModel.Visible = false;
        LoadDefaultData();
        LoadChildDefaultData();
        LoadModelIDs();
        LoadModelIDData();
        BindReleaseInfobyModelId();
        BindKitInfoByModelNo();
        BindReleaseKitID();
        //TabContainer1.ActiveTabIndex = 0;
        btnupdatemodel.Visible = true;
        btnNewModel.Visible = true;
        btncancelModel.Visible = false;
        btnDeleteModel.Visible = true;
    }
    protected void btncancelModel_Click(object sender, ImageClickEventArgs e) {
        try {

            pageloaddata();
            UserAccess();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void btnupdatemodel_Click(object sender, ImageClickEventArgs e) {
        try {
            UpdateModelInfo();
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }
    protected void btnchupdate_Click(object sender, ImageClickEventArgs e) {
        try {
            //  btDeleteChildModels.Visible = true;
            int mainassembly;
            int priorversion;
            int tracking;
            int modelStatus;
            if (ddlchmodelstatus.Text == "Active") {
                modelStatus = -1;
            } else {
                modelStatus = 0;
            }
            if (ddlchmainassembly.Text == "Yes") {
                mainassembly = -1;
            } else {
                mainassembly = 0;
            }
            if (ddlchprversreq.Text == "Yes") {
                priorversion = -1;
            } else {
                priorversion = 0;
            }
            if (ddlchtracking.Text == "Yes") {
                tracking = -1;
            } else {
                tracking = 0;
            }
            model.UpdateModel(ddlchmodelid.SelectedValue.ToString(), modelStatus, ddlchtype.Text, txtchmodeldesc.Text, mainassembly, priorversion, tracking, ddlchsource.Text, Convert.ToInt32(txtchupgest.Text), txtchcomments.Text, txtchnotes.Text, Session["userId"].ToString(), DateTime.Now);
            string strupdchmodelid = ddlchmodelid.SelectedValue;
            lblerror.ForeColor = Color.Green;
            lblerror.Text = strupdchmodelid + " details are updated.";

            SaveChildModelData();
            // BindChildModelDataByid();
            LoadChildModelIDs();
            ddlchmodelid.Visible = true;
            txtchmodelid.Visible = false;
            btnchupdate.Visible = true;
            btnDeleteChildInfo.Visible = true;
            SaveChildModel.Visible = false;
            btnchcancel.Visible = false;
            ddlchmodelid.SelectedValue = strupdchmodelid;
            DataSet dsmodelInfo = model.GetModelInfo(ddlchmodelid.SelectedValue);
            LoadChildModelData(dsmodelInfo);
            BindChildModelDataByid();
            UserAccess();
            //reqchmodelid.Visible = false;
            if (gviewchmodel.Rows.Count > 0) {
                btDeleteChildModels.Visible = true;
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    protected void btnchcancel_Click(object sender, ImageClickEventArgs e) {
        childpageloaddata();
        UserAccess();
        if (gviewchmodel.Rows.Count > 0) {
            btDeleteChildModels.Visible = true;
        }
    }
    protected void btDeleteChildModels_Click(object sender, EventArgs e) {
        try {
            btDeleteChildModels.Visible = true;
            foreach (GridViewRow gvRow in gviewchmodel.Rows) {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chkSelect");
                if (chkSel.Checked == true) {
                    Label lblChildModel = (Label)gvRow.FindControl("lblChildControl");

                    if (txtchmodelid.Text.Length > 0) {
                        model.DeleteChildModel(txtchmodelid.Text.ToUpper(), lblChildModel.Text.ToUpper());

                    } else {
                        model.DeleteChildModel(ddlchmodelid.Text, lblChildModel.Text.ToUpper());

                    }
                }
            }
            BindChildModelDataByid();
            // childpageloaddata();
            // BindChildModelDataByid();
            UserAccess();
            lblerror.Visible = true;
            lblerror.ForeColor = Color.Red;
            lblerror.Text = "Child Model Data is deleted.";
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());

            if (ex.Message.Contains("integrity constraint (SERVICESPLUS.FDATRELEASESKITSDATRELEASES) violated - child record found")) {
                lblerror.Text = "Unable to delete the selected MODEL ID-child record found";
            } else {
                lblerror.Text = "The following exception occurs, " + ex.Message;
            }
        }
    }
    protected void ddlrelkits_SelectedIndexChanged(object sender, EventArgs e) {
        try {
            if (ddlrelkits.SelectedValue == "All Kits Related to This Model") {
                BindKitInfoByModelNo();
            } else {
                string strmodelid, strversion, strreldate, strkitpartno, strmodelversion;
                strmodelversion = ddlrelkits.SelectedValue;
                string[] strtemp = strmodelversion.Split('-');
                strmodelid = strtemp[0].Trim();
                strversion = strtemp[1].Trim();
                strreldate = strtemp[2].Trim();
                strkitpartno = strtemp[3].Trim();

                ds = model.GetRelKitInfoByKitPartNo(strkitpartno);

                if (ds.Tables[0].Rows.Count > 0) {
                    pnlgviewkitinfo.Visible = true;
                    pnlgviewkitinfo.Height = 100;
                    gviewkitinfo.Visible = true;
                    gviewkitinfo.DataSource = ds;
                    gviewkitinfo.AllowPaging = true;
                    gviewkitinfo.DataBind();
                } else {
                    pnlgviewkitinfo.Height = 100;
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    gviewkitinfo.Visible = true;
                    gviewkitinfo.DataSource = ds;
                    gviewkitinfo.DataBind();
                    int columncount = gviewkitinfo.Rows[0].Cells.Count;
                    gviewkitinfo.Rows[0].Cells.Clear();
                    gviewkitinfo.Rows[0].Cells.Add(new TableCell());
                    gviewkitinfo.Rows[0].Cells[0].ColumnSpan = columncount;
                    gviewkitinfo.Rows[0].Cells[0].Text = "No Kit Information Details found for the selected Model ID.";
                    gviewkitinfo.Rows[0].Cells[0].ForeColor = Color.Red;
                }
            }
        } catch (Exception ex) {
            lblerror.ForeColor = Color.Red;
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
}
