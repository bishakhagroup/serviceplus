﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SoftwarePlus.BusinessAccess;
using Sony.US.siam;
using Sony.US.ServicesPLUS.Data;
using System.Xml;
using System.Collections;
using System.Security.Principal;
using Sony.US.SIAMUtilities;
public partial class sw_Default : System.Web.UI.Page
{
	private string userId = string.Empty;
	//SoftwarePlusConfigManager configMgr;
	//UsersDataManager usermgr;
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			UserRoleSecurityManager ursm = new UserRoleSecurityManager();
			if (ConfigurationData.GeneralSettings.AdminLocalAccess.AuthenticateAdminLocalFlag.ToString() == "Y")
				userId = ConfigurationData.GeneralSettings.AdminLocalAccess.AuthenticateAdminLocalGlobalID.ToString();
			else
			{
				if (HttpContext.Current.Request.ServerVariables["HTTP_IDP_UID"] != null)
					userId = HttpContext.Current.Request.ServerVariables["HTTP_IDP_UID"];
				else
					userId = Environment.UserName;
			}
			Session.Add("userId", userId);

			ProcessUser();
			/*Code added by Sneha for bug fix but checkin is not required */
			System.Web.UI.HtmlControls.HtmlMeta tag = new System.Web.UI.HtmlControls.HtmlMeta();
			tag.HttpEquiv = "X-UA-Compatible";
			tag.Content = "IE=5";
			Header.Controls.Add(tag);

		}
	}
	private void RedirectPages()
	{
		//Private Sub CloseAndRedirect()
		//        If Not Request.QueryString("post") Is Nothing Then
		//            Select Case Request.QueryString("post")
		//                Case "om"
		//                    Response.Redirect("sony-operation-manual.aspx", False)
		//                    HttpContext.Current.ApplicationInstance.CompleteRequest()
		//                Case "qo"

		if (Request.QueryString["post"] != null)
		{
			switch (Request.QueryString["post"].ToString())
			{
				case "Model":
					Response.Redirect("ModelInformation.aspx", false);
					break;
				case "Kit":
					Response.Redirect("KitInformation.aspx", false);
					break;
				case "ReleaseLog":
					Response.Redirect("ReleaseLog.aspx", false);
					break;
				case "SysMaster":
					Response.Redirect("SystemMaster.aspx", false);
					break;
				case "SysConfig":
					Response.Redirect("SystemConfiguration.aspx", false);
					break;
				case "textreports":
					Response.Redirect("TestReports.aspx", false);
					break;
				case "exportreports":
					Response.Redirect("ExportReports.aspx", false);
					break;
				case "logreports":
					Response.Redirect("LoginAlertReports.aspx", false);
					break;
				default:
					break;
			}
		}
	}

	private void ProcessUser()
	{
		SecurityAdministrator sam = new SecurityAdministrator();
		FetchUserResponse userReponse = new FetchUserResponse();
		XmlDocument doc = new XmlDocument();
		userReponse = sam.GetUserWithEmail(userId);

		if (userReponse.TheUser == null)
		{
			Response.Redirect("../NotLoggedIn.aspx", false);
		}
		else
		{
			Session.Add("UserName", userReponse.TheUser.FirstName + " " + userReponse.TheUser.LastName);

			UserRoleSecurityManager userRoleSecurityManager = new UserRoleSecurityManager();
			doc = userRoleSecurityManager.GetUserRolesDocs(userReponse.TheUser.Identity);
			XmlElement element = doc.DocumentElement;
			XmlNodeList nodes = element.ChildNodes;
			ArrayList roleslist = new ArrayList();
			for (int i = 0; i <= nodes.Count - 1; i++)
			{
				roleslist.Add(nodes[i]["ROLENAME"].InnerText);
			}

			if (roleslist.Contains("SoftwareAdmin"))
			{
				Session.Add("Role", "SoftwareAdmin");
				RedirectPages();
			}
			else if (roleslist.Contains("SoftwareEngineer"))
			{
				Session.Add("Role", "SoftwareEngineer");
				RedirectPages();
			}
			else if (roleslist.Contains("SoftwareReadOnly"))
			{
				Session.Add("Role", "SoftwareReadOnly");
				RedirectPages();
			}
			else
			{
				Response.Redirect("../NotLoggedIn.aspx", false);
			}
		}
	}
}
