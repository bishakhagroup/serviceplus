﻿using System;
using System.Data;
using System.Web.UI;
using SoftwarePlus.BusinessAccess;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;

public partial class sw_LoginAlertReports : System.Web.UI.Page {
    #region Properties

    Reports reports = new Reports();
    XmlErrorLog errorlog;
    ReportDocument rd = new ReportDocument();
    SoftwarePlusConfigManager configManager = new SoftwarePlusConfigManager();

    #endregion

    protected void Page_Load(object sender, EventArgs e) {
        lblerror.Text = "";
        lblReportStatus.Text = "";

        if (Session["UserName"] == null) {
            Response.Redirect("Default.aspx?post=logreports");
        } else {
            if (!Page.IsPostBack) {
                pnlLog.Visible = false;
                txtStartDate.Text = DateTime.Now.AddDays(-3).ToShortDateString();
                txtEndDate.Text = DateTime.Now.ToShortDateString();
            } else {
                ReportSelection();
            }
        }
    }

    private void ReportSelection() {
        try {
            string[] con = configManager.SoftwarePlusDBConnectionString().Split(new char[] { ';' });
            string UserName, Password;
            UserName = con[1].Split(new char[] { '=' }).GetValue(1).ToString();
            Password = con[2].Split(new char[] { '=' }).GetValue(1).ToString();
            if (lstReportName.SelectedValue == "SystemDeleteLog") {
                rd.Load(Server.MapPath("~/Report/SystemDeleteLog.rpt"));
                rd.SetDataSource(SystemDeleteLogData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();
                if (SystemDeleteLogData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            } else if (lstReportName.SelectedValue == "SystemErrorLog") {
                rd.Load(Server.MapPath("~/Report/SystemErrorLog.rpt"));
                rd.SetDataSource(SystemErrorLogData());
                rd.SetDatabaseLogon(@UserName, Password);
                CrystalReportViewer1.ReportSource = rd;
                CrystalReportViewer1.DataBind();
                if (SystemErrorLogData().Count == 0) { CrystalReportViewer1.Visible = false; lblReportStatus.Text = "No Records Found"; } else { CrystalReportViewer1.Visible = true; }
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void lstReportName_SelectedIndexChanged(object sender, EventArgs e) {
        if (lstReportName.SelectedValue == "SystemDeleteLog") {
            pnlLog.Visible = true;
        } else if (lstReportName.SelectedValue == "SystemErrorLog") {
            pnlLog.Visible = true;
        }

    }

    private List<clsSystemDeleteLog> SystemDeleteLogData() {

        DataSet dsRPTSystemDeleteLog = reports.RPTSystemDeleteLog(DateTime.Parse(txtStartDate.Text), DateTime.Parse(txtEndDate.Text));
        DataTable dt = dsRPTSystemDeleteLog.Tables[0];

        List<clsSystemDeleteLog> lsdl = new List<clsSystemDeleteLog>();

        foreach (DataRow r in dt.Rows) {
            clsSystemDeleteLog dl = new clsSystemDeleteLog();
            dl.UserID = r["USER_ID"].ToString();
            dl.DeleteDate = DateTime.Parse(r["DELETE_DATE"].ToString());
            dl.FormName = r["FORM_NAME"].ToString();
            dl.FormValue = r["FORM_VALUE"].ToString();
            dl.StartDate = DateTime.Parse(txtStartDate.Text);
            dl.Enddate = DateTime.Parse(txtEndDate.Text);
            lsdl.Add(dl);
        }
        if (dt.Rows.Count == 0) {
            clsSystemDeleteLog dl = new clsSystemDeleteLog();
            dl.StartDate = DateTime.Parse(txtStartDate.Text);
            dl.Enddate = DateTime.Parse(txtEndDate.Text);
            lsdl.Add(dl);
        }

        return lsdl;

    }

    private List<clsSystemErrorLog> SystemErrorLogData() {

        DataSet dsRPTSystemErrorLog = reports.RPTSystemErrorLog(DateTime.Parse(txtStartDate.Text), DateTime.Parse(txtEndDate.Text));
        DataTable dt = dsRPTSystemErrorLog.Tables[0];

        List<clsSystemErrorLog> lsel = new List<clsSystemErrorLog>();

        foreach (DataRow r in dt.Rows) {
            clsSystemErrorLog el = new clsSystemErrorLog();

            el.UserID = r["USER_ID"].ToString();
            el.ErrorDate = DateTime.Parse(r["ERROR_DATE"].ToString());
            el.Version = r["VERSION"].ToString();
            el.FormName = r["FORM_NAME"].ToString();
            el.ProcedureName = r["PROCEDURE_NAME"].ToString();
            el.ErrorCode = r["ERROR_CODE"].ToString();
            el.ErrorDescription = r["ERROR_DESCRIPTION"].ToString();
            el.StartDate = DateTime.Parse(txtStartDate.Text);
            el.Enddate = DateTime.Parse(txtEndDate.Text);
            lsel.Add(el);

        }
        if (dt.Rows.Count == 0) {
            clsSystemErrorLog el = new clsSystemErrorLog();
            el.StartDate = DateTime.Parse(txtStartDate.Text);
            el.Enddate = DateTime.Parse(txtEndDate.Text);
            lsel.Add(el);
        }
        return lsel;

    }

    protected void btnSubmit_Click(object sender, ImageClickEventArgs e) {

    }
}
