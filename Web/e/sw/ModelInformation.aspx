<%@ Page Title="Model Information" Language="C#" MasterPageFile="~/e/sw/SWPlusMaster.master"
    AutoEventWireup="true" CodeFile="ModelInformation.aspx.cs" Inherits="sw_ModelInformation_cm"
    Theme="SkinFile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Controls" Namespace="Sony.US.ServicesPLUS.Controls" TagPrefix="SPS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="updpnlmodelinfo" runat="server">
        <ContentTemplate>
            <table border="0" style="width: 100%">
                <tr>
                    <td style="width: 4px">
                    </td>
                    <td>
                        <table border="0" style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblerror" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" AutoPostBack="True"
                                        Width="900px" OnActiveTabChanged="TabContainer1_ActiveTabChanged">
                                        <asp:TabPanel runat="server" HeaderText="Model Information" ID="TabPanel1">
                                            <HeaderTemplate>
                                                Model Information</HeaderTemplate>
                                            <ContentTemplate>
                                                <table border="0" style="width: 100%">
                                                    <tr bgcolor="#f2f5f8">
                                                        <td style="width: 141px">
                                                            <asp:Label ID="lblModelId" runat="server" Text="Model ID:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="revalmodelid" runat="server" ControlToValidate="txtmodelid" ErrorMessage="Enter Model ID"
                                                                SetFocusOnError="True" ValidationGroup="Parent">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="ddlmodelId" runat="server" AutoPostBack="True" DropDownStyle="DropDownList"
                                                                AutoCompleteMode="SuggestAppend" OnSelectedIndexChanged="ddlmodelId_SelectedIndexChanged"
                                                                Visible="False" MaxLength="0">
                                                            </asp:ComboBox>
                                                            <SPS:SPSTextBox ID="txtmodelid" runat="server" MaxLength="15"></SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                                ID="txtmodelid_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers, UppercaseLetters, LowercaseLetters, Custom"
                                                                TargetControlID="txtmodelid" ValidChars="/">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblModelStatus" runat="server" Text="Model Status:"></asp:Label>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlmodelstatus" runat="server" Width="75px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 141px">
                                                            <asp:Label ID="lblType" runat="server" Text="Type:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="reqvalddltype" runat="server" ControlToValidate="ddltype" ErrorMessage="Select Model Type"
                                                                InitialValue="Select" SetFocusOnError="True" ValidationGroup="Parent">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddltype" runat="server" Width="135px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblupgradeest" runat="server" Text="Upgrade Estimate:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="reqvalupgest" runat="server" ControlToValidate="txtupgradeest" Display="Dynamic"
                                                                ErrorMessage="Enter Upgrade Estimate" SetFocusOnError="True" ValidationGroup="Parent">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtupgradeest" runat="server" MaxLength="4" Width="75px">0</SPS:SPSTextBox><asp:FilteredTextBoxExtender
                                                                ID="txtupgradeest_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers"
                                                                TargetControlID="txtupgradeest">
                                                            </asp:FilteredTextBoxExtender>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td style="width: 141px">
                                                            <asp:Label ID="lblMainAssembly" runat="server" Text="Main Assembly:"></asp:Label>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlmainassembly" runat="server" Width="135px">
                                                                <asp:ListItem>No</asp:ListItem>
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 4px">
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblprversionreq" runat="server" Text="Prior Version Required:"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlprversreq" runat="server" Width="75px">
                                                                <asp:ListItem>No</asp:ListItem>
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 141px">
                                                            <asp:Label ID="lbltracking" runat="server" Text="Tracking:"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddltracking" runat="server" Width="135px">
                                                                <asp:ListItem>No</asp:ListItem>
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblsource" runat="server" Text="Source:"></asp:Label>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlsource" runat="server" Width="75px">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 141px">
                                                            <asp:Label ID="lblModelDesc" runat="server" Text="Model Description:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="revalmodeldesc" runat="server" ControlToValidate="txtmodeldesc" ErrorMessage="Enter Model Description"
                                                                SetFocusOnError="True" ValidationGroup="Parent">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                        </td>
                                                        <td colspan="5">
                                                            <SPS:SPSTextBox ID="txtmodeldesc" runat="server" Height="50px" TextMode="MultiLine"
                                                                Width="250px"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td colspan="7">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                  <fieldset>
                                <legend class="legendlabel">All Releases Related to This Model</legend>
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Panel ID="pnlmodelinfo" runat="server" Width="850px" ScrollBars="Auto">
                                                                <asp:GridView ID="gviewmodelinfo" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gviewmodelinfo_PageIndexChanging"
                                                                    Width="800px">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                     <asp:TemplateField SortExpression="Version" HeaderText="Version">
                                                                     <ItemTemplate>
                                                                      <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Version") %>' NavigateUrl='<%# "ReleaseLog.aspx" + "?Identity="+DataBinder.Eval(Container.DataItem, "MODEL_ID").ToString()+" V"+DataBinder.Eval(Container.DataItem, "VERSION").ToString() %>' ID="Hyperlink2" ForeColor="#000099">
                                                                                             </asp:HyperLink>
                                                                     </ItemTemplate>
                                                                     </asp:TemplateField>

                                                                        <asp:BoundField DataField="RELEASE_DATE" HeaderText="Release Date" HtmlEncode="False"
                                                                            DataFormatString="{0:MM/dd/yyyy}">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle Wrap="False"/>
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RELEASE_TYPE" HeaderText="Release Type">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DISTRIBUTION_TYPE" HeaderText="Distribution Type">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="VISIBILITY_TYPE" HeaderText="Visibility Type">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RELEASE_STATUS" HeaderText="Release Status">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RELEASE_QUANTITY" HeaderText="Release Quantity">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MINIMUM_ON_HAND" HeaderText="Minimum On Hand">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="START_NUMBER" HeaderText="Start Number">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="END_NUMBER" HeaderText="End Number">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="USER_MANUAL_FILE" HeaderText="User Manual File">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="TECHNICAL_REFERENCE" HeaderText="Technical Reference">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COMMENTS" HeaderText="Comments">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle  Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COMPATIBILITY" HeaderText="Compatibility">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle Wrap="False" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </fieldset>
                                              <fieldset align="middle">
                                <legend class="legendlabel">All Kits Related to This Model</legend>
                                 <table>
                                 <tr>
                                                <td>
                                                    <asp:Label ID="lblkitrelease" runat="server" Text="Release Kits:"></asp:Label></td>
                                                <td><asp:ComboBox ID="ddlrelkits" runat="server" AutoPostBack="True" 
                                                        DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend" 
                                                        MaxLength="0" onselectedindexchanged="ddlrelkits_SelectedIndexChanged" 
                                                        Width="400px">
                                                            </asp:ComboBox></td>
                                                
                                                </tr>
                                                </TABLE>
                                                <table border="0" style="width: 100%">
                                                   <tr>
                                                        <td>
                                                            <asp:Panel ID="pnlgviewkitinfo" runat="server" ScrollBars="Auto" Width="850px">
                                                                <asp:GridView ID="gviewkitinfo" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gviewkitinfo_PageIndexChanging"
                                                                    Width="800px">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                       <asp:TemplateField SortExpression="Kit Part Number" HeaderText="Kit Part Number">
                                                                     <ItemTemplate>
                                                                      <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KIT_PART_NUMBER") %>' NavigateUrl='<%# "KitInformation.aspx" + "?Identity="+DataBinder.Eval(Container.DataItem, "KIT_PART_NUMBER").ToString() %>' ID="Hyperlink3" ForeColor="#000099">
                                                                                             </asp:HyperLink>
                                                                     </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="KIT_DESCRIPTION" HeaderText="Kit Description">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="SUPERCEDED_KIT" HeaderText="Superceded Kit">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="MANUFACTURER_REMARKS" HeaderText="Manufacturer Remarks">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DOWNLOAD_FILE" HeaderText="Download File">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="RELEASE_NOTES_FILE" HeaderText="Release Notes File">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="USER_MANUAL_FILE" HeaderText="User Manual File">
                                                                            <HeaderStyle Wrap="False" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </fieldset>
                                                <table class="style1" id="tblbtn" align="center">
                                                    <tr>
                                                        <td align="center" class="style16">
                                                            <asp:ImageButton ID="btnNewModel" runat="server" OnClick="btnNewModel_Click" ImageUrl="../../Shared/images/sw_Add.JPG"
                                                                CausesValidation="False" />
                                                        </td>
                                                        <td align="center" class="style17">
                                                            <asp:ImageButton ID="btnSaveModel" runat="server" ImageUrl="~/Shared/Images/sw_Save.JPG"
                                                                OnClick="btnSaveModel_Click" ImageAlign="AbsMiddle" ValidationGroup="Parent" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnupdatemodel" runat="server" ImageUrl="~/Shared/images/sw_Update.JPG"
                                                                OnClick="btnupdatemodel_Click" ValidationGroup="Parent" />
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btncancelModel" runat="server" CausesValidation="False" ImageUrl="~/Shared/images/sw_Cancel.jpg"
                                                                OnClick="btncancelModel_Click" />
                                                        </td>
                                                        <td align="center" class="style18">
                                                            <asp:ImageButton ID="btnDeleteModel" runat="server" ImageUrl="~/Shared/images/sw_Delete.JPG"
                                                                OnClick="btnDeleteModel_Click" CausesValidation="False" />
                                                        </td>
                                                        <td>
                              <%--<asp:Panel ID="panelUpdateProgress" runat="server">
                                       <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                           AssociatedUpdatePanelID="updpnlmodelinfo" DisplayAfter="0">
                                           <ProgressTemplate>
                                           <div style="position: relative; top: 30%; text-align: center;">
						<img src="../../Shared/Images/progbar.gif"  style="vertical-align: middle" alt="Processing" />
						Processing ...
					</div>
                                           </ProgressTemplate>
                                       </asp:UpdateProgress>
                                       </asp:Panel>--%>
                                       </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td align="center" class="style16">
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                                ShowSummary="False" ValidationGroup="Parent" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="Child Model">
                                            <ContentTemplate>
                                                <table border="0" style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblchmodelid" runat="server" Text="Model ID:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="reqchmodelid" runat="server" ControlToValidate="txtchmodelid" Display="Dynamic"
                                                                ErrorMessage="Enter Model ID" SetFocusOnError="True" ValidationGroup="Child">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:ComboBox ID="ddlchmodelid" runat="server" AutoPostBack="True" DropDownStyle="DropDownList"
                                                                AutoCompleteMode="SuggestAppend" OnSelectedIndexChanged="ddlchmodelid_SelectedIndexChanged"
                                                                MaxLength="0">
                                                            </asp:ComboBox>
                                                            <SPS:SPSTextBox ID="txtchmodelid" runat="server" Visible="False" MaxLength="15"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchupgest" runat="server" Text="Upgrade Estimate:"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="reqchupgest" runat="server" 
                                                                ControlToValidate="txtchupgest" Display="Dynamic" 
                                                                ErrorMessage="Enter Upgrade Estimate" SetFocusOnError="True" 
                                                                ValidationGroup="Child">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtchupgest" runat="server" MaxLength="4"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchmodelstatus" runat="server" Text="Model Status:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11">
                                                            <asp:DropDownList ID="ddlchmodelstatus" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchmodeldesc" runat="server" Text="Model Description:"></asp:Label><asp:RequiredFieldValidator
                                                                ID="reqchmodeldesc" runat="server" ControlToValidate="txtchmodeldesc" Display="Dynamic"
                                                                ErrorMessage="Enter Model Description" SetFocusOnError="True" ValidationGroup="Child">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtchmodeldesc" runat="server" MaxLength="75" 
                                                                TextMode="MultiLine" Width="175px"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblchtype" runat="server" Text="Type:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlchtype" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchmainassembly" runat="server" Text="Main Assembly:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlchmainassembly" runat="server">
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                                <asp:ListItem>No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchsource" runat="server" Text="Source:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11">
                                                            <asp:DropDownList ID="ddlchsource" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchprversreq" runat="server" Text="Prior Version Required:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlchprversreq" runat="server">
                                                                <asp:ListItem Value="Yes"></asp:ListItem>
                                                                <asp:ListItem Value="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchtracking" runat="server" Text="Tracking:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11">
                                                            <asp:DropDownList ID="ddlchtracking" runat="server">
                                                                <asp:ListItem>Yes</asp:ListItem>
                                                                <asp:ListItem>No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchcreatedby" runat="server" Text="Created By:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11">
                                                            <SPS:SPSTextBox ID="txtchcreatedby" runat="server" 
                                                                MaxLength="21" BackColor="#CCCCCC" ReadOnly="True"></SPS:SPSTextBox>
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchdatecreated" runat="server" Text="Date Created:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtchdatecreated" runat="server" BackColor="#CCCCCC" 
                                                                ReadOnly="True"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchupdatedby" runat="server" Text="Updated By:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11">
                                                            <SPS:SPSTextBox ID="txtchupdatedby" runat="server" 
                                                                MaxLength="21" BackColor="#CCCCCC" ReadOnly="True"></SPS:SPSTextBox>
                                                        </td>
                                                        <td class="style12">
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblchlastupdated" runat="server" Text="Last Updated:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <SPS:SPSTextBox ID="txtchlastupdated" runat="server" BackColor="#CCCCCC" 
                                                                ReadOnly="True"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchcomments" runat="server" Text="Comments:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11" colspan="4">
                                                            <SPS:SPSTextBox ID="txtchcomments" runat="server" Height="50px" TextMode="MultiLine"
                                                                Width="250px" MaxLength="4000"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style15">
                                                            <asp:Label ID="lblchnotes" runat="server" Text="Notes:"></asp:Label>
                                                        </td>
                                                        <td class="style14">
                                                            &nbsp;
                                                        </td>
                                                        <td class="style11" colspan="4">
                                                            <SPS:SPSTextBox ID="txtchnotes" runat="server" Height="50px" TextMode="MultiLine"
                                                                Width="250px" MaxLength="4000"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <fieldset align="center">
                                                    <legend class="legendlabel">Add ChildModel Information</legend>
                                                    <table border="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblchilModelSearch" Text="Search ChildModel" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <SPS:SPSTextBox ID="txtchildModelSearch" runat="server" MaxLength="15"></SPS:SPSTextBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <td align="center" class="style18">
                                                                    <asp:Button ID="btnChildModelSearch" runat="server" Text="Search" Width="100px" OnClick="btnChildModelSearch_Click"
                                                                        CausesValidation="False" />
                                                                </td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table border="0" style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="gviewchmodel" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                                    ForeColor="#333333" GridLines="None" OnPageIndexChanging="gviewchmodel_PageIndexChanging">
                                                                    <AlternatingRowStyle BackColor="White" />
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged" /></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Child ModelId">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblChildControl" runat="server" Text='<%# Eval("MODEL_ID") %>' /></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Model Description">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("MODEL_DESCRIPTION") %>' /></ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <EditRowStyle BackColor="#2461BF" />
                                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                                    <RowStyle BackColor="#EFF3FB" />
                                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                </asp:GridView>
                                                            </td>
                                                           
                                                        </tr>
                                                    </table>
                                                     <table>
                                                <tr>
                                                <td>
                                                    <asp:Button ID="btDeleteChildModels" runat="server" Text="Delete Child Models" 
                                                        Width="152px" onclick="btDeleteChildModels_Click" />
                                                </td>
                                                </fieldset>
                                               
                                                </tr>
                                                </table>
                                                <table align="center">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:ImageButton ID="SaveChildModel" runat="server" ImageUrl="~/Shared/images/sw_Save.JPG"
                                                                OnClick="SaveChildModel_Click" ValidationGroup="Child" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnchupdate" runat="server" ImageUrl="~/Shared/images/sw_Update.JPG"
                                                                OnClick="btnchupdate_Click" ValidationGroup="Child" />
                                                        </td>
                                                        <td align="left">
                                                            <asp:ImageButton ID="btnchcancel" runat="server" CausesValidation="False" ImageUrl="~/Shared/images/sw_Cancel.jpg"
                                                                OnClick="btnchcancel_Click" />
                                                        </td>
                                                        <td align="center">
                                                            <asp:ImageButton ID="btnDeleteChildInfo" runat="server" ImageUrl="~/Shared/images/sw_Delete.JPG"
                                                                OnClick="btnDeleteChildInfo_Click" CausesValidation="False"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table>
                                                    <tr>
                                                        <td align="center" class="style16">
                                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                                                                ShowSummary="False" ValidationGroup="Child" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                    </asp:TabContainer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 4px">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
