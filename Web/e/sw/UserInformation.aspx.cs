﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sony.US.siam;
using SoftwarePlus.BusinessAccess;
using System.Collections;
using System.Data;

public partial class sw_UserInformation : System.Web.UI.Page {
    SoftwarePlusConfigManager configManager;
    UsersDataManager userdbManager;
    Users user;
    XmlErrorLog errorLog = new XmlErrorLog();
    SecurityAdministrator securityAdmin;

    protected void Page_Load(object sender, EventArgs e) {
        try {
            if (!IsPostBack) {
                LoadChildData();
                LoadParentDefaultData();
                tabcontainuserinfo.ActiveTabIndex = 0;
                txtuserid.Visible = false;
                // btnSaveuser.Visible = false;
            }
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
            lblMessage.Text = "The following error occured." + ex.Message;
        }
    }

    private void LoadParentDefaultData() {
        userdbManager = new UsersDataManager();
        configManager = new SoftwarePlusConfigManager();
        ArrayList roles = configManager.SoftwarePlusUserRoles();
        ddlrole.DataSource = roles;
        ddlrole.DataBind();

        DataSet userIds = userdbManager.GetUserIds();

        ddluserid.DataSource = userIds.Tables[0];
        ddluserid.DataTextField = userIds.Tables[0].Columns["USER_ID"].ToString();
        ddluserid.DataBind();
        GetUserInfo();
    }

    private void GetUserInfo() {
        userdbManager = new UsersDataManager();
        Users user = (Users)userdbManager.GetUserInfo(ddluserid.SelectedValue);
        // txtuserid.Text =user.UserId  ;
        txtchaddress.Text = user.Address1 + "\n" + user.Address2;
        txtchcellular.Text = user.Cellular;
        txtchcity.Text = user.City;
        txtchhomephone.Text = user.HomePhone;
        txtchpager.Text = user.Pager;
        txtchvoicemail.Text = user.VoiceMail;
        txtchzip.Text = user.Zip;
        if (user.Country != "") {
            ddlchcountry.SelectedValue = user.Country;
        } else {
            ddlchcountry.SelectedValue = "SELECT";
        }
        if (user.State != "") {
            ddlchstate.SelectedValue = user.State;
        }
        txtEmail.Text = user.Email;
        txtfname.Text = user.FirstName;
        txtlname.Text = user.LastName;
        // user.Address1 = txtchaddress.Text;
        //  user.City = txtchcity.Text;
        //user.Country = ddlchcountry.SelectedValue;
        //user.State = ddlchstate.SelectedValue;
        ddlrole.SelectedValue = user.UserRole;
        if (user.UserStatus == -1) {
            // user.UserStatus = -1;
            ddluserstatus.SelectedValue = "Active";
        } else {
            ddluserstatus.SelectedValue = "Inactive";
        }
        //user.VoiceMail = txtchvoicemail.Text;
        // user.Zip = txtchzip.Text;
        // txtchstdate.Text = user.StartDate.ToString();
        txtcreatedby.Text = user.CreatedBy;
        txtdatecreated.Text = user.DateCreated.ToString();
        txtupdatedby.Text = user.UpdateBy;
        txtlastupdated.Text = user.LastUpdatedDate.ToString();
        txtoffphone.Text = user.OfficePhone;
        txtoffphext.Text = user.Extension;
        txtfax.Text = user.FaxNumber;
        txtnotes.Text = user.Notes;
    }

    private void LoadChildData() {
        UsersDataManager db = new UsersDataManager();
        DataSet countryds = db.GetCountryInfo();

        ddlchcountry.DataSource = countryds.Tables[0];
        ddlchcountry.DataTextField = countryds.Tables[0].Columns["COUNTRY"].ToString();
        ddlchcountry.DataBind();
        // STATE
        DataSet stateds = db.GetStatesInfo();
        ddlchstate.DataSource = stateds.Tables[0];
        ddlchstate.DataTextField = stateds.Tables[0].Columns["STATE"].ToString();
        ddlchstate.DataBind();
    }

    protected void btnNewuser_Click(object sender, ImageClickEventArgs e) {
        try {
            Session.Remove("EditUser");
            Session.Remove("Edit");
            btnSaveuser.Visible = true;
            txtuserid.Visible = true;
            ddluserid.Visible = false;
            ClearChildData();
            ClearParentData();
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
            lblMessage.Text = "The following error occured." + ex.Message;
        }
    }

    private void ClearParentData() {
        txtEmail.Text = "";
        txtfname.Text = "";
        txtlname.Text = "";

        txtcreatedby.Text = HttpContext.Current.User.Identity.Name;
        txtdatecreated.Text = System.DateTime.Now.ToString();
        txtupdatedby.Text = "";
        txtlastupdated.Text = "";
        txtoffphone.Text = "";
        txtoffphext.Text = "";
        txtfax.Text = "";
        txtnotes.Text = "";
        txtuserid.Text = "";
    }

    private void ClearChildData() {
        txtchaddress.Text = "";
        txtchzip.Text = "";
        txtchpager.Text = "";
        txtchcity.Text = "";
        txtchhomephone.Text = "";
        txtchpager.Text = "";
        txtchcellular.Text = "";
    }

    protected void btnSaveuser_Click(object sender, ImageClickEventArgs e) {
        try {
            var userDataManager = new UsersDataManager();
            user = new Users {
                Email = txtEmail.Text,
                FirstName = txtfname.Text,
                LastName = txtlname.Text,
                Address1 = txtchaddress.Text,
                City = txtchcity.Text,
                Country = ddlchcountry.SelectedValue,
                State = ddlchstate.SelectedValue,
                UserRole = ddlrole.SelectedValue,
                StartDate = DateTime.Now,       //Convert.ToDateTime(txtchstdate.Text);
                CreatedBy = txtcreatedby.Text,
                DateCreated = DateTime.Now,     //Convert.ToDateTime(txtdatecreated.Text);
                UpdateBy = txtupdatedby.Text,
                LastUpdatedDate = DateTime.Now  //Convert.ToDateTime(txtlastupdated.Text);
            };

            if (txtuserid.Text.Length > 0) {
                user.UserId = txtuserid.Text;
            } else {
                user.UserId = ddluserid.Text;
            }
            if (ddluserstatus.SelectedValue == "Active") {
                user.UserStatus = -1;
            } else {
                user.UserStatus = 0;
            }

            if (Session["Edit"] != null) {
                userDataManager.UpdateUser(user);
                Session.Add("EditUser", user);
                lblMessage.Text = "Record Updated Sucessfully";
            } else {
                //string userdata = string.Empty;
                //Users user = (Users)Session["User"];
                securityAdmin = new SecurityAdministrator();
                //string userdata =securityAdmin.SearchUser("sanbbbpatel@gmail.com");
                string userdata = securityAdmin.SearchUser(txtEmail.Text.Trim());
                if (userdata == "") {
                    string error = string.Empty;
                    user.Password = txtPassword.Text;
                    securityAdmin.AddSoftwarePlusUser(user, out error);

                    int value = userDataManager.ValidateUserData(txtEmail.Text.Trim());

                    if (value == 0) {
                        userDataManager.AddUser(user);
                        // Session.Add("ADDEDUSERID", user.UserId);
                        Session.Add("NewUser", user);
                    }
                } else {
                    int value = userDataManager.ValidateUserData(txtEmail.Text.Trim());

                    if (value == 0) {
                        userDataManager.AddUser(user);
                        // Session.Add("ADDEDUSERID", user.UserId);
                        Session.Add("NewUser", user);
                    } else {
                        lblMessage.Text = "User Already Exists";
                    }
                }
            }
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
            lblMessage.Text = "The following error occured." + ex.Message;
        }
    }

    protected void tabcontainuser_ActiveTabChanged(object sender, EventArgs e) {
        if (tabcontainuserinfo.ActiveTabIndex == 0) {
            //txtchildModelSearch.Text = "";
        } else {
            //LoadChildData();
        }
    }

    protected void ddluserid_SelectedIndexChanged(object sender, EventArgs e) {
        GetUserInfo();
    }

    protected void btnEdituser_Click(object sender, ImageClickEventArgs e) {
        try {
            Session.Add("Edit", "true");
            txtuserid.Visible = false;
            ddluserid.Visible = true;
            btnSaveuser.Visible = true;
            LoadChildData();
            LoadParentDefaultData();
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
        }
    }

    protected void btnDeleteuser_Click(object sender, ImageClickEventArgs e) {
        // userdbManager.DeleteUser(ddluserid.SelectedIn
        userdbManager = new UsersDataManager();
        try {
            userdbManager.DeleteUser(ddluserid.SelectedValue);
            lblMessage.Text = "Record Deleted Sucessfully.";
            LoadChildData();
            LoadParentDefaultData();
            tabcontainuserinfo.ActiveTabIndex = 0;
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
            lblMessage.Text = "The following error occured." + ex.Message;
        }
    }

    protected void btnchsave_Click(object sender, ImageClickEventArgs e) {
        try {
            userdbManager = new UsersDataManager();
            if (Session["Edit"] != null) {
                Users user = (Users)Session["EditUser"];
                user.City = txtchcity.Text;
                // user.Country = ddlchcountry.SelectedValue;
                //user.State = ddlchstate.SelectedValue;
                user.VoiceMail = txtchvoicemail.Text;
                user.Zip = txtchzip.Text;
                user.Pager = txtchpager.Text;
                user.Cellular = txtchcellular.Text;
                user.HomePhone = txtchhomephone.Text;
                user.Address1 = txtchaddress.Text;

                userdbManager.UpdateUser(user);
            } else {
                Users user = (Users)Session["NewUser"];

                if (user != null) {
                    user.City = txtchcity.Text;
                    // user.Country = ddlchcountry.SelectedValue;
                    //user.State = ddlchstate.SelectedValue;
                    user.VoiceMail = txtchvoicemail.Text;
                    user.Zip = txtchzip.Text;
                    user.Pager = txtchpager.Text;
                    user.Cellular = txtchcellular.Text;
                    user.HomePhone = txtchhomephone.Text;
                    user.Address1 = txtchaddress.Text;

                    userdbManager.UpdateChildUser(user);

                    lblMessage.Text = "Record Saved Sucessfully";
                } else {
                    user = new Users {
                        UserId = txtuserid.Text,
                        Email = txtEmail.Text,
                        FirstName = txtfname.Text,
                        LastName = txtlname.Text,
                        Address1 = txtchaddress.Text,
                        City = txtchcity.Text,
                        Country = ddlchcountry.SelectedValue,
                        State = ddlchstate.SelectedValue,
                        UserRole = ddlrole.SelectedValue,
                        StartDate = System.DateTime.Now, //Convert.ToDateTime(txtchstdate.Text);
                        CreatedBy = txtcreatedby.Text,
                        DateCreated = System.DateTime.Now,//Convert.ToDateTime(txtdatecreated.Text);
                        UpdateBy = txtupdatedby.Text,
                        LastUpdatedDate = System.DateTime.Now,//Convert.ToDateTime(txtlastupdated.Text);
                        VoiceMail = txtchvoicemail.Text,
                        Zip = txtchzip.Text,
                        Pager = txtchpager.Text,
                        Cellular = txtchcellular.Text,
                        HomePhone = txtchhomephone.Text
                    };

                    if (ddluserstatus.SelectedValue == "Active") {
                        user.UserStatus = -1;
                    } else {
                        user.UserStatus = 0;
                    }

                    userdbManager.AddUser(user);
                    lblMessage.Text = "Record Saved Sucessfully";
                }
            }
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
        }
    }

    protected void btnchdelete_Click(object sender, ImageClickEventArgs e) {
        // userdbManager.DeleteUser(ddluserid.SelectedIn
        userdbManager = new UsersDataManager();
        try {
            userdbManager.DeleteUser(ddluserid.SelectedValue);
            LoadChildData();
            LoadParentDefaultData();
            tabcontainuserinfo.ActiveTabIndex = 0;
        } catch (Exception ex) {
            errorLog = new XmlErrorLog();
            errorLog.LogError(ex, severity.Error.ToString());
        }
    }
}
