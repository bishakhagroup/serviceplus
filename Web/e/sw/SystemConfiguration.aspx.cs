﻿using System;
using System.Web.UI;
using SoftwarePlus.BusinessAccess;
using System.Data;
using System.Drawing;
using System.Net;
using Sony.US.SIAMUtilities;
using Sftp.SharpSsh;

public partial class sw_SystemConfiguration : System.Web.UI.Page {
    #region Properties
    DataSet ds;
    Configuration conf = new Configuration();
    SoftwarePlusConfigManager softwareConfigMgr = new SoftwarePlusConfigManager();
    XmlErrorLog errorlog;
    #endregion
    protected void Page_Load(object sender, EventArgs e) {
        try {
            lblerror.Text = "";
            if (Session["UserName"] == null) {
                Response.Redirect("Default.aspx?post=SysConfig");
            } else
            if (!IsPostBack) {
                UserAccess();
                BindStateInfo();
                BindCountryInfo();
                LoadSystemConfigData();
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void UserAccess() {
        if (Session["Role"]?.ToString() == "SoftwareAdmin") {

        } else if (Session["Role"]?.ToString() == "SoftwareEngineer") {
            btncancel.Visible = false;
            btnsave.Visible = false;
        } else {
            btncancel.Visible = false;
            btnsave.Visible = false;
        }
    }
    private void BindStateInfo() {
        try {
            DataSet dsStateInfo = conf.GetStatesInfo();
            ddlstate.DataSource = dsStateInfo.Tables[0];
            ddlstate.DataTextField = dsStateInfo.Tables[0].Columns["STATE_NAME"].ToString();
            ddlstate.DataValueField = dsStateInfo.Tables[0].Columns["STATE"].ToString();
            ddlstate.DataBind();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void BindCountryInfo() {
        try {
            DataSet dsCountryInfo = conf.GetCountryInfo();
            ddlcountry.DataSource = dsCountryInfo.Tables[0];
            ddlcountry.DataTextField = dsCountryInfo.Tables[0].Columns["COUNTRY"].ToString();
            ddlcountry.DataBind();
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
    private void LoadSystemConfigData() {
        try {
            lblerror.Text = "";
            DataSet dsSystemConfigData = conf.GetSystemConfigData();
            if (dsSystemConfigData != null) {
                DataTable dt = dsSystemConfigData.Tables[0];
                foreach (DataRow row in dt.Rows) {
                    hideseccheck.Value = row["SECURITY_CHECK"].ToString();
                    txtadd1.Text = row["ADDRESS"].ToString();
                    txtadd2.Text = row["ADDRESS_2"].ToString();
                    txtcity.Text = row["CITY"].ToString();
                    ddlstate.SelectedValue = row["STATE"].ToString();
                    txtpin.Text = row["ZIP"].ToString();
                    ddlcountry.SelectedValue = row["COUNTRY"].ToString();
                    txtoffphone.Text = row["OFFICE_PHONE"].ToString();
                    txtfaxnum.Text = row["FAX_NUMBER"].ToString();
                    txtinternetadd.Text = row["INTERNET_ADDRESS"].ToString();
                    txtrptfaxnum.Text = row["REPORT_FAX"].ToString();
                    txtrepphnum.Text = row["REPORT_PHONE"].ToString();
                    txtrepoption.Text = row["REPORT_OPTION"].ToString();
                    txtftpserver.Text = row["FTP_SERVER"].ToString();
                    txtftpuserid.Text = row["FTP_USER_ID"].ToString();
                    //txtftppwd.Text = row["FTP_PASSWORD"].ToString();
                    //txtftppwd.Attributes.Add("value",row["FTP_PASSWORD"].ToString());
                    txtftpdownfile.Text = row["FTP_LOCATION"].ToString();
                    txtftprelnotefile.Text = row["FTP_LOCATION_2"].ToString();
                    txtftpuserfile.Text = row["FTP_LOCATION_3"].ToString();

                    if (row["DELETE_LOCAL"].ToString().Contains("-1")) {
                        chkftpdownfile.Checked = true;
                        //chbstatus = 1;
                    } else {
                        chkftpdownfile.Checked = false;
                    }

                    if (row["DELETE_LOCAL_2"].ToString().Contains("-1")) {
                        chkftprelnotefile.Checked = true;
                        //chbstatus = 1;
                    } else {
                        chkftprelnotefile.Checked = false;
                    }
                    if (row["DELETE_LOCAL_3"].ToString().Contains("-1")) {
                        chkftpuserfile.Checked = true;
                        //chbstatus = 1;
                    } else {
                        chkftpuserfile.Checked = false;
                    }
                }
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }

    }

    protected void btncancel_Click(object sender, ImageClickEventArgs e) {
        UserAccess();
        BindStateInfo();
        BindCountryInfo();
        LoadSystemConfigData();
    }
    protected void btnsave_Click(object sender, ImageClickEventArgs e) {
        try {
            int deldown;
            int delrel;
            int deluser;
            if (chkftpdownfile.Checked == true) {
                deldown = 1;
            } else {
                deldown = 0;
            }
            if (chkftprelnotefile.Checked == true) {
                delrel = 1;
            } else {
                delrel = 0;
            }
            if (chkftpuserfile.Checked == true) {
                deluser = 1;
            } else {
                deluser = 0;
            }
            string strsftpencryptpwd = Sony.US.SIAMUtilities.Encryption.Encrypt(txtftppwd.Text, Sony.US.SIAMUtilities.Encryption.PWDKey);
            conf.UpdSysConfig(Convert.ToInt32(hideseccheck.Value), txtadd1.Text, txtadd2.Text, txtcity.Text, ddlstate.SelectedValue, txtpin.Text, ddlcountry.SelectedValue, txtoffphone.Text, txtfaxnum.Text, txtinternetadd.Text, txtrptfaxnum.Text, txtrepphnum.Text, txtrepoption.Text, txtftpserver.Text, txtftpuserid.Text, strsftpencryptpwd, txtftpdownfile.Text, txtftprelnotefile.Text, deldown, delrel, txtftpuserfile.Text, deluser);
            lblerror.Text = "Record Updated Succesfully.";
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }

    protected void btntestftp_Click(object sender, ImageClickEventArgs e) {

        try {
            IPAddress[] addresslist = Dns.GetHostAddresses(txtftpserver.Text);
            string ipAddress = string.Empty;

            foreach (IPAddress theaddress in addresslist) {
                ipAddress = theaddress.ToString();
            }
            Scp scp = new Scp();

            bool connection = scp.TestConnection(ipAddress, txtftpdownfile.Text, txtftpuserid.Text, txtftppwd.Text);
            bool connection1 = scp.TestConnection(ipAddress, txtftprelnotefile.Text, txtftpuserid.Text, txtftppwd.Text);
            bool connection2 = scp.TestConnection(ipAddress, txtftpuserfile.Text, txtftpuserid.Text, txtftppwd.Text);
            if ((connection) && (connection1) && (connection2)) {
                lblStatus.ForeColor = Color.Green;
                lblStatus.Text = "Test Connection Sucessfull";
            } else {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = "Test Connection Failed";
            }
        } catch (Exception ex) {
            errorlog = new XmlErrorLog();
            errorlog.LogError(ex, severity.Error.ToString());
            lblerror.Text = "The following exception occurs, " + ex.Message;
        }
    }
}