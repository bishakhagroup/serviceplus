Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports System.Globalization
Imports Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports System.Data
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin
    Partial Class training_class_edit
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtCourseNumber As SPSTextBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            Me.btnDelete.Attributes.Add("onClick", "return confirmDelete('Do you really want to hide this class');")
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    populateCourseDropDown()
                    populateLocationDropDown()
                    txtTime.Text = ConfigurationData.GeneralSettings.ClassRoomTrainingTimings
                    If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-class-list") Is Nothing) Then
                        Dim courseSelected As CourseSchedule
                        Dim itemNumber As Integer = -1
                        itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                        courseSelected = CType(Session.Item("training-class-list"), Array)(itemNumber)
                        Me.txtCapacity.Text = courseSelected.Capacity
                        'Me.txtCourseNumber.Text = courseSelected.Course.Number
                        Me.ddlTrainingCourse.SelectedValue = courseSelected.Course.Number.Trim()
                        If courseSelected.EndDateDisplay <> "TBD" Then
                            Me.txtEndDate.Text = courseSelected.EndDateDisplay
                        End If
                        If courseSelected.StartDateDisplay <> "TBD" Then
                            Me.txtStartDate.Text = courseSelected.StartDateDisplay
                        End If
                        Me.txtNotes.Text = courseSelected.Notes
                        Me.txtPartNumber.Text = courseSelected.PartNumber
                        Me.txtTime.Text = courseSelected.Time
                        Me.ddlLocation.SelectedValue = courseSelected.Location.Code
                        ddlLocation.Enabled = False
                        If courseSelected.StatusCode = "1" And courseSelected.Hide = "0" Then
                            rdoStatus.SelectedIndex = 0
                            btnDelete.Visible = True
                            btnActivate.Visible = False
                        ElseIf courseSelected.StatusCode = "1" And courseSelected.Hide = "1" Then
                            rdoStatus.SelectedIndex = 1
                            btnDelete.Visible = False
                            btnActivate.Visible = True
                        ElseIf courseSelected.StatusCode = "2" Then ' And courseSelected.Hide = "1" Then ' It could be Hide = 0 or 1
                            rdoStatus.SelectedIndex = 2
                            btnDelete.Visible = False
                            btnUpdate.Visible = False
                            btnActivate.Visible = False
                        End If
                        If Not Session("NewCourseSchedule") Is Nothing Then
                            Session.Remove("NewCourseSchedule")
                        End If
                    Else
                        btnUpdate.Text = "  Add  "
                        Me.txtPartNumber.BackColor = Color.Gray 'Added for fixing Bug# 471
                        rdoStatus.SelectedIndex = 0
                        If Not Session("NewCourseSchedule") Is Nothing Then
                            Session.Remove("NewCourseSchedule")
                        End If
                    End If




                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)

                End Try
            End If
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("training-class-list.aspx")
        End Sub


        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            If Not ValidateForm() Then
                ErrorLabel.Text = "Error entering class info:" + ErrorLabel.Text
                Return
            End If
            Dim bUpdate As Boolean = False
            Dim courseSelected As CourseSchedule
            Dim IsNullDate As Boolean = True
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnUpdate_Click"
                objPCILogger.Message = "Update Success."
                objPCILogger.EventType = EventType.Update
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("training-class-list") Is Nothing) Then
                    bUpdate = True
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-class-list"), Array)(itemNumber)
                ElseIf (btnUpdate.Text = "Update") And (Not Session("NewCourseSchedule") Is Nothing) Then
                    bUpdate = True
                    courseSelected = Session("NewCourseSchedule")
                Else
                    courseSelected = New CourseSchedule
                End If

                courseSelected.Capacity = Me.txtCapacity.Text
                courseSelected.Course.Number = Me.ddlTrainingCourse.SelectedValue
                If Me.txtEndDate.Text <> "" Then
                    courseSelected.EndDate = Me.txtEndDate.Text
                    IsNullDate = False
                End If
                If Me.txtStartDate.Text <> "" Then
                    courseSelected.StartDate = Me.txtStartDate.Text
                    IsNullDate = False
                End If

                courseSelected.Notes = Me.txtNotes.Text

                If bUpdate Then
                    courseSelected.PartNumber = Me.txtPartNumber.Text 'new class's partnumber will be generated automatically
                Else
                    courseSelected.PartNumber = "TRNEW"
                End If
                courseSelected.Time = Me.txtTime.Text
                courseSelected.Location.Code = Me.ddlLocation.SelectedValue

                Dim cm As New CourseManager
                If bUpdate = False Then
                    cm.AddCourseSchedule(courseSelected)
                    Dim classArray(CType(Session.Item("training-class-list"), Array).Length) As Sony.US.ServicesPLUS.Core.CourseSchedule
                    CType(Session.Item("training-class-list"), Array).CopyTo(classArray, 0)
                    classArray(classArray.Length - 1) = courseSelected
                    Session("training-class-list") = classArray
                    ErrorLabel.Text = "Class added successfuly."
                    rdoStatus.SelectedIndex = 0
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                    btnUpdate.Text = "Update"
                    Session("NewCourseSchedule") = courseSelected
                    txtPartNumber.Text = courseSelected.PartNumber
                Else
                    cm.UpdateCourseSchedule(courseSelected)
                    rdoStatus.SelectedIndex = 0
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                    If (Not Session("NewCourseSchedule") Is Nothing) Then
                        Session("NewCourseSchedule") = courseSelected
                    End If
                    ErrorLabel.Text = "Class updated successfuly."
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Update Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Sub populateCourseDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim courseData As DataSet = courseDataManager.GetClassRoomTrainingCourse()
                ddlTrainingCourse.DataTextField = "COURSENUMBER"
                ddlTrainingCourse.DataValueField = "COURSENUMBER"
                ddlTrainingCourse.DataSource = courseData.Tables(0)
                ddlTrainingCourse.DataBind()




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub populateLocationDropDown()
            Try
                Dim courseDataManager As New CourseDataManager
                Dim location() As CourseLocation = courseDataManager.GetLocations()
                For Each cl As CourseLocation In location
                    Me.ddlLocation.Items.Add(New ListItem(cl.City + ", " + cl.State, cl.Code))
                Next




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Dim cm As New CourseManager
            Dim courseSelected As CourseSchedule
            Dim objPCILogger As New PCILogger() '6524
            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnDelete_Click"
                objPCILogger.Message = "Delete Success."
                objPCILogger.EventType = EventType.Delete
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-class-list"), Array)(itemNumber)

                    'Dim IsDefaultLoc As Boolean = cm.CheckIsDefaultLocation(courseSelected.Course.Number, courseSelected.Location.Code)
                    'If IsDefaultLoc = False And courseSelected.StartDate.ToString.Trim() = "" Then
                    '    '-- cannot delete a default location class
                    '    ErrorLabel.Text = "Cannot hide a class with default location"
                    '    Return
                    'End If

                    cm.CancelCourseSchedule(courseSelected)
                    rdoStatus.SelectedIndex = 1
                    btnDelete.Visible = False
                    btnActivate.Visible = True
                ElseIf (Not Session("NewCourseSchedule") Is Nothing) Then
                    courseSelected = Session("NewCourseSchedule")

                    'Dim IsDefaultLoc As Boolean = cm.CheckIsDefaultLocation(courseSelected.Course.Number, courseSelected.Location.Code)
                    'If IsDefaultLoc = False And courseSelected.StartDate.ToString.Trim() = "" Then
                    '    '-- cannot delete a default location class
                    '    ErrorLabel.Text = "Cannot hide a class with default location"
                    '    Return
                    'End If

                    cm.CancelCourseSchedule(courseSelected)
                    rdoStatus.SelectedIndex = 1
                    btnDelete.Visible = False
                    btnActivate.Visible = True
                    Session("NewCourseSchedule") = courseSelected
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Delete Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Private Function ValidateForm() As Boolean
            Dim bValid = True

            Me.labelCourseNumber.CssClass = "Body"
            Me.labelCapacity.CssClass = "Body"
            Me.labelPartNumber.CssClass = "Body"

            Me.LabelCourseNumberStar.Text = ""
            Me.LabelCapacityStar.Text = ""
            'Me.LabelpartNumberStar.Text = ""

            'If Me.txtCourseNumber.Text = "" Then
            If Me.ddlTrainingCourse.SelectedValue = "" Then
                Me.labelCourseNumber.CssClass = "redAsterick"
                Me.LabelCourseNumberStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If Me.txtCapacity.Text = "" Then
                Me.labelCapacity.CssClass = "redAsterick"
                Me.LabelCapacityStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            If Me.txtStartDate.Text.Trim() = "" Then
                bValid = False
                ErrorLabel.Text = "Please enter start date"
            Else
                If Me.txtEndDate.Text.Trim() = "" Then
                    bValid = False
                    ErrorLabel.Text = "Please enter end date"
                End If
            End If

            If Me.txtEndDate.Text.Trim() = "" And Me.txtStartDate.Text.Trim() <> "" Then
                bValid = False
                ErrorLabel.Text = "Please enter end date"
            Else
                If Me.txtEndDate.Text.Trim() <> "" And Me.txtStartDate.Text.Trim() = "" Then
                    bValid = False
                    ErrorLabel.Text = "Please enter start date"
                End If
            End If

            Dim myCI As CultureInfo
            myCI = CultureInfo.CreateSpecificCulture("en-GB")
            myCI.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
            myCI.DateTimeFormat.DateSeparator = "/"
            Dim mstartDate As DateTime
            Dim mendDate As DateTime
            mstartDate = Convert.ToDateTime(txtStartDate.Text.ToString(), myCI.DateTimeFormat)
            mendDate = Convert.ToDateTime(txtEndDate.Text.ToString(), myCI.DateTimeFormat)
            If mstartDate < Date.Now Or mendDate < mstartDate Then
                bValid = False
                ErrorLabel.Text = "Start date must be greater than today and End date must be equal to or greater than Start date."
            End If

            Try
                Integer.Parse(Me.txtCapacity.Text)
                bValid = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                bValid = False
            End Try

            Return bValid
        End Function

        Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivate.Click
            Dim cm As New CourseManager
            Dim courseSelected As CourseSchedule
            Dim objPCILogger As New PCILogger() '6524
            Try

                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnActivate_Click"
                objPCILogger.Message = "Activate Success."
                objPCILogger.EventType = EventType.Update
                '6524 end

                If (Not Request.QueryString("LineNo") Is Nothing) Then
                    Dim itemNumber As Integer = -1
                    itemNumber = Convert.ToInt16(Request.QueryString("LineNo"))
                    courseSelected = CType(Session.Item("training-class-list"), Array)(itemNumber)

                    'Dim IsDefaultLoc As Boolean = cm.CheckIsDefaultLocation(courseSelected.Course.Number, courseSelected.Location.Code)
                    'If IsDefaultLoc = False Then
                    '    courseSelected.IsDefaulLocation = False
                    'ElseIf IsDefaultLoc = True Then
                    '    courseSelected.IsDefaulLocation = True
                    'End If

                    cm.ActivateCourseSchedule(courseSelected)
                    rdoStatus.SelectedIndex = 0
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                ElseIf (Not Session("NewCourseSchedule") Is Nothing) Then
                    courseSelected = Session("NewCourseSchedule")

                    'Dim IsDefaultLoc As Boolean = cm.CheckIsDefaultLocation(courseSelected.Course.Number, courseSelected.Location.Code)
                    'If IsDefaultLoc = False Then
                    '    courseSelected.IsDefaulLocation = False
                    'ElseIf IsDefaultLoc = True Then
                    '    courseSelected.IsDefaulLocation = True
                    'End If

                    cm.ActivateCourseSchedule(courseSelected)
                    rdoStatus.SelectedIndex = 0
                    btnDelete.Visible = True
                    btnActivate.Visible = False
                    Session("NewCourseSchedule") = courseSelected
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Activate Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub
    End Class

End Namespace
