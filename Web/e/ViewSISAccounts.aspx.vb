Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process


Namespace SIAMAdmin


    Partial Class ViewSISAccounts
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub
#End Region



#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Not Page.IsPostBack Then controlMethed(sender, e)
        End Sub

        Private Sub DataGrid1_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DataGrid1.ItemCreated
            e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
        End Sub

#End Region



#Region "   Method  "


#Region "       Control Method      "

        Private Sub controlMethed(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim SiamIdentity As String = String.Empty
                Dim LDapID As String = String.Empty '7086
                'If Not Request.QueryString("Identity") Is Nothing Then SiamIdentity = Request.QueryString("Identity")'7086
                'SiamIdentity = Request.QueryString("Identity")'7086
                '7086 Starts
                If Not Request.QueryString("LdapId") Is Nothing Then
                    Dim cm As New CustomerManager '7086
                    LDapID = Request.QueryString("LdapId")
                    SiamIdentity = cm.GetSiamIdByLdapId(LDapID)
                End If
                '7086 Ends


                If SiamIdentity <> String.Empty Then
                    Dim thisCustomer As Customer = New CustomerManager().GetCustomer(SiamIdentity)

                    'CustomerSubNav.SiamIdentity = SiamIdentity.ToString()'7086
                    CustomerSubNav.LDapID = LDapID.ToString() '7086


                    If Not thisCustomer Is Nothing Then
                        populateDataGrid(thisCustomer)
                    End If
                End If

                writeScriptsToPage()

            Catch ex As Exception
                ErrorLabel.Text = ex.Message.ToString()
            End Try
        End Sub


#End Region


#Region "       Populate the datagrid       "


        Private Sub populateDataGrid(ByRef thisCustomer As Customer)
            Try
                Dim showNoAccountMessage As Boolean = False
                If Not thisCustomer.SISLegacyBillToAccounts Is Nothing And thisCustomer.SISLegacyBillToAccounts.Length > 0 Then
                    Dim theAccounts As Account() = getSISAccounts(thisCustomer)

                    If Not theAccounts Is Nothing Then
                        DataGrid1.DataSource = theAccounts
                        DataGrid1.AllowPaging = False
                        If theAccounts.Length > DataGrid1.PageSize Then DataGrid1.AllowPaging = True
                        DataGrid1.DataBind()
                        DataGrid1.Visible = True
                    Else
                        showNoAccountMessage = True     '-- show message - no account
                    End If
                Else
                    showNoAccountMessage = True         '-- show message - no account
                End If

                If showNoAccountMessage Then ErrorLabel.Text = "No SIS accounts."
            Catch ex As Exception
                ErrorLabel.Text = ex.Message.ToString()
            End Try
        End Sub

#End Region


#Region "       Get SIS Accounts        "


        Private Function getSISAccounts(ByRef thisCustomer As Customer) As Account()
            Dim returnValue As Account() = Nothing
            Dim thisCustMng As New CustomerManager
            Dim thisArrayList As New ArrayList
            Dim thisAccount As Account = Nothing

            Try
                If thisCustomer IsNot Nothing Then
                    For Each thisAccount In thisCustomer.SISLegacyBillToAccounts
                        Throw New NotImplementedException()
                        'thisCustMng.PopulateLegacyAccount(thisAccount)
                        'If thisAccount.Validated Then
                        If Not isDuplicateAccountNumber(thisAccount.AccountNumber, thisArrayList) And thisAccount.Address.Line1 <> String.Empty Then
                            thisArrayList.Add(thisAccount)
                        End If
                        'End If
                    Next

                    If thisArrayList.Count > 0 Then returnValue = thisArrayList.ToArray(thisAccount.GetType)
                End If
            Catch ex As Exception
                ErrorLabel.Text = ex.Message.ToString()
            End Try

            Return returnValue
        End Function

#End Region


#Region "       Helper Methods      "

        '-- style for the datagrid -- this should really except a datagrid as a parameter
        Private Sub ApplyDataGridStyle()
            DataGrid1.ApplyStyle(GetStyle(530))
            DataGrid1.PageSize = 15
            DataGrid1.AutoGenerateColumns = False
            DataGrid1.AllowPaging = False
            DataGrid1.GridLines = GridLines.Horizontal
            DataGrid1.AllowSorting = True
            DataGrid1.CellPadding = 3
            DataGrid1.PagerStyle.Mode = PagerMode.NumericPages
            DataGrid1.ItemStyle.Font.Size = FontUnit.Point(8)
            DataGrid1.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub


        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub



        '-- makes sure the passed number does not exist in the passed arraylist --
        Private Function isDuplicateAccountNumber(ByVal thisAccountNumber As String, ByRef thisArrayList As ArrayList)
            Dim returnValue As Boolean = False
            Try
                If Not thisAccountNumber = String.Empty And Not thisArrayList Is Nothing Then
                    For Each thisAccount As Account In thisArrayList
                        If thisAccount.AccountNumber = thisAccountNumber Then
                            returnValue = True
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                ErrorLabel.Text = ex.Message.ToString()
            End Try
            Return returnValue
        End Function


#End Region

#End Region
    End Class

End Namespace
