<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.AccountValidationQueue" CodeFile="AccountValidationQueue.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Account Validation Queue</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body  >
		<form id="Form1" method="post" runat="server">
			<table width="500" align="center" border="0">
				<tr>
					<td height="30" class="PageHead" align="center" width="500">Account Validation</td>
				</tr>
				<tr>
					<td height="5" align="center"><asp:label id="Label1" runat="server" ForeColor="Red" 
                            CssClass="Body" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<%--<td height="10" align="center">
						<asp:Label id="ExplainLabel" runat="server" CssClass="Body" EnableViewState="False">Click on a user to validate the account.</asp:Label>
					</td>--%>
				</tr>
				<tr>
					<td align="center" width="500">
						<asp:datagrid id="DataGrid1"  runat="server">
							<Columns>
								<asp:BoundColumn DataField="UpdateDate" SortExpression="Date Registered" HeaderText="Date Registered">
									<%--<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UPDATEDATE") %>' NavigateUrl='<%#"AccountValidation.aspx?Identity="+DataBinder.Eval(Container.DataItem, "SIAMIdentity").ToString()%>' ID="Hyperlink1">
										</asp:HyperLink>
									</ItemTemplate>--%>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="EmailAddress" SortExpression="EmailAddress" HeaderText="Email Address"></asp:BoundColumn>
								<asp:BoundColumn DataField="CompanyName" SortExpression="Company" HeaderText="Company"></asp:BoundColumn>
								<asp:TemplateColumn SortExpression="Validate" HeaderText="Validate">
									<ItemTemplate>
										<%--<asp:HyperLink runat="server" Text='Validate' NavigateUrl='<%#"AccountValidation.aspx?Identity="+DataBinder.Eval(Container.DataItem, "SIAMIdentity").ToString()%>' ID="Hyperlink2">
										</asp:HyperLink>--%><%--7086--%>
										<asp:HyperLink runat="server" Text='Validate' NavigateUrl='<%#"AccountValidation.aspx?LdapId="+DataBinder.Eval(Container.DataItem, "LdapID").ToString()%>' ID="Hyperlink2">
										</asp:HyperLink>
										<%--<asp:HyperLink runat="server" Text='Modify' NavigateUrl='<%#"CustomerInfo.aspx?Identity="+DataBinder.Eval(Container.DataItem, "SIAMIdentity").ToString()%>' ID="Hyperlink1">
										</asp:HyperLink>--%>
									</ItemTemplate>
					         </asp:TemplateColumn>
					         <asp:TemplateColumn SortExpression="Modify" HeaderText="Modify">
									<ItemTemplate>
										<%--<asp:HyperLink runat="server" Text='Validate' NavigateUrl='<%#"AccountValidationQueue.aspx?Identity="+DataBinder.Eval(Container.DataItem, "SIAMIdentity").ToString()%>' ID="Hyperlink2">
										</asp:HyperLink>--%>
										<asp:HyperLink runat="server" Text='Modify' NavigateUrl='<%#"CustomerInfo.aspx?LdapId="+DataBinder.Eval(Container.DataItem, "LdapId").ToString()%>' ID="Hyperlink1">
										</asp:HyperLink><%--7086--%>
									</ItemTemplate>
					         </asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
				<tr>
					<td align="center"><asp:label id="NoApplicationsLabel" runat="server" ForeColor="Red" CssClass="Body" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<td height="20"></td>
				</tr>				
				<tr>
					<td align="center"></td>
				</tr>
				<tr>
					<td height="5">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
