Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports System.Data

Namespace SIAMAdmin

    Partial Class ViewProcessorErrorLog
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)
            InitializeComponent()

        End Sub




#End Region

#Region "   Events  "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'ErrorLabel.Visible = False
                ErrorLabel.Text = String.Empty
                If Not IsPostBack Then
                    dgShippingMethod.CurrentPageIndex = 0
                    InitializeControls(False)
                    PopulateProcessName()
                    'PopulateDataGrid()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub AddNewUSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                If txtDate.Text.Trim() = String.Empty Then
                    ErrorLabel.Text = "Invalid date, please enter date in mm/dd/yyyy format"
                    Exit Sub
                ElseIf IsDate(Format(DateTime.ParseExact(txtDate.Text.ToString(), "mm/dd/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat))) Then
                    dgShippingMethod.CurrentPageIndex = 0
                    PopulateDataGrid()
                Else
                    ErrorLabel.Text = "Invalid date, please enter date in mm/dd/yyyy format"
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Protected Sub dgShippingMethod_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingMethod.ItemCommand
            Try
                If e.CommandName = "Show" Then
                    Dim dgInnerSAPNAME As DataGrid
                    dgInnerSAPNAME = e.Item.FindControl("dgInnerSAPNAME")
                    Dim lnkbtn As LinkButton
                    lnkbtn = e.Item.FindControl("lnkBtnEdit")
                    Dim innertableHTML As HtmlTable = CType(e.Item.FindControl("innertable"), HtmlTable)
                    innertableHTML.Visible = True
                    dgInnerSAPNAME.Visible = True
                    dgInnerSAPNAME.DataSource = PopulateInnerDataGrid(Convert.ToInt32(e.Item.Cells(0).Text.ToString()))
                    dgInnerSAPNAME.DataBind()
                    lnkbtn.Visible = False
                    'dgShippingMethod.DataBind()
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub dgShippingMethod_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShippingMethod.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    lnkbtn = e.Item.FindControl("lnkBtnEdit")
                    If Not lnkbtn Is Nothing Then
                        If Convert.ToInt16(e.Item.Cells(6).Text.ToString()) > 0 Then
                            lnkbtn.Visible = True
                        Else
                            lnkbtn.Visible = False
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Sub PopulateProcessName()
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsPROCESSNAME As DataSet = objSIAMOperation.GETPROCESSNAME()
            drpProcessName.SelectedIndex = -1
            drpProcessName.DataTextField = "PROCESSNAME"
            drpProcessName.DataValueField = "PROCESSNAME"
            drpProcessName.DataSource = dsPROCESSNAME.Tables(0)
            drpProcessName.DataBind()
        End Sub
        Private Sub PopulateDataGrid()
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsShippingMethod As DataSet = objSIAMOperation.GETPROCESSLOG(drpProcessName.SelectedItem.Text, txtDate.Text)
            dgShippingMethod.SelectedIndex = -1
            dgShippingMethod.DataSource = dsShippingMethod.Tables(0)
            dgShippingMethod.DataBind()
        End Sub
        Private Function PopulateInnerDataGrid(ByVal ProcessErrorId As Int32) As DataSet
            Dim objSIAMOperation As SIAMOperation = New SIAMOperation()
            Dim dsInnerErrorData As DataSet = objSIAMOperation.GETPROCESSORERROR(ProcessErrorId)
            Return dsInnerErrorData
        End Function
        Private Sub InitializeControls(ByVal Enable As Boolean)

        End Sub
#End Region

        Protected Sub dgShippingMethod_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgShippingMethod.PageIndexChanged
            Try
                dgShippingMethod.CurrentPageIndex = e.NewPageIndex
                PopulateDataGrid()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Protected Sub cldrDate_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cldrDate.SelectionChanged
        '    txtDate.Text = cldrDate.SelectedDate.ToShortDateString()
        'End Sub
    End Class

End Namespace


