Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class srhRole
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click

            Dim oUsrSecManager As New UserRoleSecurityManager
            Dim col_user_role() As Sony.US.ServicesPLUS.Core.UserRole
            Dim sCriteria As String = "%"
            Dim iStatus As Integer = 0

            If (txtRoleName.Text.Trim.Length = 0) Then
                sCriteria = "%"
            ElseIf (txtRoleName.Text.Trim.Length > 0) Then
                sCriteria = txtRoleName.Text + "%"
            End If

            If chkActive.Checked = True Then
                iStatus = 1
            Else
                iStatus = 0
            End If

            Try
                col_user_role = oUsrSecManager.GetUserRoles(sCriteria, iStatus)

                If col_user_role.Length > 0 Then
                    ApplyDataGridStyle()
                    dgSearchRole.DataSource = col_user_role
                    dgSearchRole.DataBind()
                    dgSearchRole.Visible = True
                Else
                    dgSearchRole.Visible = False
                    ErrorLabel.Text = "No Result(s) found."
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()

            'dgSearchRole.ApplyStyle(GetStyle(500))
            dgSearchRole.PageSize = 30
            dgSearchRole.AutoGenerateColumns = False
            dgSearchRole.AllowPaging = False
            dgSearchRole.GridLines = GridLines.Horizontal
            dgSearchRole.AllowSorting = True
            dgSearchRole.CellPadding = 3
            dgSearchRole.PagerStyle.Mode = PagerMode.NumericPages
            dgSearchRole.HeaderStyle.Font.Size = FontUnit.Point(8)
            dgSearchRole.HeaderStyle.ForeColor = Color.GhostWhite
            dgSearchRole.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgSearchRole.ItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchRole.AlternatingItemStyle.Font.Size = FontUnit.Point(8)
        End Sub

        Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
            txtRoleName.Text = ""
            chkActive.Checked = False
            dgSearchRole.Visible = False
        End Sub

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub
    End Class
End Namespace