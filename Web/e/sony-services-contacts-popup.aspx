<%@ Page Language="VB" AutoEventWireup="false" CodeFile="sony-services-contacts-popup.aspx.vb" Inherits="sony_services_contacts_popup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Sony Professional Services Contact Information</title>
</head>
<body>
    <form id="form1" runat="server">
        <table width="710" border="0">
            <tr>
                <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                    align="right" valign="top">
                    <br />
                    <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                </td>
                <td valign="top" bgcolor="#363d45">
                    <table width="246" border="0">
                        <tr>
                            <td colspan="3">
                                <img height="24" src="images/spacer.gif" width="246"></td>
                        </tr>
                        <tr>
                            <td width="5">&nbsp;</td>

                            <td width="236"><span class="memberName">
                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server"></ServicePLUSWebApp:PersonalMessage>
                            </span></td>
                            <td width="5">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td bgcolor="#f2f5f8">
                    <table width="464" border="0">
                        <tr>
                            <td width="20">
                                <img height="20" src="images/spacer.gif" width="20"></td>
                            <td>
                                <h2 class="headerTitle">Contact Us</h2>
                            </td>
                            <%--<td><img src="./images/sp_int_subhd_contactUs.gif"></td>--%>
                        </tr>
                    </table>
                </td>
                <td bgcolor="#99a8b5">&nbsp;</td>
            </tr>
            <tr height="9">
                <td bgcolor="#f2f5f8">
                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464"></td>
                <td bgcolor="#99a8b5">
                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246"></td>
            </tr>
        </table>
        <div id="tableContact" runat="server"></div>
        <table width="50%">
            <tr>
                <td colspan="3" align="center">
                    <a href="javascript:;" id="btnClose" target="_blank" width="220px" onclick="javascript:window.close();return false;">
                        <img src="images/sp_int_closeWindow_btn.gif" border="0" alt="Close Window"></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
