<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.WebMethodsTest"
    CodeFile="WebMethodsTest.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<%@ Register Src="SaleOrder.ascx" TagName="SaleOrder" TagPrefix="uc1" %>
<%@ Register Src="CancelOrder.ascx" TagName="CancelOrder" TagPrefix="uc2" %>
<%@ Register Src="CustomerLookUp.ascx" TagName="CustomerLookUp" TagPrefix="uc3" %>
<%@ Register Src="PartInquiry.ascx" TagName="PartInquiry" TagPrefix="uc4" %>
<%@ Register Src="OrderInquiry.ascx" TagName="OrderInquiry" TagPrefix="uc5" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>WebMethods Test Page</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <style type="text/css">
        Body
        {
            filter: chroma(color=#FFFFFF);
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
    </style>

    <script type="text/javascript">
        function ShowHideSentData() {
            alert('hi');
            if (document.getElementById("aShowHideDataSent").innerText === "Show Data Sent") {
                document.getElementById("aShowHideDataSent").innerText = "Hide Data Sent";
                alert(document.getElementById("tdDataSent"));
                document.getElementById("tdDataSent").style.visibility = 'visible';
            }
            else {
                document.getElementById("aShowHideDataSent").innerText = "Show Data Sent";
                document.getElementById("tdDataSent").style.visibility = 'hidden';
            }
        }
    </script>

</head>
<body topmargin="0">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table width="100%" align="left" border="0" bordercolor="gray" height="100%">
        <tr height="5%">
            <td align="center" colspan="3" class="PageHead" width="100%">
                Web Methods Test
            </td>
        </tr>
        <tr>
            <td colspan="3" height="5%">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <SPS:SPSLabel ID="lblError" CssClass="redAsterick" Visible="true" runat="server"></SPS:SPSLabel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSendData" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr height="90%">
            <td align="center" valign="top" width="35%">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <table width="100%" height="100%">
                            <tr width="100%" height="98%">
                                <td align="left" colspan="2" valign="top">
                                    <table width="100%" runat="server" id="controlList" cellpadding="0">
                                        <tr>
                                            <td class="BodyHead" colspan="2">
                                                <table class="TableDataText">
                                                    <%--<tr>
                                                        <td>
                                                            <SPS:SPSLabel ID="lblWebService" CssClass="BodyHead" Text="Environment" runat="server"></SPS:SPSLabel>
                                                        </td>
                                                        <td>--%>
                                                            <%--<asp:RadioButton ID="rbtnCurrentEnv" Checked="true" Text="e" runat="server" 
                                                        GroupName="MyGroup"   AutoPostBack="True" /></td><td>
                                                    <asp:RadioButton ID="rbtnCustom" Text="Custom"  runat="server" GroupName="MyGroup" 
                                                        AutoPostBack="True" /></td><td>
                                                <asp:TextBox runat="server" ID="txtCustomEnv"></asp:TextBox>--%>
                                                           <%-- <asp:DropDownList ID="ddlEnviroment" runat="server" AutoPostBack="True">
                                                            </asp:DropDownList>--%>
                                                         <%--   : &nbsp;<asp:Label ID="lbl_env" runat="server"  ></asp:Label>
                                                        </td>
                                                        </tr>--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%-- <SPS:SPSLabel ID="lblWebService" CssClass="BodyHead" Text="Web Service" runat="server"></SPS:SPSLabel>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="BodyHead" colspan="2">
                                    Interface
                                </td>
                            </tr>
                            <tr>
                                <td class="TableData" colspan="2">
                                    <asp:DropDownList ID="cmbInterface" runat="server" Width="99%" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                          
                            <tr>
                                <td>
                                    <uc1:SaleOrder ID="SaleOrder1" runat="server" Visible="false" />
                                    <uc2:CancelOrder ID="CancelOrder1" runat="server" Visible="false" />
                                    <uc3:CustomerLookUp ID="CustomerLookUp1" runat="server" Visible="false" />
                                    <uc4:PartInquiry ID="PartInquiry1" runat="server" Visible="false" />
                                    <uc5:OrderInquiry ID="OrderInquiry1" runat="server" Visible="false" />
                                </td>
                            </tr>
                        </table>
                        </td> </tr>
                        <tr width="100%" height="2%">
                            <td align="right" valign="middle">
                                <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel2" runat="server">
                                    <ProgressTemplate>
                                        <span class="Body">Please wait<img src="../images/progbar.gif" /></span>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnSendData" CssClass="BodyHead" Text="Send Data" runat="server" />
                            </td>
                        </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td align="center" valign="top" width="5px" bgcolor="white">
                &nbsp;
            </td>
            <td align="center" valign="top" width="65%">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" RenderMode="Inline">
                    <ContentTemplate>
                        <table width="100%" height="100%">
                            <tr>
                                <td colspan="2" valign="top" id="tdDataSent" runat="server" visible="false" class="BodyHead"
                                    height="100px">
                                    <div id="divName" style="overflow: auto; width: 100%; height: 100%; scrollbar-face-color: #c9d5e6;
                                        scrollbar-shadow-color: #632984; scrollbar-highlight-color: #632984; scrollbar-3dlight-color: #130919;
                                        scrollbar-darkshadow-color: #130919; scrollbar-track-color: #130919; scrollbar-arrow-color: black;">
                                        <SPS:SPSLabel ID="lblDataSent" CssClass="redAsterick" runat="server" Text="No data sent to Web Service."></SPS:SPSLabel>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="BodyHead" align="left" height="2%" width="80%">
                                    &nbsp;
                                    <SPS:SPSLabel ID="lblDataReturned" CssClass="BodyHead" Text="Data Returned" runat="server"></SPS:SPSLabel>
                                </td>
                                <td align="right">
                                    <%--<a href="#" id="aShowHideDataSent" onclick="ShowHideSentData(); return false;" class="redAsterick" 
                                        style="color: #FF0000">Show Data Sent</a>--%>
                                    <asp:LinkButton runat="server" ID="lnkShowHideDataSent" ForeColor="Red" CssClass="redAsterick"
                                        Text="Show Data Sent"></asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%" valign="top" height="98%">
                                    <SPS:SPSTextBox ID="txtDataReturned" runat="server" CssClass="BodyHead" Height="99%"
                                        TextMode="MultiLine" Width="100%" Rows="35"></SPS:SPSTextBox>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSendData" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="2px" bgcolor="black">
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
