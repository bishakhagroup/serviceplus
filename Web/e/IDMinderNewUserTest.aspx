<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" smartnavigation="true" CodeFile="IDMinderNewUserTest.aspx.vb" Inherits="SIAMAdmin.B2BBindTest"  CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>B2B test Tool</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
	</HEAD>
	<body bgColor="#ffffff"  class="body">
		<form id="Form1" method="post" runat="server">
			<table align="center" border="0" width="100%">
				<tr>
					<td class="PageHead" align="center" colSpan="1" height="30" rowSpan="1">B2B Test 
                        Tool</td>
				</tr>
				
				<tr  bgcolor="#c9d5e6" >
					<td align=left height=20>&nbsp;<asp:label id="ErrorLabel" runat="server" Font-Size="X-Small" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></td>
				</tr>
				<TR>
					<TD  align="center" height="50">&nbsp;
						 <div id="div1"  style="BORDER-RIGHT: #c9d5e6 thin solid; width:60%; 
								    BORDER-TOP: #c9d5e6 thin solid; BORDER-LEFT: #c9d5e6 thin solid; BORDER-BOTTOM: #c9d5e6 thin solid">
								    
                    
                    <table width="100%" border=0 cellpadding=0 cellspacing=2>
                     <%--  <tr bgcolor="#c9d5e6"><td align=left  width=30% valign="middle">
                            <asp:Label ID="Label2" runat="server" CssClass="BodyHead" Text="Environment"></asp:Label></td>
                            <td width="80%" valign="middle" align="left">
                            
                                     <asp:Label ID="lbl_envname" runat="server" CssClass="BodyHead" ></asp:Label></td>
                                                  
                            </td></tr>--%>
                        <tr><td align=left  width=30%>
                            <asp:Label ID="lblHost" runat="server" CssClass="BodyHead" Text="URL"></asp:Label></td>
                            <td width="80%">
                                <SPS:SPSTextBox ID="txtURL" runat="server" CssClass="body" Width="100%"></SPS:SPSTextBox>
                            </td></tr>
                        <tr  bgcolor="#c9d5e6" ><td align=left valign="middle">
                            <asp:Label ID="lblHost0" runat="server" CssClass="BodyHead" 
                                Text="Admin ID"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtadminID" runat="server" CssClass="body" Width="100%" 
                                        MaxLength="5"></SPS:SPSTextBox>
                            </td></tr>
                        <tr><td align=left>
                            <asp:Label ID="lblHost1" runat="server" CssClass="BodyHead" Text="User Name"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtUserName" runat="server" CssClass="body" Width="100%"></SPS:SPSTextBox></td></tr>
                        <tr  bgcolor="#c9d5e6" ><td align=left>
                            <asp:Label ID="lblHost2" runat="server" CssClass="BodyHead" 
                                Text="Password"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtPassword" runat="server" CssClass="body" Width="100%"></SPS:SPSTextBox></td></tr>
                        <tr><td align=left>
                            <asp:Label ID="lblHost3" runat="server" CssClass="BodyHead" 
                                Text="Number of Try"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtNumberOfTries" runat="server" CssClass="body" Width="20px" 
                                        MaxLength="1"></SPS:SPSTextBox></td></tr>
                        <tr bgcolor="#c9d5e6"><td align=left>
                            <asp:Label ID="lblHost6" runat="server" CssClass="BodyHead" 
                                Text="AccountType"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtAccountType" runat="server" CssClass="body" Width="100px" 
                                        MaxLength="5"></SPS:SPSTextBox>
                            </td></tr>
                        <tr><td align=left>
                            <asp:Label ID="lblHost7" runat="server" CssClass="BodyHead" 
                                Text="DUNS"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtDuns" runat="server" CssClass="body" Width="100px" 
                                        MaxLength="5"></SPS:SPSTextBox>
                            </td></tr>
                        <tr bgcolor="#c9d5e6"><td align=left>
                            <asp:Label ID="lblHost8" runat="server" CssClass="BodyHead" 
                                Text="Account"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtAccount" runat="server" CssClass="body" Width="100px" 
                                        MaxLength="5"></SPS:SPSTextBox>
                            </td></tr>
                        <tr><td align=left>
                            <asp:Label ID="lblHost9" runat="server" CssClass="BodyHead" 
                                Text="SonyContact"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtSonyContact" runat="server" CssClass="body" Width="200px" 
                                        MaxLength="5"></SPS:SPSTextBox>
                            </td></tr>
                        <tr bgcolor="#c9d5e6"><td align=left>
                            <asp:Label ID="lblEmail" runat="server" CssClass="BodyHead" 
                                Text="Email to be Register"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="SPStxtEmail" runat="server" CssClass="body" Width="200px"></SPS:SPSTextBox>
                            </td></tr>
                        <tr bgcolor="#c9d5e6"><td align=left>
                            <asp:Label ID="lblHost4" runat="server" CssClass="BodyHead" Text="Sending Data"></asp:Label></td>
                                <td align="left"  height=160  style="overflow: auto">
                                    <div style="overflow: auto; height:100%; width:100%" runat=server id="txtSendingData">
                                        
                                    </div>
                                    
                                </td></tr>                                        
                        <tr   ><td align=left valign="top">
                            <asp:Label ID="Label1" runat="server" CssClass="BodyHead" 
                                Text="Result"></asp:Label></td>
                                <td align="left" valign="middle" >
                                <SPS:SPSTextBox ID="txtResult" runat="server" CssClass="body" Width="200px" 
                                MaxLength="5"></SPS:SPSTextBox></td></tr>
                        <tr>
                       
                        <td style="height: 21px">&nbsp;</td>
                        <td style="height: 21px">
                            <%--<asp:Button ID="btnBind" runat="server" CssClass="BodyHead" Width="100px" Text="Bind"  />--%>
                            <asp:Button ID="Bind" runat="server" Text="Bind" CssClass="BodyHead" 
                                Width="100px" />
                            </td></tr>
                    </table>					
					</div></TD>
				</TR>
				
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				</table>
		</form>
	</body>
</HTML>