﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomerLookUp.ascx.vb" Inherits="e_CustomerLookUp" %>
<table>
    <tr>
        <td>
            <SPS:SPSLabel ID="lblSAPPayerAccountNumber" CssClass="BodyHead" runat="server">Select Method</SPS:SPSLabel>
        </td>
        <td>
            <asp:DropDownList ID="cmMethod" runat="server" AutoPostBack="True">
                <asp:ListItem Text="getPayerDetails" Value="getPayerDetails" Selected="True"></asp:ListItem>
                <asp:ListItem Text="getShippingInformation" Value="getShippingInformation"></asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="getPayerDetails" runat="server">
                <table>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="SPSLabel1" CssClass="BodyHead" runat="server">Company Name</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtCompanyName" CssClass="BodyHead" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="SPSLabel2" CssClass="BodyHead" runat="server">SAP Payer Account Numbers</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtSAPPayerAccountNumbers" runat="server" CssClass="BodyHead" TextMode="MultiLine"
                                Height="220px" Width="100px"></SPS:SPSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="getShippingInformation" visible="false" runat="server">
                <table>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="SPSLabel3" CssClass="BodyHead" runat="server">SAP Sold To Account Number</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtAccountNumber" CssClass="BodyHead" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <%-- Sasikumar WP SPLUS_WP014--%>
            <div id="GlobalSNA">
                <table>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="lblDistributionChannel" CssClass="BodyHead" runat="server">Distribution Channel</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtDistributionChannel" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="SPSDivision" CssClass="BodyHead" runat="server">Division</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtDivision" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <SPS:SPSLabel ID="SPSSalesOrganization" CssClass="BodyHead" runat="server">SalesOrganization</SPS:SPSLabel>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtSalesOrganization" runat="server"></SPS:SPSTextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>
