<%@ Reference Page="~/e/Utility.aspx"  %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="True" CodeFile="LogFileViewer.aspx.vb"  Inherits="SIAMAdmin.LogFileViewer" ValidateRequest="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Log File Viewer</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
</head>
<body >
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table id="Table1" align="center" border="0" height="98%" width="100%">
        <tr>
            <td class="PageHead" align="center" colspan="1" height="25" rowspan="1">
                Log File Viewer<br />
            </td>
        </tr>
       
        <tr height="100%">
            <td valign="top" align="center" height="100%">
                
               <div id="div1" style="border-right: #c9d5e6 thin solid; width: 98%; height: 98%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                  <asp:UpdatePanel ID="UpdatePanel2" runat="server" ChildrenAsTriggers="true" RenderMode="Inline" >
                      <ContentTemplate>
                        <table id="Table2" width="100%" height="5%" border="0">
                            <tr>
                                <td colspan="4">
                                        <asp:label id="ErrorLabel" runat="server" CssClass="redAsterick"></asp:label>
                                </td>
                            </tr>
                            <tr  bgcolor="#c9d5e6" valign="top" height="98%">
                                <td align="Left" width="5%">
                                    <asp:Label ID="labelFileCreatedDate" runat="server" CssClass="BodyCopy">Date</asp:Label>
                                </td>
                                <td style="height: 7px">
                                    <asp:DropDownList ID="ddlFileCreatedDate" runat="server" AutoPostBack=true  
                                        CssClass="Body" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td align="Left" width="5%">
                                    <asp:Label ID="lblFiles" runat="server" CssClass="BodyCopy">Files</asp:Label>
                                </td>
                                <td style="height: 7px">
                                   
                                    <asp:DropDownList ID="ddlFileNames" runat="server" CssClass="Body" AutoPostBack="True" 
                                        Width="360px" Enabled="False">
                                    </asp:DropDownList>
                                   
                                </td>
                            </tr>
                        </table>
                        
                        <table width="100%" height="95%" border="0">
                        <tr><td height="2px"></td></tr>
                        <tr style="background-color:#c9d5e6;color: 666666" height="2%">
                                <td>
                                File Content
                                </td>
                        </tr>
                        <tr height="99%">
                            <td valign="top" align="left">
                                 <SPS:SPSTextBox ID="txtFileContent" 
                                    style="background-color:#FFFFFF;color:#000000;font-size: 11px"  runat="server" height="100%" Width="100%" 
                                    TextMode="MultiLine" BorderStyle="None"></SPS:SPSTextBox>
                            </td>
                        </tr>
                      </table>
                  </ContentTemplate>
                  </asp:UpdatePanel>
                </div>
                 
            </td>
        </tr>
        
        
    </table>
    </form>
</body>
</html>
