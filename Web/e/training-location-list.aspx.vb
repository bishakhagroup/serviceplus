Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports System.Drawing

Namespace SIAMAdmin

    Partial Class training_location_list
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    writeScriptsToPage()
                    Dim cdm As New CourseDataManager
                    Dim css As CourseLocation() = cdm.GetLocations
                    Session.Add("training-location-list", css)
                    Me.locationDataGrid.DataSource = css
                    Me.locationDataGrid.DataBind()


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Sub locationDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                locationDataGrid.CurrentPageIndex = e.NewPageIndex
                Me.locationDataGrid.DataSource = Session("training-location-list")
                Me.locationDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            locationDataGrid.ApplyStyle(GetStyle(520))
            locationDataGrid.AllowPaging = True
            locationDataGrid.PageSize = 10
            locationDataGrid.AutoGenerateColumns = False
            locationDataGrid.GridLines = GridLines.Horizontal
            locationDataGrid.AllowSorting = True
            locationDataGrid.CellPadding = 3
            locationDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            locationDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            locationDataGrid.PagerStyle.Height = Unit.Pixel(10)
            locationDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            locationDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            locationDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            locationDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            locationDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            locationDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            locationDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub locationDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles locationDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub locationDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles locationDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(0).FindControl("lnkLocation"), HyperLink)
                    hyperLink.NavigateUrl = "training-location-edit.aspx?LineNo=" + (e.Item.ItemIndex() + locationDataGrid.CurrentPageIndex * locationDataGrid.PageSize).ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Response.Redirect("training-location-edit.aspx")
        End Sub
    End Class

End Namespace
