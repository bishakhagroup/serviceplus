<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin._default" CodeFile="default.aspx.vb"
    CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<%@ Register Src="SiamSideNav_Menu.ascx" TagName="SiamSideNav_Menu" TagPrefix="uc1" %>
<%@ Register TagPrefix="SIAMAdmin" TagName="SiamAdmin_Header" Src="SiamAdmin_Header.ascx" %>
<%@ Register TagPrefix="SIAMAdmin" TagName="SiamAdmin_SideNav" Src="SiamAdmin_SideNav.ascx" %>
<%@ Register TagPrefix="SIAMAdmin" TagName="SiamAdmin_Footer" Src="SiamAdmin_Footer.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS Administration Application </title>
    <link href="includes/style.css" type="text/css" rel="stylesheet">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <style type="text/css">
        A
        {
            text-decoration: none;
        }
        A:Visited
        {
            text-decoration: none;
        }
        a:hover
        {
            text-decoration: underline;
        }
    </style>

    <script type="text/javascript">
        function SecrecyBanner() {
            if (window.document.getElementById('hdnSecrecyBanner').value === 0) {
                window.document.getElementById('hdnSecrecyBanner').value = 1;

            }
        }
        function LoadDefault() {
            if (document.parentWindow.name === 'CSSIAMContentFrame') {
                window.parent.location = 'redirect.aspx';
            }
        }
    </script>

</head>
<body bottommargin="0" alink="gray" link="gray" leftmargin="0" topmargin="0" marginheight="0"
    marginwidth="0" onactivate="LoadDefault();" onload="SecrecyBanner (); document.getElementById('divName').style.height = (document.body.clientHeight)*.8;">
    <form id="CSSIAMDefault" method="post" runat="server">
    <table width="100%" height="97%" border="0">
        <tr height="100%">
            <td height="100%">
                <table width="100%" height="100%" style="background-color: #ffffff;"
                    border="0">
                    <tr height="5%">
                        <td colspan="2" width="100%">
                            <SIAMAdmin:SiamAdmin_Header ID="SiamAdmin_Header" runat="server"></SIAMAdmin:SiamAdmin_Header>
                        </td>
                    </tr>
                    <tr height="95%">
                        <td width="15%" bgcolor="#5d7180" valign="top" height="100%">
                            <uc1:SiamSideNav_Menu ID="SiamSideNav_Menu1" runat="server" />
                        </td>
                        <td width="85%" align="center" valign="top">
                            <iframe id="ContentFrame" name="CSSIAMContentFrame" frameborder="no" runat="server"
                                width="100%" height="100%"></iframe>
                        </td>
                    </tr>
                    <tr height="2%">
                        <td colspan="2">
                            <SIAMAdmin:SiamAdmin_Footer ID="SiamAdmin_Footer" runat="server"></SIAMAdmin:SiamAdmin_Footer>
                        </td>
                    </tr>
                </table>
                <input type="hidden" runat="server" id="hdnSecrecyBanner" value="0"></input>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
