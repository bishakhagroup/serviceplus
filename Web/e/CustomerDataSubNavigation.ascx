<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.CustomerDataSubNavigation" CodeFile="CustomerDataSubNavigation.ascx.vb" %>
<table border="0" bgcolor="#5d7180">
    <tr height="25">
        <td align="center" style="border-left: 3px white solid; <%If CurrentPage() <> "CustomerInfo" Then Response.Write("background-color: gray")%>">
            <a class="Nav3" href="CustomerInfo.aspx?SiamId=<%=SiamID()%>">&nbsp;&nbsp;View User Data &nbsp;&nbsp;</a>
        </td>
        <td align="center" style="border-left: 3px white solid; <%If CurrentPage() <> "FindOrders" Then Response.Write("background-color: gray")%>">
            <a class="Nav3" href="FindOrders.aspx?LdapId=<%=LdapID()%>">&nbsp;&nbsp;View Orders&nbsp;&nbsp;</a>
        </td>
        <%If UserType = "A" Then%>
        <td align="center" style="border-left: 3px white solid; <%If CurrentPage() <> "ViewSAPAccounts" Then Response.Write("background-color: gray")%>">
            <a class="Nav3" href="ViewSAPAccounts.aspx?LdapId=<%=LdapID()%>">&nbsp;&nbsp;View SAP Accounts&nbsp;&nbsp;</a>
        </td>
        <%End If%>
        <td align="center" style="border-left: 3px white solid; <%If CurrentPage() <> "TaxExempt" Then Response.Write("background-color: gray")%>">
            <a class="Nav3" href="TaxExempt.aspx?SiamId=<%=SiamID()%>">&nbsp;&nbsp;View Tax Exemptions&nbsp;&nbsp;</a>
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <asp:Label ID="lblNavError" runat="server" />
        </td>
    </tr>
</table>
