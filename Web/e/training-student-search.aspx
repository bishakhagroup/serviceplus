<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_student_search" CodeFile="training-student-search.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>users</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="500" align="center" border="0">
				<tr>
					<td class="PageHead" align="center" width="500" height="30">Search Students</td>
				</tr>
				<tr>
					<td height="5">&nbsp;
						<asp:label id="ErrorLabel" runat="server" CssClass="body" Width="448px" ForeColor="Red" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table width="500" align="center" border="0">
							<tr>
								<td width="10"></td>
								<td align="center" colSpan="2"><asp:label id="SearchUsersLabel" CssClass="BodyHead" Runat="server">Student Search</asp:label></td>
								<td></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td align="center" colSpan="2">
									<table border="0">
										<tr>
											<td align="right"><asp:label id="NameLabel" CssClass="Body" Runat="server">First or Last Name</asp:label></td>
											<td><SPS:SPSTextBox id="NameTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox></td>
										</tr>
										<tr>
											<td align="right"><asp:label id="EmailLabel" runat="server" CssClass="Body">Email</asp:label></td>
											<td><SPS:SPSTextBox id="EmailTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox></td>
										</tr>
										<tr>
											<td align="right"><asp:label id="CompanyLabel" runat="server" CssClass="Body">Company</asp:label></td>
											<td><SPS:SPSTextBox id="CompanyTextBox" runat="server" CssClass="Body"></SPS:SPSTextBox></td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td align="center" colSpan="2">
									<table border="0">
										<tr>
											<td><asp:button id="SearchButton" runat="server" CssClass="Body" Text="Search"></asp:button></td>
											<td><asp:button id="ShowAllStudentsButton" runat="server" CssClass="Body" Text="Show All Students"></asp:button></td>
											<td></td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
							<tr height="10">
								<td width="10"></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td align="center" colSpan="2"><asp:label id="Label1" runat="server" CssClass="body" Visible="False">Manage Students</asp:label></td>
								<td></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td colSpan="2">
									<table border="0">
										<tr>
											<td><asp:datagrid id="studentDataGrid" runat="server" Width="500px" Font-Size="Smaller"  AllowPaging="True" PageSize="10" AutoGenerateColumns="False" OnPageIndexChanged="studentDataGrid_Paging">
													<SelectedItemStyle BackColor="Lime"></SelectedItemStyle>
													<Columns>
														<asp:TemplateColumn HeaderText="Reservation#">
															<HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
															<ItemTemplate>
																<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReservationNumber") %>' NavigateUrl='' ID="HyperlinkReservationNo">
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<%--<asp:BoundColumn DataField="ReservationNumber" HeaderText="Reservation#" ReadOnly="True">
															<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>--%>
														<asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True">
															<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
														<asp:TemplateColumn HeaderText="First Name">
															<HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
															<ItemTemplate>
																<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' NavigateUrl='' ID="HyperlinkFirstName">
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Last Name">
															<HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
															<ItemTemplate>
																<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' NavigateUrl='' ID="HyperlinkLastName">
																</asp:HyperLink>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:TemplateColumn HeaderText="Status">
															<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
															<ItemTemplate>
																<asp:Label runat="server" ID="LabelStatus" Width="15px"></asp:Label>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:BoundColumn DataField="EMail" ReadOnly="True" HeaderText="Email">
															<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
															<ItemStyle HorizontalAlign="Center"></ItemStyle>
														</asp:BoundColumn>
													</Columns>
												</asp:datagrid></td>
										</tr>
									</table>
								</td>
								<td></td>
							</tr>
							<tr>
								<td width="10"></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
				</tr>
			</table>
		</form>
	</body>
</HTML>
