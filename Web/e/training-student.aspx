<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.traininmg_student" CodeFile="training-student.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>traininmg_student</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
	</HEAD>
	<body bgColor="#ffffff">
		<form id="Form1" method="post" runat="server">
			<table style="WIDTH: 567px; HEIGHT: 676px" align="center"
				border="0">
				<tr>
					<td class="PageHead" align="center" colSpan="1" height="30" rowSpan="1">Training 
						Student</td>
				</tr>
				<TR>
					<TD class="PageHead" align="center"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"
							Font-Size="Smaller"></asp:label></TD>
				</TR>
				<tr>
					<td style="HEIGHT: 11px"><asp:label id="Label2" runat="server" CssClass="Body" Font-Size="Smaller" Font-Bold="True"> Order/Reservation Information:</asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 101px" vAlign="top" align="left">
						<TABLE id="Table2" style="WIDTH: 624px; HEIGHT: 88px" width="624"
							border="1">
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 13px" align="right" colSpan="1" rowSpan="1"><asp:label id="Label1" runat="server" CssClass="Body">Number:</asp:label></TD>
								<TD style="HEIGHT: 13px"><asp:hyperlink id="lnkOrderNumber" runat="server" 
                                        CssClass="Body" Width="88px" Font-Bold="True" ForeColor="Blue">HyperLink</asp:hyperlink></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 23px" align="right"><asp:label id="Label3" runat="server" CssClass="Body">Order Date:</asp:label></TD>
								<TD style="HEIGHT: 23px"><asp:label id="labelOrderDate" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 23px" align="right"><asp:label id="Label4" runat="server" CssClass="Body">Updated By:</asp:label></TD>
								<TD style="HEIGHT: 23px"><asp:label id="labelUpdateBy" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label5" runat="server" CssClass="Body">Update Date:</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:label id="labelUpdateDate" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td style="HEIGHT: 11px"><asp:label id="Label10" runat="server" CssClass="Body" Font-Size="Smaller" Font-Bold="True"> Class Information:</asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 138px" vAlign="top" align="left">
						<TABLE id="Table2" style="WIDTH: 624px; HEIGHT: 80px" width="624"
							border="1">
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 21px" align="right" colSpan="1" rowSpan="1"><asp:label id="Label14" runat="server" CssClass="Body">Class Number:</asp:label></TD>
								<TD style="HEIGHT: 21px"><asp:label id="labelClassNumber" runat="server" CssClass="Body" Width="208px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label16" runat="server" CssClass="Body">Course Number:</asp:label></TD>
								<TD style="HEIGHT: 20px"><asp:label id="labelCourseNumber" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 21px" align="right"><asp:label id="Label18" runat="server" CssClass="Body">Course Title:</asp:label></TD>
								<TD style="HEIGHT: 21px"><asp:label id="labelCourseTitle" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label20" runat="server" CssClass="Body"> Start Date:</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:label id="labelStartDate" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label22" runat="server" CssClass="Body"> End Date:</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:label id="labelEndDate" runat="server" CssClass="Body" Width="104px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label24" runat="server" CssClass="Body"> Location:</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:label id="labelLocation" runat="server" CssClass="Body" Width="300px"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr>
					<td style="HEIGHT: 12px"><asp:label id="Label23" runat="server" CssClass="Body" Font-Size="Smaller" Font-Bold="True"> Student Information:</asp:label></td>
				</tr>
				<tr>
					<td style="HEIGHT: 56px" vAlign="top" align="left">
						<TABLE id="Table2" style="WIDTH: 624px; HEIGHT: 48px" width="624"
							border="1">
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 7px" align="right" colSpan="1" rowSpan="1"><asp:label id="labelInvoiceTitle" runat="server" CssClass="Body"> Company:</asp:label></TD>
								<TD style="HEIGHT: 7px"><SPS:SPSTextBox id="txtCompany" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 26px" align="right"><asp:label id="Label27" runat="server" CssClass="Body">First Name:</asp:label></TD>
								<TD style="HEIGHT: 26px"><SPS:SPSTextBox id="txtFirstName" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label6" runat="server" CssClass="Body">Last Name:</asp:label></TD>
								<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtLastName" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label7" runat="server" CssClass="Body">Street Address:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtAddress1" runat="server" 
                                        CssClass="Body" MaxLength="50" ></SPS:SPSTextBox>
								<asp:label id="LabelLine1Star" CssClass="redAsterick" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label8" runat="server" CssClass="Body">Address 2nd Line:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtAddress2" runat="server" 
                                        CssClass="Body" MaxLength="50"></SPS:SPSTextBox>
								<asp:label id="LabelLine2Star" CssClass="redAsterick" runat="server"></asp:label></TD>
							</TR>
								<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label30" runat="server" CssClass="Body">Address 3rd Line:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtAddress3" runat="server" 
                                        CssClass="Body" MaxLength="50"></SPS:SPSTextBox>
								<asp:label id="LabelLine3Star" CssClass="redAsterick" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 25px" align="right"><asp:label id="labelCity" runat="server" CssClass="Body">City:</asp:label></TD>
								<TD style="HEIGHT: 25px"><SPS:SPSTextBox id="txtCity" runat="server" 
                                        CssClass="Body" MaxLength="50"></SPS:SPSTextBox>
								<asp:label id="LabelLine4Star" CssClass="redAsterick" runat="server"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label11" runat="server" CssClass="Body">State:</asp:label></TD>
								<TD style="HEIGHT: 20px"><asp:dropdownlist id="ddlState" runat="server" CssClass="Body" Width="152px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label9" runat="server" CssClass="Body">Zip/Postal Code:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtPostalCode" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 21px" align="right"><asp:label id="Label12" runat="server" CssClass="Body">Country:</asp:label></TD>
								<TD style="HEIGHT: 21px"><SPS:SPSTextBox id="txtCountryCode" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="labelEMail" runat="server" CssClass="Body">EMail:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtEMail" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 26px" align="right"><asp:label id="Label13" runat="server" CssClass="Body">Phone:</asp:label></TD>
								<TD style="HEIGHT: 26px"><SPS:SPSTextBox id="txtPhone" runat="server" CssClass="Body"></SPS:SPSTextBox>&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label15" runat="server" CssClass="Body">Extension:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtExtension" runat="server" CssClass="Body"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label17" runat="server" CssClass="Body">Fax:</asp:label></TD>
								<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtFax" runat="server"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 22px" align="right"><asp:label id="Label19" runat="server" CssClass="Body">Date Reserved:</asp:label></TD>
								<TD style="HEIGHT: 22px"><asp:label id="labelDateReserved" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label25" runat="server" CssClass="Body">Date Cancelled:</asp:label></TD>
								<TD style="HEIGHT: 20px"><asp:label id="labelDateCancelled" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label21" runat="server" CssClass="Body">Date Waitlisted:</asp:label></TD>
								<TD style="HEIGHT: 20px"><asp:label id="labelDateWaited" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 93px" vAlign="top" align="right"><asp:label id="Label26" runat="server" CssClass="Body">Notes:</asp:label></TD>
								<TD style="HEIGHT: 93px"><SPS:SPSTextBox id="txtNotes" runat="server" CssClass="Body" Width="480px" Height="90px" TextMode="MultiLine"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="Label28" runat="server" CssClass="Body">Status:</asp:label></TD>
								<TD style="HEIGHT: 24px"><asp:dropdownlist id="ddlStatus" runat="server" CssClass="Body" Width="112px"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 22px" align="right"><asp:label id="Label29" runat="server" CssClass="Body">Updated By:</asp:label></TD>
								<TD style="HEIGHT: 22px"><asp:label id="labelStudentUpdatedBy" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label31" runat="server" CssClass="Body">Update Date:</asp:label></TD>
								<TD style="HEIGHT: 20px"><asp:label id="labelStudentUpdateDate" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR>
					<TD style="HEIGHT: 19px" align="center">
						<TABLE id="Table1" style="WIDTH: 626px; HEIGHT: 27px" width="626"
							border="0">
							<TR>
								<TD align="center"><asp:button id="btnUpdate" runat="server" Text="Update"></asp:button></TD>
								<TD align="center"><asp:button id="btnCancel" runat="server" Text="Cancel"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
