<%@ Page Language="vb" AutoEventWireup="false"  %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ServicesPLUS Administration Application</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" bgColor="#ffffff" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
		<center>
			<table width="525" align="center" bgColor="#ffffff" border="0">
				<tr>
					<td align="center" colSpan="3" height="50"><asp:label id="errorLabel" runat="server" ForeColor="Red" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<%--<td align="center" colSpan="3"><span class="BodyHead">ServicesPLUS Administration Application</span></td>--%>
					<td align="center" colSpan="3"><span class="BodyHead">ServicesPLUS Inside</span></td>
				</tr>
				<tr>
					<td colSpan="3" height="10">&nbsp;</td>
				</tr>
				
				<tr>
					<td colSpan="3" height="10">&nbsp;</td>
				</tr>
				<tr>
					<td colSpan="3" height="10">&nbsp;</td>
				</tr>
				   <%--'********************Changes done by Mujahid E Azam for Bug# 2633 Starts here***************--%>
				 <%If Not Session("NotLoggedInMessage") Is Nothing Then%>
				 <tr><td align='center' colSpan='3'><span class='Body'><%=Session("NotLoggedInMessage").ToString()%>  </span></td></tr>
				 <% Else%>
				 <tr><td align='center' colSpan='3'>
				    <div class='Body'>
				        <%--You are not a ServicesPLUS Administration Application user, or you do not have any role assigned to you within the application. Please contact the <a href='mailto:ApplicationSupportCenter@am.sony.com' class='Body'>Application Support Center</a> for assistance.--%>
				        <p><a href="../Member.aspx" class='Body'>Log into ServicesPLUS site as internal user.</a></p>
				        <p><a href="default.aspx" class='Body'>Log into ServicesPLUS Administration Application.</a></p>
				        <p><a href="#" class='Body'>Register for ServicesPLUS site as internal user.</a> <span style="color: Red;">Under Construction</span></p>
				    </div>
				</td></tr>
				 <%End If%>
				<%--'********************Changes done by Mujahid E Azam for Bug# 2633 Starts here***************--%>
				
                <tr>
                    <td align="center" colspan="3">&nbsp;
                    </td>
                </tr>
                
                &nbsp;</table>
		</center>
		</form>
	</body>
</HTML>
