<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.UtilityClass"
    CodeFile="Utility.aspx.vb" %>

<%@ Register Assembly="CrystalDecisions.Web" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Utility</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <style type="text/css">
        body
        {
            scrollbar-base-color: #5d7180;
            scrollbar-face-color: #c9d5e6;
            scrollbar-shadow-color: #632984;
            scrollbar-highlight-color: #632984;
            scrollbar-3dlight-color: #130919;
            scrollbar-darkshadow-color: #130919;
            scrollbar-track-color: #130919;
            scrollbar-arrow-color: black;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="redAsterick" EnableViewState="false" /><br />
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </form>
</body>
</html>
