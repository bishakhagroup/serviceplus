<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.Encrypt" CodeFile="Encrypt.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FindOrders</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="left" border="0">
				<tr>
					<td align="center" height="10"></td>
				</tr>
				<tr>
					<td colSpan="5" height="5"></td>
				</tr>
				<tr>
					<td align="center"><span class="PageHead">Encryption Helper</span></td>
				</tr>
				<tr>
					<td colSpan="5" height="5"></td>
				</tr>
                <tr>
                    <td colspan="5" height="5">
                        <asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></td>
                </tr>
				<tr>
					<td colSpan="5" height="2"></td>
				</tr>
				<tr>
					<td align="center" style="height: 103px">
						<table border="0" width="90%">
							<tr>
								<td width=10%><span class="Body">Input&nbsp;</span></td>
								<td width=90% >
								    <SPS:SPSTextBox id="txtInput" runat="server" CssClass="Body" Width="100%"></SPS:SPSTextBox>
								</td>
							</tr>
							<tr>
								<td  width=10%><span class="Body">OutPut&nbsp;</span></td>
                                <td width=90% >
                                    <SPS:SPSTextBox ID="txtOutPut" runat="server" CssClass="Body" Width="100%"></SPS:SPSTextBox>
                                </td>
							</tr>
							<tr>
								<td align="right" colSpan="2" style="height: 19px"></td>
							</tr>
                            <tr>
                                <td align="right" colspan="2">
                                    <asp:Button ID="btnEncrypt" runat="server" CssClass="Body" Text="Encrypt" />
                                    <asp:Button ID="btnDecrypt" runat="server" CssClass="Body" Text="Decrypt" />
                                    <asp:Button ID="btnClear" runat="server" CssClass="Body" Text=" Clear " /></td>
                            </tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td colSpan="5">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
