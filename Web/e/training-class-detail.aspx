<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_class_detail" CodeFile="training-class-detail.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_class_detail</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 449px; TOP: 8px; HEIGHT: 508px"
				width="449" border="0" align="center">
				<TR>
					<TD class="PageHead" align="center">Training Class Detail</TD>
				</TR>
				<TR>
					<TD align="left" style="HEIGHT: 14px">
						<asp:label id="ErrorLabel" runat="server" ForeColor="Red" EnableViewState="False" CssClass="Body"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left" style="HEIGHT: 17px">
						&nbsp;</TD>
				</TR>
				<TR>
					<td>
						<TABLE id="Table2" style="WIDTH: 488px; HEIGHT: 57px" width="488"
							border="0">
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 19px" align="right">
									<asp:Label id="Label1" runat="server" CssClass="Body">Title:</asp:Label></TD>
								<TD style="HEIGHT: 19px">
									<asp:label id="LabelTitle" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px" vAlign="top" align="right" colSpan="1" rowSpan="1">
									<asp:label id="Label8" runat="server" CssClass="Body">Description:</asp:label></TD>
								<TD>
									<asp:label id="labelDescription" runat="server" CssClass="Body" BorderWidth="1" Width="396px"
										Height="182px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 21px" align="right" colSpan="1" rowSpan="1">
									<asp:label id="Label3" runat="server" CssClass="Body" Width="80px"> Prerequisite(s):</asp:label></TD>
								<TD style="HEIGHT: 21px">
									<asp:label id="LabelPrerequisite" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 20px" vAlign="top" align="right">
									<asp:label id="Label4" runat="server" CssClass="Body">Start Date:</asp:label></TD>
								<TD style="HEIGHT: 20px" vAlign="top">
									<asp:label id="LabelStartDate" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 21px" vAlign="top" align="right">
									<asp:label id="Label5" runat="server" CssClass="Body">End Date:</asp:label></TD>
								<TD style="HEIGHT: 21px" vAlign="top">
									<asp:label id="LabelEndDate" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 21px" vAlign="top" align="right">
									<asp:label id="TrainingTime" runat="server" CssClass="Body">Training Time:</asp:label></TD>
								<TD style="HEIGHT: 21px" vAlign="top">
									<asp:label id="lblTrainingTime" runat="server" CssClass="Body"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 21px" vAlign="top" align="right" colSpan="1" rowSpan="1">&nbsp;&nbsp;&nbsp;&nbsp;
									<asp:label id="Label6" runat="server" CssClass="Body">Location:</asp:label></TD>
								<TD style="HEIGHT: 21px">
									<asp:Table id="locationTable" runat="server" BorderWidth="0px" Width="300px" Height="12px"
										CellSpacing="0"></asp:Table></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 21px" vAlign="top" align="right">
									<asp:label id="Label7" runat="server" CssClass="Body">Price per Student:</asp:label></TD>
								<TD style="HEIGHT: 21px" vAlign="top">
									<asp:label id="LabelListPrice" runat="server" CssClass="Body" Width="64px"></asp:label></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 173px; HEIGHT: 56px" vAlign="top" align="right">
									&nbsp;</TD>
								<TD style="HEIGHT: 56px">
									&nbsp;</TD>
							</TR>
						</TABLE>
						<TABLE id="Table3" style="HEIGHT: 19px" width="544"
							border="0">
							<TR>
								<TD align="left" >
									<asp:Button id="btnCancel" runat="server" CssClass="ody" Text="Cancel"></asp:Button></TD>
							</TR>
						</TABLE>
					</td>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
