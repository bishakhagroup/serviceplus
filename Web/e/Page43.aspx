<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.Page43" CodeFile="Page43.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Page43</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<SPS:SPSTextBox id="TextBox1" style="Z-INDEX: 102; LEFT: 368px; POSITION: absolute; TOP: 24px" runat="server"
				TextMode="Password" Width="152px" Height="24px" CssClass="body"></SPS:SPSTextBox><asp:label id="Label4" style="Z-INDEX: 109; LEFT: 448px; POSITION: absolute; TOP: 168px" runat="server"
				Width="177px" CssClass="body">End Time(MM/DD/YYYY HH:mm:ss)</asp:label><asp:button id="btnLogin" style="Z-INDEX: 101; LEFT: 552px; POSITION: absolute; TOP: 24px" runat="server"
				Width="48px" CssClass="body" Text="Test"></asp:button><asp:label id="Label1" style="Z-INDEX: 103; LEFT: 88px; POSITION: absolute; TOP: 96px" runat="server"
				CssClass="body">Label:</asp:label><asp:dropdownlist id="ddLabel" style="Z-INDEX: 104; LEFT: 136px; POSITION: absolute; TOP: 96px" runat="server"
				Width="104px" CssClass="body">
				<asp:ListItem Value="All">All</asp:ListItem>
				<asp:ListItem Value="Error">Error</asp:ListItem>
				<asp:ListItem Value="Verbose">Verbose</asp:ListItem>
				<asp:ListItem Value="Info">Info</asp:ListItem>
				<asp:ListItem Value="Warning">Warning</asp:ListItem>
			</asp:dropdownlist><asp:label id="Label2" style="Z-INDEX: 105; LEFT: 64px; POSITION: absolute; TOP: 128px" runat="server"
				Width="68px" CssClass="body">Text Filter:</asp:label><SPS:SPSTextBox id="txtFilter" style="Z-INDEX: 106; LEFT: 136px; POSITION: absolute; TOP: 128px"
				runat="server" Width="576px" CssClass="body"></SPS:SPSTextBox><asp:label id="Label3" style="Z-INDEX: 107; LEFT: 64px; POSITION: absolute; TOP: 168px" runat="server"
				Width="184px" CssClass="body">Begin Time(MM/DD/YYYY HH:mm:ss)</asp:label><SPS:SPSTextBox id="txtBeginTime" style="Z-INDEX: 108; LEFT: 264px; POSITION: absolute; TOP: 168px"
				runat="server" CssClass="body"></SPS:SPSTextBox><SPS:SPSTextBox id="txtEndTime" style="Z-INDEX: 110; LEFT: 640px; POSITION: absolute; TOP: 168px"
				runat="server" CssClass="body"></SPS:SPSTextBox><asp:datagrid id="gridLogMessages" style="Z-INDEX: 111; LEFT: 64px; POSITION: absolute; TOP: 264px"
				runat="server" Width="560px" OnPageIndexChanged="gridLogMessages_Paging" AutoGenerateColumns="False" OnSelectedIndexChanged="gridLogMessages_SelectedIndexChanged"
				Font-Size="Smaller">
				<SelectedItemStyle BackColor="#8080FF"></SelectedItemStyle>
				<Columns>
					<asp:ButtonColumn Text="Select" HeaderText="Select" CommandName="Select"></asp:ButtonColumn>
					<asp:TemplateColumn HeaderText="Label">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Label")) %>' ID="Label9">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Time">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.SentTime")) %>' ID="Label7">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Priority">
						<ItemTemplate>
							<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Priority")) %>' ID="labelPriority">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle Mode="NumericPages"></PagerStyle>
			</asp:datagrid><asp:label id="Label5" style="Z-INDEX: 112; LEFT: 64px; POSITION: absolute; TOP: 240px" runat="server"
				Width="112px" CssClass="body">Log Messages:</asp:label><asp:label id="lbLogMessage" style="Z-INDEX: 113; LEFT: 64px; POSITION: absolute; TOP: 608px"
				runat="server" Width="832px" Height="152px" CssClass="body" EnableViewState="False"></asp:label><asp:button id="btnGet" style="Z-INDEX: 114; LEFT: 408px; POSITION: absolute; TOP: 200px" runat="server"
				Width="48px" CssClass="body" Text="Get"></asp:button><asp:label id="lbErrorText" style="Z-INDEX: 115; LEFT: 72px; POSITION: absolute; TOP: 64px"
				runat="server" Width="496px" CssClass="body" ForeColor="Red"></asp:label><asp:label id="Label6" style="Z-INDEX: 116; LEFT: 64px; POSITION: absolute; TOP: 584px" runat="server"
				Width="80px" CssClass="body">Log Message:</asp:label></form>
	</body>
</HTML>
