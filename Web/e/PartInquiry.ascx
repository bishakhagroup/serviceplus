﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PartInquiry.ascx.vb"
    Inherits="e_PartInquiry" %>
<table>
    <tr>
        <td>
            <SPS:SPSLabel ID="lblPartNumbers" CssClass="BodyHead" runat="server">Part Number</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtPartNumbers" runat="server" CssClass="BodyHead" Width="100px">
X25808481                                                
            </SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSLocation" CssClass="BodyHead" runat="server">Location</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtLocation" CssClass="BodyHead" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSSAPAccountNumber" CssClass="BodyHead" runat="server">SAP Sold To Account Number</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtSAPAccountNumber" CssClass="BodyHead" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSPriceInfo" CssClass="BodyHead" runat="server">Price Info</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtPriceInfo" CssClass="BodyHead" runat="server">X</SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSInventoryInfo" CssClass="BodyHead" runat="server">Inventory Info</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtInventoryInfo" runat="server" CssClass="BodyHead">X</SPS:SPSTextBox>
        </td>
    </tr>
</table>
<%-- Sasikumar WP SPLUS_WP014--%>
<table>
    <tr>
        <td>
            <SPS:SPSLabel ID="lblDistributionChannel" CssClass="BodyHead" runat="server">Distribution Channel</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtDistributionChannel" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSDivision" CssClass="BodyHead" runat="server">Division</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtDivision" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSSalesOrganization" CssClass="BodyHead" runat="server">SalesOrganization</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtSalesOrganization" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSLabel ID="SPSLanguage" CssClass="BodyHead" runat="server">Language</SPS:SPSLabel>
        </td>
    </tr>
    <tr>
        <td>
            <SPS:SPSTextBox ID="txtLanguage" runat="server"></SPS:SPSTextBox>
        </td>
    </tr>
</table>
