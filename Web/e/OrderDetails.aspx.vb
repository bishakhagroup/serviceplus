Imports System.Drawing
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Namespace SIAMAdmin
    Partial Class OrderDetails
        Inherits UtilityClass

        Public Auditcustomer As User
        Dim objGlobalData As GlobalData
        Protected WithEvents OrderDetailsTable As System.Web.UI.WebControls.Table
        Dim domianurl As String = Nothing
        Dim domianur As Boolean = False
        Dim culinfodtls As String = ""

#Region " Web Form Designer Generated Code "
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            Page.ID = Request.Url.Segments.GetValue(1)

            'MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
            ApplyDataGridStyle()
        End Sub
#End Region

#Region "   Events  "
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            objGlobalData = Session("AGlobalData")
            ControlMethod(sender, e)
        End Sub

        Private Sub Format_DataGridItem(ByVal sender As System.Object, ByVal e As DataGridItemEventArgs) Handles DataGrid2.ItemCreated
            If e.Item.ItemType <> ListItemType.Header Then
                e.Item.ApplyStyle(GetStyle(e.Item.ItemType()))
            End If
        End Sub

        Protected Sub DataGrid2_PageIndexChanged(ByVal source As Object, ByVal e As DataGridPageChangedEventArgs) Handles DataGrid2.PageIndexChanged
            DataGrid2.CurrentPageIndex = e.NewPageIndex
            DataGrid2.DataBind()
        End Sub
#End Region

#Region "   Methods     "
        Private Sub ControlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                If Request.QueryString("on") IsNot Nothing Then
                    Dim orderNumber As String = Request.QueryString("on")
                    If orderNumber.StartsWith("TRN") Then       ' It is a training order
                        Response.Redirect($"training-order.aspx?OrderNumber={orderNumber}", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Return
                    End If
                    Dim thisOrder As Order = GetOrder(orderNumber)

                    If Not thisOrder Is Nothing Then
                        Dim caller As String = String.Empty
                        If sender.Id IsNot Nothing Then caller = sender.Id.ToString()
                        If caller = Page.ID.ToString() Then WriteScriptsToPage()

                        lblMateriaType.Text = $"Order with electronically deliverable items (material type = {ConfigurationData.Environment.PartNumberMaterialType.MaterialType})?"
                        PopulateOrderDetailTable(thisOrder)
                        PopulateCustomerDetails(thisOrder)
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateCustomerDetails(ByVal thisOrder As Order)
            Dim objPCILogger As New PCILogger()

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginMethod = "Populate Customer Details"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.View
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                OrderNumberLabel.Text = thisOrder.OrderNumber.ToString()
                PoNumberLabel.Text = thisOrder.POReference.ToString()
                lblAmount.Text = FormatCurrency(thisOrder.AllocatedGrandTotal.ToString(), , , TriState.True, TriState.True)
                LblTax.Text = FormatCurrency(thisOrder.AllocatedTax.ToString(), , , TriState.True, TriState.True)
                lblshippinghandling.Text = FormatCurrency(thisOrder.AllocatedShipping.ToString(), , , TriState.True, TriState.True)
                lblRecyclingfees.Text = FormatCurrency(thisOrder.AllocatedSpecialTax.ToString(), , , TriState.True, TriState.True)
                lblPartSubTotal.Text = FormatCurrency(thisOrder.AllocatedPartSubTotal.ToString(), , , TriState.True, TriState.True)
                lblOrderDate.Text = FormatDate(thisOrder.OrderDate)
                SIAMIdentityLabel.Text = GenerateSiamIDImageString(thisOrder.Customer.SIAMIdentity)
                CCOrOnAccountLabel.Text = thisOrder.AccountNumber.ToString()

                'lblCustomerName.Text = thisOrder.Customer.FirstName + " " + thisOrder.Customer.LastName
                'customerLink.HRef = "CustomerInfo.aspx?Identity=" + thisOrder.Customer.SIAMIdentity'7086
                'customerLink.HRef = "CustomerInfo.aspx?LdapId=" + thisOrder.Customer.LdapID '7086
                'lblPhone.Text = thisOrder.ShipTo.PhoneNumber
                'If thisOrder.ShipTo.PhoneExt <> String.Empty Then
                'lblExt.Text = "(" + thisOrder.ShipTo.PhoneExt + ")"
                'End If
                'lblEmailAddress.Text = thisOrder.Customer.EmailAddress

                lblCreditCard.Text = thisOrder.CreditCard?.CreditCardNumber
                If Not String.IsNullOrWhiteSpace(lblCreditCard.Text) Then lblCreditCard.Text = lblCreditCard.Text.Substring(6, 4)

                If thisOrder.BillingOnly = 1 Then
                    label1.Text = "Yes"
                Else
                    label1.Text = "No"
                End If

                If String.IsNullOrWhiteSpace(thisOrder.Alt_Tax_Classification) Then
                    Lblalttax.Text = ""
                Else
                    Lblalttax.Text = thisOrder.Alt_Tax_Classification
                End If

                If String.IsNullOrWhiteSpace(thisOrder.EnableDocumentIDNumber) Then
                    Lblenabledocidnum.Text = ""
                Else
                    Lblenabledocidnum.Text = thisOrder.EnableDocumentIDNumber
                End If

                objPCILogger.CreditCardMaskedNumber = thisOrder.CreditCard?.CreditCardNumberMasked
                objPCILogger.CreditCardSequenceNumber = thisOrder.CreditCard?.SequenceNumber
                If Not Session("Source") = "SAP" Then
                    If If((thisOrder.CreditCard Is Nothing), "", thisOrder.CreditCard.ExpirationDate.Trim()) = "" Then
                        thisOrder.CreditCard = Nothing
                        lblExpiryDate.Text = ""
                    Else
                        If thisOrder.CreditCard.ExpirationDate.Trim() = "0000" Then
                            thisOrder.CreditCard.ExpirationDate = ""
                            lblExpiryDate.Text = ""
                        Else
                            lblExpiryDate.Text = (If((thisOrder.CreditCard Is Nothing), Nothing, Format(Convert.ToDateTime(thisOrder.CreditCard.ExpirationDate).Month.ToString(), "{0}") & " / " & (Convert.ToDateTime(thisOrder.CreditCard.ExpirationDate).Year.ToString()).Substring(2, 2)))
                        End If
                    End If
                Else
                    If Not String.IsNullOrWhiteSpace(thisOrder.CreditCard.ExpirationDate) Then
                        lblExpiryDate.Text = $"{thisOrder.CreditCard.ExpirationDate.Substring(0, 2)}/{thisOrder.CreditCard.ExpirationDate.Substring(2, 2)}"
                    End If
                End If

                ' BILL-TO ADDRESS
                Dim thisSB As New StringBuilder
                If thisOrder.BillTo?.Name Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.Name + "<br/>")
                End If

                If thisOrder.BillTo.Attn Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.Attn + "<br/>")
                End If

                If thisOrder.BillTo.Line1 Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.Line1 + "<br/>")
                End If

                If thisOrder.BillTo.Line2 Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.Line2 + "<br/>")
                End If
                If Session("Source") = "SAP" Then
                    If thisOrder.BillTo.Line4 Is Nothing Then
                        thisSB.Append("<br/>")
                    Else
                        thisSB.Append(thisOrder.BillTo.Line4 + "<br/>")
                    End If
                Else
                    If thisOrder.BillTo.City Is Nothing Then
                        thisSB.Append("<br/>")
                    Else
                        thisSB.Append(thisOrder.BillTo.City + "<br/>")
                    End If
                End If

                If thisOrder.BillTo.State Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.State + "<br/>")
                End If

                If thisOrder.BillTo.PostalCode Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.PostalCode + "<br/>")
                End If

                If thisOrder.BillTo.Country Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.Country + "<br/>")
                End If

                If thisOrder.BillTo.PhoneNumber Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.BillTo.PhoneNumber)
                End If
                If thisOrder.BillTo.PhoneExt = "" Or Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(" - " + thisOrder.BillTo.PhoneExt + "<br/>")
                End If
                BillingInfoLabel.Text = thisSB.ToString()

                ' SHIP-TO ADDRESS
                thisSB = New StringBuilder
                If thisOrder.ShipTo.Name Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.Name + "<br/>")
                End If

                If thisOrder.ShipTo.Attn Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.Attn + "<br/>")
                End If

                If thisOrder.ShipTo.Line1 Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.Line1 + "<br/>")
                End If

                If thisOrder.ShipTo.Line2 Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.Line2 + "<br/>")
                End If
                If Session("Source") = "SAP" Then
                    If thisOrder.ShipTo.Line4 Is Nothing Then
                        thisSB.Append("<br/>")
                    Else
                        thisSB.Append(thisOrder.ShipTo.Line4 + "<br/>")
                    End If
                Else
                    If thisOrder.ShipTo.City Is Nothing Then
                        thisSB.Append("<br/>")
                    Else
                        thisSB.Append(thisOrder.ShipTo.City + "<br/>")
                    End If
                End If
                If thisOrder.ShipTo.State Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.State + "<br/>")
                End If

                If thisOrder.ShipTo.PostalCode Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.PostalCode + "<br/>")
                End If

                If thisOrder.ShipTo.Country Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.Country + "<br/>")
                End If

                If thisOrder.ShipTo.PhoneNumber Is Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(thisOrder.ShipTo.PhoneNumber)
                End If
                If thisOrder.ShipTo.PhoneExt = "" Or Nothing Then
                    thisSB.Append("<br/>")
                Else
                    thisSB.Append(" - " + thisOrder.ShipTo.PhoneExt + "<br/>")
                End If

                If Session("Source") = "SAP" Then
                    If thisOrder.ShipTo.OfficeNumber Is Nothing Then
                        thisSB.Append("<br/>")
                    Else
                        thisSB.Append(thisOrder.ShipTo.OfficeNumber + "<br/>")
                    End If
                End If
                ShippingInfoLabel.Text = thisSB.ToString()

                If Session("Source") = "SAP" Then
                    ErrorLabel.Text = ""
                    Lblsource.Text = "SAP"
                    Lblorderdetail.Text = "Order Detail Information from SAP except Training Certification Activation Code"
                Else
                    If Session("Source") = "ServicePlusDatabase" Then
                        ErrorLabel.Text = "This Order does not exist in SAP"
                        Lblsource.Text = "ServicesPLUS Database"
                        Lblorderdetail.Text = "Order Detail Information from ServicesPLUS Database"
                    Else
                        Lblsource.Text = "ServicesPLUS Database"
                        Lblorderdetail.Text = "Order Detail Information from ServicesPLUS Database"
                    End If
                End If

                objPCILogger.Message = "Order No : " + OrderNumberLabel.Text
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.PushLogToMSMQ()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "Error in Retrieving Customer Details: " + ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
            If Session("Source") = "ServicePlusDatabase with error" Then
                Throw New ServicesPlusBusinessException("An error was encountered retrieving order information from SAP")
            End If
        End Sub

        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String
            Dim strImg As String = "<img src='images/ImgName.JPG' style='border:0;' alt='' />"
            Dim strValue As String = ""

            Try
                For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                    strValue += strImg.Replace("ImgName", strChar.ToString())
                Next
            Catch ex As Exception
                Return strValue
            End Try

            Return strValue
        End Function

        Private Function GetOrder(ByVal thisOrderNumber As String) As Order
            Dim thisOrderManager As New OrderManager
            Dim returnValue As Order = Nothing
            Dim getSISOrder As String
            Dim objPCILogger As New PCILogger()

            Try
                getSISOrder = Request.QueryString("on")     ' Rename getSISOrder the variable as there is no SIS order
            Catch ex As Exception
                getSISOrder = ""
            End Try

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "getOrder"
                objPCILogger.Message = "GetOrder Succeeded."
                objPCILogger.EventType = EventType.View

                returnValue = thisOrderManager.GetOrder(thisOrderNumber, False, True, objGlobalData)

                If getSISOrder = "False" Or getSISOrder = "" Then
                    displayGoBtn.Visible = True
                Else
                    displayGoBtn.Visible = False
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "GetOrder Failed. " & ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
            Return returnValue
        End Function

        Private Sub PopulateCustomerDetails(ByRef thisCustomer As Customer)
            Try
                If Not thisCustomer Is Nothing Then

                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateOrderDetailTable(ByRef thisOrder As Order)
            Dim orderManager As New OrderManager
            Dim orderLines As OrderLineItem()
            Dim objPCILogger As New PCILogger()

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "getOrderDetail"
                objPCILogger.Message = "getOrderDetail Succeeded."
                objPCILogger.EventType = EventType.View

                If thisOrder IsNot Nothing Then
                    '2016-05-05 ASleight Modifying to retrieve Order Detail lines, as GetOrder method ignores them.
                    If (thisOrder.LineItems Is Nothing OrElse thisOrder.LineItems.Length = 0) Then
                        orderLines = orderManager.GetOrderLineItems(thisOrder.OrderNumber)
                    Else
                        orderLines = thisOrder.LineItems
                    End If
                    If (orderLines IsNot Nothing AndAlso orderLines.Length > 0) Then
                        DataGrid2.DataSource = orderLines
                        'DataGrid2.AllowPaging = False
                        'DataGrid2.PageSize = 10
                        'If orderLines.Length > DataGrid2.PageSize Then DataGrid2.AllowPaging = True
                        DataGrid2.DataBind()
                    End If
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "getOrderDetail Failed. " & ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function BuildInvoiceTrackingAndStatusTable(ByRef thisOrder As Order) As TableRow
            Dim returnValue As New TableRow
            Dim td As New TableCell

            Try
                If thisOrder IsNot Nothing Then
                    td.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing-""0"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>"))
                    td.Controls.Add(New LiteralControl("</table>"))
                    '-- return the table row 
                    returnValue.Controls.Add(td)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            Return returnValue
        End Function

        Private Function BuildOrderDetailsHeader() As TableRow
            Dim returnValue As New TableRow
            Dim td As New TableCell

            Try
                td.Controls.Add(New LiteralControl("<table width=""510"" border=""0"" cellpadding=""0"" cellspacing""1"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr bgcolor=""DarkSlateGray"">" + vbCrLf))
                '-- part number
                td.Controls.Add(New LiteralControl("<td width=""100"" height=""25""><span color=""white"">&nbsp;&nbsp;Part Number&nbsp;&nbsp;</span></td>"))
                '-- description 
                td.Controls.Add(New LiteralControl("<td width=""150""><span color=""white"">&nbsp;&nbsp;Description&nbsp;&nbsp;</span></td>"))
                '-- qty
                td.Controls.Add(New LiteralControl("<td width=""40""><span color=""white"">&nbsp;&nbsp;QTY&nbsp;&nbsp;</span></td>"))
                '-- item price
                td.Controls.Add(New LiteralControl("<td width=""70""><span color=""white"">&nbsp;&nbsp;Item Price&nbsp;&nbsp;</span></td>"))
                '-- total price
                td.Controls.Add(New LiteralControl("<td width=""100""><span color=""white"">&nbsp;&nbsp;Total Price&nbsp;&nbsp;</span></td>"))
                '-- cancel
                td.Controls.Add(New LiteralControl("<td width=""50""><span color=""white"">&nbsp;&nbsp;Cancel&nbsp;&nbsp;</span></td>"))
                td.Controls.Add(New LiteralControl("</tr>"))
                td.Controls.Add(New LiteralControl("</table>"))

                '-- return the table row 
                returnValue.Controls.Add(td)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

        Private Function BuildOrderDetails(ByRef thisOrder As Order) As TableRow
            Dim returnValue As New TableRow
            Dim td As New TableCell
            Dim bgColor As String = "SlateGray"

            Try
                If thisOrder IsNot Nothing Then
                    td.Controls.Add(New LiteralControl("<table width=""510"" border=""0"" cellpadding=""0"" cellspacing""1"">" + vbCrLf))

                    For Each thisLineItem As OrderLineItem In thisOrder.LineItems
                        bgColor = IIf(bgColor = "LightSlateGray", "SlateGray", "LightSlateGray")
                        td.Controls.Add(New LiteralControl($"<tr bgcolor={bgColor}>{vbCrLf}"))

                        '-- part number
                        Dim partNumber As String = IIf(thisLineItem.Product.PartNumber Is Nothing, String.Empty, thisLineItem.partnumber.ToString())
                        td.Controls.Add(New LiteralControl("<td width=""100"" height=""15"">&nbsp;&nbsp;<span color=""white"">" + partNumber.ToString() + "</span></td>"))
                        '-- description 
                        Dim partDescription As String = IIf(thisLineItem.Product.Description Is Nothing, String.Empty, thisLineItem.DESCRIPTION.ToString())
                        td.Controls.Add(New LiteralControl("<td width=""150"">&nbsp;&nbsp;<span color=""white"">" + partDescription.ToString() + "</span></td>"))
                        '-- qty
                        Dim qty As String = IIf(thisLineItem.Quantity.ToString() Is Nothing, String.Empty, thisLineItem.Quantity.ToString())
                        td.Controls.Add(New LiteralControl("<td width=""40"">&nbsp;&nbsp;<span color=""white"">" + qty.ToString() + "</span></td>"))
                        '-- item price
                        Dim itemPrice As Double = IIf(thisLineItem.Product.ListPrice.ToString() Is Nothing, 0, thisLineItem.LISTPRICE)
                        td.Controls.Add(New LiteralControl("<td width=""70"">&nbsp;&nbsp;<span color=""white"">$" + Format(itemPrice, "##0.00").ToString() + "</span></td>"))
                        '-- total price
                        Dim totalPrice As Double = IIf(thisLineItem.Product.ListPrice.ToString() Is Nothing, 0, (thisLineItem.LISTPRICE * thisLineItem.Quantity))
                        td.Controls.Add(New LiteralControl("<td width=""100"">&nbsp;&nbsp;<span color=""white"">$" + Format(totalPrice, "##0.00").ToString() + "</span></td>"))
                        '-- cancel
                        td.Controls.Add(New LiteralControl("<td width=""50"">&nbsp;&nbsp;<span color=""white"">N/A</span>&nbsp;&nbsp;</td>"))

                        td.Controls.Add(New LiteralControl("</tr>"))
                    Next
                    td.Controls.Add(New LiteralControl("</table>"))

                    '-- return the table row 
                    returnValue.Controls.Add(td)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

        Private Sub ApplyDataGridStyle()
            DataGrid2.ApplyStyle(GetStyle(0))
            ' 2018-07-30 Many of these properties can/should be set at the UI layer.
            'DataGrid2.PageSize = 10
            'DataGrid2.AutoGenerateColumns = False
            'DataGrid2.AllowPaging = True
            'DataGrid2.GridLines = GridLines.Horizontal
            'DataGrid2.AllowSorting = True
            DataGrid2.CellPadding = 3
            'DataGrid2.PagerStyle.Mode = PagerMode.NumericPages
            'DataGrid2.PagerStyle.Height = Unit.Pixel(10)
            'DataGrid2.HeaderStyle.Height = Unit.Pixel(30)
            'DataGrid2.HeaderStyle.Font.Size = FontUnit.Point(10)
            'DataGrid2.HeaderStyle.ForeColor = Color.GhostWhite
            'DataGrid2.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            'DataGrid2.ItemStyle.Font.Size = FontUnit.Point(10)
            'DataGrid2.ItemStyle.ForeColor = Color.GhostWhite
            'DataGrid2.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            'DataGrid2.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub WriteScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Public Function SelectCCNo(ByVal cardNumber As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager

            creditNo = objAuditDataMgr.SelectCC(cardNumber)

            Return creditNo
        End Function

        Public Function GetAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = ""

            If Not String.IsNullOrWhiteSpace(thisCCNo) Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If

            Return returnValue
        End Function
#End Region

    End Class
End Namespace
