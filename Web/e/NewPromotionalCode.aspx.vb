﻿Imports Sony.US.ServicesPLUS.Process
Imports System.Data
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin
    Partial Class NewPromotionalCode
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub Page_Init1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)

        End Sub
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    LoadIndustry()
                    LoadModelPrefix()
                    LoadReseller()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlModelPrefix_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModelPrefix.SelectedIndexChanged
            Try
                'Load Model
                If ddlModelPrefix.SelectedIndex > 0 Then
                    LoadModel()
                    Dim objPUSH As ListItem
                    For Each objPUSH In lstSelected.Items
                        lstBoxAvailable.Items.Remove(objPUSH)
                    Next
                Else
                    ddlModelPrefix.Items.Clear()
                    ddlModelPrefix.Items.Insert(0, New ListItem("Select a model prefix above before proceeding"))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Function ValidateData() As Boolean
            If (Len(txtpromocodenumber.Text.Trim()) = 10) Then
                If Not ((txtpromocodenumber.Text.Trim().ToUpper.StartsWith("IN") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("MO") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("RE") Or txtpromocodenumber.Text.Trim().ToUpper.StartsWith("CB"))) Then
                    ErrorLabel.Text = "The Promo Code Number you entered is not valid."
                    Return False
                End If
            Else
                If (txtpromocodenumber.Text.Trim() = "") Then
                    ErrorLabel.Text = "Please enter Promo Code Number."
                    Return False
                End If
                ErrorLabel.Text = "The Promo Code Number you entered is not valid."
                Return False
            End If
            'Promo start date check
            If (txtPromoStartdate.Text.Trim() = String.Empty) Then
                ErrorLabel.Text = "Please enter Promo Start Date."
                ErrorLabel.Visible = True
                Return False
            Else
                If IsDate(txtPromoStartdate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtPromoStartdate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Promo Start Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False
                    End If
                Else
                    ErrorLabel.Text = "Please enter Promo Start Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            'Promo end date check
            If (txtpromoenddate.Text.Trim() = String.Empty) Then
                ErrorLabel.Text = "Please enter Promo End Date."
                ErrorLabel.Visible = True
                Return False
            Else
                If IsDate(txtpromoenddate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtpromoenddate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Promo End Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False

                    End If
                Else
                    ErrorLabel.Text = "Please enter Promo End Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            'Promo registration end date check
            If (txtproregenddate.Text.Trim() = String.Empty) Then
                ErrorLabel.Text = "Please enter Promo End Date."
                ErrorLabel.Visible = True
                Return False
            Else
                If IsDate(txtproregenddate.Text.ToString()) Then
                    Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                    If Not System.Text.RegularExpressions.Regex.IsMatch(txtproregenddate.Text.ToString(), thisPattern) Then
                        ErrorLabel.Text = "Please enter Product Registration End Date in MM/DD/YYYY format."
                        ErrorLabel.Visible = True
                        Return False

                    End If
                Else
                    ErrorLabel.Text = "Please enter Product Registration End Date in MM/DD/YYYY format."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            If (txtmessage.Text.Trim() = String.Empty) Then
                ErrorLabel.Text = "Please enter Success Message."
                ErrorLabel.Visible = True
                Return False
            Else
                If Len(txtmessage.Text.Trim()) > 255 Then
                    ErrorLabel.Text = "Success Message allows only 255 characters."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If

            If txtpromocodenumber.Text.Trim().ToUpper.StartsWith("IN") Then
                Dim objFlag As Boolean = False
                Dim objPUSH As ListItem
                For Each objPUSH In PUSH.Items
                    If objPUSH.Selected = True Then
                        objFlag = True
                    End If
                Next
                If objFlag = False Then
                    ErrorLabel.Text = "Please select Industry."
                    ErrorLabel.Visible = True
                    Return False
                End If

            End If
            If txtpromocodenumber.Text.Trim().ToUpper.StartsWith("MO") Then
                Dim objFlag As Boolean = False
                Dim objPUSH As ListItem
                For Each objPUSH In lstSelected.Items
                    objFlag = True
                Next
                If objFlag = False Then
                    ErrorLabel.Text = "Please select Model."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            If txtpromocodenumber.Text.Trim().ToUpper.StartsWith("RE") Then
                Dim objFlag As Boolean = False
                Dim objPUSH As ListItem
                For Each objPUSH In lstResellerName.Items
                    If objPUSH.Selected = True Then
                        objFlag = True
                    End If
                Next
                If objFlag = False Then
                    ErrorLabel.Text = "Please select Reseller."
                    ErrorLabel.Visible = True
                    Return False
                End If
            End If
            If txtpromocodenumber.Text.Trim().ToUpper.StartsWith("CB") Then
                Dim objFlag As Boolean = False
                Dim objPUSH As ListItem
                For Each objPUSH In PUSH.Items
                    If objPUSH.Selected = True Then
                        objFlag = True
                    End If
                Next
                For Each objPUSH In lstSelected.Items
                    objFlag = True
                Next
                For Each objPUSH In lstResellerName.Items
                    If objPUSH.Selected = True Then
                        objFlag = True
                    End If
                Next
                If objFlag = False Then
                    ErrorLabel.Text = "Please select Industry/Model/Reseller."
                    ErrorLabel.Visible = True
                    Return False
                End If

            End If

            If Convert.ToDateTime(txtPromoStartdate.Text.Trim()) > Convert.ToDateTime(txtpromoenddate.Text.Trim()) Then
                ErrorLabel.Text = "Promo Start date should be less than or equal to Promo end date."
                ErrorLabel.Visible = True
                Return False
            End If
            If Convert.ToDateTime(txtpromoenddate.Text.Trim()) > Convert.ToDateTime(txtproregenddate.Text.Trim()) Then
                ErrorLabel.Text = "Promo end date should be less than or equal to Product registration end date."
                ErrorLabel.Visible = True
                Return False
            End If

            Return True

        End Function


        Protected Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
            Try
                Dim lstItem As ListItem
                Dim lstItemToCopy As ListItem
                For Each lstItem In lstBoxAvailable.Items
                    If lstItem.Selected = True Then
                        lstItemToCopy = New ListItem()
                        lstItemToCopy.Text = lstItem.Text
                        lstItemToCopy.Value = lstItem.Value
                        lstSelected.Items.Add(lstItemToCopy)
                        'lstItem.Enabled = False
                    End If
                Next lstItem

                Dim j, k As Int16
                k = lstBoxAvailable.Items.Count
                While k > 0
                    For j = 0 To k - 1
                        If lstBoxAvailable.Items(j).Selected = True Then
                            lstBoxAvailable.Items.Remove(lstBoxAvailable.Items(j))
                            Exit For
                        End If
                        'j += 1
                    Next
                    j = 0
                    k -= 1
                End While
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Protected Sub btnDeSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeSelect.Click
            Try
                Dim lstItem As ListItem
                Dim lstItemToCopy As ListItem
                For Each lstItem In lstSelected.Items
                    If lstItem.Selected = True Then
                        lstItemToCopy = New ListItem()
                        lstItemToCopy.Text = lstItem.Text
                        lstItemToCopy.Value = lstItem.Value
                        lstBoxAvailable.Items.Add(lstItemToCopy)
                        'lstItem.Enabled = False
                    End If
                Next lstItem
                Dim j, k As Int16
                k = lstSelected.Items.Count
                While k > 0
                    For j = 0 To k - 1
                        If lstSelected.Items(j).Selected = True Then
                            lstSelected.Items.Remove(lstSelected.Items(j))
                            Exit For
                        End If
                        'j += 1
                    Next
                    j = 0
                    k -= 1
                End While
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub LoadReseller()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetReseller()
            If Not oDS Is Nothing Then
                lstResellerName.DataTextField = "RESELLERNAME"
                lstResellerName.DataValueField = "RESELLERCODE"
                lstResellerName.DataSource = oDS.Tables(0).DefaultView()
                lstResellerName.DataBind()
            Else
                Throw New ApplicationException("Exception Occured")
            End If
        End Sub
        Private Sub LoadModel()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS As DataSet = objPRManager.GetPRModels(ddlModelPrefix.SelectedValue.ToString())
            lstBoxAvailable.Items.Clear()
            'ddlModelPrefix.AutoPostBack = False

            If Not oDS Is Nothing Then

                lstBoxAvailable.DataTextField = "MODELNUMBER"
                lstBoxAvailable.DataValueField = "MODELNUMBER"
                lstBoxAvailable.DataSource = oDS.Tables(0)
                lstBoxAvailable.DataBind()
            Else
                Throw New ApplicationException("Exception occurred")
            End If


        End Sub

        Private Sub LoadIndustry()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetIndustry()
            PUSH.Items.Clear()
            If Not oDS Is Nothing Then
                PUSH.DataTextField = "INDUSTRY"
                PUSH.DataValueField = "INDUSTRYCODE"
                PUSH.DataSource = oDS.Tables(0).DefaultView()
                PUSH.DataBind()

            Else
                Throw New ApplicationException("Exception occurred")
            End If
        End Sub
        Private Sub LoadModelPrefix()
            Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
            Dim oDS = objPRManager.GetModelPrefix()
            ddlModelPrefix.Items.Clear()
            If Not oDS Is Nothing Then
                ddlModelPrefix.DataTextField = "MODELPREFIX"
                ddlModelPrefix.DataValueField = "MODELPREFIX"
                ddlModelPrefix.DataSource = oDS.Tables(0).DefaultView()
                ddlModelPrefix.DataBind()

            Else
                Throw New ApplicationException("Exception occurred")
            End If
            ddlModelPrefix.Items.Insert(0, New ListItem("Click..."))
        End Sub

        Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
            If ValidateData() Then
                Dim objPCILogger As New PCILogger() '6524
                Dim objPRManager As OnlineProductRegistrationManager = New OnlineProductRegistrationManager()
                If objPRManager.DuplicatePromotionalCode(txtpromocodenumber.Text.Trim.ToUpper()) Then
                    ErrorLabel.Text = "Promo code number already exist"
                Else

                    Try
                        '6524 start
                        objPCILogger.EventOriginApplication = "ServicesPLUS"
                        objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                        objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                        objPCILogger.OperationalUser = Environment.UserName
                        objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                        objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.EventOriginMethod = "cmdSave_Click"
                        objPCILogger.Message = "cmdSave_Click Failed"
                        objPCILogger.EventType = EventType.Add
                        '6524 end

                        objPRManager.BeginTransaction()
                        'Save data in to the PROMOTION_CODE 
                        objPRManager.SavePromotionalCode(txtpromocodenumber.Text.Trim.ToUpper(), txtmessage.Text.Trim(), Convert.ToDateTime(txtPromoStartdate.Text.Trim()), Convert.ToDateTime(txtpromoenddate.Text.Trim()), Convert.ToDateTime(txtproregenddate.Text.Trim()))
                        'Save data in to the PROMOTION_CODE_IND
                        Select Case txtpromocodenumber.Text.Trim.ToUpper().Substring(0, 2)
                            Case "IN"
                                Dim objPUSH As ListItem
                                For Each objPUSH In PUSH.Items
                                    If objPUSH.Selected = True Then
                                        objPRManager.SavePromoIndustry(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                    End If
                                Next
                            Case "MO"
                                Dim objPUSH As ListItem
                                For Each objPUSH In lstSelected.Items
                                    objPRManager.SavePromoModel(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                Next
                            Case "RE"
                                Dim objPUSH As ListItem
                                For Each objPUSH In lstResellerName.Items
                                    If objPUSH.Selected = True Then
                                        objPRManager.SavePromoReseller(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                    End If
                                Next

                            Case "CB"
                                Dim objPUSH As ListItem
                                For Each objPUSH In PUSH.Items
                                    If objPUSH.Selected = True Then
                                        objPRManager.SavePromoIndustry(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                    End If
                                Next
                                For Each objPUSH In lstSelected.Items
                                    objPRManager.SavePromoModel(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                Next
                                For Each objPUSH In lstResellerName.Items
                                    If objPUSH.Selected = True Then
                                        objPRManager.SavePromoReseller(txtpromocodenumber.Text.Trim.ToUpper(), objPUSH.Value)
                                    End If
                                Next

                        End Select

                        objPRManager.CommitTransaction()
                        ErrorLabel.Text = "Promo code number added Successfully"


                        objPRManager.RollbackTransaction()


                        objPRManager.RollbackTransaction()
                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                        objPRManager.RollbackTransaction()
                        objPCILogger.IndicationSuccessFailure = "Failure" '6524
                        objPCILogger.Message = "cmdSave_Click Failed. " & ex.Message.ToString() '6524
                    Finally
                        objPCILogger.PushLogToMSMQ() '6524
                    End Try

                End If
            End If
        End Sub

        Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
            txtpromocodenumber.Text = ""
            txtPromoStartdate.Text = ""
            txtpromoenddate.Text = ""
            txtmessage.Text = ""
            txtproregenddate.Text = ""
            LoadIndustry()
            LoadModelPrefix()
            LoadReseller()
            lstBoxAvailable.Items.Clear()
            lstSelected.Items.Clear()
        End Sub
    End Class
End Namespace

