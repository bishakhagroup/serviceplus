Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports System.Xml
Imports System.Data
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Drawing
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities
Imports Microsoft.Win32
Imports WinLDAP
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class UpdateSiteStatus
        Inherits UtilityClass

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
            If Request.UrlReferrer Is Nothing Then Response.Redirect("default.aspx")

        End Sub

        ''' <summary>
        ''' Logger Details added.
        ''' Event Log added for while changing the Status of the application
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        ''' Modified By : Rajkumar A
        ''' Modified On : 15 Dec 2009
        Protected Sub Bind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus.Click

            Dim objPCILogger As New PCILogger()
            objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
            objPCILogger.EventOriginMethod = "Bind_Click"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
            objPCILogger.EventType = EventType.SiteStatusChange
            objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

            If Not Session.Item("siamuser") Is Nothing Then

                'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524

                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID

                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber

                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity

                'objPCILogger.B2B_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId
                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524


            End If



            'If Not Session.Item("siamuser") Is Nothing Then
            'objPCILogger.UserIdentity = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
            'End If

            'objPCILogger.UserIdentitySequenceNumber = "Not available"

            If ConfigurationData.GeneralSettings.UnderMaintenance = "Y" Then

                ConfigurationData.GeneralSettings.UnderMaintenance = "N"
                btnStatus.Text = "Put Under Maintenance"
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "Site status changed to available"
                objPCILogger.PushLogToMSMQ()

            Else

                ConfigurationData.GeneralSettings.UnderMaintenance = "Y"
                btnStatus.Text = "Set Available"
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "Site status changed to maintenance"
                objPCILogger.PushLogToMSMQ()

            End If

            Session.Remove("unmnt")
            Session.Add("unmnt", ConfigurationData.GeneralSettings.UnderMaintenance)
            'Commneted By Navdeep kaur for resolving 6401
            Response.Redirect("redirect.aspx")

            'Page.ClientScript.RegisterStartupScript(Me.GetType(), "LoadFrame", "<script language=""javascript"">window.parent.location = 'redirect.aspx';</script>")

        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If ConfigurationData.GeneralSettings.UnderMaintenance = "Y" Then
                lblStatus.Text = "under maintenance"
                btnStatus.Text = "Set Available"
            Else
                lblStatus.Text = "available"
                btnStatus.Text = "Put Under Maintenance"
            End If
        End Sub
    End Class
End Namespace
