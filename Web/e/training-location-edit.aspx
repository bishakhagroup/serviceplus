<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_location_edit" CodeFile="training-location-edit.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_location_edit</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table3" style="Z-INDEX: 101; LEFT: 8px; WIDTH: 627px; POSITION: absolute; TOP: 8px; HEIGHT: 360px"
				align="center" border="0">
				<TR>
					<TD class="PageHead" style="HEIGHT: 2px" align="center" colSpan="1" height="2" rowSpan="1">Training 
						Location</TD>
				</TR>
				<TR>
					<TD class="PageHead" style="HEIGHT: 1px" align="center"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"
							Font-Size="Smaller"></asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 12px"><asp:label id="Label23" runat="server" CssClass="Body" Font-Size="Smaller" Font-Bold="True"> Location Information:</asp:label></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 167px" vAlign="top" align="left">
						<TABLE id="Table2" style="WIDTH: 624px; HEIGHT: 48px" width="624"
							border="1">
							<TBODY>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 26px" align="right"><asp:label id="labelName" runat="server" CssClass="Body"> Name:</asp:label></TD>
									<TD style="HEIGHT: 26px"><SPS:SPSTextBox id="txtName" runat="server" Width="240px"></SPS:SPSTextBox><asp:label id="LabelNameStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="labelAddress1" runat="server" CssClass="Body">Business Address:</asp:label></TD>
									<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtAddress1" runat="server" CssClass="Body" Width="240px"></SPS:SPSTextBox><asp:label id="LabelAddress1Star" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label><span class="Body"></span></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="labelAddress2" runat="server" CssClass="Body">Address 2nd Line:</asp:label></TD>
									<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtAddress2" runat="server" CssClass="Body" Width="240px"></SPS:SPSTextBox><asp:label id="LabelAddress2Star" runat="server" CssClass="redAsterick">
											</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="labelAddress3" runat="server" CssClass="Body">Address 3rd Line:</asp:label></TD>
									<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtAddress3" runat="server" CssClass="Body" Width="240px"></SPS:SPSTextBox><asp:label id="LabelAddress3Star" runat="server" CssClass="redAsterick">
											</asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 24px" align="right"><asp:label id="labelCity" runat="server" CssClass="Body">City:</asp:label></TD>
									<TD style="HEIGHT: 24px"><SPS:SPSTextBox id="txtCity" runat="server" CssClass="Body" Width="200px"></SPS:SPSTextBox><asp:label id="LabelCityStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 19px" align="right"><asp:label id="labelState" runat="server" CssClass="Body"> State:</asp:label></TD>
									<TD style="HEIGHT: 19px"><asp:dropdownlist id="ddlState" runat="server" CssClass="Body"></asp:dropdownlist><asp:label id="LabelStateStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 21px" align="right"><asp:label id="labelZip" runat="server" CssClass="Body"> Zip:</asp:label></TD>
									<TD style="HEIGHT: 21px"><SPS:SPSTextBox id="txtZip" runat="server" CssClass="Body" Width="72px"></SPS:SPSTextBox><asp:label id="LabelZipStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 19px" align="right"><asp:label id="labelCountry" runat="server" CssClass="Body">Country:</asp:label></TD>
									<TD style="HEIGHT: 19px"><asp:dropdownlist id="ddlCountry" runat="server" CssClass="Body" Width="344px"></asp:dropdownlist><asp:label id="LabelCountryStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="labelPhone" runat="server" CssClass="Body"> Phone:</asp:label></TD>
									<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtPhone" runat="server" CssClass="Body" Width="88px"></SPS:SPSTextBox><asp:label id="LabelPhoneStar" runat="server" CssClass="redAsterick">
											<span class="redAsterick">*</span></asp:label>&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="labelExtension" runat="server" CssClass="Body">Extension:</asp:label></TD>
									<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtExtension" runat="server" CssClass="Body" Width="72px"></SPS:SPSTextBox>&nbsp;<span class="bodyCopy">xxxx</span></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 20px" align="right"><asp:label id="Label1" runat="server" CssClass="Body">Fax:</asp:label></TD>
									<TD style="HEIGHT: 20px"><SPS:SPSTextBox id="txtFax" runat="server" CssClass="Body" Width="88px"></SPS:SPSTextBox>&nbsp;<span class="bodyCopy">xxx-xxx-xxxx</span></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 21px" vAlign="top" align="right"><asp:label id="Label2" runat="server" CssClass="Body">Accomodation:</asp:label></TD>
									<TD style="HEIGHT: 21px"><SPS:SPSTextBox id="txtAccomodation" runat="server" CssClass="Body" Width="480px" Height="90px"
											TextMode="MultiLine"></SPS:SPSTextBox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 21px" vAlign="top" align="right"><asp:label id="labelDirection" runat="server" CssClass="Body">Direction:</asp:label></TD>
									<TD style="HEIGHT: 21px"><SPS:SPSTextBox id="txtDirection" runat="server" CssClass="Body" Width="480px" Height="90px" TextMode="MultiLine"></SPS:SPSTextBox></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 133px; HEIGHT: 93px" vAlign="top" align="right"><asp:label id="Label26" runat="server" CssClass="Body">Notes:</asp:label></TD>
									<TD style="HEIGHT: 93px"><SPS:SPSTextBox id="txtNotes" runat="server" CssClass="Body" Width="480px" Height="90px" TextMode="MultiLine"></SPS:SPSTextBox></TD>
								</TR>
							</TBODY>
						</TABLE>
					</TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 19px" align="center">
						<TABLE id="Table1" style="WIDTH: 626px; HEIGHT: 27px" width="626"
							border="0">
							<TR>
								<TD align="center"><asp:button id="btnUpdate" runat="server" Text="Update"></asp:button></TD>
								<TD align="center">
									<asp:button id="btnCancel" runat="server" Text="Close"></asp:button></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
