Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports System.Data
Imports System.Text.RegularExpressions
Imports Sony.US.AuditLog
Imports ServicesPlusException
Imports ServicePLUSWebApp


Namespace SIAMAdmin



    Partial Class CustomerInfo
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents Button1 As System.Web.UI.WebControls.Button
        Protected WithEvents Label1 As System.Web.UI.WebControls.Label
        Protected WithEvents UserTypeDDL As System.Web.UI.WebControls.DropDownList
        Protected WithEvents USerStatusDDL As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ResetPassword As System.Web.UI.WebControls.Label
        Protected WithEvents tb_username As SPSTextBox
        Protected WithEvents lblTaxExempt As System.Web.UI.WebControls.Label
        Protected WithEvents rdbtnTaxExempt As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents divRightContent As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents divLeftContent As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents rvPhoneNumberTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVCityTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVStateTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVZipCodeTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVEmailAddressTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVFirstNameTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rvLastNameTextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVAddressLine1TextBox As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents rVtxtUserName As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents CustomerSubNav As CustomerDataSubNavigation
        'Public minPwdLength As Int16
        'Public maxPwdLength As Int16

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            'minPwdLength = Integer.Parse("0" + Application("MinPwdLength").ToString())
            'maxPwdLength = Integer.Parse("0" + Application("MaxPwdLength").ToString())
        End Sub

#End Region


#Region "   Page variables  "

        Private siamIdentity As String
        Private LdapID As String '7086
        Private customerObj As New Sony.US.ServicesPLUS.Core.Customer
        'Protected CustUserType As String
        Private Siamuser As Sony.US.siam.User
        Private sa As New SecurityAdministrator
        Dim siamUpdatedUser As Sony.US.siam.User = New Sony.US.siam.User
        Dim statusID As Integer
        Dim IsSuperUser As Boolean = False
        Dim IsCSAdmin As Boolean = False
        Dim IsTRAdmin As Boolean = False
        'Dim objUtilties As Utilities = New Utilities
#End Region


#Region "   Events  "


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim accvalidation As String = Request.QueryString("vu")

                If Not Page.IsPostBack Then
                    txtZipCode.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
                End If

                If Not accvalidation = "" Then
                    ErrorLabel.Text = "Account(s) have been validated."
                    ErrorLabel.Visible = True
                End If

                If Not Page.IsPostBack Then
                    LoadUserTypes()
                    controlMethod(sender, e)
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'If HttpContextManager.SelectedCustomer IsNot Nothing Then
            '    Dim cust As Customer = HttpContextManager.SelectedCustomer
            '    CustUserType = cust.UserType
            'Else
            '    CustUserType = ""
            'End If
            ' ASleight - Why are we wiping out the Customer object?
            'HttpContextManager.SelectedCustomer = Nothing

            'If ddlUserType.Text = "Validated Account Holder" Or ddlUserType.Text = "Pending Account Holder" Then '6782
            '    maxPwdLength = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthAccount
            'Else
            '    maxPwdLength = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount
            'End If
        End Sub

        Private Sub LoadUserTypes()
            ddlUserType.Items.Clear()
            ddlUserType.Items.Add(New ListItem(" Select One ", ""))
            ddlUserType.Items.Add(New ListItem("Credit Card Only", "C"))
            ddlUserType.Items.Add(New ListItem("Validated Account Holder", "A"))
            ddlUserType.Items.Add(New ListItem("Pending Account Holder", "P"))
            ddlUserType.Items.Add(New ListItem("Unknown", "U"))
            ddlUserType.Items.Add(New ListItem("Internal", "I")) '6935

        End Sub
#End Region


#Region "   Properties  "

        Public ReadOnly Property getIdentity() As String
            Get
                Return htxtSiamIdentity.Text  'siamIdentity.ToString()
            End Get
        End Property
        '7086 Starts
        Public ReadOnly Property getLDapID_prop() As String
            Get
                Return htxtLDAPID.Text  'LDAPID
            End Get
        End Property

        '7086 Ends
#End Region

#Region "   Methods     "

#Region "       Control Method      "
        Private Sub controlMethod(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim thisUser As User = Session(CustomerSessionVariable)
                Dim cm As New CustomerManager
                Dim caller As String = String.Empty

                siamIdentity = ucCustomerMenu.SiamID
                If (String.IsNullOrEmpty(siamIdentity)) Then Throw New System.Exception("Missing SIAM Identity")

                customerObj = cm.GetCustomer(siamIdentity)
                htxtSiamIdentity.Text = siamIdentity

                If (customerObj IsNot Nothing) Then
                    htxtLDAPID.Text = customerObj.LdapID
                    LdapID = customerObj.LdapID
                Else
                    'customer information not available in customer table, reach siam user table to fetch the same
                    Try
                        Dim fur As FetchUserResponse = sa.GetUser(siamIdentity)
                        If fur.SiamResponseMessage = Messages.GET_USER_COMPLETED.ToString() Then
                            Siamuser = fur.TheUser
                        Else
                            Session.Remove("LastError")
                            Session.Add("LastError", fur.SiamResponseMessage)
                            Response.Redirect("ErrorPage.aspx")
                        End If
                        'copy siam customer information into customer obj
                        'customerObj = cm.CreateCustomer()
                        'customerObj.SIAMIdentity = siamIdentity
                        'customerObj.CompanyName = Siamuser.CompanyName
                        'customerObj.FirstName = Siamuser.FirstName
                        'customerObj.LastName = Siamuser.LastName
                        'customerObj.Address.Line1 = Siamuser.AddressLine1
                        'customerObj.Address.Line2 = Siamuser.AddressLine2
                        'customerObj.Address.Line3 = Siamuser.AddressLine3
                        'customerObj.Address.City = Siamuser.City
                        'customerObj.Address.State = Siamuser.State
                        'customerObj.CountryCode = Siamuser.Country
                        'customerObj.Address.PostalCode = Siamuser.Zip
                        'customerObj.PhoneNumber = Siamuser.Phone
                        'customerObj.FaxNumber = Siamuser.Fax
                        'customerObj.EmailAddress = Siamuser.EmailAddress
                        'customerObj.EmailOk = True
                        ''customerObj.UserStatus = Siamuser.UserStatus'6668
                        'customerObj.StatusCode = Siamuser.UserStatusCode '6668
                        'customerObj.LdapID = LdapID
                        customerObj = Siamuser.ConvertToCustomer()
                        'customerObj.EmailOk = True
                        customerObj.LdapID = ucCustomerMenu.LdapID
                        'customerObj.SubscriptionID=Siamuser.
                        ' when no customer rec is available, 
                        ' add a new record
                        ' later update both customer and siam user tables when the update button is clicked
                        cm.RegisterCustomer(customerObj)

                        'htxtStatusId.Text = Siamuser.UserStatus '6668
                        htxtStatusId.Text = Siamuser.UserStatusCode '6668
                    Catch ex As Exception
                        Throw New System.Exception("Unable to obtain customer information using the supplied identity")
                    End Try
                End If

                HttpContextManager.SelectedCustomer = customerObj

                If Not sender.ID Is Nothing Then caller = sender.ID.ToString

                Select Case caller
                    Case Page.ID.ToString()
                        bindDDLs()
                        writeScriptsToPage()
                End Select

                'showRetryButton(customerObj, btnReTryB2BCall)
                checkUsersRole(thisUser)
                PopulateUI()
                txtDateReg.Text = cm.GetDateRegistered(siamIdentity)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Session.Item("LastError") = ex.Message
                Response.Redirect("ErrorPage.aspx")
            End Try
        End Sub
#End Region

        Private Function GenerateSiamIDImageString(ByVal strSiamIdentity As String) As String

            Dim strImg As String = "<img src='images/ImgName.JPG' style='border:0;' alt='' />"
            Dim strValue As String = ""

            Try
                For Each strChar In strSiamIdentity.Replace(" ", "").ToCharArray()
                    strValue += strImg.Replace("ImgName", strChar.ToString())
                Next
            Catch ex As Exception
                Return strValue
            End Try

            Return strValue
        End Function

        Private Sub PopulateUI()
            Try
                '-- add urls to links        
                lblsiamidimg.Text = GenerateSiamIDImageString(customerObj.SIAMIdentity) '7086
                txtCustomerID.Text = customerObj.CustomerID

                ' Dim LDAPId As String = String.Empty
                ' LDAPId = getLDAPId(customerObj)

                ' txtLDAPID.Text = LDAPId
                txtCompanyName.Text = customerObj.CompanyName
                txtCompanyName.Enabled = False
                txtFirstName.Text = customerObj.FirstName
                txtLastName.Text = customerObj.LastName
                txtAddressLine1.Text = customerObj.Address.Line1
                txtAddressLine2.Text = customerObj.Address.Line2
                txtCity.Text = customerObj.Address.City
                ddlCountry.SelectedValue = customerObj.Address.Country
                bindStateDDL()
                'ZipCode()
                ddlState.SelectedValue = customerObj.Address.State
                txtZipCode.Text = customerObj.Address.PostalCode

                If ddlCountry.SelectedValue = "US" Then
                    lblZipCode.Text = "Zip Code"
                    txtZipCode.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                Else
                    lblZipCode.Text = "Postal Code"
                    txtZipCode.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                End If

                txtPhoneNumber.Text = customerObj.PhoneNumber
                txtExtension.Text = customerObj.PhoneExtension
                txtFaxNumber.Text = customerObj.FaxNumber
                txtEmailAddress.Text = customerObj.EmailAddress
                txtSubscriptionID.Text = customerObj.SubscriptionID
                isSubscriptionActive.Checked = customerObj.IsSubscriptionActive
                txtLastLoginTime.Text = customerObj.LastLoginTime
                If customerObj.EmailOk Then
                    Me.rdbtnSendInfo.SelectedIndex = 0
                Else
                    Me.rdbtnSendInfo.SelectedIndex = 1
                End If

                txtEnableDocumentIDNumber.Text = customerObj.Enabledocumentidnumber

                If customerObj.UserType = "C" Then
                    'txtUserType.Text = "Credit card only"
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("A"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("U"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("I")) '6935
                    ddlUserType.Items.FindByValue("C").Selected = True
                ElseIf customerObj.UserType = "P" Then
                    'txtUserType.Text = "Pending account holder"
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("A"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("U"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("I")) '6935
                    ddlUserType.Items.FindByValue("P").Selected = True
                ElseIf customerObj.UserType = "A" Then
                    'txtUserType.Text = "Validated account holder"
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("U"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("I")) '6935
                    ddlUserType.Items.FindByValue("A").Selected = True
                    '6935 starts
                ElseIf customerObj.UserType = "I" Then
                    'txtUserType.Text = "Internal"
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("A"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("P"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("C"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("U"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue(""))
                    ddlUserType.Items.FindByValue("I").Selected = True
                    ddlUserType.Enabled = False
                    '6935 ends
                Else
                    'txtUserType.Text = "Unknown"
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("A"))
                    ddlUserType.Items.Remove(ddlUserType.Items.FindByValue("I")) '6935
                    ddlUserType.Items.FindByValue("U").Selected = True
                End If

                Session("custUserType") = customerObj.UserType

                txtUserName.Text = customerObj.UserName

                If customerObj.StatusCode = 1 Then
                    ddlUserStatus.Items.FindByText("Active").Selected = True
                Else
                    ddlUserStatus.Items.FindByText("Inactive").Selected = True
                End If
                Session("custUserStatus") = customerObj.StatusCode

                ' If thisUser.UserType = "AccountHolder" And (thisUser.AlternateUserId = "" Or thisUser.AlternateUserId = String.Empty) Then
                'If thisUser.UserType = "AccountHolder" Then
                If Not String.IsNullOrEmpty(customerObj.LdapID) Then
                    txtLDAPID.Text = customerObj.LdapID
                    ddlUserStatus.Enabled = True '6972
                Else 'ldapid data is null
                    ddlUserStatus.Enabled = False '6972
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       Build DDLs      "


        Private Sub bindDDLs()
            'bindLockStatusToDDL(LockStatusDDL)
            'Added by prasad for 2188
            bindCountryDDL()
            bindStateDDL()

        End Sub


        Private Sub bindLockStatusToDDL(ByRef thisDDL As DropDownList)
            Try
                If Not thisDDL Is Nothing Then
                    thisDDL.Items.Add(New ListItem("Locked", "1"))
                    thisDDL.Items.Add(New ListItem("Active", "0"))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        'Added by prasad for 2188
        Private Sub bindStateDDL()
            Try
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                Dim statedetail As DataSet = data_store.GetStateDetailsByCountry(ddlCountry.SelectedItem.Text)

                ddlState.Items.Clear()

                If statedetail.Tables(0).Rows.Count > 0 Then
                    With statedetail.Tables(0)
                        For i As Integer = 0 To .Rows.Count - 1
                            ddlState.Items.Add(New ListItem(.Rows(i)("STATE"), .Rows(i)("ABBREVIATION")))
                        Next
                    End With
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
#End Region

        Private Sub checkUsersRole(ByRef thisUser As User)
            Try
                If Not thisUser.Roles Is Nothing Then
                    For Each thisRole As Role In thisUser.Roles
                        Select Case thisRole.Name
                            Case enumRoles.SiamAdministrator.ToString() ' siam admin role
                                ' turn on the siam Admin links
                                IsSuperUser = True
                            Case enumRoles.CustomerService.ToString()   ' customer service role
                                ' turn on the CS links
                                IsCSAdmin = True
                            Case enumRoles.TrainingAdmin.ToString()
                                IsTRAdmin = True
                                'Case enumRoles.AccountValidators.ToString() ' account validator role
                                ' turn on the account validation links
                        End Select
                    Next
                End If
            Catch ex As Exception

            End Try
        End Sub

        Private Sub updateCustomersLockStatus()
            Try
                If Not customerObj Is Nothing Then
                    Dim thisSA As New SecurityAdministrator
                    Dim thisUser As User = thisSA.GetUser(customerObj.SIAMIdentity).TheUser

                    If thisUser IsNot Nothing Then
                        'If LockStatusDDL.SelectedValue = "0" Then
                        '    'thisSA.UnLockAccount(thisUser)
                        'ElseIf LockStatusDDL.SelectedValue = "1" Then
                        '    'thisSA.LockAccount(thisUser)
                        'End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub showRetryButton(ByRef thisCustomer As Customer, ByRef thisButton As Button)
            Try
                Dim sa As New SecurityAdministrator
                Dim thisUser As User = sa.GetUser(thisCustomer.SIAMIdentity.ToString()).TheUser

                'If thisUser.UserType = "AccountHolder" And (thisUser.AlternateUserId = "" Or thisUser.AlternateUserId = String.Empty) Then'6668
                If (thisUser.UserType = "A" Or thisUser.UserType = "P") And (thisUser.AlternateUserId = "" Or thisUser.AlternateUserId = String.Empty) Then '6668
                    thisButton.Enabled = True
                Else
                    thisButton.Enabled = False
                End If

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "       Helper Methods      "

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Private Sub GetUIData()
            siamUpdatedUser.CustomerID = txtCustomerID.Text.Trim() 'customerObj.CustomerID
            siamUpdatedUser.AlternateUserId = txtLDAPID.Text
            siamUpdatedUser.Identity = htxtSiamIdentity.Text
            siamUpdatedUser.UserId = txtUserName.Text.Trim()

            siamUpdatedUser.FirstName = txtFirstName.Text.Trim()
            siamUpdatedUser.LastName = txtLastName.Text.Trim()
            siamUpdatedUser.AddressLine1 = txtAddressLine1.Text.Trim()
            siamUpdatedUser.AddressLine2 = txtAddressLine2.Text.Trim()
            siamUpdatedUser.City = txtCity.Text.Trim()
            siamUpdatedUser.State = ddlState.SelectedValue
            siamUpdatedUser.Country = ddlCountry.SelectedValue
            siamUpdatedUser.Zip = txtZipCode.Text.Trim()

            siamUpdatedUser.Phone = txtPhoneNumber.Text.Trim()
            siamUpdatedUser.PhoneExtension = txtExtension.Text.Trim()
            siamUpdatedUser.Fax = txtFaxNumber.Text.Trim()
            siamUpdatedUser.EmailAddress = txtEmailAddress.Text.Trim()
            siamUpdatedUser.CompanyName = txtCompanyName.Text.Trim()
            siamUpdatedUser.UserType = ddlUserType.SelectedValue
            siamUpdatedUser.UserStatusCode = Int32.Parse(ddlUserStatus.SelectedValue)
            siamUpdatedUser.EmailOK = Boolean.Parse(rdbtnSendInfo.SelectedValue)
            siamUpdatedUser.Enabledocumentidnumber = txtEnableDocumentIDNumber.Text.Trim()
            siamUpdatedUser.SubscriptionID = txtSubscriptionID.Text.Trim()

            If HttpContextManager.SelectedCustomer IsNot Nothing Then
                customerObj.SequenceNumber = HttpContextManager.SelectedCustomer.SequenceNumber
            Else
                Dim custMgr As New Sony.US.ServicesPLUS.Process.CustomerManager
                If (custMgr Is Nothing) Then
                    Throw New System.Exception("Unable to create customer manager object.")
                End If
                customerObj = custMgr.GetCustomer(htxtSiamIdentity.Text) '7086
            End If
            siamUpdatedUser.SequenceNumber = customerObj.SequenceNumber

        End Sub

        Private Function validateData() As Boolean
            Dim thisPattern As String
            Dim sZip As String = txtZipCode.Text.Trim()
            Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")

            Try
                ErrorLabel.Text = ""
                ErrorLabel.Visible = False

                If txtZipCode.Text.Trim().Length > 0 Then
                    If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2) Then
                        objZipCodePattern = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                    End If

                    If Not objZipCodePattern.IsMatch(sZip) Then
                        ErrorLabel.Text = "Invalid Postal/Zip Code format <br/>"
                    End If

                    If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                        If txtZipCode.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US) Then
                            Me.ErrorLabel.Text += "Please enter valid Postal Code <br/>"
                        End If
                    Else
                        If txtZipCode.Text.Length < Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA) Then
                            Me.ErrorLabel.Text += "Please enter valid Postal Code <br/>"
                        End If
                    End If
                Else
                    Me.ErrorLabel.Text += "Please enter valid Postal Code <br/>"
                End If

                thisPattern = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                If Not Regex.IsMatch(txtEmailAddress.Text, thisPattern) Or (txtEmailAddress.Text.Trim).Length = 0 Then
                    Me.ErrorLabel.Text += "Please enter valid email address <br/>"
                    txtEmailAddress.Focus()
                End If
                If String.IsNullOrEmpty(txtUserName.Text.Trim()) Then
                    Me.ErrorLabel.Text += "Please enter user name <br/>"
                    txtUserName.Focus()
                End If
                If String.IsNullOrEmpty(txtPhoneNumber.Text.Trim()) Then
                    Me.ErrorLabel.Text += "Please enter phone number <br/>"
                    txtPhoneNumber.Focus()
                End If
                If String.IsNullOrEmpty(txtAddressLine1.Text.Trim()) Then
                    Me.ErrorLabel.Text += "Please enter address line1 <br/>"
                    txtAddressLine1.Focus()
                ElseIf txtAddressLine1.Text.Trim().Length > 35 Then
                    Me.ErrorLabel.Text += "Street Address must be 35 characters or less in length <br/>"
                    txtAddressLine1.Focus()
                End If
                If txtAddressLine2.Text.Trim().Length > 35 Then
                    Me.ErrorLabel.Text += "Address 2nd Line must be 35 characters or less in length <br/>"
                    txtAddressLine2.Focus()
                End If
                If String.IsNullOrEmpty(txtCity.Text.Trim()) Then
                    ErrorLabel.Text += "Please enter city <br/>"
                    txtCity.Focus()
                ElseIf (txtCity.Text.Trim().Length > 35) Then
                    ErrorLabel.Text += "City must be 35 characters or less in length <br/>"
                    txtCity.Focus()
                End If
                If ddlState.SelectedValue = "" Then
                    Me.ErrorLabel.Text += "Please select the state <br/>"
                    ddlState.Focus()
                End If
                If String.IsNullOrEmpty(txtFirstName.Text.Trim()) Then
                    Me.ErrorLabel.Text += "Please enter first name <br/>"
                    txtFirstName.Focus()
                End If
                If String.IsNullOrEmpty(txtLastName.Text.Trim()) Then
                    Me.ErrorLabel.Text += "Please enter last name <br/>"
                    txtLastName.Focus()
                End If

                'Validate UserType if it is changed
                Dim UserTypeOldValue As String = Session("custUserType").ToString()
                If ddlUserType.SelectedValue = "" Then
                    Me.ErrorLabel.Text += " Please select UserType "
                ElseIf ddlUserType.SelectedValue <> UserTypeOldValue Then
                    If UserTypeOldValue = "C" Then
                        If ddlUserType.SelectedValue <> "P" Then
                            Me.ErrorLabel.Text += " ""Credit Card Only"" User Type can be changed only to ""Pending Account Holder"" User Type <br/>"
                        End If
                    ElseIf UserTypeOldValue = "P" Then
                        If ddlUserType.SelectedValue <> "C" Then
                            Me.ErrorLabel.Text += " ""Pending Account Holder"" User Type can be changed only to ""Credit Card Only"" User Type "
                        End If
                    ElseIf UserTypeOldValue = "U" Then
                        If ddlUserType.SelectedValue = "A" Then
                            Me.ErrorLabel.Text += " Cannot change User Type to ""Validated Account Holder"" "
                        End If
                    ElseIf UserTypeOldValue = "A" Then
                        If ddlUserType.SelectedValue = "U" Then
                            Me.ErrorLabel.Text += " Cannot change User Type to ""Unknown"" "
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            ErrorLabel.Visible = Not String.IsNullOrEmpty(ErrorLabel.Text)   ' Make the Error Label visible if it has any text in it.
            Return Not ErrorLabel.Visible   ' Returns "False" if the Error Label is visible, denoting failure.
        End Function
#End Region


#End Region

        Private Sub btnUpdateUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdateUser.Click
            Dim objPCILogger As New PCILogger()
            Dim secAdmin As New SecurityAdministrator
            Dim cm As New CustomerManager()
            Dim errorstring As String = String.Empty
            Dim IsValidSubscription As Boolean = False

            Try
                If Session.Item("siamuser") IsNot Nothing Then
                    Dim SiamUser = DirectCast(Session.Item("siamuser"), Sony.US.siam.User)
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = SiamUser.EmailAddress '6524
                    objPCILogger.CustomerID = SiamUser.CustomerID
                    objPCILogger.CustomerID_SequenceNumber = SiamUser.SequenceNumber
                    objPCILogger.SIAM_ID = SiamUser.Identity
                    'objPCILogger.B2B_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId'6524
                    objPCILogger.LDAP_ID = SiamUser.AlternateUserId '6524
                End If
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnUpdateUser_Click(update User Profile)"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.UserIdentity = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Update
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                objPCILogger.Message = "Update Profile for UserID " & txtCustomerID.Text & " and SIAM Identity " & htxtSiamIdentity.Text
                objPCILogger.IndicationSuccessFailure = "Failure"

                GetUIData()
                If Not validateData() Then Return
                If Trace.IsEnabled Then Trace.Warn("Debuging", "save button clicked: updating siam user")

                'Try
                siamUpdatedUser.Password = "-1-1-1-1"

                If Not String.IsNullOrEmpty(txtSubscriptionID.Text) Then
                    Select Case (cm.IsValidSubscriptionID(customerObj.CustomerID, txtSubscriptionID.Text))
                        Case 1
                            'OK
                            isSubscriptionActive.Checked = True
                            IsValidSubscription = True
                        Case 2
                            ErrorLabel.Text = "The maximum number of ServicesPLUS users permitted for this subscription ID have already been registered. Please try another subscription ID."
                            ErrorLabel.Visible = True
                            isSubscriptionActive.Checked = False
                            IsValidSubscription = False
                            Return
                        Case 3
                            ErrorLabel.Text = "Invalid subscription ID. Please try another subscription ID."
                            ErrorLabel.Visible = True
                            isSubscriptionActive.Checked = False
                            IsValidSubscription = False
                            Return
                        Case Else
                            customerObj.SubscriptionID = siamUpdatedUser.SubscriptionID
                            isSubscriptionActive.Checked = True
                            IsValidSubscription = False
                    End Select
                End If

                If Session("custUserType").ToString() = "A" And Session("custUserStatus").ToString() = "3" And siamUpdatedUser.UserStatusCode = 1 And siamUpdatedUser.UserType = "A" Then
                    siamUpdatedUser.UserType = "P"
                End If

                customerObj = siamUpdatedUser.ConvertToCustomer()

                'Utilities.LogMessages("-- ADMIN Customer Info - Updating: LDAPID = " & customerObj.LdapID & ", Country = " & customerObj.CountryCode & ", State = " & customerObj.Address.State)
                cm.UpdateCustomerSubscription(customerObj)

                'Dim customer As Customer = Session.Item("customer")
                'Dim LDAPId As String = String.Empty
                'Utilities.LogMessages("-- ADMIN Customer Info - Updating LDAP ID: " & htxtLDAPID.Text)
                secAdmin.modifyUserInfo(txtLDAPID.Text, siamUpdatedUser, errorstring)
                objPCILogger.IndicationSuccessFailure = "Success"

                isSubscriptionActive.Checked = IsValidSubscription

                ' Refresh the Session Variable with the new value
                HttpContextManager.SelectedCustomer = customerObj
                ErrorLabel.Visible = True
                ErrorLabel.Text = "Profile has been sucessfully updated"
            Catch ex As Exception
                ErrorLabel.Visible = True
                objPCILogger.IndicationSuccessFailure = "Failure"
                If (ex.Message = "PasswordMatch") Then
                    'ErrorLabel.Visible = True
                    ErrorLabel.Text = "The new password can not be the same as one of the last three passwords used. Please choose another password."
                    'objPCILogger.IndicationSuccessFailure = "Failure"
                    'objPCILogger.PushLogToMSMQ()
                    'Exit Sub
                ElseIf (ex.Message = "DuplicateUser") Then
                    'ErrorLabel.Visible = True
                    ErrorLabel.Text = "User already exist. Please select another user name."
                    'objPCILogger.IndicationSuccessFailure = "Failure"
                    'objPCILogger.PushLogToMSMQ()
                    'Exit Sub
                ElseIf (ex.Message.Contains("The operation has timed out")) Then
                    'ErrorLabel.Visible = True
                    Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Text = "The operation has timed out"
                    'objPCILogger.IndicationSuccessFailure = "Failure"
                    'objPCILogger.PushLogToMSMQ()
                    'Exit Sub
                Else
                    'ErrorLabel.Visible = True
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    'objPCILogger.IndicationSuccessFailure = "Failure"
                    'objPCILogger.PushLogToMSMQ()
                    'Exit Sub
                End If
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Protected Sub btnUpdateUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateUser.Load

        End Sub

        Private Sub bindCountryDDL()
            Try
                'Fetch from DB 
                Dim data_store As New Sony.US.ServicesPLUS.Data.SISDataManager
                Dim countrydetail As DataSet = data_store.GetCountryDetails()

                If countrydetail.Tables(0).Rows.Count > 0 Then
                    With countrydetail.Tables(0)
                        For i As Integer = 0 To countrydetail.Tables(0).Rows.Count - 1
                            ddlCountry.Items.Add(New ListItem(countrydetail.Tables(0).Rows(i)("COUNTRYNAME"), countrydetail.Tables(0).Rows(i)("COUNTRYCODE")))
                        Next
                    End With
                End If

                'bind card type array to dropdown list control   
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub ddlcountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcountry.SelectedIndexChanged
            Try
                bindStateDDL()
                ZipCode()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ZipCode()
            If ddlCountry.SelectedValue = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2) Then
                lblZipCode.Text = "Zip Code"
                txtZipCode.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.US)
                txtZipCode.Text = ""
            Else
                lblZipCode.Text = "Postal Code"
                txtZipCode.MaxLength = Convert.ToInt32(ConfigurationData.GeneralSettings.GlobalData.ZipCode.CA)
                txtZipCode.Text = ""
            End If
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function

    End Class

End Namespace
