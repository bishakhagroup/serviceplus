<%@ Reference Control="~/e/InvoicePayment.ascx" %>
<%@ Reference Control="~/e/CreditCardPayment.ascx" %>
<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_order"
    CodeFile="training-order.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<%@ Reference Control="InvoicePayment.ascx" %>
<%@ Reference Control="AccountPayment.ascx" %>
<%@ Reference Control="CreditCardPayment.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Training Order</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>

  
    <style type="text/css">
        .style1
        {
            width: 133px;
            height: 23px;
        }
        .style2
        {
            width: 408px;
            height: 23px;
        }
    </style>
</head>
<body bgcolor="#ffffff">
    <form id="Form1" method="post" runat="server">
    <table style="width: 100%;" align="center" border="0">
        <tr>
            <td align="center" colspan="1" rowspan="1" class="headerBig">
                Training Order
            </td>
        </tr>
        <tr>
            <td class="PageHead" style="width: 100%" align="left">
                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td style="width: 50%; height: 11px">
                            <asp:Label ID="Label2" runat="server" CssClass="headerBig"> Order Information:</asp:Label>
                        </td>
                        <td style="width: 50%; height: 11px">
                            <asp:Label ID="Label6" runat="server" CssClass="headerBig"> Contact Information:</asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table id="Table13" width="100%" border="0">
                    <tr>
                        <td width="50%" valign="top">
                            <table width="100%" border="0">
                                <tr>
                                    <td align="Left" colspan="1" rowspan="1" width="30%">
                                        <asp:Label ID="Label1" runat="server" CssClass="bodyCopyBold">Number:</asp:Label>
                                    </td>
                                    <td style="width: 80%; height: 23px">
                                        &nbsp;
                                        <asp:Label ID="labelNumber" runat="server" CssClass="bodycopy" Width="160px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label3" runat="server" CssClass="bodyCopyBold">Order Date:</asp:Label>
                                    </td>
                                    <td class="style2">
                                        &nbsp;
                                        <asp:Label ID="labelOrderDate" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label4" runat="server" CssClass="bodyCopyBold">By:</asp:Label>
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:Label ID="labelBy" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label5" runat="server" CssClass="bodyCopyBold">Update Date:</asp:Label>
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:Label ID="labelUpdateDate" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="50%" valign="top">
                            <table id="Table2" align="middle" width="100%" border="0">
                                <tr>
                                    <td align="Left" colspan="1" rowspan="1" width="30%">
                                        <asp:Label ID="Label7" runat="server" CssClass="bodyCopyBold">Last Name:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 20px">
                                        &nbsp;
                                        <asp:Label ID="labelLastName" runat="server" CssClass="bodycopy" Width="208px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label9" runat="server" CssClass="bodyCopyBold">First Name:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 20px">
                                        &nbsp;
                                        <asp:Label ID="labelFirstName" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label11" runat="server" CssClass="bodyCopyBold">Phone:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 21px">
                                        &nbsp;
                                        <asp:Label ID="labelPhone" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label13" runat="server" CssClass="bodyCopyBold"> Extension:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 24px">
                                        &nbsp;
                                        <asp:Label ID="labelExtension" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label8" runat="server" CssClass="bodyCopyBold"> Fax:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 24px">
                                        &nbsp;
                                        <asp:Label ID="labelFax" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Left" width="30%">
                                        <asp:Label ID="Label12" runat="server" CssClass="bodyCopyBold"> E-mail Address:</asp:Label>
                                    </td>
                                    <td style="width: 412px; height: 24px">
                                        &nbsp;
                                        <asp:Label ID="labelEMail" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
            <table width="100%">
                <tr>
                    <td width="50%" valign="top" align="left">
                        <table id="Table2" style="width: 100%; height: 209px" cellpadding="0"
                            border="0">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="Label15" runat="server" CssClass="headerBig"> Class Information:</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" colspan="1" rowspan="1" width="30%">
                                    <asp:Label ID="Label14" runat="server" CssClass="bodyCopyBold">Class Number:</asp:Label>
                                </td>
                                <td style="height: 21px">
                                    &nbsp;
                                    <asp:Label ID="labelClassNumber" runat="server" CssClass="bodycopy" Width="208px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="Label16" runat="server" CssClass="bodyCopyBold">Course Number:</asp:Label>
                                </td>
                                <td style="height: 20px">
                                    &nbsp;
                                    <asp:Label ID="labelCourseNumber" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="Label18" runat="server" CssClass="bodyCopyBold">Course Title:</asp:Label>
                                </td>
                                <td style="height: 22px">
                                    &nbsp;
                                    <asp:Label ID="labelCourseTitle" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 133px; height: 24px" align="Left" width="30%">
                                    <asp:Label ID="lblStartDate" runat="server" CssClass="bodyCopyBold"> Start Date:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelStartDate" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="lblEndDate" runat="server" CssClass="bodyCopyBold"> End Date:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelEndDate" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="lblLocation" runat="server" CssClass="bodyCopyBold"> Location:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelLocation" runat="server" CssClass="bodycopy" Width="352px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="lblPrice" runat="server" CssClass="bodyCopyBold"> Price per Student:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelPrice" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="lblNoStudent" runat="server" CssClass="bodyCopyBold"> Number of Student:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelNoStudent" runat="server" CssClass="bodycopy" Width="80px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="Left" width="30%">
                                    <asp:Label ID="lblTotal" runat="server" CssClass="bodyCopyBold"> Total:</asp:Label>
                                </td>
                                <td style="height: 24px">
                                    &nbsp;
                                    <asp:Label ID="labelTotal" runat="server" CssClass="bodycopy" Width="104px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%" valign="top" align="left">
                        <asp:PlaceHolder ID="phPayment" runat="server"></asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </tr>
        <tr>
            <td style="width: 100%; height: 154px" valign="top" align="left">
                <table id="Table12" style="width: 100%; height: 156px" cellpadding="0"
                    border="0">
                    <tr>
                        <td style="width: 133px; height: 14px" align="left" colspan="1">
                            <asp:Label ID="lblPONumber" runat="server" CssClass="headerBig">PO Number:</asp:Label>
                        </td>
                        <td style="height: 14px">
                            &nbsp;
                            <asp:Label ID="lblPONoValue" runat="server" CssClass="bodycopy"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 133px; height: 14px" align="right" colspan="1">
                            &nbsp;
                        </td>
                        <td style="height: 14px">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:Label ID="Label23" runat="server" CssClass="headerBig"> Process Information:</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 133px; height: 14px" align="right" colspan="1" rowspan="1">
                            <asp:Label ID="labelInvoiceTitle" runat="server" CssClass="bodyCopyBold">Invoice Number:</asp:Label>
                        </td>
                        <td style="height: 14px">
                            <SPS:SPSTextBox ID="txtInvoiceNumber" runat="server" CssClass="bodycopy"></SPS:SPSTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 133px; height: 20px" valign="top" align="right">
                            <asp:Label ID="Label27" runat="server" CssClass="bodyCopyBold">Order Notes:</asp:Label>
                        </td>
                        <td>
                            <SPS:SPSTextBox ID="txtOrderNotes" runat="server" CssClass="bodycopy" Width="100%"
                                TextMode="MultiLine" Height="60px"></SPS:SPSTextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 495px; height: 15px">
                <asp:Label ID="lblStudentInfo" runat="server" CssClass="bodycopy" Font-Size="Smaller"
                    Font-Bold="True"> Student Information:</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 50%">
                <asp:DataGrid ID="studentDataGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    Width="100%">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditItemStyle BackColor="#999999" />
                    <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#284775" CssClass="BodyCopy" ForeColor="White" HorizontalAlign="Center"
                        Mode="NumericPages" />
                    <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="BodyCopy" />
                    <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="BodyCopy" />
                    <HeaderStyle BackColor="#284775" CssClass="BodyCopyBold" ForeColor="White" />
                    <SelectedItemStyle BackColor="Lime"></SelectedItemStyle>
                    <Columns>
                        <asp:BoundColumn DataField="Company" HeaderText="Company" ReadOnly="True">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="First Name">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>'
                                    NavigateUrl='' ID="HyperlinkFirstName">
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Last Name">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>'
                                    NavigateUrl='' ID="HyperlinkLastName">
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Status">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:DropDownList runat="server" CssClass="BodyCopy" ID="DDLStatus" AutoPostBack="True"
                                    OnSelectedIndexChanged="DDLStatus_SelectedIndexChangeed">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="EMail" ReadOnly="True" HeaderText="Email">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td style="width: 495px; height: 19px" align="center">
                <table id="Table1" style="width: 509px; height: 27px" cellpadding="0"
                    width="509" border="0">
                    <tr>
                        <td style="width: 301px" align="center">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update"></asp:Button>
                        </td>
                        <td style="width: 310px" align="center">
                            <asp:Button ID="btnClose" runat="server" Text="Close"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
