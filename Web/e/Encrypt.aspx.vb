Imports Sony.US.SIAMUtilities
Imports Sony.US.AuditLog
Imports Sony.US.siam
Imports ServicesPlusException


Namespace SIAMAdmin
    Partial Class Encrypt
        Inherits UtilityClass
        Private siamIdentity As String
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Page.ID = Request.Url.Segments.GetValue(1)

            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            '-- apply the datagrid style --
        End Sub

#End Region

#Region "Events"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        End Sub
#End Region

        Protected Sub btnEncrypt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEncrypt.Click
            Dim objPCILogger As New PCILogger()
            Try
                'prasad added on 11-12-2009
                'Event logging code block added for Encrypt functionality
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "btnEncrypt_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Encryption
                If Not Session.Item("siamuser") Is Nothing Then
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                    objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                    objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                    objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                    objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                txtOutPut.Text = Encryption.Encrypt(txtInput.Text, Encryption.PWDKey)
                objPCILogger.IndicationSuccessFailure = "Success"
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Protected Sub btnDecrypt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecrypt.Click
            Dim objPCILogger As New PCILogger()
            Try
                'prasad added on 11-12-2009
                'Event logging code block added for Decrypt functionality
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "btnDecrypt_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventType = EventType.Encryption
                If Not Session.Item("siamuser") Is Nothing Then
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                    objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                    objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                    objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                    objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                txtOutPut.Text = Encryption.DeCrypt(txtInput.Text, Encryption.PWDKey)
                objPCILogger.IndicationSuccessFailure = "Success"

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
            txtInput.Text = ""
            txtOutPut.Text = ""
            ErrorLabel.Text = ""
        End Sub
    End Class

End Namespace
