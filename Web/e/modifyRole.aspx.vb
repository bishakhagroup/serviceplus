Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System.Web.UI.WebControls
Imports Sony.US.AuditLog
Imports ServicesPlusException

Namespace SIAMAdmin

    Partial Class modifyRole
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    If (Request.QueryString("Identity").Length = 0) Then
                        Response.Redirect("srhRole.aspx")
                    Else
                        txtRoleID.Text = Request.QueryString("Identity")
                        txtRoleName.Text = Request.QueryString("rn")
                        If Request.QueryString("as") = "Yes" Then
                            chkActive.Checked = True
                        Else
                            chkActive.Checked = False
                        End If
                        Session.Add("snRoleID", txtRoleID.Text)
                        Session.Add("snRole", txtRoleName.Text)
                        Session.Add("snChecked", Request.QueryString("as"))
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click

            Dim oUsrRoleMgr As New UserRoleSecurityManager
            Dim iRetStatus As Integer = 0
            Dim sRoleName As String
            Dim iStatus As Integer
            Dim objPCILogger As New PCILogger()
            Try
                'prasad added on 14-12-2009
                'Event logging code added for Modify Role functionality. BugId #2100
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                objPCILogger.EventOriginMethod = "cmdSave_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                If Not Session.Item("siamuser") Is Nothing Then
                    'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                    objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                    objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                    objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                    objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                    objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524

                End If
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                sRoleName = txtRoleName.Text
                If chkActive.Checked = True Then
                    iStatus = 0
                    objPCILogger.EventType = EventType.Update
                Else
                    iStatus = 1
                    objPCILogger.EventType = EventType.Delete
                End If

                'oUsrRoleMgr.UpdateUserRole(sRoleName, iStatus, txtRoleID.Text)

                If ValidateUsrInputs() = True Then

                    iRetStatus = oUsrRoleMgr.UpdateUserRole(sRoleName, iStatus, txtRoleID.Text)

                    If iRetStatus = vbNull Then
                        ErrorLabel.Text = "Error on updating Role."
                        objPCILogger.IndicationSuccessFailure = "Failure"
                        objPCILogger.Message = "Error on updating Role Name " & sRoleName & " with Role ID " & txtRoleID.Text
                    Else
                        ErrorLabel.Text = "Role updated successfully."
                        objPCILogger.IndicationSuccessFailure = "Success"
                        If iStatus = 0 Then
                            objPCILogger.Message = "Role Name " & sRoleName & " with Role ID " & txtRoleID.Text & " is updated successfully"
                        Else
                            objPCILogger.Message = "Role Name " & sRoleName & " with Role ID " & txtRoleID.Text & " is deleted successfully"
                        End If

                    End If
                Else
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = "Invalid Role Name"
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ex.Message.ToString()
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function ValidateUsrInputs() As Boolean
            Dim iRet As Boolean = False

            If txtRoleName.Text.Trim.Length > 0 Then
                ErrorLabel.Text = ""
                iRet = True
            Else
                ErrorLabel.Text = "Please enter valid Role name."
                iRet = False
            End If
            Return iRet
        End Function

        Private Sub ResetValues()

            txtRoleID.Text = Session("snRoleID")
            txtRoleName.Text = Session("snRole")

            If Session("snChecked") = "Yes" Then
                chkActive.Checked = True
            Else
                chkActive.Checked = False
            End If
        End Sub

        Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
            'ResetValues()
            Response.Redirect("srhRole.aspx")
        End Sub
    End Class
End Namespace