Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls

Imports System.Text.RegularExpressions
Imports Sony.US.AuditLog



Namespace SIAMAdmin



    Partial Class CreditCardPayment
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private m_order As Sony.US.ServicesPLUS.Core.Order
        Private m_TrainingStartDate As String = String.Empty

        '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
        Private m_ReqContextValues As Object = Nothing
        '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region



        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            lblValidCCNumber.Visible = False
            Dim m_customer As Customer
            If Not IsPostBack Then
                LoadDDLS()
                'populate credit card types
                Dim card_types() As CreditCardType
                Dim pm As PaymentManager = New PaymentManager

                '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                Dim objPCILogger As New PCILogger()
                '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                card_types = pm.GetCreditCardTypes()
                ddlType.Items.Add(New ListItem("Select One", ""))
                Session.Add("training_card_types", card_types)

                For Each cc As CreditCardType In card_types
                    ddlType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                Next

                '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                Try
                    '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                    If Not m_order Is Nothing Then 'fill in existing order's bill info
                        'this is when the page is called for displaying the order
                        If m_TrainingStartDate = "TBD" Then
                            If DateDiff(DateInterval.Day, m_order.OrderDate, Date.Now.Date) < 30 Then
                                Me.txtCCNumber.Text = m_order.CreditCard.CreditCardNumber
                            Else
                                Me.txtCCNumber.Text = m_order.CreditCard.CreditCardNumberMasked
                            End If
                        Else
                            If DateDiff(DateInterval.Day, Convert.ToDateTime(m_TrainingStartDate), Date.Now.Date) < 30 Then
                                Me.txtCCNumber.Text = m_order.CreditCard.CreditCardNumber
                            Else
                                Me.txtCCNumber.Text = m_order.CreditCard.CreditCardNumberMasked
                            End If
                        End If

                        '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                        objPCILogger.CreditCardMaskedNumber = m_order.CreditCard.CreditCardNumberMasked
                        objPCILogger.CreditCardSequenceNumber = m_order.CreditCard.SequenceNumber
                        objPCILogger.EventOriginApplication = "ServicesPLUS"
                        objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                        objPCILogger.EventOriginMethod = "Page_Load"
                        objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                        objPCILogger.OperationalUser = Environment.UserName
                        If Not Session.Item("siamuser") Is Nothing Then
                            'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                            objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                            objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                            objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                            objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                            objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                        End If
                        objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                        objPCILogger.EventType = EventType.View
                        objPCILogger.IndicationSuccessFailure = "Success"
                        If Not GetReqContextValue() Is Nothing Then
                            objPCILogger.HTTPRequestObjectValues = GetReqContextValue()
                        End If
                        objPCILogger.Message = "Order No: - " + m_order.OrderNumber
                        '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                    End If

                    '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                Catch
                    objPCILogger.IndicationSuccessFailure = "Failure"
                Finally
                    objPCILogger.PushLogToMSMQ()
                End Try
                '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                'Commented by chandra for PCI
                Dim expireDate As Date = m_order.CreditCard.ExpirationDate
                ViewState.Add("training_Card_CardSequenceNumber", m_order.CreditCard.SequenceNumber)
                Dim ParseDate() As String = CType(expireDate, String).Split("/")
                Me.txtExpireationDate.Text = ParseDate(0) + "/" + ParseDate(2).Substring(0, 4)
                ddlType.SelectedValue = m_order.CreditCard.Type.CreditCardTypeCode

                Me.txtName.Text = m_order.BillTo.Attn
                txtCompanyName.Text = m_order.BillTo.Name
                Me.txtStreet1.Text = m_order.BillTo.Line1
                Me.txtStreet2.Text = m_order.BillTo.Line2
                Me.txtStreet3.Text = m_order.BillTo.Line3
                Me.TextBoxCity.Text = m_order.BillTo.City
                Me.DDLState.SelectedValue = m_order.BillTo.State
                Me.txtZip.Text = m_order.BillTo.PostalCode

            ElseIf Not Session.Item("customer") Is Nothing Then
                'this is when the training order is created
                m_customer = Session.Item("customer")
                Me.txtCompanyName.Text = m_customer.CompanyName
                Me.txtStreet1.Text = m_customer.Address.Line1
                Me.txtStreet2.Text = m_customer.Address.Line2
                Me.txtStreet3.Text = m_customer.Address.Line3
                Me.TextBoxCity.Text = m_customer.Address.City
                Me.DDLState.SelectedValue = m_customer.Address.State
                Me.txtZip.Text = m_customer.Address.PostalCode
            End If
        End Sub
        Public Property EnableDDLType() As Boolean
            Get
                Return ddlType.Enabled
            End Get
            Set(ByVal Value As Boolean)
                ddlType.Enabled = Value
            End Set
        End Property
        Public Property Order() As Sony.US.ServicesPLUS.Core.Order
            Get
                Return m_order
            End Get
            Set(ByVal Value As Sony.US.ServicesPLUS.Core.Order)
                m_order = Value
            End Set
        End Property
        Public Property TrainingStartDate() As String
            Get
                Return m_TrainingStartDate
            End Get
            Set(ByVal Value As String)
                m_TrainingStartDate = Value
            End Set
        End Property

        '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
        Public Property GetReqContextValue() As Object
            Get
                Return m_ReqContextValues
            End Get
            Set(ByVal Value As Object)
                m_ReqContextValues = Value
            End Set
        End Property
        '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************


        Public Function Save(ByRef customer As Customer) As Boolean

            Dim m_creditcard_modified As Boolean = False

            If Not ValidateForm() Then
                Return False
            End If

            '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
            Dim objPCILogger As New PCILogger()
            '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

            Dim tm As New CourseManager

            'Get and/or Add credit card type selected 
            Dim nYear As Integer
            Dim nMonth As Integer
            Dim nDay As Integer = 1 'doesn't matter
            Dim sCCNumber As String

            Dim delimStr As String = "/"
            Dim delimiter As Char() = delimStr.ToCharArray()
            Dim split As String() = Nothing

            split = Me.txtExpireationDate.Text.Split(delimiter)
            nMonth = Convert.ToInt16(split(0))
            nYear = Convert.ToInt16(split(1))
            sCCNumber = Me.txtCCNumber.Text
            Dim card_types() As CreditCardType
            card_types = Session.Item("training_card_types")

            Dim exp_date As New Date(nYear, nMonth, nDay)
            Dim credit_card_type As Sony.US.ServicesPLUS.Core.CreditCardType
            Dim whichCCType As Integer
            Dim index As Integer

            whichCCType = ddlType.SelectedIndex - 1
            credit_card_type = card_types.GetValue(whichCCType)
            '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
            Try
                '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                If Not m_order Is Nothing Then 'existing order
                    Dim card As CreditCard = Nothing
                    For index = 0 To customer.CreditCards.Length - 1 'try to find the existing card
                        If (customer.CreditCards(index).SequenceNumber = ViewState("training_Card_CardSequenceNumber").ToString()) _
                            And (customer.CreditCards(index).Type.CreditCardTypeCode = credit_card_type.CreditCardTypeCode) Then
                            card = customer.CreditCards(index)
                        End If
                    Next
                    Dim cm As CustomerManager = New CustomerManager
                    If (card Is Nothing) Then 'new card
                        card = customer.AddCreditCard(sCCNumber, exp_date, credit_card_type, "", "")
                        card.Action = CoreObject.ActionType.ADD
                        cm.UpdateCustomerCreditCard(Order.Customer)
                        '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                        m_creditcard_modified = True

                        objPCILogger.CreditCardMaskedNumber = m_order.CreditCard.CreditCardNumberMasked
                        objPCILogger.CreditCardSequenceNumber = m_order.CreditCard.SequenceNumber
                        objPCILogger.EventOriginApplication = "ServicesPLUS"
                        objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                        objPCILogger.EventOriginMethod = "Save"
                        objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                        objPCILogger.OperationalUser = Environment.UserName
                        If Not Session.Item("siamuser") Is Nothing Then
                            'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                            objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                            objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                            objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                            objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                            objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                        End If
                        objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                        objPCILogger.EventType = EventType.Add
                        objPCILogger.Message = "Order No: - " + m_order.OrderNumber
                        objPCILogger.IndicationSuccessFailure = "Success"
                        objPCILogger.HTTPRequestObjectValues = GetReqContextValue()
                        '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                    Else
                        Dim bValid As Boolean = False
                        If ddlType.SelectedIndex > 0 Then
                            If txtCCNumber.Text = "" Then
                                LabelCCNumber.CssClass = "redAsterick"
                                LabelCCNumberStar.Text = "*"
                                lblValidCCNumber.Text = "Please enter credit card number."
                                bValid = False
                            Else
                                Dim strCCErroMessage As String = String.Empty
                                bValid = True ' Sony.US.SIAMUtilities.Validation.IsCreditCardValid(txtCCNumber.Text, ddlType.SelectedItem.Text.Substring(0, 1).ToUpper(), strCCErroMessage)
                                lblValidCCNumber.Text = IIf(bValid, "", strCCErroMessage)
                                lblValidCCNumber.Visible = True
                                If bValid = False Then
                                    LabelCCNumber.CssClass = "redAsterick"
                                    txtCCNumber.Focus()
                                Else
                                    card.CreditCardNumber = sCCNumber
                                    ViewState("training_Card_CardNumber") = sCCNumber
                                    LabelCCNumber.CssClass = "Body"
                                End If
                            End If
                        End If
                        If bValid Then
                            cm.UpdateCustomerCreditCard(Order.Customer)

                            '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                            If card.Dirty Then
                                m_creditcard_modified = True
                            Else
                                m_creditcard_modified = False
                            End If

                            objPCILogger.CreditCardMaskedNumber = m_order.CreditCard.CreditCardNumberMasked
                            objPCILogger.CreditCardSequenceNumber = m_order.CreditCard.SequenceNumber
                            objPCILogger.EventOriginApplication = "ServicesPLUS"
                            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                            objPCILogger.EventOriginMethod = "Save"
                            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                            objPCILogger.OperationalUser = Environment.UserName
                            If Not Session.Item("siamuser") Is Nothing Then
                                'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                            End If
                            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                            objPCILogger.EventType = EventType.Update
                            objPCILogger.Message = "Order No: - " + m_order.OrderNumber
                            objPCILogger.IndicationSuccessFailure = "Success"
                            objPCILogger.HTTPRequestObjectValues = GetReqContextValue()
                        Else
                            objPCILogger.CreditCardMaskedNumber = m_order.CreditCard.CreditCardNumberMasked
                            objPCILogger.CreditCardSequenceNumber = m_order.CreditCard.SequenceNumber
                            objPCILogger.EventOriginApplication = "ServicesPLUS"
                            objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                            objPCILogger.EventOriginMethod = "Save"
                            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                            objPCILogger.OperationalUser = Environment.UserName
                            If Not Session.Item("siamuser") Is Nothing Then
                                'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                                objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                                objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                                objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                                objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                                objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                            End If
                            objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                            objPCILogger.EventType = EventType.Update
                            objPCILogger.Message = lblValidCCNumber.Text
                            objPCILogger.IndicationSuccessFailure = "Failure"
                            objPCILogger.HTTPRequestObjectValues = GetReqContextValue()
                        End If
                            '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                        End If

                    m_order.Dirty = False 'no change
                    If m_order.BillTo.Name <> Me.txtName.Text Then
                        m_order.BillTo.Name = Me.txtName.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.Line1 <> Me.txtStreet1.Text Then
                        m_order.BillTo.Line1 = Me.txtStreet1.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.Line2 <> Me.txtStreet2.Text Then
                        m_order.BillTo.Line2 = Me.txtStreet2.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.Line3 <> Me.txtStreet3.Text Then
                        m_order.BillTo.Line3 = Me.txtStreet3.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.City <> Me.TextBoxCity.Text Then
                        m_order.BillTo.City = Me.TextBoxCity.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.State <> Me.DDLState.SelectedItem.Text Then
                        m_order.BillTo.State = Me.DDLState.SelectedItem.Text
                        m_order.Dirty = True
                    End If
                    If m_order.BillTo.PostalCode <> Me.txtZip.Text Then
                        m_order.BillTo.PostalCode = Me.txtZip.Text
                        m_order.Dirty = True
                    End If
                    If m_order.CreditCard.SequenceNumber <> card.SequenceNumber Then
                        m_order.CreditCard.SequenceNumber = card.SequenceNumber
                        m_order.Dirty = True
                    End If

                    '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
                    If m_order.Dirty = True Then
                        m_creditcard_modified = True
                    End If
                    '********************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************
                Else 'create new order
                    Dim creditCard As CreditCard = customer.AddCreditCard(sCCNumber, exp_date, credit_card_type, "", "")

                    '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************

                    If creditCard.Dirty Then
                        m_creditcard_modified = True
                    Else
                        m_creditcard_modified = False
                    End If

                    objPCILogger.CreditCardMaskedNumber = creditCard.CreditCardNumberMasked
                    objPCILogger.CreditCardSequenceNumber = creditCard.SequenceNumber
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginMethod = "Save"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    If Not Session.Item("siamuser") Is Nothing Then
                        'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                        objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                        objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                        objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                        objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                        objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524
                    End If
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.EventType = EventType.Add
                    objPCILogger.Message = "Order No: - " + m_order.OrderNumber
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.HTTPRequestObjectValues = GetReqContextValue()
                    '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

                    Dim cm As New CustomerManager

                    cm.UpdateCustomer(customer) 'update customer with the credit card
                    Dim bill_to As Sony.US.ServicesPLUS.Core.Address = New Sony.US.ServicesPLUS.Core.Address
                    bill_to.Name = Me.txtCompanyName.Text
                    bill_to.Attn = txtName.Text
                    bill_to.Line1 = Me.txtStreet1.Text
                    bill_to.Line2 = Me.txtStreet2.Text
                    bill_to.Line3 = Me.txtStreet3.Text
                    bill_to.State = Me.DDLState.SelectedValue
                    bill_to.City = Me.TextBoxCity.Text
                    bill_to.PostalCode = Me.txtZip.Text
                    m_order = tm.Checkout(customer, creditCard, bill_to)
                End If
                '********************Changes done by Mujahid E Azam for Bug# 2099 Starts here***************
            Catch ex As Exception
                objPCILogger.Message = "Order No: - " + m_order.OrderNumber + " Failed... " + ex.Message
                objPCILogger.IndicationSuccessFailure = "Failure"
            Finally
                If (m_creditcard_modified Or objPCILogger.IndicationSuccessFailure = "Failure") Then
                    objPCILogger.PushLogToMSMQ()
                End If
            End Try
            '***************Changes done by Mujahid E Azam for Bug# 2099 Ends here***************

            Return True
        End Function
        Private Function ValidateForm() As Boolean
            Dim bValid As Boolean = True

            LabelCardStar.Text = ""
            LabelNameStar.Text = ""
            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelCityStar.Text = ""
            LabelZipStar.Text = ""
            LabelStateStar.Text = ""

            LabelCCNumberStar.Text = ""
            LabelExpStar.Text = ""


            If Not Order Is Nothing Then
                If Order.BillTo.Name <> Me.txtName.Text And Me.txtName.Text = "" Then
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Order.BillTo.Line1 <> Me.txtStreet1.Text And Me.txtStreet1.Text = "" Then
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Order.BillTo.City <> Me.TextBoxCity.Text And TextBoxCity.Text = "" Then
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Order.BillTo.State <> Me.DDLState.SelectedItem.Text And DDLState.SelectedIndex = 0 Then
                    LabelState.CssClass = "redAsterick"
                    LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                End If
            Else
                If Me.txtName.Text = "" Then
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Me.txtStreet1.Text = "" Then
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                End If

                If TextBoxCity.Text = "" Then
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If DDLState.SelectedIndex = 0 Then
                    LabelState.CssClass = "redAsterick"
                    LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                End If
            End If

            If Me.txtStreet1.Text.Length > 35 Then
                LabelLine1.CssClass = "redAsterick"
                LabelLine1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length</span>"
                Me.txtStreet1.Focus()
            End If

            If Me.txtStreet2.Text.Length > 35 Then
                LabelLine2.CssClass = "redAsterick"
                LabelLine2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length</span>"
                Me.txtStreet2.Focus()
            End If

            If Me.txtStreet3.Text.Length > 35 Then
                LabelLine3.CssClass = "redAsterick"
                LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length</span>"
                Me.txtStreet3.Focus()
            End If

            If TextBoxCity.Text.Length > 35 Then
                LabelCity.CssClass = "redAsterick"
                LabelCityStar.Text = "<span class=""redAsterick"">City must be 35 characters or less in length.</span>"
                TextBoxCity.Focus()
            End If

            Dim sZip As String = Me.txtZip.Text.Trim()
            Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
            If Not objZipCodePattern.IsMatch(sZip) Then
                LabelZip.CssClass = "redAsterick"
                LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
            End If

            If Me.txtCCNumber.Text = "" Then
                LabelCCNumber.CssClass = "redAsterick"
                LabelCCNumberStar.Text = "<span class=""redAsterick"">*</span>"
            End If


            If IsDate(Me.txtExpireationDate.Text) Then
                Dim thisPattern As String = "^(\d{1,2}?\/\d{4}$)"
                If Not System.Text.RegularExpressions.Regex.IsMatch(Me.txtExpireationDate.Text, thisPattern) Then
                    bValid = False
                End If
            Else
                bValid = False
            End If

            If Me.txtCCNumber.Text = "" Then
                bValid = False
            End If

            If Me.ddlType.SelectedIndex = 0 Then
                bValid = False
            End If

            Return bValid

        End Function

        Private Sub LoadDDLS()
            DDLState.BackColor = Drawing.Color.White
            DDLState.Items.Add(New ListItem("Select One", ""))
            DDLState.Items.Add(New ListItem("AL", "AL"))
            DDLState.Items.Add(New ListItem("AK", "AK"))
            DDLState.Items.Add(New ListItem("AR", "AR"))
            DDLState.Items.Add(New ListItem("AZ", "AZ"))
            DDLState.Items.Add(New ListItem("CA", "CA"))
            DDLState.Items.Add(New ListItem("CO", "CO"))
            DDLState.Items.Add(New ListItem("CT", "CT"))
            DDLState.Items.Add(New ListItem("DC", "DC"))
            DDLState.Items.Add(New ListItem("DE", "DE"))
            DDLState.Items.Add(New ListItem("FL", "FL"))
            DDLState.Items.Add(New ListItem("GA", "GA"))
            DDLState.Items.Add(New ListItem("HI", "HI"))
            DDLState.Items.Add(New ListItem("IA", "IA"))
            DDLState.Items.Add(New ListItem("ID", "ID"))
            DDLState.Items.Add(New ListItem("IL", "IL"))
            DDLState.Items.Add(New ListItem("IN", "IN"))
            DDLState.Items.Add(New ListItem("KS", "KS"))
            DDLState.Items.Add(New ListItem("KY", "KY"))
            DDLState.Items.Add(New ListItem("LA", "LA"))
            DDLState.Items.Add(New ListItem("MA", "MA"))
            DDLState.Items.Add(New ListItem("MD", "MD"))
            DDLState.Items.Add(New ListItem("ME", "ME"))
            DDLState.Items.Add(New ListItem("MI", "MI"))
            DDLState.Items.Add(New ListItem("MN", "MN"))
            DDLState.Items.Add(New ListItem("MO", "MO"))
            DDLState.Items.Add(New ListItem("MS", "MS"))
            DDLState.Items.Add(New ListItem("MT", "MT"))
            DDLState.Items.Add(New ListItem("NC", "NC"))
            DDLState.Items.Add(New ListItem("ND", "ND"))
            DDLState.Items.Add(New ListItem("NE", "NE"))
            DDLState.Items.Add(New ListItem("NH", "NH"))
            DDLState.Items.Add(New ListItem("NJ", "NJ"))
            DDLState.Items.Add(New ListItem("NM", "NM"))
            DDLState.Items.Add(New ListItem("NV", "NV"))
            DDLState.Items.Add(New ListItem("NY", "NY"))
            DDLState.Items.Add(New ListItem("OH", "OH"))
            DDLState.Items.Add(New ListItem("OK", "OK"))
            DDLState.Items.Add(New ListItem("OR", "OR"))
            DDLState.Items.Add(New ListItem("PA", "PA"))
            DDLState.Items.Add(New ListItem("RI", "RI"))
            DDLState.Items.Add(New ListItem("SC", "SC"))
            DDLState.Items.Add(New ListItem("SD", "SD"))
            DDLState.Items.Add(New ListItem("TN", "TN"))
            DDLState.Items.Add(New ListItem("TX", "TX"))
            DDLState.Items.Add(New ListItem("UT", "UT"))
            DDLState.Items.Add(New ListItem("VA", "VA"))
            DDLState.Items.Add(New ListItem("VT", "VT"))
            DDLState.Items.Add(New ListItem("WA", "WA"))
            DDLState.Items.Add(New ListItem("WI", "WI"))
            DDLState.Items.Add(New ListItem("WV", "WV"))
            DDLState.Items.Add(New ListItem("WY", "WY"))
        End Sub

    End Class

End Namespace
