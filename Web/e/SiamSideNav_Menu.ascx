<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SiamSideNav_Menu.ascx.vb"
    Inherits="SIAMAdmin.SiamSideNav_Menu" %>
    
<table height="100%" width="100%" border="0">
    <tr height=100%>
        <td width="1px" height=100%>
        </td>
        <td valign="top" height=100%>
            <div id="divName" name ="divName" style="overflow: auto; width: 100%;  scrollbar-face-color: #c9d5e6;
                scrollbar-shadow-color: #632984; scrollbar-highlight-color: #632984; scrollbar-3dlight-color: #130919;
                scrollbar-darkshadow-color: #130919; scrollbar-track-color: #130919; scrollbar-arrow-color: black;">
                <asp:DataGrid ID="dgMenu" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                    Width="100%" GridLines="None" BorderStyle="None" CellPadding="1" Visible="False"
                    Style="text-decoration: none;">
                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                        Font-Underline="False" ForeColor="GhostWhite" />
                    <HeaderStyle BackColor="ControlDarkDark" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="GhostWhite" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                    <SPS:SPSLabel ID="lblGroup" runat="server" Text="GroupName" Visible=false ForeColor="White" CssClass="TableHeader"></SPS:SPSLabel>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton runat="server" CssClass="Nav2" ID="lnkFunction" Text='<%# DataBinder.Eval(Container, "DataItem.NAME") %>'>
                            <ItemStyle CssClass="Nav2" />
                                    </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                Font-Underline="False" />
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </td>
    </tr>
    <%--<tr height=20px>
        <td width=1px></td>
        <td valign="top" >
            <span class="bodyCopySMWhite"><br />Please be advised that some of the information shown in this application is considered to be Secret and should only be revealed to authorized persons.<br /></span>
        </td>
    </tr>--%>
</table>
