Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Text.RegularExpressions
Imports ServicesPlusException


Namespace SIAMAdmin


Partial Class InvoicePayment
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected m_order As Sony.US.ServicesPLUS.Core.Order
    Private m_cr As CourseReservation

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
        'Dim objUtilties As Utilities = New Utilities
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    LoadDDLS()
                    If Not m_cr Is Nothing Then
                        Dim status As Byte = m_cr.ParticipantsStatus
                        Select Case status
                            Case 1 'All Reserved
                                Me.labelInvoice.Text = "Customer registered for class, and requested an invoice which was automatically sent to them via email for payment."
                            Case 2 'All WaitListed
                                Me.labelInvoice.Text = "Customer requested to be added to the waitlist for the class, and requested a quote which was automatically sent to them via email."
                            Case 4 'All cancelled
                        End Select
                    End If
                    'If m_order Is Nothing Then 'Modified for fixing Bug# 310
                    '    Me.labelInvoice.Visible = False 'creating, no invoice known yet
                    'Else
                    If Not m_order Is Nothing Then
                        Me.txtCompanyName.Text = m_order.BillTo.Name
                        txtAtn.Text = m_order.BillTo.Attn
                        Me.txtStreet1.Text = m_order.BillTo.Line1
                        Me.txtStreet2.Text = m_order.BillTo.Line2
                        Me.txtStreet3.Text = m_order.BillTo.Line3
                        Me.txtZip.Text = m_order.BillTo.PostalCode
                        Me.DDLState.SelectedValue = m_order.BillTo.State
                        Me.TextBoxCity.Text = m_order.BillTo.City
                    ElseIf m_order Is Nothing And Session.Item("customer") Is Nothing Then
                        Me.labelInvoice.Visible = False 'creating, no invoice known yet
                    ElseIf m_order Is Nothing And Not Session.Item("customer") Is Nothing Then
                        Me.labelInvoice.Visible = False 'creating, no invoice known yet
                        'this is when the training order is created
                        Dim customer As Customer = Session.Item("customer")
                        Me.txtCompanyName.Text = customer.CompanyName

                        Me.txtStreet1.Text = customer.Address.Line1
                        Me.txtStreet2.Text = customer.Address.Line2
                        Me.txtStreet3.Text = customer.Address.Line3
                        Me.TextBoxCity.Text = customer.Address.City
                        Me.DDLState.SelectedValue = customer.Address.State
                        Me.txtZip.Text = customer.Address.PostalCode
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Public Property Reservation() As CourseReservation
            Get
                Return m_cr
            End Get
            Set(ByVal Value As CourseReservation)
                m_cr = Value
            End Set
        End Property
        Public Property Order() As Sony.US.ServicesPLUS.Core.Order
            Get
                Return m_order
            End Get
            Set(ByVal Value As Sony.US.ServicesPLUS.Core.Order)
                m_order = Value
            End Set
        End Property


        Private Function ValidateForm() As Boolean
            Dim bValid As Boolean = True
            Label2.Text = ""
            LabelNameStar.Text = ""
            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelCityStar.Text = ""
            LabelZipStar.Text = ""
            LabelStateStar.Text = ""

            If Me.txtAtn.Text = "" Then
                Label1.CssClass = "redAsterick"
                Label2.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            If Me.txtCompanyName.Text = "" Then
                LabelName.CssClass = "redAsterick"
                LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            If Me.txtStreet1.Text = "" Then
                LabelLine1.CssClass = "redAsterick"
                LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If
            If Me.txtStreet1.Text.Length > 35 Then
                LabelLine1.CssClass = "redAsterick"
                LabelLine1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length</span>"
                Me.txtStreet1.Focus()
                bValid = False
            End If

            If Me.txtStreet2.Text.Length > 35 Then
                LabelLine2.CssClass = "redAsterick"
                LabelLine2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length</span>"
                Me.txtStreet2.Focus()
                bValid = False
            End If

            If Me.txtStreet3.Text.Length > 35 Then
                LabelLine3.CssClass = "redAsterick"
                LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length</span>"
                Me.txtStreet3.Focus()
                bValid = False
            End If

            If TextBoxCity.Text = "" Then
                LabelCity.CssClass = "redAsterick"
                LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            ElseIf Me.TextBoxCity.Text.Length > 35 Then
                LabelCity.CssClass = "redAsterick"
                LabelCityStar.Text = "<span class=""redAsterick"">City must be 35 characters or less in length.</span>"
                TextBoxCity.Focus()
                bValid = False
            End If

            If DDLState.SelectedIndex = 0 Then
                LabelState.CssClass = "redAsterick"
                LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            Dim sZip As String = Me.txtZip.Text.Trim()
            Dim objZipCodePattern As New Regex("\d{5}(-\d{4})?")
            If Not objZipCodePattern.IsMatch(sZip) Then
                LabelZip.CssClass = "redAsterick"
                LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
                bValid = False
            End If

            Return bValid
        End Function

        Public Function Save(ByRef customer As Customer) As Boolean
            Try
                If Not ValidateForm() Then
                    Return False
                End If

                Dim tm As New CourseManager
                Dim bill_to As Sony.US.ServicesPLUS.Core.Address = New Sony.US.ServicesPLUS.Core.Address
                bill_to.Name = Me.txtCompanyName.Text
                bill_to.Attn = Me.txtAtn.Text
                bill_to.Line1 = Me.txtStreet1.Text
                bill_to.Line2 = Me.txtStreet2.Text
                bill_to.Line3 = Me.txtStreet3.Text
                bill_to.City = Me.TextBoxCity.Text
                bill_to.State = Me.DDLState.SelectedValue
                bill_to.PostalCode = Me.txtZip.Text
                m_order = tm.Checkout(customer, bill_to)
                Return True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try

        End Function

    Private Sub LoadDDLS()
        DDLState.Items.Add(New ListItem("Select One", ""))
        DDLState.Items.Add(New ListItem("AL", "AL"))
        DDLState.Items.Add(New ListItem("AK", "AK"))
        DDLState.Items.Add(New ListItem("AR", "AR"))
        DDLState.Items.Add(New ListItem("AZ", "AZ"))
        DDLState.Items.Add(New ListItem("CA", "CA"))
        DDLState.Items.Add(New ListItem("CO", "CO"))
        DDLState.Items.Add(New ListItem("CT", "CT"))
        DDLState.Items.Add(New ListItem("DC", "DC"))
        DDLState.Items.Add(New ListItem("DE", "DE"))
        DDLState.Items.Add(New ListItem("FL", "FL"))
        DDLState.Items.Add(New ListItem("GA", "GA"))
        DDLState.Items.Add(New ListItem("HI", "HI"))
        DDLState.Items.Add(New ListItem("IA", "IA"))
        DDLState.Items.Add(New ListItem("ID", "ID"))
        DDLState.Items.Add(New ListItem("IL", "IL"))
        DDLState.Items.Add(New ListItem("IN", "IN"))
        DDLState.Items.Add(New ListItem("KS", "KS"))
        DDLState.Items.Add(New ListItem("KY", "KY"))
        DDLState.Items.Add(New ListItem("LA", "LA"))
        DDLState.Items.Add(New ListItem("MA", "MA"))
        DDLState.Items.Add(New ListItem("MD", "MD"))
        DDLState.Items.Add(New ListItem("ME", "ME"))
        DDLState.Items.Add(New ListItem("MI", "MI"))
        DDLState.Items.Add(New ListItem("MN", "MN"))
        DDLState.Items.Add(New ListItem("MO", "MO"))
        DDLState.Items.Add(New ListItem("MS", "MS"))
        DDLState.Items.Add(New ListItem("MT", "MT"))
        DDLState.Items.Add(New ListItem("NC", "NC"))
        DDLState.Items.Add(New ListItem("ND", "ND"))
        DDLState.Items.Add(New ListItem("NE", "NE"))
        DDLState.Items.Add(New ListItem("NH", "NH"))
        DDLState.Items.Add(New ListItem("NJ", "NJ"))
        DDLState.Items.Add(New ListItem("NM", "NM"))
        DDLState.Items.Add(New ListItem("NV", "NV"))
        DDLState.Items.Add(New ListItem("NY", "NY"))
        DDLState.Items.Add(New ListItem("OH", "OH"))
        DDLState.Items.Add(New ListItem("OK", "OK"))
        DDLState.Items.Add(New ListItem("OR", "OR"))
        DDLState.Items.Add(New ListItem("PA", "PA"))
        DDLState.Items.Add(New ListItem("RI", "RI"))
        DDLState.Items.Add(New ListItem("SC", "SC"))
        DDLState.Items.Add(New ListItem("SD", "SD"))
        DDLState.Items.Add(New ListItem("TN", "TN"))
        DDLState.Items.Add(New ListItem("TX", "TX"))
        DDLState.Items.Add(New ListItem("UT", "UT"))
        DDLState.Items.Add(New ListItem("VA", "VA"))
        DDLState.Items.Add(New ListItem("VT", "VT"))
        DDLState.Items.Add(New ListItem("WA", "WA"))
        DDLState.Items.Add(New ListItem("WI", "WI"))
        DDLState.Items.Add(New ListItem("WV", "WV"))
        DDLState.Items.Add(New ListItem("WY", "WY"))
    End Sub
End Class

End Namespace
