Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports ServicesPlusException

Namespace ServicePLUSWebApp


Partial Class PersonalMessage
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
        'Dim objUtilties As Utilities = New Utilities
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
            Try
                Dim customer As Customer = Nothing

                If Session.Item("customer") IsNot Nothing Then customer = Session.Item("customer")

                If customer IsNot Nothing Then
                    Dim cat_man As New CatalogManager
                    Dim my_cat_items() As CustomerCatalogItem
                    Dim my_reminders() As CustomerCatalogItem = Nothing

                    LabelMessage.Text = ""

                    'my_cat_items = cat_man.MyCatalog(customer)
                    my_cat_items = cat_man.MyReminder(customer)
                    If Not my_cat_items Is Nothing Then
                        my_reminders = cat_man.MyCatalogReminders(my_cat_items)
                    End If
                    Dim NumReminders As String = "0"

                    If my_reminders IsNot Nothing Then
                        NumReminders = my_reminders.Length.ToString() + " "
                    End If

                    Dim Name As String
                    Name = customer.FirstName + " " + customer.LastName
                    'dont repeat the member name, member page redesign as a part of tech bulletin development
                    'Deepa V, Jul 21,2006
                    'LabelMessage.Text = ("<span class=""memberName"">" + Name + "</span><br/><span class=""memberMessage""><a class=""memberMessage"" href=""./ViewMyCatalog.aspx"">You have <span class=""redAsterick"">" + NumReminders + "</span> reminders in your catalog.</a></span>")
                    LabelMessage.Text = ("<span class=""memberMessage""><a class=""memberMessage"" href=""./ViewMyCatalog.aspx"">" + Resources.Resource.mem_hdr_msg + "<span class=""redAsterick"">" + NumReminders + "</span>" + Resources.Resource.mem_hdr_msg_1 + " </a></span>")
                End If
            Catch ex As Exception
                LabelMessage.Text = Utilities.WrapExceptionforUI(ex)
                LabelMessage.CssClass = "redAsterick"
            End Try
        End Sub

End Class

End Namespace
