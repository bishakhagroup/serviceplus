Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports System.Drawing
Imports ServicesPlusException


Namespace SIAMAdmin

    Partial Class training_course_list
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim cdm As New CourseDataManager
                Dim css As Course() = Nothing

                Try
                    writeScriptsToPage()
                    Select Case rdoStatus.SelectedIndex
                        Case 0
                            css = cdm.GetActiveCourse("1", "0")
                        Case 1
                            css = cdm.GetActiveCourse("1", "1")
                        Case 2
                            css = cdm.GetActiveCourse("2", "1")
                    End Select
                    Session.Add("training-course-list", css)
                    Me.coursesDataGrid.DataSource = css
                    Me.coursesDataGrid.DataBind()
                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Sub coursesDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                coursesDataGrid.CurrentPageIndex = e.NewPageIndex
                Me.coursesDataGrid.DataSource = Session("training-course-list")
                Me.coursesDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            'coursesDataGrid.ApplyStyle(GetStyle(520))
            coursesDataGrid.AllowPaging = True
            coursesDataGrid.PageSize = 10
            coursesDataGrid.AutoGenerateColumns = False
            coursesDataGrid.GridLines = GridLines.Horizontal
            coursesDataGrid.AllowSorting = True
            coursesDataGrid.CellPadding = 3
            coursesDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            coursesDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            coursesDataGrid.PagerStyle.Height = Unit.Pixel(10)
            coursesDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            coursesDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            coursesDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            coursesDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            coursesDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            coursesDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            coursesDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub coursesDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles coursesDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub coursesDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles coursesDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(0).FindControl("lnkCourse"), HyperLink)
                    hyperLink.NavigateUrl = "training-course-edit.aspx?LineNo=" + (e.Item.ItemIndex() + coursesDataGrid.CurrentPageIndex * coursesDataGrid.PageSize).ToString()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Response.Redirect("training-course-edit.aspx")
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Protected Sub btnSearchCourseNumber_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchCourseNumber.Click
            Try
                Dim css As Course() = Session.Item("training-course-list")
                Dim iCount As Integer = 0
                Dim bfound As Boolean = False
                For Each crs As Course In css
                    If crs.Number.ToUpper() = txtCourseNumber.Text.Trim().ToUpper() Then
                        Response.Redirect("training-course-edit.aspx?LineNo=" + iCount.ToString())
                        bfound = True
                    End If
                    iCount += 1
                Next
                If bfound = False Then
                    lblCourseSearch.Text = "Course number " + txtCourseNumber.Text.Trim() + " does not exist."
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub rdoStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoStatus.SelectedIndexChanged
            Dim cdm As New CourseDataManager
            Dim css As Course() = Nothing

            Try
                coursesDataGrid.CurrentPageIndex = 0
                writeScriptsToPage()
                Select Case rdoStatus.SelectedIndex
                    Case 0
                        css = cdm.GetActiveCourse("1", "0")
                    Case 1
                        css = cdm.GetActiveCourse("1", "1")
                    Case 2
                        css = cdm.GetActiveCourse("2", "1")
                End Select
                Session.Add("training-course-list", css)
                Me.coursesDataGrid.DataSource = css
                Me.coursesDataGrid.DataBind()
                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
    End Class

End Namespace
