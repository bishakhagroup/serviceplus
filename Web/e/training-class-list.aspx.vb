Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports System.Drawing

Namespace SIAMAdmin

    Partial Class training_class_list
        Inherits UtilityClass

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
            ApplyDataGridStyle()
        End Sub

#End Region


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    writeScriptsToPage()
                    Dim cdm As New CourseDataManager
                    Dim css As CourseSchedule() = Nothing

                    Select Case rdoStatus.SelectedIndex
                        Case 0
                            css = cdm.GetActiveCourseClass("1", "0")
                        Case 1
                            css = cdm.GetActiveCourseClass("1", "1")
                        Case 2
                            css = cdm.GetActiveCourseClass("2", "1")
                    End Select

                    Session.Add("training-class-list", css)
                    Me.classesDataGrid.DataSource = css
                    Me.classesDataGrid.DataBind()


                    ErrorLabel.Visible = True


                    ErrorLabel.Visible = True
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End Try
            End If
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Response.Redirect("training-class-edit.aspx")
        End Sub

        '-- helper scripts --
        Private Sub writeScriptsToPage()
            '-- this script will make sure that this page is only accessed via the iform tag in default.aspx -         
            RegisterClientScriptBlock("redirect", "<script language=""javascript"">if ( parent.frames.length === 0 ){window.location.replace(""" + MyBase.EntryPointPage + """)}</script>")

            '-- this will clear the status bar on the browser 
            RegisterStartupScript("ClearStatus", "<script language=""javascript"">window.parent.status = ""Done."";</script>")

            '-- this script is used to confirm that a user wants to remove an application -
            RegisterStartupScript("RemoveApplication", "<script language=""javascript"">function DeleteOp(itemId){var ok = window.confirm(""Are you sure ?"");if (ok === true){document.location.href=""" + Page.Request.Url.Segments.GetValue(1).ToString() + "?Remove="" + itemId;}}</script>")
        End Sub

        Sub classesDataGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                classesDataGrid.CurrentPageIndex = e.NewPageIndex
                Me.classesDataGrid.DataSource = Session("training-class-list")
                Me.classesDataGrid.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ApplyDataGridStyle()
            'classesDataGrid.ApplyStyle(GetStyle(520))
            classesDataGrid.AllowPaging = True
            classesDataGrid.PageSize = 10
            classesDataGrid.AutoGenerateColumns = False
            classesDataGrid.GridLines = GridLines.Horizontal
            classesDataGrid.AllowSorting = True
            classesDataGrid.CellPadding = 3
            classesDataGrid.PagerStyle.Mode = PagerMode.NumericPages
            classesDataGrid.HeaderStyle.Height = Unit.Pixel(30)
            classesDataGrid.PagerStyle.Height = Unit.Pixel(10)
            classesDataGrid.HeaderStyle.Font.Size = FontUnit.Point(10)
            classesDataGrid.HeaderStyle.ForeColor = Color.GhostWhite
            classesDataGrid.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            classesDataGrid.ItemStyle.Font.Size = FontUnit.Point(10)
            classesDataGrid.ItemStyle.ForeColor = Color.GhostWhite
            classesDataGrid.AlternatingItemStyle.Font.Size = FontUnit.Point(10)
            classesDataGrid.AlternatingItemStyle.ForeColor = Color.GhostWhite
        End Sub

        Private Sub classesDataGrid_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles classesDataGrid.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub classesDataGrid_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles classesDataGrid.ItemDataBound
            Try
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    Dim hyperLink As HyperLink = CType(e.Item.Cells(0).FindControl("lnkClass"), HyperLink)
                    hyperLink.NavigateUrl = "training-class-edit.aspx?LineNo=" + (e.Item.ItemIndex() + classesDataGrid.CurrentPageIndex * classesDataGrid.PageSize).ToString
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnSearchPartNumber_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchPartNumber.Click
            Try
                Dim css As CourseSchedule() = Session.Item("training-class-list")
                Dim iCount As Integer = 0
                Dim bfound As Boolean = False
                For Each cs As CourseSchedule In css
                    If cs.PartNumber.ToUpper() = txtPartNumber.Text.Trim().ToUpper() Then
                        Response.Redirect("training-class-edit.aspx?LineNo=" + iCount.ToString())
                        bfound = True
                    End If
                    iCount += 1
                Next
                If bfound = False Then
                    lblPartSearch.Text = "Part number " + txtPartNumber.Text.Trim() + " does not exist."
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub rdoStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoStatus.SelectedIndexChanged
            Dim cdm As New CourseDataManager
            Dim css As CourseSchedule() = Nothing

            Try
                classesDataGrid.CurrentPageIndex = 0
                writeScriptsToPage()

                Select Case rdoStatus.SelectedIndex
                    Case 0
                        css = cdm.GetActiveCourseClass("1", "0")
                    Case 1
                        css = cdm.GetActiveCourseClass("1", "1")
                    Case 2
                        css = cdm.GetActiveCourseClass("2", "1")
                End Select

                Session.Add("training-class-list", css)
                Me.classesDataGrid.DataSource = css
                Me.classesDataGrid.DataBind()
                ErrorLabel.Visible = True
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                ErrorLabel.Visible = True
            End Try
        End Sub
    End Class

End Namespace
