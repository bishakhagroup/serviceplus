<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_open_orders" CodeFile="training-open-orders.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>FindOrders</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="left" border="0">
				<TR>
					<TD class="PageHead" align="center" height="30">Manage&nbsp;Training&nbsp;Orders</TD>
				</TR>
				<tr>
					<td align="center" height="10"><asp:label id="ErrorLabel" runat="server" EnableViewState="False" ForeColor="Red" CssClass="Body"></asp:label></td>
				</tr>
				<tr>
					<td colSpan="5" height="5"></td>
				</tr>
				<tr>
					<td align="center"><span class="BodyHead">
							<asp:Label id="Label1" runat="server">Open Training Orders</asp:Label></span></td>
				</tr>
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
				
				<tr>
					<td height="10">&nbsp;&nbsp;<asp:Label id="Label2" runat="server" CssClass="BodyHead">Order Number</asp:Label>
					<SPS:SPSTextBox ID="txtOrderNumber"  CssClass="body" runat=server></SPS:SPSTextBox>
					<asp:Button id="btnSearchOrder" CssClass="body" runat=server Text="Search" /> 
					</td>
				</tr>
				<tr>
					<td height="10">&nbsp;&nbsp;<asp:label id="lblOrderSearch" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></td>
				</tr>
				<tr>
					<td height="10"><asp:Label id="lblActiveOrder" runat="server" CssClass="Body"></asp:Label>
					    <asp:LinkButton ID="lnkAllOrder" runat=server CssClass="Body"></asp:LinkButton>&nbsp;
					</td>
				</tr>
				<tr width=100%>
					<td ><asp:datagrid id="orderDataGrid" Width=100%  runat="server" OnPageIndexChanged="orderDataGrid_Paging">
							<Columns>
								<asp:TemplateColumn HeaderText="OrderNumber"   SortExpression="ReservationNumber">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ReservationNumber") %>' NavigateUrl='<%#"training-order.aspx?OrderNumber="+DataBinder.Eval(Container, "DataItem.ReservationNumber").ToString()%>' ID="Hyperlink1">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Start Date"  SortExpression="StartDate">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.CourseSchedule.StartDateDisplay")) %>' ID="labelStartDate">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update Date"  SortExpression="UpdateDate">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.UpdateDate")) %>' ID="labelUpdateDate">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Course Title"   SortExpression="Title">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.CourseSchedule.Course.Title")) %>' ID="labelTitle">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="First Name"   SortExpression="FirstName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.CustomerFirstName")) %>' ID="labelFirstName">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last Name"  SortExpression="LastName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.CustomerLastName")) %>' ID="labelLastName">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td colSpan="5">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
