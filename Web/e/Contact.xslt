<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Table border="0">
      <xsl:apply-templates select="//CONTACT" />
    </Table>
  </xsl:template>
  <xsl:template match="CONTACT">
    <tr>
      <td>
        <img height="1" src="images/spacer.gif" width="7"/>
      </td>
      <td>
        <table width="650" border="0">
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <xsl:if test="MAINTENANCE = 1">
            <tr runat="server" id = "maintenanceTR" visible="false">
              <td >
                <span class="bodyCopy">
                  <STRONG>
                    <img height="1" src="images/spacer.gif" width="7"/>The ServicesPLUS<sup>sm</sup> website you are trying to reach is currently undergoing maintenance.
                  </STRONG>
                </span>
              </td>
            </tr>
            <tr>
              <td>
                <img height="1" src="images/spacer.gif" width="7" />
                <span class="bodyCopy">We regret any inconvenience this may cause, and expect to restore the site on or about August 15th, 2011.</span>
              </td>
            </tr>
            <tr>
              <td>
                <img height="1" src="images/spacer.gif" width="7" />
                <span class="bodyCopy">Below are some alternate resources to assist you during this period; we will respond as quickly as possible.</span>
              </td>
            </tr>


          </xsl:if>
          <tr>
            <td>
              <img height="5" src="images/spacer.gif" width="7"/>
            </td>
          </tr>


          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td>
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Repair Parts :
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <table width="650" border="0">
                <tr>
                  <td style="HEIGHT: 10px">
                    <img height="1" src="images/spacer.gif" width="7"/>
                    <span class="bodyCopy">
                      Customer Service:  ordering, order tracking, returns
                    </span>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="40" />
                    <span class="bodyCopy">
                      Email: <A href="mailto:ProParts@am.sony.com?subject=Parts Customer Service:  ordering, tracking, research, and returns&amp;Body=Date:%0d%0d%0dName:%0dCompany Name:%0dSony Account Number:%0dShipping Address:%0dBilling Address:%0dPhone number:%0dIs this is a parts inquiry or Order?%0dPO Number:%0d%0dItem                                 Part number                    Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo you want  shipping method Next day,2 day, or ground transportation?%0d%0dOther Parts requests: ">ProParts@am.sony.com</A>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="7" />
                    <span class="bodyCopy">Parts research:</span>
                  </td>
                </tr>
                <tr>
                  <td style="padding-bottom: 10px">
                    <img height="1" src="images/spacer.gif" width="40" />
                    <span class="bodyCopy">
                      Email: <a href="mailto:ProPartsResearch@am.sony.com">ProPartsResearch@am.sony.com</a>
                    </span>
                  </td>
                </tr>

                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="7"/>
                    <span class="bodyCopy">
                      Phone: 800-538-7550, 9:00 a.m. - 7:00 p.m. ET, Monday - Friday.
                    </span>
                  </td>
                </tr>
                <!--<tr runat="server" id = "linkTR">
                  <td style="height: 14px">
                    <xsl:if test="MAINTENANCE = 0">
                      <img height="1" src="images/spacer.gif" width="7"/>
                      <xsl:if test="SESSION = 1">
                         <span class="bodyCopy">Link: <b>Please click on the <a href="#" onclick="window.open('RARequest.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=no,width=800,height=650'); return false;" class="body">Return Authorization Form</a>.</b></span>
                      </xsl:if>
                      <xsl:if test="SESSION = 0">
                          <span class="bodyCopy">Link: <b>Please click on the Return Authorization Form. (Requires login, to login please <a href="#" onclick="window.location.href='SignIn.aspx?RAForm=True'; return false;">click here</a>)
                            </b>
                          </span>
                      </xsl:if>
                    </xsl:if>
                  </td>
                </tr>-->
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Repair Services:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Email: <a href="mailto:SEL.BPC.Svc@am.sony.com">SEL.BPC.Svc@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43"/>
              <span class="bodyCopy">Phone: 866-766-9272.</span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Software:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Email: <A href="mailto:ProParts@am.sony.com?subject=Parts Customer Service:  ordering, tracking, research, and returns&amp;Body=Date:%0d%0d%0dName:%0dCompany Name:%0dSony Account Number:%0dShipping Address:%0dBilling Address:%0dPhone number:%0dIs this is a parts inquiry or Order?%0dPO Number:%0d%0dItem                                 Part number                    Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo you want  shipping method Next day,2 day, or ground transportation?%0d%0dOther Parts requests: ">ProParts@am.sony.com</A>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43"/>
              <span class="bodyCopy">Phone: 800-538-7550, 9:00 a.m. - 7:00 p.m. ET, Monday - Friday.</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Technical Subscriptions:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Email: <a href="mailto:BSSC.TIS@am.sony.com">BSSC.TIS@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43"/>
              <span class="bodyCopy">Phone : 408-352-4500</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Sony Training Institute:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" />
              <span class="bodyCopy">
                Research and sign up for classes, purchase training materials
              </span>
            </td>
          </tr>

          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Email: <a href="mailto:Training@am.sony.com">Training@am.sony.com</a>
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43"/>
              <span class="bodyCopy">Phone : 408-352-4500.</span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="1" src="images/spacer.gif" width="43"/>
              <span class="bodyCopy">Fax : 408-352-4212.</span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>SupportNET<sup>sm</sup> Service Agreements and SystemWatch<sup>sm</sup>
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <table width="650" border="0">
                <tr>
                  <td>
                    <img height="1" src="images/spacer.gif" width="40"/>
                    <span class="bodyCopy">
                      Email:<A href="mailto:SupportNET@am.sony.com">SupportNET@am.sony.com</A>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td >
                    <img height="1" src="images/spacer.gif" width="37"/>
                    <span class="bodyCopy">
                      Phone: 877-398-7669
                    </span>
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Registration for Professional Products and Extended Warranty Agreements:
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Not available; please retain and present your proof of purchase if service is needed
              </span>
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
          <tr>
            <td >
              <span class="bodyCopy">
                <STRONG>
                  <img height="1" src="images/spacer.gif" width="7"/>Technical Support for Professional Products :
                </STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" />
              <span class="bodyCopy">
                Product operational assistance
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Phone: 800-883-6817, 8:30 a.m. - 8:00 p.m. ET, Monday - Friday
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="7" />
              <span class="bodyCopy">
                Product technical diagnostic support
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                If you have a current SupportNET agreement
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Phone: 877-398-7669, 24x7x365
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                If you do not have a current SupportNET agreement
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Phone: 866-766-9272 option 1, 24x7x365
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Technical Support
              </span>
            </td>
          </tr>
          <tr>
            <td >
              <img height="1" src="images/spacer.gif" width="40" />
              <span class="bodyCopy">
                Email: <a href="mailto:bis.product.support@am.sony.com">bis.product.support@am.sony.com</a>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

