<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="uc1" TagName="TrainingDataSubNavigation" Src="TrainingDataSubNavigation.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.training_class_list" CodeFile="training-class-list.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>training_classe_list</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="Table1" width="100%" align="left" border="0">
				<TR>
					<TD class="PageHead" align="center" height="30">Manage&nbsp;Training&nbsp;Classes</TD>
				</TR>
				<TR>
					<TD align="center" height="10"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></TD>
				</TR>
				<TR>
					<TD align="left" height="10">
						<uc1:TrainingDataSubNavigation id="TrainingDataSubNavigation" runat="server"></uc1:TrainingDataSubNavigation></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 2px" align="center"></TD>
				</TR>
				
				<tr>
					<td height="10">&nbsp;</td>
				</tr>
				
				<tr>
					<td style="height: 10px">&nbsp;&nbsp;<asp:Label id="Label2" runat="server" CssClass="BodyHead">Part Number</asp:Label><SPS:SPSTextBox ID="txtPartNumber"  CssClass="body" runat=server></SPS:SPSTextBox><asp:Button id="btnSearchPartNumber" CssClass="body" runat=server Text="Search" /> <asp:label id="lblPartSearch" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:label></td>
				</tr>
				
			    <tr width=100%>
					<td style="height: 10px" width=100%>
					        <table width=100%>
					            <tr width=100%>
					                <td width=10%>
					                    &nbsp;<asp:Label id="Label4" runat="server" CssClass="BodyHead">Class Status</asp:Label>
					                </td>
					                <td width=30%>
					                    <asp:RadioButtonList ID="rdoStatus" AutoPostBack=true  CssClass="Body" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                            <asp:ListItem Value="2">Hidden</asp:ListItem>
                                            <asp:ListItem Value="3">Deleted</asp:ListItem>
                                        </asp:RadioButtonList>
					                </td>
					                <td align=right width=60%>
					                    <asp:button id="btnAdd" runat="server" Text="Add New Class"></asp:button>&nbsp;
					                </td>
					            </tr>
					        </table>
                    </td>
				</tr>
				
				<TR>
					<TD height="5"><asp:datagrid id="classesDataGrid" runat="server" Width=100% OnPageIndexChanged="classesDataGrid_Paging">
							<Columns>
								<asp:TemplateColumn HeaderText="Part#">
									<ItemTemplate>
										<asp:HyperLink runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PartNumber") %>' NavigateUrl='' ID="lnkClass">
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Course#">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Course.Number")) %>' ID="Label3">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Start Date">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.StartDateDisplay")) %>' ID="labelStartDate">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="End Date">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.EndDateDisplay")) %>' ID="Label2">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.Location.City")) & ", " & Format(DataBinder.Eval(Container, "DataItem.Location.State")) %>' ID="Label10">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update Date">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# Format(DataBinder.Eval(Container, "DataItem.UpdateDate")) %>' ID="Label101">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>&nbsp;</TD>
				</TR>
				
				</TABLE>
			&nbsp;
		</form>
	</body>
</HTML>
