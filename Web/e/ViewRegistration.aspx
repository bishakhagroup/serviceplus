<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.ViewRegistration" CodeFile="ViewRegistration.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>View Registration</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style2.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
	</HEAD>
	<body bgColor="#ffffff">
		<form id="Form1" method="post" runat="server">
			<table style="WIDTH: 497px; HEIGHT: 700px" align="center"
				border="0">
				<tr>
					<td class="PageHead" style="WIDTH: 565px" align="center" colSpan="1" height="30" rowSpan="1">
						View&nbsp;Registration</td>
				</tr>
				<TR>
					<TD class="PageHead" style="WIDTH: 565px" align="center"><asp:label id="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"
							Font-Size="Smaller"></asp:label></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 565px; HEIGHT: 106px">
						<TABLE id="Table1" style="WIDTH: 656px; HEIGHT: 100px" cellPadding="0"
							width="656" border="0">
							<TR>
								<TD style="WIDTH: 239px" vAlign="middle" colSpan="1" rowSpan="1"><asp:label id="Label3" runat="server" Font-Size="Smaller">Number:</asp:label><SPS:SPSTextBox id="nameTextBox" runat="server" Width="160px"></SPS:SPSTextBox></TD>
								<TD style="WIDTH: 217px"><asp:label id="Label6" runat="server" Font-Size="Smaller">Title:</asp:label><SPS:SPSTextBox id="titleTextBox" runat="server" Width="176px"></SPS:SPSTextBox></TD>
								<TD><asp:label id="Label7" runat="server" Font-Size="Smaller">Location:</asp:label><SPS:SPSTextBox id="locationTextBox" runat="server" Width="125px"></SPS:SPSTextBox></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 239px"><asp:label id="Label11" runat="server" Font-Size="Smaller">From:</asp:label><SPS:SPSTextBox id="fromTextBox" runat="server" Width="104px"></SPS:SPSTextBox>&nbsp;
									<asp:label id="Label12" runat="server" Font-Size="Smaller">(MM/DD/YYYY)</asp:label></TD>
								<TD style="WIDTH: 217px"><asp:label id="Label13" runat="server" Font-Size="Smaller">To:</asp:label><SPS:SPSTextBox id="toTextBox" runat="server" Width="104px"></SPS:SPSTextBox><asp:label id="Label14" runat="server" Font-Size="Smaller">(MM/DD/YYYY)</asp:label></TD>
								<TD></TD>
							</TR>
							<TR>
								<TD style="WIDTH: 239px"><asp:label id="Label15" runat="server" Font-Size="Smaller">Type:</asp:label><asp:dropdownlist id="typeDropDownList" runat="server" Width="104px"></asp:dropdownlist></TD>
								<TD style="WIDTH: 217px"><asp:label id="Label16" runat="server" Font-Size="Smaller">Category:</asp:label><asp:dropdownlist id="categoryDropDownList" runat="server" Width="136px"></asp:dropdownlist></TD>
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td style="WIDTH: 565px; HEIGHT: 12px"><asp:label id="Label2" runat="server" CssClass="Body" Font-Size="Smaller" Font-Bold="True">Course List:</asp:label></td>
				</tr>
				<tr>
					<td style="WIDTH: 565px; HEIGHT: 224px" vAlign="top" align="left">
						
						<div style="OVERFLOW: auto; WIDTH: 655px; HEIGHT: 266px"><asp:datagrid id="coursesDataGrid" runat="server" Font-Size="Smaller" Width="552px" Height="194px"
								OnSelectedIndexChanged="exceptionDataGrid_SelectedIndexChanged" AutoGenerateColumns="False">
								<SelectedItemStyle BackColor="Lime"></SelectedItemStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Course#">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Course.Number").ToString() %>' ID="LabelNumber">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Type">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="20px"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Course.Type").ToString() %>' ID="LabelType">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="MinorCategory" HeaderText="Category">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="StartDate" HeaderText="From">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="To">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Order.OrderNumber").ToString() %>' ID="Label4">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Location">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
										<ItemTemplate>
											<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber").ToString() %>' ID="Label5">
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="TotalAmount" HeaderText="Availability">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
									</asp:BoundColumn>
									<asp:BoundColumn DataField="Comments" HeaderText="Instructor">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
									</asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Participants">
										<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
										<ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
										<ItemTemplate>
											<asp:Button runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Order.OrderNumber").ToString() %>' ID="Label17">
											</asp:Button>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:datagrid></div>
					</td>
				</tr>
				<TR>
					<TD style="WIDTH: 565px; HEIGHT: 21px" align="center"><asp:button id="btnSubmit" runat="server" Enabled="False" Text="Submit"></asp:button></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>
