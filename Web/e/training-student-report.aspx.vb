Imports System.Data
Imports System.Xml
Imports System.Xml.Xsl
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace SIAMAdmin

    Partial Class training_student_report
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            MyBase.IsProtectedPage(True, True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Try
                    Dim ds As DataSet = GetReportData()
                    If Not ds Is Nothing Then
                        Me.labelTitle.Text = ds.Tables(0).Rows(0)("TITLE").ToString()
                        Me.labelCourseNumber.Text = ds.Tables(0).Rows(0)("COURSENUMBER").ToString()
                        Me.labelStartDate.Text = ds.Tables(0).Rows(0)("STARTDATE").ToString()
                        Me.labelEndDate.Text = ds.Tables(0).Rows(0)("ENDDATE").ToString()
                        Me.participantGrid.DataSource = ds.Tables(1)
                        Me.participantGrid.DataBind()
                    End If
                Catch ex As Exception
                    errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                    errorMessageLabel.Visible = True
                End Try
            End If
        End Sub

        Private Function GetReportData() As DataSet
            Dim ds As DataSet = Nothing
            Dim cdm = New CourseDataManager

            If (Not Request.QueryString("LineNo") Is Nothing) And (Not Session.Item("searchedClass") Is Nothing) Then
                Dim lineNumber As Integer = Convert.ToInt16(Request.QueryString("LineNo"))
                Dim courseSelected As CourseSchedule = CType(Session.Item("searchedClass"), Array)(lineNumber)

                ds = cdm.GetClassParticipants(courseSelected.PartNumber, "") 'return all status
            End If
            Return ds
        End Function

        Sub participantGrid_Paging(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)
            Try
                participantGrid.CurrentPageIndex = e.NewPageIndex
                Dim ds As DataSet = GetReportData()
                Me.participantGrid.DataSource = ds.Tables(1)
                Me.participantGrid.DataBind()
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                errorMessageLabel.Visible = True
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Dim objPCILogger As New PCILogger() '6524

            Dim ds As DataSet = GetReportData()
            If Not ds Is Nothing Then
                Try
                    '6524 start
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.EventOriginMethod = "btnExport_Click"
                    objPCILogger.Message = "Export Training Student Report Success."
                    objPCILogger.EventType = EventType.Others
                    '6524 end

                    'Transform the DataSet XML using transform.xslt
                    'and return the results to the client in Response.Outputstream.
                    Dim xmlDoc As XmlDataDocument = New XmlDataDocument(ds)
                    Dim xslTran As XslTransform = New XslTransform
                    xslTran.Load(Context.Server.MapPath("CourseParticipants.xslt"))
                    Response.ClearContent()
                    Response.ClearHeaders()
                    Response.ContentType = "application/x-msexcel"
                    xslTran.Transform(xmlDoc, Nothing, Response.OutputStream, Nothing)
                    Response.Flush()
                    Response.Close()
                Catch ex As Exception
                    errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                    errorMessageLabel.Visible = True
                    objPCILogger.IndicationSuccessFailure = "Failure" '6524
                    objPCILogger.Message = "Export Training Student Report Failed. " & ex.Message.ToString() '6524
                Finally
                    objPCILogger.PushLogToMSMQ() '6524
                End Try
            End If
        End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Response.Redirect("training-class-search.aspx")
        End Sub
    End Class

End Namespace
