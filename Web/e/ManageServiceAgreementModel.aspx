﻿<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ManageServiceAgreementModel.aspx.vb"
    Inherits="SIAMAdmin.ManageServiceAgreementModel" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head runat="server">
    <title>Manage Certificate Numbers</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        function ConfirmDelete() {
            return confirm("This activity will delete selected model entry.");
        }
        window.parent.status = "Maintain Service Agreement Model is open.";
    </script>

</head>
<body>
    <form id="Form11" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center" border="0">
        <tr>
            <td align="center" height="20">
                <asp:Label ID="lblHeader" CssClass="headerTitle" runat="server">Maintain Service Agreement Model</asp:Label>
            </td>
        </tr>
        <tr>
            <td height="15">
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" width="100%" align="center">
                    <tr height="20">
                        <td align="left">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="FieldUpdatePanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="div1" style="border-right: #c9d5e6 thin solid; width: 100%; border-top: #c9d5e6 thin solid;
                                        border-left: #c9d5e6 thin solid; border-bottom: #c9d5e6 thin solid">
                                        <table width="100%" cellspacing="0">
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblProductCategory" CssClass="bodyCopyBold" runat="server">Product Catagory</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtProductCCode" runat="server" CssClass="bodyCopy" MaxLength="100"
                                                        Text="" Width="80%"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblProductCategoryGrp" runat="server" CssClass="bodyCopyBold">Product Group</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtProductGroup" runat="server" CssClass="bodyCopy" MaxLength="5"
                                                        Text="" Width="80%"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="12%">
                                                    <asp:Label ID="lblProductCode" runat="server" CssClass="bodyCopyBold">ProductCode</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtProductCode" runat="server" Width="80%" CssClass="bodyCopy"
                                                        Text="" MaxLength="100"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                                <td width="12%">
                                                    <asp:Label ID="lblModelNumber" runat="server" CssClass="bodyCopyBold">Model Number</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <SPS:SPSTextBox ID="txtModelNumber" runat="server" Width="80%" CssClass="bodyCopy"
                                                        Text="" MaxLength="100"></SPS:SPSTextBox>
                                                    <span class="redAsterick">*</span>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#c9d5e6" height="20px">
                                                <td width="10%">
                                                    <asp:Label ID="lblCountryName" CssClass="bodyCopyBold" runat="server">Country</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <asp:DropDownList ID="ddlcountry" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="10%">
                                                    <asp:Label ID="lblLanguage" runat="server" CssClass="bodyCopyBold">Language</asp:Label>
                                                </td>
                                                <td width="10%">
                                                    <asp:DropDownList ID="ddllanguage" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr height="20px">
                                                <td colspan="5">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" align="right" width="25%">
                                                    <asp:Label ID="lblSAModelID" runat="server" Visible="False"></asp:Label>
                                                    <asp:Button ID="btnAddNew" runat="server" CssClass="bodyCopyBoldBtnNew" Text="Add New"
                                                        Width="70px" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnSave" runat="server" CssClass="bodyCopyBoldBtnNew" Text="Save"
                                                        Width="70px" />
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="bodyCopyBoldBtnNew" Text="Cancel"
                                                        Width="70px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dgServiceAgreementModel" EventName="ItemCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:UpdatePanel ID="GridUpdatePanel" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:DataGrid ID="dgServiceAgreementModel" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        PageSize="25" Width="100%">
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <EditItemStyle BackColor="#999999" />
                                        <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle BackColor="#284775" CssClass="bodyCopy" ForeColor="White" HorizontalAlign="Center"
                                            Mode="NumericPages" />
                                        <AlternatingItemStyle BackColor="White" ForeColor="#284775" CssClass="bodyCopy" />
                                        <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="bodyCopy" />
                                        <HeaderStyle BackColor="#284775" CssClass="tableHeader" ForeColor="White" />
                                        <Columns>
                                            <asp:BoundColumn DataField="CATEGORY" HeaderText="Product Catagory">
                                                <HeaderStyle Width="20%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PRODUCTGROUP" HeaderText="Product Group">
                                                <HeaderStyle Width="20%" HorizontalAlign="Left" Font-Bold="False" Font-Italic="False"
                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="PRODUCTCODE" HeaderText="Product Code"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="MODEL" HeaderText="Model">
                                                <HeaderStyle Width="20%" HorizontalAlign="Left" Font-Bold="False" Font-Italic="False"
                                                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                                                <ItemStyle HorizontalAlign="Left" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" />
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Width="20%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="COUNTRY" HeaderText="COUNTRY"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LANGUAGE" HeaderText="LANGUAGE"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                                        CommandName="Edit" CssClass="bodyCopySM" Text="Edit">
                                                    </asp:LinkButton>
                                                    <itemstyle cssclass="bodycopy" horizontalalign="Center" verticalalign="Middle" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                                        CommandName="Delete" CssClass="bodyCopySM" Text="Delete">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" Visible="False"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <input type="hidden" id="hdnLastOpr" runat="server" />
    </form>
</body>
</html>
