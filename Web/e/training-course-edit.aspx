<%@ Reference Page="~/e/Utility.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" ValidateRequest="false"
    Inherits="SIAMAdmin.training_course_edit" CodeFile="training-course-edit.aspx.vb"
    CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>training_course_edit</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/style.css" type="text/css" rel="stylesheet">

    <script src="includes/SiamAdmin.js" type="text/javascript"></script>

    <script type="text/javascript">
        function ShowLocation(edit, strselected) {
            var voptions = document.getElementById('ddlType').options(document.getElementById('ddlType').selectedIndex);

            var objItem;
            var i = 0;
            var j = 0;
            j = document.getElementById('chkDefaultLocations').children.item().children.length;
            // iterate through listitems that need to be enabled or disabled
            for (i = 0; i < j; i++) {
                objItem = document.getElementById('chkDefaultLocations_' + i);
                if (objItem === null) {
                    continue;
                }
                if (voptions.text !== 'Classroom Training') {
                    // Disable/Enable the checkbox.
                    objItem.disabled = true;
                    if (i === 0) {
                        objItem.checked = true;
                    }
                    else {
                        // Should the checkbox be disabled?
                        objItem.checked = false;
                    }
                }
                else if (edit === false) {
                    // Disable/Enable the checkbox.
                    document.getElementById('chkDefaultLocations').disabled = false;
                    objItem.disabled = false;
                    objItem.parentElement.disabled = false;
                    objItem.checked = false;
                }
                else if (edit === true) {
                    // Disable/Enable the checkbox.
                    //document.getElementById('chkDefaultLocations').disabled = false;
                    //objItem.disabled = false;
                    //objItem.parentElement.disabled = false;
                    var position = j - (j - i);
                    //alert(i + ':' + position);
                    if (strselected.substring(i, position + 1) === "1") {
                        objItem.checked = true;
                    }
                    else {
                        objItem.checked = false;
                    }

                }
            }
        }

    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table id="Table3" style="z-index: 101; left: 8px; width: 600px; position: absolute; top: 8px;"
            align="center" border="0">
            <tr>
                <td class="PageHead" style="height: 1px; width: 601px;" align="center" colspan="1"
                    height="1" rowspan="1">
                    Training Course
                </td>
            </tr>
            <tr>
                <td class="PageHead" style="height: 3px; width: 601px;" align="left">
                    &nbsp;<asp:Label ID="ErrorLabel" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 12px; width: 601px;">
                </td>
            </tr>
            <tr>
                <td style="height: 343px; width: 601px;" valign="top" align="left">
                    <table id="Table2" width="100%" border="0">
                        <tr bgcolor="#f2f5f8" height="25">
                            <td align="right" colspan="1" rowspan="1">
                                <asp:Label ID="labelCourseStatus" runat="server" CssClass="Body" Font-Bold="True"> Course status:</asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdoStatus" Enabled="false" AutoPostBack="true" CssClass="Body"
                                    runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="True">Active</asp:ListItem>
                                    <asp:ListItem Value="2">Hidden</asp:ListItem>
                                    <asp:ListItem Value="3">Deleted</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr height="25">
                            <td align="right" colspan="1" rowspan="1">
                                <asp:Label ID="labelCourseNumber" runat="server" CssClass="Body" Font-Bold="True"> Course#:</asp:Label>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtCourseNumber" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                <asp:Label ID="LabelCourseNumberStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#f2f5f8" height="25">
                            <td align="right">
                                <asp:Label ID="labelTitle" runat="server" CssClass="Body" Font-Bold="True"> Title:</asp:Label>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtTitle" runat="server" CssClass="body"></SPS:SPSTextBox>
                                <asp:Label ID="LabelTitleStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label><span class="Body"></span>
                            </td>
                        </tr>
                        <tr height="25">
                            <td align="right" valign="top" style="width: 117px">
                                <asp:Label ID="labelDescription" runat="server" CssClass="Body" Font-Bold="True">Description:</asp:Label>
                            </td>
                            <td valign="top">
                                <SPS:SPSTextBox ID="txtDescription" runat="server" CssClass="Body" Width="440px"
                                    Height="80px" TextMode="MultiLine"></SPS:SPSTextBox>
                                <asp:Label ID="LabelDescriptionStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#f2f5f8" height="25">
                            <td align="right">
                                <asp:Label ID="Label7" runat="server" CssClass="Body" Font-Bold="True">Type :</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlType" AutoPostBack="true" runat="server" CssClass="Body"
                                    Width="176px">
                                </asp:DropDownList>
                                <asp:Label ID="LabelTypeStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                            </td>
                        </tr>
                        <tr height="25">
                            <td align="right">
                                <asp:Label ID="labelCapacity" runat="server" CssClass="Body" Font-Bold="True"> Minor Category:</asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlMinorCategory" runat="server" CssClass="Body" Width="440px">
                                </asp:DropDownList>
                                <asp:Label ID="LabelMinorCategoryStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#f2f5f8" height="25">
                            <td style="width: 117px;" align="right" valign="middle">
                                <asp:Label ID="LabelDefaultLocation" runat="server" CssClass="Body" Font-Bold="True"> Default Location(s):</asp:Label>
                            </td>
                            <td align="left" valign="top" width="493px">
                                <table border="0" width="100%" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:CheckBoxList runat="server" CssClass="Body" ID="chkDefaultLocations">
                                            </asp:CheckBoxList>
                                            <SPS:SPSTextBox ID="txtURL" Visible="false" runat="server" CssClass="Body" Width="440px"></SPS:SPSTextBox>
                                            <asp:Label ID="lblLocationAstric" runat="server" CssClass="redAsterick"> </asp:Label>
                                        </td>
                                        <td width="10%">
                                            <span id="locationAstrick"
                                                runat="server" class="redAsterick">*</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr height="25">
                            <td align="right">
                                <asp:Label ID="labelPrice" runat="server" CssClass="Body" Font-Bold="True"> Price:</asp:Label>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtPrice" runat="server" CssClass="Body" Width="88px"></SPS:SPSTextBox>
                                <asp:Label ID="LabelPriceStar" runat="server" CssClass="redAsterick">
										<span class="redAsterick">*</span></asp:Label>
                            </td>
                        </tr>
                        <tr bgcolor="#f2f5f8" height="25">
                            <td style="width: 117px;" align="right">
                                <asp:Label ID="labelPreRequisite" runat="server" CssClass="Body" Font-Bold="True">PreRequisite:</asp:Label>
                            </td>
                            <td>
                                <SPS:SPSTextBox ID="txtPreRequisite" runat="server" CssClass="Body"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr height="25">
                            <td valign="top" align="right" style="width: 117px">
                                <asp:Label ID="Label26" runat="server" CssClass="Body" Font-Bold="True">Notes:</asp:Label>
                            </td>
                            <td valign="top">
                                <SPS:SPSTextBox ID="txtNotes" runat="server" CssClass="Body" Width="440px" Height="50px"
                                    TextMode="MultiLine"></SPS:SPSTextBox>
                            </td>
                        </tr>
                        <tr bgcolor="#f2f5f8">
                            <td align="right" valign="top" style="width: 117px">
                                <asp:Label ID="Label1" runat="server" CssClass="Body" Font-Bold="True">Model:</asp:Label>
                            </td>
                            <td valign="top">
                                <table style="width: 100%" border="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlModelList" CssClass="Body">
                                            </asp:DropDownList>
                                            <SPS:SPSTextBox ID="txtModel" runat="server" CssClass="Body"></SPS:SPSTextBox>
                                            <asp:Label ID="lblReqModel" runat="server" CssClass="redAsterick"></asp:Label>&nbsp;<asp:Button
                                                ID="btnAddModel" runat="server" Text="Add Model" CssClass="body" Font-Bold="True" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblGridError" runat="server" CssClass="Body" ForeColor="Red" EnableViewState="False"></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%;">
                                            <asp:DataGrid ID="dgModel" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                                ForeColor="#333333" GridLines="None" Width="100%">
                                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                <EditItemStyle BackColor="#999999" />
                                                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                                <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" CssClass="Body" ForeColor="White" />
                                                <Columns>
                                                    <asp:BoundColumn DataField="Model" HeaderText="Model">
                                                        <ItemStyle Font-Bold="False" CssClass="body" Font-Italic="False" Font-Overline="False"
                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        <HeaderStyle Width="15%" />
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Remove">
                                                        <HeaderStyle Width="5%" Font-Bold="True" Font-Italic="False" Font-Overline="False"
                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="imgDeleteProduct" CommandName="Delete" runat="server" ImageUrl="Images/Delete.gif"
                                                                AlternateText="Remove this product" />
                                                        </ItemTemplate>
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="ItemId" HeaderText="ItemId" Visible="False"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 19px; width: 601px;" align="center">
                    <table id="Table1" style="width: 595px; height: 27px" cellpadding="0"
                        border="0">
                        <tr>
                            <td align="center" style="width: 3px">
                            </td>
                            <td align="right">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="body" Font-Bold="True"></asp:Button>&nbsp;<asp:Button ID="btnCancel" runat="server" Text="Close"
                                    CssClass="body" Font-Bold="True"></asp:Button>
                                &nbsp;<asp:Button ID="btnDelete" runat="server" Text="Hide" CssClass="body" Font-Bold="True"
                                    Visible="false"></asp:Button>&nbsp;<asp:Button ID="btnActivate" runat="server" Text="Activate"
                                        CssClass="body" Font-Bold="True" Visible="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
