<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="appsUser.aspx.vb" Inherits="SIAMAdmin.appsUser" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head runat="server">
    <title>New User</title>
    <LINK href="includes/style.css" type="text/css" rel="stylesheet">
</head>
<body >
    <form id="frmUser" runat="server">
<table width="75%" border="0" align="center" cellspacing="2">
    <tr class="PageHead">
        <td colspan="2" style="text-align: center; width: 601px;">
            User Manager</td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="width: 601px; height: 25px">&nbsp;</td>
    </tr>
  <tr> 
    <td colspan="2" bgColor="#5d7180" align=center style="height: 25px; width: 601px;" >
                <table border="0">
				<tr style="height:25">
				    <td class=Nav3>New</td>
					<td class="Nav3" align="center" width="5">&nbsp;|&nbsp;</td>
					<td><A class=Nav3 href="SearchUser.aspx">Search</A></td>
                </tr>
            </table></td>
  </tr>
  <tr> 
    <td colspan="2" style="text-align: center; width: 601px; height: 19px;"><asp:label id="ErrorLabel" runat="server" CssClass="body" ForeColor="Red" EnableViewState="False"></asp:label>&nbsp;</td>
  </tr>
    <tr>
        <td colspan=2>
          <div id="div1" style="BORDER-RIGHT: gray thin solid; BORDER-TOP: gray thin solid; BORDER-LEFT: gray thin solid; BORDER-BOTTOM: gray thin solid; width:100%">
            <table width=100% border=0 cellpadding=0 cellspacing=0>
                <tr>
                    <td style="width: 170px; text-align: right">&nbsp;
                    </td>
                    <td>
                    </td>
                </tr>
          <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblFirstName" runat="server" CssClass="body" Text="First Name:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span></td>
          </tr>
          <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblLastName" runat="server" CssClass="body" Text="Last Name:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif"> 
                <SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span></td>
          </tr>
          <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblCompName" runat="server" CssClass="body" Text="Company Name:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtCompName" runat="server" CssClass="body"></SPS:SPSTextBox>
                <span style="font-size: 6pt; color: #ff3333">*</span></span></td>
          </tr>
          <tr> 
            <td style="text-align: right">
                <asp:Label ID="lblAddr" runat="server" CssClass="body" Text="Street Address:"></asp:Label></td>
            <td><span size="2" face="MS Reference Sans Serif">
                <SPS:SPSTextBox ID="txtAddr1" runat="server" CssClass="body" MaxLength="50" Width="182px"></SPS:SPSTextBox>
                <asp:label id="LabelLine1Star" runat="server" CssClass="redAsterick"></asp:label><span style="font-size: 6pt; color: #ff3333">*</span></span></td>
             </tr>
             <tr>   
                <td style="text-align: right">
                <asp:Label ID="lblAddr2" runat="server" CssClass="body" Text="Address 2nd Line:"></asp:Label></td>
            <td>
                <SPS:SPSTextBox ID="txtAddr2" runat="server" CssClass="body" MaxLength="50" Width="182px"></SPS:SPSTextBox><asp:label id="LabelLine2Star" runat="server" CssClass="redAsterick"></asp:label></td>
             </tr>
          </tr>
          <tr>
                <td style="text-align: right"><asp:Label ID="lblAddr3" runat="server" CssClass="body" Text="Address 3rd Line:"></asp:Label>
                    </td>
                <td>
                    <SPS:SPSTextBox ID="txtAddr3" runat="server" CssClass="body" MaxLength="50" Width="182px"></SPS:SPSTextBox>
                    <asp:label id="LabelLine3Star" runat="server" CssClass="redAsterick"></asp:label>
                    </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <span style="font-size: 8pt; font-family: MS Sans Serif">
                        <asp:Label ID="lblCity" runat="server" CssClass="body" Text="City :"></asp:Label></span></td>
                <td>
                    <SPS:SPSTextBox ID="txtCity" runat="server" CssClass="body" MaxLength="50"></SPS:SPSTextBox>
                    <asp:label id="LabelLine4Star" runat="server" CssClass="redAsterick"></asp:label>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
                                    <tr>
                <td style="text-align: right; height: 22px;">
                    <asp:Label ID="lblCountry" runat="server" CssClass="body" Text="Country:"></asp:Label></td>
                <td style="height: 22px">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="body" 
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            <tr>
                <td style="text-align: right; height: 22px;">
                    <asp:Label ID="lblState" runat="server" CssClass="body" Text="State:"></asp:Label></td>
                <td style="height: 22px">
                    <asp:DropDownList ID="ddlStates" runat="server" CssClass="body">
                    </asp:DropDownList>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>

            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblZip" runat="server" CssClass="body" Text="Zip Code:"></asp:Label></td>
                <td>
                    <SPS:SPSTextBox ID="txtZip" runat="server" CssClass="body" MaxLength="5"></SPS:SPSTextBox>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblPhone" runat="server" CssClass="body" Text="Phone:"></asp:Label></td>
                <td>
                    <SPS:SPSTextBox ID="txtPhone" runat="server" CssClass="body" MaxLength="20"></SPS:SPSTextBox>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span>
                    <SPS:SPSTextBox ID="txtExt" Width=40 MaxLength=5 runat="server" CssClass="body"></SPS:SPSTextBox>
                    </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblFax" runat="server" CssClass="body" Text="FAX:"></asp:Label></td>
                <td>
                    <SPS:SPSTextBox ID="txtFax" runat="server" CssClass="body" MaxLength="20"></SPS:SPSTextBox></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblEmail" runat="server" CssClass="body" Text="E mail:"></asp:Label></td>
                <td>
                    <SPS:SPSTextBox ID="txtEmail" runat="server" CssClass="body" MaxLength="100"></SPS:SPSTextBox>
                   <%-- <asp:Label ID="lblEmailSuffix" runat="server" CssClass="body" Text="@am.sony.com"></asp:Label>--%>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblUserName" runat="server" CssClass="body" 
                        Text="10 character Global ID:"></asp:Label></td>
                <td>
                    <SPS:SPSTextBox ID="txtUsrName" runat="server" CssClass="body" MaxLength="10"></SPS:SPSTextBox>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblRole" runat="server" CssClass="body" Text="Role:"></asp:Label></td>
                <td>
                
                    <asp:ListBox
                        ID="lstRole" runat="server" CssClass="body" SelectionMode="Multiple" Width="165px">
                    </asp:ListBox><span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            <tr>
                <td style="text-align: right; height: 12px;">
                    <asp:Label ID="lblType" runat="server" CssClass="body" Text="Type:"></asp:Label></td>
                <td style="height: 12px">
               
                    <SPS:SPSTextBox ID="txtUserType" runat="server" CssClass="body" MaxLength="20" 
                        ReadOnly="True" Enabled="False">Internal</SPS:SPSTextBox></td> 
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:Label ID="lblStatus" runat="server" CssClass="body" Text="Status:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlUsrStatus" runat="server" CssClass="body" Width="129px">
                    </asp:DropDownList>
                    <span style="font-size: 6pt; color: #ff3333; font-family: MS Reference Sans Serif">*</span></td>
            </tr>
            
          <tr> 
            <td style="width: 299px; height: 19px;" colspan=2>&nbsp;</td>            
          </tr>
          <tr> 
            <td></td>
            <%--<td colspan="2"><div align="center"> --%>
            <td>
                <asp:Button ID="cmdUpdate" runat="server" CssClass="body" Text="  Edit  " ToolTip="Click to Update the Role" />
                <asp:Button ID="cmdSave" runat="server" CssClass="body" Text="  Save  " ToolTip="Click to Save the Role" />
                <%--<asp:Button ID="cmdCancel" runat="server" CssClass="body" Text=" Cancel " ToolTip="Click to Cancel the entry" />--%>&nbsp;
                <asp:HiddenField ID="hdnSIAMIdentity" runat="server" />
              </div></td>
          </tr>
           </table>&nbsp;
          </div>        
        </td>
    </tr>
  </table>
    </form>
</body>
</html>
