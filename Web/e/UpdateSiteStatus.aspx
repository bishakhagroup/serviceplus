<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Page Language="VB" AutoEventWireup="false" smartnavigation="true" CodeFile="UpdateSiteStatus.aspx.vb" Inherits="SIAMAdmin.UpdateSiteStatus"  CodeFileBaseClass="SIAMAdmin.UtilityClass" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>View Credit Card Audit Trail details</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="includes/style.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="includes/SiamAdmin.js" type="text/javascript"></script>
	</HEAD>
	<body bgColor="#ffffff"  class="body">
		<form id="Form1" method="post" runat="server">
			<table align="center" border="0" width="100%">
				<tr>
					<td class="PageHead" align="center" colSpan="1" height="30" rowSpan="1">Update Site 
                        Status</td>
				</tr>
				
				<tr  bgcolor="#c9d5e6" >
					<td align=left height=20>&nbsp;</td>
				</tr>
				<TR>
					<TD  align="center" height="50">&nbsp;
						 <div id="div1"  style="BORDER-RIGHT: #c9d5e6 thin solid; width:60%; 
								    BORDER-TOP: #c9d5e6 thin solid; BORDER-LEFT: #c9d5e6 thin solid; BORDER-BOTTOM: #c9d5e6 thin solid">
								    
                    
                    <table width="100%" border=0 cellpadding=0 cellspacing=2>
                        <tr><td align=left  width=30% colspan="2" valign="top">
                            <span class=BodyHead> ServicesPLUS is currently
                            <asp:Label ID="lblStatus" runat="server" CssClass="BodyHead"></asp:Label>. 
                            Please use following button to change the status from running to under 
                            maintenance or under maintenance to the running status.<br /></span>
                            <br />
                            </td>
                            </tr>
                        <tr><td align=left  width=30% colspan="2" valign="top">
                            &nbsp;&nbsp;</td>
                            </tr>
                        <tr>
                        <td style="height: 21px">
                            <asp:Button ID="btnStatus" runat="server" Text="" CssClass="BodyHead" 
                                Width="244px" />
                            </td>
                        <td style="height: 21px">
                            <%--<asp:Button ID="btnBind" runat="server" CssClass="BodyHead" Width="100px" Text="Bind"  />--%>
                            </td></tr>
                    </table>					
					</div></TD>
				</TR>
				
				<tr><td>&nbsp;</td></tr>
				<tr  bgcolor="#c9d5e6" >
					<td align=left height=20>&nbsp;</td>
				</tr>
				</table>
		</form>
	</body>
</HTML>