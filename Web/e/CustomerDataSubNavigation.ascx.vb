Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException
Imports ServicePLUSWebApp

Namespace SIAMAdmin
    Partial Class CustomerDataSubNavigation
        Inherits System.Web.UI.UserControl

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private _siamId As String = String.Empty
        Private _ldapId As String = String.Empty '7086
        Private _userType As String = String.Empty
        Private _currentPage As String = String.Empty

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            Try
                'Utilities.LogMessages("== NEW ENTRY ==")
                Dim customer As Sony.US.ServicesPLUS.Core.Customer = Nothing

                If HttpContextManager.SelectedCustomer IsNot Nothing Then
                    'Utilities.LogMessages("NAV: CustomerObj found")
                    customer = HttpContextManager.SelectedCustomer
                ElseIf Request.QueryString("SiamId") IsNot Nothing Then
                    'Utilities.LogMessages("NAV: Find Customer by SIAM ID: " & Request.QueryString("SiamId"))
                    Dim cm As New CustomerManager()
                    customer = cm.GetCustomer(Request.QueryString("SiamId"))
                    HttpContextManager.SelectedCustomer = customer
                ElseIf Request.QueryString("LdapId") IsNot Nothing Then
                    'Utilities.LogMessages("NAV: Find Customer by LDAP ID: " & Request.QueryString("LdapId"))
                    Dim cm As New CustomerManager()   ' There is no way to fetch a full user data from LdapID because... reasons?
                    Dim siamID = cm.GetSiamIdByLdapId(Request.QueryString("LdapId"))   ' Find their SIAM ID
                    'Utilities.LogMessages("NAV: Got SIAM ID: " & siamID)
                    customer = cm.GetCustomer(siamID)   ' Now find the Customer based on SIAM ID
                    HttpContextManager.SelectedCustomer = customer
                Else
                    Throw New Exception("Customer data was not properly loaded. Please try a new search.")
                    'lblNavError.Text = "Customer data was not properly loaded. Please try a new search."
                End If

                If customer IsNot Nothing Then
                    'Utilities.LogMessages("NAV: Customer found. SIAM: " & customer.SIAMIdentity & ", LDAP: " & customer.LdapID & ", UserType: " & customer.UserType)
                    _siamId = customer.SIAMIdentity
                    _ldapId = customer.LdapID
                    _userType = customer.UserType
                End If
            Catch ex As Exception
                lblNavError.Text = ex.Message & vbCrLf & ex.InnerException.Message
            End Try
        End Sub


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        End Sub

#Region "  Properties  "
        Public Property SiamID() As String
            Get
                Return _siamId
            End Get
            Set(ByVal Value As String)
                _siamId = Value
            End Set
        End Property

        Public Property LdapID() As String
            Get
                Return _ldapId
            End Get
            Set(ByVal Value As String)
                _ldapId = Value
            End Set
        End Property

        Public Property UserType() As String
            Get
                Return _userType
            End Get
            Set(ByVal Value As String)
                _userType = Value.ToUpper()
            End Set
        End Property

        Public ReadOnly Property CurrentPage() As String
            Get
                If _currentPage = String.Empty Then
                    Dim url = HttpContext.Current.Request.Url.AbsoluteUri.ToLower()

                    If url.Contains("customerinfo.aspx") Then
                        _currentPage = "CustomerInfo"
                    ElseIf url.Contains("findorders.aspx") Then
                        _currentPage = "FindOrders"
                    ElseIf url.Contains("viewsapaccounts.aspx") Then
                        _currentPage = "ViewSAPAccounts"
                    ElseIf url.Contains("taxexempt.aspx") Then
                        _currentPage = "TaxExempt"
                    Else
                        _currentPage = String.Empty
                    End If
                End If

                Return _currentPage
            End Get
        End Property
#End Region

    End Class

End Namespace
