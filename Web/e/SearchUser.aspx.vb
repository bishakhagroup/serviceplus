Imports System.Web.UI
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam, Sony.US.ServicesPLUS.Controls
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Drawing
Imports Sony.US.AuditLog
Imports ServicesPlusException
Imports Sony.US.SIAMUtilities '6668
Namespace SIAMAdmin

    Partial Class SearchUser
        Inherits UtilityClass
        'Dim objUtilties As Utilities = New Utilities

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then

                    PopulateRole()
                    'Added by Prasad for 2645
                    'PopulateStatus()

                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub PopulateRole()
            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim user_role() As Sony.US.ServicesPLUS.Core.UserRole = oUsrSecMgr.GetUserRoles("%", "1")
            Dim iUsrRole As Sony.US.ServicesPLUS.Core.UserRole

            Dim newListItem1 As New ListItem()
            newListItem1.Text = "Select One"
            newListItem1.Value = ""

            ddlRole.Items.Add(newListItem1)

            For Each iUsrRole In user_role
                Dim newListItem As New ListItem()
                newListItem.Text = ChangeCase(iUsrRole.RoleName)
                newListItem.Value = iUsrRole.RoleID
                ddlRole.Items.Add(newListItem)
            Next
        End Sub
        'Added by Prasad for 2645
        Private Sub PopulateStatus()
            '6668 starts
            'Dim oUsrSecMgr As New UserRoleSecurityManager
            'Dim user_type() As UserType = oUsrSecMgr.GetUserStatus()
            'Dim iUsrType As UserType

            'For Each iUsrType In user_type
            '    If ChangeCase(iUsrType.StatusName) <> "Pending" Then
            '        Dim newListItem As New ListItem()
            '        newListItem.Text = ChangeCase(iUsrType.StatusName)
            '        newListItem.Value = iUsrType.StatusID
            '        ddlStatus.Items.Add(newListItem)
            '    End If
            'Next

            ddlStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Active) '6668
            ddlStatus.Items.Add(ConfigurationData.GeneralSettings.StatusCodes.Inactive) '6668
            '6668 ends
        End Sub

        Private Function ChangeCase(ByVal pString As String) As String

            Dim str As String = pString.ToLower()

            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)

        End Function

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim col_user() As SiamUser
            Dim srchPattern As String = ""
            Dim iStatus As Integer = 0
            Dim objPCILogger As New PCILogger() '6524

            'If txtSiamId.Text.Length > 0 Then
            '    'Modified by Prasad.
            '    If IsNumeric(txtSiamId.Text.Trim()) Then
            '        srchPattern = " and LOWER(a.siamIdentity) like '%" + txtSiamId.Text.ToLower() + "%'"
            '    Else
            '        ErrorLabel.Text = "Please Enter only numeric value for SIAM User ID."
            '        ErrorLabel.Visible = True
            '        Exit Sub
            '    End If

            'End If

            'If txtFirstName.Text.Length > 0 Then

            '    If srchPattern = "" Then
            '        srchPattern = " and LOWER(FirstName) like '%" + txtFirstName.Text.ToLower() + "%'"
            '    Else
            '        srchPattern = srchPattern + " and LOWER(FirstName) like '%" + txtFirstName.Text.ToLower() + "%'"
            '    End If

            'End If

            'If txtLastName.Text.Length > 0 Then

            '    If srchPattern = "" Then
            '        srchPattern = " and LOWER(LastName) like '%" + txtLastName.Text.ToLower() + "%'"
            '    Else
            '        srchPattern = srchPattern + " and LOWER(LastName) like '%" + txtLastName.Text.ToLower() + "%'"
            '    End If

            'End If

            'If txtCompanyName.Text.Length > 0 Then

            '    If srchPattern = "" Then
            '        srchPattern = " and LOWER(CompanyName) like '%" + txtCompanyName.Text.ToLower() + "%'"
            '    Else
            '        srchPattern = srchPattern + " and LOWER(CompanyName) like '%" + txtCompanyName.Text.ToLower() + "%'"
            '    End If

            'End If

            'If txtUserName.Text.Length > 0 Then

            '    If srchPattern = "" Then
            '        srchPattern = " and LOWER(UserName) like '%" + txtUserName.Text.ToLower() + "%'"
            '    Else
            '        srchPattern = srchPattern + " and LOWER(UserName) like '%" + txtUserName.Text.ToLower() + "%'"
            '    End If

            'End If

            'If txtEmail.Text.Length > 0 Then

            '    If srchPattern = "" Then
            '        srchPattern = " and LOWER(EmailAddress) like '%" + txtEmail.Text.ToLower() + "%'"
            '    Else
            '        srchPattern = srchPattern + " and LOWER(EmailAddress) like '%" + txtEmail.Text.ToLower() + "%'"
            '    End If

            'End If

            If ddlRole.SelectedIndex > 0 Then

                If srchPattern = "" Then
                    srchPattern = " and b.roleid = " + ddlRole.SelectedValue()
                Else
                    srchPattern = srchPattern + " and b.roleid = " + ddlRole.SelectedValue()
                End If

            End If

            'If srchPattern = "" Then
            '    srchPattern = " and a.statusid = " + ddlStatus.SelectedValue()
            'Else
            '    srchPattern = srchPattern + " and a.statusid = " + ddlStatus.SelectedValue()
            'End If

            'If srchPattern = "" Then
            '    srchPattern = " and a.statuscode = 1"
            'Else
            '    If ddlStatus.SelectedValue() = 1 Then
            '        srchPattern = " and a.statuscode = 1"
            '    Else
            '        srchPattern = " and a.statuscode = 2"
            '    End If
            'End If

            'If ddlStatus.SelectedValue() = 1 Then'6668
            'If ddlStatus.SelectedItem.Text = "Active" Then '6668
            '    srchPattern = srchPattern & " and a.statuscode = 1"
            'Else
            '    srchPattern = srchPattern + " and a.statusid = " + ddlStatus.SelectedValue()
            'End If

            Dim code As Integer
            If ddlStatus.SelectedItem.Text.ToUpper() = "ACTIVE" Then '6756
                code = 1
            ElseIf ddlStatus.SelectedItem.Text.ToUpper() = "INACTIVE" Then
                code = 3
            ElseIf ddlStatus.SelectedItem.Text.ToUpper() = "ACTIVE AND INACTIVE" Then
                code = 4
            End If

            srchPattern = txtFirstName.Text + "*" + txtLastName.Text + "*" + txtEmail.Text + "*" + txtCompanyName.Text + "*" + txtUserName.Text.Trim() + "*" + txtSiamId.Text.Trim() + "*" + ddlRole.SelectedValue() + "*" + code.ToString()
            'Start Modification for E773
            Session("srchpattern") = srchPattern
            'End Modification for E773

            Try
                '6524 start
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.EventOriginMethod = "btnSearch_Click"
                objPCILogger.Message = "Search User Success."
                objPCILogger.EventType = EventType.View
                '6524 end

                col_user = oUsrSecMgr.SearchUsers(srchPattern)

                Session.Remove("snSearchUser")
                Session.Add("snSearchUser", col_user)

                If col_user.Length > 0 Then
                    dgSearchUser.CurrentPageIndex = 0
                    dgSearchUser.DataSource = col_user
                    dgSearchUser.DataBind()
                    dgSearchUser.Visible = True
                    ErrorLabel.Text = ""
                Else
                    dgSearchUser.Visible = False
                    ErrorLabel.Text = "No Result(s) found."
                End If





            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "Search User Failed. " & ex.Message.ToString() '6524
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try

        End Sub

        Private Sub ApplyDataGridStyle()

            dgSearchUser.ApplyStyle(GetStyle(500))
            dgSearchUser.PageSize = 8
            dgSearchUser.AutoGenerateColumns = False
            dgSearchUser.AllowPaging = True
            dgSearchUser.GridLines = GridLines.Horizontal
            dgSearchUser.AllowSorting = True
            dgSearchUser.CellPadding = 3
            dgSearchUser.PagerStyle.Mode = PagerMode.NumericPages
            dgSearchUser.HeaderStyle.Font.Size = FontUnit.Point(8)
            dgSearchUser.HeaderStyle.ForeColor = Color.GhostWhite
            dgSearchUser.HeaderStyle.BackColor = Color.FromKnownColor(KnownColor.ControlDarkDark)
            dgSearchUser.ItemStyle.Font.Size = FontUnit.Point(8)
            dgSearchUser.AlternatingItemStyle.Font.Size = FontUnit.Point(8)

        End Sub

        Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

            txtSiamId.Text = ""
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtCompanyName.Text = ""
            txtUserName.Text = ""
            txtEmail.Text = ""
            ddlRole.SelectedIndex = 0
            dgSearchUser.Visible = False
            ErrorLabel.Text = ""

        End Sub

        Protected Sub dgSearchUser_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgSearchUser.PageIndexChanged
            Try
                dgSearchUser.CurrentPageIndex = e.NewPageIndex
                dgSearchUser.DataSource = Session.Item("snSearchUser")
                dgSearchUser.DataBind()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            MyBase.IsProtectedPage(True, True)
            ApplyDataGridStyle()
        End Sub

        'Start Modification for E773
        Protected Sub dgSearchUser_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSearchUser.ItemDataBound
            Try
                Dim lnkbtn As LinkButton
                If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                    lnkbtn = e.Item.FindControl("lnkBtnDelete")
                    If Not lnkbtn Is Nothing Then
                        lnkbtn.Attributes.Add("onclick", "javascript:ConfirmDelete();")
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub dgSearchUser_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSearchUser.ItemCreated
            Try
                If e.Item.ItemType <> ListItemType.Header Then
                    e.Item.ApplyStyle(GetStyle(e.Item.ItemType))
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgSearchUser_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearchUser.ItemCommand

            Dim oUsrSecMgr As New UserRoleSecurityManager
            Dim col_user() As SiamUser
            Dim tempSrchPattern As String = Session("srchpattern")



            If e.CommandName = "Delete" Then
                Dim objPCILogger As New PCILogger()
                Try
                    Dim objCustomer As Customer = New Customer()
                    objCustomer.SIAMIdentity = e.CommandArgument.ToString()
                    Dim objCustomerManager As CustomerManager = New CustomerManager()
                    'prasad added on 21-12-2009
                    'Event logging code added . 
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginApplication = "ServicesPLUS Admin"
                    objPCILogger.EventOriginMethod = "dgSearchUser_ItemCommand"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventType = EventType.Delete
                    If Not Session.Item("siamuser") Is Nothing Then
                        'objPCILogger.UserName = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).UserId
                        objPCILogger.EmailAddress = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).EmailAddress '6524
                        objPCILogger.CustomerID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).CustomerID
                        objPCILogger.CustomerID_SequenceNumber = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).SequenceNumber
                        objPCILogger.SIAM_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).Identity
                        objPCILogger.LDAP_ID = DirectCast(DirectCast(Session.Item("siamuser"), System.Object), Sony.US.siam.User).AlternateUserId '6524

                    End If
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()
                    objCustomerManager.DeleteCustomer(objCustomer)
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.Message = "Successfully deleted user " & e.Item.Cells(1).Text & " " & e.Item.Cells(2).Text & " with userid " & e.CommandArgument.ToString()
                    If Not String.IsNullOrEmpty(objCustomer.SequenceNumber) AndAlso (objCustomer.SequenceNumber <> "null") Then
                        Dim mySA As New SecurityAdministrator
                        Dim oUser As Sony.US.siam.User
                        oUser = Session(CurrentUserSessionVariables)
                        mySA.DeleteUser(e.CommandArgument.ToString(), oUser)

                        col_user = oUsrSecMgr.SearchUsers(tempSrchPattern)

                        Session.Remove("snSearchUser")
                        Session.Add("snSearchUser", col_user)

                        If col_user.Length > 0 Then
                            dgSearchUser.DataSource = col_user
                            dgSearchUser.DataBind()
                            dgSearchUser.Visible = True
                            dgSearchUser.CurrentPageIndex = 0
                            ErrorLabel.Text = ""
                        Else
                            dgSearchUser.Visible = False
                            ErrorLabel.Text = "No Result(s) found."
                        End If

                    End If
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    objPCILogger.IndicationSuccessFailure = "Failure"
                    objPCILogger.Message = ex.Message.ToString()
                Finally
                    objPCILogger.PushLogToMSMQ()
                End Try

            End If



        End Sub
        'End Modification for E773

    End Class

End Namespace
