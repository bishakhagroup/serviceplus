<%@ Control Language="vb" AutoEventWireup="false" Inherits="SIAMAdmin.SiamAdmin_SideNav"
    CodeFile="SiamAdmin_SideNav.ascx.vb" %>
<table height="100%" width="100%" border="0">
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkApplications" href="javascript:LoadContentFrame('Applications.aspx')"
                runat="server">SIAM Applications</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkActions" href="javascript:LoadContentFrame('Actions.aspx')"
                runat="server">SIAM Actions</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkRoles" href="javascript:LoadContentFrame('Roles.aspx')" runat="server">
                SIAM Roles</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkRF" href="javascript:LoadContentFrame('Role-Function-Mapping.aspx')"
                runat="server">RF Mapping</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkAccount" href="javascript:LoadContentFrame('AccountValidationQueue.aspx')"
                runat="server">Validate Account</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkViewExceptions" href="javascript:LoadContentFrame('ViewCreditCardException.aspx')"
                runat="server">CC Exceptions</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkViewBOExceptions" href="javascript:LoadContentFrame('ViewBOException.aspx')"
                runat="server">BO Exceptions</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkViewOtherExceptions" href="javascript:LoadContentFrame('ViewOtherException.aspx')"
                runat="server">Invoice Exceptions</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkAuditTrail" href="javascript:LoadContentFrame('sony-auditTrail.aspx')"
                runat="server">CC AuditTrail</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkCustomer" href="javascript:LoadContentFrame('Users.aspx?customer=true')"
                runat="server">Find User</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkOrder" href="javascript:LoadContentFrame('FindOrders.aspx')"
                runat="server">Find Order</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkClass" href="javascript:LoadContentFrame('Users.aspx?IsTraining=true')"
                runat="server">Purchase Training</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkClassSearch" href="javascript:LoadContentFrame('training-class-search.aspx?IsSearch=true')"
                runat="server">Training Class</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkStudent" href="javascript:LoadContentFrame('training-student-search.aspx')"
                runat="server">Training Student</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkTrainingOrder" href="javascript:LoadContentFrame('training-open-orders.aspx')"
                runat="server">Training Order</a>
        </td>
    </tr>
    <tr>
        <td align="left" bgcolor="#5d7180">
            <a class="Nav2" id="linkTrainingData" href="javascript:LoadContentFrame('training-class-list.aspx')"
                runat="server">Training Data</a>
        </td>
    </tr>
</table>
