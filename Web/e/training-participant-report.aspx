<%@ Reference Page="~/e/Utility.aspx" %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web" %>

<%@ Page Language="vb" SmartNavigation="true" AutoEventWireup="false" Inherits="SIAMAdmin.training_participant_report" CodeFile="training-participant-report.aspx.vb" CodeFileBaseClass="SIAMAdmin.UtilityClass" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Training Participant Report</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/style2.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table id="Table1" width="100%" border="0" align="center">
            <tr>
                <td colspan="2" class="PageHead" align="center" style="height: 21px">Training 
						Participant Report</td>
            </tr>
            <tr>
                <td colspan="2" align="left" style="height: 13px">
                    <asp:Label ID="errorMessageLabel" runat="server" ForeColor="Red" EnableViewState="False" Font-Size="Smaller"
                        CssClass="Body"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="overflow: auto; width: 100%; height: 550;" align="center">
                        <%--<cr:CrystalReportViewer id=courseParticipantCRV accessKey='<%# ".\CourseParticipantReport.rpt" %>' runat="server" Height="50px" Width="350px" DisplayGroupTree="False">
						</cr:CrystalReportViewer>--%>
                        <cr:crystalreportviewer id="courseParticipantCRV" autodatabind="false"
                            runat="server" height="50px" width="350px"
                            enabledatabaselogonprompt="False" hasdrillupbutton="False"
                            hasgotopagebutton="False" hassearchbutton="False"
                            hastogglegrouptreebutton="False" haszoomfactorlist="False" />
                        <%--Obsolete: DisplayGroupTree="False" hasviewlist="False"--%>
                    </div>
                </td>

            </tr>
            <tr>
                <td colspan="2" align="center">&nbsp;</td>
            </tr>

            <tr>
                <td align="left" colspan="2">
                    <%--<asp:Button id="btnBack" runat="server" Text="Back"></asp:Button>&nbsp;&nbsp;--%><asp:Button
                        ID="btnExport" runat="server" Text="Export" Visible="False"></asp:Button></td>

            </tr>
        </table>
    </form>
</body>
</html>
