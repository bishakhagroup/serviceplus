<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<Table border="0">
			<xsl:apply-templates select="//TRAININGCATEGORY" />
		</Table>
	</xsl:template>
	<xsl:template match="TRAININGCATEGORY">
			<TR>
				<TD class="promoCopyBold"><xsl:value-of select="MJDESCRIPTION" /><xsl:text disable-output-escaping="yes"> > </xsl:text><xsl:value-of select="MNDESCRIPTION" /></TD>
			</TR>
			<TR>
				<TD class="promoCopyMid" valign="TOP" >
					<xsl:apply-templates select="TRAININGCATCOURSE">
						<xsl:sort select="CTITLE" />
					</xsl:apply-templates>
				</TD>
			</TR>
			<TR></TR>
	</xsl:template>
	<xsl:template match="TRAININGCATCOURSE">
		<li><a href="sony-training-catalog-2.aspx#{CNUMBER}"><xsl:value-of select="CTITLE" /></a></li>
	</xsl:template>
</xsl:stylesheet>

  