Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp

    Partial Class ViewSavedCart
        Inherits SSL

        Public cm As New CustomerManager
        Public customer As Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Dim pm As New PaymentManager
            'Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart

            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = Session.Item("customer")
                Else
                    ErrorLabel.Text = Resources.Resource.el_RegistertoAddCart '("You must be a registered customer to view a shopping cart.")
                    Return
                End If

                If customer.IsPunchOutUser Then
                    Checkout.Visible = False
                    PunchOut.Visible = True
                    checkouttext.Visible = False
                Else
                    If (ConfigurationData.GeneralSettings.General.EnableCheckOut) Then
                        Checkout.Visible = True
                        lblMessageText.Text = ""
                    Else
                        Checkout.Visible = False
                        ' lblMessageText.Text = "Online ordering is temporarily unavailable. Please order by calling 1-800-538-7550 or emailing" + "<a  style=""color:Blue"" href=""mailto:ProParts@am.sony.com"" +"">" + "  ProParts@am.sony.com" + "</a>"
                        lblMessageText.Text = Resources.Resource.el_Order_Unavailable1 + "<a  style=""color:Blue"" href=""mailto:ProParts@am.sony.com"" +"">" + "  ProParts@am.sony.com" + "</a>"
                    End If
                End If

                formFieldInitialization()

                If Session.Item("carts") IsNot Nothing Then
                    cart = Session.Item("carts")
                Else
                    ErrorLabel.Text = Resources.Resource.el_EmptyShoppingCard '"Shopping cart is empty."
                    PunchOut.Visible = False
                    Return
                End If

                If cart.PurchaseOrderNumber IsNot Nothing And String.IsNullOrWhiteSpace(TextBoxPO.Text) Then TextBoxPO.Text = cart.PurchaseOrderNumber

                lblQuote.Text = cart.ReferenceName
                lblExpires.Text = cart.ExpirationDate
                tdLegend.Visible = False
                GetCart()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub GetCart()
            Try
                Dim cart As ShoppingCart
                Dim strDescription As String
                'Dim PartsAvailable As Int32
                Dim isFRB As Boolean
                Dim isKit As Boolean
                Dim isWhiteRow As Boolean
                Dim n As Int32 = 0
                Dim sb As New StringBuilder
                Dim sbKit As New StringBuilder
                Dim TR As New TableRow
                Dim TD As New TableCell
                Dim promoManager As New PromotionManager
                Dim hasPromo As Boolean = False
                Dim boolrecycleflag As Boolean = False
                Dim boolshowkitinfo As Boolean = False

                If Session.Item("carts") Is Nothing Then
                    ErrorLabel.Text = Resources.Resource.el_EmptyShoppingCard '"Shopping cart is empty."
                    Return
                End If
                cart = Session.Item("carts")

                'Cart header
                tblCartDetail.Rows.Clear()

                TD.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""670"">"))
                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("</tr>"))

                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""80"" bgColor=""#d5dee9"" height=""28"">&nbsp;&nbsp;" + Resources.Resource.el_itemNumber + "</td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""160"" bgColor=""#d5dee9"" height=""28"">&nbsp;&nbsp;" + Resources.Resource.el_Description + "</td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))

                If cart.ContainsDownloadOnlyItems Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader""  colspan =3 align=""center"" width=""72"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_Quantity + Resources.Resource.el_Requested + "</td>"))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""72"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_Quantity + "<br>" + Resources.Resource.el_Requested + " </td>"))
                End If

                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))
                If Not cart.ContainsDownloadOnlyItems Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_Quantity + "<br>" + Resources.Resource.el_Available + "</td>"))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))
                End If

                TD.Controls.Add(New LiteralControl("<td class=""tableHeader""  align=""right"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_YourPrice + "&nbsp;</td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))

                'price difference for CA and US 
                If HttpContextManager.GlobalData.IsCanada Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_ListPrice_CAD + "&nbsp;</td>"))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_ListPrice + "&nbsp;</td>"))
                End If

                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td>"))

                If HttpContextManager.GlobalData.IsCanada Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_TotalPrice_CAD + "&nbsp;</td>"))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""70"" bgColor=""#d5dee9"" height=""28"">" + Resources.Resource.el_TotalPrice + "&nbsp;</td>"))
                End If
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""28"" src=""images/spacer.gif"" width=""1""></td></tr>"))

                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""80"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""160"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""72"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("</TR>"))

                Me.tdLegend.Visible = False
                Me.tdrecyclefee.Visible = False
                Me.tdshowkitinfo.Visible = False

                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    isFRB = False
                    isKit = False
                    isWhiteRow = False

                    Dim tb As New Sony.US.ServicesPLUS.Controls.SPSTextBox With {
                        .ID = $"tb{n}",
                        .Width = Unit.Pixel(40),
                        .Text = i.Quantity.ToString()
                    }

                    If i.Product.Type = Product.ProductType.Kit Then
                        strDescription = $" <span class=tableHeader><span class=""redAsterick"">*</span>{Resources.Resource.el_KIT}</span> {i.Product.Description}"
                        isKit = True
                        boolshowkitinfo = True
                    Else
                        If i.Product.IsFRB Then
                            strDescription = $"<span class=tableHeader><span class=""redAsterick"">*</span>{Resources.Resource.el_Price_FRB}</span> {i.Product.Description}"
                            isFRB = True
                        Else
                            strDescription = (i.Product.Description)
                        End If
                    End If

                    TD.Controls.Add(New LiteralControl("<tr><td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
                    '2 Product Number
                    Dim strPartNumber As String
                    If i.Product.SpecialTax > 0.0 Then
                        strPartNumber = i.Product.PartNumber + ("<span class=tableHeader><span class=""redAsterick"">*</span></span> ")
                        TD.Controls.Add(New LiteralControl("<td width=""80"" class=""tableHeader"">&nbsp;&nbsp;" + strPartNumber + "</td>"))
                        boolrecycleflag = True
                    Else
                        strPartNumber = i.Product.PartNumber
                        TD.Controls.Add(New LiteralControl("<td width=""80"" class=""tableHeader"">&nbsp;&nbsp;" + strPartNumber + "</td>"))
                        lblNote.Text = Resources.Resource.el_ApplicableShippingFee ' "The subtotal does not include any applicable shipping fees or tax."
                    End If
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

                    '4 Product Description
                    TD.Controls.Add(New LiteralControl("<td width=""160"" class=""tableData"">&nbsp;&nbsp;" + strDescription + "</td>"))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

                    '6 Quantity 
                    If cart.ContainsDownloadOnlyItems Then
                        TD.Controls.Add(New LiteralControl("<td width=""72"" colspan =3 class=""tableData"" align=""center"">" + i.Quantity.ToString() + "</td>"))
                    Else
                        TD.Controls.Add(New LiteralControl("<td width=""72"" class=""tableData"" align=""center"">" + i.Quantity.ToString() + "</td>"))
                    End If

                    '8 Quantity Available
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
                    Dim nAvailToShow As String = "0"
                    If isKit Then
                        nAvailToShow = String.Empty
                    Else
                        If i.Quantity <= i.NumberOfPartsAvailable Then
                            nAvailToShow = i.Quantity
                        Else
                            nAvailToShow = i.NumberOfPartsAvailable
                        End If
                    End If
                    If Not cart.ContainsDownloadOnlyItems Then
                        TD.Controls.Add(New LiteralControl("<td width=""70"" class=""tableData"" align=""center"">" + nAvailToShow + "</td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
                    End If

                    '10 Your Price
                    If customer.IsActive Then
                        TD.Controls.Add(New LiteralControl("<td width=""70"" class=""tableData"" align=""right"">"))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.YourPrice)).ToString()))
                        If promoManager.IsPromotionPart(strPartNumber) Then
                            TD.Controls.Add(New LiteralControl("<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"))
                            hasPromo = True
                        End If
                        TD.Controls.Add(New LiteralControl("&nbsp;</td>"))
                    Else
                        TD.Controls.Add(New LiteralControl("<td width=""70""  class=""tableData"" align=""right"">"))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.Product.ListPrice)).ToString() + "&nbsp;</td>"))
                    End If
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

                    '12 List Price
                    TD.Controls.Add(New LiteralControl("<td width=""70""  class=""tableData"" align=""right"">"))
                    TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.Product.ListPrice)).ToString() + "&nbsp;</td>"))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))

                    '14 Total
                    TD.Controls.Add(New LiteralControl("<td width=""70"" class=""tableData"" align=""right"">"))
                    TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.TotalLineItemPrice)).ToString() + "&nbsp;</td>"))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1""></td>"))
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1""></td></tr>"))

                    If (isFRB) Then
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=17 bgcolor=""#d5dee9""><span class=""legalCopy""><span class=""redAsterick"">*</span>" + Resources.Resource.el_Price_FRB + Resources.Resource.el_Price_FRB1))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.Product.CoreCharge)).ToString() + Resources.Resource.el_Price_FRB2))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", i.Product.CoreCharge)).ToString() + " credit per unit returned.<br></td></tr>"))
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=17><IMG height=""1"" src=""images/spacer.gif"" width=""20"">"))
                    End If

                    If (isKit) Then
                        Dim k As Sony.US.ServicesPLUS.Core.Kit 'In kits
                        k = i.Product
                        TD.Controls.Add(New LiteralControl("<TR>"))
                        'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">" + x.Product.PartNumber + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5""><IMG height=""1"" src=""images/spacer.gif"" width=""10""><span class=""popupCopyBold"">" + Resources.Resource.el_KitspartDesc + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""popupCopyBold"">" + Resources.Resource.el_KitQty + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""popupCopyBold"">" + Resources.Resource.el_QtyAval + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                        'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">List Price<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))'7412

                        'price difference for CA and US 
                        If HttpContextManager.GlobalData.IsCanada Then
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">" + Resources.Resource.el_extndListPrice_CAD + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>")) '7412
                        Else
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">" + Resources.Resource.el_extndListPrice + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>")) '7412
                        End If
                        TD.Controls.Add(New LiteralControl("</TR>"))
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=17><IMG height=""1"" src=""images/spacer.gif"" width=""20""></tr>"))
                        For Each x As Sony.US.ServicesPLUS.Core.KitItem In k.Items
                            '7412 starts
                            ''Kit  Quantity(Available)
                            'Dim nKitAvailToShow As Integer
                            'If x.Quantity <= x.NumberOfPartsAvailable Then
                            '    nKitAvailToShow = x.Quantity
                            'Else
                            '    nKitAvailToShow = x.NumberOfPartsAvailable
                            'End If
                            'Kit  Quantity(Available)
                            Dim nKitAvailToShow As Integer
                            Dim showtotalqty As Integer = i.Quantity * x.Quantity

                            If x.NumberOfPartsAvailable >= showtotalqty Then
                                nKitAvailToShow = showtotalqty
                            Else
                                nKitAvailToShow = x.NumberOfPartsAvailable
                            End If
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<TR>"))
                            'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">" + x.Product.PartNumber + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5""><IMG height=""1"" src=""images/spacer.gif"" width=""10""><span class=""legalCopy"">" + x.Product.Description + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            '7412 starts
                            'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><IMG height=""1"" src=""images/spacer.gif"" width=""30""><span class=""legalCopy"">" + x.Quantity.ToString + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            Dim ntotalqty As Integer = i.Quantity * x.Quantity
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><IMG height=""1"" src=""images/spacer.gif"" width=""30""><span class=""legalCopy"">" + ntotalqty.ToString + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4"" align=""center""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">" + nKitAvailToShow.ToString + "<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><IMG height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">"))
                            '7412 starts
                            'TD.Controls.Add(New LiteralControl((String.Format("{0:c}", x.Product.ListPrice)).ToString()))
                            Dim qtyreq_ELP, totqty_ELP As Double
                            qtyreq_ELP = Convert.ToDouble(ntotalqty)
                            totqty_ELP = x.Product.ListPrice * qtyreq_ELP
                            TD.Controls.Add(New LiteralControl((String.Format("{0:c}", totqty_ELP)).ToString()))
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<IMG height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            TD.Controls.Add(New LiteralControl("</TR>"))
                            TD.Controls.Add(New LiteralControl("<tr><td colspan=17><IMG height=""1"" src=""images/spacer.gif"" width=""20""></tr>"))
                        Next
                    End If
                    n = n + 1
                Next

                ' Set visibility of page elements showing explanations and warnings.
                If boolrecycleflag = True Then
                    tdrecyclefee.Visible = True
                    lblNote.Text = Resources.Resource.el_SubtotalInfo '"The subtotal does not include any applicable shipping fees, tax, or recycle fees."
                End If
                If hasPromo Then tdLegend.Visible = True
                If boolshowkitinfo Then tdshowkitinfo.Visible = True

                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""80"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""160"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""72"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("</TR>"))

                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""80"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""160"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""72"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""70"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" width=""70"" bgColor=""#d5dee9"">SUB TOTAL<IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" width=""70"" bgColor=""#d5dee9"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cart.ShoppingCartTotalPrice)).ToString() + "<IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("</TR>"))

                TD.Controls.Add(New LiteralControl("<TR>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td bgColor=""#d5dee9"" height=""1""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><IMG height=""1"" src=""images/spacer.gif"" width=""1""></td>"))
                TD.Controls.Add(New LiteralControl("</tr>"))

                TD.Controls.Add(New LiteralControl("</table>"))

                TR.Cells.Add(TD)
                tblCartDetail.Rows.Add(TR)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub AddPO_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AddPO.Click
            Dim cart As ShoppingCart
            Dim sm As New ShoppingCartManager
            'Dim pm As New PaymentManager

            Try
                If String.IsNullOrWhiteSpace(TextBoxPO.Text) Then
                    POErrorLabel.Text = Resources.Resource.el_EnterPONum        ' "You must enter a PO number."
                    Return
                End If
                If Session.Item("carts") Is Nothing Then
                    ErrorLabel.Text = Resources.Resource.el_EmptyShoppingCard   ' "Shopping cart is empty."
                    Return
                End If
                cart = Session.Item("carts")
                cart.PurchaseOrderNumber = TextBoxPO.Text
                sm.UpdateShoppingCart(cart)
                TextBoxPO.ReadOnly = True
            Catch ex As Exception
                POErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub continueShopping_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles continueShopping.Click
            Response.Redirect("Member.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Private Sub Checkout_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Checkout.Click
            'Dim cart As ShoppingCart
            Dim cc_only_purchase = True
            Dim currentpage As Integer = 0

            Try
                If customer Is Nothing Then
                    Response.Redirect("default.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Return
                End If

                If Session.Item("carts") Is Nothing Then
                    ErrorLabel.Text = Resources.Resource.el_CurrentCartEmpty '"Current cart is empty."
                    PunchOut.Visible = False
                    Return
                End If

                ' 2016-05-20 ASleight - Cleaning up the logic for this sanity check. Looping isn't required if we're not checking account details
                'For Each sap As Account In customer.SAPBillToAccounts
                '    'If sap.Validated Then 'commented for 6865
                '    If customer.UserType = "A" Then
                '        cc_only_purchase = False
                '    End If
                'Next
                'For Each sis As SISAccount In customer.SISLegacyBillToAccounts
                '    'If sis.Validated Then 'commented for 6865
                '    If customer.UserType = "A" Then
                '        cc_only_purchase = False
                '    End If
                'Next
                If customer.UserType = "A" Then
                    If (customer.SAPBillToAccounts.Length > 0) Or (customer.SISLegacyBillToAccounts.Length > 0) Then
                        cc_only_purchase = False
                    End If
                End If

                '-- if previous order is left open cancel it
                If Session.Item("CurrentPageIndex") IsNot Nothing Then
                    currentpage = Convert.ToInt32(Session.Item("CurrentPageIndex").ToString())
                    Session.Remove("CurrentPageIndex")
                End If

                'If currentpage > 1 Then
                '    cancelOpenOrder()
                'End If

                If Session.Item("currentProcessIndex") IsNot Nothing Then
                    Session.Remove("currentProcessIndex")
                End If

                '--- modification for disabling back button 
                '-- current process index - 0 => parts check out
                Session.Add("currentProcessIndex", "0")
                Session.Add("CurrentPageIndex", "0") '-- check out button click is the begining of the check out process

                If Not cc_only_purchase And customer.IsActive Then
                    Response.Redirect("AccntHolder.aspx", False)
                Else
                    Response.Redirect("NAccntHolder.aspx", False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        'Private Sub cancelOpenOrder()
        '    If Not Session.Item("order") Is Nothing And Not Session.Item("carts") Is Nothing Then
        '        Try
        '            Dim order As Sony.US.ServicesPLUS.Core.Order = Session.Item("order")
        '            Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
        '            Dim om As New OrderManager

        '            '***************Changes done for Bug# 1212 Starts here***************
        '            '**********Kannapiran S****************
        '            
        '            objSISParameter.UserLocation = customer.UserLocation
        '            om.CancelOrder(order, cart)
        '            '***************Changes done for Bug# 1212 Ends here***************

        '        Catch ex As Exception
        '            ErrorLabel.Text = ex.Message
        '        End Try
        '    End If
        'End Sub

        Private Sub EmailQuote_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles EmailQuote.Click
            Dim cart As ShoppingCart
            Dim sm As New ShoppingCartManager
            Dim EmailBody As New StringBuilder(5000)
            Dim Subject As String = String.Empty

            Try
                If customer Is Nothing Then
                    ErrorLabel.Text = Resources.Resource.el_RegistertoAddCart ' ("You must be a registered customer to view a shopping cart.")
                    Return
                End If

                If Session.Item("carts") Is Nothing Then
                    ErrorLabel.Text = Resources.Resource.el_EmptyShoppingCard '"Shopping cart is empty."
                    PunchOut.Visible = False
                    Return
                Else
                    cart = Session.Item("carts")
                End If

                buildMailBody(EmailBody, Subject)
                sm.SendShoppingCartInEmail(customer.EmailAddress, EmailBody.ToString(), Subject)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub formFieldInitialization()
            TextBoxPO.Attributes("onkeydown") = "SetTheFocusButton(event, 'Checkout')"
        End Sub

        'Protected Sub PunchOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PunchOut.Click
        '    Try
        '        If Not Session.Item("customer") Is Nothing Then
        '            customer = Session.Item("customer")
        '        Else
        '            ErrorLabel.Text = Resources.Resource.el_RegisterPunchout '("You must be a registered customer to punchout shopping cart. Please login into system.")
        '            Return
        '        End If

        '        Dim strDataToPost As String

        '        If Not Session.Item("carts") Is Nothing Then
        '            Dim cart As ShoppingCart
        '            cart = Session.Item("carts")
        '            If cart.ShoppingCartItems.Length < 1 Then
        '                ErrorLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
        '                PunchOut.Visible = False
        '                Return
        '            Else
        '                strDataToPost = CXMLOperation.GetOrderCXML(cart)
        '                'PunchOut Cart
        '            End If
        '        Else
        '            ErrorLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
        '            PunchOut.Visible = False
        '            Return
        '        End If

        '        Try
        '            Dim myWebRequest As WebRequest
        '            Dim myRequestStream As Stream
        '            Dim myStreamWriter As StreamWriter

        '            Dim myWebResponse As WebResponse
        '            Dim myResponseStream As Stream
        '            Dim myStreamReader As StreamReader

        '            ' Create a new WebRequest which targets that page with
        '            ' which we want to interact.
        '            Dim objPunchOutRequestData As PunchOutRequestData = Nothing
        '            If Not Session("PunchOutRequestData") Is Nothing Then
        '                objPunchOutRequestData = Session("PunchOutRequestData")
        '            Else
        '                If Not Session("PunchOutSesison") Then
        '                    objPunchOutRequestData = CXMLSesisonManager.GetPunchOutRequestData(Session("PunchOutSesison").ToString())
        '                End If
        '            End If
        '            myWebRequest = WebRequest.Create(objPunchOutRequestData.POSTTOURL.Replace("&", "&amp;"))

        '            ' Set the method to "POST" and the content type so the
        '            ' server knows to expect form data in the body of the
        '            ' request.
        '            With myWebRequest
        '                .Method = "POST"
        '                .ContentType = "application/x-www-form-urlencoded"
        '            End With


        '            ' Get a handle on the Stream of data we're sending to
        '            ' the remote server, connect a StreamWriter to it, and
        '            ' write our data to the Stream using the StreamWriter.

        '            myRequestStream = myWebRequest.GetRequestStream()
        '            myStreamWriter = New StreamWriter(myRequestStream)
        '            myStreamWriter.Write(strDataToPost)
        '            myStreamWriter.Flush()
        '            myStreamWriter.Close()
        '            myRequestStream.Close()

        '            ' Get the response from the remote server.
        '            myWebResponse = myWebRequest.GetResponse()

        '            ' Get the server's response status?

        '            ' Just like when we sent the data, we'll get a reference
        '            ' to the response Stream, connect a StreamReader to the
        '            ' Stream and use the reader to actually read the reply.
        '            myResponseStream = myWebResponse.GetResponseStream()
        '            myStreamReader = New StreamReader(myResponseStream)

        '            Dim strResponse As String = myStreamReader.ReadToEnd()
        '            myStreamReader.Close()
        '            myResponseStream.Close()

        '            ' Close the WebResponse
        '            myWebResponse.Close()
        '            Session.Remove("carts")
        '            Session.Clear()
        '            Session.Abandon()
        '            'Dim SessionHashObject As Hashtable = Application("punchout")
        '            'CXMLSesisonManager.RemovePunchOutSession(Session.SessionID, SessionHashObject)

        '            'Response.Redirect("PunchOutSuccess.aspx?punchid=" + strResponse, True)
        '            Response.Redirect(objPunchOutRequestData.POSTTOURL + "?punchid=" + strResponse, True)

        '        Catch ex As Exception
        '            Throw ex
        '        End Try
        '    Catch exThrd As Threading.ThreadAbortException
        '        'Do nothing




        '    Catch ex As Exception
        '        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

        Sub buildMailBody(ByRef Emailbody As StringBuilder, ByRef Subject As String)
            Dim cart As ShoppingCart

            customer = Session.Item("customer")
            If Session.Item("carts") Is Nothing Then
                ErrorLabel.Text = "Shopping cart is empty."
                PunchOut.Visible = False
                Return
            Else
                cart = Session.Item("carts")
            End If

            Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
            Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
            Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
            Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
            Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
            Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
            Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
            Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link
            Dim terms_and_conditions_link_ca As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link_ca
            Dim contact_us_link_ca As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link_ca
            Dim customer_service_link_ca As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link_ca

            Emailbody.Append(Resources.Resource_mail.ml_common_Dear_en + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line1_en + servicesplus_link + "." + vbCrLf + vbCrLf) 'need cart url

            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line2_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line3_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line4_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line5_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line6_en + cart.SequenceNumber.ToString() + ". " + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line7_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line8_en)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line9_en)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line10_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line11_en + vbCrLf + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_Sincerely_en + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_en + vbCrLf + vbCrLf)

            'cart details
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_PONumber_en + cart.PurchaseOrderNumber + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_QuoteNumber_en + cart.ReferenceName + vbCrLf)
            Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_QuiteExpires_en + cart.ExpirationDate + vbCrLf + vbCrLf)

            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                Emailbody.Append(item.Product.PartNumber + vbCrLf)
                Emailbody.Append(item.Product.Description + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_Quantity_en + " " + item.Quantity.ToString() + vbTab)

                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_ItemPrice_en + " $" + Format(item.YourPrice, "##0.00") + vbTab)
                '$" + Format(p.ListPrice, "##0.00")
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_ExtendedPrice_en + " $" + Format(item.TotalLineItemPrice, "##0.00") + vbCrLf + vbCrLf)
            Next

            Emailbody.Append(vbCrLf + Resources.Resource_mail.ml_EmailQuote_TotalExtendedPrice_en + " $" + Format(cart.ShoppingCartTotalPrice, "##0.00") + vbCrLf + vbCrLf)

            Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_en + vbCrLf + vbCrLf)
            Emailbody.Append("")
            Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_en + privacy_policy_link + vbCrLf)

            If HttpContextManager.GlobalData.IsAmerica Then
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_en + customer_service_link + vbCrLf)
            Else
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_en + terms_and_conditions_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_en + contact_us_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_en + customer_service_link_ca + vbCrLf)
            End If


            If HttpContextManager.GlobalData.IsCanada Then
                Emailbody.Append("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" + vbCrLf + vbCrLf)

                Emailbody.Append(Resources.Resource_mail.ml_common_Dear_fr + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line1_fr + servicesplus_link + "." + vbCrLf + vbCrLf) 'need cart url

                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line2_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line3_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line4_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line5_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line6_fr + cart.SequenceNumber.ToString() + ". " + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line7_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line8_fr)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line9_fr)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line10_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_body_line11_fr + vbCrLf + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_sincerely_fr + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ServicesPLUSTeam_fr + vbCrLf + vbCrLf)

                'cart details
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_PONumber_fr + cart.PurchaseOrderNumber + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_QuoteNumber_fr + cart.ReferenceName + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_QuiteExpires_fr + cart.ExpirationDate + vbCrLf + vbCrLf)

                For Each item As ShoppingCartItem In cart.ShoppingCartItems
                    Emailbody.Append(item.Product.PartNumber + vbCrLf)
                    Emailbody.Append(item.Product.Description + vbCrLf)
                    Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_Quantity_fr + " " + item.Quantity.ToString() + vbTab)
                    Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_ItemPrice_fr + " $" + Format(item.YourPrice, "##0.00") + vbTab)
                    Emailbody.Append(Resources.Resource_mail.ml_EmailQuote_ExtendedPrice_fr + " $" + Format(item.TotalLineItemPrice, "##0.00") + vbCrLf + vbCrLf)
                Next

                Emailbody.Append(vbCrLf + Resources.Resource_mail.ml_EmailQuote_TotalExtendedPrice_fr + " $" + Format(cart.ShoppingCartTotalPrice, "##0.00") + vbCrLf + vbCrLf)

                Emailbody.Append(Resources.Resource_mail.ml_common_CopyRights_fr + vbCrLf + vbCrLf)
                Emailbody.Append("")
                Emailbody.Append(Resources.Resource_mail.ml_common_PrivacyPolicy_fr + privacy_policy_link + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_TC_fr + terms_and_conditions_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_ContactUs_fr + contact_us_link_ca + vbCrLf)
                Emailbody.Append(Resources.Resource_mail.ml_common_CustomerService_fr + customer_service_link_ca + vbCrLf)

                Subject = Resources.Resource_mail.ml_EmailQuote_subject_en + cart.ReferenceName + ")" + "/" + Resources.Resource_mail.ml_EmailQuote_subject_fr + cart.ReferenceName + ")"
            Else
                Subject = Resources.Resource_mail.ml_EmailQuote_subject_en + cart.ReferenceName + ")"
            End If
        End Sub

    End Class

End Namespace
