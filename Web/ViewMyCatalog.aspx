<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.Migrated_ViewMyCatalog"
    EnableViewStateMac="true" CodeFile="ViewMyCatalog.aspx.vb" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_MyCatalog%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <script type="text/javascript">
        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }
        var exception = false;
        $(document).ready(function () {
            $("#messageclose").click(function () {
                $("#dialog").dialog("close");
            });
        });                       //Document ready

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {
            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            }
            else {
                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    if (jQuery.trim(partnumber.val()) === "") {
                        exception = true;

                        $("#divException").append("<strong>" + "<%= Resources.Resource.atc_ex_InvalidPartNumber%>" + qty.val().tostring() + "</strong><br/>");
                        //$("#divMessage").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong>");
                        $("#divException").append("<br/>");

                    } else {
                        exception = true;
                        $("#divException").append("<br/>");
                        $("#divException").append("<strong>" + "<%= Resources.Resource.atc_ex_InvalidQuantity%>" + partnumber.val() + "</strong><br/>");
                        // $("#divMessage").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;
                        $("#divException").append("<strong>  " + "<%= Resources.Resource.el_item%>" + " " + partnumber.val() + "<%= Resources.Resource.el_Pts_OrderQty%>" + "</strong><br/><br/>");
                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;
                        $("#divException").append("<strong>  " + "<%= Resources.Resource.el_item%>" + " " + partnumber.val() + "<%= Resources.Resource.el_Pts_OrderQty%>" + " </strong><br/><br/>");
                        return false;
                    }
                }
                return true;
            }
        }

        function AddToCart_onclick() {
            exception = false;

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"><%= Resources.Resource.prg_PleaseWait%><br/> <img src="images/progbar.gif" alt="" /></span>');
            $("#divMessage").append(' <br/>');
            var i = 1;
            var totalNull = "";
            var count = 0;
            // $('#hidespstextitemno').val(val)
            //$('#hidespstextqty').val(1)
            //var sQty = '#hidespstextqty'
            //var sPart = '#hidespstextitemno'
            var lboxcount = $("#hidelistbox").find("option").length - 1;

            //for (i = 1; i <= 10; i++) {
            for (i = 0; i <= lboxcount; i++) {
                //var sQty = '#Qty' + i
                $('#hidespstextqty').val(1);
                var sQty = '#hidespstextqty';

                //var sPart = '#PartNumber' + i
                var test = $('#hidelistbox').find("option")[i].value
                $('#hidespstextitemno').val(test);
                var sPart = '#hidespstextitemno';

                var qty = $(sQty);
                var part = $(sPart);

                totalNull = totalNull + qty.val() + part.val();

                //if (totalNull === "" && i === 10) {
                if (totalNull === "" && i === lboxcount) {
                    $("#divMessage").text('');
                    ///   $("#divMessage").html("Please enter at least one part number.");
                    $("#dialog").dialog({
                        title: "<%= Resources.Resource.atc_title_QuickOrderExceptions%>"
                    });
                    $("#divMessage").append('<br/> <br/><a><img src="<%= Resources.Resource.img_btnCloseWindow()%>" alt ="Close this popup dialog." onclick="javascript:return closedilog();" id="messageclose" /></a>');
                }
                var itemsInCart = $("#itemsInCart");
                var totalPriceOfCart = $("#totalPriceOfCart");
                var destination = itemsInCart.position();
                var position = part.position();

                $("#dialog").dialog({
                    title: "<strong><%= Resources.Resource.atc_title_OrderStatus%></strong>",
                    height: 200,
                    width: 560,
                    modal: true,
                    position: 'top',
                    resizable: false
                });
                count = count + 1;

                if (validPart(part, qty)) {
                    count = count + 1;
                    $("#divMessage").html("<br/><strong><%= Resources.Resource.atc_AddingItem%>" + replaceAll(part.val(), "-", "") + "<%= Resources.Resource.atc_ToCart%></strong><br/>");
                    var parametervalues = '{sPartNumber: "' + jQuery.trim(replaceAll(part.val(), "-", "")) + '",sPartQuantity: "' + jQuery.trim(qty[0].value) + '" }';
                    //Call webmethod using ajax
                    jQuery.ajax({
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        data: parametervalues,
                        dataType: 'json',
                        url: 'ViewMyCatalog.aspx/addPartToCartwSMyCatalog',
                        success: function (result) {
                            count = count - 1;
                            if (result.d.differentCart === true) {
                                exception = true;
                                $("#divException").append("<br/><strong>" + "<%= Resources.Resource.atc_ex_DifferentCart%>" + "</strong><br/>");
                            }

                            if (result.d.success === 1) {
                                $("#divMessage").html("<br/><strong>" + "<%= Resources.Resource.atc_AddingItem%>" + replaceAll(result.d.partnumber, "-", "") + "<%= Resources.Resource.atc_ToCart%>" + " </strong><br/>");
                                result.d.partnumber = '';
                                result.d.Quantity = '';
                                itemsInCart.text(result.d.items);
                                totalPriceOfCart.text(result.d.totalAmount);
                            }
                            else if (result.d.success === 2) {
                                exception = true;
                                $("#divMessage").html("<br/><strong>" + "<%= Resources.Resource.atc_AddingItem%>" + replaceAll(result.d.partnumber, "-", "") + "<%= Resources.Resource.atc_ToCart%>" + " </strong><br/>");
                                result.d.partnumber = '';
                                result.d.Quantity = '';
                                itemsInCart.text(result.d.items);
                                totalPriceOfCart.text(result.d.totalAmount);
                                $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                            }
                            //Bug 9052: added by satheesh for handling negative values.-start
                            else if (result.d.success === -1) {
                                exception = true;
                                $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                            }
                            //Bug 9052 :added by satheesh for handling negative values.-end
                            else {
                                exception = true;
                                $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                            }
                            //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");

                            if (count === 0) {
                                if (exception) {
                                    var sHeight = $("#divException").height() + 200;

                                    $("#progress").remove();
                                    $("#dialog").dialog({
                                        //title: 'Quick Order Add to Cart Exceptions',
                                        title: '<%= Resources.Resource.atc_title_OrderExceptions%>',
                                        height: sHeight.toString(),
                                        position: 'top'
                                    }).dialog("moveToTop");
                                    $("#divMessage").text('');
                                    $("#divException").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close this popup dialog." onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                                    $("#divException").show("fast");
                                }
                                else {
                                    $("#dialog").dialog("close");
                                }
                            }
                        }
                    });
                }
                count = count - 1;
            } //For each

            if (count === 0) {
                $("#progress").remove();
                if (exception) {
                    var sHeight = $("#divException").height() + 200;

                    $("#progress").remove();
                    $("#dialog").dialog({
                        //title: 'Quick Order Add to Cart Exceptions',
                        title: '<%= Resources.Resource.atc_title_OrderExceptions%>',
                        height: sHeight.toString(),
                        position: 'top'
                    }).dialog("moveToTop");
                    $("#divMessage").text('');
                    $("#divException").append('<br/> <br/><a ><img src ="<%= Resources.Resource.img_btnCloseWindow%>" alt="Close this popup dialog." onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                    $("#divException").show("fast");
                    return false; //6994
                }
            }
            return false; //6994
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="ValidationMessageDiv">
    </div>
    <div id="dialog" title="Order Add to Cart Status">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;">
        </div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;">
        </div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
            <img src="<%=Resources.Resource.img_btnCloseWindow()%>" alt="Close this popup dialog." onclick="javascript:return closedilog();"
                id="messageclose" />
        </div>
    </div>
    <center>
        <form name="form1" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_MyCatalog %></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <asp:Label ID="errorMessageLabel" runat="server" CssClass="tableData" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td valign="top" width="670">
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7" height="1">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="25" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td align="center" bgcolor="#ffffff" colspan="11">
                                                            <img height="25" hspace="0" src="<%=Resources.Resource.sna_svc_img_52%>" width="668"
                                                                vspace="0" border="0" alt="Your current re-order reminders.">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="25" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="80" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="255" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="63" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="13">
                                                            <asp:Table ID="MyReminderTable" runat="server" BorderWidth="0px">
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="80" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="255" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="90" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="63" bgcolor="#ffffff">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="5" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                        <td width="1" bgcolor="#b7b7b7">
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <img height="20" src="images/spacer.gif" width="670" alt=""><br />
                                                <table width="500" border="0" role="presentation">
                                                    <tr>
                                                        <td width="120" colspan="2">
                                                            <%--<img height="12" src="<%=Resources.Resource.sna_svc_img_58%>" width="87" alt="Sort My Catalog by:">--%>
                                                            <label for="SortCatalog" class="tableData"><%=Resources.Resource.cat_SortCatalogBy %></label>
                                                        </td>
                                                        <td>
                                                            <img height="1" src="images/spacer.gif" width="1" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%-- SORTING CONTROLS --%>
                                                        <td width="80">
                                                            <asp:DropDownList ID="SortCatalog" CssClass="tableData" runat="server" />
                                                        </td>
                                                        <td width="75" align="left">
                                                            <asp:ImageButton ID="SortCatalogButton" runat="server" ImageUrl="<%$ Resources:Resource,img_btnGoTo%>"
                                                                Width="40" Height="25" ToolTip="Sort Catalog" AlternateText="Sort Catalog"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <%-- PAGINATION NAV --%>
                                                            <table width="298" border="0" role="presentation">
                                                                <tr>
                                                                    <td width="60">
                                                                        <asp:ImageButton ID="btnPreviousPage" runat="server" Width="9" Height="9" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                            ToolTip="Previous Result Page"></asp:ImageButton>
                                                                        <asp:LinkButton ID="PreviousLinkButton" runat="server" CssClass="tableData" ToolTip="Previous Result Page"> <%=Resources.Resource.el_Previous%></asp:LinkButton>&nbsp;
                                                                    </td>
                                                                    <td class="tableData" align="center" width="50">
                                                                        <asp:Label ID="lblCurrentPage" runat="server">1</asp:Label>&nbsp;of&nbsp;
                                                                    <asp:Label ID="lblEndingPage" runat="server">1</asp:Label>
                                                                    </td>
                                                                    <td width="45">
                                                                        &nbsp;
                                                                    <asp:LinkButton ID="NextLinkButton" runat="server" CssClass="tableData" ToolTip="Next Result Page"><%=Resources.Resource.el_Next%> </asp:LinkButton>
                                                                        <asp:ImageButton ID="btnNextPage" runat="server" Width="9" Height="9" ImageUrl="images/sp_int_rightArrow_btn.gif"
                                                                            ToolTip="Next Result Page"></asp:ImageButton>
                                                                    </td>
                                                                    <td width="25">
                                                                        <img height="4" src="images/spacer.gif" width="25" alt="">
                                                                    </td>
                                                                    <td class="tableData" align="right" width="40">
                                                                        <label for="txtPageNumber"><%=Resources.Resource.el_Page%></label>
                                                                    </td>
                                                                    <td width="40">
                                                                        <SPS:SPSTextBox ID="txtPageNumber" runat="server" Width="40px" MaxLength="4"></SPS:SPSTextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnGoToPage" runat="server" Width="40" Height="25" ImageUrl="<%$ Resources:Resource,img_btnGoTo%>"
                                                                            ToolTip="Goto Page" AlternateText="Go to Specified Page"></asp:ImageButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <asp:Table ID="MyCatalogTable" runat="server" BorderWidth="0px" Caption="My Catalog items.">
                                                            </asp:Table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <img height="5" src="images/spacer.gif" width="1" alt=""><br>
                                                <table width="400" border="0" role="presentation">
                                                    <tr>
                                                        <td width="122">
                                                            <asp:ImageButton ID="AddToCartButton" runat="server" ImageUrl="<%$Resources:Resource,img_btnAddToCart%>"
                                                                Width="122" Height="30" ToolTip="Add this item to Shopping Cart" AlternateText="Add To Cart"></asp:ImageButton>
                                                        </td>
                                                        <td width="15">
                                                            <img height="15" src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="UpdateMyCatalogButton" runat="server" ImageUrl="<%$Resources:Resource,sna_svc_img_25%>"
                                                                Width="168" Height="30" ToolTip="Update Catalog" AlternateText="Update My Catalog"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <img height="15" src="images/spacer.gif" width="15" alt=""><br>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <%--6994 starts--%>
                            <tr>
                                <td align="left" colspan="3">
                                    <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none"></SPS:SPSTextBox>
                                    <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none"></SPS:SPSTextBox>
                                    <asp:ListBox ID="hidelistbox" runat="server" Style="display: none"></asp:ListBox>
                                </td>
                            </tr>
                            <%--6994 ends--%>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
