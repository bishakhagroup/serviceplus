Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class ShipMethod
        Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
        End Sub

        Public Sub Initialize(ByVal bVisible As Boolean)
            Me.Visible = bVisible
            BuildShipMethods()
            If Not bVisible Then
                rbShipMethods.SelectedIndex = rbShipMethods.Items.Count - 1
            End If
        End Sub

        ReadOnly Property SelectedShipMethodValue() As String
            Get
                Return rbShipMethods.SelectedValue
            End Get
        End Property

        ReadOnly Property SelectedShipMethodIndex() As Integer
            Get
                Return rbShipMethods.SelectedIndex
            End Get
        End Property

        'ReadOnly Property ShipBackChecked() As Boolean
        '    Get
        '        Return cbShipBack.Checked
        '    End Get
        'End Property

        Private Sub BuildShipMethods()
            Dim om As New OrderManager
            Dim ship_methods() As ShippingMethod
            lblShippingMessage.Text = ConfigurationData.GeneralSettings.ShippingMessage
            lblShippingMessage.Style.Item("Color") = ConfigurationData.GeneralSettings.ShippingMessageColor

			'Dim ObjGlobalData As New GlobalData
			'If Not Session.Item("GlobalData") Is Nothing Then
			'    ObjGlobalData = Session.Item("GlobalData")
			'End If

			ship_methods = om.GetAllShippingMethods(HttpContextManager.GlobalData)

            Session.Add("shipmethods", ship_methods)
            Dim shippingMethods As New ArrayList
            For Each method As ShippingMethod In ship_methods
                shippingMethods.Add(method.Description)
            Next

            rbShipMethods.DataSource = shippingMethods
            rbShipMethods.DataBind()
            'If rbShipMethods.SelectedItem.Value = "UPS" Then
            '    rbShipMethods.SelectedItem.Selected = True
            'End If
        End Sub

        Public Function ValidateForm() As Boolean
            Dim bValid As Boolean = True
            LabelShipMethods.Text = ""
            If rbShipMethods.SelectedIndex < 0 Then
                LabelShipMethods.Text = "Please select a shipping method."
                bValid = False
            End If
            Return bValid
        End Function
    End Class

End Namespace
