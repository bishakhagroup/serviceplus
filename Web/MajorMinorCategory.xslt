<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<Table border="0">
			<xsl:apply-templates select="//TRAININGMAJORCATEGORY">
				<xsl:sort select="MJDESCRIPTION" />
			</xsl:apply-templates>
		</Table>
	</xsl:template>
	<xsl:template match="TRAININGMAJORCATEGORY">
        <!-- 2016-04-29 ASleight adding variable and If statement to exclude Categories no longer offered. -->
        <xsl:variable name="MJCODE" select="MJCODE"/>
        <xsl:if test="$MJCODE != 3">
            <TR>
                <TD class="promoCopyBold">
                    <xsl:value-of select="MJDESCRIPTION" />
                </TD>
            </TR>
            <TR>
                <TD class="promoCopy" valign="TOP" >
                    <xsl:apply-templates select="TRAININGMINORCATEGORY">
                        <xsl:sort select="MNDESCRIPTION" />
                    </xsl:apply-templates>
                </TD>
            </TR>
            <TR></TR>
        </xsl:if>
	</xsl:template>
	<xsl:template match="TRAININGMINORCATEGORY">
	    <!-- 2016-04-29 ASleight adding variable and If statement to exclude Categories no longer offered.
	    It's not pretty, but XSL 1.0 doesn't allow for arrays or easily checking if a value is a member of a list.
	    My best attempt resulted in false positives such as MNCODE "14" excluding Categories 14, 1 and 4. -->
        <xsl:variable name="MNCODE" select="MNCODE"/>
        <xsl:if test="($MNCODE != 1) and ($MNCODE != 2) and ($MNCODE != 6) and ($MNCODE != 13) and 
        ($MNCODE != 14) and ($MNCODE != 16) and ($MNCODE != 36) and ($MNCODE != 56) and ($MNCODE != 76) and 
        ($MNCODE != 137) and ($MNCODE != 177) and ($MNCODE != 217)">
            <li><a href="sony-training-catalog-2.aspx?mincatid={MNCODE}"><xsl:value-of select="MNDESCRIPTION" /></a></li>
        </xsl:if>
	</xsl:template>
</xsl:stylesheet>

  