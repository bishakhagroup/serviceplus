<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NotAuthorized.aspx.vb" Inherits="NotAuthorized" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Not Authorized</title>
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
	<body  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
		<form method="post" runat="server" id="form1" >
			<table width="100%" border="0" role="presentation">
				<tr>
					<td>
					   &nbsp; <span class="tableheader">You are not authoirized to view this page.</span>
					</td>
				</tr>
			</table>
		</form>
	</body>

</html>
