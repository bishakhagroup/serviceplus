<%@ Control Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TopNav" CodeFile="TopNav.ascx.vb" %>

<link href="includes/svcplus_globalization.css" rel="stylesheet" />
<link href="includes/ServicesPLUS_style.css" rel="stylesheet" />

<div style="text-align: right; vertical-align: top;">
    <asp:PlaceHolder ID="MyCatalogPH" runat="server" />
    <asp:PlaceHolder ID="ViewCartPH" runat="server" />
    <asp:PlaceHolder ID="MyOrderPH" runat="server" />
    <asp:PlaceHolder ID="MyProfilePH" runat="server" />
    <asp:PlaceHolder ID="PromosPH" runat="server" />
    <asp:PlaceHolder ID="HelpPH" runat="server" />
    <asp:PlaceHolder ID="ContactUsPH" runat="server" />
    <asp:PlaceHolder ID="LogoutPH" runat="server" />
    <dl id="ddlLanguage" class="dropdown" style="display: inline-block; vertical-align: top; margin: 5px;">
        <dt><a href="#" class="dropdown"><span><%=selectedLanguage%></span></a></dt>
        <dd>
            <ul>
                <li><a href="#" class="dropdown">America (English)<span class="value">en-US</span></a></li>
                <li><a href="#" class="dropdown">Canada (English)<span class="value">en-CA</span></a></li>
                <li><a href="#" class="dropdown">Canada (Fran�ais)<span class="value">fr-CA</span></a></li>
            </ul>
        </dd>
    </dl>
</div>
<input type="hidden" id="hdnlogout" runat="server" />
