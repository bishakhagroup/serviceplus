<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.Product_Registration_Customer"
    EnableViewStateMac="true" CodeFile="product-registration-customer.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_ProfesstionalProduct%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
	<%
        If focusFormField <> "" Then
            Response.Write("document.getElementById('" + focusFormField + "').focus();")
        End If
	%>	
        function copyaddress() {
            if (document.getElementById("chkCopyCustomerInfo").checked === false) return;
            document.getElementById("txtLocationFirstName").value = document.getElementById("FirstName").value;
            document.getElementById("txtLocationLastName").value = document.getElementById("LastName").value;
            document.getElementById("txtLocationEmailAddreess").value = document.getElementById("Email").value;
            document.getElementById("txtLocationCompanyName").value = document.getElementById("CompanyName").value;
            document.getElementById("txtLocationAddress1").value = document.getElementById("Address1").value;
            document.getElementById("txtLocationAddress2").value = document.getElementById("Address2").value;
            document.getElementById("txtLocationAddress3").value = document.getElementById("Address3").value;
            document.getElementById("txtLocationCity").value = document.getElementById("City").value;
            document.getElementById("ddlLocationState").selectedIndex = document.getElementById("ddlCustomerInfoState").selectedIndex;
            document.getElementById("txtLocationZip").value = document.getElementById("Zip").value;
            document.getElementById("txtLocationPhone").value = document.getElementById("Phone").value;
            document.getElementById("txtLocationExtension").value = document.getElementById("Ext").value;
            document.getElementById("txtLocationFax").value = document.getElementById("Fax").value;
        }
    </script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form id="form1" method="post" runat="server" onsubmit="document.getElementById('hdnNavigator').value = navigator.appName;">
        <center>
            <table width="710" border="0" align="center">
                <tr>
                    <td width="710" bgcolor="#ffffff" style="height: 714px">
                        <table width="710" border="0">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="Label1" runat="server" CssClass="redAsterick"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_header_top_PartsPLUS-ServiceAgreements.gif"
                                                bgcolor="#363d45" height="82">
                                                <br/>
                                                <h1 class="headerText"><%=Resources.Resource.hplnk_svc_agreement%> &nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464">
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                                <img style="height: 9px" height="9" src="images/sp_int_header_btm_right.gif" width="246">
                                            </td>
                                        </tr>
                                    </table>
                                    <img src="images/spacer.gif" width="16">
                                    <asp:Label ID="errorMessageLabel" runat="server" ForeColor="Red" CssClass="tableData"
                                        EnableViewState="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                            <td width="1" height="18">
                                                <img height="15" src="images/spacer.gif" width="3">
                                            </td>
                                            <td height="18">
                                                <span class="tableHeader"><%=Resources.Resource.el_ProductReg_Cnt1%> </span><span
                                                    class="redAsterick">*</span><span class="tableHeader"><%=Resources.Resource.el_ProductReg_Cnt2%>  </span>
                                            </td>
                                        </tr>
                                        <%-- <tr>
												<td width="17" height="15"><img height="15" src="images/spacer.gif" width="20"></td>
						                        <td width="1" height="18"><img height="15" src="images/spacer.gif" width="3"></td>
						                        <td height="18"><span class="tableHeader">If you are signed in to ServicesPLUS, please review the information below and correct as necessary.</td>
					                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                            <td width="678" height="15">
                                                <img height="15" src="images/spacer.gif" width="670">
                                            </td>
                                            <td width="20" height="15">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                            <td width="678">
                                                <table width="670" border="0">
                                                    <tr bgcolor="#ffffff">
                                                        <td width="3">
                                                            <img height="1" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td width="127">
                                                            <img height="1" src="images/spacer.gif" width="125">
                                                        </td>
                                                        <td width="542">
                                                            <img height="1" src="images/spacer.gif" width="542">
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td colspan="2" height="18">
                                                            <span class="tableHeader"><%=Resources.Resource.el_CustInfo%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td width="127">
                                                        </td>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3">
                                                            <asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCIFirstName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_FirstName%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="FirstName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td align="right" width="127">
                                                            <asp:Label ID="lblCILastName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_LastName%>"></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="LastName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCIEmail" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_EmailAdd%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Email" runat="server" CssClass="bodyCopy" MaxLength="100"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                            <asp:Label ID="Label2" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label runat="server" ID="lblCICompanyName" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_CompanyName%>">
                                                            </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="CompanyName" runat="server" CssClass="bodyCopy" MaxLength="100"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span><span class="bodyCopy">&nbsp;&nbsp;<%=Resources.Resource.el_SelfEmp%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label runat="server" ID="lblCIAddress1" Text="<%$ Resources:Resource, el_rgn_StreetAddress%>">
                                                            </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Address1" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLineCI1Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label runat="server" ID="lblCIAddress2" Text="<%$ Resources:Resource, el_rgn_2ndAddress%>">
                                                            </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Address2" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLineCI2Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label runat="server" ID="lblCIAddress3" Text="<%$ Resources:Resource, el_Address3rdLine%>"> 
                                                            </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Address3" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLineCI3Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick"></span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCICity" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_City%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="City" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLineCI4Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCIState" runat="server" Text="<%$ Resources:Resource, el_rgn_State%>">   </asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <asp:DropDownList ID="ddlCustomerInfoState" CssClass="bodycopy" runat="server">
                                                        </asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCIZip" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_ZipCode%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Zip" runat="server" CssClass="bodyCopy" MaxLength="5"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                            <asp:RegularExpressionValidator ID="zipRegularExpressionValidator" runat="server"
                                                                CssClass="bodyCopy" ErrorMessage="You have entered an invalid zip code." ControlToValidate="Zip"
                                                                ForeColor=" " ValidationExpression="\d{5}(-\d{4})?" EnableClientScript="False"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblCIPhone" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Phone%>"></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            <table border="0">
                                                                <tr>
                                                                    <td style="width: 25%">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="Phone" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox><span
                                                                        class="redAsterick">*</span>
                                                                    </td>
                                                                    <td class="bodyCopy" width="20%">
                                                                        &nbsp;&nbsp;xxx-xxx-xxxx
                                                                    </td>
                                                                    <td class="bodyCopy" width="10%">
                                                                        <%=Resources.Resource.el_rgn_Extension%>
                                                                    &nbsp;
                                                                    </td>
                                                                    <td class="bodyCopy" width="35%">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="Ext" runat="server" CssClass="bodyCopy" Width="50" MaxLength="6"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td style="height: 30px">
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <%=Resources.Resource.el_rgn_Fax%>
                                                        &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="Fax" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox><span
                                                            class="bodyCopy">&nbsp;xxx-xxx-xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" colspan="3" height="20">
                                                            <img height="1" src="images/spacer.gif" width="3">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                            <td width="678">
                                                <table width="100%" border="0">
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td colspan="2" height="18" width="135">
                                                            <span class="tableHeader"><%=Resources.Resource.el_ProductLoc%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="18">
                                                            <span class="tableheader"><%=Resources.Resource.el_ProductReg_Contact%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td width="127">
                                                        </td>
                                                        <td>
                                                            <img src="images/spacer.gif" width="3">
                                                            <asp:Label ID="lblLocationError" runat="server" Visible="false" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td colspan="1" width="127">
                                                            &nbsp;
                                                        </td>
                                                        <td class="bodyCopy">
                                                            &nbsp;<input type="checkbox" id="chkCopyCustomerInfo" value="Copy Customer Information"
                                                                onclick="copyaddress();" class="bodyCopy" /><%=Resources.Resource.el_CopyCustInfo%>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIFirstName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_FirstName%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td width="550">
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationFirstName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td align="right" width="127">
                                                            <asp:Label ID="lblLILastName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_LastName%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationLastName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIEmail" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_EmailAdd%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationEmailAddreess" runat="server" CssClass="bodyCopy"
                                                            MaxLength="100"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                            <asp:Label ID="Label10" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLICompanyName" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_CompanyName%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationCompanyName" runat="server" CssClass="bodyCopy" MaxLength="100"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span><span class="bodyCopy">&nbsp;&nbsp;<%=Resources.Resource.el_SelfEmp%>  </span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIAddress1" runat="server" CssClass="bodycopy" Text="<%$ Resources:Resource, el_rgn_StreetAddress%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationAddress1" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLinePL1Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIAddress2" runat="server" CssClass="bodycopy" Text="<%$ Resources:Resource, el_rgn_2ndAddress%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationAddress2" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLinePL2Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick"></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIAddress3" runat="server" CssClass="bodycopy" Text="<%$ Resources:Resource, el_Address3rdLine%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationAddress3" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLinePL3Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick"></span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLICity" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_City%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationCity" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label
                                                            ID="LabelLinePL4Star" runat="server" CssClass="redAsterick"></asp:Label><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIState" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_State%>"> </asp:Label>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <asp:DropDownList ID="ddlLocationState" CssClass="bodycopy" runat="server">
                                                        </asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIZip" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_ZipCode%>"></asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationZip" runat="server" CssClass="bodyCopy" MaxLength="5"></SPS:SPSTextBox><span
                                                            class="redAsterick">*</span>
                                                            <asp:RegularExpressionValidator ID="zipLocationRegularExpressionValidator" runat="server"
                                                                CssClass="bodyCopy" ErrorMessage="You have entered an invalid zip code." ControlToValidate="txtLocationZip"
                                                                ForeColor=" " ValidationExpression="\d{5}(-\d{4})?" EnableClientScript="False"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblLIPhone" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_rgn_Phone%>">  </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            <table border="0">
                                                                <tr>
                                                                    <td width="25%">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="txtLocationPhone" runat="server" CssClass="bodyCopy"></SPS:SPSTextBox><span
                                                                        class="redAsterick">*</span>
                                                                    </td>
                                                                    <td class="bodyCopy" width="20%">
                                                                        &nbsp;&nbsp;xxx-xxx-xxxx
                                                                    </td>
                                                                    <td class="bodyCopy" width="10%">
                                                                        <%=Resources.Resource.el_rgn_Extension%>
                                                                    &nbsp;
                                                                    </td>
                                                                    <td class="bodyCopy" width="35%">
                                                                        &nbsp;
                                                                    <SPS:SPSTextBox ID="txtLocationExtension" runat="server" CssClass="bodyCopy" Width="50"
                                                                        MaxLength="6"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td height="25">
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <%=Resources.Resource.el_rgn_Fax%>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtLocationFax" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                            class="bodyCopy">&nbsp;xxx-xxx-xxxx</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td colspan="2">
                                                            <span class="bodyCopy">
                                                                <asp:Label ID="Label16" runat="server" CssClass="redAsterick"></asp:Label>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td width="3" colspan="3" style="height: 20px">
                                                            <img height="1" src="images/spacer.gif" width="3">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="15" src="images/spacer.gif" width="20">
                                            </td>
                                            <td width="678">
                                                <table width="670" border="0">
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td colspan="2" height="18">
                                                            <span class="tableHeader"><%=Resources.Resource.el_PurchaseInfo%> </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td width="127">
                                                        </td>
                                                        <td>
                                                            <img height="7" src="images/spacer.gif" width="3">
                                                            <asp:Label ID="ErrorDealer" runat="server" CssClass="redAsterick"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="127">
                                                            <asp:Label ID="lblDealer" runat="server" CssClass="bodyCopy" Text="<%$ Resources:Resource, el_ServiceAgreePurchaseForm%>"> </asp:Label>&nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <table border="0" cellspacing="0">
                                                            <tr>
                                                                <td valign="bottom">
                                                                    <asp:RadioButtonList ID="rdoPurchaseFrom" runat="server" CssClass="bodyCopy">
                                                                        <asp:ListItem Value="0" Text="<%$ Resources:Resource, el_ServiceAgree_Dealer%>"> </asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="<%$ Resources:Resource, el_ServiceAgree_AcctManager%>">    </asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="<%$ Resources:Resource, el_ServiceAgree_SonyStyle%>"> </asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </td>
                                                                <td valign="top" style="width: 137px">
                                                                    <SPS:SPSTextBox ID="txtDealer" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span
                                                                        class="redAsterick">*</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" colspan="3" height="20">
                                                            <img height="1" src="images/spacer.gif" width="3">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trAgreement">
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="3">
                                            </td>
                                            <td width="670" height="30" align="left" valign="top">
                                                <table>
                                                    <tr>
                                                        <td valign="top">
                                                            <input type="checkbox" id="optin" checked="checked" runat="server">
                                                        </td>
                                                        <td>
                                                            <span class="bodycopy"><%=Resources.Resource.el_ServiceAgree_Ackn%>  </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="3">
                                            </td>
                                            <td width="670" height="30" align="right" valign="middle">
                                                <asp:ImageButton ID="imgNext" ImageUrl="images/sp_int_next_btn.gif" runat="server"
                                                    AlternateText="Next" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" colspan="2" height="18">
                                            </td>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>
        <input type="hidden" runat="server" id="hdnNavigator" />
    </form>
</body>
</html>
