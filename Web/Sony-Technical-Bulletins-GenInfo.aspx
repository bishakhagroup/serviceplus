<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TechBulletinGenInfo" CodeFile="Sony-Technical-Bulletins-GenInfo.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register Src="~\UserControl\SPSPromotion.ascx" TagName="SPSPromotion" TagPrefix="uc1" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Technical Bulletins for Professional Products</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta content="sony, training, video, audio, broadcast" name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script>
        function launchWin2(url, newWidth, newHeight) {
            newWin = window.open(url, "", "toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=no,resize=1,width=" + newWidth + ",height=" + newHeight);
        }
    </script>
</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form id="Form1" method="post" runat="server">
        <center>
            <table style="width: 680px; height: 572px" width="680"
                border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td style="width: 727px" width="727" bgcolor="#ffffff">
                        <table width="708" border="0" role="presentation">
                            <tr>
                                <td style="width: 708px">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px; height: 1px">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_subhd_technical_bulletins.jpg"
                                                bgcolor="#363d45" height="82">
                                                <br>
                                                <h1 class="headerText">Technical Bulletins &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server"></ServicePLUSWebApp:PersonalMessage>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5"></td>
                                        </tr>
                                        <tr height="9">
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                                <img style="height: 9px" height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                    <img src="images/spacer.gif" width="16" alt="">
                                    <asp:Label ID="errorMessageLabel" runat="server" Width="641px" ForeColor="Red" CssClass="tableData"
                                        EnableViewState="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px; height: 202px">
                                    <table id="Table1" style="width: 696px; height: 399px" cellpadding="0"
                                        width="696" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 14px" valign="top" width="14">
                                                <img style="width: 15px" src="images/spacer.gif" width="15" alt=""></td>
                                            <td style="width: 403px" valign="top" width="403">
                                                <table role="presentation">
                                                    <tr>
                                                        <td>
                                                            <table id="Table2" role="presentation">
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3">
                                                                        Sony Technical Bulletins provide service 
																			information required for maintaining Sony Professional Products.
                                                                    </td>
                                                                </tr>

                                                                <%--Commented for bug 6773--%>

                                                                <%--<tr>
																		<td class="promoCopy" colSpan="3" height="10">&nbsp;
																		</td>
																	</tr>--%>
                                                                <%--<tr>
																		<td class="promoCopy" colSpan="3">Access to Technical Bulletins is available on a 
																		subscription basis in two formats: 
																		</td>
																	</tr>--%>
                                                                <%--<tr>
																		<td class="promoCopy" colSpan="1" height="10">&nbsp;
																		</td>
																		<td class="promoCopy" colSpan="2">
																			<ui>
																				<li>
																					CD-ROM and Internet Access  
																				</li>
																				<li>
																					Internet Access Only 
																				</li>
																			</ui>
																			
																		</td>
																	</tr>--%>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3" height="10">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3">
                                                                        <div id="divContent" runat="server">
                                                                            If you are a current subscriber to technical bulletins please <a class="promoCopyBold" id="id1" href="#" onclick="javascript:window.location.href='SignIn.aspx?Lang='+ '<%=LanguageSM%>' + '&TechBulletin=True');">log in</a> to ServicesPLUS for access.  
																			After you log in to ServicesPLUS, you will be required to enter your technical bulletin subscription ID and 
																			password the first time you access your subscription on this site.  																		         
                                                                        </div>
                                                                        <div id="divAltContent" runat="server">
                                                                            If you have a current subscription for 
																				access to technical bulletins, please enter your subscription identification 
																				and password for access.
																				<br>
                                                                            <table role="presentation">
                                                                                <tr>
                                                                                    <td class="promoCopy" align="right">
                                                                                        <asp:Label ID="lblSubscriptionID" CssClass="promoCopy" runat="server" Width="200">Subscription ID (a.k.a ESI ID):</asp:Label></td>
                                                                                    <td class="promoCopy">
                                                                                        <SPS:SPSTextBox ID="txtSubscriptionID" CssClass="promoCopy" runat="server" Width="100px" MaxLength="20"
                                                                                           ></SPS:SPSTextBox>
                                                                                    </td>
                                                                                    <td class="promoCopy" rowspan="2">
                                                                                        <asp:ImageButton ID="btnSearch" runat="server" ToolTip="Subscription Login" ImageUrl="<%=Resources.Resource.img_btnSubmit()%>"
                                                                                            AlternateText="Submit"></asp:ImageButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="promoCopy" align="right">
                                                                                        <asp:Label ID="lblPassword" CssClass="promoCopy" runat="server" Width="200">Password:</asp:Label></td>
                                                                                    <td class="promoCopy">
                                                                                        <SPS:SPSTextBox ID="txtPassword" CssClass="promoCopy" runat="server" Width="100px" MaxLength="10"
                                                                                            TextMode="Password"></SPS:SPSTextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="promoCopy" colspan="3">
                                                                                        <asp:Label ID="lblSpace" runat="server" Height="10">&nbsp;</asp:Label></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3" height="10">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3">
                                                                        If you have trouble accessing your technical subscription,
																		 contact the technical subscription number on the <a class="promoCopyBold" href="sony-service-contacts.aspx">contact us</a> page.
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3" height="10">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="promoCopy" colspan="3">
                                                                        If you are not a subscriber and would like to subscribe 
																		to Sony Service Technical Bulletins for Sony Professional Products, please submit a <a class="promoCopyBold" id="refSubscriptionform" runat="server">subscription order form</a>.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top">
                                                <%--<uc1:SPSPromotion id="Promotions1" DisplayPage="Sony-Technical-Bulletins-GenInfo" Type="1"  runat="server"/>--%>
                                                <%--<IMG height="280" src="images/sp_ESI_CD_on_bulletin.gif" width="275" alt="Technical Bulletins">--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
    </center>
    </form>
</body>
</html>
