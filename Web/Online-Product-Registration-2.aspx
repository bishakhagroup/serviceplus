<%@ Page Language="VB" AutoEventWireup="false" SmartNavigation="true" CodeFile="Online-Product-Registration-2.aspx.vb"
    Inherits="ServicePLUSWebApp.Online_Product_Registration_2" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="~/SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="~/TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="~/ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head id="Head1" runat="server">
    <title>Sony Online Product Registration</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript">
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hideStuff(id) {
            document.getElementById(id).style.display = 'none';
        }

        function ModelPopup() {
            var ModelNo = $("#ddlModel :selected").val();
            var NewModelNo = $("#hdnModelNumber").val();
            if (ModelNo === NewModelNo) {
                return true;
            }
            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");

            jQuery.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{ 'ModelNo': '" + ModelNo + "' }",
                dataType: 'json',
                url: 'Online-Product-Registration-2.aspx/CheckConflicts',
                success: function (result) {

                    if (jQuery.trim(result.d).length > 0) {

                        $("#divMessage").dialog({ // dialog box
                            title: '<span class="modalpopup-title">Model Renamed</span>',
                            height: 220,
                            width: 560,
                            modal: true,
                            position: 'top',
                            close: function () {
                                redirectTo();
                            }
                        });
                        var NewModelNo = result.d;
                        $("#hdnModelNumber").val(NewModelNo);
                        var content = "<strong>The model " + ModelNo + " has been renamed to " + NewModelNo + " so " + NewModelNo + "<br/>"
                        content += "has been selected in the Model drop down menu.</strong>";
                        $("#divMessage").append(content); //message to display in divmessage div
                        $("#divMessage").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
                        return false;
                    }

                } //End Function(result)
            });
            return true;
        }

        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }

        function redirectTo() {
            var NewModelNo = $("#hdnModelNumber").val();
            $("#ddlModel option[value='" + NewModelNo + "']").attr("selected", "selected");
            $("#ddlModel").change();
            return true;
        }

        function RefreshCaptcha() {
            $("#imgCaptchaShow").attr("src", "ImageHandler.ashx?w=300&h=75&" + (new Date()).getTime());
        }

    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="hideStuff('progress')">
    <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
    <form id="frmOnlineProductRegistration" method="post" runat="server">
        <asp:HiddenField ID="hdnModelNumber" runat="server" />
        <center>
            <table width="710" border="0" align="center" role="presentation">
                <tr>
                    <td width="710" bgcolor="#ffffff" style="height: 714px">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    <asp:Label ID="Label1" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 708px" valign="middle">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/pghdr_prodreg.jpg"
                                                bgcolor="#363d45" height="82">
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 464px" bgcolor="#f2f5f8">
                                                <img src="images/spacer.gif" width="16" alt="">&nbsp;
                                            </td>
                                            <td style="width: 246px" bgcolor="#99a8b5">
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8" valign="bottom" background="images/sp_int_header_btm_left_onepix.gif">
                                                <%--<img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">--%>
                                            </td>
                                            <td bgcolor="" valign="bottom" background="images/sp_int_header_btm_right.gif">
                                                <%-- <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%-- <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>--%>
                                    <img src="images/spacer.gif" width="20" alt="">
                                    <asp:Label ID="errorMessageLabel" runat="server" ForeColor="Red" CssClass="tableData"
                                        EnableViewState="False"></asp:Label>
                                    <%-- </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="imgSubmit" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="1" height="18">
                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                            </td>
                                            <td height="18">
                                                <span class="tableHeader">To register your product, please fill out the 
                                            information below. Fields marked with a </span><span class="redAsterick">*</span><span class="tableHeader">
                                                    are required.</span>
                                            </td>
                                        </tr>
                                        <%-- <tr>
												<td width="17" height="15"><img height="15" src="images/spacer.gif" width="20" alt=""></td>
						                        <td width="1" height="18"><img height="15" src="images/spacer.gif" width="3" alt=""></td>
						                        <td height="18"><span class="tableHeader">If you are signed in to ServicesPLUS, please review the information below and correct as necessary.</td>
					                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678" height="15">
                                                <img height="15" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="15">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17">
                                                <img height="15" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678">
                                                <table width="670" border="0" role="presentation">
                                                    <tr bgcolor="#ffffff">
                                                        <td width="3">
                                                            <img height="1" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td width="127">
                                                            <img height="1" src="images/spacer.gif" width="125" alt="">
                                                        </td>
                                                        <td width="542">
                                                            <img height="1" src="images/spacer.gif" width="542" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td width="3" height="18">
                                                            <img height="15" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" height="18">
                                                            <span class="tableHeader">Product Information</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label6" runat="server" CssClass="bodyCopy">Model Prefix:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <asp:DropDownList ID="ddlModelPrefix" CssClass="bodycopy" runat="server" AutoPostBack="True"
                                                            Width="70px">
                                                        </asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr valign="top" bgcolor="#f2f5f8">
                                                        <td>
                                                            <img src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td align="left" width="150">
                                                        </td>
                                                        <td class="bodyCopy">
                                                            &nbsp;&nbsp;&nbsp;Click to select or type first letter
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label2" runat="server" CssClass="bodyCopy">Model:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                            <ContentTemplate>--%>
                                                                &nbsp;&nbsp;<asp:DropDownList ID="ddlModel" CssClass="bodycopy" runat="server" AutoPostBack="True"
                                                                    Width="300px">
                                                                </asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                            <%-- </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlModelPrefix" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label4" runat="server" CssClass="bodyCopy">Purchased From:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <asp:DropDownList OnSelectedIndexChanged="ddlPurchasedFrom_SelectedIndexChanged"
                                                            ID="ddlPurchasedFrom" CssClass="bodyCopy" AutoPostBack="true" runat="server"
                                                            Width="200px">
                                                            <asp:ListItem Value="0" Selected="True">Click to select</asp:ListItem>
                                                            <asp:ListItem Value="1">Direct from Sony</asp:ListItem>
                                                            <asp:ListItem Value="2">Reseller</asp:ListItem>
                                                            <asp:ListItem Value="3">Other</asp:ListItem>
                                                        </asp:DropDownList>
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            <%-- <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                            <ContentTemplate>--%>
                                                            <table width="100%" border="0" role="presentation">
                                                                <tr bgcolor="#f2f5f8" runat="server" id="trResellerName" visible="false">
                                                                    <td class="bodyCopy" align="right" width="128">
                                                                        <asp:Label ID="lblResellerName" runat="server" CssClass="bodyCopy">Reseller Name:</asp:Label>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                            <asp:DropDownList ID="ddlResellerName" CssClass="bodycopy" runat="server" Width="300px">
                                                                            </asp:DropDownList>
                                                                        <span class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#f2f5f8">
                                                                    <%--<td colspan="3">
                                                                            <img height="2" src="images/spacer.gif" width="3" alt="">
                                                                        </td>--%>
                                                                </tr>
                                                                <tr bgcolor="#f2f5f8" runat="server" id="trResellerCity" visible="false">
                                                                    <td class="bodyCopy" align="right" width="128">
                                                                        <asp:Label ID="lblResellerCity" runat="server" CssClass="bodyCopy">Reseller City:</asp:Label>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                            <SPS:SPSTextBox ID="txtResellerCity" runat="server" Width="300px" CssClass="bodyCopy"
                                                                                MaxLength="50"></SPS:SPSTextBox>
                                                                        <span class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#f2f5f8" runat="server" id="trResellerState" visible="false">
                                                                    <td class="bodyCopy" align="right" width="128">
                                                                        <asp:Label ID="lblResellerState" runat="server" CssClass="bodyCopy">Reseller State:</asp:Label>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                            <asp:DropDownList ID="ddlResellerState" CssClass="bodycopy" runat="server" Width="150px">
                                                                            </asp:DropDownList>
                                                                        <span class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr bgcolor="#f2f5f8" runat="server" id="trResellerOther" visible="false">
                                                                    <td class="bodyCopy" align="right" width="128">
                                                                        <asp:Label ID="Label8" runat="server" CssClass="bodyCopy">Other:</asp:Label>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                            <SPS:SPSTextBox ID="txtOtherPurchasedFrom" runat="server" Width="300px" CssClass="bodyCopy"
                                                                                MaxLength="50"></SPS:SPSTextBox>
                                                                        <span class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%-- </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlPurchasedFrom" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label5" runat="server" CssClass="bodyCopy">Date of Purchase:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtDate" runat="server" CssClass="bodycopy"></SPS:SPSTextBox>
                                                            <span class="bodycopy">&nbsp;&nbsp;MM/DD/YYYY</span><span class="redAsterick">*</span>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150">
                                                            <asp:Label ID="Label3" runat="server" CssClass="bodyCopy">Serial Number:</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtSerialNumber" runat="server" Width="45%" CssClass="bodyCopy"
                                                            MaxLength="50"></SPS:SPSTextBox>
                                                            <span class="redAsterick">*</span>
                                                            <br />
                                                            <span style="padding-left: 10px;" class="bodyCopy"><a href="#" onclick="window.open('OnlineProductRegistrationSerialInfo.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=no,width=400,height=450'); return false;">Click here </a>to help find your serial number.</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="3" colspan="3" height="20">
                                                            <img height="1" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" style="padding-left: 20px;" class="bodyCopy" width="100%" align="left">
                                                            <asp:Label ID="lblProductCount" runat="server" CssClass="bodyCopy">If you purchased more than one of the same model, you can register additional serial numbers for this model.  Did you purchase more than one?</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td align="left" valign="middle" colspan="2">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:DropDownList ID="ddlProductCount" CssClass="bodycopy" runat="server" Width="54%"
                                                            AutoPostBack="true">
                                                            <asp:ListItem Text="Click to Select" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="No, I only purchased one." Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Yes, I purchased more than one." Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" width="100%">
                                                            <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                            <ContentTemplate>--%>
                                                            <table border="0" width="100%" role="presentation">
                                                                <tr runat="server" id="trOtherSerial" visible="false">
                                                                    <td colspan="3" height="20" width="100%">
                                                                        <table width="100%" border="0" role="presentation">
                                                                            <tr bgcolor="#f2f5f8">
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label10" runat="server" CssClass="bodyCopy">2<sup>nd</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber2" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label11" runat="server" CssClass="bodyCopy">3<sup>rd</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber3" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr bgcolor="#f2f5f8">
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label12" runat="server" CssClass="bodyCopy">4<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber4" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label13" runat="server" CssClass="bodyCopy">5<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber5" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr bgcolor="#f2f5f8">
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label14" runat="server" CssClass="bodyCopy">6<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber6" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label15" runat="server" CssClass="bodyCopy">7<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber7" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr bgcolor="#f2f5f8">
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label16" runat="server" CssClass="bodyCopy">8<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber8" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label17" runat="server" CssClass="bodyCopy">9<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber9" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr bgcolor="#f2f5f8">
                                                                                <td width="3">
                                                                                    <img height="25" src="images/spacer.gif" width="3" alt="">
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    <asp:Label ID="Label18" runat="server" CssClass="bodyCopy">10<sup>th</sup> Serial Number:</asp:Label>
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;&nbsp;
                                                                                        <SPS:SPSTextBox ID="txtSerialNumber10" runat="server" Width="45%" CssClass="bodyCopy"
                                                                                            MaxLength="50"></SPS:SPSTextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="3">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td width="117" align="right">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%-- </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlProductCount" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr valign="bottom" bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td colspan="2" style="padding-left: 20px;" class="bodyCopy" width="400px" align="left">
                                                            <asp:Label ID="lblNextProduct" runat="server" CssClass="bodyCopy"> What general product category would you anticipate purchasing next?</asp:Label>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr valign="top" bgcolor="#f2f5f8">
                                                        <td>
                                                            <img height="25" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                        <td align="left" valign="middle" colspan="2">
                                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:DropDownList ID="ddlNextProduct" CssClass="bodycopy" runat="server" Width="54%"
                                                            AutoPostBack="true">
                                                            <asp:ListItem Value="0" Text="Click to Select" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="Studio Camera" Text="Studio Camera"></asp:ListItem>
                                                            <asp:ListItem Value="ENG/Portable Camera" Text="ENG/Portable Camera"></asp:ListItem>
                                                            <asp:ListItem Value="Studio Deck" Text="Studio Deck"></asp:ListItem>
                                                            <asp:ListItem Value="Portable Deck" Text="Portable Deck"></asp:ListItem>
                                                            <asp:ListItem Value="Video Monitor" Text="Video Monitor"></asp:ListItem>
                                                            <asp:ListItem Value="Server" Text="Server"></asp:ListItem>
                                                            <asp:ListItem Value="Switcher" Text="Switcher"></asp:ListItem>
                                                            <asp:ListItem Value="Professional Photo Printer" Text="Professional Photo Printer"></asp:ListItem>
                                                            <asp:ListItem Value="Projector" Text="Projector"></asp:ListItem>
                                                            <asp:ListItem Value="Laptop Computer" Text="Laptop Computer"></asp:ListItem>
                                                            <asp:ListItem Value="IT Display" Text="IT Display"></asp:ListItem>
                                                            <asp:ListItem Value="Service Contract" Text="Service Contract"></asp:ListItem>
                                                            <asp:ListItem Value="Other" Text="Other"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" width="100%">
                                                            <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                            <ContentTemplate>--%>
                                                            <table width="100%" runat="server" id="tlbOtherPurchase"
                                                                visible="false">
                                                                <tr bgcolor="#f2f5f8" valign="bottom">
                                                                    <td class="bodyCopy" align="right" width="130" valign="bottom">
                                                                        <asp:Label ID="Label9" runat="server" CssClass="bodyCopy">Other, please specify:</asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                            <SPS:SPSTextBox ID="txtOtherPurchaseCat" runat="server" Width="25%" CssClass="bodyCopy"
                                                                                MaxLength="50"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <%-- </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ddlNextProduct" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="bottom">
                                                            <img height="25" src="images/spacer.gif" width="4" alt="">
                                                        </td>
                                                        <td class="bodyCopy" align="right" width="150" valign="bottom">
                                                            <asp:Label ID="lblPromotionalCode" runat="server" CssClass="bodyCopy">Promotional Code (if any):</asp:Label>

                                                        </td>
                                                        <td valign="bottom" align="left">
                                                            &nbsp;
                                                        <SPS:SPSTextBox ID="txtPromotionalCode" runat="server" MaxLength="50" Width="25%" CssClass="bodycopy"></SPS:SPSTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="20">
                                                            <img height="1" src="images/spacer.gif" width="3" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td colspan="3" height="20">
                                                            <%-- <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel6" runat="server" >
                                                                    <ProgressTemplate>--%>
                                                            <img height="1" src="images/spacer.gif" width="25" alt="">
                                                            <span id="progress" class="tableHeader">Please wait...<br />
                                                                <img src="images/progbar.gif" alt="Progres" />
                                                                <%--  </ProgressTemplate>
                                                                </asp:UpdateProgress>   --%>
                                                                <%--<asp:UpdatePanel ID="UpdatePanel6" runat="server">--%>
                                                                <%--  <ContentTemplate>--%>
                                                                <input type="hidden" runat="server" id="hdnNavigator" value="0" />
                                                                <%--</ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="imgSubmit" EventName="click" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>--%>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="2" alt="" />
                                            </td>
                                            <td width="670" height="30" align="center" valign="middle" style="padding-right: 100px;">
                                                <img src="ImageHandler.ashx?w=300&h=75" id="imgCaptchaShow" alt="Captcha Image" /><br />
                                                <a href="#" onclick="javascript:RefreshCaptcha();">Refresh</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="2" alt="" />
                                            </td>
                                            <td align="center" valign="middle" style="padding-right: 275px;">
                                                <SPS:SPSTextBox ID="txtCaptchaValidation" runat="server" MaxLength="50" Width="30%" CssClass="bodyCopy"></SPS:SPSTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="2" alt="" />
                                            </td>
                                            <td class="bodyCopy" align="center" valign="middle" style="padding-right: 225px;">
                                                Please enter the code in the Image.
                                            </td>
                                        </tr>
                                        <tr bgcolor="#f2f5f8">
                                            <td width="3" height="18">
                                                <img height="15" src="images/spacer.gif" width="3" alt="">
                                            </td>
                                            <td width="670" height="30" align="center" valign="middle">
                                                <asp:ImageButton ID="imgSubmit" ImageUrl="<%$Resources:Resource,img_btnSubmit%>" runat="server"
                                                    AlternateText="Submit Registration" OnClientClick="showStuff('progress')" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="3" colspan="2" height="18">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </center>

    </form>
</body>
</html>
