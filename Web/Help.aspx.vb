Imports System.Globalization

Namespace ServicePLUSWebApp

    Partial Class Help
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ' ASleight - This is handled in SSL.aspx, which all pages inherit from.
            'If (HttpContextManager.SelectedLang IsNot Nothing) Then
            '	Dim ci As New CultureInfo(HttpContextManager.SelectedLang)
            '	Page.Culture = ci.ToString()
            '	Page.UICulture = ci.ToString()
            'Else
            '	HttpContextManager.SelectedLang = "en-US"
            '	Page.Culture = "en-US"
            '	Page.UICulture = "en-US"
            'End If

            'Put user code to initialize the page here
            If HttpContextManager.Customer IsNot Nothing Then
                RaRequestLabel.Text = "<span class=""bodyCopy"">" + GetGlobalResourceObject("Resource", "help_cnt_hdrp_msg_9") + "<a href=""RARequest.aspx"">" + GetGlobalResourceObject("Resource", "help_cnt_hdrp_msg_10") + "</a>.</span>"
            Else
                RaRequestLabel.Text = "<span class=""bodyCopy"">" + Resources.Resource.help_cnt_hdrp_msg_11 + "<a href=""default.aspx"">" + GetGlobalResourceObject("Resource", "help_cnt_hdrp_msg_12") + " </a>)</span>"
            End If
            'GetGlobalResourceObject("Resource", "help_cnt_hdrp_msg_11")
		End Sub

    End Class

End Namespace
