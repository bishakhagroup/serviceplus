Imports Sony.US.ServicesPLUS.Core

Namespace ServicePLUSWebApp

    Partial Class sony_service_sn
        Inherits SSL

        Public customer As Customer
        Public sModelName As String = ""
        Public sWarranty As Char = "M"
        Public sURL As String

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            ''If Not PreviousPage Is Nothing Then
            ''    sModelName = PreviousPage.ModelName
            ''Else
            ''    Exit Sub
            ''End If

            'Dim displayValues As New StringBuilder()
            'Dim postedValues As NameValueCollection = Request.Form
            'Dim nextKey As String
            'For i As Integer = 0 To postedValues.AllKeys.Length - 1
            '    nextKey = postedValues.AllKeys(i)
            '    If nextKey.Substring(0, 2) <> "__" Then
            '        displayValues.Append("<br>")
            '        displayValues.Append(nextKey)
            '        displayValues.Append(" = ")
            '        displayValues.Append(postedValues(i))
            '    End If
            'Next
            If Not Session.Item("ServiceModelNumber") Is Nothing Then
                sModelName = Session.Item("ServiceModelNumber").ToString()
            Else
                Response.Redirect("sony-repair.aspx", True)
                Response.End()
            End If

            'If Not Request.QueryString("model") Is Nothing Then
            '    sModelName = Request.QueryString("model").Trim()
            'End If
            'imgBtnNext.Attributes.Add("onclick", "javascript:checkServiceContract();")
            lblSubHdr.InnerText = $"{sModelName} {Resources.Resource.repair_svcacc_dept_msg}"
            If Not Session("snServiceInfo") Is Nothing Then
                loadInitialData()
            End If
            If Not Session("RequestPath") Is Nothing Then
                If Session("RequestPath").ToString() = "engine" Then
                    sURL = "sony-service-repair-maintenance.aspx"
                Else
                    sURL = "sony-repair.aspx"
                End If
            Else
                sURL = "sony-repair.aspx"
            End If
        End Sub

        Private Sub loadInitialData()
            Dim oServiceInfo As ServiceItemInfo
            oServiceInfo = Session("snServiceInfo")

            txtAccessories.Text = oServiceInfo.Accessories
            txtSerialNumber.Text = oServiceInfo.SerialNumber

            If UCase(oServiceInfo.WarrantyType) = "C" Then
                txtContractNo.Text = oServiceInfo.ContractNumber
            Else
                txtContractNo.Text = ""
            End If

            sWarranty = UCase(oServiceInfo.WarrantyType)

        End Sub
#Region "To check the customer logged status"

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'Grab customer from session if available
            If Not Session.Item("customer") Is Nothing Then
                customer = Session.Item("customer")
            Else
                Response.Redirect("sony-repair.aspx", True)
                Response.End()
            End If
        End Sub
#End Region

        Protected Sub imgBtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnNext.Click

            Dim oServiceItem As New ServiceItemInfo
            Dim isError As Boolean = False
            Dim isValid As Boolean = True
            ' validate
            If Trim(txtSerialNumber.Text) = "" Then
                'lblSerialNo.CssClass = "redAsterick"
                isError = True
            Else
                'promoCopyBold
                'lblSerialNo.CssClass = "promoCopyBold"
                isError = False
            End If

            ' dropped this validation, since there is no need to validate contract no.
            'If Request.Form("group1").ToString() = "c" And Trim(txtContractNo.Text) = "" Then
            '    lblServiceErrMsg.Visible = True
            '    isError = True
            'Else
            '    lblServiceErrMsg.Visible = False
            'End If

            If isError = True Then
                ErrorValidation.Visible = True
                Return
            End If

            oServiceItem.Accessories = Trim(txtAccessories.Text)
            oServiceItem.ContractNumber = Trim(txtContractNo.Text)
            oServiceItem.SerialNumber = Trim(txtSerialNumber.Text)
            oServiceItem.WarrantyType = Request.Form("group1").ToString()

            customer = Session.Item("customer")

            '' temp blocked '''''''''''
            'customer = getCustomer("6170600603004619") '<-- need to comment before create installer
            'customer = getCustomer("6170600603004619") ' comment this line for B2C customer
            'temp
            'Session.Item("customer") = customer '<-- need to comment before create installer, comment this line for B2C customer cehcking
            '' temp block end ''''''''''''''

            Session.Add("snServiceInfo", oServiceItem)
            Try

                '' ******* Code Spec. to determine they account type and redirects accordingly ******'
                'If getUsersAccountType(customer.SIAMIdentity) = enumAccountType.AccountHolder Then
                '    For Each a As Account In customer.SAPBillToAccounts
                '        If Not a.Validated Then
                '            isValid = False
                '            Exit For
                '        End If
                '    Next
                'If (customer.StatusCode = 1 And customer.UserType = "AccountHolder" And customer.StatusID = 1) Then'6668
                If (customer.StatusCode = 1 And (customer.UserType = "A")) Then '6668
                    Response.Redirect("Service-AccntHolder.aspx") '?model=" + sModelName)   ' redirect to Service-AccntHolder.aspx
                    'Response.Redirect("Service-AccntHolder.aspx?model=" + sModelName)   ' redirect to Service-AccntHolder.aspx
                Else
                    Response.Redirect("service-naccntholder.aspx") '?model=" + sModelName)   ' redirect to Service-NAccntHolder.aspx
                    'Response.Redirect("service-naccntholder.aspx?model=" + sModelName)   ' redirect to Service-NAccntHolder.aspx
                End If
                'Else
                'Response.Redirect("service-naccntholder.aspx") '?model=" + sModelName)   ' redirect to Service-NAccntHolder.aspx
                ''Response.Redirect("service-naccntholder.aspx?model=" + sModelName)   ' redirect to Service-NAccntHolder.aspx
                'End If
                ' ******* End of Code Spec. to determine they account type and redirects accordingly ******'
            Catch ex As Exception

            End Try


        End Sub

        'Protected Sub cvServiceContract_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cvServiceContract.ServerValidate
        '    Dim sContract As String
        '    sContract = txtContractNo.Text
        '    If sContract.Trim() = "" Then
        '        cvServiceContract.Visible = True
        '        cvServiceContract.Text = "Please enter your number"
        '    Else
        '        cvServiceContract.Visible = True
        '        cvServiceContract.Text = "Welcome"

        '    End If

        'End Sub

    End Class
End Namespace
