<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.New_Register"
    EnableViewStateMac="false" CodeFile="Registration.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_Register%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link type="text/css" href="includes/ServicesPLUS_style.css" rel="stylesheet">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>

    <script type="text/javascript">
        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }
        function hideStuff(id) {
            document.getElementById(id).style.display = 'none';
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;" onload="hideStuff('progress')">
    <form id="form1" autocomplete="off" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="<%=Resources.Resource.sna_svc_img_49 %>" border="0" alt="ServicesPlus Registration">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="17" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="678" height="20">
                                                <img height="20" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td width="678">
                                                <table width="670" border="0" role="presentation">
                                                    <tr bgcolor="#d5dee9">
                                                        <td height="18">
                                                            <span class="tableHeader">
                                                                <span id="spnData" runat="server"><%=Resources.Resource.el_rgn_SubHdl1%>  </span>
                                                                <span id="spnData_CA" runat="server"><%=Resources.Resource.el_rgn_SubHdl1_CAN%>  </span>
                                                                <br />
                                                                <span id="spnData1" runat="server"><%=Resources.Resource.el_rgn_SubHdl2%> </span>
                                                                <span id="spnData1_CA" runat="server"><%=Resources.Resource.el_rgn_SubHdl2_CAN%> </span>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" height="15">
                                                            <br />
                                                            <a class="bodyCopy" href="default.aspx"><%=Resources.Resource.el_HomePge%></a>
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#d5dee9">
                                                        <td height="18">
                                                            <span class="tableHeader"><%=Resources.Resource.el_rgn_SubHdl3%></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td class="bodyCopy">
                                                <%=Resources.Resource.el_rgn_SubHdl4%>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr id="ErrLbl">
                                            <td>&nbsp;</td>
                                            <td class="bodyCopy" colspan="5">
                                                <div style="margin-top: 3px;">
                                                    <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick" />
                                                </div>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <fieldset>
                                                    <table width="660" border="0" role="form">
                                                    <colgroup>
                                                        <col class="bodyCopy" style="width: 127px; text-align: right;" />
                                                        <col class="bodyCopy" />
                                                        <col class="bodyCopy" style="width: 80px;" />
                                                        <col class="bodyCopy" style="width: 147px; text-align: right;" />
                                                        <col class="bodyCopy" />
                                                    </colgroup>
                                                    <%--<tr >  <asp:label id="ErrorLabel" runat="server" CssClass="redAsterick"></asp:label> </tr>--%>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <label for="txtEmail" id="lblEmail" runat="server"><%=Resources.Resource.el_rgn_Email%></label>
                                                        </td>
                                                        <td>
                                                            &nbsp;<SPS:SPSTextBox ID="txtEmail" runat="server" CssClass="bodyCopy" MaxLength="70" aria-required="true" />
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <label for="txtEmail2" id="lblEmail2" runat="server"><%=Resources.Resource.el_rgn_ConfirmEmail %></label>
                                                        </td>
                                                        <td>
                                                            &nbsp;<SPS:SPSTextBox ID="txtEmail2" runat="server" CssClass="bodyCopy" MaxLength="70" aria-required="true" />
                                                            <span class="redAsterick"> *</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td>
                                                            <label for="txtFirstName" id="lblFirstName" runat="server"><%=Resources.Resource.el_rgn_FirstName %></label>
                                                        </td>
                                                        <td>
                                                            &nbsp;<SPS:SPSTextBox ID="txtFirstName" runat="server" CssClass="bodyCopy" MaxLength="50" aria-required="true" />
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                        <td colspan="3">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr bgcolor="#f2f5f8">
                                                        <td class="bodyCopy" align="right">
                                                            <label for="txtLastName" id="lblLastName" runat="server"><%=Resources.Resource.el_rgn_LastName %></label>
                                                        </td>
                                                        <td>
                                                            &nbsp;<SPS:SPSTextBox ID="txtLastName" runat="server" CssClass="bodyCopy" MaxLength="50" aria-required="true" />
                                                            <span class="redAsterick">*</span>
                                                        </td>
                                                        <td class="bodyCopy" colspan="3">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="5">
                                                            <asp:ImageButton ID="btnNext" OnClientClick="showStuff('progress')" ImageUrl="<%$Resources:Resource,img_btnNext%>"
                                                                AlternateText="Go to the next page to continue your registration." role="button" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            <span id="progress" class="tableHeader" style="display: none;">Please wait...<br />
                                                                <img src="images/progbar.gif" alt="Progress Bar" /></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">&nbsp;</td>
                                                    </tr>
                                                </table>
                                                </fieldset>
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
