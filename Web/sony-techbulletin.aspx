<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.TechBulletinSearch" CodeFile="sony-techbulletin.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Technical Bulletins for Professional Products</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software, Sony Technical Bulletins"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="frmTechBulletinSearch" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td valign="top" align="right" width="464" background="images/sp_int_subhd_technical_bulletins.jpg"
                                                bgcolor="#363d45" height="82">
                                                <br>
                                                <h1 class="headerText">Technical Bulletins &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="466" bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td>
                                                            <h2 class="headerTitle">Search Technical Bulletins</h2><br/>
                                                            <table width="440" border="0" role="presentation">
                                                                <tr>
                                                                    <td colspan="3" height="5">
                                                                        <img height="5" src="images/spacer.gif" width="20" alt=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="138">
                                                                        <asp:Label ID="lblProductLine" CssClass="tableData" runat="server">Product Line:</asp:Label></td>
                                                                    <td>
                                                                        <asp:Label ID="lblModelNo" CssClass="tableData" runat="server">Model Number:</asp:Label></td>
                                                                    <td>
                                                                        <asp:Label ID="lblSerialNo" CssClass="tableData" runat="server">Serial Number:</asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="138" height="18">
                                                                        <asp:DropDownList ID="ddlProductLine" CssClass="tableData" runat="server" Width="128px" AutoPostBack="True"
                                                                            ForeColor="Black">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td height="18">
                                                                        <SPS:SPSTextBox ID="txtModelNo" CssClass="tableData" runat="server" MaxLength="100"></SPS:SPSTextBox></td>
                                                                    <td height="22">
                                                                        <SPS:SPSTextBox ID="txtSerialNo" CssClass="tableData" runat="server" MaxLength="12"></SPS:SPSTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" height="5">
                                                                        <img height="5" src="images/spacer.gif" width="20" alt=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="138">
                                                                        <asp:Label ID="lblBulletinNo" CssClass="tableData" runat="server">Bulletin Number:</asp:Label></td>
                                                                    <td colspan="2">
                                                                        <asp:Label ID="lblSubject" CssClass="tableData" runat="server">Subject:</asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="138">
                                                                        <SPS:SPSTextBox ID="txtBulletinNo" CssClass="tableData" runat="server" MaxLength="10"></SPS:SPSTextBox></td>
                                                                    <td colspan="2">
                                                                        <SPS:SPSTextBox ID="txtSubject" CssClass="tableData" runat="server" Width="282px" MaxLength="300"></SPS:SPSTextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="3" height="5">
                                                                        <img height="5" src="images/spacer.gif" width="20" alt=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblDateFrom" CssClass="tableData" runat="server">Date From:</asp:Label></td>
                                                                    <td>
                                                                        <asp:Label ID="lblDateTo" CssClass="tableData" runat="server">Date To:</asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtDateFrom" CssClass="tableData" runat="server" MaxLength="10"></SPS:SPSTextBox><a onclick="showCalendarControl(txtDateFrom)" href="#"><img alt="Click to select a date" src="images/cal.gif" border="0"></a>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtDateTo" CssClass="tableData" runat="server" MaxLength="10"></SPS:SPSTextBox><a onclick="showCalendarControl(txtDateTo)" href="#"><img alt="Click to select a date" src="images/cal.gif" border="0"></a>
                                                                    </td>
                                                                    <td align="center">
                                                                        <asp:ImageButton ID="btnLast30days" runat="server" AlternateText="Search Tech bulletins from last 30 days"
                                                                            ImageUrl="images/sp_int_last30_btn.jpg" Height="32" Width="106" ToolTip="Search for Bulletins published in the last 30 days"
                                                                            Visible="False"></asp:ImageButton><asp:Label ID="dtFormat" CssClass="tableData" runat="server">Date Format: dd/mm/yyyy</asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <table width="100%" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <img height="35" src="images/spacer.gif" width="41" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="35" src="images/spacer.gif" width="41" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <img height="35" src="images/spacer.gif" width="41" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="btnSearch" runat="server" AlternateText="Start SoftwarePLUS Search" ImageUrl="images/sp_int_SearchHdr_btn.gif"
                                                                Height="32" Width="125" ToolTip="Search technical bulletin index"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td width="466" bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                    <br>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="errorMessageLabel" runat="server" CssClass="tableData" Width="641px" ForeColor="Red"
                                                    EnableViewState="False"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="5">
                                                <img height="5" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="670" height="5">
                                                <img height="5" src="images/spacer.gif" width="670" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td valign="top" width="670">
                                                <table width="298" border="0" role="navigation">
                                                    <tr>
                                                        <td width="60">
                                                            <asp:ImageButton ID="btnPreviousPage" runat="server" ImageUrl="images/sp_int_leftArrow_btn.gif" Height="9"
                                                                Width="9" ToolTip="Previous Result Page"></asp:ImageButton><asp:LinkButton ID="PreviousLinkButton" runat="server" CssClass="tableData" ToolTip="Previous Result Page">Previous</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td class="tableData" align="center" width="50">
                                                            <asp:Label ID="lblCurrentPage" runat="server">1</asp:Label>&nbsp;<asp:Label ID="lblof1" runat="server">of</asp:Label>&nbsp;
																<asp:Label ID="lblEndingPage" runat="server">1</asp:Label>
                                                        </td>
                                                        <td width="45">
                                                            &nbsp;
																<asp:LinkButton ID="NextLinkButton" runat="server" CssClass="tableData" ToolTip="Next Result Page">Next</asp:LinkButton><asp:ImageButton ID="btnNextPage" runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9"
                                                                    Width="9" ToolTip="Next Result Page"></asp:ImageButton>
                                                        </td>
                                                        <td width="25">
                                                            <img height="4" src="images/spacer.gif" width="25" alt=""></td>
                                                        <td class="tableData" align="right" width="40">
                                                            <asp:Label ID="lblPage1" runat="server">Page:</asp:Label></td>
                                                        <td width="40">
                                                            <SPS:SPSTextBox ID="txtPageNumber" runat="server" CssClass="tableData" Width="40px" MaxLength="7"
                                                                ToolTip="Page Number"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage" runat="server" AlternateText="Go to specified result page" ImageUrl="images/sp_int_go_btn.gif"
                                                                Height="25" Width="40" ToolTip="Goto Page"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="670" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <asp:DataGrid ID="dgTechBulletinResults" runat="server" Width="680" AutoGenerateColumns="False">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="Bulletin Number" ItemStyle-Width="15%" ItemStyle-Height="100%">
                                                                        <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                        <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblTBulletinNo" CssClass="tableData" runat="server" BackColor="White" Width="100%"></asp:Label><br>
                                                                            <asp:Label ID="lblNullspace" runat="server" CssClass="tableData" Width="100%">&nbsp;</asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Subject" ItemStyle-Width="65%" ItemStyle-Height="100%">
                                                                        <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                        <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="lnkBulletin" CssClass="tableData" BackColor="white" runat="server" Width="100%"
                                                                                Target="_blank">lnkBulletin</asp:HyperLink><br>
                                                                            <asp:Label ID="lblModel" CssClass="tableData" runat="server" Width="12%">
																					<strong>Model(s):</strong>
                                                                            </asp:Label><br>
                                                                            <asp:Label ID="lblModelNullSpace" CssClass="tableData" runat="server" Width="12%">
																					&nbsp;
                                                                            </asp:Label>
                                                                            <asp:Label ID="lblModelList" runat="server" CssClass="tableData" Width="88%"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Date Published" ItemStyle-Width="10%" ItemStyle-Height="100%">
                                                                        <HeaderStyle CssClass="tableHeader" BackColor="#D5DEE9"></HeaderStyle>
                                                                        <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPrint" CssClass="tableData" runat="server" BackColor="White" Width="100%"></asp:Label><br>
                                                                            <asp:Label ID="lblNullspace1" CssClass="tableData" runat="server" Width="100%">&nbsp;</asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn ItemStyle-Width="10%" ItemStyle-Height="100%">
                                                                        <HeaderStyle BackColor="#D5DEE9"></HeaderStyle>
                                                                        <ItemStyle VerticalAlign="Top" BackColor="#F2F5F8"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:HyperLink ID="lnkImgRelease" runat="server" Target="_blank" BorderStyle="None" BackColor="White"
                                                                                Width="100%" ToolTip="View technical bulletin">
                                                                                <img id="imgReleaseNotes" runat="server" src="images/sp_int_releaseNotes_btn.gif" border="0" alt="View technical bulletin">
                                                                            </asp:HyperLink><br>
                                                                            <asp:Label ID="lblNullspace2" runat="server" CssClass="tableData" Width="100%">&nbsp;</asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="298" border="0" role="navigation">
                                                    <tr>
                                                        <td width="60">
                                                            <asp:ImageButton ID="btnPreviousPage2" runat="server" ImageUrl="images/sp_int_leftArrow_btn.gif"
                                                                Height="9" Width="9" ToolTip="Previous Results Page"></asp:ImageButton><asp:LinkButton ID="PreviousLinkButton2" runat="server" CssClass="tableData" ToolTip="Previous Results Page">Previous</asp:LinkButton>&nbsp;
                                                        </td>
                                                        <td class="tableData" align="center" width="50">
                                                            <asp:Label ID="lblCurrentPage2" runat="server">1</asp:Label>&nbsp;<asp:Label ID="lblof2" runat="server">of</asp:Label>&nbsp;
																<asp:Label ID="lblEndingPage2" runat="server">1</asp:Label>
                                                        </td>
                                                        <td width="45">
                                                            &nbsp;
																<asp:LinkButton ID="NextLinkButton2" runat="server" CssClass="tableData" ToolTip="Next Result Page">Next</asp:LinkButton><asp:ImageButton ID="btnNextPage2" runat="server" ImageUrl="images/sp_int_rightArrow_btn.gif" Height="9"
                                                                    Width="9" ToolTip="Next Result Page"></asp:ImageButton>
                                                        </td>
                                                        <td width="25">
                                                            <img height="4" src="images/spacer.gif" width="25" alt=""></td>
                                                        <td class="tableData" align="right" width="40">
                                                            <asp:Label ID="lblPage2" runat="server">Page:</asp:Label></td>
                                                        <td width="40">
                                                            <SPS:SPSTextBox ID="txtPageNumber2" runat="server" CssClass="tableData" Width="40px" MaxLength="7"
                                                                ToolTip="Page Number"></SPS:SPSTextBox>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="btnGoToPage2" runat="server" AlternateText="Goto specified result page" ImageUrl="images/sp_int_go_btn.gif"
                                                                Height="25" Width="40" ToolTip="Goto Page"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="283" border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <a class="tableHeaderLink2" href="#top">
                                                                <img height="28" alt="Back to top of the results page" src="images/sp_int_back2top_btn.gif"
                                                                    width="78" border="0"></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
        <script language="javascript">
            document.getElementById('<%=focusField%>').focus();
        </script>
    </center>
</body>
</html>
