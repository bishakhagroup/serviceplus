Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp
    Partial Class PgModelSearchResult
        Inherits Page

        Const conModelsCatalogDetailPath As String = "sony-pg-service-maintenance-repair.aspx?model="
        Private isOneRow As Boolean = False
        Public LanguageSM As String = "en_US"
        Public divrprmsrShow As Boolean

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            LanguageSM = HttpContextManager.GlobalData.LanguageForIDP
            divrprmsrShow = HttpContextManager.GlobalData.IsAmerica

            If HttpContextManager.Customer IsNot Nothing And Session("isOneRow") IsNot Nothing Then
                If Trim(Session("isOneRow").ToString()) = "1" Then
                    Session.Add("ServiceModelNumber", Request.QueryString("srh").ToUpper().Trim())
                    Response.Redirect("sony-service-sn.aspx") '?model=" + )
                End If
            End If

            If Not IsPostBack Then
                If Request.QueryString("srh") IsNot Nothing Then
                    BindGrid(Request.QueryString("srh").Trim())
                    Return
                End If
            End If
        End Sub

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Dim rec As ModelDetails
            Dim lnkStartRange As LinkButton

            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    rec = e.Row.DataItem
                    lnkStartRange = CType(e.Row.Cells(1).FindControl("lnkModel"), LinkButton)
                    lnkStartRange.Style.Add("cursor", "hand")
                    lnkStartRange.Style.Add("text-decoration", "underline")
                    ' ASleight - This will always be false, because .To_DISCONT is never given a value.
                    If (rec.DiscontinuedFlag <> "Y3" And rec.DiscontinuedFlag <> "Y4" And rec.DiscontinuedFlag <> "Y6" And rec.DiscontinuedFlag <> "Y7" And rec.To_DISCONT = False) Then
                        If Session.Item("customer") Is Nothing Then
                            lnkStartRange.PostBackUrl = "javascript:ShowLogin('" + parseURL(rec.ModelName) + "');"
                        Else
                            lnkStartRange.PostBackUrl = "sony-service-sn.aspx" '?model=" + parseURL(rec.ModelName.ToString())
                        End If
                    Else
                        If rec.ModelName IsNot Nothing Then
                            lnkStartRange.PostBackUrl = "sony-pg-service-maintenance-repair.aspx?model=" + parseURL(rec.ModelName)
                        End If
                    End If
                ElseIf (e.Row.RowType = DataControlRowType.Header) Then
                    e.Row.Cells(1).Text = Resources.Resource.repair_mdno_msg 'GetGlobalResourceObject("Resource", "repair_mdno_msg")
                    e.Row.Cells(2).Text = Resources.Resource.repair_desc_msg 'GetGlobalResourceObject("Resource", "repair_desc_msg")
                End If
            Catch ex As Exception
                Response.Write(ex.Message())
            End Try
        End Sub

        Protected Sub BindGrid(ByVal pSrhString As String)
            Dim modelsGroups As ModelDetails()
            Dim catMan As New CatalogManager

            Try
                modelsGroups = catMan.GetSrhModelDetails(pSrhString, HttpContextManager.GlobalData)
                Session("isOneRow") = "0"

                ' check for no. of rows as result from search
                If modelsGroups.Length = 0 Then
                    Return
                ElseIf modelsGroups.Length = 1 Then
                    If Session.Item("customer") Is Nothing Then
                        isOneRow = True
                        Session("isOneRow") = "1"
                    End If
                End If

                Me.GridPartsGroup.DataSource = modelsGroups
                Me.GridPartsGroup.DataBind()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End Sub

        Private Function parseURL(ByVal psURL As String) As String
            Dim sURL As String = psURL
            Dim token As String = "//"
            Dim index As Integer = sURL.IndexOf(token)

            If index <> -1 Then
                sURL = Left(sURL, index + 1) + "+" + sURL.Substring(index + 2).ToString()
            End If

            token = "\"
            index = sURL.IndexOf(token)
            If index <> -1 Then
                sURL = Left(sURL, index) + "!" + sURL.Substring(index + 1).ToString()
            End If

            Return sURL
        End Function
    End Class
End Namespace
