﻿Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports ServicesPlusException

Namespace ServicePLUSWebApp
    Partial Class Online_Product_Registration
        Inherits SSL

        Private thisCustomer As Customer

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    thisCustomer = HttpContextManager.Customer
                    If IsPostBack Then
                        hdnNavigator.Value = "0"
                    Else
                        LoadIndustry()
                        LoadTitle()
                        PopulateCustomerInformation(thisCustomer)
                    End If
                Else
                    'Response.Redirect("SignIn.aspx?post=prr", True)
                    Response.Redirect("SignIn-Register.aspx?post=prr", True)
                    Return
                End If
            Catch ex As Exception
                errorMessageLabel.InnerHtml = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            imgNext.Attributes.Add("onclick", "javascript:document.getElementById('errorMessageLabel').innerHTML=''; return validate(this);")
        End Sub

        Private Sub PopulateCustomerInformation(ByVal thisCustomer As Customer)
            If Not thisCustomer Is Nothing Then
                FNAM.Value = thisCustomer.FirstName
                LNAM.Value = thisCustomer.LastName
                COMP.Value = thisCustomer.CompanyName
                ADD1.Value = thisCustomer.Address.Address1
                ADD2.Value = thisCustomer.Address.Address2
                ADD3.Value = thisCustomer.Address.Address3 '6872
                CITY0.Value = thisCustomer.Address.City
                STAT.Items.FindByValue(thisCustomer.Address.State).Selected = True
                ZIPC.Value = thisCustomer.Address.PostalCode
                PHAC.Value = thisCustomer.PhoneNumber.Replace("-", "").Replace(" ", "").Substring(0, 3)
                PHPR.Value = thisCustomer.PhoneNumber.Replace("-", "").Replace(" ", "").Substring(3, 3)
                PHNM.Value = thisCustomer.PhoneNumber.Replace("-", "").Replace(" ", "").Substring(6, 4)

                If thisCustomer.FaxNumber.Trim() <> String.Empty Then
                    FXAC.Value = thisCustomer.FaxNumber.Replace("-", "").Substring(0, 3)
                    FXPR.Value = thisCustomer.FaxNumber.Replace("-", "").Substring(3, 3)
                    FXNM.Value = thisCustomer.FaxNumber.Replace("-", "").Substring(4, 4)
                End If

                EMAL.Value = thisCustomer.EmailAddress
                CMAL.Value = thisCustomer.EmailAddress

            End If
        End Sub

        Private Sub LoadIndustry()
            Dim regMan As New OnlineProductRegistrationManager()
            Dim oDS = regMan.GetIndustry()
            PUSE.Items.Clear()
            If oDS IsNot Nothing Then
                PUSE.DataTextField = "INDUSTRY"
                PUSE.DataValueField = "INDUSTRYCODE"
                PUSE.DataSource = oDS.Tables(0).DefaultView()
                PUSE.DataBind()
            Else
                Throw New ApplicationException("Exception occurred")
            End If
            PUSE.Items.Insert(0, New ListItem("Click to Select"))
        End Sub

        Private Sub LoadTitle()
            Dim regMan As New OnlineProductRegistrationManager()
            Dim oDS = regMan.GetTitle()
            OCCU.Items.Clear()
            If Not oDS Is Nothing Then
                OCCU.DataTextField = "Title"
                OCCU.DataValueField = "Title"
                OCCU.DataSource = oDS.Tables(0).DefaultView()
                OCCU.DataBind()

            Else
                Throw New ApplicationException("Exception occurred")
            End If
            OCCU.Items.Insert(0, New ListItem("Click to Select"))
        End Sub

        Protected Sub imgNext_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles imgNext.Click
            LabelLine1Star.Text = ""
            LabelLine2Star.Text = ""
            LabelLine3Star.Text = ""
            LabelLine4Star.Text = ""

            If PUSE.SelectedIndex = 0 Then
                errorMessageLabel.InnerText = "Please select industry field."
                Exit Sub
            ElseIf OCCU.SelectedIndex = 0 Then
                errorMessageLabel.InnerText = "Please select title field."
                Exit Sub
            End If

            If String.IsNullOrWhiteSpace(ADD1.Value) Then
                errorMessageLabel.InnerText = "Please enter Street Address"
                LabelLine1Star.Text = "<span class=""redAsterick"">Please enter Street Address</span>"
                ADD1.Focus()
                Exit Sub
            ElseIf ADD1.Value.Length > 35 Then
                LabelLine1Star.Text = "<span class=""redAsterick"">Street Address must be 35 characters or less in length</span>"
                ADD1.Focus()
                Exit Sub
            Else
                LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            If ADD2.Value.Length > 35 Then
                LabelLine2Star.Text = "<span class=""redAsterick"">Address 2nd Line must be 35 characters or less in length</span>"
                ADD2.Focus()
                Exit Sub
            End If

            If ADD3.Value.Length > 35 Then
                LabelLine3Star.Text = "<span class=""redAsterick"">Address 3rd Line must be 35 characters or less in length</span>"
                ADD3.Focus()
                Exit Sub
            End If

            If String.IsNullOrWhiteSpace(CITY0.Value) Then
                LabelLine4Star.Text = "<span class=""redAsterick"">Please enter City</span>"
                CITY0.Focus()
                Exit Sub
            ElseIf CITY0.Value.Length > 35 Then
                errorMessageLabel.InnerText = "Please enter City"
                LabelLine4Star.Text = "<span class=""redAsterick"">City must be 35 characters or less in length.</span>"
                CITY0.Focus()
                Exit Sub
            Else
                LabelLine4Star.Text = "<span class=""redAsterick"">*</span>"
            End If

            errorMessageLabel.InnerText = ""
            'Response.Write(Request.Form("PUSE"))
            'Dim strValueOCCU = Request.Form("OCCU").ToString()
            'strValueOCCU = strValueOCCU.Substring(strValueOCCU.IndexOf(" ") + 1, strValueOCCU.Length - strValueOCCU.IndexOf(" ") - 1)
            'Response.Write(strValueOCCU)
            'objCustomerPR.LINE_OF_WORK = OCCU.SelectedItem.Text ' Request.Form("OCCU").ToString()
            Dim objCustomerPR As New CustomerProductRegistration With {
                .CustomerID = thisCustomer.CustomerID,
                .FIRST_NAME = Request.Form("FNAM"),
                .LAST_NAME = Request.Form("LNAM"),
                .COMPANY_NAME = Request.Form("COMP"),
                .ADDRESS1 = Request.Form("ADD1"),
                .ADDRESS2 = Request.Form("ADD2"),
                .ADDRESS3 = "",     ' Line 3 has been removed from most places in the site.
                .CITY = Request.Form("CITY0"),
                .STATE = Request.Form("STAT"),
                .ZIPCODE = Request.Form("ZIPC"),
                .PR_PHONE = Request.Form("PHAC") + Request.Form("PHPR") + Request.Form("PHNM"),
                .FAX = Request.Form("FXAC") + Request.Form("FXPR") + Request.Form("FXNM"),
                .EMAIL = Request.Form("EMAL"),
                .LINE_OF_WORK = PUSE.SelectedValue + " - " + PUSE.SelectedItem.Text,
                .TITLE = OCCU.SelectedItem.Text,
                .OPTOUT = If(Request.Form("NEMA")?.ToString(), "0")
            }
            Session.Add("customerpr", objCustomerPR)
            Server.Transfer("Online-Product-Registration-2.aspx", True)
        End Sub
    End Class
End Namespace

