<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" Inherits="ServicePLUSWebApp.sony_service_agreement_info" EnableViewStateMac="true" CodeFile="sony-service-agreement-info.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus()%></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form method="post" runat="server" id="form1">
        <table width="400" border="0" role="presentation">
            <tr>
                <td>
                    <table height="65" width="400" border="0" role="presentation">
                        <tr height="38" width="400">
                            <td colspan="2" width="400" background="images/sp_int_popup_login_hdrbkgd_onepix.gif">
                                <h1 class="headerText">&nbsp;&nbsp;&nbsp;&nbsp;<%=Resources.Resource.help_cnt_svcus_msg_1()%></h1>
                            </td>
                        </tr>
                        <tr height="10">
                            <td colspan="2" background="images/sp_int_popup_login_hdrbkgd_lowr.gif">
                                &nbsp;
                            </td>
                        </tr>
                        <tr height="1">
                            <td cellpadding="0">
                                <table height="1" width="401" border="0" role="presentation">
                                    <tr>
                                        <td width="21" colspan="1" height="9" background="images/sp_int_header_btm_right.gif"></td>
                                        <td height="1" width="378" colspann="1" background="images/sp_int_header_btm_left_onepix.gif"></td>
                                        <td height="1" width="1" colspann="1" background="images/sp_int_header_btm_right.gif"></td>
                                    </tr>
                                </table>

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divLogin" runat="server" visible="true">
                        <table width="370" border="0" cellpadding="2" class="redAsterick" role="presentation">
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="tableheader">
                                    <%=Resources.Resource.help_cnt_svcus_msg()%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                <td class="bodycopy" width="100%">
                                    <ul>
                                        <li>
                                            <%=Resources.Resource.help_cnt_svcus_msg_2()%>
                                            <br />
                                            <br />
                                        </li>
                                        <li>
                                            <%=Resources.Resource.help_cnt_svcus_msg_3()%>
                                            <br />
                                            <br />
                                        </li>
                                        <li>
                                            <br />
                                            <%=Resources.Resource.help_cnt_svcus_msg_4()%><br />
                                        </li>
                                        <li>
                                            <%=Resources.Resource.help_cnt_svcus_msg_5()%>
                                            <br />
                                            <br />
                                        </li>
                                        <li>
                                            <%=Resources.Resource.help_cnt_svcus_msg_6%>
                                            <br />
                                            <br />
                                        </li>
                                        <li>
                                            <%=Resources.Resource.help_cnt_svcus_msg_7%>
                                            <br />
                                            <br />
                                        </li>
                                        <li>
                                            <%=Resources.Resource. help_cnt_svcus_msg_8%>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=Resources.Resource.img_btnCloseWindow()%>" onclick="window.close();" alt="Close Window" /></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
