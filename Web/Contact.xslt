<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <Table border="0">
      <xsl:apply-templates select="//CONTACT" />
    </Table>
  </xsl:template>
  <xsl:template match="CONTACT">
    <tr>
      <td>
        <img height="1" src="images/spacer.gif" width="7" alt=""/>
      </td>
      <td>
        <table width="650" border="0">
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <xsl:if test="MAINTENANCE = 1">
            <tr runat="server" id = "maintenanceTR" visible="false">
              <td class="bodyCopy" style="padding-left: 7px;">
                <STRONG>
                  The ServicesPLUS<sup>sm</sup> website you are trying to reach is currently undergoing maintenance.
                </STRONG>
              </td>
            </tr>
            <tr>
              <td class="bodyCopy" style="padding-left: 7px;">
                Below are some alternate resources to assist you during this period; we will respond as quickly as possible.
              </td>
            </tr>


          </xsl:if>
          <tr>
            <td>
              <img height="5" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>

          <!--<tr>
                        <td class="bodyCopy" style="padding-left: 7px;">
                            <STRONG>
                                You must provide an address in USA to make a Credit Card order on ServicesPLUS.<br/>
                                Canadian users may order if they have an approved Sony Buyer Account.
                            </STRONG>
                        </td>
                    </tr>-->
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt="" />
            </td>
          </tr>
          <tr>
            <td>
              <span class="bodyCopy" style="padding-left: 7px;">
                <STRONG>Contact information for broadcast and business services and support:</STRONG>
              </span>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Repair Parts:</STRONG>
            </td>
          </tr>
          <tr>
            <td >
              <table width="650" border="0">
                <tr>
                  <td class="bodyCopy" style="padding-left: 7px;">
                    Customer Service:  ordering, order tracking, returns
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Email: <A href="mailto:ProParts@am.sony.com?subject=Parts%20Customer%20Service:%20%20ordering,%20tracking,%20research,%20and%20returns&amp;Body=Date:%0d%0d%0dName:%0dCompany%20Name:%0dSony%20Account%20Number:%0dShipping%20Address:%0dBilling%20Address:%0dPhone%20number:%0dIs%20this%20is%20a%20parts%20inquiry%20or%20Order?%0dPO%20Number:%0d%0dItem%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20Part%20number%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo%20you%20want%20%20shipping%20method%20Next%20day,2%20day,%20or%20ground%20transportation?%0d%0dOther%20Parts%20requests:%20"
                              title="Click to send an email to ProParts@am.sony.com">ProParts@am.sony.com</A>
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Phone: 800-538-7550, 9:00 a.m. - 7:00 p.m. ET, Monday - Friday.
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 7px;">
                    Parts research:
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Email: <a href="mailto:ProPartsResearch@am.sony.com" title="Click to send an email to ProPartsResearch@am.sony.com">ProPartsResearch@am.sony.com</a>
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Phone: 800-538-7550, 9:00 a.m. - 7:00 p.m. ET, Monday - Friday.
                  </td>
                </tr>
                <!--<tr runat="server" id = "linkTR">
                    <td style="height: 14px">
                      <xsl:if test="MAINTENANCE = 0">
                        <img height="1" src="images/spacer.gif" width="7" alt=""/>
                        <xsl:if test="SESSION = 1">
                            <span class="bodyCopy">Link: <b>Please click on the <a href="#" onclick="window.open('RARequest.aspx','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=yes,resize=no,width=800,height=650'); return false;" class="body">Return Authorization Form</a>.</b></span>
                        </xsl:if>
                        <xsl:if test="SESSION = 0">
                            <span class="bodyCopy">Link: <b>Please click on the Return Authorization Form. (Requires login, to login please <a href="#" onclick="window.location.href='SignIn.aspx?RAForm=True'; return false;">click here</a>)
                              </b>
                            </span>
                        </xsl:if>
                      </xsl:if>
                    </td>
                  </tr>-->
              </table>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <strong>Repair Services:</strong>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Email: <a href="mailto:SEL.BPC.Svc@am.sony.com" title="Click to send an email to SEL.BPC.Svc@am.sony.com">SEL.BPC.Svc@am.sony.com</a>
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Phone: 866-766-9272.
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Software:</STRONG>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Email: <A href="mailto:ProParts@am.sony.com?subject=Parts%20Customer%20Service:%20%20ordering,%20tracking,%20research,%20and%20returns&amp;Body=Date:%0d%0d%0dName:%0dCompany%20Name:%0dSony%20Account%20Number:%0dShipping%20Address:%0dBilling%20Address:%0dPhone%20number:%0dIs%20this%20is%20a%20parts%20inquiry%20or%20Order?%0dPO%20Number:%0d%0dItem%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20Part%20number%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20Quantity%0d1.%0d2.%0d3.%0d4.%0d5.%0d%0dDo%20you%20want%20%20shipping%20method%20Next%20day,2%20day,%20or%20ground%20transportation?%0d%0dOther%20Parts%20requests:%20"
                        title="Click to send an email to ProParts@am.sony.com">ProParts@am.sony.com</A>
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Phone: 800-538-7550, 9:00 a.m. - 7:00 p.m. ET, Monday - Friday.
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Technical Subscriptions:</STRONG>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Email: <a href="mailto:BSSC.TIS@am.sony.com" title="Click to send an email to BSSC.TIS@am.sony.com">BSSC.TIS@am.sony.com</a>
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Phone : 408-352-4500
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Sony Training Institute:</STRONG>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              Research and sign up for classes, purchase training materials
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Email: <a href="mailto:Training@am.sony.com" title="Click to send an email to Training@am.sony.com">Training@am.sony.com</a>
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Phone : 408-352-4500.
            </td>
          </tr>

          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Fax : 408-352-4212.
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>
                SupportNET<sup>sm</sup> Service Agreements and SystemWatch<sup>sm</sup>
              </STRONG>
            </td>
          </tr>
          <tr>
            <td>
              <table width="650" border="0">
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Email:<A href="mailto:SupportNET@am.sony.com" title="Click to send an email to SupportNet@am.sony.com">SupportNET@am.sony.com</A>
                  </td>
                </tr>
                <tr>
                  <td class="bodyCopy" style="padding-left: 40px;">
                    Phone: 877-398-7669
                  </td>
                </tr>
              </table>

            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Registration for Professional Products:</STRONG>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Not available; please retain and present your proof of purchase if service is needed
            </td>
          </tr>

          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              <STRONG>Technical Support for Professional Products:</STRONG>
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              Product operational assistance
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Phone: 800-883-6817, 8:30 a.m. - 8:00 p.m. ET, Monday - Friday
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 7px;">
              Product technical diagnostic support
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              If you have a current SupportNET agreement
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 50px;">
              Phone: 877-398-7669, 24 hours a day, 7 days a week.
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              If you do not have a current SupportNET agreement
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 50px;">
              Phone: 866-766-9272, 24 hours a day, 7 days a week
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 40px;">
              Technical Support
            </td>
          </tr>
          <tr>
            <td class="bodyCopy" style="padding-left: 50px;">
              Email: <a href="mailto:bis.product.support@am.sony.com" title="Click to send an email to bis.product.support@am.sony.com">bis.product.support@am.sony.com</a>
            </td>
          </tr>
          <tr>
            <td>
              <img height="10" src="images/spacer.gif" width="7" alt=""/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

