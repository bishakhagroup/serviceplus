<%@ Reference Page="~/SSL.aspx" %>
<%@ Reference Control="~/TrainingPromotionInsert.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="promotions.aspx.vb" Inherits="ServicePLUSWebApp.promotions" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html lang="<%=PageLanguage %>">
<head>

    <title>ServicesPLUS notifications for Sony parts and software</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <%-- <style type="text/css">
            .style1
            {
                height: 96px;
            }
        </style>--%>
    <style type="text/css">
        .style1 {
            width: 249px;
        }
    </style>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <center>
        <form id="form1" method="post" runat="server">
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td bgcolor="#ffffff" width="689">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td width="582" class="style1">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23" width="582">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="582">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" background="images/sp_int_header_top_ServicesPLUS_onepix.gif" height="57" align="right" valign="top">
                                                <br />
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;">Promotions</h2>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <img height="24" src="images/spacer.gif" width="246" alt="">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 582px">
                                    <asp:PlaceHolder ID="phPromotionInserts" runat="server"></asp:PlaceHolder>
                                    <table width="703" height="270" border="0" role="presentation">
                                        <tr valign="top">
                                            <td width="20">
                                                <img src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <a name="promo3"></a>

                                                <p><span id="Span1" class="promoCopyBold"></span></p>
                                                <span id="spnPartPromo" class="promoCopyBold">Parts Promotions: Exclusive Internet-only Discounts for ServicesPLUS purchases only!</span>
                                                <ul class="finderCopyDark">
                                                    <li>
                                                        <a href="parts-promotions.aspx#promo1">Learn more&#8230;</a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><span id="spnTrnPromo" class="promoCopyBold">Training Promotions:</span></p>
                                                <ul class="finderCopyDark">
                                                    <li>1.	<strong>Now on CD:</strong> MVS-8000/DVS-9000 Switcher Operator Training
                                                    </li>
                                                    <li>
                                                        <ul class="finderCopyDark">
                                                            <li>Extend your skills, learn from industry experts. Become a Certified Technical Director.
                                                                <a href="sony-training-class-details.aspx?CourseNumber=TR1004,-1">Learn more&#8230;</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td height="22">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td colspan="2" height="22">
                                                <asp:Label ID="ErrorLabel" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 582px">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnSessionId" runat="server" />
        </form>
    </center>
</body>

</html>
