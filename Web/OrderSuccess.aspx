<%@ Reference Page="~/Survey.aspx" %>
<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="Message" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.OrderSuccess" EnableViewStateMac="true" CodeFile="OrderSuccess.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_OrderSuccess%> </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="form" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif'); width: 464px; height: 40px; text-align: right; vertical-align: middle;">
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td style="vertical-align: middle; background: #363d45">
                                                <ServicePLUSWebApp:Message ID="Message1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_OrderSuccess%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="670" height="20">
                                                <asp:Label ID="errorMessage" runat="server" CssClass="tableData" ForeColor="Red"></asp:Label></td>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="670">
                                                <table cellpadding="3" border="0" role="presentation">
                                                    <tr>
                                                        <td width="315">
                                                            <table cellpadding="2" width="310" border="0" role="presentation">
                                                                <tr>
                                                                    <td class="headerBig">
                                                                        <%=Resources.Resource.el_urOrderNo%>
                                                                    </td>
                                                                    <td class="headerBig" bgcolor="#f2f5f8">
                                                                        <asp:Label ID="LabelOrderNumber" runat="server" CssClass="headerBig" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="25">&nbsp;</td>
                                                        <td width="315">
                                                            <span id="lblOrderInProcess" class="headerBig" runat="server"><%=Resources.Resource.el_OrderProcess%></span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <img height="20" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="100%">
                                                <asp:Table ID="TableSuccess" runat="server" border="0" Width="100%"></asp:Table>
                                                <img height="5" src="images/spacer.gif" width="670" alt="">
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                            <td width="670">
                                                <asp:Literal ID="SurveyLink" runat="server" />
                                            </td>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 20px;">
                                    <span class="legalCopy">
                                        <a href="#" onclick="window.open('TC2.aspx'); return false;" class="legalLink"><%=Resources.Resource.el_LimitedWaranty%></a></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 20px;">
                                    <span class="legalCopy">
                                        <br />
                                        <br />
                                        <asp:Label ID="labelSpecialTax" runat="server" Text="*  <%$ Resources:Resource, el_RecyclingFee1%>"> </asp:Label>
                                        <asp:HyperLink ID="specialTaxHyperLink" runat="server" Font-Underline="True" Font-Names="Arial"><%=Resources.Resource.el_RecyclingFee2%></asp:HyperLink>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
