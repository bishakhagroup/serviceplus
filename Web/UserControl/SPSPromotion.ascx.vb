Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class SPSPromotion
        Inherits UserControl

        Protected m_displayPage As String
        Protected m_Type As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim promoManager As PromotionManager = New PromotionManager
                Dim advertisement As Advertisement = promoManager.GetAdvertisement(Server.MapPath("PromotionConfig.xml"), Me.DisplayPage, Me.Type)
                If advertisement IsNot Nothing Then
                    imgPromo.AlternateText = advertisement.AltText
                    imgPromo.ImageUrl = advertisement.ImageFile
                    If Not String.IsNullOrWhiteSpace(advertisement.URL) Then
                        If advertisement.Popup Then
                            lnkPromo.Target = "_parent"
                        End If
                        lnkPromo.NavigateUrl = advertisement.URL
                        lnkPromo.Attributes.Item("title") = "Click to view information on the following promotion: " & advertisement.AltText
                    End If
                End If
            End If
        End Sub

        Public Property DisplayPage() As String
            Get
                If String.IsNullOrWhiteSpace(m_displayPage) Then m_displayPage = Page.GetType.Name.Replace("_aspx", "")
                Return m_displayPage
            End Get
            Set(ByVal Value As String)
                m_displayPage = Value
            End Set
        End Property

        Public Property Type() As Int16
            Get
                Return m_Type
            End Get
            Set(ByVal Value As Int16)
                m_Type = Value
            End Set
        End Property

    End Class

End Namespace