Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Partial Class UserControl_PromotionalItem
    Inherits System.Web.UI.UserControl

    Public cm As New CustomerManager
    Public Customer As Customer
    Public models() As Model

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Dim promoManager As PromotionManager = New PromotionManager
        Dim advistements As Advertisement() = promoManager.GetAdvertisementInserts(Server.MapPath("PromotionConfig.xml"))
        For Each advertisment As Advertisement In advistements
            phPromotionInserts.Controls.Add(Page.LoadControl(advertisment.URL))
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '6994 starts
        If Not Session.Item("customer") Is Nothing Then
            Customer = Session.Item("customer")
        End If
        '6994 ends
        populateInternetPromotionParts()
    End Sub
    Private Sub populateInternetPromotionParts()
        Dim internetPromotionParts As PromotionPart()
        Dim promotionManager As PromotionManager = New PromotionManager
        'Sasikumar WP SPLUS_WP007
		'Dim objGlobalData As GlobalData
		'If Not Session.Item("GlobalData") Is Nothing Then
		'    objGlobalData = Session.Item("GlobalData")
		'End If

        'If Cache.Item("InternetPromoPart") Is Nothing Then
		internetPromotionParts = promotionManager.GetInternetPromotionParts(ServicePLUSWebApp.HttpContextManager.GlobalData)
        'Cache.Insert("InternetPromoPart", internetPromotionParts, Nothing, DateTime.Now.AddMinutes(120), TimeSpan.Zero)
        'Else
        'internetPromotionParts = Cache.Item("InternetPromoPart")
        'End If

        'If Cache.Item("InternetPromoPart") Is Nothing Then
        '    internetPromotionParts = promotionManager.GetInternetPromotionParts()
        '    Cache.Insert("InternetPromoPart", internetPromotionParts, Nothing, DateTime.Now.AddMinutes(120), TimeSpan.Zero)
        'Else
        '    internetPromotionParts = Cache.Item("InternetPromoPart")
        'End If

        internetPromoDataGrid.DataSource = internetPromotionParts
        internetPromoDataGrid.DataBind()

        Dim promokits As PromotionKit()
        If Cache.Item("InternetPromoKit") Is Nothing Then
            promokits = promotionManager.GetInternetPromotionKits()
            Cache.Insert("InternetPromoKit", promokits, Nothing, DateTime.Now.AddMinutes(120), TimeSpan.Zero)

        Else
            promokits = Cache.Item("InternetPromoKit")
        End If

        'Bug 9063 Commenetd below 2 lined by Arshad
        'dgKits.DataSource = promokits '6994 v1
        'dgKits.DataBind()'6994 v1
    End Sub

    Protected Sub internetPromoDataGrid_OnItemDataBound(ByVal sender As System.Object, ByVal e As DataGridItemEventArgs) Handles internetPromoDataGrid.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim discountPercentage As String = e.Item.Cells(5).Text
            e.Item.Cells(5).Text = discountPercentage + "%"
            Dim n As Integer = e.Item.DataSetIndex()
            
            Dim lnkCart As HyperLink = CType(e.Item.Cells(6).Controls(1), HyperLink)
            lnkCart.Style.Add("cursor", "hand")
            Dim lblPart As Label
            lblPart = CType(e.Item.Cells(1).Controls(1), Label)
            If Customer Is Nothing Then
                lnkCart.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCart.aspx?post=promopart&PromoPartLineNo=" + lblPart.Text.ToString() + "';")
            Else

                lnkCart.NavigateUrl = "..//parts-promotions.aspx?pValue=" + lblPart.Text.ToString() + "&atc=true" '6994
                'lnkCart.Attributes.Add("onclick", "javascript:addPartToCart('" + lblPart.Text.ToString() + "'); return false;")
                ' lnkCart.Attributes.Add("onclick", "return AddToCart_onclick('" +  lblPart.Text.ToString() + "')") '6994
            End If
        End If
    End Sub
    'Bug 9063 Commented below even by Arshad 
    '6994 V1 starts
    'Protected Sub dgKits_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgKits.ItemDataBound
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        'Dim discountPercentage As String = e.Item.Cells(1).Text
    '        'e.Item.Cells(1).Text = discountPercentage + "%"
    '        Try
    '            Dim lnkKit As HyperLink = CType(e.Item.Cells(0).Controls(1), HyperLink)
    '            lnkKit.Style.Add("cursor", "hand")

    '            Dim lnkCart As HyperLink = CType(e.Item.Cells(2).Controls(1), HyperLink)
    '            lnkCart.Style.Add("cursor", "hand")

    '            '' Dim lblModel As Label
    '            'For Each t_kit As Kit In kits
    '            '' lblModel = CType(e.Item.Cells(3).Controls(1), Label)
    '            lnkKit.NavigateUrl = "..//" + lnkKit.Text.ToString() + "-kit.aspx"
    '            If Customer Is Nothing Then
    '                lnkCart.Attributes.Add("onclick", "window.location.href='NotLoggedInAddToCart.aspx?post=promopart&PromoKitLineNo=" + lnkKit.Text.ToString() + "';")
    '            Else
    '                lnkCart.NavigateUrl = "..//parts-promotions.aspx?kitVal=" + lnkKit.Text.ToString() + "&atc=true" '6994
    '                '' lnkCart.Attributes.Add("onclick", "return AddToCart_onclick('" + lnkKit.Text.ToString() + "')") '6994

    '            End If
    '        Catch ex As Exception
    '            Response.Write(ex.Message())
    '        End Try
    '    End If
    'End Sub
    '6994 V1 ends
  
End Class

