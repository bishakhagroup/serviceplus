<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.WebUserGuide" EnableViewStateMac="true" CodeFile="WebUserGuide.aspx.vb" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.header_serviceplus%> - <%=Resources.Resource.web_usr_gd_msg_1%></title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta name="keywords" content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software">

    <link type="text/css" rel="stylesheet" href="includes/ServicesPLUS_style.css">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" height="57" style="background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif')"
                                                align="right" valign="top">
                                                <br />
                                                <h1 class="headerText"><%=Resources.Resource.header_serviceplus%> &nbsp;&nbsp;</h1>
                                            </td>
                                            <td bgcolor="#363d45" valign="middle">
                                                <%--<table width="246" border="0" role="presentation">
                                                    <tr>
                                                        <td colspan="3">
                                                            <img src="images/spacer.gif" width="246" height="24" alt="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5">&nbsp;</td>
                                                        <td width="236">
                                                            <span class="memberName">--%>
                                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                            <%--</span>
                                                        </td>
                                                        <td width="5">&nbsp;</td>
                                                    </tr>
                                                </table>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.footer_help%></h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td bgcolor="#f2f5f8">
                                                <img src="images/sp_int_header_btm_left_onepix.gif" width="464" height="9" alt="">
                                            </td>
                                            <td bgcolor="#99a8b5">
                                                <img src="images/sp_int_header_btm_right.gif" width="246" height="9" alt="">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="650" border="0" role="presentation">
                                        <tr>
                                            <td align="center">
                                                <img src="images/spacer.gif" width="25" height="29" alt="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"><h3 class="promoCopyBold"><%=Resources.Resource.web_usr_gd_msg%></h3></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="images/spacer.gif" width="25" height="10" alt="" />
                                                <ul class="bodyCopy">
                                                    <%-- "ServicesPlus Registration" section --%>
                                                    <li>
                                                        <%=Resources.Resource.web_usr_gd_svcreg_msg%>
                                                        <p><%=Resources.Resource.web_usr_gd_svcreg_msg_1%></p>
                                                        <p><%=Resources.Resource.web_usr_gd_svcreg_msg_2%></p>
                                                        <table role="presentation">
                                                            <tr>
                                                                <td style="vertical-align: top;"><span class="bodyCopy"><%=Resources.Resource.web_usr_gd_simclk_msg%> &nbsp;</span></td>
                                                                <td>
                                                                    <a href="Registration.aspx" title="Click to Register a new ServicesPlus login.">
                                                                        <img src="<%=Resources.Resource.sna_svc_img_16%>" width="190" height="57" border="0" alt="Register Now"></a>
                                                                </td>
                                                                <td style="vertical-align: top;"><span class="bodyCopy"><%=Resources.Resource.web_usr_gd_simclk_msg_1%></span></td>
                                                            </tr>
                                                        </table>
                                                        <%-- "My Profile" section --%>
                                                        <p><%=Resources.Resource.web_usr_gd_simclk_msg_2%></p>
                                                        <table role="presentation">
                                                            <tr>
                                                                <td>
                                                                    <img src="<%=Resources.Resource.home_hdr_proon_img %>" width="78" height="27" alt="">
                                                                </td>
                                                                <td><span class="bodyCopy">&nbsp;<%=Resources.Resource.web_usr_gd_simclk_msg_3%></span></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                        <br />
                                                    </li>
                                                    <%-- "Home" section --%>
                                                    <li>
                                                        <table border="0" role="presentation">
                                                            <tr>
                                                                <td style="background: url('images/sp_hm_btm_bar.gif'); vertical-align: top; text-align: center; height: 29px; width: 86px;">
                                                                    <a href="default.aspx" class="menuClass" title="Click to return to the home page."><%=Resources.Resource.menu_home%></a>
                                                                </td>
                                                                <td style="vertical-align: top;">
                                                                    <span class="bodyCopy"><asp:Label ID="ServicesPlusLabel" runat="server" /></span>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                        <br />
                                                    </li>
                                                    <%-- "Catalog" section --%>
                                                    <li>
                                                        <table role="presentation">
                                                            <tr>
                                                                <td style="vertical-align: top;">
                                                                    <img src="<%=Resources.Resource.home_hdr_cat_img %>" border="0" alt="">
                                                                </td>
                                                                <td style="vertical-align: top;">
                                                                    <span class="bodyCopy">&nbsp;<%=Resources.Resource.web_usr_gd_catlg_msg%></span>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" role="presentation">
                                                            <tr>
                                                                <td>
                                                                    <img src="images/spacer.gif" width="100" height="11" alt="">
                                                                </td>
                                                                <td>
                                                                    <ul class="bodyCopy" type="circle">
                                                                        <li><%=Resources.Resource.web_usr_gd_catlglst_msg%></li>
                                                                        <li><%=Resources.Resource.web_usr_gd_rmndr_msg%></li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>
                                                    <%-- "Parts" section --%>
                                                    <li>
                                                        <table role="presentation">
                                                            <tr>
                                                                <td style="background: url('images/sp_hm_btm_bar.gif') no-repeat; vertical-align: top; text-align: center; height: 29px; width: 86px;">
                                                                    <a href="sony-parts.aspx" class="menuClass" title="Click to visit the Parts page."><%=Resources.Resource.menu_parts%></a>
                                                                </td>
                                                                <td>
                                                                    <ul class="bodyCopy" type="circle">
                                                                        <li><%=Resources.Resource.web_usr_gd_prts_msg%></li>
                                                                        <li><%=Resources.Resource.web_usr_gd_qksrch_msg%></li>
                                                                        <li><%=Resources.Resource.web_usr_gd_qkordr_msg%></li>
                                                                    </ul>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                    </li>
                                                    <%-- "View Cart" section --%>
                                                    <li>
                                                        <table border="0" role="presentation">
                                                            <tr>
                                                                <td style="vertical-align: top;">
                                                                    <span style="background: black;" class="PromoWhiteText"><img src="<%=Resources.Resource.mem_vwcart_img%>" alt="">&nbsp;<a class="PromoWhiteText" href="vc.aspx" title="Click to view your shopping cart."><%=Resources.Resource.mem_curnt_cart_msg%></a></span>
                                                                </td>
                                                                <td class="bodyCopy"><%=Resources.Resource.web_usr_gd_qkordr_msg_1%></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                    </li>
                                                    <%-- "My Orders" section --%>
                                                    <li>
                                                        <table border="0" role="presentation">
                                                            <tr>
                                                                <td style="vertical-align: top;">
                                                                    <img src="<%=Resources.Resource.home_hdr_oron_img %>" alt=""/>&nbsp;
                                                                </td>
                                                                <td><span class="bodyCopy"><%=Resources.Resource.web_usr_gd_qkordr_msg_2%></span></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                    </li>
                                                    <%-- "Order Limits" section --%>
                                                    <li>
                                                        <span class="bodyCopy"><%=Resources.Resource.web_usr_gd_orderlimts_msg%></span>
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <br />
                                                    </li>
                                                    <%-- "Software" section --%>
                                                    <li>
                                                        <table border="0" role="presentation">
                                                            <tr>
                                                                <td style="background: url('images/sp_hm_btm_bar.gif') no-repeat; vertical-align: top; text-align: center; height: 29px; width: 86px;">
                                                                    <a href="sony-software.aspx" class="menuClass" title="Click to view the software search page."><%=Resources.Resource.menu_software%></a>
                                                                </td>
                                                                <td class="bodyCopy"><%=Resources.Resource.web_usr_gd_sftbtns_msg%></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <br />
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
