<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.RARequest" EnableViewStateMac="true" CodeFile="RARequest.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><%=Resources.Resource.rar_req_hdr_msg() %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>

    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script language="javascript" type="text/javascript">

        function PopupWindow() {
            var PartNo = $("#PartNumber").val();

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");

            jQuery.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: "{ 'PartNumber': '" + PartNo + "' }",
                dataType: 'json',
                url: 'RARequest.aspx/GetConflictedPart',
                success: function (result) {

                    if ((jQuery.trim(result.d)) === "Not Valid Material") {

                        $("#divMessage").dialog({ // dialog box
                            title: '<span class="modalpopup-title"><%=Resources.Resource.rar_req_pup_msg()%></span>',
                                    height: 220,
                                    width: 560,
                                    modal: true,
                                    position: 'top'

                                });
                                var content = "</br><b><%=Resources.Resource.rar_req_pup_msg_1()%>" + PartNo + "<%=Resources.Resource.rar_req_pup_msg_2()%></b></br>"
                                $("#divMessage").append(content); //message to display in divmessage div
                                $("#divMessage").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return CloseValidatePopUp();"  id="messagegovalid" /></a>');
                                return false;
                            }
                            else if (jQuery.trim(result.d).length > 0) {// check conflict part

                                $("#divMessage").text('');
                                $("#divMessage").dialog({ // dialog box
                                    title: '<span class="modalpopup-title"><%=Resources.Resource.rar_req_pup_msg_3()%></span>',
                                    height: 220,
                                    width: 560,
                                    modal: true,
                                    position: 'top',
                                    close: function () {
                                        redirectTo();
                                    }

                                });
                                var NewPartNo = result.d;
                                $("#hdnNewPartNo").val(NewPartNo);
                                var content = "<br/><b><%=Resources.Resource.rar_req_pup_msg_1()%>" + PartNo + "<%=Resources.Resource.rar_req_pup_msg_4()%> " + NewPartNo + ".</br>"
                                content += "<%=Resources.Resource.rar_req_pup_msg_5()%></b></br>"
                                $("#divMessage").append(content); //message to display in divmessage div
                                $("#divMessage").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagegoconf" /></a>');
                        return false;

                    }

                } //End Function(result)
            });
            return true;
        }

        function CloseValidatePopUp() {
            $("#divMessage").dialog("close");
            $("#PartNumber").val("");
            $("#PartNumber").focus();
        }
        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }
        function redirectTo() {
            var NewModelNo = $("#hdnNewPartNo").val();
            $("#PartNumber").val(NewModelNo);
            return true;
        }


    </script>

    <script><!--
    //global variable declaration
    var req_edt = '<%=Resources.Resource.rar_req_glbl_vrble_msg()%>';
        var req_num_only = '<%=Resources.Resource.rar_req_glbl_vrble_msg_1()%>';
        var req_valid_email = '<%=Resources.Resource.rar_req_glbl_vrble_msg_2()%>';
        var req_good_zip = '<%=Resources.Resource.rar_req_glbl_vrble_msg_3()%>';
        var req_Line = '<%=Resources.Resource.rar_req_glbl_vrble_msg_4()%>';
        var req_good_phone = '<%=Resources.Resource.rar_req_glbl_vrble_msg_5()%>';
        var req_org = '<%=Resources.Resource.rar_req_glbl_vrble_msg_6()%>';

        //Change the label to red mark it validation failed
        function fBodyCopy(obj) {
            $("#" + obj).removeClass('redAsterick').addClass('bodyCopy');
        }
        //Revert the lable marks
        function fRedAsterick(obj) {
            $("#" + obj).removeClass('bodyCopy').addClass('redAsterick');
        }

        function validate() {
            var error = false;
            var br = "\n";

            $("#ErrorLabel").text("");

            var rgCell = /[0-9]{3}-[0-9]{3}-[0-9]{4}/;

            var vFstNa = $.trim($('#FirstName').val());
            var vLstNa = $.trim($('#LastName').val());
            var vEmail = $.trim($('#email').val());
            var vCompNa = $.trim($('#CompanyName').val());
            var vLine1 = $.trim($('#Line1').val());
            var vLine2 = $.trim($('#Line2').val());
            var vLine3 = $.trim($('#Line3').val());
            var vCity = $.trim($('#City').val());
            var vState = $.trim($('#DropDownList1').val());
            var vZip = $.trim($('#Zip').val());
            var vPh = $.trim($('#Phone').val());
            var vOrdNo = $.trim($('#OOrderNumber').val());
            var vInvNo = $.trim($('#OInvoiceNumber').val());
            var vPartNo = $.trim($('#PartNumber').val());
            var vReason = $.trim($('#Reason').val());
            var vRemarks = $.trim($('#Remarks').val());

            if (vFstNa === "") { fRedAsterick("LabelFN"); error = true; } else fBodyCopy("LabelFN");
            if (vLstNa === "") { fRedAsterick("LabelLN"); error = true; } else fBodyCopy("LabelLN");

            if (vEmail === "") { fRedAsterick("LabelEmail"); error = true; }
            else if (vEmail.indexOf("@", 0) === -1 || vEmail.indexOf(".", 0) === -1) { fRedAsterick("LabelEmail"); $('#ErrorLabel').text(req_valid_email); error = true; }
            else fBodyCopy("LabelEmail");

            if (vCompNa === "") { fRedAsterick("LabelCO"); error = true; } else fBodyCopy("LabelCO");

            if (vLine1 === "") { fRedAsterick("LabelLine1"); error = true; }
            else if (vLine1.length > 35) { fRedAsterick("LabelLine1"); $('#ErrorLabel').text(req_Line); error = true; }
            else fBodyCopy("LabelLine1");

            if (vLine2.length > 35) { fRedAsterick("LabelLine2"); $('#ErrorLabel').text(req_Line); error = true; }
            else fBodyCopy("LabelLine2");

            if (vLine3.length > 35) { fRedAsterick("LabelLine3"); $('#ErrorLabel').text(req_Line); error = true; }
            else fBodyCopy("LabelLine3");

            if (vCity === "") { fRedAsterick("LabelCity"); error = true; } else fBodyCopy("LabelCity");
            if (vState === "") { fRedAsterick("LabelState"); error = true; } else fBodyCopy("LabelState");

            if (vZip === "") { fRedAsterick("LabelZip"); error = true; }
            else if ((vZip.length !== 5 && vZip.length !== 10) ||
                (vZip.length === 5 && isNaN(vZip) === true) ||
                (vZip.length === 10 && vZip.indexOf("-", 5) === -1) ||
                (vZip.length === 10 && isNaN(vZip.substring(0, 4)) === true) ||
                (vZip.length === 10 && isNaN(vZip.substring(6)) === true)) { fRedAsterick("LabelZip"); $('#ErrorLabel').text(req_good_zip); error = true; }
            else fBodyCopy("LabelZip");

            if (vPh === "") { fRedAsterick("LabelPhone"); error = true; }
            else if (rgCell.test(vPh) === false) { fRedAsterick("LabelPhone"); $('#ErrorLabel').text(req_good_phone); error = true; } else fBodyCopy("LabelPhone");

            if (vOrdNo === "" && vInvNo === "") { fRedAsterick("LabelOOrderNumber"); fRedAsterick("LabelOInvoiceNumber"); error = true; } else { fBodyCopy("LabelOOrderNumber"); fBodyCopy("LabelOInvoiceNumber"); }
            if (vPartNo === "") { fRedAsterick("Label4"); error = true; } else fBodyCopy("Label4");
            if (vReason === "") { fRedAsterick("LabelReason"); error = true; } else fBodyCopy("LabelReason");
            if (vRemarks === "") { fRedAsterick("LabelRemarks"); error = true; } else fBodyCopy("LabelRemarks");

            if (error === true)
                return false;
            else
                return true;
        }
//-->
    </script>




</head>
<body text="#000000" bgcolor="#5d7180" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
    <%--<form id="form4" action="https://qa.esupport.sony.com/US/s/Form-Processor.pl"  method="post" runat="server">
		</form>--%>
    <form id="form4" method="post" runat="server">
        <input type="hidden" name="frh.form" value="proparts@am.sony.com">
        <input type="hidden" id="stype" name="stype" runat="server">
        <input type="hidden" id="cus_email" name="cus_email" runat="server">
        <input type="hidden" id="hdnNewPartNo" name="stype" runat="server">
        <center>
            <table width="710" border="0" role="presentation">
                <tbody>
                    <tr>
                        <td style="height: 1000px" width="25" background="images/sp_left_bkgd.gif">
                            <img height="25" src="images/spacer.gif" width="25">
                        </td>
                        <td style="height: 1000px" width="710" bgcolor="#ffffff">
                            <table width="650" border="0" role="presentation">
                                <tr>
                                    <td>
                                        <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 21px" height="21">
                                        <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="650" border="0" role="presentation">
                                            <tr>
                                                <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                    align="right" valign="top">
                                                    <br />
                                                    <h1 class="headerText"><%=Resources.Resource.header_serviceplus()%> &nbsp;&nbsp;</h1>
                                                </td>
                                                <td valign="middle" bgcolor="#363d45">
                                                    <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#f2f5f8">
                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.rar_req_prts_srrf_msg%></h2>
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr height="9">
                                                <td bgcolor="#f2f5f8">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464">
                                                </td>
                                                <td bgcolor="#99a8b5">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 12px" align="left">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="710" border="0" role="presentation">
                                            <tr>
                                                <td width="20">
                                                    <img height="20" src="images/spacer.gif" width="20"></td>
                                                <td width="670">

                                                    <table border="0" role="presentation">
                                                        <%--<tr>
															<td colSpan="4" height="15"><span class="tableHeader"><asp:label id="LabelHeader" runat="server" CssClass="tableHeader">PARTS RETURN REQUEST FORM</asp:label>&nbsp;</span></td>
														</tr>--%>
                                                        <tr>
                                                            <td colspan="4" height="18"><span class="tableHeader"><%=Resources.Resource.rar_req_prts_srrf_msg_1() %> </span><span class="redAsterick">*</span><span class="tableHeader"><%=Resources.Resource.rar_req_prts_srrf_msg_2()%></span></td>
                                                        </tr>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <span class="tableHeader">
                                                        <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>&nbsp;
																	<asp:Label ID="ErrorValidation" runat="server" CssClass="redAsterick"></asp:Label></span>
                                                </td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelFN" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_rgn_FirstName() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="FirstName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelLN" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_rgn_LastName() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="LastName" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelEmail" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_EmailAdd() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="email" runat="server" CssClass="bodyCopy" MaxLength="100"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelCO" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_svcacc_cmyname_msg() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="CompanyName" runat="server" CssClass="bodyCopy" MaxLength="100"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelLine1" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_svcacc_adrs_msg() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="Line1" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span><%--<asp:Label ID="LabelLine1Star" runat="server" CssClass="redAsterick" Text="*"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelLine2" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_svcacc_adrs_msg_1() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="Line2" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span><%--<asp:Label ID="LabelLine2Star" runat="server" CssClass="redAsterick"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60" /></td>
                                            </tr>
                                            <%--<tr>
															<td><img height="30" src="images/spacer.gif" width="13"></td>
															<td><asp:label id="LabelLine3" runat="server" CssClass="bodyCopy">Address 3rd Line:</asp:label>&nbsp;</td>
															<td>&nbsp;
																<SPS:SPSTextBox id="Line3" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick"></asp:Label></td>
															<td><img height="30" src="images/spacer.gif" width="60"></td>
														</tr>--%>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelCity" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_svcacc_city_msg()%></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="City" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span><%--<asp:Label ID="LabelLine4Star" runat="server" CssClass="redAsterick" Text="*"></asp:Label>--%>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelState" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_svcacc_state_msg() %> </asp:Label></td>
                                                <td>
                                                    &nbsp;
																<asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelZip" runat="server" CssClass="bodyCopy"><%=Resources.Resource.el_rgn_ZipCode() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="Zip" runat="server" CssClass="bodyCopy" MaxLength="20"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelPhone" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_snysvc_cfrmphn_msg() %></asp:Label>&nbsp;</td>
                                                <td height="29">
                                                    &nbsp;
																<SPS:SPSTextBox ID="Phone" runat="server" CssClass="bodyCopy" MaxLength="20"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <%If isAccntHolder = True Then%>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_3() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList><span class="redAsterick"></span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <%End If%>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td></td>
                                                <td height="29" cssclass="bodyCopy">&nbsp;<span class="bodyCopy"><span class="redAsterick">*</span>&nbsp;<%=Resources.Resource.rar_req_prts_srrf_msg_4() %></span></td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelOOrderNumber" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_5() %></asp:Label>&nbsp;</td>
                                                <td height="29">
                                                    &nbsp;
																<SPS:SPSTextBox ID="OOrderNumber" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelOInvoiceNumber" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_6() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="OInvoiceNumber" runat="server" CssClass="bodyCopy" MaxLength="8"></SPS:SPSTextBox>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_7() %></asp:Label>&nbsp;</td>
                                                <td height="29">
                                                    &nbsp;
																<SPS:SPSTextBox ID="PartNumber" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_8() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="Price" runat="server" CssClass="bodyCopy" MaxLength="12"></SPS:SPSTextBox><span class="redAsterick"></span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelReason" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_9() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<asp:DropDownList ID="Reason" runat="server"></asp:DropDownList><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="LabelRemarks" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_10() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="Remarks" runat="server" CssClass="bodyCopy" MaxLength="400" Width="400px" TextMode="MultiLine"
                                                                    Height="60px"></SPS:SPSTextBox><span class="redAsterick">*</span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" CssClass="bodyCopy"><%=Resources.Resource.rar_req_prts_srrf_msg_11() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="bodyCopy" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="Yes" Text="<%$ Resources:Resource, repair_svcspdsc_ys_msg%>"></asp:ListItem>
                                                                    <asp:ListItem Value="No" Selected="True" Text="<%$ Resources:Resource, el_no%>"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="13"></td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" CssClass="bodyCopy"><%=Resources.Resource.repair_snysvc_sno_msg() %></asp:Label>&nbsp;</td>
                                                <td>
                                                    &nbsp;
																<SPS:SPSTextBox ID="SerialNumber" runat="server" CssClass="bodyCopy" MaxLength="50"></SPS:SPSTextBox><span class="redAsterick"></span>
                                                </td>
                                                <td>
                                                    <img height="30" src="images/spacer.gif" width="60"></td>
                                            </tr>
                                            <tr bgcolor="#f2f5f8">
                                                <td colspan="4">
                                                    <table cellpadding="2" width="670" border="0" role="presentation">
                                                        <tr>
                                                            <td class="legalCopy">
                                                                <u><%=Resources.Resource.rar_req_prts_imp_msg%></u>!<br />
                                                                <%=Resources.Resource.rar_req_prts_imp_msg_1%> <u><%=Resources.Resource.rar_req_prts_imp_msg_2 %></u><%=Resources.Resource.rar_req_prts_imp_msg_3%>
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="4" height="40">
                                                    &nbsp;
															<img height="5" src="images/spacer.gif" width="670"><br />

                                                    <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" AlternateText="Create RA Request" ImageUrl="<%$ Resources:Resource,sna_svc_img_30%>"></asp:ImageButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <img height="5" src="images/spacer.gif" width="670"><br />
                                        <br />
                                        <span class="legalCopy">
                                            <a class="legalLink" href="LW.aspx"><%=Resources.Resource.rar_req_prts_lw_msg()%></a></span><br />
                                    </td>
                                    <td width="20">
                                        <img height="20" src="images/spacer.gif" width="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                        </td>
                    </tr>

            </table>

            </td>
                     <td width="25" background="images/sp_right_bkgd.gif">
                         <img height="20" src="images/spacer.gif" width="25"></td>
            </tr></tbody></table>
        </center>
    </form>
</body>
</html>
