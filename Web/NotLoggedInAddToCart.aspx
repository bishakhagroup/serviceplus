<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.NotLoggedInAddToCart" EnableViewStateMac="true" CodeFile="NotLoggedInAddToCart.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>ServicesPLUS - Must be Logged in</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
</head>
<body style="color: #000000; background: #ffffff; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <table width="400" border="0" role="presentation">
            <tr>
                <td background="<%=Resources.Resource.sna_svc_img_45%>">
                    <img src="images/spacer.gif" width="250" height="55" alt="">
                </td>
            </tr>
            <tr>
                <td height="30">
                    <img src="images/spacer.gif" width="250" height="30" alt="">
                </td>
            </tr>
            <tr>
                <td style="text-align: left; padding: 0px 20px;">
                    <asp:Label ID="MessageLabel" runat="server" CssClass="redAsterick" /><br />
                    <asp:ImageButton ID="btnClose" runat="server" ImageUrl="<%$ Resources:Resource,img_btnCloseWindow%>" AlternateText="Close Window" />
                </td>
            </tr>
            <tr>
                <td height="30">
                    <img src="images/spacer.gif" width="250" height="30" alt="">
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
