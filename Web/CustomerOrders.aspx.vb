Imports System.Data
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class CustomerOrders
        Inherits SSL

        Protected WithEvents errrorMessageLabel As System.Web.UI.WebControls.Label
        Private customer As Customer
        Private txtPageNumber As New Sony.US.ServicesPLUS.Controls.SPSTextBox()
        Private lblPageNumberError As New Label()
        Private btnGoTo As New ImageButton()
        Private Const conOrdersDetailPage As String = "MyOrderDetails.aspx"
        Private blnShowPagingText As Boolean = True
        Dim objUtilties As New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.

            isProtectedPage(True)
            InitializeComponent()
        End Sub

#End Region

#Region "Page members"

        Private Enum enumResultPageRequested
            PreviousPage = 0
            NextPage = 1
            UserEnteredPage = 2
            FirstPage = 3
            LastPage = 4
        End Enum

        Private Enum enumItemToProcess
            Cart = 0
            Order = 1
        End Enum

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If HttpContextManager.Customer IsNot Nothing Then customer = HttpContextManager.Customer
                lblPageNumberError.Text = String.Empty
                lblPageNumberError.CssClass = "redAsterick"
                If Not IsPostBack Then
                    'ViewState.Add("sortorder", "ASC")
                    'ViewState.Add("sortexp", "ordernumber")
                    ViewState.Add("sortorder", "DESC")
                    ViewState.Add("sortexp", "orderdate")
                    processPage(sender, e)
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "Grid Command"

        Protected Sub dgOrders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOrders.ItemCommand
            Try
                If e.CommandArgument = "GotoPage" Then
                    Dim iPageNumber As Int16
                    If (Not IsNumeric(txtPageNumber.Text)) OrElse (Not Int16.TryParse(txtPageNumber.Text, iPageNumber)) Then
                        lblPageNumberError.Text = Resources.Resource.el_NumericPageNo '"    Page number should be numeric."
                    Else
                        If (iPageNumber > dgOrders.PageCount) Or iPageNumber < 1 Then
                            lblPageNumberError.Text = "   " + Resources.Resource.el_EnterPageNo + " " + dgOrders.PageCount.ToString() + "." '"    Please enter page number between 1 to " + 
                        Else
                            dgOrders.CurrentPageIndex = iPageNumber - 1   ' Page numbers start at 1, but Arrays start at 0
                            txtPageNumber.Text = String.Empty
                            DisplayResultTables()
                            dgOrders.DataBind()
                        End If
                    End If
                ElseIf e.CommandArgument = "OrderDetail" Then
                    Session.Add("OrderNumber", e.Item.Cells(0).Text.ToString())
                    Server.Transfer(conOrdersDetailPage, False)
                    'Response.Redirect(conOrdersDetailPage, True)
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Protected Sub dgOrders_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOrders.ItemCreated
            If e.Item.ItemType = ListItemType.Pager Then
                If blnShowPagingText Then
                    Dim objDataGridItem As DataGridItem
                    Dim oHTMLTD As New HtmlTableCell("sp-pagingtext")
                    Dim oBlankTD As New HtmlTableCell("sp-blankspace")
                    Dim lblHtmlLabel As New Label With {
                        .ID = "lblPageNumber",      ' 2019-01-18 Added for WCAG Compliance
                        .Text = Resources.Resource.el_PageNo, '"Page Number:"
                        .CssClass = "BodyCopy"
                    }
                    Dim htmlImg As New HtmlImage With {
                        .Src = "images/spacer.gif",
                        .Width = "10",
                        .Alt = ""       ' 2019-01-18 Added for WCAG Compliance
                    }
                    oHTMLTD.Width = "550"
                    txtPageNumber.ID = "txtPageNumber"
                    txtPageNumber.CssClass = "BodyCopy"
                    txtPageNumber.Width = Unit.Pixel(25)
                    txtPageNumber.Height = Unit.Pixel(17)
                    txtPageNumber.Attributes.Add("aria-labelledby", "lblPageNumber")    ' 2019-01-18 Added for WCAG Compliance

                    btnGoTo.ID = "btnGoTo"
                    btnGoTo.ImageUrl = "images/sp_int_rightArrow_btn.gif"
                    btnGoTo.CommandArgument = "GotoPage"
                    btnGoTo.AlternateText = "Go to the page number specified."          ' 2019-01-18 Added for WCAG Compliance
                    objDataGridItem = e.Item
                    oBlankTD.Controls.Add(htmlImg)
                    oHTMLTD.Controls.Add(oBlankTD)
                    oHTMLTD.Controls.Add(lblHtmlLabel)
                    oHTMLTD.Controls.Add(txtPageNumber)
                    oHTMLTD.Controls.Add(btnGoTo)
                    objDataGridItem.Controls(0).Controls.Add(oHTMLTD)
                    oHTMLTD.Controls.Add(lblPageNumberError)
                End If
            End If
        End Sub

        Protected Sub dgOrders_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOrders.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Pager
                    Dim objDataGridItem As DataGridItem
                    objDataGridItem = e.Item
                Case Else
                    Exit Select
            End Select
        End Sub

        Protected Sub dgOrders_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgOrders.PageIndexChanged
            Dim tmpDataSet As DataSet
            tmpDataSet = dgOrders.DataSource
            dgOrders.CurrentPageIndex = e.NewPageIndex
            If tmpDataSet Is Nothing Then
                DisplayResultTables(False, ViewState("sortexp").ToString())
            End If
            dgOrders.DataBind()
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, '" + btnGoTo.ClientID + "')"
        End Sub

        Protected Sub dgOrders_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOrders.SortCommand
            If e.SortExpression <> ViewState("sortexp").ToString() Then ViewState("sortorder") = "ASC"
            ViewState.Add("sortexp", e.SortExpression)
            DisplayResultTables(True, e.SortExpression)
        End Sub

#End Region

#Region "Dispaly table"
        Private Sub processPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                DisplayResultTables(True, ViewState("sortexp").ToString())
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisplayResultTables(Optional ByVal DoSort As Boolean = False, Optional ByVal SortExp As String = "")
            BindOrdersGrid(New OrderManager().GetCustomerOrders(customer.CustomerID), DoSort, SortExp)
        End Sub
#End Region

#Region "Build Orders Table"
        Private Sub BindOrdersGrid(ByVal customerOrders As DataSet, Optional ByVal DoSort As Boolean = False, Optional ByVal SortExp As String = "")
            Dim dView As DataView

            If customerOrders IsNot Nothing Then
                If customerOrders.Tables.Count > 0 Then
                    If DoSort = False Then
                        dView = customerOrders.Tables(0).DefaultView
                        dView.Sort = ViewState("sortexp") + " " + IIf(ViewState("sortorder").ToString() = "DESC", "ASC", "DESC")
                        ApplySortingArrow(dView.Sort)
                        dgOrders.DataSource = dView
                        dgOrders.DataBind()
                    Else
                        dView = customerOrders.Tables(0).DefaultView
                        dView.Sort = SortExp + " " + ViewState("sortorder").ToString()
                        ApplySortingArrow(dView.Sort)
                        dgOrders.DataSource = dView
                        dgOrders.DataBind()
                        ViewState("sortorder") = IIf(ViewState("sortorder").ToString() = "DESC", "ASC", "DESC")
                    End If
                    'If Not customer.UserType = "AccountHolder" Then'6668
                    If Not customer.UserType = "A" Or Not customer.UserType = "P" Then '6668
                        dgOrders.Columns(4).Visible = False
                    End If
                Else
                    errorMessageLabel.Text = Resources.Resource.el_DoNotOrder '"You do no order."
                End If
            Else
                errorMessageLabel.Text = Resources.Resource.el_DoNotOrder '"You do no order."
            End If

        End Sub

        Private Sub ApplySortingArrow(ByVal SortingValue As String)
            Select Case SortingValue
                Case "ordernumber ASC"
                    dgOrders.Columns(0).HeaderStyle.CssClass = "sortedASC"
                    dgOrders.Columns(1).HeaderStyle.CssClass = "tableHeader"
                    Exit Select
                Case "ordernumber DESC"
                    dgOrders.Columns(0).HeaderStyle.CssClass = "sortedDESC"
                    dgOrders.Columns(1).HeaderStyle.CssClass = "tableHeader"
                    Exit Select
                Case "orderdate ASC"
                    dgOrders.Columns(1).HeaderStyle.CssClass = "sortedASC"
                    dgOrders.Columns(0).HeaderStyle.CssClass = "tableHeader"
                    Exit Select

                Case "orderdate DESC"
                    dgOrders.Columns(1).HeaderStyle.CssClass = "sortedDESC"
                    dgOrders.Columns(0).HeaderStyle.CssClass = "tableHeader"
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End Sub

#End Region
    End Class

End Namespace
