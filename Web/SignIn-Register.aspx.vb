﻿Imports System.Threading
Imports Microsoft.Security.Application
Imports Sony.US.ServicesPLUS.Core

Namespace ServicePLUSWebApp
    Partial Class SignIn_Register
        Inherits SSL

        Protected strRequestedPage As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Session("customer") IsNot Nothing Then
                CloseAndRedirect()
            Else
                ProcessRedirections()
            End If
        End Sub

        Private Sub CloseAndRedirect()
            If Not String.IsNullOrWhiteSpace(Request.QueryString("post")) Then
                Select Case Request.QueryString("post")
                    Case "om"
                        Response.Redirect("sony-operation-manual.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "qo"
                        Response.Redirect("sony-parts.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "promopart"
                        Response.Redirect("parts-promotions.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "prc"
                        Response.Redirect("sony-service-agreements.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "prr"
                        Response.Redirect("Online-Product-Registration.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "ssmfree"
                        Response.Redirect("SoftwareFreeDownload.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case "ssm"
                        If Not Request.QueryString("ModelNumber") Is Nothing Then
                            Response.Redirect("sony-software-model-" + Request.QueryString("ModelNumber") + ".aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-software.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "ssp"
                        If Not Request.QueryString("ModelNumber") Is Nothing Then
                            Response.Redirect("sony-software-part-" + Request.QueryString("PartNumber") + ".aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-training-catalog.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "ppr"
                        If Not Request.QueryString("stype") Is Nothing Then
                            If Not Request.QueryString("PartLineNo") Is Nothing Then
                                Response.Redirect("PartsPLUSResults.aspx?stype=" + Request.QueryString("stype").ToString() + "&PartLineNo=" + Request.QueryString("PartLineNo"), False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                            ElseIf Not Request.QueryString("KitLineNo") Is Nothing Then
                                Response.Redirect("PartsPLUSResults.aspx?stype=" + Request.QueryString("stype").ToString() + "&KitLineNo=" + Request.QueryString("KitLineNo"), False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                            Else
                                Response.Redirect("sony-parts.aspx", False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                            End If
                        Else
                            Response.Redirect("sony-parts.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "pps"
                        If Not Request.QueryString("ArrayVal") Is Nothing Then
                            Response.Redirect("PartsPLUSSearch.aspx?ArrayVal=" + Request.QueryString("ArrayVal") + "&returnfrom=signin", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-parts.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "ssrm", "spmsr", "sr", "srms"
                        If Not Request.QueryString("SPGSModel") Is Nothing Then
                            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
                            Response.Redirect("sony-service-sn.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-repair.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "spcd"
                        If Not Request.QueryString("groupid") Is Nothing Then
                            Response.Redirect("groupid" + Request.QueryString("groupid") + ".aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-part-catalog.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "stcd"
                        If Not Request.QueryString("CourseLineno") Is Nothing Then
                            Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                            Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                            Dim cr As New CourseReservation
                            cr.CourseSchedule = courseSelected
                            Session("trainingreservation") = cr
                            CloseAndRedirectCourse()
                        Else
                            Response.Redirect("sony-training-catalog.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "stc2"
                        If Not Request.QueryString("mincatid") Is Nothing Then
                            Response.Redirect("sony-training-catalog-2.aspx?mincatid=" + Request.QueryString("mincatid"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            Response.Redirect("sony-training-catalog.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "stcdatc"
                        If Not Request.QueryString("ItemNumber") Is Nothing Then
                            Response.Redirect("sony-training-class-details.aspx?ItemNumber=" + Request.QueryString("ItemNumber"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        ElseIf Not Request.QueryString("ClassNumber") Is Nothing Then
                            Response.Redirect("sony-training-class-details.aspx?ClassNumber=" + Request.QueryString("ClassNumber"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        ElseIf Request.QueryString("CourseNumber") Is Nothing Then
                            Response.Redirect("sony-training-class-details.aspx?CourseNumber=" + Request.QueryString("CourseNumber"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "ewprod"
                        'post=ewprod&ewc=<%=EWProductCode%>&pc=<%=ProductCode%>
                        If Not Request.QueryString("ewc") Is Nothing And Not Request.QueryString("pc") Is Nothing Then
                            Response.Redirect("Sony-EW-PurchaseInfo.aspx?ewc=" + Request.QueryString("ewc").ToString() + "&pc=" + Request.QueryString("pc").ToString() + "&mdl=" + Request.QueryString("mdl"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Else
                            'Added by prasad for 2523
                            Response.Redirect("sony-extended-warranties.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "ewcert"
                        If Not Request.QueryString("onum") Is Nothing Then
                            Server.Transfer("Sony-EWCertificate.aspx?onum=" + Request.QueryString("onum"), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Case "mark"
                        Response.Redirect("shared/Marketing.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Case Else
                        'Do nothing
                End Select
            Else
                If Not Session("LoginFrom") Is Nothing Then
                    Select Case Session("LoginFrom").ToString()
                        Case "Bulletin"
                            Response.Redirect(Session("RedirectTo").ToString(), False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        Case Else
                            Response.Redirect("Member.aspx", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End Select
                ElseIf Not Request.QueryString("PartNumber") Is Nothing Then 'from popup after selecting download free
                    'closeAndRedirectPart() 'go to member.aspx after log in
                    Response.Redirect("SoftwareFreeDownload.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                ElseIf Not Request.QueryString("FromHome") Is Nothing Then 'from popup after selecting login menu
                    CloseAndRedirectPart() 'go to member.aspx after log in
                ElseIf Not Request.QueryString("TechBulletin") Is Nothing Then
                    CloseAndRedirectTechBulletin()
                ElseIf Not Request.QueryString("CourseLineno") Is Nothing Then
                    Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                    Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                    Dim cr As New CourseReservation
                    cr.CourseSchedule = courseSelected
                    Session("trainingreservation") = cr
                    CloseAndRedirectCourse()
                ElseIf Not Request.QueryString("SPGSModel") Is Nothing Then
                    CloseAndRedirectDepotRequest()
                ElseIf Not Request.QueryString("Promo") Is Nothing Then
                    CloseAndRedirectPromoRequest(Request.QueryString("Promo"))
                ElseIf Not Request.QueryString("RAForm") Is Nothing Then
                    CloseAndRedirectRAForm()
                    'ElseIf newPunchOutUser Then
                    '    Response.Redirect("PunchOutWelcome.aspx", True)
                Else
                    RefreshParentPage()
                End If
            End If
        End Sub
        Private Sub CloseAndRedirectPromoRequest(ByVal sPromoPath As String)
            Response.Redirect(sPromoPath)
        End Sub
        Private Sub CloseAndRedirectDepotRequest()
            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
            Response.Redirect("sony-service-sn.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Private Sub CloseAndRedirectPart()
            'Response.Redirect("Member.aspx")
            Response.Redirect("Member.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Private Sub CloseAndRedirectTechBulletin()
            Response.Redirect("Sony-Technical-Bulletins-GenInfo.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Private Sub CloseAndRedirectCourse()
            Dim Customer As Customer
            If Not Session.Item("customer") Is Nothing Then
                Customer = Session.Item("customer")
            Else
                Return
            End If

            Dim nonAccount = True

            'For Each sap As Account In Customer.SAPBillToAccounts 'commented for 6865
            '    If sap.Validated Then
            '        nonAccount = False
            '    End If
            'Next

            'For Each sis As SISAccount In Customer.SISLegacyBillToAccounts 'commented for 6865
            '    If sis.Validated Then
            '        nonAccount = False
            '    End If
            'Next

            If nonAccount And IsUserActive(Customer) Then
                Response.Redirect("training-payment-naccnt.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Else
                Response.Redirect("training-payment-accnt.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If

        End Sub
        'Start fixing Bug#:125
        Private Sub CloseAndRedirectRAForm()
            Response.Redirect("sony-service-contacts.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub
        Private Sub RefreshParentPage()
            Try

                Response.Redirect("Member.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            Catch ex As ThreadAbortException

            End Try
        End Sub

        Private Sub ProcessRedirections()
            'Dim LanguageSM = HttpContextManager.GlobalData.LanguageForSM
            strRequestedPage = ""

            'Query strings to be requested:
            ' post ewc pc mdl onum om mincatid ModelNumber PartNumber stype PartLineNo KitLineNo ArrayVal SPGSModel groupid
            ' CourseLineno ItemNumber  ClassNumber CourseNumber FromHome SPGSModel TechBulletin Promo RAForm

            'Pass language to siteminder 
            'If (Session("SelectedLang") IsNot Nothing) Then
            '    LanguageSM = Session("SelectedLang").ToString().Substring(0, 2).ToUpper()
            'End If 
            'Dim objGlobalData As New GlobalData
            'If Not Session.Item("GlobalData") Is Nothing Then
            '    objGlobalData = Session.Item("GlobalData")
            'End If
            'If HttpContextManager.GlobalData.IsCanada Then
            '    If HttpContextManager.GlobalData.IsEnglish Then
            '        LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA_en.Replace("-", "_")
            '    Else
            '        LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.CA.Replace("-", "_")
            '    End If
            'Else
            '    LanguageSM = ConfigurationData.GeneralSettings.GlobalData.Language.US.Replace("-", "_")
            'End If

            If Not String.IsNullOrEmpty(Request.QueryString("post")) Then
                strRequestedPage = "post=" & AntiXss.HtmlEncode(Request.QueryString("post"))
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("ewc")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "ewc=" & AntiXss.HtmlEncode(Request.QueryString("ewc"))
                Else
                    strRequestedPage = strRequestedPage & "&ewc=" & AntiXss.HtmlEncode(Request.QueryString("ewc"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("pc")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "pc=" & AntiXss.HtmlEncode(Request.QueryString("pc"))
                Else
                    strRequestedPage = strRequestedPage & "&pc=" & AntiXss.HtmlEncode(Request.QueryString("pc"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("mdl")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "mdl=" & AntiXss.HtmlEncode(Request.QueryString("mdl"))
                Else
                    strRequestedPage = strRequestedPage & "&mdl=" & AntiXss.HtmlEncode(Request.QueryString("mdl"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("onum")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "onum=" & AntiXss.HtmlEncode(Request.QueryString("onum"))
                Else
                    strRequestedPage = strRequestedPage & "&onum=" & AntiXss.HtmlEncode(Request.QueryString("onum"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("om")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "om=" & AntiXss.HtmlEncode(Request.QueryString("om"))
                Else
                    strRequestedPage = strRequestedPage & "&om=" & AntiXss.HtmlEncode(Request.QueryString("om"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("mincatid")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "mincatid=" & AntiXss.HtmlEncode(Request.QueryString("mincatid"))
                Else
                    strRequestedPage = strRequestedPage & "&mincatid=" & AntiXss.HtmlEncode(Request.QueryString("mincatid"))
                End If
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("ModelNumber")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "ModelNumber=" & AntiXss.HtmlEncode(Request.QueryString("ModelNumber"))
                Else
                    strRequestedPage = strRequestedPage & "&ModelNumber=" & AntiXss.HtmlEncode(Request.QueryString("ModelNumber"))
                End If
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("PartNumber")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "PartNumber=" & AntiXss.HtmlEncode(Request.QueryString("PartNumber"))
                Else
                    strRequestedPage = strRequestedPage & "&PartNumber=" & AntiXss.HtmlEncode(Request.QueryString("PartNumber"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("stype")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "stype=" & AntiXss.HtmlEncode(Request.QueryString("stype"))
                Else
                    strRequestedPage = strRequestedPage & "&stype=" & AntiXss.HtmlEncode(Request.QueryString("stype"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("PartLineNo")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "PartLineNo=" & AntiXss.HtmlEncode(Request.QueryString("PartLineNo"))
                Else
                    strRequestedPage = strRequestedPage & "&PartLineNo=" & AntiXss.HtmlEncode(Request.QueryString("PartLineNo"))
                End If
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("KitLineNo")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "KitLineNo=" & Request.QueryString("KitLineNo")
                Else
                    strRequestedPage = strRequestedPage & "&KitLineNo=" & AntiXss.HtmlEncode(Request.QueryString("KitLineNo"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("ArrayVal")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "ArrayVal=" & Request.QueryString("ArrayVal")
                Else
                    strRequestedPage = strRequestedPage & "&ArrayVal=" & AntiXss.HtmlEncode(Request.QueryString("ArrayVal"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("SPGSModel")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "SPGSModel=" & AntiXss.HtmlEncode(Request.QueryString("SPGSModel"))
                Else
                    strRequestedPage = strRequestedPage & "&SPGSModel=" & AntiXss.HtmlEncode(Request.QueryString("SPGSModel"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("groupid")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "groupid=" & AntiXss.HtmlEncode(Request.QueryString("groupid"))
                Else
                    strRequestedPage = strRequestedPage & "&groupid=" & AntiXss.HtmlEncode(Request.QueryString("groupid"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("CourseLineno")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "CourseLineno=" & AntiXss.HtmlEncode(Request.QueryString("CourseLineno"))
                Else
                    strRequestedPage = strRequestedPage & "&CourseLineno=" & AntiXss.HtmlEncode(Request.QueryString("CourseLineno"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("ItemNumber")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "ItemNumber=" & Request.QueryString("ItemNumber")
                Else
                    strRequestedPage = strRequestedPage & "&ItemNumber=" & AntiXss.HtmlEncode(Request.QueryString("ItemNumber"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("ClassNumber")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "ClassNumber=" & Request.QueryString("ClassNumber")
                Else
                    strRequestedPage = strRequestedPage & "&ClassNumber=" & AntiXss.HtmlEncode(Request.QueryString("ClassNumber"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("CourseNumber")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "CourseNumber=" & AntiXss.HtmlEncode(Request.QueryString("CourseNumber"))
                Else
                    strRequestedPage = strRequestedPage & "&CourseNumber=" & AntiXss.HtmlEncode(Request.QueryString("CourseNumber"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("FromHome")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "FromHome=" & AntiXss.HtmlEncode(Request.QueryString("FromHome"))
                Else
                    strRequestedPage = strRequestedPage & "&FromHome=" & AntiXss.HtmlEncode(Request.QueryString("FromHome"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("TechBulletin")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "TechBulletin=" & AntiXss.HtmlEncode(Request.QueryString("TechBulletin"))
                Else
                    strRequestedPage = strRequestedPage & "&TechBulletin=" & AntiXss.HtmlEncode(Request.QueryString("TechBulletin"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("Promo")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "Promo=" & AntiXss.HtmlEncode(Request.QueryString("Promo"))
                Else
                    strRequestedPage = strRequestedPage & "&Promo=" & AntiXss.HtmlEncode(Request.QueryString("Promo"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("RAForm")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "RAForm=" & AntiXss.HtmlEncode(Request.QueryString("RAForm"))
                Else
                    strRequestedPage = strRequestedPage & "&RAForm=" & AntiXss.HtmlEncode(Request.QueryString("RAForm"))
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("SessionID")) Then
                If strRequestedPage = "" Then
                    strRequestedPage = "SessionID=" & AntiXss.HtmlEncode(Request.QueryString("SessionID"))
                Else
                    strRequestedPage = strRequestedPage & "&SessionID=" & AntiXss.HtmlEncode(Request.QueryString("SessionID"))
                End If
            End If

            If strRequestedPage <> "" Then
                strRequestedPage = "?Lang=" + HttpContextManager.GlobalData.LanguageForIDP & "&" & strRequestedPage
            Else
                strRequestedPage = "?Lang=" + HttpContextManager.GlobalData.LanguageForIDP
            End If

        End Sub

    End Class
End Namespace