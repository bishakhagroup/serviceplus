Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class vc
        Inherits SSL

        'Public cm As New CustomerManager
        Public customer As Customer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Protected WithEvents MemberName As System.Web.UI.WebControls.Label
        Protected WithEvents MemberMessage As System.Web.UI.WebControls.Label
        Protected WithEvents SubTotalLabel As System.Web.UI.WebControls.Label
        Protected WithEvents ContentLabel As System.Web.UI.WebControls.Label


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim cart As ShoppingCart = Nothing

            If HttpContextManager.Customer Is Nothing Then
                Response.Redirect("default.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                Return
            End If
            customer = HttpContextManager.Customer

            If (Request.QueryString("crtnum") IsNot Nothing) AndAlso (IsValidString(Request.QueryString("crtnum"))) Then
                cart = getThisCart(customer, Request.QueryString("crtnum"))
                Session.Add("carts", cart)
            ElseIf (Session.Item("carts") IsNot Nothing) Then
                cart = Session.Item("carts")
            End If


            If Not Page.IsPostBack Then
                Try
                    Me.tdLegend.Visible = False
                    lblSubHdr.InnerText = Resources.Resource.el_ViewCart    ' 2018-09-14 Moved from HTML markup for WCAG compliance. (can't use ASP:Label, have to use <h2>, so can't use server tags AND update content from code side.)
                    GetCart()
                Catch ex As Exception
                    CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
                    Return
                End Try
            End If

            '-- display cart buttons only if there is any valid cart
            lblNote.Visible = True
            If cart IsNot Nothing AndAlso cart.ShoppingCartItems.Length > 0 Then
                If customer.IsPunchOutUser Then
                    PunchOut.Visible = Not cart.ContainsDownloadOnlyItems
                    CheckOutButton.Visible = cart.ContainsDownloadOnlyItems
                    CustomerLabel.Text = ""
                Else
                    PunchOut.Visible = False
                    If (ConfigurationData.GeneralSettings.General.EnableCheckOut) Then
                        CheckOutButton.Visible = True
                        CustomerLabel.Visible = False
                    Else
                        CheckOutButton.Visible = False
                        CustomerLabel.Visible = True
                        ' "Online ordering is temporarily unavailable. Please order by calling 1-800-538-7550 or emailing"
                        CustomerLabel.Text = Resources.Resource.el_Order_Unavailable1 + " <a style=""color:Blue"" href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a>"
                    End If
                End If
                UpdateButton.Visible = True
                SaveCartButton.Visible = True
            Else
                UpdateButton.Visible = False
                SaveCartButton.Visible = False
                CheckOutButton.Visible = False
                PunchOut.Visible = False
                CustomerLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
                trCart.Visible = False
            End If
        End Sub

        Private Sub UpdateButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles UpdateButton.Click
            If updateSCart() = -1 Then
                Return
            End If
        End Sub

        Private Sub CheckOutButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles CheckOutButton.Click
            Dim debugMessage As String = $"VC.aspx - CheckOutButton_Click - START at {DateTime.Now.ToShortTimeString}{Environment.NewLine}"
            Dim cart As ShoppingCart
            Dim cc_only_purchase = True

            Try
                If HttpContextManager.Customer IsNot Nothing Then
                    customer = HttpContextManager.Customer
                Else
                    CustomerLabel.Text = Resources.Resource.el_RegistertoAddCart '("You must be a registered customer to view a shopping cart.")
                    Return
                End If
                debugMessage &= $"- Customer fetched. Email: {If(customer?.EmailAddress, "null")}, Sequence: {If(customer?.SequenceNumber, "null")}.{Environment.NewLine}"

                updateSCart()
                debugMessage &= $"- Cart updated. Refetching from Session.{Environment.NewLine}"

                cart = Session.Item("carts")
                If (cart Is Nothing) OrElse (cart.ShoppingCartItems Is Nothing) OrElse (cart.ShoppingCartItems.Length < 1) Then
                    CustomerLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
                    trCart.Visible = False
                    lblNote.Visible = True
                    Return
                End If

                If customer.UserType = "A" Then
                    debugMessage &= $"- Customer is Account Holder.{Environment.NewLine}"
                    If (customer.SAPBillToAccounts?.Length > 0) Or (customer.SISLegacyBillToAccounts?.Length > 0) Then
                        cc_only_purchase = False
                    End If
                End If

                '--- modification for disabling back button - Deepa V, Oct 20, 2006
                '-- current process index - 0 => parts check out
                Session("currentProcessIndex") = "0"
                Session("CurrentPageIndex") = "0" '-- check out button click is the begining of the check out process

                debugMessage &= $"- Redirecting to next page..."
                If cc_only_purchase = False And customer.IsActive Then
                    Response.Redirect("AccntHolder.aspx", False)
                Else
                    Response.Redirect("NAccntHolder.aspx", False)
                End If
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch exThrd As Threading.ThreadAbortException
                '-- do nothing 
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
                Dim objPCILogger As New PCILogger With {
                    .EventOriginApplication = "ServicesPLUS",
                    .EventOriginApplicationLocation = Me.Page.GetType().Name,
                    .EventOriginServerMachineName = HttpContext.Current.Server.MachineName,
                    .OperationalUser = Environment.UserName,
                    .EventDateTime = DateTime.Now.ToLongTimeString(),
                    .EventOriginMethod = "CheckOutButton_Click",
                    .EventType = EventType.Others,
                    .CustomerID = customer?.CustomerID,
                    .CustomerID_SequenceNumber = customer?.SequenceNumber,
                    .EmailAddress = customer?.EmailAddress,
                    .SIAM_ID = customer?.SIAMIdentity,
                    .LDAP_ID = customer?.LdapID,
                    .IndicationSuccessFailure = "Failure",
                    .Message = "CheckOut Failed. " & ex.Message.ToString()
                }
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        'Private Sub cancelOpenOrder()
        '    If Not Session.Item("order") Is Nothing And Not Session.Item("carts") Is Nothing Then
        '        Try
        '            Dim order As Sony.US.ServicesPLUS.Core.Order = Session.Item("order")
        '            Dim cart As Sony.US.ServicesPLUS.Core.ShoppingCart = Session.Item("carts")
        '            Dim om As New OrderManager

        '            '***************Changes done for Bug# 1212 Starts here***************
        '            '**********Kannapiran S****************
        '            
        '            objSISParameter.UserLocation = customer.UserLocation
        '            om.CancelOrder(order, cart)
        '            '***************Changes done for Bug# 1212 Ends here***************

        '        Catch ex As Exception
        '            CustomerLabel.Text = ex.Message
        '        End Try
        '    End If
        'End Sub

        Private Sub SaveCartButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SaveCartButton.Click
            If Session.Item("carts") Is Nothing Then
                CustomerLabel.Text = Resources.Resource.el_CurrentCartEmpty '"Current cart is empty."
                Return
            End If
            If updateSCart() = -1 Then
                Return
            Else
                Response.Redirect("ViewSavedCart.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        End Sub

        'Protected Sub PunchOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles PunchOut.Click
        '    Dim objPCILogger As New PCILogger() '6524
        '    Try
        '        If Not HttpContextManager.Customer Is Nothing Then
        '            customer = HttpContextManager.Customer
        '        Else
        '            CustomerLabel.Text = Resources.Resource.el_RegisterPunchout '("You must be a registered customer to punchout shopping cart. Please login into system.")
        '            Return
        '        End If


        '        '6524 V21 start
        '        'objPCILogger.EventOriginApplication = "ServicesPLUS"
        '        'objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
        '        'objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
        '        'objPCILogger.OperationalUser = Environment.UserName
        '        'objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
        '        'objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
        '        'objPCILogger.IndicationSuccessFailure = "Success"
        '        'objPCILogger.CustomerID = customer.CustomerID
        '        'objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
        '        ''objPCILogger.UserName = customer.UserName
        '        'objPCILogger.EmailAddress = customer.EmailAddress '6524
        '        'objPCILogger.SIAM_ID = customer.SIAMIdentity
        '        'objPCILogger.LDAP_ID = customer.LdapID '6524
        '        'objPCILogger.EventOriginMethod = "PunchOut_Click"
        '        'objPCILogger.Message = "PunchOut_Click Success."
        '        'objPCILogger.EventType = EventType.Add
        '        '6524 V21 end

        '        '-- for issue E595 - Deepa V, Oct 30, 2006
        '        updateSCart()

        '        Dim strDataToPost As String

        '        If Not Session.Item("carts") Is Nothing Then
        '            Dim cart As ShoppingCart
        '            cart = Session.Item("carts")
        '            If cart.ShoppingCartItems.Length < 1 Then
        '                CustomerLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
        '                trCart.Visible = False
        '                lblNote.Visible = True
        '                Return
        '            Else
        '                strDataToPost = CXMLOperation.GetOrderCXML(cart)
        '                'PunchOut Cart
        '            End If
        '        Else
        '            CustomerLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
        '            trCart.Visible = False
        '            lblNote.Visible = True
        '            Return
        '        End If

        '        Try
        '            Dim myWebRequest As WebRequest
        '            Dim myRequestStream As Stream
        '            Dim myStreamWriter As StreamWriter

        '            Dim myWebResponse As WebResponse
        '            Dim myResponseStream As Stream
        '            Dim myStreamReader As StreamReader

        '            ' Create a new WebRequest which targets that page with
        '            ' which we want to interact.
        '            Dim objPunchOutRequestData As PunchOut.PunchOutRequestData = Nothing
        '            If Not Session("PunchOutRequestData") Is Nothing Then
        '                objPunchOutRequestData = Session("PunchOutRequestData")
        '            Else
        '                If Not Session("PunchOutSesison") Is Nothing Then
        '                    objPunchOutRequestData = CXMLSesisonManager.GetPunchOutRequestData(Session("PunchOutSesison").ToString())
        '                End If
        '            End If

        '            myWebRequest = WebRequest.Create(objPunchOutRequestData.POSTTOURL.Replace("&", "&amp;"))

        '            ' Set the method to "POST" and the content type so the
        '            ' server knows to expect form data in the body of the
        '            ' request.
        '            With myWebRequest
        '                .Method = "POST"
        '                .ContentType = "application/x-www-form-urlencoded"
        '            End With


        '            ' Get a handle on the Stream of data we're sending to
        '            ' the remote server, connect a StreamWriter to it, and
        '            ' write our data to the Stream using the StreamWriter.

        '            myRequestStream = myWebRequest.GetRequestStream()
        '            myStreamWriter = New StreamWriter(myRequestStream)
        '            myStreamWriter.Write(strDataToPost)
        '            myStreamWriter.Flush()
        '            myStreamWriter.Close()
        '            myRequestStream.Close()

        '            ' Get the response from the remote server.
        '            myWebResponse = myWebRequest.GetResponse()

        '            ' Get the server's response status?

        '            ' Just like when we sent the data, we'll get a reference
        '            ' to the response Stream, connect a StreamReader to the
        '            ' Stream and use the reader to actually read the reply.
        '            myResponseStream = myWebResponse.GetResponseStream()
        '            myStreamReader = New StreamReader(myResponseStream)

        '            Dim strResponse As String = myStreamReader.ReadToEnd()
        '            myStreamReader.Close()
        '            myResponseStream.Close()

        '            ' Close the WebResponse
        '            myWebResponse.Close()

        '            'Clear Session
        '            Session.Clear()
        '            Session.Abandon()

        '            'Dim SessionHashObject As Hashtable = Application("punchout")
        '            'CXMLSesisonManager.RemovePunchOutSession(Session.SessionID, SessionHashObject)
        '            Response.Redirect(objPunchOutRequestData.POSTTOURL & "?punchid=" & strResponse, True)
        '            'Response.Redirect("PunchOutSuccess.aspx?punchid=" + strResponse, True)
        '        Catch ex As Exception
        '            Throw ex
        '        End Try
        '    Catch exThrd As Threading.ThreadAbortException
        '        'Do nothing
        '    Catch ex As Exception
        '        CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
        '        '6524 V21 starts
        '        objPCILogger.EventOriginApplication = "ServicesPLUS"
        '        objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
        '        objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
        '        objPCILogger.OperationalUser = Environment.UserName
        '        objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
        '        objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
        '        objPCILogger.CustomerID = customer.CustomerID
        '        objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
        '        'objPCILogger.UserName = customer.UserName
        '        objPCILogger.EmailAddress = customer.EmailAddress '6524
        '        objPCILogger.SIAM_ID = customer.SIAMIdentity
        '        objPCILogger.LDAP_ID = customer.LdapID '6524
        '        objPCILogger.EventOriginMethod = "PunchOut_Click"
        '        objPCILogger.EventType = EventType.Add
        '        '6524 V21 ends
        '        objPCILogger.IndicationSuccessFailure = "Failure" '6524
        '        objPCILogger.Message = "PunchOut_Click Failed. " & ex.Message.ToString() '6524
        '        objPCILogger.PushLogToMSMQ() '6524 V21
        '    Finally
        '        'objPCILogger.PushLogToMSMQ() '6524
        '    End Try
        'End Sub

#Region "    HELPER METHODS    "
        Private Function getThisCart(ByRef thisCustomer As Customer, ByVal sequenceNumber As String) As ShoppingCart
            Dim thisCart As ShoppingCart() = Nothing
            Try
                If Not thisCustomer Is Nothing Then
                    thisCart = New ShoppingCartManager().GetShoppingCarts(HttpContextManager.GlobalData, thisCustomer, True, Convert.ToInt32(sequenceNumber))
                    'For Each thisCart In New ShoppingCartManager().GetShoppingCarts(thisCustomer, True, Convert.ToInt64(sequenceNumber))
                    '    If thisCart.SequenceNumber.ToString() = sequenceNumber.ToString() Then Exit For
                    'Next
                End If
            Catch ex As Exception
                CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
            If Not thisCart Is Nothing Then
                Return thisCart(0)
            Else
                Return Nothing
            End If

        End Function

        Private Function updateSCart() As Integer
            Dim cart As ShoppingCart
            Dim pm As New PaymentManager
            Dim sm As New ShoppingCartManager
            Dim updateCart As Boolean = False
            Dim n As Int32 = 0
            Dim objPCILogger As New PCILogger()

            Try
                If Session.Item("carts") Is Nothing Then
                    CustomerLabel.Text = Resources.Resource.el_EmptyShoppingCard '"Shopping cart is empty."
                    Return -1
                Else
                    cart = Session.Item("carts")
                End If

                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    Dim sQty As String
                    If i.Product.Type = Product.ProductType.ExtendedWarranty Then
                        sQty = "1"
                    Else
                        sQty = (Request.Form("tb" + n.ToString()))
                    End If

                    Dim objNotNaturalPattern As New Regex("[^0-9]")
                    If String.IsNullOrEmpty(sQty) OrElse objNotNaturalPattern.IsMatch(sQty) Then
                        CustomerLabel.Text = Resources.Resource.el_ValidOrderQty '"Please enter a valid number for your order quantity."
                        GetCart()
                        Return -1
                    End If

                    Dim isChecked As String
                    isChecked = (Request.Form("cb" + n.ToString()))

                    If isChecked = "on" Or sQty = "0" Then
                        sm.RemoveItemFromCart(cart, i)
                        updateCart = True
                    Else
                        '-- need to see if quantities are different
                        Dim nQty = CType(sQty, Integer)
                        If i.Quantity <> nQty Then
                            Try
                                i.Quantity = nQty
                                'sm.UpdateShoppingCart(cart)
                                'Session.Item("carts") = cart
                                updateCart = True
                            Catch ex As Exception
                                CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
                                Exit For
                                'i.Quantity = 0
                                'SaveCartButton.Visible = False
                                'CheckOutButton.Visible = False
                                'UpdateButton.Visible = False
                                'Return
                            End Try
                        End If
                    End If
                    n = n + 1
                Next

                If updateCart Then
                    sm.UpdateShoppingCart(cart)
                    Session.Item("carts") = cart
                    'Else
                    '    Return -1
                End If

                GetCart()

                If cart.ShoppingCartItems.Length < 1 Then
                    CustomerLabel.Text = Resources.Resource.el_CurrentcartEmpt '"Your current cart is empty"
                    trCart.Visible = False
                    lblNote.Visible = True
                    Return -1
                End If
            Catch ex As Exception
                CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
                SaveCartButton.Visible = False
                CheckOutButton.Visible = False
                PunchOut.Visible = False
                UpdateButton.Visible = False
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer?.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer?.SequenceNumber
                objPCILogger.EmailAddress = customer?.EmailAddress
                objPCILogger.SIAM_ID = customer?.SIAMIdentity
                objPCILogger.LDAP_ID = customer?.LdapID
                objPCILogger.EventOriginMethod = "updateSCart"
                objPCILogger.EventType = EventType.Update
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "updateSCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
            Return 1
        End Function

        Private Sub GetCart()
            Dim objPCILogger As New PCILogger()
            Dim promoManager As New PromotionManager
            Dim cart As ShoppingCart = Nothing
            Dim sb As New StringBuilder
            Dim sbKit As New StringBuilder
            Dim sbMessagelabel As New StringBuilder()
            Dim TR As New TableRow
            Dim TD As New TableCell
            Dim txtQuantity As Sony.US.ServicesPLUS.Controls.SPSTextBox
            Dim chkDelete As HtmlInputCheckBox ' Was CheckBox
            Dim strDescription As String
            Dim strPartNumber As String
            Dim strOldPartNumber As String
            Dim isFRB As Boolean
            Dim isKit As Boolean
            Dim isWhiteRow As Boolean
            Dim n As Int32 = 0
            Dim hasPromo As Boolean = False
            Dim boolrecycleflag As Boolean = False
            Dim boolshowkitinfo As Boolean = False

            Try
                '2016-05-20 ASleight Adding a check on the Session variable before we go assuming everything is set fine
                If Session.Item("carts") IsNot Nothing Then cart = Session.Item("carts")

                '2016-05-03 ASleight changed "Else" to "OrElse" to prevent null reference exceptions on second part of if statement
                If cart Is Nothing OrElse cart.ShoppingCartItems?.Length < 1 Then
                    CustomerLabel.Text = Resources.Resource.el_EmptyShoppingCard    ' "Shopping cart is empty."
                    Return
                End If

                tblCart.Rows.Clear()

                If cart.ContainsDownloadOnlyItems Then
                    lblSubHdr.InnerText = Resources.Resource.el_Getcart_1 '"Current Cart for Electronically Deliverable Items"
                    CheckOutButton.Visible = True
                    PunchOut.Visible = False
                    ' "This cart contains items for electronic delivery. No items that require physical shipment can be added to this cart..."
                    If customer.IsPunchOutUser Then
                        sbMessagelabel.Append($"<span class=""tableheader""> {Resources.Resource.el_Getcart_2}{Resources.Resource.el_Getcart_3}<br/> ")
                        sbMessagelabel.Append($"{Resources.Resource.el_Getcart_4}{Resources.Resource.el_Getcart_5}</span>.{Environment.NewLine}")
                        MessageLabel.Text = sbMessagelabel.ToString()
                    Else
                        MessageLabel.Text = $"<span class=""tableheader""> {Resources.Resource.el_Getcart_2}{Resources.Resource.el_Getcart_3}{Resources.Resource.el_Getcart_5}</span>.{Environment.NewLine}"
                    End If
                Else
                    lblSubHdr.InnerText = Resources.Resource.el_CurrentShippingitem ' "Current Cart for Shippable Items"
                    ' "This cart contains items for physical shipment. No items that require electronic delivery can be added to this cart..."
                    If customer.IsPunchOutUser Then
                        MessageLabel.Text = $"<span class=""tableheader""> {Resources.Resource.el_Getcart_8}</span>.{Environment.NewLine}"
                    Else
                        MessageLabel.Text = $"<span class=""tableheader""> {Resources.Resource.el_Getcart_9}</span>.{Environment.NewLine}"
                    End If
                End If


                TD.Controls.Add(New LiteralControl("<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""670"">" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=19  width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""75"" bgColor=""#d5dee9"" height=""28"">&nbsp;&nbsp;" & Resources.Resource.el_PartsRequesterd & "</td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""75"" bgColor=""#d5dee9"" height=""28"">&nbsp;&nbsp;" & Resources.Resource.el_PartsSupplied & "</td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""150"" bgColor=""#d5dee9"" height=""28"">&nbsp;&nbsp;" & Resources.Resource.el_Description & "</td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28""><label id=""lblQuantityRequested"">" & Resources.Resource.el_Quantity & "<br/>" & Resources.Resource.el_Requested & "</label> </td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))

                '5068
                If Not cart.ContainsDownloadOnlyItems Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_Quantity & "<br/>" & Resources.Resource.el_Available & "</td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                End If

                'Disply price difference for CA and US site
                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_YourPrice_CAD & "&nbsp;</td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_ListPrice_CAD & "&nbsp;</td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_ExtendPrice_CAD & "&nbsp;</td>" & vbCrLf))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_YourPrice & "&nbsp;</td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_ListPrice & "&nbsp;</td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28"">" & Resources.Resource.el_ExtendPrice & "&nbsp;</td>" & vbCrLf))
                End If

                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9"" height=""28""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""center"" width=""60"" bgColor=""#d5dee9"" height=""28""><label id=""lblRemoveFromCart"">" & Resources.Resource.el_RemovefromCart & "</label></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img height=""28"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan= 19 width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                Me.tdLegend.Visible = False
                Me.tdrecyclefee.Visible = False
                Me.tdshowkitinfo.Visible = False

                For Each cartItem As ShoppingCartItem In cart.ShoppingCartItems
                    isFRB = False
                    isKit = False
                    isWhiteRow = False
                    boolrecycleflag = False
                    boolshowkitinfo = False
                    hasPromo = False

                    txtQuantity = New Sony.US.ServicesPLUS.Controls.SPSTextBox With {
                        .ID = $"tb{n}",
                        .Width = Unit.Pixel(40),
                        .Text = cartItem.Quantity.ToString()
                    }
                    txtQuantity.Attributes.Add("aria-labelledby", "lblQuantityRequested")   ' 2018-07-17 - For WCAG compliance, inputs must be associated with a label

                    chkDelete = New HtmlInputCheckBox With { ' Was "New Checkbox"
                        .ID = $"cb{n}",
                        .Visible = True
                    }
                    chkDelete.Attributes.Add("aria-labelledby", "lblRemoveFromCart")     ' 2018-07-17 - For WCAG compliance, inputs must be associated with a label

                    If cartItem.Product.Type = Product.ProductType.Kit Then
                        strDescription = (" <span class=tableHeader><span class=""redAsterick"">*</span>" & Resources.Resource.el_KIT & " </span> " & cartItem.Product.Description)
                        isKit = True
                        boolshowkitinfo = True '8091
                    ElseIf cartItem.Product.IsFRB Then
                        strDescription = ("<span class=tableHeader><span class=""redAsterick"">*</span>" & Resources.Resource.el_Price_FRB & "</span> " & cartItem.Product.Description)
                        isFRB = True
                    Else
                        strDescription = (cartItem.Product.Description)
                    End If
                    '7814 starts
                    ''        If i.Product.SpecialTax > 0.0 Then
                    ''       strPartNumber = i.Product.PartNumber + ("<span class=tableHeader><span class=""redAsterick"">*</span></span> ")
                    ''      Else
                    'strPartNumber = i.Product.PartNumber
                    '---------------------
                    'If i.Product.IsReplaced Then
                    strOldPartNumber = cartItem.Product.PartNumber
                    'Else
                    '    strOldPartNumber = "-"
                    'End If
                    ' Due to Shopping Cart update, we now expect a Replacement Part number. Since a Saved Cart does not store this, we copy it from original Part Number
                    If (String.IsNullOrEmpty(cartItem.Product.ReplacementPartNumber)) Then cartItem.Product.ReplacementPartNumber = cartItem.Product.PartNumber
                    strPartNumber = cartItem.Product.ReplacementPartNumber

                    If cartItem.Product.SpecialTax > 0.0R Then
                        boolrecycleflag = True
                    Else
                        lblNote.Text = Resources.Resource.el_ApplicableShippingFee ' "The subtotal does not include any applicable shipping fees or tax."
                    End If

                    '-----------------------
                    'Dim recycleflag As Double = 0
                    'Dim catMng As New CatalogManager
                    'recycleflag = catMng.ISRecyclingFlagCheck(i.Product.PartNumber, customer)
                    'If recycleflag > 0 Then
                    '    strPartNumber = i.Product.PartNumber + ("<span class=tableHeader><span class=""redAsterick"">*</span></span> ")
                    '    boolrecycleflag = True
                    'Else
                    '    strPartNumber = i.Product.PartNumber
                    '    lblNote.Text = "The subtotal does not include any applicable shipping fees or tax."
                    'End If
                    '7814 ends
                    TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                    ' ASleight - Added the following two columns for "Replaced Part" logic.
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '2 Old Product Number - If the user searched or added a specific part number, store it here. Otherwise, it will be equal to Replaced.
                    TD.Controls.Add(New LiteralControl("<td align=""center"" class=""tableData"">" & strOldPartNumber & "</td>" & vbCrLf))
                    '3
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '4 Product Number
                    Dim displayPartNum = strPartNumber
                    If boolrecycleflag Then displayPartNum &= "<span class=tableHeader><span class=""redAsterick"">*</span></span>"
                    TD.Controls.Add(New LiteralControl("<td align=""center"" class=""tableHeader"">" & displayPartNum & "</td>" & vbCrLf))
                    '5 
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '6 Product Description
                    TD.Controls.Add(New LiteralControl("<td align=""center"" class=""tableData"">" & strDescription & "</td>" & vbCrLf))
                    '7
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '8 Quantity TextBox
                    TD.Controls.Add(New LiteralControl("<td align=""center"" class=""tableData"">" & vbCrLf))
                    txtQuantity.Enabled = (cartItem.Product.Type <> Product.ProductType.ExtendedWarranty)
                    TD.Controls.Add(txtQuantity)
                    TD.Controls.Add(New LiteralControl("</td>" & vbCrLf))

                    '<input type=""text"" name=""textfield=" + n.ToString + """ size=""3"" value=""" + i.Quantity.ToString() + """ /></td>"))
                    '9
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))

                    ' Quantity Available
                    Dim nAvailToShow As String = "0"
                    If isKit Then
                        nAvailToShow = String.Empty
                    Else
                        If cartItem.Quantity <= cartItem.NumberOfPartsAvailable Then
                            nAvailToShow = cartItem.Quantity
                        Else
                            nAvailToShow = cartItem.NumberOfPartsAvailable
                        End If
                    End If

                    If Not cart.ContainsDownloadOnlyItems Then
                        '10 Quantity Available
                        TD.Controls.Add(New LiteralControl($"<td align=""center"" class=""tableData"">{nAvailToShow}</td>{Environment.NewLine}"))
                        '11
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    End If

                    '12 Your Price
                    'If isUserActive(customer) Then
                    If customer.IsActive Then
                        TD.Controls.Add(New LiteralControl("<td class=""tableData"" align=""right"">"))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.YourPrice)).ToString()))
                        If promoManager.IsPromotionPart(strPartNumber) Then
                            TD.Controls.Add(New LiteralControl("<span style=""font-family: Wingdings; color: red; font-size: 12px;"">v</span>"))
                            hasPromo = True
                        End If
                        TD.Controls.Add(New LiteralControl("&nbsp;</td>" & vbCrLf))
                    Else
                        TD.Controls.Add(New LiteralControl("<td class=""tableData"" align=""right"">"))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.ListPrice)).ToString()))
                        TD.Controls.Add(New LiteralControl("&nbsp;</td>" & vbCrLf))
                    End If
                    '13
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '14 List Price
                    TD.Controls.Add(New LiteralControl("<td class=""tableData"" align=""right"">"))
                    TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.ListPrice)).ToString()))
                    TD.Controls.Add(New LiteralControl("&nbsp;</td>" & vbCrLf))


                    '13
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '14 Total
                    TD.Controls.Add(New LiteralControl("<td class=""tableData"" align=""right"">"))
                    TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.TotalLineItemPrice)).ToString()))
                    TD.Controls.Add(New LiteralControl("&nbsp;</td>" & vbCrLf))
                    '15
                    TD.Controls.Add(New LiteralControl("<td width=""1"" bgcolor=""#d5dee9""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    '16 Delete CheckBox
                    TD.Controls.Add(New LiteralControl("<td class=""tableData"" align=""center"">"))
                    TD.Controls.Add(chkDelete)
                    TD.Controls.Add(New LiteralControl("</td>" & vbCrLf))
                    '17
                    TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" & vbCrLf))
                    TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                    If cartItem.Product.Type = Product.ProductType.ExtendedWarranty Then
                        'TD.Controls.Add(New LiteralControl("<td width=""160"" class=""tableData"">&nbsp;&nbsp;" + strDescription + "</br>Date Purchased: " + CType(i.Product.SoftwareItem, ExtendedWarrantyModel).EWPurchaseDate.Date.ToString("mm/dd/yyyy") + "</br>Serial Number: " + CType(i.Product.SoftwareItem, ExtendedWarrantyModel).EWSerialNumber + "</br>Model Number: " + CType(i.Product.SoftwareItem, ExtendedWarrantyModel).EWModelNumber + "</td>"))
                        TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""><span class=""popupCopyBold""><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        If Not cartItem.Product.SoftwareItem Is Nothing Then
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Model: </span>" & CType(cartItem.Product.SoftwareItem, ExtendedWarrantyModel).EWModelNumber & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Purchased On: </span>" & CType(cartItem.Product.SoftwareItem, ExtendedWarrantyModel).EWPurchaseDate.Date.ToString("MM/dd/yyyy") & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Serial: </span>" & CType(cartItem.Product.SoftwareItem, ExtendedWarrantyModel).EWSerialNumber & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        Else
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""8"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Model: </span>" & CType(cartItem.Product, ExtendedWarrantyModel).EWModelNumber & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""3"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Purchased On: </span>" & CType(cartItem.Product, ExtendedWarrantyModel).EWPurchaseDate.Date.ToString("MM/dd/yyyy") & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""Left""><span class=""tabledata""><span class=""tableHeader"">Serial: </span>" & CType(cartItem.Product, ExtendedWarrantyModel).EWSerialNumber & "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        End If

                        'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""15""><img height=""1"" src=""images/spacer.gif"" width=""20""><span class=""popupCopyBold""><img height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                        TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))
                    End If

                    '*******************************************************

                    If (isFRB) Then
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=19 bgcolor=""#d5dee9""><span class=""legalCopy""><span class=""redAsterick"">*</span>" & Resources.Resource.el_Price_FRB & Resources.Resource.el_Price_FRB1))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.CoreCharge)).ToString()))
                        TD.Controls.Add(New LiteralControl(Resources.Resource.el_Price_FRB2))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.CoreCharge)).ToString() & " " & Resources.Resource.el_Price_FRB3 & "<br></td></tr>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=19><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">" & vbCrLf))
                    ElseIf (cartItem.Product.HasCoreCharge) Then
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=19 bgcolor=""#d5dee9""><span class=""legalCopy""><span class=""redAsterick"">*</span>" & Resources.Resource.el_Price_FRB1))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.CoreCharge)).ToString() & Resources.Resource.el_Price_FRB2))
                        TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cartItem.Product.CoreCharge)).ToString() & " " & Resources.Resource.el_Price_FRB3 & "<br></td></tr>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=19><img height=""1"" src=""images/spacer.gif"" width=""20"" alt="""">" & vbCrLf))
                    End If

                    If (isKit) Then
                        Dim k As Sony.US.ServicesPLUS.Core.Kit 'In kits
                        k = cartItem.Product
                        TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                        'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2""><img height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">" + x.Product.PartNumber + "<img height=""1"" src=""images/spacer.gif"" width=""10""></td>"))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5""><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""><span class=""popupCopyBold"">" & Resources.Resource.el_KitspartDesc & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""><span class=""popupCopyBold"">" & Resources.Resource.el_KitQty & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""><span class=""popupCopyBold"">" & Resources.Resource.el_QtyAval & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                        'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">List Price<img height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))'7412

                        'price difference for CA and US 
                        If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">" & Resources.Resource.el_extndListPrice_CAD & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf)) '7412
                        Else
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><span class=""popupCopyBold"">" & Resources.Resource.el_extndListPrice & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf)) '7412
                        End If

                        TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))
                        TD.Controls.Add(New LiteralControl("<tr><td colspan=19><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""></tr>" & vbCrLf))
                        For Each x As Sony.US.ServicesPLUS.Core.KitItem In k.Items
                            '7412 starts
                            ''Kit  Quantity(Available)
                            'Dim nKitAvailToShow As Integer
                            'If x.Quantity <= x.NumberOfPartsAvailable Then
                            '    nKitAvailToShow = x.Quantity
                            'Else
                            '    nKitAvailToShow = x.NumberOfPartsAvailable
                            'End If
                            'Kit  Quantity(Available)
                            Dim nKitAvailToShow As Integer
                            Dim showtotalqty As Integer = cartItem.Quantity * x.Quantity

                            If x.NumberOfPartsAvailable >= showtotalqty Then
                                nKitAvailToShow = showtotalqty
                            Else
                                nKitAvailToShow = x.NumberOfPartsAvailable
                            End If
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                            'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2""><img height=""1"" src=""images/spacer.gif"" width=""20""><span class=""legalCopy"">" + x.Product.PartNumber + "<img height=""1"" src=""images/spacer.gif"" width=""10""></td>"))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5""><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""><span class=""legalCopy"">" & x.Product.Description & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            '7412 starts
                            'TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><img height=""1"" src=""images/spacer.gif"" width=""30""><span class=""legalCopy"">" + x.Quantity.ToString + "<img height=""1"" src=""images/spacer.gif"" width=""10""></span></td>"))
                            Dim ntotalqty As Integer = cartItem.Quantity * x.Quantity
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4""><img height=""1"" src=""images/spacer.gif"" width=""30"" alt=""""><span class=""legalCopy"">" & ntotalqty.ToString & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""4"" align=""center""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""><span class=""legalCopy"">" & nKitAvailToShow.ToString & "<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""5"" align=""right""><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""><span class=""legalCopy"">" & vbCrLf))
                            '7412 starts
                            'TD.Controls.Add(New LiteralControl((String.Format("{0:c}", x.Product.ListPrice)).ToString()))'7412
                            Dim qtyreq_ELP, totqty_ELP As Double
                            qtyreq_ELP = Convert.ToDouble(ntotalqty)
                            totqty_ELP = x.Product.ListPrice * qtyreq_ELP
                            TD.Controls.Add(New LiteralControl((String.Format("{0:c}", totqty_ELP)).ToString()))
                            '7412 ends
                            TD.Controls.Add(New LiteralControl("<img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span></td>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))
                            TD.Controls.Add(New LiteralControl("<tr><td colspan=19><img height=""1"" src=""images/spacer.gif"" width=""20"" alt=""""></tr>" & vbCrLf))
                        Next
                    End If

                    n = n + 1
                Next

                ' -- display legend only if there is at least one part with promotion
                If hasPromo = True Then
                    Me.tdLegend.Visible = True
                End If
                '7814 starts
                If boolrecycleflag = True Then
                    Me.tdrecyclefee.Visible = True
                    lblNote.Text = Resources.Resource.el_SubtotalInfo '"The subtotal does not include any applicable shipping fees, tax, or recycle fees."
                End If
                '7814 ends
                '8091 starts
                If boolshowkitinfo = True Then
                    Me.tdshowkitinfo.Visible = True
                End If
                '8091 ends
                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""19"" width=""100%"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""13"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))

                'price difference for CA and US 
                If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" bgColor=""#d5dee9"">" & Resources.Resource.el_SubTotal_CAD & "<img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                Else
                    TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" bgColor=""#d5dee9"">" & Resources.Resource.el_SubTotal & "<img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                End If

                TD.Controls.Add(New LiteralControl("<td width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td class=""tableHeader"" align=""right"" bgColor=""#d5dee9"" colspan=""2"">"))
                TD.Controls.Add(New LiteralControl((String.Format("{0:c}", cart.ShoppingCartTotalPrice)).ToString() & "<img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""2"" width=""1"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" width=""1"" alt=""""></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                TD.Controls.Add(New LiteralControl("<tr>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("<td colspan=""19"" bgColor=""#d5dee9""><img height=""1"" src=""images/spacer.gif"" alt="""" ></td>" & vbCrLf))
                TD.Controls.Add(New LiteralControl("</tr>" & vbCrLf))

                TD.Controls.Add(New LiteralControl("</table>" & vbCrLf))

                TR.Cells.Add(TD)
                tblCart.Rows.Add(TR)
            Catch ex As Exception
                CustomerLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer?.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer?.SequenceNumber
                objPCILogger.EmailAddress = customer?.EmailAddress
                objPCILogger.SIAM_ID = customer?.SIAMIdentity
                objPCILogger.LDAP_ID = customer?.LdapID
                objPCILogger.EventOriginMethod = "GetCart"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "GetCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub
#End Region
    End Class

End Namespace
