Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports System.Text
Imports ServicesPlusException
Imports Sony.US.AuditLog

Namespace ServicePLUSWebApp



    Partial Class CustomerSavedCarts
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        '--
        '--
        Protected WithEvents errrorMessageLabel As System.Web.UI.WebControls.Label



        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.

            isProtectedPage(True)
            InitializeComponent()

        End Sub

#End Region

#Region "Page members"

        Private customer As Customer
        Private txtPageNumber As New Sony.US.ServicesPLUS.Controls.SPSTextBox()
        Private lblPageNumberError As New Label()
        Private btnGoTo As New ImageButton()
        Private Const conViewCartPage As String = "vc.aspx"
        Private blnShowPagingText As Boolean = True
        'Dim objUtilties As Utilities = New Utilities

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblPageNumberError.Text = String.Empty
                lblPageNumberError.CssClass = "redAsterick"
                If HttpContextManager.Customer IsNot Nothing Then customer = HttpContextManager.Customer
                If Not IsPostBack Then
                    ViewState.Add("sortorder", "DESC")
                    ViewState.Add("sortexp", "DATECREATED")
                    processPage(sender, e)
                End If

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "Grid Command"

        Protected Sub dgCarts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCarts.ItemCommand
            Try
                If e.CommandArgument = "GotoPage" Then
                    If IsNumeric(txtPageNumber.Text.ToString()) = False Then
                        lblPageNumberError.Text = "    " + Resources.Resource.el_NumericPageNo 'Page number should be numeric."
                    Else
                        Dim iPageNumber As Int16 = Convert.ToInt16(txtPageNumber.Text.ToString()) - 1
                        If (iPageNumber + 1 > dgCarts.PageCount) Or iPageNumber < 0 Then
                            lblPageNumberError.Text = "    " + Resources.Resource.el_EnterPageNo + " " + dgCarts.PageCount.ToString() + "."
                        Else
                            dgCarts.CurrentPageIndex = iPageNumber
                            txtPageNumber.Text = String.Empty
                            DisplayResultTables()
                            dgCarts.DataBind()
                        End If
                    End If
                    'ElseIf e.CommandArgument = "OpenCart" Then
                    '    Session.Add("CartNumber", e.Item.Cells(0).Text.ToString())
                    '    Try
                    '        If Not customer Is Nothing Then
                    '            Dim thisCart As ShoppingCart

                    '            thisCart = getThisCart(customer, e.Item.Cells(0).Text.ToString())

                    '            If Not thisCart Is Nothing Then
                    '                If Not Session.Item("carts") Is Nothing Then
                    '                    Session.Item("carts") = thisCart
                    '                Else
                    '                    Session.Add("carts", thisCart)
                    '                End If
                    '            End If

                    '            Try
                    '                If Not Session.Item("carts") Is Nothing Then Response.Redirect(conViewCartPage, True)
                    '            Catch ex As System.Threading.ThreadAbortException
                    '            End Try
                    '        End If
                    '    Catch ex As Exception
                    '        errorMessageLabel.Text = "Error redirecting user."
                    '    End Try
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub dgCarts_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCarts.ItemCreated
            Select Case e.Item.ItemType
                Case ListItemType.Pager
                    If blnShowPagingText Then

                        Dim objDataGridItem As DataGridItem
                        Dim oHTMLTD As New HtmlTableCell("pagingtext")
                        Dim oBlankTD As New HtmlTableCell("blankspace")
                        Dim lblHtmlLabel As New Label
                        lblHtmlLabel.Text = Resources.Resource.el_PageNo '"Page Number:"
                        lblHtmlLabel.CssClass = "BodyCopy"
                        oHTMLTD.Width = "550"
                        txtPageNumber.ID = "txtPageNumber"
                        txtPageNumber.CssClass = "BodyCopy"
                        txtPageNumber.Width = Unit.Pixel(25)
                        txtPageNumber.Height = Unit.Pixel(17)

                        btnGoTo.ID = "btnGoTo"
                        btnGoTo.ImageUrl = "images/sp_int_rightArrow_btn.gif"
                        btnGoTo.CommandArgument = "GotoPage"
                        objDataGridItem = e.Item
                        Dim htmlImg As New HtmlImage()
                        htmlImg.Src = "images/spacer.gif"
                        htmlImg.Width = "10"
                        oBlankTD.Controls.Add(htmlImg)
                        oHTMLTD.Controls.Add(oBlankTD)
                        oHTMLTD.Controls.Add(lblHtmlLabel)
                        oHTMLTD.Controls.Add(txtPageNumber)
                        oHTMLTD.Controls.Add(btnGoTo)
                        objDataGridItem.Controls(0).Controls.Add(oHTMLTD)
                        oHTMLTD.Controls.Add(lblPageNumberError)
                    End If
                Case ListItemType.AlternatingItem
                Case ListItemType.Footer
                Case ListItemType.Header
                Case Else
                    Exit Select
            End Select
        End Sub

        Protected Sub dgCarts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCarts.ItemDataBound
            Select Case e.Item.ItemType
                Case ListItemType.Pager
                    Dim objDataGridItem As DataGridItem
                    objDataGridItem = e.Item

                Case ListItemType.AlternatingItem, ListItemType.Item
                    If e.Item.Cells(1).Text = "Ship" Then
                        e.Item.Cells(1).Text = "Shipment"
                    ElseIf e.Item.Cells(1).Text = "Download" Then
                        e.Item.Cells(1).Text = "Electronic Delivery"
                    End If
                Case ListItemType.Footer
                Case ListItemType.Header
                Case Else
                    Exit Select
            End Select
        End Sub

        Protected Sub dgCarts_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgCarts.PageIndexChanged
            Dim tmpDataSet As DataSet
            tmpDataSet = dgCarts.DataSource
            dgCarts.CurrentPageIndex = e.NewPageIndex
            If tmpDataSet Is Nothing Then
                DisplayResultTables(False, ViewState("sortexp").ToString())
            End If
            dgCarts.DataBind()
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            txtPageNumber.Attributes("onkeydown") = "SetTheFocusButton(event, '" + btnGoTo.ClientID + "')"
        End Sub

        Protected Sub dgCarts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCarts.SortCommand
            If e.SortExpression <> ViewState("sortexp").ToString() Then ViewState("sortorder") = "ASC"
            ViewState.Add("sortexp", e.SortExpression)
            DisplayResultTables(True, e.SortExpression)
        End Sub

#End Region

#Region "Display table"
        Private Sub processPage(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                DisplayResultTables(True, ViewState("sortexp").ToString())
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub DisplayResultTables(Optional ByVal DoSort As Boolean = False, Optional ByVal SortExp As String = "")
            If HttpContextManager.Customer IsNot Nothing Then
                customer = HttpContextManager.Customer
                BindOrdersGrid(New ShoppingCartManager().GetSavedCarts(customer.CustomerID), DoSort, SortExp)
            End If
        End Sub
#End Region

#Region "Build Orders Table"
        Private Sub BindOrdersGrid(ByVal customercarts As DataSet, Optional ByVal DoSort As Boolean = False, Optional ByVal SortExp As String = "")
            Dim dView As DataView
            If Not customercarts Is Nothing Then
                If customercarts.Tables.Count > 0 Then
                    If customercarts.Tables(0).Rows.Count <= dgCarts.PageSize Then blnShowPagingText = False
                    If DoSort = False Then
                        dView = customercarts.Tables(0).DefaultView
                        dView.Sort = ViewState("sortexp") + " " + IIf(ViewState("sortorder").ToString() = "DESC", "ASC", "DESC")
                        ApplySortingArrow(dView.Sort)
                        dgCarts.DataSource = dView
                        dgCarts.DataBind()
                    Else
                        dView = customercarts.Tables(0).DefaultView
                        dView.Sort = SortExp + " " + ViewState("sortorder").ToString()
                        ApplySortingArrow(dView.Sort)
                        dgCarts.DataSource = dView
                        dgCarts.DataBind()
                        ViewState("sortorder") = IIf(ViewState("sortorder").ToString() = "DESC", "ASC", "DESC")
                    End If
                Else
                    errorMessageLabel.Text = Resources.Resource.el_Savecart '"You do not have saved cart."
                End If
            Else
                errorMessageLabel.Text = Resources.Resource.el_Savecart '"You do not have saved cart."
            End If


        End Sub

        Private Sub ApplySortingArrow(ByVal SortingValue As String)
            dgCarts.Columns(0).HeaderStyle.CssClass = "tableHeader"
            dgCarts.Columns(1).HeaderStyle.CssClass = "tableHeader"
            dgCarts.Columns(2).HeaderStyle.CssClass = "tableHeader"
            Select Case SortingValue
                Case "SEQUENCENUMBER ASC"
                    dgCarts.Columns(0).HeaderStyle.CssClass = "sortedASC"
                    Exit Select
                Case "SEQUENCENUMBER DESC"
                    dgCarts.Columns(0).HeaderStyle.CssClass = "sortedDESC"
                    Exit Select
                Case "CartType ASC"
                    dgCarts.Columns(1).HeaderStyle.CssClass = "sortedASC"
                    Exit Select
                Case "CartType DESC"
                    dgCarts.Columns(1).HeaderStyle.CssClass = "sortedDESC"
                    Exit Select
                Case "DATECREATED ASC"
                    dgCarts.Columns(2).HeaderStyle.CssClass = "sortedASC"
                    Exit Select
                Case "DATECREATED DESC"
                    dgCarts.Columns(2).HeaderStyle.CssClass = "sortedDESC"
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End Sub

#End Region

        Private Function getThisCart(ByRef thisCustomer As Customer, ByVal sequenceNumber As String, ByVal objGlobalData As GlobalData) As ShoppingCart
            Dim thisCart As ShoppingCart = Nothing
            Dim objPCILogger As New PCILogger() '6524
            Try
                If thisCustomer IsNot Nothing Then
                    For Each thisCart In New ShoppingCartManager().GetShoppingCarts(objGlobalData, thisCustomer, True)
                        If thisCart.SequenceNumber.ToString() = sequenceNumber.ToString() Then Exit For
                    Next
                End If
            Catch ex As Exception
                '6524 V21 starts
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = thisCustomer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = thiscustomer.SequenceNumber
                'objPCILogger.UserName = thisCustomer.UserName
                objPCILogger.EmailAddress = thisCustomer.EmailAddress '6524
                objPCILogger.SIAM_ID = thisCustomer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventOriginMethod = "getThisCart"
                objPCILogger.EventType = EventType.View
                '6524 V21 ends
                objPCILogger.IndicationSuccessFailure = "Failure" '6524
                objPCILogger.Message = "getThisCart Failed. " & ex.Message.ToString() '6524
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.PushLogToMSMQ() '6524 V21
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            Return thisCart
        End Function

    End Class

End Namespace
