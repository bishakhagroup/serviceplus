Imports Sony.US.SIAM
Imports Sony.US.ServicesPLUS
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException

Namespace ServicePLUSWebApp



    Partial Class PunchOutSignIn
        Inherits SSL

        'Private handler As New ConfigHandler("SOFTWARE\SERVICESPLUS")
        'Private config_settings As Hashtable
        Public maxPwdLength As Int16 = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount)
        Public minPwdLength As Int16 = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength)
        Private customer As Customer
        Private strPunchOutSessioID As String = String.Empty
        'Dim objUtilties As Utilities = New Utilities

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            'config_settings = handler.GetConfigSettings("//item[@name='SecurityManagement']")
            'maxPwdLength = Integer.Parse("0" + config_settings.Item("MaxPwdLength").ToString())
            'minPwdLength = Integer.Parse("0" + config_settings.Item("MinPwdLength").ToString())

        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If (Not Request.QueryString("SessionID") Is Nothing) Then
                    'Get the SessionId and check into Database if thie SessionId exist for the PunchOut
                    If ("PunchOut SesisonID exist in database".Length > 0) Then
                        Session.Add("IsPunchOut", "True")
                        strPunchOutSessioID = Request.QueryString("SessionID")
                        Application.Add(strPunchOutSessioID, Date.Now)
                        Session.Add("PunchOutSessionID", strPunchOutSessioID)
                        frgtPassLink.HRef = "SignInForgotPassword.aspx?SessionID=" + strPunchOutSessioID
                        'Get Next Q from Database with this sessionid and associated customerid/identity

                    Else
                        'Session Expire or Incorrect URL
                        'Redirect to the Error page 
                        'Generate a new PunchOut error page showing URL incorrect
                        'Message to re-request for punchout
                    End If
                Else
                    'Normal Login
                    'Response.Redirect("SignIn.aspx", True)
                    Response.Redirect("SignIn-Register.aspx", True)
                End If

                'If Session Already exist for customer then show member page
                If Not Session("customer") Is Nothing Then Response.Redirect("Member.aspx", True)

                If Not Request.QueryString("f") Is Nothing Then
                    UName.Text = Request.QueryString("f")
                    Exit Sub
                End If

                If Not Me.IsPostBack Then
                    Session.Remove("SecurityTrys")
                    Dim strKitPartNumber = Request.QueryString("PartNumber")
                    If strKitPartNumber IsNot Nothing Then
                        Dim dm As SoftwarePlusDataManager = New SoftwarePlusDataManager
                        Dim sw As SelectedSoftware = dm.SearchSoftwareByID(strKitPartNumber)
                        If (sw IsNot Nothing) AndAlso (sw.FreeOfCharge And sw.Tracking) Then 'only add those that is free and need to login
                            Session("SelectedFreeSoftwareBeforeLoggedIn") = sw
                        End If
                    End If

                Else
                    '-- check if the b2b account is locked due to attempt with forgot password
                    Dim tries As Integer = 0
                    If Not Session.Item("SecurityTrys") Is Nothing Then
                        tries = Session.Item("SecurityTrys")
                        Dim obj() As Object = Session.Item("ForgotPWStructure")
                        Dim isLocked As Boolean = False
                        If Not obj Is Nothing Then
                            If UName.Text = CType(obj(0), Sony.US.siam.User).UserId Then
                                isLocked = True
                            End If
                        End If
                        Dim TrysAllowed As Integer
                        TrysAllowed = Integer.Parse(0 + Application("InvalidLoginAttempsAllowed").ToString())

                        If tries >= TrysAllowed And isLocked Then
                            Dim sa As Sony.US.siam.SecurityAdministrator = New Sony.US.siam.SecurityAdministrator
                            ' sa.LockAccount(CType(obj(0), Sony.US.siam.User))
                            Session.Remove("SecurityTrys")
                            Try
                                Response.Redirect("SignInForgotPassword.aspx")
                            Catch ex As Threading.ThreadAbortException
                                '-- do nothing
                            End Try
                        End If
                    End If
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function DoCallback() As Boolean
            If Not Request.QueryString("callback") Is Nothing Then
                Dim param As String = Request.QueryString("param")
                Session.Add("RegisterReturnPage", param)
                Response.Flush()
                Response.End()
                Return True
            End If
            Session("RegisterReturnPage") = Nothing
            Return False
        End Function

        Private Sub Submit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Submit.Click
            'Dim blnActiveUSer As Boolean
            Try
                '-- clear previous errors
                lnkcontact.Visible = False
                TechError.Visible = False

                '-- lets clear the session variables --
                If Not Session.Item("carts") Is Nothing Then Session.Remove("carts")
                If Not Session.Item("order") Is Nothing Then Session.Remove("order")
                If Not Session.Item("customer") Is Nothing Then Session.Remove("customer")

                Dim sm As New SecurityManager
                Dim c As New Sony.US.siam.Credential
                Dim errorMessage As String

                c.UserName = UName.Text
                c.Password = Password.Text

                If c.UserName.Trim() = "" Or c.Password.Trim() = "" Then
                    Me.ErrorLabel.Text = "Please enter user name and password to login"
                    Return
                End If

                Dim ar As Sony.US.siam.AuthenticationResult = sm.Authenticate(c)


                Dim cm As New Sony.US.ServicesPLUS.Process.CustomerManager

                Dim isUserLocked As Integer = False

                Try
                    isUserLocked = ar.user.LockStatus


                    Return


                    Return
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    Return
                End Try

                '--- check if the account is locked
                If isUserLocked = 1 Then
                    errorMessage = "Your account has been locked.  To unlock your account, please enter and submit the information below."
                    Response.Redirect("SignInForgotPassword.aspx?errMessage=" + errorMessage)
                    Response.End()
                End If

                If (ar.Result) Then
                    customer = cm.GetCustomer(ar.user.Identity, ar.user.EmailAddress, ar.user.FirstName, ar.user.LastName)
                    'Added by chandra on 23rd July 2007 
                    If (Not customer Is Nothing) Then
                        customer.UserName = c.UserName
                        customer.Password = c.Password
                    Else
                        ErrorLabel.Text = "Information mismatch; please call 1-800-538-7550 for assistance."
                        Exit Sub
                    End If

                    If customer.IsActive = False Then
                        ErrorLabel.Text = "Your account is inactive; please call 1-800-538-7550 for assistance."
                        Exit Sub
                        'ElseIf (ar.user.UserType = "AccountHolder" And Not ar.user.AlternateUserId Is Nothing) Then'6668
                    ElseIf ((ar.user.UserType = "A" Or ar.user.UserType = "P") And Not ar.user.AlternateUserId Is Nothing) Then '6668
                        ' Prompt for third id.
                        Try
                            Dim responseurl As String = "SignInThirdId.aspx"
                            Dim strParamaters As String = String.Empty
                            strParamaters = ResolveURLParameter()
                            responseurl = responseurl + IIf(strParamaters = String.Empty, String.Empty, "?" + strParamaters)
                            If Not ar.user Is Nothing Then
                                QuestionLabel.Text = ar.user.ThirdID.NextQuestion.Replace("Mothers", "Mother's")
                                If isDateQuestion(ar.user.ThirdID.NextQuestion) Then DateFormatLabel.Text = "MM/DD/YYYY"
                                If dataValidated() Then
                                    If checkID(ar.user) Then
                                        Dim IsValidTBSubscription As Boolean
                                        If Not customer.SubscriptionID = "" Then
                                            IsValidTBSubscription = cm.IsValidSubscriptionID(customer.CustomerID)
                                        Else
                                            IsValidTBSubscription = False
                                        End If
                                        Session.Add("IsValidSubscription", IsValidTBSubscription)
                                    Else

                                    End If
                                End If
                            End If
                        Catch ex As Threading.ThreadAbortException
                            'Do nothing
                        End Try
                    Else
                        ErrorLabel.Text = "Invalid account information."
                        Exit Sub
                    End If

                    cm.UpdateLoginTime(customer.CustomerID)
                    cm = Nothing
                    customer = Nothing
                    CloseAndRedirect()
                Else
                    Select Case ar.SiamResponseMessage.ToString()
                        Case Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString()
                            errorMessage = "The user name or password you entered was invalid.<br/>Please try again."
                        Case Messages.AUTHENTICATE_FAILURE_BADUSERID.ToString()
                            errorMessage = "The user name or password you entered was invalid.<br/>Please try again."
                        Case Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString()
                            ' when the account is locked, the behaviour needs to be as in default page E102
                            errorMessage = "Your account has been locked.  To unlock your account, please enter and submit the information below."
                            Response.Redirect("SignInForgotPassword.aspx?errMessage=" + errorMessage)
                        Case Else
                            errorMessage = "An error was encountered. Please try again, or "
                            lnkcontact.Visible = True
                            TechError.Visible = True
                            If ar.SiamInnerResponseMessage.ToString() <> String.Empty Then TechError.Text = "[" + ar.SiamInnerResponseMessage.ToString() + "]"
                    End Select
                    ErrorLabel.Text = errorMessage
                End If
            Catch thrdEX As Threading.ThreadAbortException




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub
        Private Function checkID(ByRef user As Sony.US.siam.User) As Boolean
            Try
                Dim result As Integer = New SecurityAdministrator().EvaluateThirdId(user, DOB.Text)
                '-- compare result to result return messages enum

                If result = CType(Messages.AUTHENTICATE_SUCCESS, Integer) Then
                    Dim custManager As CustomerManager = New CustomerManager
                    customer = Session("processingcustomer")
                    Session.Add("customer", customer)

                    If Not custManager.CheckPasswordValidity(user.Password, user.EmailAddress, Me.lblPrompt, user.UserType) Then
                        divPromptPassword.Visible = True
                        divLogin.Visible = False

                        If customer.IsActive Then lblSecurityQuestion.Text = getNextSecurityQuestion(customer)
                        Session.Add("LoginUser", user)
                        Return True
                    End If
                Else
                    Select Case (result)
                        Case CType(Messages.AUTHENTICATE_FAILURE_BADPASSWORD, Integer)
                            ErrorLabel.Text = Messages.AUTHENTICATE_FAILURE_BADPASSWORD.ToString()
                        Case CType(Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED, Integer)
                            '-- redirect to forgot password
                            Try
                                Response.Redirect("SignInForgotPassword.aspx?errMessage=" + Messages.AUTHENTICATE_FAILURE_ACCOUNTLOCKED.ToString())
                                Response.End()
                            Catch ex As Threading.ThreadAbortException
                                '-- do nothing, expected for redirect            
                            End Try
                        Case Else
                            ErrorLabel.Text = "Error Validating the account"
                    End Select
                    Return False
                End If


                Return False


                Return False
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return False
            End Try
        End Function

        Private Function getNextSecurityQuestion(ByRef customer As Customer) As String
            Dim returnValue As String

            Try
                If Not customer Is Nothing Then
                    Dim thisSA As New SecurityAdministrator
                    Dim thisUser As User = thisSA.GetUser(customer.SIAMIdentity).TheUser

                    If Not thisUser Is Nothing Then
                        Dim thisSM As New SecurityManager
                        Dim thisCustomersCredentials As Credential
                        Dim thisAuthorizationResponse As New AuthenticationResult

                        thisCustomersCredentials.UserName = thisUser.UserId.ToString()
                        thisCustomersCredentials.Password = thisUser.Password.ToString()

                        thisAuthorizationResponse = thisSM.Authenticate(thisCustomersCredentials)

                        If Not thisAuthorizationResponse Is Nothing Then
                            returnValue = thisAuthorizationResponse.user.ThirdID.NextQuestion.ToString() + ":"
                            ViewState.Add("SA", Sony.US.SIAMUtilities.Encryption.DeCrypt(thisAuthorizationResponse.user.ThirdID.UserSecurityPINs(thisAuthorizationResponse.user.ThirdID.NextQuestionIndex), "test key"))
                            ViewState.Add("SQ", returnValue.ToString())
                        End If
                    End If
                Else
                    lblPrompt.Text = "Unable to get security question, customer does not exist."
                End If
            Catch ex As Exception
                lblPrompt.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

        Private Function isDateQuestion(ByVal NextQuestion As String) As Boolean
            Dim returnValue As Boolean = False

            If NextQuestion.ToLower() = "date of birth" Then returnValue = True

            Return returnValue
        End Function

        Private Function dataValidated() As Boolean
            Dim returnValue As Boolean = False
            Try
                If isDateQuestion(QuestionLabel.Text.ToString()) Then
                    Dim isDateValid As Boolean = True

                    If IsDate(DOB.Text.ToString()) Then
                        Dim thisPattern As String = "^(\d{2}\/\d{2}\/\d{4}$)"
                        If Not Regex.IsMatch(DOB.Text.ToString(), thisPattern) Then
                            isDateValid = False
                        End If
                    Else
                        isDateValid = False
                    End If

                    If Not isDateValid Then
                        ErrorLabel.Text = "Date of birth is not formatted correctly."
                    End If

                    If isDateValid Then returnValue = True
                Else
                    If DOB.Text.Length > 0 Then
                        returnValue = True
                    Else
                        ErrorLabel.Text = "Please answer security question."
                    End If
                End If




            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return returnValue
        End Function

#Region "Close and Redirect"

        Private Sub CloseAndRedirect()
            If Not Request.QueryString("post") Is Nothing Then
                Select Case Request.QueryString("post")
                    Case "qo"
                        Response.Redirect("sony-parts.aspx")
                    Case "promopart"
                        Response.Redirect("parts-promotions.aspx")
                    Case "prc"
                        Response.Redirect("sony-service-agreements.aspx")
                    Case "ssm"
                        If Not Request.QueryString("ModelNumber") Is Nothing Then
                            Response.Redirect("sony-software-model-" + Request.QueryString("ModelNumber") + ".aspx")
                        Else
                            Response.Redirect("sony-software.aspx")
                        End If
                    Case "ppr"
                        If Not Request.QueryString("stype") Is Nothing Then
                            If Not Request.QueryString("PartLineNo") Is Nothing Then
                                Response.Redirect("PartsPLUSResults.aspx?stype=" + Request.QueryString("stype").ToString() + "&PartLineNo=" + Request.QueryString("PartLineNo"))
                            ElseIf Not Request.QueryString("KitLineNo") Is Nothing Then
                                Response.Redirect("PartsPLUSResults.aspx?stype=" + Request.QueryString("stype").ToString() + "&KitLineNo=" + Request.QueryString("PartLineNo"))
                            Else
                                Response.Redirect("sony-parts.aspx")
                            End If
                        Else
                            Response.Redirect("sony-parts.aspx")
                        End If
                    Case "pps"
                        If Not Request.QueryString("ArrayVal") Is Nothing Then
                            Response.Redirect("PartsPLUSSearch.aspx?ArrayVal=" + Request.QueryString("ArrayVal") + "&returnfrom=signin")
                        Else
                            Response.Redirect("sony-parts.aspx")
                        End If
                    Case "ssrm", "spmsr", "sr", "srms"
                        If Not Request.QueryString("SPGSModel") Is Nothing Then
                            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
                            Response.Redirect("sony-service-sn.aspx")
                        Else
                            Response.Redirect("sony-repair.aspx")
                        End If
                    Case "spcd"
                        If Not Request.QueryString("groupid") Is Nothing Then
                            Response.Redirect("groupid" + Request.QueryString("groupid") + ".aspx")
                        Else
                            Response.Redirect("sony-part-catalog.aspx")
                        End If
                    Case "stcd"
                        If Not Request.QueryString("CourseLineno") Is Nothing Then
                            Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                            Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                            Dim cr As CourseReservation = New CourseReservation
                            cr.CourseSchedule = courseSelected
                            Session("trainingreservation") = cr
                            closeAndRedirectCourse()
                        Else
                            Response.Redirect("sony-training-catalog.aspx")
                        End If
                    Case Else
                        'Do nothing
                End Select
            Else
                If Not Session("LoginFrom") Is Nothing Then
                    Select Case Session("LoginFrom").ToString()
                        Case "Bulletin"
                            Response.Redirect(Session("RedirectTo").ToString())
                        Case Else
                            Response.Redirect("Member.aspx")
                    End Select
                ElseIf Not Request.QueryString("PartNumber") Is Nothing Then 'from popup after selecting download free
                    closeAndRedirectPart() 'go to member.aspx after log in
                ElseIf Not Request.QueryString("FromHome") Is Nothing Then 'from popup after selecting login menu
                    closeAndRedirectPart() 'go to member.aspx after log in
                ElseIf Not Request.QueryString("TechBulletin") Is Nothing Then
                    closeAndRedirectTechBulletin()
                ElseIf Not Request.QueryString("CourseLineno") Is Nothing Then
                    Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("CourseLineno"))
                    Dim courseSelected As CourseSchedule = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                    Dim cr As CourseReservation = New CourseReservation
                    cr.CourseSchedule = courseSelected
                    Session("trainingreservation") = cr
                    closeAndRedirectCourse()
                ElseIf Not Request.QueryString("SPGSModel") Is Nothing Then
                    closeAndRedirectDepotRequest()
                ElseIf Not Request.QueryString("Promo") Is Nothing Then
                    closeAndRedirectPromoRequest(Request.QueryString("Promo"))
                ElseIf Not Request.QueryString("RAForm") Is Nothing Then
                    closeAndRedirectRAForm()
                Else
                    refreshParentPage()
                End If
            End If
        End Sub

        Private Sub closeAndRedirectPromoRequest(ByVal sPromoPath As String)
            Response.Redirect(sPromoPath)
        End Sub
        Private Sub refreshParentPage()
            Response.Redirect("Member.aspx")
        End Sub
        Private Sub closeAndRedirectDepotRequest()
            Session.Add("ServiceModelNumber", Request.QueryString("SPGSModel"))
            Response.Redirect("sony-service-sn.aspx")
        End Sub

        Private Sub closeAndRedirectPart()
            Response.Redirect("Member.aspx")
        End Sub

        Private Sub closeAndRedirectTechBulletin()
            Response.Redirect("Sony-Technical-Bulletins-GenInfo.aspx")
        End Sub

        Private Sub closeAndRedirectCourse()
            Dim customer As Customer
            Dim nonAccount = True

            If HttpContextManager.Customer Is Nothing Then
                Return
            End If
            customer = HttpContextManager.Customer

            For Each sap As Account In customer.SAPBillToAccounts
                'If sap.Validated Then 'commented for 6865
                If customer.UserType = "A" Then
                    nonAccount = False
                End If
            Next

            For Each sis As SISAccount In customer.SISLegacyBillToAccounts
                'If sis.Validated Then 'commented for 6865
                If customer.UserType = "A" Then
                    nonAccount = False
                End If
            Next
            If nonAccount And IsUserActive(customer) Then
                Response.Redirect("training-payment-naccnt.aspx")
            Else
                Response.Redirect("training-payment-accnt.aspx")
            End If

        End Sub
        'Start fixing Bug#:125
        Private Sub closeAndRedirectRAForm()
            Response.Redirect("RARequest.aspx")
        End Sub
        'End fixing Bug#:125
#End Region

        'Protected Sub btnUpdatePwd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdatePwd.Click
        '    Try
        '        Dim Customer As New Customer
        '        If Not Session.Item("customer") Is Nothing Then
        '            Customer = Session.Item("customer")
        '        End If
        '        If Not Customer Is Nothing Then

        '            Dim thisSA As New SecurityAdministrator
        '            Dim thisUser As User = thisSA.GetUser(Customer.SIAMIdentity).TheUser
        '            Dim thisCredintial As New Credential
        '            Dim thisResponse As New SiamResponse
        '            Dim cm As New CustomerManager
        '            Dim conExpiresInDays As Int32 = 45

        '            thisCredintial.UserName = thisUser.UserId
        '            thisCredintial.Password = Me.txtNewPassword1.Text.Trim()

        '            If Me.txtNewPassword1.Text.Trim() <> Me.txtNewPassword2.Text.Trim() Then
        '                lblPrompt.Text = "The passwords do not match."
        '            Else
        '                If cm.CheckPasswordValidity(thisCredintial.UserName, Me.txtNewPassword1.Text.Trim(), thisUser.EmailAddress, Me.lblPrompt, Customer.UserType) Then
        '                    thisResponse = thisSA.UpdateCredentials(thisUser, thisCredintial, conExpiresInDays)

        '                    If thisResponse.SiamResponseCode = CType(Messages.UPDATE_CREDENTIALS_FAILED, Integer) Then
        '                        If thisResponse.SiamResponseMessage.ToString().IndexOf("The new password specified has been used as one of the last three user passwords.") > 0 Then
        '                            lblPrompt.Text = "The new password can not be the same as one of the last three passwords used. Please select a different password."
        '                        Else
        '                            lblPrompt.Text = thisResponse.SiamResponseMessage.ToString()
        '                        End If
        '                    ElseIf thisResponse.SiamResponseCode <> CType(Messages.UPDATE_CREDENTIALS_COMPLETED, Integer) Then
        '                        lblPrompt.Text = thisResponse.SiamResponseMessage.ToString()
        '                    Else
        '                        '-- all validated and updated 
        '                        CloseAndRedirect()
        '                    End If
        '                End If
        '            End If
        '        End If
        '    Catch ex As Exception
        '        lblPrompt.Text = Utilities.WrapExceptionforUI(ex)
        '    End Try
        'End Sub

    End Class

End Namespace
