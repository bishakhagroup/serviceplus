Imports System.Globalization
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp
    Partial Class NAccntHolder
        Inherits SSL

        Public Auditcustomer As Customer 'Added by Narayanan July 31 for AuditTrail
        Private usCulture As New CultureInfo("en-US")
        Public cm As New CustomerManager
        Public siamID As String
        Public customer As Customer
        Private focusFormField As String = "TextBoxName"
        Private Const currentPageIndex As String = "1"
        Private Const currentProcessIndex As String = "0"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents rbSaveCard As System.Web.UI.WebControls.RadioButton
        Protected WithEvents chkBoxShipBack As System.Web.UI.WebControls.CheckBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()

            'noPoRequiredCheckBox.Attributes("onclick") = "javascript: var thisControl = FIND('" + TextBoxPO.ID.ToString() + "'); if (thisControl != null) {thisControl.value = 'NO-PO-REQ';}"
        End Sub

#End Region

#Region "   Events      "

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim debugMessage As String = $"NAccntHolder.aspx - Page_Load - START at {DateTime.Now.ToShortTimeString}{Environment.NewLine}"

            If Not IsPostBack Then
                'Upper case - Zip code 
                TextBoxZip.Attributes.Add("onblur", "this.value = this.value.toUpperCase()")
                pnlNickName.Visible = False
                pnlCCInfo.Visible = False
                pnlSavedCard.Visible = False
                btnNext.Style.Add("display", "inline")
            Else
                If pnlCCInfo.Visible = True Then
                    ' pnlCCInfo.Visible = True
                    ' pnlSavedCard.Visible = True
                    ' pnlNickName.Visible = True
                Else
                    pnlCCInfo.Visible = False
                    pnlSavedCard.Visible = False
                End If
            End If

            Try
                formFieldInitialization()
                If (Not Page.IsPostBack) Then
                    debugMessage &= $"- Checking Page and Process Indices.{Environment.NewLine}"
                    If Session.Item("CurrentPageIndex") IsNot Nothing And Session.Item("currentProcessIndex") IsNot Nothing Then
                        debugMessage &= $"--- CurrentPageIndex = {Session("CurrentPageIndex")}, currentProcessIndex = {Session("currentProcessIndex")}.{Environment.NewLine}"
                        If Convert.ToInt32(Session.Item("CurrentPageIndex")) < Convert.ToInt32(currentPageIndex) And Convert.ToInt32(Session.Item("currentProcessIndex")) = Convert.ToInt32(currentProcessIndex) Then
                            Session.Item("CurrentPageIndex") = currentPageIndex
                        Else
                            debugMessage &= $"--- First Else statement hit. Redirecting to SessionExpired."
                            Utilities.LogDebug(debugMessage)
                            Response.Redirect("SessionExpired.aspx&returnto=cart", False)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    Else
                        debugMessage &= $"--- Second Else statement hit. Redirecting to SessionExpired."
                        Utilities.LogDebug(debugMessage)
                        Response.Redirect("SessionExpired.aspx&returnto=cart", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                btnNext.Visible = False     ' If this page failed, the user clicking "Next" will likely just submit an invalid Order anyway.
            End Try

            Try
                Dim cart As ShoppingCart = Nothing

                customer = HttpContextManager.Customer
                If Session.Item("carts") IsNot Nothing Then cart = Session.Item("carts")

                If customer IsNot Nothing Then
                    'If customer.CreditCards.Length = 0 Then 'Is Nothing Then
                    '    btnCCSaved.Visible = False
                    '    DDLCCName.Enabled = False
                    'Else
                    '    DDLCCName.Enabled = True
                    '    btnCCSaved.Visible = True
                    'End If
                    debugMessage &= $"- Checking Credit Cards.{Environment.NewLine}"
                    Dim cardsNotHidden = From c In customer.CreditCards Where c.HideCardFlag = False
                    If customer.CreditCards.Length = 0 Or cardsNotHidden.Count = 0 Then 'Is Nothing Then
                        debugMessage &= $"--- No cards found.{Environment.NewLine}"
                        btnCCSaved.Visible = False
                        DDLCCName.Enabled = False
                    ElseIf customer.CreditCards.Length = 1 Then
                        debugMessage &= $"--- One card found.{Environment.NewLine}"
                        If String.IsNullOrWhiteSpace(customer.CreditCards(0)?.NickName) Then
                            btnCCSaved.Visible = False
                        Else
                            btnCCSaved.Visible = True
                            DDLCCName.Enabled = True
                        End If
                    Else    ' More than one card
                        debugMessage &= $"--- Cards found.{Environment.NewLine}"
                        btnCCSaved.Visible = True
                        DDLCCName.Enabled = True
                    End If

                    If Not Page.IsPostBack Then
                        '-- if download only item are selected the we'll select ground shipping so that the customer doesn't have to pick a shipping 
                        '-- method and so the page validation wil still work. - jwu
                        If cart Is Nothing Then
                            Throw New NullReferenceException("Shopping Cart variable is null.")
                        ElseIf cart.Type = ShoppingCart.enumCartType.Download Then
                            ShipMethod.Initialize(False)
                        Else
                            ShipMethod.Initialize(True)
                        End If

                        debugMessage &= $"- Calling LoadDDLS().{Environment.NewLine}"
                        LoadDDLS()

                        debugMessage &= $"- Checking user account type.{Environment.NewLine}"
                        ' 2018-03-27 ASleight - I implemented then reverted the below, because this "getUsersAccountType" treats Pending and Account-Holder the same way.
                        'If getUsersAccountType(customer.SIAMIdentity) = enumAccountType.AccountHolder Then
                        '    debugMessage &= $"--- Account holder found. Redirecting to AccntHolder.aspx."
                        '    Utilities.LogDebug(debugMessage)
                        '    Response.Redirect("AccntHolder.aspx", False)
                        '    HttpContext.Current.ApplicationInstance.CompleteRequest()
                        'End If
                        ' For Pending users, show the explanation for why they must order by Credit Card.
                        ' Note: Validated Account Holders should not be on this page to begin with.
                        If getUsersAccountType(customer.SIAMIdentity) = enumAccountType.AccountHolder Then ShowPendingAccountMessage()

                        'Prepop Bill To Fields
                        debugMessage &= $"- Populating controls with Customer detail.{Environment.NewLine}"
                        TextBoxName.Text = customer.CompanyName
                        Dim AttnName As String = customer.FirstName + " " + customer.LastName
                        If AttnName.Length() > 30 Then
                            TextboxAttn.Text = AttnName.Substring(0, 30)
                        Else
                            TextboxAttn.Text = AttnName
                        End If
                        TextBoxLine1.Text = customer.Address.Line1
                        TextBoxLine2.Text = customer.Address.Line2
                        TextBoxCity.Text = customer.Address.City
                        'hdCAPartialTax.Value = customer.CaliforniaPartialTax
                        hdTaxExempt.Value = customer.Tax_Exempt

                        If Not String.IsNullOrEmpty(customer.Address.State) Then
                            debugMessage &= $"- Setting selected state. Value = {customer.Address.State}.{Environment.NewLine}"
                            DDLState.SelectedValue = customer.Address.State
                        End If

                        TextBoxZip.Text = customer.Address.PostalCode
                        If Not String.IsNullOrEmpty(cart.PurchaseOrderNumber) Then TextBoxPO.Text = cart.PurchaseOrderNumber

                        txtPhoneShip.Text = customer.PhoneNumber
                        txtShipExtension.Text = customer.PhoneExtension
                        btnNext.Visible = True  ' Show the button, in case a previous error hid it.
                        focusFormField = "TextBoxName"
                        RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
                    End If
                End If
            Catch ex As Exception
                Utilities.LogMessages(debugMessage)
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                btnNext.Visible = False     ' If this page failed, the user clicking "Next" will likely just submit an invalid Order anyway.
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
            Dim objPCILogger As New PCILogger()
            Dim om As New OrderManager
            Dim sm As New ShoppingCartManager
            Dim pm As New PaymentManager
            Dim cart As ShoppingCart
            Dim card_types() As CreditCardType
            Dim ship_methods() As ShippingMethod
            Dim credit_card_type As CreditCardType = Nothing
            Dim method As New ShippingMethod
            Dim card As CreditCard = Nothing
            Dim bill_to As New Sony.US.ServicesPLUS.Core.Address
            Dim ship_to As New Sony.US.ServicesPLUS.Core.Address
            Dim order As Order
            Dim objSAPBillToAccount As Account = New Account()
            Dim objSAPShipToAccount As Account = New Account()
            Dim nickNameList As New ArrayList
            Dim exp_date As Date
            Dim sCCNumber As String
            Dim nYear As Integer
            Dim nMonth As Integer
            Dim nDay As Integer
            Dim whichCCType As Integer
            'Dim index As Integer
            'Dim whichMethod As Integer
            Dim iscreditCardAdd As Boolean = True

            If Session("token") IsNot Nothing Then
                Dim CCnumber() As String = Session("token").ToString().Split("-")
                TextBoxCCNumber.Text = "XXXXXXXXXXXX" & CCnumber(2)
            End If

            If String.IsNullOrEmpty(TextBoxCCNumber.Text) Then
                btnNext.Style.Add("display", "inline")
                If btnCCSaved.Visible = True Then       'User has saved creditcards
                    btnNext.Enabled = True
                    ErrorLabel.Text = "Please enter credit card information or select a saved credit card by clicking one of the buttons below."
                Else        'User does not have saved creditcards
                    ErrorLabel.Text = "Please enter credit card information by clicking ""Enter New Credit Card"" below."
                End If
                Return
            End If

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnNext_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.EventType = EventType.Add
                objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                chkSaveCard.Checked = (Not String.IsNullOrEmpty(TextBoxNName.Text))

                ValidateForm()
                If ErrorLabel.Visible Then
                    If String.IsNullOrEmpty(ErrorLabel.Text) Then
                        btnNext.Style.Add("display", "inline")
                        ErrorLabel.Text = "Please complete the required fields."
                    End If
                    Return
                End If

                '''''''''''''''''''''''''''''''Create Order''''''''''''''''''''''''''''''''
                ' Get Cart
                If Session.Item("carts") Is Nothing Then
                    ErrorLabel.Visible = True
                    btnNext.Style.Add("display", "inline")
                    ErrorLabel.Text = ("Cart is empty.")
                    Return
                End If
                cart = Session.Item("carts")

                'Get and/or Add credit card type selected 

                nYear = CType(ddlYear.SelectedItem.Value, Integer)
                nMonth = CType(ddlMonth.SelectedItem.Value, Integer)
                nDay = Date.DaysInMonth(nYear, nMonth)
                exp_date = New Date(nYear, nMonth, nDay)
                sCCNumber = TextBoxCCNumber.Text
                If (Session.Item("card_types") Is Nothing) Then Throw New NullReferenceException("card_types Session variable is null.")
                card_types = Session.Item("card_types")

                If DDLCCName.SelectedIndex > 0 Then
                    whichCCType = DDLCCName.SelectedIndex - 1
                    card = customer.CreditCards.GetValue(whichCCType)

                    For Each cc As CreditCard In customer.CreditCards
                        If (cc.NickName = DDLCCName.SelectedValue) And (Not cc.HideCardFlag) Then
                            card = cc
                            iscreditCardAdd = False
                            Exit For
                        End If
                    Next
                Else
                    whichCCType = DDLType.SelectedIndex - 1
                    If whichCCType > card_types.Length Then Throw New IndexOutOfRangeException("whichCCType (" & whichCCType & ") > card_types.Length (" & card_types.Length & ")")
                    credit_card_type = card_types.GetValue(whichCCType)
                    If Session("token") IsNot Nothing Then
                        If (nickNameList.Count > 0) Then    ' ASleight This array is always empty, so this block will never run.
                            If (nickNameList.Contains(Session("token").ToString())) Then
                                iscreditCardAdd = False
                            End If
                        End If
                    End If
                End If

                'Save card if rb is checked 
                'If rbSaveCard.Checked Then
                'cm.UpdateCustomer(customer)
                'End If

                'Shipping Method
                If (Session.Item("shipmethods") Is Nothing) Then Throw New NullReferenceException("ShipMethods session variable is null")
                ship_methods = Session.Item("shipmethods")

                For Each m As ShippingMethod In ship_methods
                    ' For Each subModelTemp As Model In Model.SubModels
                    If m.Description = ShipMethod.SelectedShipMethodValue Then
                        method = ship_methods.GetValue(ShipMethod.SelectedShipMethodIndex)
                        Exit For
                    End If
                Next
                method.ISBackOrderSameShippingMethod = False

                ' Bill To
                bill_to.Name = TextBoxName.Text
                bill_to.Attn = TextboxAttn.Text
                bill_to.Line1 = TextBoxLine1.Text
                bill_to.Line2 = TextBoxLine2.Text
                bill_to.City = TextBoxCity.Text
                bill_to.Country = customer.CountryCode
                bill_to.State = DDLState.SelectedValue
                bill_to.PostalCode = TextBoxZip.Text

                'Ship To
                ship_to.Name = TextBoxNameShip.Text
                ship_to.Attn = TextboxShipAttn.Text
                ship_to.Line1 = TextBoxLineShip.Text
                ship_to.Line2 = TextBoxLine2Ship.Text
                ship_to.City = TextBoxCityShip.Text
                ship_to.State = DDLStateShip.SelectedValue
                ship_to.PostalCode = TextBox5.Text
                ship_to.PhoneNumber = txtPhoneShip.Text.Trim()
                ship_to.PhoneExt = txtShipExtension.Text.Trim()

                If cart.Type = ShoppingCart.enumCartType.Download Then
                    'use the same info as bill to for download only
                    If String.IsNullOrEmpty(ship_to.Name) Then ship_to.Name = TextBoxName.Text
                    If String.IsNullOrEmpty(ship_to.Line1) Then ship_to.Line1 = TextBoxLine1.Text
                    If String.IsNullOrEmpty(ship_to.Line2) Then ship_to.Line2 = TextBoxLine2.Text
                    If String.IsNullOrEmpty(ship_to.City) Then ship_to.City = TextBoxCity.Text
                    If String.IsNullOrEmpty(ship_to.State) Then ship_to.State = Request.Form.Item("DDLState")
                    If String.IsNullOrEmpty(ship_to.PostalCode) Then ship_to.PostalCode = TextBoxZip.Text
                End If
                '-- add P.O. number to cart
                cart.PurchaseOrderNumber = TextBoxPO.Text.ToString()

                'Finally create order
                objSAPBillToAccount.Address = bill_to
                objSAPBillToAccount.AccountNumber = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
                objSAPShipToAccount.Address = ship_to
                objSAPShipToAccount.SAPAccount = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
                objSAPShipToAccount.AccountNumber = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
                objSAPShipToAccount.TaxExempt = ""
                If tbCalifornia.Visible Then
                    If rbCalifornia.SelectedValue = "0" Then objSAPShipToAccount.TaxExempt = "9"
                ElseIf pm.IsTaxExempt(customer.SIAMIdentity, DDLStateShip.SelectedItem.Text) Then
                    objSAPShipToAccount.TaxExempt = "2"
                End If

                'Simulate Order
                order = om.SimulateOrder(customer, cart, objSAPBillToAccount, False, objSAPShipToAccount, True, method, True, card, HttpContextManager.GlobalData)
                order.CreditCard = card
                order.BillToAccount = objSAPBillToAccount
                order.ShipToAccount = objSAPShipToAccount
                'order.BillTo.Attn = TextboxAttn.Text.Trim()
                'order.ShipTo.Attn = TextboxShipAttn.Text.Trim()
                'order.ShipTo.PhoneNumber = txtPhoneShip.Text.Trim()
                'order.ShipTo.PhoneExt = txtShipExtension.Text.Trim()

                If order.MessageType = "E" Then
                    If order.MessageText.Contains("Error in calling") Then
                        Throw New Exception(order.MessageText)
                    ElseIf order.MessageText.Contains("Error in calling Z_SV_I1448_ORDER_INJECTION BAPI") Then
                        Throw New Exception("MessageText= 00: AppCode=SPARC2, Order Number=WJqgVcJIIgaC : Error in calling Z_SV_I1448_ORDER_INJECTION BAPI in  SelSparcTokenization.Inbound.Services.SalesOrder:receiveSalesOrderNotification Service for WJqgVcJIIgaC Order Number.")
                    Else
                        ErrorLabel.Text = order.MessageText
                        ErrorLabel.Visible = True
                        Return
                    End If
                End If

                '* Insert a "2" value into the orders.taxexemptovessrride field if the customer is
                'a credit card only customer and they are determined to have full tax exemption
                'for the order per the enabletaxexemptstatus table.
                '* Insert a "9" value into the orders.taxexemptoverride field if the customer is
                'a credit card only customer and they are determined to not have full tax
                'exemption for the order per the enabletaxexemptstatus table, but are partially
                'tax exempt because the customer selected "Yes" on the NAccntHolder.aspx page.
                '* Insert a "9" value into the orders.taxexemptoverride field if the customer is
                'an account customer and they are partially tax exempt because the customer
                'selected "Yes" on AccntHolder.aspx page.

                order.Alt_Tax_Classification = ""
                If customer.Tax_Exempt Then
                    order.Alt_Tax_Classification = "2"
                    order.EnableDocumentIDNumber = customer.ETDocumentidnumber
                ElseIf tbCalifornia.Visible Then
                    If rbCalifornia.SelectedValue = 0 Then
                        order.Alt_Tax_Classification = "9"
                        order.EnableDocumentIDNumber = customer.Enabledocumentidnumber
                    End If
                End If

                Session.Add("order", order)
                If DDLCCName.SelectedIndex = 0 Then
                    btnNext.Enabled = True
                    If chkSaveCard.Checked Then
                        objPCILogger.Message = "Nick Name of the Newly Added Credit Card: " + TextBoxNName.Text.Trim()
                    Else
                        objPCILogger.Message = "Newly Added Credit Card Details Not Saved"
                    End If
                End If

                If (iscreditCardAdd) Then
                    card = customer.AddCreditCard(Session("token").ToString(), exp_date, credit_card_type, "", TextBoxNName.Text)
                    customer.CreditCards(0) = card
                    Dim objCustomerManager As CustomerManager = New CustomerManager()
                    objCustomerManager.UpdateCustomerCreditCard(customer)
                    order.CreditCard = card
                End If

                If Session("token") IsNot Nothing Then
                    nickNameList.Add(Session("token").ToString())
                End If
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.PushLogToMSMQ()
                Session.Remove("token")
                Response.Redirect("TC.aspx", False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch exThrd As Threading.ThreadAbortException
                'Do Nothing
            Catch ex As Exception
                If (ex.Message.Contains("Please select unique nickname")) Then
                    lblCustomError.Text = "Please select unique nickname"
                ElseIf ex.Message.Contains("Error in calling") Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    ErrorLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = ErrorLabel.Text.Replace(ErrorLabel.Text, "Unexpected error encountered. For assistance, please <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> contact us </a> and mention incident number")
                    ErrorLabel.Text = er + " " + strUniqueKey + "."
                    ErrorLabel.Visible = True
                ElseIf ex.Message.Contains("Error in calling Z_SV_I1448_ORDER_INJECTION BAPI") Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    ErrorLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = ErrorLabel.Text.Replace(ErrorLabel.Text, "Unexpected error encountered. For assistance, please <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> contact us </a> and mention incident number")
                    ErrorLabel.Text = er + " " + strUniqueKey + "."
                    ErrorLabel.Visible = True
                Else
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    ErrorLabel.Visible = True
                End If
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ErrorLabel.Text.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try

            'End If
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click

            '-- clear CurrentPageIndex, currentProcessIndex - restart
            If Not Session.Item("CurrentPageIndex") Is Nothing Then
                Session.Remove("CurrentPageIndex")
            End If
            If Not Session.Item("currentProcessIndex") Is Nothing Then
                Session.Remove("currentProcessIndex")
            End If

            Response.Redirect("vc.aspx", False)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        End Sub

        Public Sub DDLCCName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DDLCCName.SelectedIndexChanged
            Dim objPCILogger As New PCILogger()
            Dim om As New OrderManager
            Dim sm As New ShoppingCartManager
            Dim pm As New PaymentManager
            Dim cards() As CreditCard
            Dim card As CreditCard = Nothing
            'Dim index As Int16
            Dim expireDate As Date
            Dim popCardNumber As String
            Dim LastFour As String
            Dim str() As String
            Dim ccLength As Int32
            Dim Month As String
            Dim Year As Integer
            Dim tempYear As Integer

            pnlCCInfo.Visible = True

            Try
                ErrorLabel.Text = ""
                If DDLCCName.SelectedIndex > 0 Then
                    objPCILogger.EventOriginApplication = "ServicesPLUS"
                    objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                    objPCILogger.EventOriginMethod = "DDLCCName_SelectedIndexChanged"
                    objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    objPCILogger.OperationalUser = Environment.UserName
                    objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                    objPCILogger.EventType = EventType.View
                    objPCILogger.HTTPRequestObjectValues = GetRequestContextValue()

                    TextBoxCCNumber.Text = ""
                    DDLType.SelectedIndex = 0

                    customer = HttpContextManager.Customer
                    If customer IsNot Nothing Then
                        '6524 start
                        objPCILogger.CustomerID = customer.CustomerID
                        objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                        objPCILogger.EmailAddress = customer.EmailAddress
                        objPCILogger.SIAM_ID = customer.SIAMIdentity
                        objPCILogger.LDAP_ID = customer.LdapID
                    Else
                        objPCILogger.Message = "Unable to fetch Customer details"
                        objPCILogger.PushLogToMSMQ()
                        Response.Redirect("default.aspx", False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                    cards = customer.CreditCards
                    For Each c As CreditCard In cards
                        If c.NickName = DDLCCName.SelectedValue And c.HideCardFlag = False Then
                            card = c
                        End If
                    Next


                    If card Is Nothing Then
                        Throw New NullReferenceException("Credit Card is null")
                    ElseIf card.CreditCardNumber.Length = 16 Then
                        ccLength = card.CreditCardNumber.Length - 4
                        LastFour = card.CreditCardNumber.Substring(ccLength, 4)
                        popCardNumber = ("xxxxxxxxxxxx" + LastFour)
                        TextBoxCCNumber.Text = popCardNumber
                    Else
                        str = card.CreditCardNumber.Split("-")
                        popCardNumber = ("xxxxxxxxxxxx" + str(2))
                        TextBoxCCNumber.Text = popCardNumber
                    End If

                    'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                    objPCILogger.CreditCardMaskedNumber = card.CreditCardNumberMasked
                    objPCILogger.CreditCardSequenceNumber = card.SequenceNumber
                    objPCILogger.Message = "Nick Name of the Credit Card: " + card.NickName

                    'Get ExpirationDate
                    expireDate = Convert.ToDateTime(card.ExpirationDate, usCulture)
                    Month = Format(expireDate, "MM")
                    Year = Format(expireDate, "yyyy")

                    'Set Credit Card Exp Date DDL 
                    If (Month.Length < 2) Then
                        Month = "0" + Month
                    End If
                    ddlMonth.SelectedValue = Month
                    'Add  Month to show in Textbox(ddl is hidden)
                    txtMonth.Text = ddlMonth.SelectedItem.Text

                    populateYearddl()
                    tempYear = Convert.ToInt32(ddlYear.Items(0).Value)
                    If Year < tempYear Then
                        Me.ErrorLabel.Text = "This credit card has expired, please choose another"
                        Return
                    End If
                    ddlYear.SelectedValue = Year.ToString()
                    ' Add  Year to show in Textbox(ddl is hidden)
                    txtYear.Text = Year

                    'Set DDLType option
                    DDLType.SelectedValue = card.Type.CreditCardTypeCode
                    ' Add type to show in Textbox(ddl is hidden)
                    txtCCType.Text = DDLType.SelectedItem.Text

                    'Reset form validation err msg
                    'LabelCCNumberStar.Text = ""
                    'LabelExpStar.Text = ""
                    'LabelOneStar.Text = ""
                    'LabelCardStar.Text = ""
                    LabelOneCard.CssClass = "tableData"
                    LabelCC.CssClass = "tableData"
                    LabelCCNumber.CssClass = "tableData"
                    LabelExpire.CssClass = "tableData"
                    LabelOneCard.CssClass = "tableData"

                    TextBoxNName.Text = card.NickName
                    'rbSaveCard.Enabled = False
                    chkSaveCard.Enabled = False

                    'should be readonly if using an existing cc
                    TextBoxCCNumber.Enabled = False
                    ddlMonth.Enabled = False
                    ddlYear.Enabled = False
                    DDLType.Enabled = False
                    TextBoxNName.Enabled = False

                    'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                    objPCILogger.IndicationSuccessFailure = "Success"
                    objPCILogger.PushLogToMSMQ()
                Else
                    'TextBoxCCNumber.Enabled = True
                    'ddlMonth.Enabled = True
                    'ddlYear.Enabled = True
                    'ddlMonth.Visible = True
                    'ddlYear.Visible = True
                    txtMonth.Text = ""
                    txtYear.Text = ""
                    'DDLType.Enabled = True
                    'TextBoxNName.Enabled = True
                    chkSaveCard.Enabled = True
                    'rbSaveCard.Enabled = True
                    TextBoxCCNumber.Text = ""
                    ddlMonth.SelectedIndex = 0
                    'ddlYear.SelectedIndex = 0
                    'commented by prasad for 2600
                    'ddlYear.Items.Clear()
                    ddlYear.SelectedIndex = 0
                    DDLType.SelectedIndex = 0
                    TextBoxNName.Text = ""
                    'focusFormField = "TextBoxNName"
                    'RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
                End If
            Catch ex As Exception
                'Dim objUtilties As Utilities = New Utilities
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log 
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = ErrorLabel.Text.ToString()
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        'Private Sub chkSameShip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSameShip.CheckedChanged
        '    Try
        '        If chkSameShip.Checked Then
        '            'Prepop Bill To Fields
        '            TextBoxNameShip.Text = TextBoxName.Text
        '            TextboxShipAttn.Text = TextboxAttn.Text
        '            TextBoxLineShip.Text = TextBoxLine1.Text
        '            TextBoxLine2Ship.Text = TextBoxLine2.Text
        '            TextBoxCityShip.Text = TextBoxCity.Text
        '            Dim whichState As String

        '            whichState = Request.Form.Item("DDLState")

        '            DDLStateShip.SelectedValue = whichState

        '            TextBox5.Text = TextBoxZip.Text

        '            'Reset form validation err msg
        '            LabelStateShip.CssClass = "tableData"
        '            ZipShip.CssClass = "tableData"
        '            LabelStateShip.CssClass = "tableData"
        '            LabelCityShip.CssClass = "tableData"
        '            LabelNameShip.CssClass = "tableData"
        '            LabelLine1Ship.CssClass = "tableData"

        '            LabelName2Star.Text = ""
        '            LabelLine1ShipStar.Text = ""
        '            LabelState2Star.Text = ""
        '            LabelZip2Star.Text = ""
        '            LabelCity2Star.Text = ""
        '        End If

        '        focusFormField = "ImageButton1"
        '            RegisterStartupScript("SetFocus", "<script language='javascript'>document.getElementById('" + focusFormField + "').focus()</script>")
        '    Catch ex As Exception
        '        ErrorLabel.Text = ex.Message
        '        ErrorLabel.Visible = True
        '    End Try
        'End Sub
#End Region

#Region "   Methods     "
        Private Sub LoadDDLS()
            Dim objPCILogger As New PCILogger() '6524
            Dim orderMan As New OrderManager
            Dim cartMan As New ShoppingCartManager
            Dim payMan As New PaymentManager
            Dim card_types() As CreditCardType
            Dim cards() As CreditCard

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "LoadDDLS"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILogger.UserName = customer.UserName
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "LoadDDLS Success."

                ErrorLabel.Text = ""

                DDLState.Items.Clear()
                DDLState.Items.Add(New ListItem("Select One", ""))
                populateStateDropdownList(DDLState, False)

                DDLStateShip.Items.Clear()
                DDLStateShip.Items.Add(New ListItem("Select One", ""))
                populateStateDropdownList(DDLStateShip, False)

                'Expire Date DDL's
                ddlMonth.Items.Clear()
                txtMonth.Text = ""
                ddlMonth.Items.Add(New ListItem("Month", ""))
                ddlMonth.Items.Add(New ListItem("January", "01"))
                ddlMonth.Items.Add(New ListItem("February", "02"))
                ddlMonth.Items.Add(New ListItem("March", "03"))
                ddlMonth.Items.Add(New ListItem("April", "04"))
                ddlMonth.Items.Add(New ListItem("May", "05"))
                ddlMonth.Items.Add(New ListItem("June", "06"))
                ddlMonth.Items.Add(New ListItem("July", "07"))
                ddlMonth.Items.Add(New ListItem("August", "08"))
                ddlMonth.Items.Add(New ListItem("September", "09"))
                ddlMonth.Items.Add(New ListItem("October", "10"))
                ddlMonth.Items.Add(New ListItem("November", "11"))
                ddlMonth.Items.Add(New ListItem("December", "12"))

                populateYearddl()

                'Load Credit cards
                card_types = payMan.GetCreditCardTypes()
                Session.Add("card_types", card_types)

                DDLType.Items.Add(New ListItem(" ", ""))
                For Each cc As CreditCardType In card_types
                    DDLType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                Next

                'Load cc's from customer
                cards = customer.CreditCards

                DDLCCName.Items.Clear()
                DDLCCName.Items.Add(New ListItem("Select One", ""))
                For Each c As CreditCard In cards
                    'Get Last four digits
                    'Dim popCardNumber As String
                    'Dim ccLength As Int32
                    'Dim LastFour As String
                    'ccLength = c.CreditCardNumber.Length - 4
                    ' LastFour = c.CreditCardNumber.Substring(ccLength, 4)

                    '--exclude expired cards
                    Dim ccYear As Integer
                    Dim ccMonth As Integer
                    Dim cardDate As DateTime
                    Try
                        cardDate = Convert.ToDateTime(c.ExpirationDate, usCulture)
                        ccYear = cardDate.Year
                        ccMonth = cardDate.Month
                    Catch ex As Exception
                        '-- make the card invalid
                        ccYear = DateTime.Today.Year - 1
                        Utilities.WrapExceptionforUI(ex)
                    End Try
                    'If cards.Length <= 0 Then
                    '    DDLCCName.Items.Add(New ListItem("Select One", ""))
                    'End If
                    Dim currentYear As Integer = DateTime.Today.Year
                    Dim currentMonth As Integer = DateTime.Today.Month

                    '-- adding hidecardflag condition to hide those cards deleted by the customer
                    '-- for bug 166 - Changed by Deepa Vembu 11/01
                    If Not String.IsNullOrWhiteSpace(c.NickName) And Not c.HideCardFlag And (ccYear > currentYear Or (ccYear = currentYear And ccMonth > currentMonth)) Then
                        DDLCCName.Items.Add(New ListItem(c.NickName))
                    End If
                Next

                'If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.StatusID = 1 Then'6668
                If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then '6668
                    tbCalifornia.Visible = (DDLStateShip.Text = "CA")
                Else
                    tbCalifornia.Visible = False
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "LoadDDLS Failed. " & ex.Message
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Public Sub ValidateForm()
            Try
                ErrorLabel.Text = ""
                ErrorLabel.Visible = False
                'Start Modification for E458
                CCErrorLabel.Text = ""
                'End Modification for E458

                LabelName.CssClass = "tableData"
                LabelLine1.CssClass = "tableData"
                LabelLine2.CssClass = "tableData"
                'LabelLine3.CssClass = "tableData"'7901
                LabelCity.CssClass = "tableData"
                LabelState.CssClass = "tableData"
                LabelZip.CssClass = "tableData"
                PoNumberLabel.CssClass = "tableData"
                LabelOneCard.CssClass = "tableData"
                LabelCC.CssClass = "tableData"
                LabelCCNumber.CssClass = "tableData"
                LabelExpire.CssClass = "tableData"
                LabelCC.CssClass = "tableData"
                LabelNameShip.CssClass = "tableData"
                LabelLine1Ship.CssClass = "tableData"
                LabelLine2Ship.CssClass = "tableData"
                'LabelLine3Ship.CssClass = "tableData"'7901
                LabelCityShip.CssClass = "tableData"
                LabelStateShip.CssClass = "tableData"
                ZipShip.CssClass = "tableData"
                LabelCCNumber.CssClass = "tableData"
                LabelNName.CssClass = "tableData"
                'Start Modification for E458
                PhoneShip.CssClass = "tableData"
                'lblCVV.CssClass = "tableData"
                LabelAttn.CssClass = "tableData"
                'End Modification for E458


                'LabelOneStar.Text = ""
                'LabelCardStar.Text = ""
                LabelNameStar.Text = ""
                lblAttnStar.Text = ""
                LabelLine1Star.Text = ""
                LabelLine2Star.Text = ""
                'LabelLine3Star.Text = ""'7901
                LabelCityStar.Text = ""
                LabelZipStar.Text = ""
                LabelStateStar.Text = ""
                LabelLine1ShipAttention.Text = ""
                LabelName2Star.Text = ""
                LabelLine1ShipStar.Text = ""
                LabelLine2ShipStar.Text = ""
                'LabelLine3ShipStar.Text = ""'7901
                LabelCity2Star.Text = ""
                LabelZip2Star.Text = ""
                LabelState2Star.Text = ""
                PoNumberAsterick.Text = ""

                'LabelCCNumberStar.Text = ""
                'LabelExpStar.Text = ""
                'LabelNNameStar.Text = ""
                'Start Modification for E458
                LabelShipPhoneStar.Text = ""
                'End Modification for E458
                LabelCalifornia.Text = ""

                If String.IsNullOrEmpty(TextBoxName.Text) Then
                    ErrorLabel.Visible = True
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxName.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">Bill To Name must be 35 characters or less in length.</span>"
                End If

                If String.IsNullOrEmpty(TextboxAttn.Text) Then
                    ErrorLabel.Visible = True
                    LabelAttn.CssClass = "redAsterick"
                    lblAttnStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextboxAttn.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelName.CssClass = "redAsterick"
                    LabelNameStar.Text = "<span class=""redAsterick"">Bill To Attention must be 35 characters or less in length.</span>"
                End If

                If String.IsNullOrEmpty(TextBoxLine1.Text) Then
                    ErrorLabel.Visible = True
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxLine1.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine1.CssClass = "redAsterick"
                    LabelLine1Star.Text = "<span class=""redAsterick"">Bill To Street Address must be 35 characters or less in length.</span>"
                    TextBoxLine1.Focus()
                End If

                If TextBoxLine2.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine2.CssClass = "redAsterick"
                    LabelLine2Star.Text = "<span class=""redAsterick"">Bill To Address 2nd Line must be 35 characters or less in length.</span>"
                    TextBoxLine2.Focus()
                End If

                If String.IsNullOrEmpty(TextBoxCity.Text) Then
                    ErrorLabel.Visible = True
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxCity.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelCity.CssClass = "redAsterick"
                    LabelCityStar.Text = "<span class=""redAsterick"">Bill To City must be 35 characters or less in length.</span>"
                    TextBoxCity.Focus()
                End If

                If String.IsNullOrEmpty(TextBoxPO.Text) Then
                    ErrorLabel.Visible = True
                    PoNumberLabel.CssClass = "redAsterick"
                    PoNumberAsterick.Text = "<span class=""redAsterick"">*</span>"
                ElseIf TextBoxPO.Text.Length > 20 Then
                    ErrorLabel.Visible = True
                    CCErrorLabel.Text = "PO Length must be less or equal to 20."
                End If

                If DDLState.SelectedIndex = 0 Then
                    ErrorLabel.Visible = True
                    LabelState.CssClass = "redAsterick"
                    LabelStateStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If Not ShipMethod.ValidateForm() Then '6988
                    ErrorLabel.Visible = True
                End If

                Dim sZip As String = TextBoxZip.Text.Trim()
                Dim objZipCodePattern As New Regex(IIf(HttpContextManager.GlobalData.IsAmerica, "\d{5}(-\d{4})?", "^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$"))
                'If HttpContextManager.GlobalData.IsCanada Then
                '    objZipCodePattern = New Regex("^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$")
                'End If
                If Not objZipCodePattern.IsMatch(sZip) Then
                    ErrorLabel.Visible = True
                    LabelZip.CssClass = "redAsterick"
                    LabelZipStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If tbCalifornia.Visible = True Then
                    If rbCalifornia.SelectedIndex <> 0 And rbCalifornia.SelectedIndex <> 1 Then
                        ErrorLabel.Visible = True
                        LabelCalifornia.CssClass = "redAsterick"
                        LabelCalifornia.Text = "<span class=""redAsterick"">Please select elgible for California section 6378 partial tax exemption</span>"
                    End If
                End If

                If DDLCCName.SelectedIndex = 0 And DDLType.SelectedIndex = 0 Then
                    ErrorLabel.Visible = True
                    LabelOneCard.CssClass = "redAsterick"
                    LabelCC.CssClass = "redAsterick"
                    'LabelOneStar.Text = "<span class=""redAsterick"">*</span>"
                    'LabelCardStar.Text = "<span class=""redAsterick"">*</span>"
                End If

                If DDLType.SelectedIndex > 0 Then
                    If String.IsNullOrEmpty(TextBoxCCNumber.Text) Then
                        ErrorLabel.Visible = True
                        LabelCCNumber.CssClass = "redAsterick"
                        'LabelCCNumberStar.Text = "*"
                    Else
                        Dim strCCErroMessage As String = String.Empty
                        Dim strCardNumber As String = String.Empty
                        If DDLCCName.SelectedIndex > 0 Then
                            For index = 0 To customer.CreditCards.Length - 1
                                If customer.CreditCards(index).NickName = DDLCCName.SelectedValue And customer.CreditCards(index).HideCardFlag = False Then
                                    strCardNumber = customer.CreditCards(index).CreditCardNumber
                                End If
                            Next
                        Else
                            strCardNumber = TextBoxCCNumber.Text
                        End If

                        Dim blnCCValidaiton As Boolean = True ' Sony.US.SIAMUtilities.Validation.IsCreditCardValid(strCardNumber, DDLType.SelectedItem.Text.Substring(0, 1).ToUpper(), strCCErroMessage)
                        'LabelCCNumberStar.Text = IIf(blnCCValidaiton, "*", strCCErroMessage)
                        If blnCCValidaiton = False Then
                            LabelCCNumber.CssClass = "redAsterick"
                            ErrorLabel.Visible = True
                        End If
                    End If
                End If

                Dim cart As ShoppingCart
                If Session.Item("carts") IsNot Nothing Then
                    cart = Session.Item("carts")
                Else
                    ErrorLabel.Text = "No part available in the current cart. Please add to cart again."
                    ErrorLabel.Visible = True
                    Return
                End If

                If cart.Type <> ShoppingCart.enumCartType.Download Then
                    'download only item don't need to fill in ship to info
                    If String.IsNullOrEmpty(TextBoxNameShip.Text) Then
                        ErrorLabel.Visible = True
                        LabelNameShip.CssClass = "redAsterick"
                        LabelName2Star.Text = "<span class=""redAsterick"">*</span>"
                    End If
                    If TextBoxLineShip.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelLine1Ship.CssClass = "redAsterick"
                        LabelLine1ShipStar.Text = "<span class=""redAsterick"">Ship To Street Address must be 35 characters or less in length.</span>"
                        TextBoxLineShip.Focus()
                    Else
                        'Dim poBoxPattern As New Regex("P\s*O\s*BOX|P.O.\s*", RegexOptions.IgnoreCase) 'PO Box or P.O. are not allowed
                        ' the validation should not affect addresses which has p followed by o as in Proctor
                        ' removed * for the issue - Deepa V , May 25, 2006
                        Dim poBoxPattern As New Regex("P\sO\sBOX|PO\sBOX|P\.O\s*|P\sO\.\s*|PO\.\sBOX", RegexOptions.IgnoreCase)
                        If poBoxPattern.IsMatch(TextBoxLineShip.Text) Then
                            ErrorLabel.Visible = True
                            LabelLine1Ship.CssClass = "redAsterick"
                            LabelLine1ShipStar.Text = "<span class=""redAsterick"">*</span>"
                            Me.ErrorLabel.Text = "Orders can not be shipped to a PO Box. Please enter a street address."
                            TextBoxLineShip.Focus()
                        End If
                    End If

                    If Not String.IsNullOrEmpty(TextBoxLine2Ship.Text) Then
                        'Dim poBoxPattern As New Regex("P.?O.?\s*BOX", RegexOptions.IgnoreCase)
                        ' the validation should not affect addresses which has p followed by o as in Proctor
                        ' removed * for the issue - Deepa V , May 25, 2006
                        If TextBoxLine2Ship.Text.Length > 35 Then
                            ErrorLabel.Visible = True
                            LabelLine2Ship.CssClass = "redAsterick"
                            LabelLine2ShipStar.Text = "<span class=""redAsterick"">Ship To Address 2nd Line must be 35 characters or less in length.</span>"
                            TextBoxLine2Ship.Focus()
                        End If

                        Dim poBoxPattern As New Regex("P\sO\sBOX|PO\sBOX|P\.O\s*|P\sO\.\s*|PO\.\sBOX", RegexOptions.IgnoreCase)
                        If poBoxPattern.IsMatch(TextBoxLine2Ship.Text) Then
                            ErrorLabel.Visible = True
                            Me.ErrorLabel.Text = "Ship to address can not be PO Box."
                            TextBoxLine2Ship.Focus()
                        End If
                    End If

                    If String.IsNullOrEmpty(TextBoxCityShip.Text) Then
                        ErrorLabel.Visible = True
                        LabelCityShip.CssClass = "redAsterick"
                        'LabelCity2Star.Text = "<span class=""redAsterick"">*</span>"'bug2
                    ElseIf TextBoxCityShip.Text.Length > 35 Then
                        ErrorLabel.Visible = True
                        LabelCityShip.CssClass = "redAsterick"
                        LabelCity2Star.Text = "<span class=""redAsterick"">Ship To City must be 35 characters or less in length.</span>"
                        TextBoxCityShip.Focus()
                    End If

                    If DDLStateShip.SelectedIndex = 0 Then
                        ErrorLabel.Visible = True
                        LabelStateShip.CssClass = "redAsterick"
                        LabelState2Star.Text = "<span class=""redAsterick"">*</span>"
                    End If

                    Dim sZip2 As String = TextBox5.Text.Trim()
                    If Not objZipCodePattern.IsMatch(sZip2) Then
                        ErrorLabel.Visible = True
                        ZipShip.CssClass = "redAsterick"
                        LabelZip2Star.Text = "<span class=""redAsterick"">*</span>"
                    End If
                End If
                'bill_to.Name = TextBoxName.Text
                'bill_to.Attn = TextboxAttn.Text
                'ship_to.Name = TextBoxNameShip.Text
                'ship_to.Attn = TextboxShipAttn.Text

                If TextBoxNameShip.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelNameShip.CssClass = "redAsterick"
                    LabelName2Star.Text = "<span class=""redAsterick"">Ship To Name must be 35 characters or less in length.</span>"
                End If

                If TextboxShipAttn.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine1ShipAttention.CssClass = "redAsterick"
                    LabelLine1ShipAttention.Text = "<span class=""redAsterick"">Ship To Attention must be 35 characters or less in length.</span>"
                End If

                If TextBoxLineShip.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine1Ship.CssClass = "redAsterick"
                    LabelLine1ShipStar.Text = "<span class=""redAsterick"">Ship To Street Address must be 35 characters or less in length.</span>"
                    TextBoxLineShip.Focus()
                End If

                If TextBoxLine2Ship.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelLine2Ship.CssClass = "redAsterick"
                    LabelLine2ShipStar.Text = "<span class=""redAsterick"">Ship To Address 2nd Line must be 35 characters or less in length.</span>"
                    TextBoxLine2Ship.Focus()
                End If

                If TextBoxCityShip.Text.Length > 35 Then
                    ErrorLabel.Visible = True
                    LabelCityShip.CssClass = "redAsterick"
                    LabelCity2Star.Text = "<span class=""redAsterick"">Ship To City must be 35 characters or less in length.</span>"
                    TextBoxCityShip.Focus()
                End If

                If String.IsNullOrEmpty(txtPhoneShip.Text.Trim) Then
                    ErrorLabel.Visible = True
                    PhoneShip.CssClass = "redAsterick"
                    LabelShipPhoneStar.Text = "<span class=""redAsterick"">*</span>"
                Else
                    Dim objHPhoneNumber As New Regex("(\d{3}[-]{1}\d{3}[-]{1}\d{4})")
                    If Not objHPhoneNumber.IsMatch(txtPhoneShip.Text) Then
                        ErrorLabel.Visible = True
                        PhoneShip.CssClass = "redAsterick"
                        LabelShipPhoneStar.Text = "<span class=""redAsterick"">*</span>"
                        CCErrorLabel.Text = "Invalid Phone number."
                    End If
                End If

                Dim sExtension As String = Me.txtShipExtension.Text.Trim.ToString()
                If sExtension.Length > 0 Then
                    Dim objExt As New Regex("^[0-9]*$")
                    If Not objExt.IsMatch(sExtension) Or sExtension.Length > 6 Then
                        CCErrorLabel.Text = "Extension must be numeric and 6 or fewer digits."
                    End If
                End If

                If chkSaveCard.Checked Then
                    Dim isControlValid As Boolean = False
                    Dim thisPattern As String = "^[a-zA-Z0-9]*$"
                    Dim thispatternsplchar As String = "[!#-$ %*=+:;,?~@^&()_{}{}|\'<>?/]"

                    If TextBoxNName.Text.Length > 1 And TextBoxNName.Text.Length <= 17 Then
                        If DDLCCName.Items.FindByText(TextBoxNName.Text.ToString()) Is Nothing Or TextBoxNName.Text.ToString() = DDLCCName.SelectedValue.ToString() Then
                            If Not Regex.IsMatch(TextBoxNName.Text, thisPattern) = True Then
                                isControlValid = True
                                pnlNickName.Visible = True
                            End If
                            If Regex.IsMatch(TextBoxNName.Text, thispatternsplchar) = True Then
                                isControlValid = True
                                pnlNickName.Visible = True
                                Me.ErrorLabel.Text = "Please enter Nickname without any spaces or special characters."
                                LabelNName.CssClass = "redAsterick"
                            End If
                        Else
                            CCErrorLabel.Text = "You must select a unique Credit Card Nickname." + vbCrLf
                            pnlNickName.Visible = True
                            LabelNName.CssClass = "redAsterick"
                        End If
                    Else
                        CCErrorLabel.Text = "Nicknames must be between 1 and 17 characters." + vbCrLf
                        pnlNickName.Visible = True
                        LabelNName.CssClass = "redAsterick"
                    End If

                    If isControlValid = True Then
                        ErrorLabel.Visible = True
                        LabelNName.CssClass = "redAsterick"
                        'LabelNNameStar.Text = "<span class=""redAsterick"">*</span>"
                    End If
                End If
                'If txtCVV.Text = "" Then
                '    ErrorLabel.Visible = True
                '    lblCVV.CssClass = "redAsterick"
                'End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ''' <summary>
        ''' For a newly-registered account with an SAP buyer account attached, we have to notify
        ''' them that they must wait for registration to be validated, but can make a CC order instead.
        ''' </summary>
        Private Sub ShowPendingAccountMessage()
            If CCErrorLabel IsNot Nothing Then
                CCErrorLabel.Text = "Your Sony account has not yet been validated, you can purchase on credit card and receive list pricing. If you have any questions please contact our customer service group at 1-800-538-7550."
            End If
        End Sub
#End Region

        Private Sub populateYearddl()
            Try
                Dim currentDate As Date = DateTime.Today()
                Dim currentYear As Integer
                currentYear = currentDate.Year()
                ddlYear.Items.Clear()
                txtYear.Text = ""

                'If Me.ddlMonth.SelectedIndex > 0 Then
                '    Dim selectedMonth As Integer = Me.ddlMonth.SelectedValue
                '    If selectedMonth < currentDate.Month Then
                '        currentYear += 1
                '    End If
                '    Dim i As Integer
                '    For i = currentYear To currentYear + 20
                '        ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
                '    Next i
                'End If

                Dim i As Integer
                For i = currentYear To currentYear + 20
                    ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
                Next i

                Dim list0 As ListItem = New ListItem
                list0.Value = "0"
                list0.Text = "Year"
                ddlYear.Items.Insert(0, list0)
                ddlYear.CssClass = "tableData"
                ddlYear.SelectedValue = "0"
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub ddlMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            populateYearddl()
        End Sub

        Public Function getAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = ""
            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Int16 = thisCCNo.Length
                returnValue = thisCCNo.Substring(0, 4)
                returnValue = returnValue.PadRight((ccNumberLength - 4), "X")
                returnValue &= thisCCNo.Substring((ccNumberLength - 4), 4)
            End If

            Return returnValue
        End Function

        Public Function selectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)

            Return creditNo
        End Function

        Private Sub formFieldInitialization()
            'Added by Sunil on 14-12-09 for [Bug 2097] New: PCI Event Log
            'Form1.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            Form1.Attributes("onkeydown") = "SetTheFocusButton(event, 'btnNext')"
            'TextBoxName.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextboxAttn.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxLine1.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxLine2.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxCity.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxZip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxPO.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxCCNumber.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxNName.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxNameShip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextboxShipAttn.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxLineShip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxLine2Ship.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBoxCityShip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'TextBox5.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'txtPhoneShip.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
            'txtShipExtension.Attributes("onkeydown") = "SetTheFocusButton(event, 'ImageButton1')"
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            'chkSameShip.Attributes("onchange") = "copyAddress();"
            'If (DDLCCName.SelectedIndex = 0) Then
            '    If (ErrorLabel.Text.Length > 0 Or CCErrorLabel.Text.Length > 0) Then
            '        'txtCVV.Text = ""
            '        'TextBoxCCNumber.Text = ""
            '    End If
            'End If
        End Sub

        Protected Sub DDLStateShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLStateShip.SelectedIndexChanged
            Try
                tbCalifornia.Visible = False
                If Not String.IsNullOrEmpty(customer.Enabledocumentidnumber) And customer.UserType = "A" Then
                    If DDLStateShip.Text = "CA" Then tbCalifornia.Visible = True
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidden.Click
            Dim objPCILogger As New PCILogger() '6524
            Dim errorMessage As String = String.Empty

            pnlNickName.Visible = True
            pnlCCInfo.Visible = True
            pnlSavedCard.Visible = False
            TextBoxNName.Text = ""
            TextBoxNName.Enabled = True
            chkSaveCard.Visible = False
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "btnHidden_Click"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "btnHidden_Click Success."

                ' ASleight - Check for null Session variables
                If Session("ExpirationYear") Is Nothing Then
                    errorMessage &= "ExpirationYear Session variable is null.  "
                ElseIf Session("ExpirationMonth") Is Nothing Then
                    errorMessage &= "ExpirationMonth Session variable is null.  "
                ElseIf Session("CreditCardType") Is Nothing Then
                    errorMessage &= "CreditCardType Session variable is null.  "
                ElseIf Session("token") Is Nothing Then
                    errorMessage &= "token Session variable is null.  "
                End If
                If Not String.IsNullOrEmpty(errorMessage) Then Throw New NullReferenceException(errorMessage)

                'Add Expiration Year
                Dim list0 As ListItem = New ListItem("Year", Session("ExpirationYear").ToString())
                ddlYear.CssClass = "tableData"
                ddlYear.SelectedValue = "20" + list0.Value.ToString()

                ' Add Expiration Year to show in Textbox(ddl is hidden)
                txtYear.CssClass = "tableData"
                txtYear.Text = "20" + list0.Value.ToString()

                'Add Expiration Month
                Dim expDate As String
                If (Session("ExpirationMonth").ToString().Length < 2) Then
                    expDate = "0" + Session("ExpirationMonth").ToString()
                Else
                    expDate = Session("ExpirationMonth").ToString()
                End If
                Dim list1 As ListItem = New ListItem(MonthName(expDate), expDate)
                ddlMonth.CssClass = "tableData"
                ddlMonth.SelectedValue = expDate
                'Add Expiration Month to show in Textbox(ddl is hidden)
                txtMonth.CssClass = "tableData"
                txtMonth.Text = ddlMonth.SelectedItem.Text

                'Add CreditCard Type
                Dim card_typesVen() As CreditCardType
                Dim pm As New PaymentManager
                Dim CCType As String

                card_typesVen = pm.GetCreditCardTypes()
                DDLType.Items.Add(New ListItem("Select One", ""))
                For Each cc As CreditCardType In card_typesVen
                    DDLType.Items.Add(New ListItem(cc.Description, cc.CreditCardTypeCode))
                    If cc.Description.ToUpper() = "AMERICAN EXPRESS" Then
                        CCType = "AMEX"
                    Else
                        CCType = cc.Description.ToUpper()
                    End If
                    If CCType = Session("CreditCardType").ToString().ToUpper() Then
                        'Dim list3 As ListItem = New ListItem
                        'list3.Text = cc.Description
                        'list3.Value = cc.CreditCardTypeCode
                        DDLType.CssClass = "tableData"
                        DDLType.SelectedValue = cc.CreditCardTypeCode 'list3.Value.ToString()
                        'Add type to show in Textbox(ddl is hidden)
                        txtCCType.Text = DDLType.SelectedItem.Text
                    End If
                Next

                Dim CCnumber() As String

                CCnumber = Session("token").ToString().Split("-")
                TextBoxCCNumber.Text = "XXXXXXXXXXXX" & CCnumber(2)
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "btnHidden_Click Failed. " & ex.Message
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Protected Sub btnCCSaved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCCSaved.Click
            pnlCCInfo.Visible = True
            pnlSavedCard.Visible = True
        End Sub
    End Class

End Namespace
