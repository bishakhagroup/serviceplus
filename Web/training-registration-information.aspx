<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.training_registration_information" CodeFile="training-registration-information.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Training Institute � <%=strQuote %> Information</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultClientScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="includes/ServicesPLUS.js"></script>
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>

</head>
<body style="margin: 0px; background: #5d7180; color: Black;">
    <form id="form2" method="post" runat="server">
        <center>
            <table width="760" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif')">
                        <img height="25" src="images/spacer.gif" width="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="464" bgcolor="#363d45" height="57" background="images/sp_int_header_top_ServicesPLUS_onepix.gif"
                                                align="right" valign="top">
                                                <br>
                                                <h1 class="headerText">ServicesPLUS&nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="middle" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="font-size: 12pt; font-family: Times New Roman">
                                            <td bgcolor="#f2f5f8">
                                                <h2 id="lblSubHdr" class="headerTitle" style="padding-right: 20px; text-align: right;" runat="server">Registration</h2>
                                            </td>
                                            <td bgcolor="#99a8b5">&nbsp;</td>
                                        </tr>
                                        <tr style="font-size: 12pt; height: 9px;">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="font-size: 12pt; font-family: Times New Roman">
                                <td>
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="height: 21px" width="20" height="21">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td style="height: 21px" valign="top" colspan="2">
                                                <asp:Label ID="labelAction" runat="server" CssClass="redAsterick" Width="680px" Height="12px"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="20" height="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td colspan="2">
                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td width="20" height="254" style="height: 254px">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td width="670" height="254" style="height: 254px">
                                                <table id="Table1" style="width: 600px; height: 216px" cellpadding="0"
                                                    width="600" border="0" role="presentation">
                                                    <tr>
                                                        <td style="width: 143px; height: 22px" align="right" colspan="1" rowspan="1">
                                                            <asp:Label ID="Label1" runat="server" CssClass="tableData">Order Number:</asp:Label></td>
                                                        <td style="height: 22px">&nbsp;<asp:Label ID="LabelOrderNumber" runat="server" CssClass="tableData"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 143px; height: 22px" align="right">
                                                            <asp:Label ID="Label2" runat="server" CssClass="tableData">Course Number:</asp:Label></td>
                                                        <td style="height: 22px">&nbsp;<asp:Label ID="LabelCourseNumber" runat="server" CssClass="tableData"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 143px; height: 24px" align="right">
                                                            <asp:Label ID="Label3" runat="server" CssClass="tableData">Course Title:</asp:Label>
                                                        </td>
                                                        <td style="height: 24px">
                                                            &nbsp;<asp:Label ID="LabelCourseTitle" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trStartDate">
                                                        <td style="width: 143px; height: 22px" align="right">
                                                            <asp:Label ID="Label4" runat="server" CssClass="tableData">Start Date:</asp:Label>
                                                        </td>
                                                        <td style="height: 22px">
                                                            &nbsp;<asp:Label ID="LabelStartDate" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trEndDate">
                                                        <td style="width: 143px; height: 23px" align="right">
                                                            <asp:Label ID="Label5" runat="server" CssClass="tableData">End Date:</asp:Label>
                                                        </td>
                                                        <td style="height: 23px">
                                                            &nbsp;<asp:Label ID="LabelEndDate" runat="server" CssClass="tableData" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trTrainingTime">
                                                        <td style="width: 143px; height: 23px" align="right">
                                                            <asp:Label ID="Label7" runat="server" CssClass="tableData">Training Time:</asp:Label></td>
                                                        <td style="height: 23px">&nbsp;<asp:Label ID="lblTrainingTime" runat="server" CssClass="tableData"></asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 143px; height: 65px" valign="top" align="right" colspan="1" rowspan="1">&nbsp;<asp:Label ID="Label6" runat="server" CssClass="tableData">Location:</asp:Label></td>
                                                        <td valign="top" style="height: 65px">
                                                            <table id="TableLocation" width="300" height="64" border="0" role="presentation">
                                                                <tr>
                                                                    <td style="height: 20px" valign="top" colspan="1" rowspan="1">
                                                                        &nbsp;<asp:Label ID="LabelLocationName" runat="server" CssClass="tableData"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 21px" valign="top">&nbsp;<asp:Label ID="LabelLocationAddr1" runat="server" CssClass="tableData" Width="200px"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 20px" valign="top">&nbsp;<asp:Label ID="LabelLocationAddr2" runat="server" CssClass="tableData" Width="224px"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 17px" valign="top">&nbsp;<asp:Label ID="LabelLocationCityStateZip" runat="server" CssClass="tableData" Width="248px"></asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height: 19px" valign="top">&nbsp;<asp:Label ID="LabelLocationCountry" runat="server" CssClass="tableData"></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td height="254" style="height: 254px; width: 20px;"></td>
                                        </tr>
                                        <tr>
                                            <td style="height: 33px" width="20"></td>
                                            <td style="height: 33px" width="670">
                                                <asp:Table ID="TableParticipant" runat="server" Width="656px"></asp:Table>
                                            </td>
                                            <td style="height: 33px; width: 20px;"></td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                            </td>
                                            <td width="670">
                                                &nbsp;&nbsp;
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 19px" width="20">
                                            </td>
                                            <td style="height: 19px" width="670">
                                                <a class="promoCopyBold" href="sony-training-catalog.aspx">Return to Training Catalog</a>
                                            </td>
                                            <td style="height: 19px">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="font-size: 12pt; font-family: Times New Roman">

                                <td>
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img height="20" src="images/spacer.gif" width="25" alt="">
                    </td>
                </tr>
            </table>
        </center>
    </form>
</body>
</html>
