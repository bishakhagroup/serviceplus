<%@ Reference Page="~/SSL.aspx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.sony_parts"
    EnableViewStateMac="true" CodeFile="sony-parts.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SPSPromotion" Src="UserControl\SPSPromotion.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html lang="<%=PageLanguage %>">
<head>
    <title>
        <%=Resources.Resource.ttl_Parts%>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords" />
    <link href="includes/ServicesPLUS_style.css" rel="stylesheet" type="text/css" />
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />

    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>

    <script type="text/javascript">
        //Added and Modified by Sneha on 17th oct 2013 for showing popup
        //code written by Umesh Dubey
        function OrderLookUp(cnt, models, iskit) {
            var lstModels = new Array(models.split(','));

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divMessage").append(' <br/>');
            $("divMessage").dialog("destroy");
            $("#divMessage").dialog({ // dialog box
                title: '<span class="modalpopup-title">' + '<%= Resources.Resource.el_Pts_ModelRenamed%>' + ' </span>',
                       height: 260,
                       width: 560,
                       modal: true,
                       position: 'top',
                       close: function () {
                           redirectTo(iskit);
                       }
                   });
                   var content = "<span class='modalpopup-content'>" +'<%= Resources.Resource.el_Pts_modelConflict%>' + "</span>"
                   content += "<br/>" + "<table class='modalpopup-content' style='margin-left:100px;'>"
                   content += '<tr><td width="100px"><u>' +'<%= Resources.Resource.el_Pts_OldModelName%>' + '</u></td><td width="100px"><u>' +'<%= Resources.Resource.el_Pts_NewModelName%>' + '</u></td></tr>';
                   for (i = 0; i < cnt; i++) {
                       var lstmodel = new Array(lstModels[0][i].split(':'));
                       content += '<tr><td>' + lstmodel[0][0] + '</td><td>' + lstmodel[0][1] + '</td></tr>';
                   }
                   content += "</table>"
                   $("#divMessage").append(content); //message to display in divmessage div
                   $("#divMessage").append('<br/> <br/><a ><img src ="<%=Resources.Resource.img_btnCloseWindow()%>" alt ="Close" onclick="javascript:return ClosePopUp();" id="messagego" /></a>');
            return false;
        }

        function ClosePopUp() {
            $("#divMessage").dialog("close");
        }
        function redirectTo(iskit) {
            var url = window.location.href;
            var finalSlashLength = url.substring(url.lastIndexOf('/') + 1).length;
            var newUrl = url.substring(0, url.length - finalSlashLength);
            if (!iskit)
                newUrl += "PartsPLUSResults.aspx?stype=models";
            else
                newUrl += "PartsPLUSResults.aspx?stype=kits";
            window.location.href = newUrl;
            return true;
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto; font-weight: bold;"></div>
    <center>
        <form id="Form1" method="post" runat="server">
            <table width="760" border="0" role="presentation">
                <tr>
                    <td style="width: 25px; background: url(images/sp_left_bkgd.gif);">
                        &nbsp;
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td>
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td style="width: 464px; height: 82px; background: #363d45 url('images/Parts_Banner.JPG'); text-align: right; vertical-align: middle;">
                                                <h1 class="headerText"><%=Resources.Resource.el_Pts_MainHeading%></h1>
                                            </td>
                                            <td valign="top" width="174" bgcolor="#363d45">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="418" bgcolor="#f2f5f8">
                                                <table height="284" width="448" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt="">
                                                        </td>
                                                        <td width="427">
                                                            <h2 class="headerTitle"><%=Resources.Resource.el_Pts_SubHeading %></h2><br />
                                                            <img height="2" src="images/spacer.gif" width="20" alt="">
                                                            <table height="236" width="433" border="0" role="presentation">
                                                                <tr>
                                                                    <td valign="top" width="190" style="height: 236px">
                                                                        <table width="190" border="0" role="presentation">
                                                                            <tr>
                                                                                <td>
                                                                                    <h3 class="finderSubhead"><%=Resources.Resource.el_Pts_H1%></h3><br />
                                                                                    <span class="finderCopyDark"><%=Resources.Resource.el_Pts_P1%></span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="height: 15px;">
                                                                                <td>
                                                                                    <asp:Label ID="Search1ErrorLabel" runat="server" CssClass="redAsterick" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="height: 12px;">
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtModelNumber"><%=Resources.Resource.el_ModelNumber%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtModelNumber" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <img height="5" src="images/spacer.gif" width="4" alt=""><br />
                                                                                    <asp:ImageButton ID="btnSearchModel" ImageUrl='<%$ Resources:Resource,img_btnSearch%>'
                                                                                        AlternateText="Search Parts Finder" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="18" style="height: 236px">
                                                                        <img height="1" src="images/spacer.gif" width="9" alt="">
                                                                    </td>
                                                                    <td valign="top" width="190" style="height: 236px">
                                                                        <table width="190" border="0" role="presentation">
                                                                            <tr>
                                                                                <td>
                                                                                    <h3 class="finderSubhead"><%=Resources.Resource.el_Pts_Search%></h3><br />
                                                                                    <span class="finderCopyDark"><%=Resources.Resource.el_Pts_Search_p1%></span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="15">
                                                                                    <asp:Label ID="Search2ErrorLabel" runat="server" CssClass="redAsterick" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtPrefix"><%=Resources.Resource.el_Pts_Srch_Model%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtPrefix" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtPartNumber"><%=Resources.Resource.el_Pts_Srch_Part%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtPartNumber" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <label class="finderCopyDark" for="txtDescription"><%=Resources.Resource.el_Pts_Srch_Desc%></label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtDescription" runat="server" CssClass="finderCopyDark" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <img src="images/spacer.gif" height="5" width="4" alt=""><br />
                                                                                    <asp:ImageButton ID="btnSearchPart" runat="server" ImageUrl="<%$ Resources:Resource,img_btnSearch%>"
                                                                                        AlternateText="<%$ Resources:Resource, el_Pts_Srch%>" ToolTip="<%$ Resources:Resource, el_Pts_Srch%>" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="7" style="height: 236px">
                                                                        <img src="images/spacer.gif" height="10" width="10" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img src="images/spacer.gif" width="20" alt="">
                                                                    </td>
                                                                    <td width="18">
                                                                        <img src="images/spacer.gif" height="1" width="9" alt="">
                                                                    </td>
                                                                    <%--<td><a href="sony-part-catalog.aspx" class="finderSubhead" runat="server" visible="true" >Part Catalog</a></td>--%>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td valign="top" width="174" bgcolor="#99a8b5" style="width: 174px; background: #99a8b5; vertical-align: middle; padding: 0px 10px;">
                                                <span class="finderCopyDark" style="color: white;"><%=Resources.Resource.el_Pts_QuickInfo1%></span><br />
                                                <br />
                                                <span class="finderSubhead" style="color: white;"><a href="#" onclick="window.location.href='SignIn.aspx?Lang=<%=LanguageSM %>&post=qo'; return false;">
                                                    <%=Resources.Resource.el_Login%></a> <%=Resources.Resource.el_Pts_QuickInfo2%></span>
                                            </td>
                                        </tr>
                                        <tr style="height: 9px;">
                                            <td style="width: 464px; background: #f2f5f8 url(images/sp_int_header_btm_left_onepix.gif) repeat-x;">
                                                <img src="images/spacer.gif" height="1" alt="" />
                                            </td>
                                            <td style="width: 246px; background: #99a8b5 url(images/sp_int_header_btm_right.gif) repeat-x;">
                                                <img src="images/spacer.gif" height="1" alt="" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="lblError" runat="server" CssClass="redAsterick" />
                                    <asp:Label ID="lblNotFound" runat="server" CssClass="redAsterick" />
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td width="20" height="20">
                                                <img src="images/spacer.gif" height="20" width="20" alt="">
                                            </td>
                                            <td width="670" height="20">
                                                <img src="images/spacer.gif" height="20" width="670" alt="">
                                            </td>
                                        </tr>
                                        <%If divspShow = True Then%>
                                        <tr>
                                            <td width="10">
                                                <img height="10" src="images/spacer.gif" width="10" alt="">
                                            </td>
                                            <td colspan="2">
                                                <table border="0" role="presentation">
                                                    <tr>
                                                        <td>
                                                            <ServicePLUSWebApp:SPSPromotion ID="promo1" DisplayPage="sony-parts" Type="2" runat="server" />
                                                        </td>
                                                        <td width="16">
                                                            <img src="images/spacer.gif" width="15" alt="">
                                                        </td>
                                                        <td>
                                                            <ServicePLUSWebApp:SPSPromotion ID="promo2" DisplayPage="sony-parts" Type="3" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%End If%>
                                    </table>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center">
                                    <table align="center" border="0" role="presentation">
                                        <tr align="center">
                                            <td align="center">
                                                <a class="promoCopyBold" href="sony-service-repair-maintenance.aspx"><%=Resources.Resource.el_ServiceModelList%></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="700">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 25px; background: url(images/sp_right_bkgd.gif);">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
