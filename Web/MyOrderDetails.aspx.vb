Imports System.Data
Imports System.IO
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities
'<-------------------------------------------------------------------------------------------->
'<Modified>    : Rujuta on 14th Dec 2009 for BUG : 2096
'<Description> : Changes done for PCI Event Log . 
'              : Modification done under buildPartsOrderBillToShipToTable and buildTrainingOrderBillToShipToTable> 
'<-------------------------------------------------------------------------------------------->

Namespace ServicePLUSWebApp


    Partial Class MyOrderDetails
        Inherits SSL

        Public Auditcustomer As Customer 'Added by Narayanan July 31 for AuditTrail

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents BackOrderedItemsTable As Table


        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            isProtectedPage(True)
            InitializeComponent()
        End Sub

#End Region

#Region "page members"

        'Private Const conMyOrdersPage As String = "MyOrders.aspx"'7008
        Private Const conRaRequestPage As String = "RARequest.aspx"
        Private Const conCancelOrderItemPage As String = "CancelOrderItemPage.aspx"
        Private Const conRowColorWhite As String = "white"
        Private Const conRowColorLightBlue As String = "#f2f5f8"
        Private Const conCancelBackOrderedItemFieldName As String = "CancelBackOrderedItem"
        Private Const conFileDownloadPage As String = "FileDownloadArea.aspx"

        Public customer As Customer
        Private m_cr As CourseReservation
        Private strOrderQueryString() As String
        Private conSoftwareDownloadPath As String

#End Region

#Region "Events"

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            Try
                SetFileDownloadPaths()
                If HttpContextManager.Customer IsNot Nothing Then customer = HttpContextManager.Customer

                If Not IsPostBack Then
                    ProcessPage(sender, e)
                    hOrderId.Value = Request.QueryString("order")
                End If

                If Convert.ToBoolean(hCancelEvent.Value) Then
                    Dim thisOrder As Order
                    strOrderQueryString = hOrderId.Value.Split(",".ToCharArray())
                    If Not String.IsNullOrEmpty(strOrderQueryString(1)) Then
                        thisOrder = GetOrder(strOrderQueryString(1))
                        CancelBackOrderedItem(hLineItem.Value.Replace(",", "").Trim(), thisOrder)
                    End If
                End If
                Dim om As New OrderDataManager
                Dim strURL As String = om.GetSpecialTaxURL("USA", "CA", "*", "Recycling")
                specialTaxHyperLink.Target = "_blank"
                specialTaxHyperLink.NavigateUrl = strURL
                specialTaxHyperLink.Text = strURL
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#End Region

#Region "Methods"

        Private Sub ProcessPage(ByVal sender As Object, ByVal e As EventArgs)
            Dim objPCILogger As New PCILogger()
            'Dim objUtilties As Utilities = New Utilities
            Dim thisOrder As Order
            Dim redirectUser As Boolean = True
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "processPage"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "processPage Success."

                If Request.QueryString("order") IsNot Nothing Then
                    strOrderQueryString = Request.QueryString("order").Split(",".ToCharArray())
                    thisOrder = GetOrder(strOrderQueryString(1))
                    If Not thisOrder Is Nothing Then
                        'Added the below condition by Arshad to fix the Order details issue.
                        Dim validOrder As Boolean = False
                        Dim customerOrders As DataSet = New OrderManager().GetCustomerOrders(customer.CustomerID)
                        If Not customerOrders Is Nothing Then
                            For Each DataRow As DataRow In customerOrders.Tables(0).Rows
                                If thisOrder.OrderNumber = Convert.ToString(DataRow("OrderNumber")) Then
                                    validOrder = True
                                    Exit For
                                End If
                            Next
                        End If
                        If validOrder = True Then
                            thisOrder.Customer = customer
                            WasCancelItemClicked(thisOrder)
                            If thisOrder.OrderNumber.StartsWith("TRN") Then
                                BuildTrainingOrderBillToShipToTable(thisOrder)
                                BuildTrainingOrderedItemsTable(thisOrder)
                            Else
                                BuildPartsOrderBillToShipToTable(thisOrder)
                                BuildOrderedItemsTable(thisOrder)
                                BuildInvoicesTable(thisOrder)
                            End If
                        Else
                            errorMessageLabel.Text = "You're not authorised to view details for order: " + thisOrder.OrderNumber
                        End If
                    End If
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "processPage Failed. " & ex.Message.ToString()
                If (ex.Message.Contains("The remote name could not be resolved") Or ex.Message.Contains("The operation has timed out")) Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    errorMessageLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = errorMessageLabel.Text.Replace(errorMessageLabel.Text, "Operation to view order details timed out.Please try again or <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> contact us </a> for assistance and mention the incident number")
                    errorMessageLabel.Text = er + " " + strUniqueKey + "."
                ElseIf (ex.Message.Contains("Error in the application")) Then
                    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0 "
                    Dim strUniqueKey As String = Utilities.GetUniqueKey
                    errorMessageLabel.Text = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                    Dim er As String
                    er = errorMessageLabel.Text.Replace(errorMessageLabel.Text, "Unexpected error encountered.Please try again or <a  style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');""> contact us </a> for assistance and mention the incident number")
                    errorMessageLabel.Text = er + " " + strUniqueKey + "."
                Else
                    errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                End If
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try
        End Sub

        Private Function GetOrder(ByVal OrderNumber As String) As Order
            Dim thisOrder As Order
            Dim doesOrderExistInSession As Boolean = False
            Dim objPCILogger As New PCILogger()
            Dim orderMan As New OrderManager

            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "getOrder"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "getOrder Success."

                If OrderNumber.StartsWith("TRN") Then
                    thisOrder = orderMan.GetOrder(OrderNumber, False, False, HttpContextManager.GlobalData)
                    orderMan.GetOrderInvoices(thisOrder)
                    Dim cm As New CourseManager
                    m_cr = cm.GetReservation(thisOrder.OrderNumber)
                Else
                    thisOrder = orderMan.GetOrder(OrderNumber, True, False, HttpContextManager.GlobalData)
                End If
                If thisOrder IsNot Nothing Then Session.Item("order") = thisOrder

                objPCILogger.PushLogToMSMQ()
                Return thisOrder
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "getOrder Failed. " & ex.Message
                objPCILogger.PushLogToMSMQ()
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                Return Nothing
            End Try
        End Function

        Private Function GetCreditCardDataForOrder(ByRef order As Order) As CreditCard
            Dim thisCreditCard As CreditCard = Nothing
            Dim objPCILogger As New PCILogger() '6524
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "getCreditCardDataForOrder"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventType = EventType.View
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"
                objPCILogger.Message = "getCreditCardDataForOrder Success."

                Dim pmDataManager As New PaymentDataManager()
                If (Not order.CreditCard Is Nothing) Then
                    thisCreditCard = pmDataManager.GetCustomerCreditCard(order.CreditCard.SequenceNumber)
                End If
            Catch ex As Exception
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "getCreditCardDataForOrder Failed. " & ex.Message.ToString()
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            Finally
                objPCILogger.PushLogToMSMQ()
            End Try

            Return thisCreditCard
        End Function

        Private Function WasCancelItemClicked(ByRef thisOrder As Order) As Boolean
            Dim returnValue As Boolean = False

            If Not Request.Form.Item("__EVENTTARGET") Is Nothing And Not Request.Form.Item("__EVENTARGUMENT") Is Nothing Then
                If Request.Form.Item("__EVENTTARGET").ToString() = conCancelBackOrderedItemFieldName.ToString() Then CancelBackOrderedItem(Request.Form.Item("__EVENTARGUMENT").ToString(), thisOrder)
                returnValue = True
            End If

            Return returnValue
        End Function
#End Region

        '-- build bill to ship to table --
        Private Sub BuildPartsOrderBillToShipToTable(ByVal thisOrder As Order)
            Dim objPCILoggerForCustomerOrder As New PCILogger()
            Try
                Dim td As New TableCell
                Dim tr As New TableRow

                td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""3"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<table width=""310"" border=""0"" cellspacing=""0"" cellpadding=""2"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                '-- your order number is

                If strOrderQueryString(1) = "0" Then
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"">" + Resources.Resource.el_urOrderNo + vbCrLf)) '+"Your Order Number is</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableHeader"">&nbsp;"))
                    If Not thisOrder.OrderNumber Is Nothing Then
                        td.Controls.Add(New LiteralControl(strOrderQueryString(2)))
                    Else
                        td.Controls.Add(New LiteralControl(""))
                    End If
                Else
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"">" + Resources.Resource.el_UrOrderWas + "</td>" + vbCrLf)) ' +"Your Order Number was</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableHeader"">&nbsp;"))
                    td.Controls.Add(New LiteralControl(strOrderQueryString(2)))
                End If

                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                If strOrderQueryString(1) = "1" Then
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td class=""tableHeader"">" + Resources.Resource.el_UrNewOrderNumber + "</td>" + vbCrLf)) '"Your new Order Number is</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableHeader"">&nbsp;"))
                    If Not thisOrder.OrderNumber Is Nothing Then
                        td.Controls.Add(New LiteralControl(strOrderQueryString(2)))
                    Else
                        td.Controls.Add(New LiteralControl(""))
                    End If
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                End If

                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<table width=""310"" border=""0"" cellspacing=""0"" cellpadding=""2"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                '-- your po number is
                td.Controls.Add(New LiteralControl("<td class=""tableHeader"">" + Resources.Resource.el_UrPONumber + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""tableHeader"">"))
                If Not thisOrder.POReference Is Nothing Then
                    td.Controls.Add(New LiteralControl(thisOrder.POReference.ToString()))
                Else
                    td.Controls.Add(New LiteralControl(""))
                End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                'td.Controls.Add(New LiteralControl("<td width=""315"" bgcolor=""#d5dee9"" class=""tableHeader"">Billing Info</td>" + vbCrLf))'8060
                td.Controls.Add(New LiteralControl("<td width=""315"" bgcolor=""#d5dee9"" class=""tableHeader"">" + Resources.Resource.el_BillingInformation + "</td>" + vbCrLf)) '8060
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                'td.Controls.Add(New LiteralControl("<td width=""315"" bgcolor=""#d5dee9"" class=""tableHeader"">Shipping Info</td>" + vbCrLf))'8060
                td.Controls.Add(New LiteralControl("<td width=""315"" bgcolor=""#d5dee9"" class=""tableHeader"">" + Resources.Resource.el_ShippingInformation + "</td>" + vbCrLf)) '8060
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" class=""bodyCopy"">"))

                '-- bill to info -- if credit card information exist then display else purchased on account so display the billing info. 
                Dim thisCreditCard As CreditCard
                If thisOrder.CreditCard Is Nothing Then
                    thisCreditCard = GetCreditCardDataForOrder(thisOrder)
                Else
                    thisCreditCard = thisOrder.CreditCard
                End If
                objPCILoggerForCustomerOrder.CustomerID = thisOrder.Customer.CustomerID
                objPCILoggerForCustomerOrder.CustomerID_SequenceNumber = thisOrder.Customer.SequenceNumber
                objPCILoggerForCustomerOrder.EmailAddress = customer.EmailAddress.ToString() '6524
                objPCILoggerForCustomerOrder.SIAM_ID = customer.SIAMIdentity.ToString()
                objPCILoggerForCustomerOrder.LDAP_ID = customer.LdapID.ToString() '6524
                objPCILoggerForCustomerOrder.EventOriginApplication = "ServicesPLUS"
                objPCILoggerForCustomerOrder.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILoggerForCustomerOrder.EventOriginMethod = "buildPartsOrderBillToShipToTable"
                objPCILoggerForCustomerOrder.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILoggerForCustomerOrder.OperationalUser = Environment.UserName
                objPCILoggerForCustomerOrder.EventType = EventType.View
                objPCILoggerForCustomerOrder.EventDateTime = Date.Now.ToLongTimeString()
                objPCILoggerForCustomerOrder.HTTPRequestObjectValues = GetRequestContextValue()
                If thisOrder.OrderNumber IsNot Nothing Then objPCILoggerForCustomerOrder.Message = "Order Number : " & thisOrder.OrderNumber.ToString()

                If thisCreditCard IsNot Nothing AndAlso Not String.IsNullOrEmpty(thisCreditCard.CreditCardNumber) Then
                    objPCILoggerForCustomerOrder.CreditCardMaskedNumber = thisCreditCard.CreditCardNumberMasked
                    objPCILoggerForCustomerOrder.CreditCardSequenceNumber = thisCreditCard.SequenceNumber

                    If thisCreditCard.CreditCardNumber <> String.Empty Then
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_BilledToCreditCard + " " + hideCCNumbers(thisCreditCard.CreditCardNumber.ToString()))) '8060
                    Else
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_BilledToCreditCard + "."))
                    End If
                    objPCILoggerForCustomerOrder.IndicationSuccessFailure = "Success"
                    objPCILoggerForCustomerOrder.PushLogToMSMQ()
                Else
                    '-- bill to name
                    If Not thisOrder.BillTo.Name Is Nothing Then td.Controls.Add(New LiteralControl(thisOrder.BillTo.Name.ToString()))
                    td.Controls.Add(New LiteralControl("<br/>" + vbCrLf))

                    If Not thisOrder.BillTo.Line1 Is Nothing Then td.Controls.Add(New LiteralControl(thisOrder.BillTo.Line1.ToString()))
                    If Not thisOrder.BillTo.Line2 Is Nothing Then td.Controls.Add(New LiteralControl("<br/>" + thisOrder.BillTo.Line2.ToString()))
                    td.Controls.Add(New LiteralControl("<br/>" + vbCrLf))

                    '-- bill to city, state, and zip
                    If Not thisOrder.BillTo.City Is Nothing And Not thisOrder.BillTo.State Is Nothing And Not thisOrder.BillTo.PostalCode Is Nothing Then
                        td.Controls.Add(New LiteralControl(thisOrder.BillTo.City.ToString() + " " + thisOrder.BillTo.State.ToString() + " " + thisOrder.BillTo.PostalCode.ToString()))
                    End If
                End If

                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" class=""bodyCopy"">"))
                '-- ship to name
                If thisOrder.ShipTo.Name IsNot Nothing Then td.Controls.Add(New LiteralControl(thisOrder.ShipTo.Name.ToString()))
                td.Controls.Add(New LiteralControl("<br/> " + vbCrLf))

                If Not thisOrder.ShipTo.Attn Is Nothing Then td.Controls.Add(New LiteralControl("<br/>" + thisOrder.ShipTo.Attn.ToString()))

                '-- ship to address
                If thisOrder.ShipTo.Line1 IsNot Nothing Then td.Controls.Add(New LiteralControl(thisOrder.ShipTo.Line1.ToString()))
                If thisOrder.ShipTo.Line2 IsNot Nothing Then td.Controls.Add(New LiteralControl("<br/>" + thisOrder.ShipTo.Line2.ToString()))
                td.Controls.Add(New LiteralControl("<br/>" + vbCrLf))
                '-- ship to city, state, and zip
                If thisOrder.ShipTo.City IsNot Nothing And thisOrder.ShipTo.State IsNot Nothing And thisOrder.ShipTo.PostalCode IsNot Nothing Then
                    If Not thisOrder.ShipTo.City = "-" Then
                        td.Controls.Add(New LiteralControl(thisOrder.ShipTo.City.ToString() + " " + thisOrder.ShipTo.State.ToString() + " " + thisOrder.ShipTo.PostalCode.ToString()))
                    Else
                        td.Controls.Add(New LiteralControl(thisOrder.ShipTo.State.ToString() + " " + thisOrder.ShipTo.PostalCode.ToString()))
                    End If
                End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

                tr.Controls.Add(td)
                OrderNumberTable.Controls.Add(tr)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILoggerForCustomerOrder.IndicationSuccessFailure = "Failure"
                objPCILoggerForCustomerOrder.Message = errorMessageLabel.Text
                objPCILoggerForCustomerOrder.PushLogToMSMQ()
            End Try

        End Sub

        Private Sub BuildTrainingOrderBillToShipToTable(ByVal thisOrder As Order)
            Dim objPCILoggerForCustomerOrder As New PCILogger()
            Try
                Dim td As New TableCell
                Dim tr As New TableRow

                td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""3"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<table width=""310"" border=""0"" cellspacing=""0"" cellpadding=""2"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                '-- your order number is
                td.Controls.Add(New LiteralControl("<td class=""headerBig"">" + Resources.Resource.el_urOrderNo + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""headerBig"">&nbsp;"))
                If Not thisOrder.OrderNumber Is Nothing Then
                    td.Controls.Add(New LiteralControl(thisOrder.OrderNumber.ToString()))
                Else
                    td.Controls.Add(New LiteralControl("&nbsp;n/a&nbsp;&nbsp;"))
                End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<table width=""310"" border=""0"" cellspacing=""0"" cellpadding=""2"" role=""presentation"">" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                '-- your po number is
                td.Controls.Add(New LiteralControl("<td class=""headerBig"">" + Resources.Resource.el_UrPONumber + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#f2f5f8"" class=""headerBig"">"))
                If Not thisOrder.POReference Is Nothing Then
                    td.Controls.Add(New LiteralControl(thisOrder.POReference.ToString()))
                Else
                    td.Controls.Add(New LiteralControl("&nbsp;n/a&nbsp;&nbsp;"))
                End If
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315""><img src=""images/spacer.gif"" width=""4"" height=""10"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" bgcolor=""#d5dee9"" class=""tableHeader"">" + Resources.Resource.el_BillingInformation + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" ></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" class=""bodyCopy"">"))

                '-- bill to info -- if credit card information exist then display else purchased on account so display the billing info. 
                Dim thisCreditCard As CreditCard
                If thisOrder.CreditCard Is Nothing Then
                    thisCreditCard = GetCreditCardDataForOrder(thisOrder)
                Else
                    thisCreditCard = thisOrder.CreditCard
                End If

                objPCILoggerForCustomerOrder.CustomerID = thisOrder.Customer.CustomerID
                objPCILoggerForCustomerOrder.CustomerID_SequenceNumber = thisOrder.Customer.SequenceNumber
                objPCILoggerForCustomerOrder.EmailAddress = customer.EmailAddress.ToString()
                objPCILoggerForCustomerOrder.SIAM_ID = customer.SIAMIdentity.ToString()
                objPCILoggerForCustomerOrder.LDAP_ID = customer.LdapID.ToString()
                objPCILoggerForCustomerOrder.EventOriginApplication = "ServicesPLUS"
                objPCILoggerForCustomerOrder.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILoggerForCustomerOrder.EventOriginMethod = "buildTrainingOrderBillToShipToTable"
                objPCILoggerForCustomerOrder.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILoggerForCustomerOrder.OperationalUser = Environment.UserName
                objPCILoggerForCustomerOrder.EventType = EventType.View
                objPCILoggerForCustomerOrder.EventDateTime = Date.Now.ToLongTimeString()
                objPCILoggerForCustomerOrder.HTTPRequestObjectValues = GetRequestContextValue()
                If Not thisOrder.OrderNumber Is Nothing Then objPCILoggerForCustomerOrder.Message = "Order Number : " & thisOrder.OrderNumber.ToString()

                If Not thisCreditCard Is Nothing Then
                    objPCILoggerForCustomerOrder.CreditCardMaskedNumber = thisCreditCard.CreditCardNumberMasked
                    objPCILoggerForCustomerOrder.CreditCardSequenceNumber = thisCreditCard.SequenceNumber
                    If thisCreditCard.CreditCardNumber <> String.Empty Then
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_BilledToCreditCard + " " + hideCCNumbers(thisCreditCard.CreditCardNumber.ToString())))
                    Else
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_BilledToCreditCard + " N/A"))
                    End If
                    objPCILoggerForCustomerOrder.IndicationSuccessFailure = "Success"
                    objPCILoggerForCustomerOrder.PushLogToMSMQ()
                Else
                    '-- bill to name
                    If Not thisOrder.BillTo.Name Is Nothing Then td.Controls.Add(New LiteralControl(thisOrder.BillTo.Name.ToString()))
                    td.Controls.Add(New LiteralControl("<br/>" + vbCrLf))

                    If Not thisOrder.BillTo.Line1 Is Nothing Then td.Controls.Add(New LiteralControl(thisOrder.BillTo.Line1.ToString()))
                    If Not thisOrder.BillTo.Line2 Is Nothing Then td.Controls.Add(New LiteralControl("<br/>" + thisOrder.BillTo.Line2.ToString()))
                    td.Controls.Add(New LiteralControl("<br/>" + vbCrLf))

                    '-- bill to city, state, and zip
                    If Not thisOrder.BillTo.City Is Nothing And Not thisOrder.BillTo.State Is Nothing And Not thisOrder.BillTo.PostalCode Is Nothing Then
                        td.Controls.Add(New LiteralControl(thisOrder.BillTo.City.ToString() + " " + thisOrder.BillTo.State.ToString() + " " + thisOrder.BillTo.PostalCode.ToString()))
                    End If
                End If

                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""25"">&nbsp;</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td width=""315"" class=""bodyCopy"">"))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</table>" + vbCrLf))
                tr.Controls.Add(td)
                OrderNumberTable.Controls.Add(tr)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
                objPCILoggerForCustomerOrder.IndicationSuccessFailure = "Failure"
                objPCILoggerForCustomerOrder.Message = errorMessageLabel.Text
                objPCILoggerForCustomerOrder.PushLogToMSMQ()
            End Try

        End Sub

#Region "build invoices table"

        Private Sub BuildTrainingInvoicesTable(ByVal thisOrder As Order)
            Try
                Dim haveInvoices As Boolean = False

                If Not thisOrder Is Nothing Then
                    Dim rowColor As String
                    OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeaders())

                    For Each thisInvoice As Invoice In thisOrder.Invoices
                        rowColor = conRowColorWhite

                        '-- invoice number, tracking number, status, date shipped --
                        OrderInvoiceTable.Controls.Add(BuildMainInvoiceTable(thisInvoice))

                        '-- invoice line item headers --
                        OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeader())
                        '5325 starts
                        'For Each thisLineItem As Sony.US.ServicesPLUS.Core.InvoiceLineItem In thisInvoice.LineItems
                        '    'thisLineItem.Invoice.Order.LineItems.GetValue(Integer.Parse(thisLineItem.OrderLineNumber))
                        '    rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                        '    'invoice line item details --
                        '    OrderInvoiceTable.Controls.Add(buildInvoiceDetails(thisLineItem, rowColor))
                        'Next
                        For Each thisLineItem As InvoiceLineItem In thisInvoice.LineItems
                            'thisLineItem.Invoice.Order.LineItems.GetValue(Integer.Parse(thisLineItem.OrderLineNumber))
                            rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                            'invoice line item details --
                            If Not thisLineItem Is Nothing Or thisLineItem Is String.Empty Then '5325
                                If thisInvoice.InvoiceNumber = thisLineItem.InvoiceNumber Then
                                    OrderInvoiceTable.Controls.Add(BuildInvoiceDetails(thisLineItem, rowColor))
                                End If
                            End If '5325
                        Next
                        '5325 ends
                        'invoice totals --
                        OrderInvoiceTable.Controls.Add(BuildInvoiceTotals(thisInvoice))
                    Next

                    If thisOrder.Invoices.Length = 0 Then
                        '-- invoice line item headers --
                        OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeader())
                        OrderInvoiceTable.Controls.Add(BuildNoInvoicesTable())
                    End If

                    OrderInvoiceTable.Controls.Add(BuildInvoiceFooter())
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Sub BuildInvoicesTable(ByVal thisOrder As Order)
            Try
                Dim haveInvoices As Boolean = False

                If Not thisOrder Is Nothing Then
                    Dim rowColor As String
                    OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeaders())
                    If Not thisOrder.Invoices Is Nothing Then
                        For Each thisInvoice As Invoice In thisOrder.Invoices
                            rowColor = conRowColorWhite

                            '-- invoice number, tracking number, status, date shipped --
                            OrderInvoiceTable.Controls.Add(BuildMainInvoiceTable(thisInvoice))

                            '-- invoice line item headers --
                            OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeader())
                            '5325 starts
                            'For Each thisLineItem As Sony.US.ServicesPLUS.Core.InvoiceLineItem In thisInvoice.LineItems
                            '    'thisLineItem.Invoice.Order.LineItems.GetValue(Integer.Parse(thisLineItem.OrderLineNumber))
                            '    rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                            '    'invoice line item details --
                            '    OrderInvoiceTable.Controls.Add(buildInvoiceDetails(thisLineItem, rowColor))
                            'Next

                            For Each thisLineItem As InvoiceLineItem In thisInvoice.LineItems
                                'thisLineItem.Invoice.Order.LineItems.GetValue(Integer.Parse(thisLineItem.OrderLineNumber))
                                rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                                'invoice line item details --
                                If Not thisLineItem Is Nothing Or thisLineItem Is String.Empty Then '5325
                                    If thisInvoice.InvoiceNumber = thisLineItem.InvoiceNumber Then
                                        OrderInvoiceTable.Controls.Add(BuildInvoiceDetails(thisLineItem, rowColor))
                                    End If
                                End If '5325
                            Next

                            '5325 ends
                            'invoice totals --
                            OrderInvoiceTable.Controls.Add(BuildInvoiceTotals(thisInvoice))
                        Next
                    Else
                        OrderInvoiceTable.Controls.Add(BuildInvoiceColumnHeader())
                        OrderInvoiceTable.Controls.Add(BuildNoInvoicesTable())
                    End If
                    OrderInvoiceTable.Controls.Add(BuildInvoiceFooter())
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

        End Sub

        Private Function BuildMainInvoiceTable(ByRef thisInvoice As Invoice) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            Try
                If Not thisInvoice Is Nothing Then
                    td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">"))

                    td.Controls.Add(New LiteralControl("<tr>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""690"" colspan=""11"" align=""center"">"))

                    td.Controls.Add(New LiteralControl("<table width=""660"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" role=""presentation"">" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr bgcolor=""#d5dee9"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td colspan=""10""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""95"" class=""tableHeader"">&nbsp;" + Resources.Resource.el_InvoiceNumber + "</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""75"" class=""tableData"">&nbsp;"))
                    '-- invoice number
                    If Not thisInvoice.InvoiceNumber Is Nothing Then td.Controls.Add(New LiteralControl(thisInvoice.InvoiceNumber.ToString()))
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""100"" class=""tableHeader"">&nbsp;" + Resources.Resource.el_NoTrackNum + "</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""113"" class=""tableData"">&nbsp;"))
                    '-- tracking number
                    'Commented By Navdeep kaur on 18th May for resolving 6396
                    '5325 starts
                    Try
                        If Not thisInvoice.Order.ContainsDownloadableItems = True Then
                            If Not thisInvoice.TrackingNumber Is Nothing Then
                                If thisInvoice.TrackingNumber <> String.Empty Then
                                    If Not thisInvoice.ShippingMethod.TrackingURL Is Nothing Then
                                        'td.Controls.Add(New LiteralControl("<a href=""" + thisInvoice.ShippingMethod.TrackingURL.Replace("TRACKINGNUMBER", thisInvoice.TrackingNumber.ToString()) + """ class=""Body"">"))
                                        'td.Controls.Add(New LiteralControl("<a style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + thisInvoice.ShippingMethod.TrackingURL.Replace("TRACKINGNUMBER", thisInvoice.TrackingNumber.ToString()) + "');""class=""Body"">" + thisInvoice.TrackingNumber.ToString() + "</a>"))
                                        td.Controls.Add(New LiteralControl("<a style=""color:Blue"" href=""#"" onclick=""javascript:window.open('" + thisInvoice.ShippingMethod.TrackingURL.Replace("TRACKINGNUMBER", thisInvoice.TrackingNumber.ToString()) + "');""class=""Body"">"))
                                        td.Controls.Add(New LiteralControl(thisInvoice.TrackingNumber.ToString()))
                                        td.Controls.Add(New LiteralControl("</a>"))
                                    Else
                                        td.Controls.Add(New LiteralControl(Resources.Resource.el_NoTrackURL))
                                    End If
                                Else
                                    td.Controls.Add(New LiteralControl(Resources.Resource.el_EmptyTrackingNo))
                                End If
                            Else
                                td.Controls.Add(New LiteralControl(Resources.Resource.el_NoTrackNum))
                            End If
                        Else
                            td.Controls.Add(New LiteralControl(Resources.Resource.el_NADownload))
                        End If
                    Catch ex As Exception
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_TrackingNoExecp))
                    End Try
                    '5325 ends
                    'End of Comment By Navdeep kaur on 18th May for resolving 6396

                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""45"" class=""tableHeader"">&nbsp;Status</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""65"" class=""tableData"">&nbsp;"))
                    '-- status
                    If Not thisInvoice.Order Is Nothing Then
                        If thisInvoice.Order.ContainsDownloadableItems Then
                            td.Controls.Add(New LiteralControl("N/A"))
                        Else
                            td.Controls.Add(New LiteralControl(Resources.Resource.el_Shipped))
                        End If
                    Else
                        td.Controls.Add(New LiteralControl("No order."))
                    End If
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""80"" class=""tableHeader"">&nbsp;" + Resources.Resource.el_DateShipped + "</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td width=""75"" class=""tableData"">&nbsp;"))
                    '-- date shipped
                    If IsDate(thisInvoice.InvoiceDate) Then
                        If thisInvoice.InvoiceDate.ToString() <> String.Empty Then
                            td.Controls.Add(New LiteralControl(Format(thisInvoice.InvoiceDate, "MM/dd/yyyy")))
                        Else
                            td.Controls.Add(New LiteralControl("n/a"))
                        End If
                    Else
                        td.Controls.Add(New LiteralControl(Resources.Resource.el_NoInvoiceDate))
                    End If

                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr bgcolor=""#d5dee9"">" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("</table>"))

                    td.Controls.Add(New LiteralControl("</td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
                    td.Controls.Add(New LiteralControl("</tr>"))

                    td.Controls.Add(New LiteralControl("</table>"))

                    tr.Controls.Add(td)
                Else
                    errorMessageLabel.Text = Resources.Resource.el_NoInvoiceBuildMain '"No invoice exist - build main invoice table."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        Private Function BuildInvoiceColumnHeader() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table border=""0"" Width=""670"" cellpadding=""0"" cellspacing=""0"" role=""presentation"">" + vbCrLf))
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
            '-- item number (part number)         
            'td.Controls.Add(New LiteralControl("<td width=""95"" class=""tableHeader"" align=""center"">Part Number</td>" + vbCrLf))'5325
            td.Controls.Add(New LiteralControl("<td width=""95"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_itemNumber + "</td>" + vbCrLf)) '5325

            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
            '-- description 
            td.Controls.Add(New LiteralControl("<td width=""240"" class=""tableHeader"">&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_Description + "</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
            '-- quantity
            td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_Quantity + "</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))


            'price difference for CA and US 
            If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                '-- item price
                td.Controls.Add(New LiteralControl("<td width=""85"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_ItemPrice_CAD + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
                '-- total price
                td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_TotalPrice_CAD + "</td>" + vbCrLf))
            Else
                '-- item price
                td.Controls.Add(New LiteralControl("<td width=""85"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_ItemPrice + "</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
                '-- total price
                td.Controls.Add(New LiteralControl("<td width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_TotalPrice + "</td>" + vbCrLf))
            End If

            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td width=""63"" class=""tableHeader"" align=""center"">&nbsp;" + Resources.Resource.el_Return + "</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr

        End Function

        Private Function BuildInvoiceDetails(ByRef thisLineItem As InvoiceLineItem, ByVal rowColor As String) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            Try
                If thisLineItem IsNot Nothing Then
                    td.Controls.Add(New LiteralControl("<table Width=""670"" border=""0"" cellpadding=""0"" cellspacing=""0"" role=""presentation"">" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center"">"))
                    td.Controls.Add(New LiteralControl(thisLineItem.PartNumberShipped))

                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""240"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
                    '-- description 
                    td.Controls.Add(New LiteralControl(thisLineItem.PartDescription))
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""90"" class=""tableData"" align=""center"">"))
                    '-- quantity
                    td.Controls.Add(New LiteralControl(thisLineItem.Quantity.ToString()))
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""85"" class=""tableData"" align=""right"">"))
                    '-- item price 
                    'td.Controls.Add(New LiteralControl("$" + Format(thisLineItem.ItemPrice, "##0.00").ToString()))'5325
                    td.Controls.Add(New LiteralControl(thisLineItem.ItemPrice.ToString("C"))) '5325
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""90"" class=""tableData"" align=""right"">"))
                    '-- total price 
                    'td.Controls.Add(New LiteralControl("$" + Format(thisLineItem.TotalPrice, "##0.00").ToString()))'5325
                    td.Controls.Add(New LiteralControl(thisLineItem.TotalPrice.ToString("C"))) '5325
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""63"" class=""tableHeader"" align=""center"">"))
                    If thisLineItem.Invoice.Order.ContainsDownloadableItems Then
                        td.Controls.Add(New LiteralControl("&nbsp;"))
                    Else
                        td.Controls.Add(New LiteralControl("<a href=""" + conRaRequestPage + """>" + Resources.Resource.el_Return.ToString().ToUpper + "</a>"))
                    End If
                    td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

                    tr.Controls.Add(td)
                Else
                    errorMessageLabel.Text = Resources.Resource.el_NoInvoicePassed '"No invoice passed to invoice details."
                End If
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        Private Function BuildInvoiceTotals(ByRef thisInvoice As Invoice) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            Dim anySpecialTax As Boolean = False
            Try
                If Not thisInvoice Is Nothing Then

                    td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    '-- shipping cost
                    'Commented By Navdeep kaur on 18th May for resolving 6396
                    'td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""85"" align=""right"" class=""tableHeader"">SHIPPING</td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""90"" align=""right"" class=""tableHeader"">"))
                    'td.Controls.Add(New LiteralControl("$" + Format(thisInvoice.Shipping, "##0.00").ToString()))
                    'td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    'End of Comment By Navdeep kaur on 18th May for resolving 6396

                    If thisInvoice.SpecialTax > 0.0 Then
                        anySpecialTax = True

                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))

                        '-- special tax
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""85"" align=""right"" class=""tableHeader"">" + Resources.Resource.el_STATERECYCLINGFEE + "<span class=tableHeader><span class=""redAsterick"">*</span></span></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""90"" align=""right"" class=""tableHeader"">"))
                        'td.Controls.Add(New LiteralControl("$" + Format(thisInvoice.SpecialTax, "##0.00").ToString()))'5325
                        td.Controls.Add(New LiteralControl(thisInvoice.SpecialTax.ToString("C")))
                        td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    End If

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    '-- tax cost
                    'Commented By Navdeep kaur on 18th May for resolving 6396
                    'td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""85"" align=""right"" class=""tableHeader"">TAX</td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""90"" align=""right"" class=""tableHeader"">"))
                    'td.Controls.Add(New LiteralControl("$" + Format(thisInvoice.Tax, "##0.00").ToString()))
                    'td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    'td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    'Comment By Navdeep kaur on 18th May for resolving 6396

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    '-- total cost
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))

                    'price difference for CA and US 
                    If HttpContextManager.GlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA Then
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""85"" class=""tableHeader"" align=""right"">" + Resources.Resource.el_SubTotal_CAD + "</td>" + vbCrLf))
                    Else
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""85"" class=""tableHeader"" align=""right"">" + Resources.Resource.el_SubTotal + "</td>" + vbCrLf))
                    End If

                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""90"" align=""right"" class=""tableHeader"">"))
                    'td.Controls.Add(New LiteralControl("$" + Format(thisInvoice.Total, "##0.00").ToString()))'5325
                    td.Controls.Add(New LiteralControl(thisInvoice.Total.ToString("C")))
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))


                    td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

                    tr.Controls.Add(td)
                Else
                    errorMessageLabel.Text = Resources.Resource.el_NoInvoicePassedTotals '"No invoice passed to invoice totals."
                End If

            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Me.labelSpecialTax.Visible = anySpecialTax
            Me.specialTaxHyperLink.Visible = anySpecialTax

            Return tr
        End Function

        Private Function BuildNoInvoicesTable() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""0"" width=""670"" role=""presentation"">"))

            td.Controls.Add(New LiteralControl("<tr>"))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + conRowColorLightBlue + """ colspan=""11"" align=""center"" class=""tableData"">"))
            td.Controls.Add(New LiteralControl(Resources.Resource.el_NoInvoiceAvailable))
            td.Controls.Add(New LiteralControl("</td>"))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
            td.Controls.Add(New LiteralControl("</tr>"))

            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildInvoiceColumnHeaders() As TableRow
            Dim tr As New TableRow

            Try
                Dim td As New TableCell

                td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">"))

                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7"" colspan=""13""><img src=""images/spacer.gif"" height=""1"" alt=""""></td>"))
                td.Controls.Add(New LiteralControl("</tr>"))

                td.Controls.Add(New LiteralControl("<tr>"))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" colspan=""11"" align=""center""><img src=""images/sp_int_invoicesTable_hdr.gif"" width=""668"" height=""25"" hspace=""0"" vspace=""0"" border=""0"" alt=""Invoices""></td>"))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>"))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

                td.Controls.Add(New LiteralControl("</table>"))

                tr.Controls.Add(td)
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try

            Return tr
        End Function

        Private Function BuildInvoiceFooter() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">"))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr
        End Function
#End Region

        Private Sub SetFileDownloadPaths()
            Try
                Dim downloadPath As String = ConfigurationData.Environment.Download.Physical
                conSoftwareDownloadPath = downloadPath + "/" + ConfigurationData.Environment.Download.DownloadFiles
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

#Region "build ordered items table"

        Private Sub BuildOrderedItemsTable(ByVal thisOrder As Order)
            Try
                Dim haveBackOrders As Boolean = False
                Dim rowColor As String
                Dim haveDownloadableItems As Boolean = False
                Dim haveOrderDetails As Boolean = False

                '-- column headers --
                OrderDetailsTable.Visible = True
                OrderInvoiceTable.Visible = True
                OrderDetailsTable.Controls.Add(BuildOrderColumnHeader(thisOrder.ContainsDownloadableItems))
                rowColor = conRowColorWhite

                For Each thisOrderItem As OrderLineItem In thisOrder.LineItems
                    rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                    Dim isDownloadable As Boolean = False

                    If thisOrderItem.Downloadable And thisOrderItem.Product.Type = Product.ProductType.Software Then
                        '-- when there is no software download file name no need to map to the physical path at all
                        If thisOrderItem.SoftwareDownloadFileName.ToString() <> "" Then
                            'If File.Exists(Server.MapPath(conSoftwareDownloadPath).ToString() + "/" + thisOrderItem.SoftwareDownloadFileName.ToString()) Then
                            If File.Exists(conSoftwareDownloadPath + "/" + thisOrderItem.SoftwareDownloadFileName.ToString()) Then
                                isDownloadable = True
                            End If
                        End If
                    End If

                    If thisOrderItem.LineStatus = Sony.US.ServicesPLUS.Core.OrderLineItem.Status.Backordered Then
                        '--back ordered line item details --                
                        OrderDetailsTable.Controls.Add(BuildBackOrderedDetails(thisOrderItem, rowColor, isDownloadable))
                        If Not haveOrderDetails Then haveOrderDetails = True
                    ElseIf thisOrderItem.LineStatus <> OrderLineItem.Status.Cancelled And thisOrderItem.LineNumber <> OrderLineItem.Status.ErrorStatus And thisOrderItem.LineNumber <> OrderLineItem.Status.Deleted Then
                        '-- in stock order item details
                        OrderDetailsTable.Controls.Add(BuildOrderedDetails(thisOrderItem, rowColor, isDownloadable))
                        If Not haveOrderDetails Then haveOrderDetails = True
                    End If
                Next

                If haveDownloadableItems Then errorMessageLabel.Text = Resources.Resource.el_ItemMarkered_DownloadPart '"Item marked with an * can be downloaded by clicking on the part number."
                If Not haveOrderDetails Then OrderDetailsTable.Controls.Add(BuildNoOrderedItemsTable())

                '-- footer --
                OrderDetailsTable.Controls.Add(BuildOrderfooter())
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function BuildOrderColumnHeader(ByVal Downloadable As Boolean) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            '-- top dark line 
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            '-- back order image
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" colspan=""11"" align=""center""><img src=""images/sp_int_OrderDetails_hdr.gif"" width=""668"" height=""25"" hspace=""0"" vspace=""0"" border=""0"" alt=""Order Details""><br/></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            '-- space line 
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""260""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""70""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            '-- header columns
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<th bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
            '-- item number (part number) 
            td.Controls.Add(New LiteralControl("<th width=""95"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_itemNumber + "</th>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
            '-- part description
            td.Controls.Add(New LiteralControl("<th width=""260"" class=""tableHeader"">&nbsp;&nbsp;&nbsp;" + Resources.Resource.el_Description + "</th>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
            '-- quantity 
            td.Controls.Add(New LiteralControl("<th width=""70"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_Quantity + "</th>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))

            'price difference for CA and US 
            If HttpContextManager.GlobalData.IsCanada Then
                '-- item price
                td.Controls.Add(New LiteralControl("<th width=""85"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_ItemPrice_CAD + "</th>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
                '-- total price
                td.Controls.Add(New LiteralControl("<th width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_TotalPrice_CAD + "</th>" + vbCrLf))
            Else
                '-- item price
                td.Controls.Add(New LiteralControl("<th width=""85"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_ItemPrice + "</th>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
                '-- total price
                td.Controls.Add(New LiteralControl("<th width=""90"" class=""tableHeader"" align=""center"">" + Resources.Resource.el_TotalPrice + "</th>" + vbCrLf))
            End If

            td.Controls.Add(New LiteralControl("<th bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
            '-- cancel order item
            If Downloadable Then
                td.Controls.Add(New LiteralControl("<th width=""63"" class=""tableHeader"" align=""center"">&nbsp;</th>" + vbCrLf))
            Else
                td.Controls.Add(New LiteralControl("<th width=""63"" class=""tableHeader"" align=""center"">&nbsp;" + Resources.Resource.el_Cancel + "</th>" + vbCrLf))
            End If
            td.Controls.Add(New LiteralControl("<th bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""35"" alt=""""></th>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildBackOrderedDetails(ByRef thisLineItem As OrderLineItem, ByVal rowColor As String, ByVal isDownloadable As Boolean) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow
            Dim strDescription As String

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- item number (part number)
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center"">"))
            If isDownloadable Then
                td.Controls.Add(New LiteralControl("<span color=""blue""><a  href=""" + conFileDownloadPage + "?ln=" + thisLineItem.LineNumber + """ >"))
                td.Controls.Add(New LiteralControl(thisLineItem.Product.PartNumber))
                td.Controls.Add(New LiteralControl("</a></span>&nbsp;"))
                td.Controls.Add(New LiteralControl("<span class=""redAsterick"">*</span>"))
            Else
                td.Controls.Add(New LiteralControl(thisLineItem.Product.PartNumber))
            End If
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- part description 
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""260"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
            If thisLineItem.Product.SpecialTax > 0.0 Then
                strDescription = thisLineItem.Product.Description + "<br/>" + Resources.Resource.el_RecyclingFee + "<span class=tableHeader><span class=""redAsterick"">*</span></span>"
            Else
                strDescription = thisLineItem.Product.Description
            End If
            td.Controls.Add(New LiteralControl(strDescription))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- quantity
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""70"" class=""tableData"" align=""center"">"))
            td.Controls.Add(New LiteralControl(thisLineItem.Quantity.ToString()))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- item price
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""85"" class=""tableData"" align=""right"">"))
            'td.Controls.Add(New LiteralControl("$" + Format(thisLineItem.Product.ListPrice, "##0.00").ToString()))'5325
            td.Controls.Add(New LiteralControl(thisLineItem.Product.ListPrice.ToString("C")))
            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- total price
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""90"" class=""tableData"" align=""right"">"))
            'td.Controls.Add(New LiteralControl("$" + Format((thisLineItem.Product.ListPrice * thisLineItem.Quantity), "##0.00").ToString()))'5325
            td.Controls.Add(New LiteralControl((thisLineItem.Product.ListPrice * thisLineItem.Quantity).ToString("C")))
            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- cancel order item link
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""63"" class=""tableHeader"" align=""center"">"))
            Dim objLine As String = String.Empty
            objLine = String.Join(",", thisLineItem.LineNumber) '2019-01-18 Replaced slow loop with faster Join.
            'For Each line As String In thisLineItem.LineNumber
            '    objLine = objLine + "," + line
            'Next
            'objLine.TrimStart(",")
            td.Controls.Add(New LiteralControl($"<a href=""#"" onclick=""cancelOrderItem('{conCancelBackOrderedItemFieldName}',' {objLine}')"">{Resources.Resource.el_Cancel.ToUpper}</a>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildOrderedDetails(ByRef thisLineItem As OrderLineItem, ByVal rowColor As String, ByVal isDownloadable As Boolean) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- item number (part number)
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center"">"))
            If isDownloadable Then
                td.Controls.Add(New LiteralControl("<a style=""color: blue;"" href=""" + conFileDownloadPage + "?ln=" + thisLineItem.LineNumber + """>"))
                td.Controls.Add(New LiteralControl(thisLineItem.Product.PartNumber))
                td.Controls.Add(New LiteralControl("</a>&nbsp;"))
                td.Controls.Add(New LiteralControl("<span class=""redAsterick"">*</span>"))
            Else
                td.Controls.Add(New LiteralControl(thisLineItem.Product.PartNumber))
            End If

            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- part description 
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""260"" class=""tableData"">&nbsp;&nbsp;&nbsp;"))
            td.Controls.Add(New LiteralControl(thisLineItem.Product.Description))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- quantity
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""70"" class=""tableData"" align=""center"">"))
            td.Controls.Add(New LiteralControl(thisLineItem.Quantity.ToString()))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- item price
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""85"" class=""tableData"" align=""right"">"))
            'td.Controls.Add(New LiteralControl("$" + Format(thisLineItem.Product.ListPrice, "##0.00").ToString()))'5325
            td.Controls.Add(New LiteralControl(thisLineItem.Product.ListPrice.ToString("C")))
            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            '-- total price
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""90"" class=""tableData"" align=""right"">"))
            'td.Controls.Add(New LiteralControl("$" + Format(thisLineItem.Quantity * thisLineItem.Product.ListPrice, "##0.00").ToString()))'5325
            td.Controls.Add(New LiteralControl((thisLineItem.Quantity * thisLineItem.Product.ListPrice).ToString("C")))
            td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""63"" class=""tableHeader"" align=""center"">"))
            '-- spacer
            If thisLineItem.Product.Type = Product.ProductType.ExtendedWarranty Then
                'Dim lnkButton As LinkButton = New LinkButton()
                'lnkButton.Text = "Download Certificate"
                'lnkButton.ToolTip = "Download Certificate"
                'lnkButton.CommandArgument = thisLineItem.Product.PartNumber
                'lnkButton.CommandName = "downloadEW"
                'AddHandler lnkButton.Click, AddressOf ConvertCrReportToPDF
                'lnkButton.ID = "lnk" + thisLineItem.Product.PartNumber
                'td.Controls.Add(lnkButton)
                td.Controls.Add(New LiteralControl("&nbsp;<a href=""#"" onclick=""window.open('Sony-EWCertificate.aspx?onum=" + CType(thisLineItem.Product.SoftwareItem, ExtendedWarrantyModel).CertificateGUID + "','','toolbar=0,location=0,top=0,left=0,directories=0,status=0,menubar=0,scrollbars=no,resize=no');return false;"">Download Certificate</a>"))
            Else
                td.Controls.Add(New LiteralControl("&nbsp;"))
            End If
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            Select Case thisLineItem.Product.Type
                Case Product.ProductType.Software
                    If isDownloadable Then
                        td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center""></td>"))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""9"" width=""570"" class=""tableData"" align=""left"">"))
                        td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy""><span class=""redAsterick"">*</span>" + Resources.Resource.el_DownloadSW + "</span><br/> "))

                        td.Controls.Add(New LiteralControl("</td>"))
                        td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                        td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                    End If
                Case Product.ProductType.ExtendedWarranty
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""9"" width=""570"" class=""tableData"" align=""left"">"))
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""tabledata""><span class=""tableHeader""> " + Resources.Resource.el_PurchasedOn + ": </span>" + CType(thisLineItem.Product, ExtendedWarrantyModel).EWPurchaseDate.Date.ToString("MM/dd/yyyy") + "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span><span class=""tabledata""><span class=""tableHeader"">Serial: </span>" + CType(thisLineItem.Product, ExtendedWarrantyModel).EWSerialNumber + "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""><span class=""tabledata""><span class=""tableHeader"">Model: </span>" + CType(thisLineItem.Product, ExtendedWarrantyModel).EWModelNumber + "</span><img height=""1"" src=""images/spacer.gif"" width=""10"" alt=""""></span>"))


                    td.Controls.Add(New LiteralControl("</td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                Case Product.ProductType.Certificate
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""9"" width=""570"" class=""tableData"" align=""left"">"))
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy"">Go to " + "<a  href=""#"" onclick=""window.open('" + thisLineItem.Product.TrainingURL + "'); return false;"">" + thisLineItem.Product.TrainingURL + "</a>" + " to activate your certification test.</span><br/> "))
                    If String.IsNullOrWhiteSpace(thisLineItem.Product.TrainingActivationCode) Then
                        td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy"">" + Resources.Resource.el_AddCertificateItem3 + ConfigurationData.Environment.Contacts.TrainingInstitute + ".</span>"))
                    Else
                        td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy"">" + Resources.Resource.el_AddCertificateItem2 + " " + thisLineItem.Product.TrainingActivationCode + ".</span>"))
                    End If
                    td.Controls.Add(New LiteralControl("</td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
                Case Product.ProductType.OnlineTraining
                    td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""95"" class=""tableData"" align=""center""></td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""9"" width=""570"" class=""tableData"" align=""left"">"))
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy""> " + Resources.Resource.el_OnlineReg1 + " <a  href=""#"" onclick=""window.open('" + thisLineItem.Product.TrainingURL + "'); return false;"">" + thisLineItem.Product.TrainingURL + "</a> ." + Resources.Resource.el_RegisterOnlineInfo2 + ",</span><br/> "))
                    td.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class=""bodyCopy""> " + Resources.Resource.el_RegisterOnlineInfo3 + " " + thisLineItem.Order.OrderNumber + " " + Resources.Resource.el_RegisterOnlineInfo4 + " </span><br/> "))
                    td.Controls.Add(New LiteralControl("</td>"))
                    td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                    td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End Select
            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildNoOrderedItemsTable() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + conRowColorLightBlue + """ colspan=""11"" align=""center"" class=""tableData"">"))
            td.Controls.Add(New LiteralControl(Resources.Resource.el_NoOrderItemDetails))
            td.Controls.Add(New LiteralControl("</td>" & vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildOrderfooter() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""95""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""240""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""85""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""90""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#ffffff"" width=""63""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function
#End Region

#Region "build Training ordered items table"

        Private Sub BuildTrainingOrderedItemsTable(ByVal thisOrder As Order)
            Try
                Dim haveBackOrders As Boolean = False
                Dim rowColor As String
                Dim haveDownloadableItems As Boolean = False
                Dim haveOrderDetails As Boolean = False

                TrainingOrderDetailsTable.Visible = True
                orderdiv.Visible = False
                labelSpecialTax.Visible = False
                specialTaxHyperLink.Visible = False
                TrainingOrderDetailsTable.Controls.Add(BuildTrainingOrderColumnHeader())

                rowColor = conRowColorWhite
                rowColor = IIf(rowColor = conRowColorWhite, conRowColorLightBlue, conRowColorWhite)
                TrainingOrderDetailsTable.Controls.Add(BuildTrainingOrderedDetails(thisOrder, rowColor))


                TrainingOrderDetailsTable.Controls.Add(BuildTrainingOrderfooter())
            Catch ex As Exception
                errorMessageLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Function BuildTrainingOrderColumnHeader() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            '-- top dark line 
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td height=""1"" colspan=""4"" bgcolor=""#b7b7b7""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            '-- back order image
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#d5dee9"" colspan=""2"" class=""tableHeader"" align=""left"">" + Resources.Resource.el_TrainingOrderDetail + "<br/></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""25"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            '-- space line 
            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""4"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))


            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildTrainingOrderedDetails(ByRef thisLineItem As Order, ByVal rowColor As String) As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            If m_cr.CourseSchedule.PartNumber <> "TRAINTEMP" Then
                td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""120"" class=""tableData"" align=""left"">"))
                td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_ClassNumber + "</span>"))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""538"" class=""tableData"" align=""left"">"))
                td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.CourseSchedule.PartNumber + "</span>"))
                td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
                td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))
            End If

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_CourseNumber + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""538"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.CourseSchedule.Course.Number + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_Title + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""538"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.CourseSchedule.Course.Title + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_StartDate + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""538"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.CourseSchedule.StartDateDisplay.ToString() + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_EndDate + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""538"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.CourseSchedule.EndDateDisplay.ToString() + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_Price + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""white"" width=""538"" class=""tableData"" align=""left"">"))
            'td.Controls.Add(New LiteralControl("<span class=""bodycopy"">$ " + m_cr.CourseSchedule.Course.ListPrice.ToString() + "</span>"))'5325
            Dim courselistPrice As String = m_cr.CourseSchedule.Course?.ListPrice.ToString("C") '5325
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + courselistPrice + "</span>")) '5325
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""1"" width=""1""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ colspan=""1"" width=""10""><img src=""images/spacer.gif"" width=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""120"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""tableheader"">" + Resources.Resource.el_NumberofStudent + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""" + rowColor + """ width=""538"" class=""tableData"" align=""left"">"))
            td.Controls.Add(New LiteralControl("<span class=""bodycopy"">" + m_cr.Participants.Length.ToString() + "</span>"))
            td.Controls.Add(New LiteralControl("</td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" width=""1""><img src=""images/spacer.gif"" width=""1"" height=""20"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>" + vbCrLf))

            tr.Controls.Add(td)

            Return tr
        End Function

        Private Function BuildTrainingOrderfooter() As TableRow
            Dim td As New TableCell
            Dim tr As New TableRow

            td.Controls.Add(New LiteralControl("<table width=""670"" border=""0"" cellspacing=""0"" cellpadding=""0"" role=""presentation"">" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7"" colspan=""13"" ><img src=""images/spacer.gif"" width=""1"" height=""5"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("<tr>" + vbCrLf))
            td.Controls.Add(New LiteralControl("<td bgcolor=""#b7b7b7""  colspan=""13"" ><img src=""images/spacer.gif"" width=""1"" height=""1"" alt=""""></td>" + vbCrLf))
            td.Controls.Add(New LiteralControl("</tr>" + vbCrLf))

            td.Controls.Add(New LiteralControl("</table>"))

            tr.Controls.Add(td)

            Return tr
        End Function
#End Region

        Private Sub CancelBackOrderedItem(ByVal thisLineItemNumber As String, ByRef thisOrder As Order)
            Dim objPCILogger As New PCILogger() '6524
            Try
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginApplicationLocation = Me.Page.GetType().Name
                objPCILogger.EventOriginMethod = "cancelBackOrderedItem"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress '6524
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID '6524
                objPCILogger.EventType = EventType.Update
                objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.IndicationSuccessFailure = "Success"

                If Not thisOrder Is Nothing Then
                    Dim strArguments() As String = thisLineItemNumber.Split(",".ToCharArray())
                    Dim objSAPHelper As SAPHelper = New SAPHelper()
                    If (objSAPHelper.CancelOrder(thisOrder.OrderNumber.Trim(), strArguments, ConfigurationData.Environment.CancelOrder.RejectionReason) = "S") Then
                        Session.Remove("order")
                        objPCILogger.Message = "cancelBackOrderedItem Success."
                        Response.Redirect("MyOrderDetails.aspx?order=" + thisOrder.OrderNumber.Trim() + "," + thisOrder.OrderNumber.Trim() + "," + strOrderQueryString(2).Trim, True) '7792
                    Else
                        objPCILogger.Message = "cancelBackOrderedItem Failed. Error trying to cancel back ordered item"
                        errorMessageLabel.Text = Resources.Resource.el_CancelOrder_Err1 '"Error trying to cancel back ordered item."
                    End If
                Else
                    objPCILogger.Message = "cancelBackOrderedItem Failed. Error trying to cancel back ordered item. Order does not exits."
                    errorMessageLabel.Text = Resources.Resource.el_CancelOrder_Err1 ' "Error trying to cancel back ordered item. Order does not exits."
                End If
            Catch ex As Exception
                objPCILogger.Message = "cancelBackOrderedItem Failed. " & ex.Message.ToString()
                errorMessageLabel.Text = Utilities.WrapException(ex).Message
            Finally
                objPCILogger.PushLogToMSMQ() '6524
            End Try
        End Sub

        Public Function SelectCCNo(ByVal aCustomer As Integer) As String
            Dim creditNo As String
            Dim objAuditDataMgr As New AuditDataManager
            creditNo = objAuditDataMgr.SelectCC(aCustomer)
            Return creditNo
        End Function

        Public Function GetAuditCCNo(ByVal thisCCNo As String) As String
            Dim returnValue As String = ""
            If thisCCNo <> String.Empty Then
                Dim ccNumberLength As Short = thisCCNo.Length
                Dim startValue As String = thisCCNo.Substring(0, 4)
                startValue = startValue.PadRight((ccNumberLength - 4), "X")
                Dim endValue As String = thisCCNo.Substring((ccNumberLength - 4), 4)
                returnValue = startValue + endValue
            End If
            Return returnValue
        End Function

    End Class

End Namespace
