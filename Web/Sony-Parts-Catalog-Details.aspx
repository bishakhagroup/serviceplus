<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Sony-Parts-Catalog-Details.aspx.vb" Inherits="ServicePLUSWebApp.Sony_Parts_Catalog_Details" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html lang="<%=PageLanguage %>">
<head>
    <title>Sony Part Catalog for Professional Products</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta content="Sony Parts, Sony Professional Parts, Sony Broadcast Parts, Sony Business Parts, Sony Software, Sony Professional Software, Sony Broadcast Software, Sony Business Software"
        name="keywords">
    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">
    <script language="javascript" src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <%--6994 starts--%>
    <link href="themes/base/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="includes/jquery.cluetip.css" rel="stylesheet" type="text/css" />
    <script src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.core.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="includes/jquery.cluetip.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.mouse.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.draggable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.position.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.resizable.js" type="text/javascript"></script>
    <script src="includes/jquery.ui.dialog.js" type="text/javascript"></script>
    <script src="includes/jquery-ui-1.8.1.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <script type="text/javascript">
        function closedilog() {
            $("#dialog").dialog("close");
            return true;
        }
        var exception = false;
        $(document).ready(function () {
            $("#messageclose").click(function () {

                $("#dialog").dialog("close");
            });


        });                       //Document ready

        function replaceAll(txt, replace, with_this) { return txt.replace(new RegExp(replace, 'g'), with_this); }

        function validPart(partnumber, qty) {

            if (jQuery.trim(partnumber.val()) === "" && jQuery.trim(qty.val()) === "") {
                return false;
            }
            else {

                if (jQuery.trim(partnumber.val()) === "" || jQuery.trim(qty.val()) === "") {
                    if (jQuery.trim(partnumber.val()) === "") {
                        exception = true;

                        $("#divException").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong><br/>");
                        //$("#divMessage").append("<strong>Please enter a valid part." + qty.val().tostring() + "</strong>");
                        $("#divException").append("<br/>");

                    } else {
                        exception = true;
                        $("#divException").append("<br/>");
                        $("#divException").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong><br/>");
                        // $("#divMessage").append("<strong>Please enter a valid quantity." + partnumber.val() + "</strong>");
                        $("#divException").append("<br/>");
                    }
                    return false;
                }
                if (jQuery.trim(partnumber.val()) !== "" && jQuery.trim(qty.val()) !== "") {
                    if (jQuery.trim(qty.val()).match("[^0-9]")) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                    if (jQuery.trim(qty.val()) <= 0) {
                        exception = true;

                        $("#divException").append("<strong> Item " + partnumber.val() + ": Please enter a valid order quantity. </strong><br/><br/>");

                        return false;
                    }
                }
                return true;
            }
        }

        function AddToCart_onclick(val) {
            exception = false;

            $("#divMessage").text('');
            var buttons = $('.ui-dialog-buttonpane').children('button');
            buttons.remove();
            $("#divException").text('');
            $("#divException").hide();
            $("#divPrg").html('<span id="progress"> Please wait...<br> <img   src="images/progbar.gif" alt ="Progres" /></span>');
            $("#divMessage").append(' <br/>');
            var totalNull = "";
            var count = 0;
            $('#hidespstextitemno').val(val)
            $('#hidespstextqty').val(1)
            var sQty = '#hidespstextqty'
            var sPart = '#hidespstextitemno'


            var qty = $(sQty);
            var part = $(sPart);

            totalNull = totalNull + qty.val() + part.val();

            if (totalNull === "" || jQuery.trim(part.val()) === "" || jQuery.trim(qty.val()) === "") {
                $("#divMessage").text('');
                ///   $("#divMessage").html("Please enter at least one part number.");
                $("#dialog").dialog({
                    //title: 'Quick Order Add to Cart Exceptions'
                    title: 'Order Add to Cart Exceptions'
                });
                $("#divMessage").append('<br/> <br/><a  ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
            }
            var itemsInCart = $("#itemsInCart");
            var totalPriceOfCart = $("#totalPriceOfCart");
            var destination = itemsInCart.position();
            var position = part.position();

            $("#dialog").dialog({
                //title: '<strong>Quick Order Add to Cart Status</strong>',
                title: '<strong>Order Add to Cart Status</strong>',
                height: 200,
                width: 560,
                modal: true,
                position: 'top',
                resizable: false
            });
            count = count + 1;
            if (validPart(part, qty)) {
                count = count + 1;
                $("#divMessage").html("<br/><strong>Adding item " + replaceAll(part.val(), "-", "") + " to cart... </strong><br/>");
                var parametervalues = '{sPartNumber: "' + jQuery.trim(replaceAll(part.val(), "-", "")) + '" }';
                //Call webmethod using ajax
                jQuery.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: parametervalues,
                    dataType: 'json',
                    url: 'Sony-Parts-Catalog-Details.aspx/addPartToCartwSCatalog',
                    success: function (result) {
                        count = count - 1;

                        if (result.d.differentCart === true) {
                            exception = true;
                            $("#divException").append("<br/><strong>The item has been added to a cart for items to be shipped; your previous cart with items for electronic delivery has been saved</strong><br/>");
                        }

                        if (result.d.success === 1) {
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                        }
                        else if (result.d.success === 2) {
                            exception = true;
                            $("#divMessage").html("<br/><strong>Adding item " + replaceAll(result.d.partnumber, "-", "") + " to cart... </strong><br/>");
                            result.d.partnumber = '';
                            result.d.Quantity = '';
                            itemsInCart.text(result.d.items);
                            totalPriceOfCart.text(result.d.totalAmount);
                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        else {
                            exception = true;

                            $("#divException").append("<br/><strong>" + result.d.message + "</strong><br/>");
                        }
                        //$("#divMessage").html("<br/><strong>" + result.d.message + "</strong><br/>");

                        if (count === 0) {
                            if (exception) {
                                var sHeight = $("#divException").height() + 200;

                                $("#progress").remove();
                                $("#dialog").dialog({
                                    //title: 'Quick Order Add to Cart Exceptions',
                                    title: 'Order Add to Cart Exceptions',
                                    height: sHeight.toString(),
                                    position: 'top'
                                }).dialog("moveToTop");
                                $("#divMessage").text('');
                                $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                                $("#divException").show("fast");
                            }
                            else {
                                $("#dialog").dialog("close");
                            }
                        }
                    }
                });
            }
            count = count - 1;

            if (count === 0) {
                $("#progress").remove();

                if (exception) {
                    var sHeight = $("#divException").height() + 200;

                    $("#progress").remove();
                    $("#dialog").dialog({
                        //title: 'Quick Order Add to Cart Exceptions',
                        title: 'Order Add to Cart Exceptions',
                        height: sHeight.toString(),
                        position: 'top'
                    }).dialog("moveToTop");
                    $("#divMessage").text('');
                    $("#divException").append('<br/> <br/><a ><img src ="images/sp_int_closeWindow_btn.gif" alt ="Close" onclick="javascript:return closedilog();"  id="messageclose" /></a>');
                    $("#divException").show("fast");
                    return false; //6994
                }
            }
            return false; //6994
        }
    </script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <div id="ValidationMessageDiv">
    </div>
    <div id="dialog" title="<strong>Order Add to Cart Status</strong>">
        <div id="divPrg" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divMessage" style="font: 11px; font-family: Arial; overflow: auto;"></div>
        <div id="divException" style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; line-height: 14px; overflow: auto; display: none;">
            <img src="images/sp_int_closeWindow_btn.gif" alt="Close" onclick="javascript:return closedilog();" id="messageclose" />
        </div>
    </div>

    <center>
        <form id="Form1" method="post" runat="server">
            <table width="710" border="0" role="presentation">
                <tr>
                    <td width="25" style="background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="689" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tr>
                                <td style="width: 714px">
                                    <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td height="23" style="width: 714px">
                                    <nav><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 714px">
                                    <table width="710" border="0" role="presentation">
                                        <tr>
                                            <td align="right" width="464" background="images/sp_int_header_top_PartsPLUS.jpg" bgcolor="#363d45"
                                                height="82">
                                                <br>
                                                <h1 class="headerText">Parts&nbsp;&amp;&nbsp;Accessories &nbsp;&nbsp;</h1>
                                            </td>
                                            <td valign="top" width="238" bgcolor="#363d45">
                                                <ServicePLUSWebApp:PersonalMessage ID="PersonalMessageDisplay" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td bgcolor="#f2f5f8">
                                                <table width="464" border="0" role="presentation">
                                                    <tr>
                                                        <td width="20">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td>
                                                            <h2 class="headerTitle">Sony Part Catalog</h2></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20" style="height: 105px">
                                                            <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                        <td style="height: 105px">
                                                            <table role="presentation">
                                                                <tr>
                                                                    <td class="finderSubhead">Quick Parts Search</td>
                                                                    <td width="20">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                                    <%--<td><img height="12" src="images/sp_int_partnumber_label.gif" width="58"></td>--%>
                                                                    <td><span class="finderCopyDark">&nbsp;Part Number:</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="finderCopyDark" width="205" rowspan="2">Don't see the part you want listed below?
                                                                        <br />
                                                                        Enter part number you want here,
                                                                        <br />
                                                                        or try <a href="sony-parts.aspx" class="finderSubhead">advanced search</a></td>
                                                                    <td width="20">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="PartNumber" runat="server" CssClass="finderCopyDark"></SPS:SPSTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td width="20">
                                                                        <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                                                    <td>
                                                                        <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="images/sp_int_submitGreybkgd_btn.gif"
                                                                            AlternateText="Quick Search"></asp:ImageButton>
                                                                        <br />
                                                                        <asp:Label ID="SearchErrorLabel" runat="server" CssClass="redAsterick"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="238" bgcolor="#99a8b5">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr height="9">
                                            <td bgcolor="#f2f5f8">
                                                <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt=""></td>
                                            <td width="238" bgcolor="#99a8b5">
                                                <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt=""></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 710px">
                                    <table role="presentation">
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td>
                                                <asp:HyperLink ID="topPreviousImg" runat="server" CssClass="tableData"><img src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous"></asp:HyperLink>&nbsp; 
								                    <asp:HyperLink ID="topPrevious" runat="server" CssClass="tableData">Previous</asp:HyperLink>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                <asp:HyperLink ID="topNext" runat="server" CssClass="tableData">Next</asp:HyperLink>&nbsp; 
								                    <asp:HyperLink ID="topNextImg" runat="server" CssClass="tableData"><img src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next"></asp:HyperLink>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td>
                                                <asp:GridView ID="GridPartsGroup" runat="server" AutoGenerateColumns="False">
                                                    <HeaderStyle BackColor="#D5DEE9" BorderStyle="None" CssClass="tableHeader" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <RowStyle BackColor="#F2F5F8" CssClass="tableData" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <Columns>
                                                        <asp:BoundField AccessibleHeaderText="Model" DataField="MODEL" HeaderText="Model">
                                                            <ItemStyle Width="100px" />
                                                            <HeaderStyle Width="100px" />
                                                        </asp:BoundField>
                                                        <%--  <asp:BoundField AccessibleHeaderText="Part Requested" DataField="PARTNUMBER" HeaderText="Part Requested" >
                                                                <ItemStyle Width="100px" />
                                                                <HeaderStyle Width="100px" />
                                                            </asp:BoundField>--%>
                                                        <asp:TemplateField AccessibleHeaderText="Part Requested" HeaderText="Part Requested">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkPartRequested" runat="server"
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.PARTNUMBER") %>'></asp:HyperLink><%--6916--%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="60px" />
                                                            <HeaderStyle Width="60px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField AccessibleHeaderText="Part Supplied" HeaderText="Part Supplied">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkPartSupplied" runat="server"
                                                                    Text='<%# DataBinder.Eval(Container, "DataItem.REPLACEMENTPARTNUMBER") %>'></asp:HyperLink><%--6916--%>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="60px" />
                                                            <HeaderStyle Width="60px" />
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField AccessibleHeaderText="Part Supplied" DataField="REPLACEMENTPARTNUMBER"
                                                                HeaderText="Part Supplied" >
                                                                <ItemStyle Width="100px" />
                                                                <HeaderStyle Width="100px" />
                                                            </asp:BoundField>--%>
                                                        <asp:BoundField AccessibleHeaderText="Description" DataField="DESCRIPTION" HeaderText="Description">
                                                            <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField AccessibleHeaderText="Availability / Price" HeaderText="Availability / Price">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkAvailability" CssClass="tableHeader" runat="server">Availability</asp:HyperLink>
                                                                <asp:Literal ID="litAvailability" runat="server" Text="Call 1-800-538-7550 for information." Visible="false"></asp:Literal>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="150px" />
                                                            <HeaderStyle Width="150px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField AccessibleHeaderText="Add Cart" HeaderText="Add Cart" SortExpression="50">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkAddToCart" runat="server">
                                                                    <asp:Image ID="imgAddCart" runat="server" ImageUrl="images/sp_int_add2Cart_btn.gif" /></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="40px" />
                                                            <HeaderStyle Width="40px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField AccessibleHeaderText="My Cat." HeaderText="My Cat.">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="lnkAddToCat" runat="server">
                                                                    <asp:Image ID="imgAddCat" runat="server" ImageUrl="images/sp_int_add2myCat_btn.gif" /></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="40px" />
                                                            <HeaderStyle Width="40px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField AccessibleHeaderText="List Price" DataField="LISTPRICE"
                                                            DataFormatString="&quot;{0:c}&quot;" HeaderText="List Price" Visible="False" />
                                                        <%--7449--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td>
                                                <asp:HyperLink ID="btmPreviousImg" runat="server" CssClass="tableData"><img src="images/sp_int_leftArrow_btn.gif" border="0" alt="Previous"></asp:HyperLink>&nbsp;  
								                    <asp:HyperLink ID="btmPrevious" runat="server" CssClass="tableData">Previous</asp:HyperLink>
                                                <img height="20" src="images/spacer.gif" width="20" alt="">
                                                <asp:HyperLink ID="btmNext" runat="server" CssClass="tableData">Next</asp:HyperLink>&nbsp;
								                    <asp:HyperLink ID="btmNextImg" runat="server" CssClass="tableData"><img src="images/sp_int_rightArrow_btn.gif" border="0" alt="Next"></asp:HyperLink>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td>
                                                <asp:Label ID="ErrorLabel" runat="server" CssClass="redAsterick"></asp:Label></td>
                                            <%--6916--%>
                                        </tr>
                                        <tr>
                                            <td width="20">
                                                <img height="20" src="images/spacer.gif" width="20" alt=""></td>
                                            <td>
                                                <a href="#top">
                                                    <img src="images/sp_int_back2top_btn.gif" width="78" height="28" border="0" alt="Back to Top"></a>
                                            </td>
                                        </tr>
                                        <%--6994 starts--%>
                                        <tr>
                                            <td align="left" colspan="3">
                                                <SPS:SPSTextBox ID="hidespstextitemno" runat="server" Style="display: none"></SPS:SPSTextBox>
                                                <SPS:SPSTextBox ID="hidespstextqty" runat="server" Style="display: none"></SPS:SPSTextBox>
                                            </td>
                                        </tr>
                                        <%--6994 ends--%>
                                    </table>

                                </td>
                            </tr>

                            <tr>

                                <td style="width: 714px">
                                    <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
