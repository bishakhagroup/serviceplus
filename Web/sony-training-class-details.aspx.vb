Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process

Namespace ServicePLUSWebApp

    Partial Class sony_training_class_details
        Inherits SSL

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents Label4 As System.Web.UI.WebControls.Label
        Protected WithEvents LabelStartDate As System.Web.UI.WebControls.Label
        Protected WithEvents Label5 As System.Web.UI.WebControls.Label
        Protected WithEvents LabelEndDate As System.Web.UI.WebControls.Label


        '-- constants --
#Region "constants"
        Private Const spaceAvailableMessage As String = "Space is available for this class. To make a reservation for a student for this class, click ""REGISTER"".<br/>You will not be immediately billed for this registration. A Sony Training Team member will contact you at a later date for billing information."
        Private Const unavailabelWithStartDateMessage As String = "This class is currently full, however openings may become available or a new class may be added.<br/>To add a student to the waitlist for this class, click ""ADD TO WAITLIST"".<BR>You will be notified by email and/or phone when an opening is available.<br/>You will not be immediately billed for this registration. A Sony Training Team member will contact you at a later date for billing information."
        Private Const unavailabelWithoutStartDateMessage As String = "A date has not been set for this class yet. <BR> To add a student to the waitlist for a future offering of this class, click ""ADD TO WAITLIST"".<br/>You will be notified by email and/or phone when an opening is available.<br/>You will not be immediately billed for this registration. A Sony Training Team member will contact you at a later date for billing information."

#End Region

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            tblData.Visible = True
            btnRegister.Visible = True
            If Not IsPostBack Then
                Dim courseMan As New CourseDataManager
                Dim courseSelected As CourseSchedule = Nothing
                Dim coursesSelected As New ArrayList
                Dim strCourseNumber = String.Empty
                Dim queryForATC = String.Empty
                Dim itemNumber As Integer = -1
                Dim queryValue = String.Empty

                Try
                    AddToCartAfterLogin(ErrorLabel)
                    If (Request.QueryString("ItemNumber") IsNot Nothing) And (Session.Item("courseschedule") IsNot Nothing) Then
                        queryValue = Request.QueryString("ItemNumber").Trim()
                        queryForATC = "ItemNumber=" & queryValue
                        If Not Integer.TryParse(queryValue, itemNumber) Then Throw New ServicesPlusBusinessException("Modified query string detected.")
                        courseSelected = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                        If courseSelected Is Nothing Then Throw New ServicesPlusBusinessException("Class not available. Please try again.")
                        strCourseNumber = courseSelected.Course.Number
                    ElseIf Request.QueryString("ClassNumber") IsNot Nothing Then
                        queryValue = Request.QueryString("ClassNumber").Trim()
                        queryForATC = "ClassNumber=" & queryValue
                        courseMan.GetCourseClassByNumber(queryValue, courseSelected)
                        If courseSelected Is Nothing Then Throw New ServicesPlusBusinessException("Class not available. Please try again.")
                        coursesSelected.Add(courseSelected)
                        strCourseNumber = courseSelected.Course.Number
                        Session.Item("courseschedule") = coursesSelected
                        itemNumber = 0
                    ElseIf Request.QueryString("CourseNumber") IsNot Nothing Then
                        queryValue = Request.QueryString("CourseNumber")
                        queryForATC = "CourseNumber=" & queryValue
                        Try
                            Dim courseSplit = queryValue.Split(",")
                            If (courseSplit.Length = 2) Then courseMan.GetCourseByCourseNumber(courseSplit(0), Convert.ToInt16(courseSplit(1)), courseSelected)
                        Catch ex As FormatException ' User set a non-integer value for second part of ?CourseNumber=123,456
                            ' It will trigger the next If anyway, but hide the conversion error.
                        End Try
                        If courseSelected Is Nothing Then Throw New ServicesPlusBusinessException("Class not available. Please try again.")
                        strCourseNumber = courseSelected.Course.Number
                        coursesSelected.Add(courseSelected)
                        Session.Item("courseschedule") = coursesSelected
                        itemNumber = 0
                    Else
                        Throw New ServicesPlusBusinessException("Invalid class selection. Please return to Training and search again.")
                    End If

                    If Request.QueryString("atc") IsNot Nothing Then
                        Dim objTraining As Training
                        Dim objCourseManager As New CourseManager

                        'TBD: Add selected Course to cart and then bind
                        If Not String.IsNullOrWhiteSpace(strCourseNumber) Then
                            objTraining = objCourseManager.SearchNonClassRoomTrainingForATC(strCourseNumber)
                            objTraining.Download = (objTraining.Type <> Product.ProductType.ComputerBasedTraining)
                            objTraining.Ship = (objTraining.Type = Product.ProductType.ComputerBasedTraining)

                            If HttpContextManager.Customer IsNot Nothing Then
                                'RegisterStartupScript("AddToCart_onclick", "<script language='javascript'>AddToCart_onclick('" + strCourseNumber + "');</script>") ' 2018-06-08 ASleight
                                ClientScript.RegisterStartupScript([GetType](), "AddToCart_onclick", $"<script language='javascript'>AddToCart_onclick('{strCourseNumber}');</script>")
                            Else
                                Dim selectedItemsBeforeLoggedIn As ArrayList
                                If Session("SelectedItemsBeforeLoggedIn") IsNot Nothing Then
                                    selectedItemsBeforeLoggedIn = CType(Session("SelectedItemsBeforeLoggedIn"), ArrayList)
                                Else
                                    selectedItemsBeforeLoggedIn = New ArrayList
                                    Session("SelectedItemsBeforeLoggedIn") = selectedItemsBeforeLoggedIn
                                End If
                                selectedItemsBeforeLoggedIn.Add(objTraining)    ' ASleight - Why do we add after we've put the variable in Session? Seems like a defect.
                                Response.Redirect($"SignIn.aspx?Lang={HttpContextManager.GlobalData.LanguageForIDP}&post=stcdatc&{queryForATC}", False)
                                HttpContext.Current.ApplicationInstance.CompleteRequest()
                            End If
                        End If
                    End If
                Catch spEx As ServicesPlusBusinessException
                    ErrorLabel.Text = spEx.Message
                    tblData.Visible = False
                    btnRegister.Visible = False
                    Exit Sub
                Catch ex As Exception
                    ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                End Try

                If courseSelected IsNot Nothing Then
                    Dim cm As New CourseManager
                    Dim courseAddress As String = ""

                    Try
                        If HttpContextManager.Customer Is Nothing Then
                            btnRegister.Visible = True
                            'btnRegister.Attributes("onclick") = "window.location.href='SignIn-Register.aspx?post=stcd&CourseLineno=" + itemNumber.ToString() + "'; return false;"
                            btnRegister.Attributes("onclick") = "window.location.href='SignIn-Register.aspx?post=stcdatc&" + queryForATC + "'; return false;"
                        End If
                        lblTitle.Text = courseSelected.Course.Title
                        lblDescription.Text = courseSelected.Course.Description
                        lblPrerequisite.Text = courseSelected.Course.PreRequisite

                        cm.GetCourseVacancy(courseSelected, True)

                        If courseSelected?.Course?.Type.TypeCode = 0 Then
                            If courseSelected.StartDateDisplay <> "TBD" Then
                                startDate.Visible = True
                                endDate.Visible = True
                                trainingTime.Visible = True
                                lblStartDate.Text = courseSelected.StartDateDisplay
                                lblEndDate.Text = courseSelected.EndDateDisplay
                                lblTrainingTime.Text = courseSelected.Time
                                If String.IsNullOrWhiteSpace(lblTrainingTime.Text) Then lblTrainingTime.Text = "TBD"
                            End If

                            ' 2018-06-11 ASleight - This used to be a bunch of <tr><td> for no good reason. Changed to a label with <br/> instead.
                            courseAddress = $"&nbsp;{courseSelected.Location.Name}<br/>"
                            courseAddress &= $"&nbsp;{courseSelected.Location.Address1}<br/>"
                            If Not String.IsNullOrWhiteSpace(courseSelected.Location.Address2) Then
                                courseAddress &= $"&nbsp;{courseSelected.Location.Address2}<br/>"
                            End If
                            courseAddress &= $"&nbsp;{courseSelected.Location.City}, {courseSelected.Location.State} {courseSelected.Location.Zip}"
                            If Not String.IsNullOrWhiteSpace(courseSelected.Location.Country?.TerritoryName) Then
                                courseAddress &= $"&nbsp;{courseSelected.Location.Country.TerritoryName}<br/>"
                            End If
                            lblLocation.Text = courseAddress
                            Session.Item("IsNonClassRoom") = 0
                        Else
                            Session.Item("IsNonClassRoom") = 1
                            imgHeader.InnerText = "Training Details"
                        End If

                        lblListPrice.Text = String.Format("{0:c}", courseSelected.Course.ListPrice)
                        If courseSelected.StartDate.ToString("MM/dd/yyyy") <> NullDateTime Then
                            If (courseSelected.Vacancy > 0) Then
                                labelAction.Text = spaceAvailableMessage
                            Else
                                labelAction.Text = unavailabelWithStartDateMessage
                                btnRegister.ImageUrl = "images/sp_int_addwaitlist_btn.gif"
                                btnRegister.AlternateText = "Add to Waitlist"
                            End If
                        ElseIf courseSelected.Course.Type.TypeCode = 3 Or courseSelected.Course.Type.TypeCode = 4 Then
                            labelAction.Visible = False
                            btnRegister.Attributes.Clear()
                            btnRegister.AlternateText = "Register"
                            btnRegister.ImageUrl = "images/sp_int_register_btn.gif"
                            btnRegister.Attributes("onclick") = " window.open('" + courseSelected.Course.URL + "','_blank','toolbar=yes, scrollbars=yes, resizable=yes,width=400,height=400');"
                        ElseIf courseSelected.Course.Type.TypeCode <> 0 Then
                            'Place a Cart Symbol and User will add it into cart
                            Response.Write("<script language='javascript'>window.document.title='Sony Training Institute � Training Details'</script>")
                            labelAction.Text = ""
                            labelAction.Visible = False
                            btnRegister.ImageUrl = "images/sp_int_order_btn.gif"
                            btnRegister.Attributes.Clear()
                            btnRegister.AlternateText = "Order"
                            If courseSelected.Course.Type.TypeCode = 1 Then
                                btnRegister.Attributes("onclick") = "window.location.href='sony-software-part-" + courseSelected.Course.Number + ".aspx'; return false;"
                            Else
                                btnRegister.Attributes("onclick") = "window.location.href='sony-training-class-details.aspx?" + queryForATC + "&atc=true'; return false;"
                            End If
                            lblLocationTitle.Visible = False
                            lblPriceTitle.Text = "Price:"
                        Else    ' TypeCode = 0
                            labelAction.Text = unavailabelWithoutStartDateMessage
                            btnRegister.ImageUrl = "images/sp_int_addwaitlist_btn.gif"
                            btnRegister.AlternateText = "Add to Waitlist"
                        End If
                    Catch ex As Exception
                        ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
                    End Try
                End If
            End If
        End Sub

        Private Sub btnRegister_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnRegister.Click
            'Dim customer As Customer
            Dim courseSelected As CourseSchedule = Nothing
            Dim cr As New CourseReservation
            'Dim nonAccount = True
            Dim isNonClassroom As Integer = 0

            Try
                If (Request.QueryString("ItemNumber") IsNot Nothing) And (Session.Item("courseschedule") IsNot Nothing) Then
                    Dim itemNumber As Integer = Convert.ToInt16(Request.QueryString("ItemNumber"))
                    courseSelected = CType(Session.Item("courseschedule"), ArrayList)(itemNumber)
                ElseIf (Request.QueryString("ClassNumber") IsNot Nothing) Or (Request.QueryString("CourseNumber") IsNot Nothing) Then
                    courseSelected = CType(Session.Item("courseschedule"), ArrayList)(0)
                End If

                If courseSelected IsNot Nothing Then
                    If courseSelected.Course.Type.TypeCode = 3 Or courseSelected.Course.Type.TypeCode = 4 Then Return
                    If HttpContextManager.Customer Is Nothing Then Return

                    'customer = HttpContextManager.Customer
                    'If customer.UserType = "A" Then
                    '    If (customer.SAPBillToAccounts?.Count > 0) Or (customer.SISLegacyBillToAccounts?.Count > 0) Then
                    '        nonAccount = False
                    '    End If
                    'End If

                    cr.CourseSchedule = courseSelected
                    Session("trainingreservation") = cr

                    If Session.Item("IsNonClassRoom") Is Nothing Then Throw New ServicesPlusBusinessException("Session variable 'IsNonClassRoom' was null.")    ' 2018-06-11 ASleight - Added null check.
                    ' 2018-07-18 ASleight - Removing payment, as it is handled by Training team.
                    'If (Not nonAccount) And customer.IsActive Then
                    '    Response.Redirect("training-payment-accnt.aspx?nonclass=" + Session.Item("IsNonClassRoom").ToString(), False)
                    'Else
                    '    Response.Redirect("training-payment-naccnt.aspx?nonclass=" + Session.Item("IsNonClassRoom").ToString(), False)
                    'End If
                    Response.Redirect($"training-student.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As ImageClickEventArgs) Handles btnCancel.Click
            Dim url As String = "sony-training-catalog.aspx"
            Dim id As Int32

            Try
                If (Request.QueryString("ItemNumber") IsNot Nothing) OrElse Request.QueryString("ClassNumber") IsNot Nothing OrElse Request.QueryString("SearchCriteria") IsNot Nothing _
                        OrElse Request.QueryString("CourseNumber") IsNot Nothing Then
                    If Session("mincatid") IsNot Nothing Then
                        id = Convert.ToInt32(Session("mincatid"))
                        url = "sony-training-catalog-2.aspx?mincatid=" & id
                    End If
                End If
                Response.Redirect(url, False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            Catch ex As FormatException     ' Session variable conversion failed.
                Response.Redirect(url, False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End Try
        End Sub

        <System.Web.Services.WebMethod(EnableSession:=True)>
        Public Shared Function addPartToCartwSTrainingClassDetails(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultTrainingClassDetails '6994
            'Public Shared Function addPartToCartwSPurchase(ByVal sPartNumber As String, ByVal sPartQuantity As String) As PartresultPurchase '6994

            Dim objPCILogger As New PCILogger() '6524
            Dim sPartNumberUpperCase As String = sPartNumber.ToUpper()
            Dim message As String
            Dim objResult As New PartresultTrainingClassDetails With {
                .partnumber = sPartNumberUpperCase,
                .Quantity = 1,
                .success = False
            }
            Dim customer As Customer = If(HttpContextManager.Customer, Nothing)
            Dim lmessage As New Label
            Dim objSSL As New SSL
            Dim carts As ShoppingCart() = objSSL.getAllShoppingCarts(customer, lmessage)
            message = lmessage.Text
            Dim sm As New ShoppingCartManager
            Dim cart As ShoppingCart
            Dim differentCart As Boolean = False
            Dim cm As CourseManager = Nothing

            Try
                ''Output1 value return form SAP helpler
                Dim output1 As String = String.Empty
                'cart = sm.AddProductToCartQuickCartExtendedWarranty(carts, sPartNumberUpperCase, sPartQuantity, True, output1, objextwarmodel) '6994
                cart = sm.AddProductToCartQuickCart(carts, sPartNumberUpperCase, sPartQuantity, True, output1) '6994
                Dim bProperpart As Boolean = True
                If Not objSSL.IsCurrentCartSameType(cart) Then
                    differentCart = True
                    objResult.differentCart = True
                End If

                For Each i As ShoppingCartItem In cart.ShoppingCartItems
                    If Not i.Product.PartNumber.Equals(sPartNumberUpperCase) Then
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 2
                        message = $"Replacement item {i.Product.PartNumber} added to cart in place of requested item {sPartNumberUpperCase}, which is no longer available."
                        objResult.message = message
                    Else
                        HttpContext.Current.Session("carts") = cart
                        objResult.items = cart.ShoppingCartItems.Length
                        objResult.totalAmount = String.Format("{0:c}", cart.ShoppingCartTotalPrice)
                        objResult.success = 1
                        message = $"Item {sPartNumberUpperCase} added to cart."
                        objResult.message = message
                    End If
                Next

            Catch exArgumentalExceptio As ArgumentException
                If exArgumentalExceptio.ParamName = "SomePartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: SomePartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "PartNotFound" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: PartNotFound", "")
                ElseIf exArgumentalExceptio.ParamName = "BillingOnlyItem" Then
                    message = exArgumentalExceptio.Message.Replace("Parameter name: BillingOnlyItem", "")
                End If
            Catch ex As Exception
                objPCILogger.EventOriginApplication = "ServicesPLUS"
                objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                objPCILogger.OperationalUser = Environment.UserName
                objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
                objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
                objPCILogger.CustomerID = customer.CustomerID
                objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
                objPCILogger.EmailAddress = customer.EmailAddress
                objPCILogger.SIAM_ID = customer.SIAMIdentity
                objPCILogger.LDAP_ID = customer.LdapID
                objPCILogger.EventOriginMethod = "addPartToCartwSTrainingClassDetails"
                objPCILogger.EventType = EventType.View
                objPCILogger.IndicationSuccessFailure = "Failure"
                objPCILogger.Message = "addPartToCart Failed. " & ex.Message.ToString()
                objPCILogger.PushLogToMSMQ()
                Dim strUniqueKey As String = Utilities.GetUniqueKey
                message = Utilities.WrapExceptionforUINumber(ex, strUniqueKey)
                message = message.Remove(message.Length - 1)
                message = message + strUniqueKey + "."
            Finally
                'objPCILogger.PushLogToMSMQ() '6524
            End Try
            objResult.message = message
            Return objResult
        End Function
    End Class

    Public Class PartresultTrainingClassDetails '6994
        Public _message As String
        Public _totalAmount As String
        Public _items As String
        Public _success As Integer
        Public _quantity As String
        Public _partnumber As String
        Public _differentCart As Boolean

        Property partnumber() As String
            Get
                Return _partnumber
            End Get
            Set(ByVal value As String)
                _partnumber = value
            End Set
        End Property

        Property message() As String
            Get
                Return _message
            End Get
            Set(ByVal value As String)
                _message = value
            End Set
        End Property

        Property Quantity() As String
            Get
                Return _quantity
            End Get
            Set(ByVal value As String)
                _quantity = value
            End Set
        End Property

        ''' <summary>
        ''' 0-Failure
        ''' 1-Success
        ''' 2-warning
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Property success() As Integer
            Get
                Return _success
            End Get
            Set(ByVal value As Integer)
                _success = value
            End Set
        End Property

        Property totalAmount() As String
            Get
                Return _totalAmount
            End Get
            Set(ByVal value As String)
                _totalAmount = value
            End Set
        End Property

        Property items() As String
            Get
                Return _items
            End Get
            Set(ByVal value As String)
                _items = value
            End Set
        End Property

        Property differentCart() As Boolean
            Get
                Return _differentCart
            End Get
            Set(ByVal value As Boolean)
                _differentCart = value
            End Set
        End Property
    End Class

End Namespace
