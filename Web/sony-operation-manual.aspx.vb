Imports System.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities

Namespace ServicePLUSWebApp

    Partial Class sony_operation_manual
        Inherits SSL

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If HttpContextManager.GlobalData.IsCanada Then
                DefaultSonyManual.Visible = False
                hiddenSonyManual.Visible = True
                Return
            Else
                DefaultSonyManual.Visible = True
                hiddenSonyManual.Visible = False
            End If

            If ConfigurationManager.AppSettings("ManualSearchLoginRequired").ToString() = "1" Then
                If HttpContextManager.Customer Is Nothing Then
                    Response.Redirect("SignIn-Register.aspx?post=om")
                End If
            End If
            If Not IsPostBack Then
                Dim blnIsContextAvailable As Boolean = False
                If Not String.IsNullOrWhiteSpace(Request.QueryString("model")) Then
                    ModelNumber.Text = Request.QueryString("model")
                    blnIsContextAvailable = True
                End If

                If blnIsContextAvailable = True Then
                    btnSearch_Click(sender, Nothing)
                End If
            End If
        End Sub

        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnSearch.Click
            Dim objCatalogManager As New CatalogManager()
            Dim xModelNumber As String
            Dim strModels As String = String.Empty
            Dim dsModels As DataSet

            Try
                If ValidateSearchCriteria() = False Then Exit Sub

                xModelNumber = ModelNumber.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()
                While xModelNumber.StartsWith("0")
                    xModelNumber = xModelNumber.Substring(1, xModelNumber.Length - 1)
                End While

                dsModels = objCatalogManager.getConflictedModels(xModelNumber)

                If dsModels.Tables(0).Rows(0)(1).ToString() <> "No Conflicted Models" Then
                    For Each item In dsModels.Tables(0).Rows
                        strModels = strModels + item(0) + ":" + item(1) + ","
                    Next
                    If Not String.IsNullOrEmpty(strModels) Then
                        strModels = strModels.TrimEnd(",")      ' Remove the last comma
                        ScriptManager.RegisterStartupScript(Me, [GetType](), "showPopup", $"javascript:OrderLookUp({dsModels.Tables(0).Rows.Count},'{strModels}');", True)
                        dgManual.CurrentPageIndex = 0
                        BindSearchResult()
                    End If
                Else
                    dgManual.CurrentPageIndex = 0
                    BindSearchResult()
                End If
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End Sub

        Private Function ValidateSearchCriteria() As Boolean
            lblError.Text = String.Empty
            If String.IsNullOrWhiteSpace(ModelNumber.Text) Then
                lblError.Text = "Please enter search criteria."
                Return False
            ElseIf Not ValidateRequest(ModelNumber.Text.Trim()) Then
                lblError.Text = "Please enter valid search criteria."
                Return False
            End If
            Return True
        End Function

        'Added the code by Arshad for Vulnerability 49679342
        Public Function ValidateRequest(ByVal strInputValue As String) As Boolean
            Dim SpecialChars() As Char = "!@#$%^&*()'_".ToCharArray
            Dim indexOf As Integer = strInputValue.IndexOfAny(SpecialChars)
            If (indexOf = -1) Then
                Return True
            Else
                Return False
            End If
        End Function

        Private Function GetSearchResult() As DataSet
            Dim xModelNumber As String = ModelNumber.Text.ToString().Replace("(", "").Replace(")", "").Replace("-", "").Replace("/", "").Replace("\", "").Replace(" ", "").ToUpper()

            While xModelNumber.StartsWith("0")
                xModelNumber = xModelNumber.Substring(1, xModelNumber.Length - 1)
            End While

            Dim xCUSTOMERID As Integer = "-1"
            Dim xCUSTOMERSEQUENCENUMBER As Integer = "-1"
            If Session.Item("customer") IsNot Nothing Then
                xCUSTOMERID = Convert.ToInt32(CType(Session.Item("customer"), Customer).CustomerID)
                xCUSTOMERSEQUENCENUMBER = Convert.ToInt32(CType(Session.Item("customer"), Customer).SequenceNumber)
            End If
            Dim xHTTP_X_FORWARDED_FOR As String = HttpContextManager.GetServerVariableValue("HTTP_X_FORWARDED_FOR")
            Dim xREMOTE_ADDR As String = HttpContextManager.GetServerVariableValue("REMOTE_ADDR")
            Dim xHTTP_REFERER As String = HttpContextManager.GetServerVariableValue("HTTP_REFERER")
            Dim xHTTP_URL As String = HttpContextManager.GetServerVariableValue("HTTP_URL")

            If xHTTP_URL = "" Then
                xHTTP_URL = HttpContextManager.GetServerVariableValue("URL") + ReturnQueryString()
            End If

            Dim xHTTP_USER_AGENT As String = HttpContextManager.GetServerVariableValue("HTTP_USER_AGENT")

            Dim objCatalogManager As New CatalogManager()
            Return objCatalogManager.SearchOperationManual(xModelNumber, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT)
        End Function

        Private Sub BindSearchResult()
            Dim objOperationManualData As DataSet
            objOperationManualData = GetSearchResult()
            If Not objOperationManualData Is Nothing Then
                If objOperationManualData.Tables(0).Rows.Count > 0 Then
                    dgManual.DataSource = objOperationManualData
                    dgManual.DataBind()
                Else
                    lblError.Text = "No result found, please change the search criteria."
                    dgManual.DataSource = Nothing
                    dgManual.DataBind()
                End If
            Else
                lblError.Text = "No result found, please change the search criteria."
                dgManual.DataSource = Nothing
                dgManual.DataBind()
            End If

        End Sub

        'Protected Sub btnClearSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClearSearch.Click
        '    statusControlPlaceHolder.Controls.Clear()
        '    Session("SesisonSearchResultCollection") = Nothing
        'End Sub

        Protected Sub btnNewSearch_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs)
            ModelNumber.Text = String.Empty
            ModelNumber.Focus()
        End Sub

        Protected Sub dgManual_PageIndexChanged(ByVal source As Object, ByVal e As DataGridPageChangedEventArgs) Handles dgManual.PageIndexChanged
            dgManual.CurrentPageIndex = e.NewPageIndex
            BindSearchResult()

        End Sub

        Protected Sub dgManual_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgManual.ItemDataBound
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblValue As Label = CType(e.Item.FindControl("lblModel"), Label)
                Dim oldvalue As String = e.Item.Cells(0).Text
                If oldvalue <> "&nbsp;" Then
                    lblValue.Text = "<br><strong>(Formerly known as " + oldvalue + ")</strong>"
                    lblValue.Visible = True
                End If
            End If
        End Sub

    End Class
End Namespace
