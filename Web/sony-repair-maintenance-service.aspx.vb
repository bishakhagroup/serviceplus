Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Process
Imports Sony.US.SIAMUtilities


Namespace ServicePLUSWebApp

    Partial Class sony_repair_maintenance_service1
        Inherits SSL

        Const conModelsCatalogGroupPath As String = "sony-repair-maintenance-service.aspx?groupid="
        Const conModelsSrchPath As String = "sony-repair-maintenance-service.aspx?srh="

        Public isEnable As String = "0"
        Public LanguageSM As String = "EN"
        Public divrprmainShow As Boolean
        Private rangePerPage As Integer
        Private modelsPerRange As Integer
        Private blnRegisterforLogin As Boolean = False

        ' ASleight - This needs to be secured. We allow the user to direct-input to a redirect URL.
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Session.Item("customer") IsNot Nothing And Session("isOneRow") IsNot Nothing Then
                    If Session("isOneRow").ToString().Trim() = "1" Then
                        Response.Redirect("sony-service-sn.aspx?model=" + ModelNumber.Text.Trim())
                    End If
                End If
                ClientScript.RegisterHiddenField("__EVENTTARGET", ImageButton1.ClientID)

                If Not Page.IsPostBack Then
                    buildModelCatalogTable()
                    PopulateStatesDDL()
                End If
                divrprmainShow = HttpContextManager.GlobalData.IsAmerica

            Catch ex As Exception
                ErrorLabel.Text = Utilities.WrapExceptionforUI(ex)
            End Try
        End Sub

        ' ASleight - This needs to be secured. We allow the user to direct-input to a javascript function.
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            LanguageSM = HttpContextManager.GlobalData.LanguageForIDP

            If Not String.IsNullOrWhiteSpace(hndLogin.Value) Then hndLogin.Value = String.Format("{0:0.#######}", Convert.ToDouble(hndLogin.Value))
            If blnRegisterforLogin = False Then
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin3", "<script language=""javascript"">function ShowLogin(){}</script>")
            Else
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "RuntimeLogin3", "<script language=""javascript"">function ShowLogin(){if(document.getElementById('hndLogin').value === '" + hndLogin.Value + "'){window.location.href='SignIn.aspx?Lang=" + LanguageSM + "&post=srms&SPGSModel=" + ModelNumber.Text.ToUpper().Trim() + "';}document.getElementById('hndLogin').value = y;}</script>")
            End If
            ImageButton1.Attributes.Add("onclick", "javascript:setHdn();")
        End Sub

        Protected Sub GridPartsGroup_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridPartsGroup.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rec As ModelDetails
                    rec = e.Row.DataItem
                    Dim lnkStartRange As HyperLink = CType(e.Row.Cells(1).Controls(0), HyperLink)
                    lnkStartRange.Style.Add("cursor", "hand")
                    lnkStartRange.Style.Add("text-decoration", "underline")

                    If rec.ModelName IsNot Nothing Then
                        Dim strModelNew = HttpUtility.UrlEncode(parseURL(ReturnCharacterSub(rec.ModelName)))
                        If strModelNew.Contains("%2b") And rec.ModelName.Contains("/") Then
                            strModelNew = strModelNew.Replace("%2b", "+")
                        End If
                        lnkStartRange.NavigateUrl = "sony-pg-service-maintenance-repair-" + strModelNew + ".aspx"
                    End If
                End If
            Catch ex As Exception
                lblErrMsgModel.Text = Utilities.WrapExceptionforUI(ex)
                lblErrMsgModel.Visible = True
            End Try
        End Sub

        ' ASleight - This needs to be secured. We allow the user to direct-input to a redirect URL.
        Protected Sub ImageButton1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            Dim modelsGroups As ModelDetails()
            Dim cm As CatalogManager = New CatalogManager
            Dim modelInput = ModelNumber.Text.Trim()

            If String.IsNullOrWhiteSpace(modelInput) Then
                lblErrMsgModel.Text = Resources.Resource.repair_svc_main_msg_3
                lblErrMsgModel.Visible = True
                Return
            End If

            modelsGroups = cm.GetSrhModelDetails(modelInput, HttpContextManager.GlobalData)

            'Added = to avoid popup blocking
            If modelsGroups.Length > 1 Then     ' ASleight - Changed from >= 1, since it blocked the ElseIf statement beneath.
                lblErrMsgModel.Text = ""
                lblErrMsgModel.Visible = False
                Response.Redirect("sony-Pg-ModelSearch-Result.aspx?srh=" + modelInput)
            ElseIf modelsGroups.Length = 1 Then ' If only one search result, send them to the detail page.
                If modelsGroups(0).DiscontinuedFlag <> "Y3" And modelsGroups(0).DiscontinuedFlag <> "Y4" And modelsGroups(0).DiscontinuedFlag <> "Y6" And modelsGroups(0).DiscontinuedFlag <> "Y7" Then
                    If Session.Item("customer") Is Nothing Then
                        blnRegisterforLogin = True
                    Else
                        Response.Redirect("sony-service-sn.aspx?model=" + modelInput)
                    End If
                Else
                    Response.Redirect("sony-pg-service-maintenance-repair.aspx?model=" + modelInput)
                End If
            Else
                lblErrMsgModel.Text = Resources.Resource.repair_error_msg
                ModelNumber.Focus()
                lblErrMsgModel.Visible = True
            End If
        End Sub

        Protected Sub ImageButton2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton2.Click
            Dim sState As String
            sState = ddlStates.SelectedValue.ToString()

            If Len(Trim(sState)) = 0 Then
                lblErrMsgState.Text = Resources.Resource.repair_error_msg_2
                lblErrMsgState.Visible = True
                ddlStates.Focus()
            Else
                lblErrMsgState.Visible = False
                Session("ShowServiceLoc") = ddlStates.SelectedValue.ToString().Trim().ToUpper()
                Response.Redirect("sony-pg-service-maintenance-repair.aspx", True)
            End If
        End Sub

        Private Sub buildModelCatalogTable()
            Dim catMan As New CatalogManager
            Dim modelsGroups As ModelDetails()

            Dim td As New TableCell
            Dim tr As New TableRow
            Dim lnkBPrev As HyperLink = CType(Me.btmPrevious, HyperLink)
            Dim lnkTPrev As HyperLink = CType(Me.topPrevious, HyperLink)
            Dim lnkBPrevImg As HyperLink = CType(Me.btmPreviousImg, HyperLink)
            Dim lnkTPrevImg As HyperLink = CType(Me.topPreviousImg, HyperLink)
            Dim lnkBNext As HyperLink = CType(Me.btmNext, HyperLink)
            Dim lnkTNext As HyperLink = CType(Me.topNext, HyperLink)
            Dim lnkBNextImg As HyperLink = CType(Me.btmNextImg, HyperLink)
            Dim lnkTNextImg As HyperLink = CType(Me.topNextImg, HyperLink)

            Dim currPage As Integer = 1
            Dim prevPage As Integer = 1
            Dim nextPage As Integer = 1
            Dim prevPageRange As Long = 1
            Dim nextPageRange As Long = 1

            catMan.GetModelRanges(rangePerPage, modelsPerRange)

            Try
                If Request.QueryString("srh") IsNot Nothing Then
                    Server.Transfer("sony-Pg-ModelSearch-Result.aspx?srh=" + Request.QueryString("srh").Trim())
                    Return
                End If

                If Request.QueryString("groupid") IsNot Nothing Then
                    currPage = Integer.Parse(Request.QueryString("groupid"))
                End If
            Catch ex As Exception
                currPage = 1
            End Try

            Dim maxGroup As Long = catMan.GetModelMaxGroupID()

            prevPageRange = currPage - modelsPerRange
            nextPageRange = currPage + modelsPerRange

            lnkBPrev.Visible = (prevPageRange > 0)
            lnkTPrev.Visible = (prevPageRange > 0)
            lnkBPrevImg.Visible = (prevPageRange > 0)
            lnkTPrevImg.Visible = (prevPageRange > 0)
            If prevPageRange > 0 Then
                prevPage = prevPageRange
            Else
                prevPage = 1
            End If

            If nextPageRange < maxGroup Then
                lnkBNext.Visible = True
                lnkTNext.Visible = True
                lnkBNextImg.Visible = True
                lnkTNextImg.Visible = True
                'nextPage = currPage + 1
                nextPage = nextPageRange
            Else
                lnkBNext.Visible = False
                lnkTNext.Visible = False
                lnkBNextImg.Visible = False
                lnkTNextImg.Visible = False
                nextPage = 1
            End If

            lnkBPrev.NavigateUrl = "model-group-" + prevPage.ToString() + ".aspx"
            lnkTPrev.NavigateUrl = "model-group-" + prevPage.ToString() + ".aspx"
            lnkBPrevImg.NavigateUrl = "model-group-" + prevPage.ToString() + ".aspx"
            lnkTPrevImg.NavigateUrl = "model-group-" + prevPage.ToString() + ".aspx"
            lnkBNextImg.NavigateUrl = "model-group-" + nextPage.ToString() + ".aspx"
            lnkTNextImg.NavigateUrl = "model-group-" + nextPage.ToString() + ".aspx"
            lnkBNext.NavigateUrl = "model-group-" + nextPage.ToString() + ".aspx"
            lnkTNext.NavigateUrl = "model-group-" + nextPage.ToString() + ".aspx"

            lnkBPrevImg.Style.Add("cursor", "hand")
            lnkTPrevImg.Style.Add("cursor", "hand")
            lnkBNextImg.Style.Add("cursor", "hand")
            lnkTNextImg.Style.Add("cursor", "hand")

            Try
                modelsGroups = catMan.GetModelDetails(currPage)
                Me.GridPartsGroup.DataSource = modelsGroups
                Me.GridPartsGroup.DataBind()
            Catch ex As Exception
                lblErrMsgModel.Text = Utilities.WrapExceptionforUI(ex)
                lblErrMsgModel.Visible = True
            End Try
        End Sub

        Protected Sub BindGrid(ByVal pSrhString As String)
            Dim modelsGroups As ModelDetails()
            Dim cm As CatalogManager = New CatalogManager

            modelsGroups = cm.GetSrhModelDetails(pSrhString, HttpContextManager.GlobalData)

            topPreviousImg.Visible = False
            topPrevious.Visible = False
            topNext.Visible = False
            topNextImg.Visible = False
            btmPreviousImg.Visible = False
            btmPrevious.Visible = False
            btmNext.Visible = False
            btmNextImg.Visible = False

            ' check for no. of rows as result from search
            If modelsGroups.Length = 0 Then
                Return
            ElseIf modelsGroups.Length = 1 Then
                Response.Redirect("sony-service-sn.aspx?model=" + modelsGroups(0).ModelName)
            End If

            Try
                Me.GridPartsGroup.DataSource = modelsGroups
                Me.GridPartsGroup.DataBind()
            Catch ex As Exception
                lblErrMsgModel.Text = Utilities.WrapExceptionforUI(ex)
                lblErrMsgModel.Visible = True
            End Try
        End Sub

        Private Function parseURL(ByVal psURL As String) As String
            Dim sURL As String = psURL
            Dim token As String = "//"
            Dim index As Integer = sURL.IndexOf(token)

            If index <> -1 Then
                sURL = Left(sURL, index + 1) + "+" + sURL.Substring(index + 2).ToString()
            End If

            token = "\"
            index = sURL.IndexOf(token)
            If index <> -1 Then
                sURL = Left(sURL, index + 1) + "!" + sURL.Substring(index + 2).ToString()
            End If

            Return sURL
        End Function

        Private Sub PopulateStatesDDL()
            Dim cm As New CatalogManager
            Dim stateList() As StatesDetail

            Try
                stateList = cm.StatesBySalesOrg(HttpContextManager.GlobalData)

                ddlStates.Items.Add(New ListItem("Select One", ""))
                For Each state In stateList
                    ddlStates.Items.Add(New ListItem(ChangeCase(state.StateName), state.StateAbbr))
                Next
            Catch ex As Exception
                lblErrMsgModel.Text = Utilities.WrapExceptionforUI(ex)
                lblErrMsgModel.Visible = True
            End Try

        End Sub

        Private Function ChangeCase(ByVal pString As String) As String
            Dim str As String = pString.ToLower()
            Return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str)
        End Function
    End Class
End Namespace
