<%@ Page Language="vb" AutoEventWireup="false" SmartNavigation="true" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>ServicesPLUS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="includes/ServicesPLUS_style.css" type="text/css">
    <script type="text/javascript" src="includes/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="includes/svcplus_globalization.js"></script>
    <style type="text/css">
        .f12 {
            font-size: 12px;
            color: #474747;
            font-family: arial,helvetica,verdana,sans-serif;
        }
    </style>
</head>
<body bgcolor="#ffffff" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <form method="post" runat="server" id="form1">
        <table width="400" border="0" role="presentation">
            <tr>
                <td>
                    <table height="65" width="400" border="0" role="presentation">
                        <tr height="38" width="400">
                            <td colspan="2" width="400" background="images/sp_int_popup_login_hdrbkgd_onepix.gif">
                                <h1 class="headerText">&nbsp;&nbsp;&nbsp;&nbsp;ServicesPLUS </h1>
                            </td>
                        </tr>
                        <tr height="10">
                            <td colspan="2" background="images/sp_int_popup_login_hdrbkgd_lowr.gif">
                                &nbsp;
                            </td>
                        </tr>
                        <tr height="1">
                            <td cellpadding="0">
                                <table height="1" width="401" border="0" role="presentation">
                                    <td width="21" colspan="1" height="9" background="images/sp_int_header_btm_right.gif"></td>
                                    <td height="1" width="378" colspann="1" background="images/sp_int_header_btm_left_onepix.gif"></td>
                                    <td height="1" width="1" colspann="1" background="images/sp_int_header_btm_right.gif"></td>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divLogin" runat="server" visible="true">
                        <table width="370" border="0" cellpadding="2" class="redAsterick" role="presentation">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td>
                                    <h2 class="tableHeader">Product Registration</h2>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="20" height="16">
                                    <img src="images/spacer.gif" width="20" height="4" alt="">
                                <td class="bodycopy" width="100%">
                                    <span style="font-size: 24px;">
                                        <table border="0" width="100%" role="main">
                                            <div id="formed">
                                                <tr>
                                                    <td>
                                                        <p class="f12">The serial number field has a requirement of 7 digits. Sony's serial 
                                                            numbers that you see on the unit itself can either be 5, 6 or 7 digits in 
                                                            length. You can take the number directly off the unit or you can get it off the 
                                                            UPC code off the shipping box of the unit. Please keep in mind that if you 
                                                            choose to input the serial number directly from the unit, you may still be 
                                                            required to submit the original UPC from the shipping box - see program bulletin 
                                                            for requirements. If the serial number on the unit is either 5 or 6 digits in 
                                                            length, you would need to add zero(s) at the beginning of the number to get to 7 
                                                            digits in length. See examples below. you must add 00 in front to get the required
                                                            7 digits to be entered. From the picture we have 11159, so the number you need to enter
                                                            is: 0011159.* If the number is 6 digits, you only need to add 1 zero.
                                                        </p>

                                                        <img src="images/SerialNumber1.jpg" alt="Image showing where to find your serial number">
                                                        
                                                        <p class="f12">Example #2 - If your serial number of the unit you sold has 7 digits, you
                                                            will just need to take the number as it appears in the field. From the picture we have
                                                            2003605, so you need to enter is: 2003605.
                                                        </p>
                                                        <img src="images/SerialNumber2.jpg" alt="Image showing where to find your serial number">
                                                        <p class="f12">Example #3 - If you are using the serial number off the UPC code from the box,
                                                            you will need to take the 7 digits AFTER the S01. In the example below the serial number
                                                            you would need to enter would be: 0010236.
                                                        </p>
                                                        <img src="images/SerialNumber3.jpg" alt="Image showing where to find your serial number">
                                            </div>
                                        </table>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><img alt="Close this browser popup window." src="<%=Resources.Resource.img_btnCloseWindow%>" onclick="window.close();" /></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
