<%@ Reference Page="~/SSL.aspx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="SonyFooter" Src="SonyFooter.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="ServicePlusNav" Src="ServicePlusNav.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="TopSonyHeader" Src="TopSonyHeader.ascx" %>
<%@ Register TagPrefix="ServicePLUSWebApp" TagName="PersonalMessage" Src="PersonalMessage.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="ServicePLUSWebApp.ContactInformation"
    EnableViewStateMac="True" CodeFile="Contact-Information.aspx.vb" CodeFileBaseClass="ServicePLUSWebApp.SSL" %>

<html lang="<%=PageLanguage %>">
<head>
    <title><%=Resources.Resource.ttl_ContactInfo%></title>
    <meta content="True" name="vs_showGrid">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="includes/ServicesPLUS_style.css" type="text/css" rel="stylesheet">

    <script src="includes/ServicesPLUS.js" type="text/javascript"></script>
    <script src="includes/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="includes/svcplus_globalization.js" type="text/javascript"></script>
</head>
<body style="color: #000000; background: #5d7180; margin: 0px;">
    <form id="Form1" method="post" runat="server">
        <center>
        <table width="760" border="0" role="presentation">
            <tr>
                    <td style="width: 25px; background: url('images/sp_left_bkgd.gif');">
                        <img src="images/spacer.gif" width="25" height="25" alt="">
                    </td>
                    <td width="710" bgcolor="#ffffff">
                        <table width="710" border="0" role="presentation">
                            <tbody>
                                <tr>
                                    <td>
                                        <ServicePLUSWebApp:TopSonyHeader ID="TopSonyHeader" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <nav role="navigation"><ServicePLUSWebApp:ServicePlusNav ID="SonyNav" runat="server" /></nav>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 710px; border: none;" role="presentation">
                                            <tr style="height: 57px;">
                                                <td style="width: 464px; vertical-align: middle; background: #363d45 url('images/sp_int_header_top_ServicesPLUS_onepix.gif');">
                                                    <h1 class="headerText" style="padding-right: 20px; text-align: right;">ServicesPLUS</h1>
                                                </td>
                                                <td style="vertical-align: top; background: #363d45;">
                                                    <img src="images/sp_int_header_top_right.gif" height="24" width="246" alt="">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background: #f2f5f8;">
                                                    <h2 class="headerTitle" style="padding-right: 20px; text-align: right;"><%=Resources.Resource.el_ContactInfo%></h2>
                                                </td>
                                                <td style="background: #99a8b5;">
                                                    <img src="images/spacer.gif" height="1" width="1" alt=""/>
                                                </td>
                                            </tr>
                                            <tr style="height: 9px;">
                                                <td style="background: #f2f5f8;">
                                                    <img height="9" src="images/sp_int_header_btm_left_onepix.gif" width="464" alt="">
                                                </td>
                                                <td style="background: #99a8b5;">
                                                    <img height="9" src="images/sp_int_header_btm_right.gif" width="246" alt="">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="710" border="0" role="presentation">
                                            <tbody>
                                                <tr>
                                                    <td width="20" height="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt=""/>
                                                    </td>
                                                    <td width="670" height="20">
                                                        <asp:Label ID="MasterErrorLabel" runat="server" CssClass="redAsterick" EnableViewState="False"/>
                                                    </td>
                                                    <td width="20" height="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                    <td valign="top" width="670">
                                                        <fieldset>
                                                            <legend class="tableHeader" style="background: #d5dee9; width: 100%; padding-left: 10px;">
                                                                <%=Resources.Resource.el_rgn_RgnSubHdl1%><span class="redAsterick">*</span>
                                                                <%=Resources.Resource.el_rgn_RgnSubHdl2%>
                                                            </legend>
                                                            <table cellpadding="3" width="100%" border="0" role="form">
                                                                <colgroup>
                                                                    <col class="tableData" style="width: 190px; text-align: right;" />
                                                                    <col class="bodyCopy" style="width: 480px;" />
                                                                </colgroup>
                                                                <tr>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="width: 190px; text-align: right;">
                                                                        <label for="txtFirstName" id="lblFirstName" runat="server"><%=Resources.Resource.el_rgn_FirstName %></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtFirstName" runat="server" Width="120px" CssClass="bodyCopy" MaxLength="50" aria-required="true"/><span
                                                                            class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtLastName" id="lblLastName" runat="server"><%=Resources.Resource.el_rgn_LastName %></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtLastName" runat="server" Width="120px" CssClass="bodyCopy" MaxLength="50" aria-required="true"/><span
                                                                            class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtEmailAddress" id="lblEmailAddress" runat="server"><%=Resources.Resource.el_rgn_Email%></label>
                                                                    </td>
                                                                    <td>
                                                                        <table width="400" border="0" role="presentation">
                                                                            <tr>
                                                                                <td width="190">
                                                                                    <SPS:SPSTextBox ID="txtEmailAddress" runat="server" Width="120px" CssClass="bodyCopy"
                                                                                        aria-required="true" MaxLength="100"/><span class="redAsterick">*</span>
                                                                                </td>
                                                                                <td>
                                                                                    <img src="images/spacer.gif" width="10" alt="">
                                                                                </td>
                                                                                <td class="tableData" width="100">
                                                                                    <label for="txtVerificationCode" id="lblVerificationCode" runat="server" visible="false"><%=Resources.Resource.el_Rgn_verificationcode%></label>
                                                                                </td>
                                                                                <td width="*">
                                                                                    <SPS:SPSTextBox ID="txtVerificationCode" Visible="false" runat="server" Width="100px" CssClass="bodyCopy"
                                                                                        MaxLength="8"/><span runat="server" id="lblVerificationCodeError" Visible="false" class="redAsterick">*</span>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <%--<tr>
																			<td class="tableData" align="right" width="190"><asp:label id="lbl_verificationcode" runat="server" CssClass="tableData" EnableViewState="False" Text="Enter Verification code"/>
																			</td>
																			<td width="480" ><SPS:SPSTextBox id="txt_Verificationcode" Visible="false" runat="server" Width="120px" CssClass="bodyCopy" MaxLength="100"/><span class="redAsterick">*</span>
																			<asp:Label ID="lbl_verificationcode_msg" Text="Please enter verification code which is sent to your emailID" runat="server" CssClass="redAsterick"/>
																			</td>
																			
																		</tr>--%>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtCompanyName" id="lblCompanyName" runat="server"><%=Resources.Resource.el_rgn_CompanyName%></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtCompanyName" runat="server" Width="120px" CssClass="bodyCopy"
                                                                            MaxLength="100" aria-required="true"/><span class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtAddress1" id="lblAddress1" runat="server"><%=Resources.Resource.el_rgn_StreetAddress%></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtAddress1" runat="server" Width="120px" CssClass="bodyCopy"
                                                                            MaxLength="50" aria-required="true"/><asp:Label ID="lblAddress1Error" runat="server" CssClass="redAsterick"/>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtAddress2" id="lblAddress2" runat="server"><%=Resources.Resource.el_rgn_2ndAddress%></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtAddress2" runat="server" Width="120px" CssClass="bodyCopy"
                                                                            MaxLength="50"/><asp:Label ID="lblAddress2Error" runat="server" CssClass="redAsterick"/>
                                                                    </td>
                                                                </tr>
                                                                <%--7901 starts--%>
                                                                <%--<tr>
																			<td class="tableData" align="right" width="190" height="27"><asp:label id="ShippingAddressLabel3" runat="server" CssClass="tableData" EnableViewState="False">Address 3rd Line:</asp:label></td>
																			<td width="480" height="27"><SPS:SPSTextBox id="ShippingAddress3" runat="server" Width="120px" CssClass="bodyCopy" MaxLength="50"/><asp:Label ID="LabelLine3Star" runat="server" CssClass="redAsterick"/></td>
																		</tr>--%>
                                                                <%--7901 ends--%>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtCity" id="lblCity" runat="server"><%=Resources.Resource.el_rgn_City %></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtCity" runat="server" Width="120px" CssClass="bodyCopy" MaxLength="50" aria-required="true" /><asp:Label
                                                                            ID="lblCityError" runat="server" CssClass="redAsterick" Text="*" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="ddlStates" id="lblState" runat="server"><%=Resources.Resource.el_rgn_State%></label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlStates" runat="server" CssClass="bodyCopy" Width="120px" aria-required="true" /><span
                                                                            class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtZipCode" id="lblZipCode" runat="server">Zip Code :</label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtZipCode" runat="server" Width="120px" CssClass="bodyCopy" aria-required="true"/><span
                                                                            class="redAsterick">*</span>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtPhone" id="lblPhone" runat="server"><%=Resources.Resource.el_rgn_Phone%></label>
                                                                    </td>
                                                                    <td>
                                                                        <table width="400" border="0" role="presentation">
                                                                            <tr>
                                                                                <td style="width: 190px;">
                                                                                    <SPS:SPSTextBox ID="txtPhone" runat="server" Width="120px" CssClass="bodyCopy" aria-required="true"/><span
                                                                                        class="redAsterick">*</span>
                                                                                </td>
                                                                                <td class="tableData" style="text-align: right; width: 65px;">
                                                                                    <label for="txtExtension" id="lblExtension" runat="server" class="tableData"><%=Resources.Resource.el_rgn_Extension%></label>&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <SPS:SPSTextBox ID="txtExtension" runat="server" Width="60px" CssClass="bodyCopy" MaxLength="6"/>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <label for="txtFax" id="lblFax" runat="server"><%=Resources.Resource.el_rgn_Fax%></label>
                                                                    </td>
                                                                    <td>
                                                                        <SPS:SPSTextBox ID="txtFax" runat="server" Width="120px" CssClass="bodyCopy"/>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableData" style="text-align: right;">
                                                                        <%=Resources.Resource.el_CustId%>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblCustomerID" runat="server" CssClass="bodyCopy"/>
                                                                    </td>
                                                                </tr>
                                                                <%--<tr style="background: #f2f5f8;">
																			<td align="right" width="190">&nbsp;&nbsp;&nbsp;
																				<span class="tableData">Ok to send me information</span></td>
																			<td width="480">
																				<table width="200" border="0" role="presentation">
																					<tr>
																						<td width="*"><asp:radiobuttonlist id="okToContact" CssClass="bodyCopy" Runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
																								CellSpacing="20">
																								<asp:ListItem Value="Yes" Selected="True">Yes&nbsp;&nbsp;&nbsp;</asp:ListItem>
																								<asp:ListItem Value="No">No</asp:ListItem>
																							</asp:radiobuttonlist></td>
																					</tr>
																				</table>
																			</td>
																		</tr>--%>
                                                                <tr id="BillTo" style="height: 26px;" runat="server">
                                                                    <td class="tableHeader" style="text-align: right; vertical-align: top;">
                                                                        <asp:Label ID="lblBillTo" runat="server" EnableViewState="False"
                                                                            Visible="false" Text="<%$ Resources:Resource, el_Billto%>"/>&nbsp;
                                                                    </td>
                                                                    <td style="vertical-align: top;">
                                                                        <asp:Label ID="lblBillToData" runat="server" CssClass="tableData"/>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 26px; background: #f2f5f8;">
                                                                    <td class="tableHeader" style="text-align: right; vertical-align: top;">
                                                                    </td>
                                                                    <td style="vertical-align: top;">
                                                                        <asp:Label ID="lblBillToCSNumber" runat="server" CssClass="tableHeader" Height="30px"
                                                                            Text="<%$ Resources:Resource, el_AcctDet1%>"/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                    <td>
                                                                        <img height="1" src="images/spacer.gif" width="1" alt="">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <img height="10" src="images/spacer.gif" width="1" alt="">
                                                                        <asp:ImageButton ID="btnUpdateProfile" runat="server" ImageUrl="<%$ Resources:Resource,sna_svc_img_65%>"
                                                                            Width="92" Height="30" AlternateText="Click to update your contact information with the new values."
                                                                            title="Click to update your contact information with the new values."/>
                                                                    </td>
                                                                </tr>
                                                        </table>
                                                        </fieldset>
                                                    </td>
                                                    <td width="20">
                                                        <img height="20" src="images/spacer.gif" width="20" alt="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <footer><ServicePLUSWebApp:SonyFooter ID="SonyFooter1" runat="server" /></footer>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="25" style="background: url('images/sp_right_bkgd.gif')">
                        <img src="images/spacer.gif" width="25" height="20" alt="">
                    </td>
                </tr>
        </table>
    </center>
    </form>

    <script type="text/javascript">
        <%
        If focusFormField <> "" Then
            Response.Write("document.getElementById('" + focusFormField + "').focus();")
        End If
        %>
</script>

</body>
</html>
