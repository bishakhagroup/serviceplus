﻿Imports System.Text
Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports System
Public Class ProcessUtilities

    Public Function GetSPSErrorList() As DataSet
        Dim dsSPSErrorList As DataSet
        Dim objDBUtilities As New DBUtilities()
        dsSPSErrorList = objDBUtilities.GetSPSErrorList()
        Return dsSPSErrorList
    End Function

End Class
