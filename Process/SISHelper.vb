Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.siam

Public Class Address
    Private _Name As String
    Private _Address1 As String
    Private _Address2 As String
    Private _Address3 As String
    Private _Address4 As String
    Private _State As String
    Private _zip As String
    Private _Attention As String
    Private _Phone As String
    Public Property Name() As String
        Get
            Return _Name

        End Get
        Set(ByVal Value As String)
            _Name = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return _Address1
        End Get
        Set(ByVal Value As String)
            _Address1 = Value
        End Set
    End Property


    Public Property Address2() As String
        Get
            Return _Address2
        End Get
        Set(ByVal Value As String)
            _Address2 = Value
        End Set
    End Property


    Public Property Address3() As String
        Get
            Return _Address3
        End Get
        Set(ByVal Value As String)
            _Address3 = Value
        End Set
    End Property


    Public Property Address4() As String
        Get
            Return _Address4
        End Get
        Set(ByVal Value As String)
            _Address4 = Value
        End Set
    End Property


    Public Property State() As String
        Get
            Return _State
        End Get
        Set(ByVal Value As String)
            _State = Value
        End Set
    End Property


    Public Property Zip() As String
        Get
            Return _zip
        End Get
        Set(ByVal Value As String)
            _zip = Value
        End Set
    End Property
    Public Property Phone() As String
        Get
            Return _Phone
        End Get
        Set(ByVal Value As String)
            _Phone = Value
        End Set
    End Property
    Public Property Attention() As String
        Get
            Return _Attention
        End Get
        Set(ByVal Value As String)
            _Attention = Value
        End Set
    End Property
End Class


Public Class SAPPayer
    Private _SAPAccountNumber As String
    Private _BillTo As Address

    Public Property BillTo() As Address
        Get
            Return _BillTo
        End Get
        Set(ByVal Value As Address)
            _BillTo = Value
        End Set
    End Property

    'To Bind Datagrid in Validate Account Web Page . 
    Public ReadOnly Property AccountNumber() As String
        Get
            Return _SAPAccountNumber
        End Get
    End Property

    Public Property SAPAccountNumber() As String
        Get
            Return _SAPAccountNumber
        End Get

        Set(ByVal Value As String)
            _SAPAccountNumber = Value
        End Set
    End Property
End Class

'Added by chandra bug 259
'Public Class ProcessWebRequest
'    Inherits SISServices

'    Public Sub New(ByVal ServerIP As String)
'        MyBase.New(ServerIP)
'    End Sub

'    Protected Overrides Function GetWebRequest(ByVal uri As Uri) As System.Net.WebRequest
'        Dim webRequest As System.Net.HttpWebRequest
'        webRequest = CType(MyBase.GetWebRequest(uri), System.Net.HttpWebRequest)
'        webRequest.KeepAlive = False
'        GetWebRequest = webRequest
'    End Function

'End Class

Public Class Legacy_SISHelper

    Private services As New Object

    Private irKit As New Object
    Private kitItem As New Object



    Private irLegacyAccount As New Object

    Private irOrder As New Object
    Private orderDetail As New Object
    Private orderInvoice As New Object

    Private irPart As New Object
    Private part As New Object

    Private irSAPName As New Object
    Private sapPayer As New Object

    Private irSAPPayer As New Object
    'Private sapReceiver As New Object

    Private nrOrder As New Object
    'Private newOrderDetail As New Object
    Private nAddress As New Object
    Private urOrder As New Object
    Private nOrderReqDetail As New Object
    Private nOrderRequest As New Object
    Private uOrderReqDetail As New Object
    Private nOrder As New Object
    Private serviceEnvironment As String
    Private Const ClassName As String = "SISHelper"
    'Private Logger As New SonyLogger
    Private m_objSISParameter As SISParameter
    'Public Sub New(ByRef objSISParameter As SISParameter) ' For fixing Bug# 1212
    '    m_objSISParameter = objSISParameter
    '    Dim strServerURL As String
    '    'If objSISParameter.UserLocation = -1 Then
    '    '    strServerURL = ConfigurationData.Environment.SISWebService.DefaultSISService
    '    'ElseIf objSISParameter.UserLocation = 1 Then
    '    '    strServerURL = ConfigurationData.Environment.SISWebService.ServiceB1
    '    'ElseIf objSISParameter.UserLocation = 2 Then
    '    '    strServerURL = ConfigurationData.Environment.SISWebService.Service01
    '    'End If
    '    strServerURL = ConfigurationData.Environment.SISWebService.ServiceB1
    '    'strServerURL = ConfigurationSettings.AppSettings(ConfigurationData.GeneralSettings.Environment.ToLower().ToString() + "B1")
    '    services = New ProcessWebRequest(strServerURL)

    '    irKit = New WebMethods.Services.InquiryReturnKit
    '    kitItem = New WebMethods.Services.KitItem

    '    irLegacyAccount = New WebMethods.Services.InquiryReturnLegacyAccount
    '    irOrder = New WebMethods.Services.InquiryReturnOrder
    '    orderDetail = New WebMethods.Services.OrderDetail
    '    orderInvoice = New WebMethods.Services.OrderInvoice
    '    irPart = New WebMethods.Services.InquiryReturnPart
    '    part = New WebMethods.Services.Part
    '    irSAPName = New WebMethods.Services.InquiryReturnSAPName
    '    sapPayer = New WebMethods.Services.SAPPayer
    '    irSAPPayer = New WebMethods.Services.InquiryReturnSAPPayer
    '    nrOrder = New WebMethods.Services.NewReturnOrder
    '    nAddress = New WebMethods.Services.NewAddress
    '    nOrderReqDetail = New WebMethods.Services.NewOrderRequestDetail
    '    nOrderRequest = New WebMethods.Services.NewOrderRequest
    '    urOrder = New WebMethods.Services.UpdateReturnOrder
    '    nOrder = New WebMethods.Services.NewOrder
    'End Sub
    Public Function GetVersion() As String
        Try
            Return services.Version()
        Catch ex As Exception
            Return "Error in getting version:" + ex.Message
        End Try
    End Function
    Public Function PartInquiry(ByRef customer As Customer, ByVal part_number As String, Optional ByVal determine_account_number As Boolean = True) As Object 'Sony.US.ServicesPLUS.Process.WebMethods.Services.Part

        'Logger.RaiseTrace(ClassName, "Enter PartInquiry (" + part_number + ")", TraceLevel.Verbose)

        Dim PartNumbers(1) As String
        Dim SAP_Account_Number As String

        PartNumbers.SetValue(part_number, 0)

        Try
            If determine_account_number Then
                If customer.SAPBillToAccounts.Length > 0 And customer.IsActive Then
                    SAP_Account_Number = customer.SAPBillToAccounts(0).AccountNumber.ToString()
                Else
                    SAP_Account_Number = ""
                End If
            Else
                SAP_Account_Number = ""
            End If
            irPart = services.PartInquiry(SAP_Account_Number, PartNumbers)
            If (irPart.ErrorNumber <> 0) Then
                Select Case irPart.ErrorNumber
                    Case 1017
                        Throw New ApplicationException("SAP Account Not Found F#" + SAP_Account_Number.ToString())
                    Case Else
                        Throw New ApplicationException(irPart.ErrorMessage.ToString().Trim().ToUpperInvariant())
                End Select
            End If
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "PartInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If irPart.ErrorNumber = 0 Then
            part = irPart.Parts.GetValue(0)
            If part.StatusFlag = "N" Then
                Throw New Exception("Part not found (" + part_number + ")")
            End If
        Else
            If irPart.ErrorNumber = 1041 Then  'inactive - add - or deactivated
                Return PartInquiry(customer, part_number, False)
            Else
                Throw New Exception(irPart.ErrorNumber.ToString() + ":" + irPart.ErrorMessage)
            End If
        End If

        'Logger.RaiseTrace(ClassName, "Exit PartInquiry (" + part_number + ")", TraceLevel.Verbose)

        Return irPart.Parts.GetValue(0)
    End Function

    Public Function SAPPayorInquiry(ByRef sap_bill_to_account As Account, ByVal details As Boolean) As Object()

        'Logger.RaiseTrace(ClassName, "Enter SAPPayorInquiry (" + sap_bill_to_account.AccountNumber + ")", TraceLevel.Verbose)

        Dim intI As Integer = 0
        Try
            irSAPPayer = services.SAPPayerInquiry(sap_bill_to_account.AccountNumber, details)

            If irSAPPayer.ErrorNumber = 0 And details = True Then
                sap_bill_to_account.Address.Name = irSAPPayer.SAPPayer.BillTo.Name
                sap_bill_to_account.Address.Line1 = irSAPPayer.SAPPayer.BillTo.Address1
                sap_bill_to_account.Address.Line2 = irSAPPayer.SAPPayer.BillTo.Address2
                sap_bill_to_account.Address.Line3 = irSAPPayer.SAPPayer.BillTo.Address3
                sap_bill_to_account.Address.Line4 = IIf(irSAPPayer.SAPPayer.BillTo.Address4 Is Nothing, "", irSAPPayer.SAPPayer.BillTo.Address4)
                sap_bill_to_account.Address.State = irSAPPayer.SAPPayer.BillTo.State
                sap_bill_to_account.Address.PostalCode = irSAPPayer.SAPPayer.BillTo.Zip
            End If
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "SAPPayerInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If irSAPPayer.ErrorNumber <> 0 Then
            Throw New Exception(irSAPPayer.ErrorNumber.ToString() + ":" + irSAPPayer.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit SAPPayorInquiry (" + sap_bill_to_account.AccountNumber + ")", TraceLevel.Verbose)

        Try
            irSAPPayer.SAPReceivers = irSAPPayer.SAPReceivers
        Catch ex As Exception
            Throw ex
        End Try
        Return irSAPPayer.SAPReceivers
    End Function

    Public Function KitInquiry(ByVal model_number As String, ByVal detail As Boolean) As Object 'Sony.US.ServicesPLUS.Process.WebMethods.Services.Kit

        'Logger.RaiseTrace(ClassName, "Enter KitInquiry (" + model_number + ")", TraceLevel.Verbose)

        Dim intI As Integer = 0

        Try
            irKit = services.KitInquiry(UCase(model_number), detail)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "KitInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If irKit.ErrorNumber <> 0 Then
            Throw New Exception(irKit.ErrorNumber.ToString() + ":" + irKit.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit KitInquiry (" + model_number + ")", TraceLevel.Verbose)

        Return irKit.Kit
    End Function

    'Private Function DetermineAccountNumber(ByRef customer As Customer) As String

    '    'Logger.RaiseTrace(ClassName, "Enter DetermineAccountNumber (" + customer.SIAMIdentity + ")", TraceLevel.Verbose)

    '    Dim sis_legacy_ship_to_accounts() As Account
    '    Dim account As Account
    '    Dim customer_manager As New CustomerManager
    '    Dim legacy_account_number As String

    '    'if has sap account use it as the driver
    '    If customer.SAPBillToAccounts.Length > 0 Then
    '        customer_manager.GetLegacyShipToAccounts(customer) 'Changes done by Kannapiran towards Bug# 1212

    '        'if sap accounts have not been validated then this may return Nothing
    '        sis_legacy_ship_to_accounts = customer.SISLegacyShipToAccounts()

    '        'then you get list price
    '        If sis_legacy_ship_to_accounts.Length < 1 Then
    '            'Throw New Exception("SAP account number on file does not have any associated legacy (SIS) account numbers.")
    '            legacy_account_number = ""
    '        Else
    '            'otherwise pick one of the legacy accounts to use for pricing.
    '            'if approved in SIAM via B2B portal - otherwise still get list price
    '            If ApprovedInSIAM(customer) Then
    '                account = sis_legacy_ship_to_accounts.GetValue(0)
    '                legacy_account_number = account.AccountNumber
    '            Else
    '                legacy_account_number = ""
    '            End If
    '        End If

    '    Else 'if has legacy account as part of profile use that (e.g. internal sony users)
    '        If customer.SISLegacyBillToAccounts.Length > 0 Then
    '            account = customer.SISLegacyBillToAccounts.GetValue(0)
    '            legacy_account_number = account.AccountNumber
    '        Else  'credit card consumer with no account, let default in SIS
    '            legacy_account_number = ""
    '        End If
    '    End If

    '    'Logger.RaiseTrace(ClassName, "Returning account (" + legacy_account_number + ")", TraceLevel.Verbose)
    '    'Logger.RaiseTrace(ClassName, "Exit DetermineAccountNumber (" + customer.SIAMIdentity + ")", TraceLevel.Verbose)

    '    Return legacy_account_number
    'End Function

    Private Function ApprovedInSIAM(ByVal customer As Customer) As Boolean
        Dim rc As Boolean = False
        Try
            'all valid status codes
            Dim thisSA As New SecurityAdministrator
            'thisUS = thisSA.GetStatusCodes.SiamPayLoad'6668

            'customer status code
            Dim thisStatus As Integer = 0
            'thisStatus = thisSA.GetUser(customer.SIAMIdentity).TheUser.UserStatus'6668
            thisStatus = thisSA.GetUser(customer.SIAMIdentity).TheUser.UserStatusCode '6668
            If thisStatus = 1 Then '6668
                rc = True '6668
            End If '6668

            '6668 starts
            'For Each s As UserStatus In thisUS
            '    If s.StatusId = thisStatus And s.StatusName = "Active" Then
            '        rc = True
            '        Exit For
            '    End If
            'Next
            '6668 ends
        Catch ex As Exception
            rc = False
        End Try

        Return rc

    End Function

    Public Function InternalCustomer(ByVal customer As Customer) As Boolean
        Dim rc As Boolean = False

        Dim thisUS As SiamResponse

        Try
            Dim thisSA As New SecurityAdministrator
            thisUS = thisSA.GetUserTypes

            Dim thisStatus As String
            thisStatus = thisSA.GetUser(customer.SIAMIdentity).TheUser.UserType

            'If thisStatus = "Internal" Then'6668
            If thisStatus = "I" Then '6668
                rc = True
            End If

        Catch ex As Exception
            rc = False
        End Try

        Return rc

    End Function

    Public Function CreateAccountCreditCardOrder(ByRef phone As String, ByRef cart As ShoppingCart, ByRef bill_to As Sony.US.ServicesPLUS.Core.Account, ByRef ship_to As Sony.US.ServicesPLUS.Core.SISAccount, ByRef ship_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean, ByVal tax_exempt As String, ByVal billing_only As Boolean) As Object    'Sony.US.ServicesPLUS.Process.WebMethods.Services.NewOrder
        'Logger.RaiseTrace(ClassName, "Enter CreateCreditCardOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)
        Dim Response As New WebMethods.Services.NewReturnOrder
        Dim order_request As New WebMethods.Services.NewOrderRequest
        Dim intI As Integer = 0
        Dim customerName As String
        Try
            order_request.LegacyAccountNumber = ship_to.AccountNumber
            order_request.SAPAccountNumber = bill_to.AccountNumber
            order_request.ShipTo = New WebMethods.Services.NewAddress
            customerName = ship_to.ShipTo.Name
            If customerName.Length > 30 Then
                customerName = customerName.Substring(0, 30)
            End If
            order_request.ShipTo.Name = customerName
            order_request.ShipTo.Attention = ship_to.Address.Attn
            order_request.ShipTo.Address1 = ship_to.ShipTo.Line1
            order_request.ShipTo.Address2 = ship_to.ShipTo.Line2
            order_request.ShipTo.Address3 = ship_to.ShipTo.Line3
            ' for a/c orders its not mandatory to have city specified
            ' hence many a/c records may have null in the city field
            ' this manipulation is just to get through the web service validation 
            ' which says non a/c orders should have city specified
            If ship_to.ShipTo.City <> "" Then
                order_request.ShipTo.City = ship_to.ShipTo.City
            Else
                order_request.ShipTo.City = "-"
            End If
            order_request.ShipTo.State = ship_to.ShipTo.State
            order_request.ShipTo.Zip = ship_to.ShipTo.PostalCode
            order_request.ShipTo.Phone = phone
            If ship_backorders_same_way Then
                order_request.CarrierCode = ship_method.SISBackOrderCarrierCode
                order_request.GroupNumber = ship_method.SISBackOrderPickGroupCode
            Else
                order_request.CarrierCode = ship_method.SISCarrierCode
                order_request.GroupNumber = ship_method.SISPickGroupCode
            End If
            ' modification as a part of enhancement of web service for SAP
            'order_request.POReference = cart.PurchaseOrderNumber
            Dim i As Integer = 0
            Dim order_detail As Object '  WebMethods.Services.NewOrderRequestDetail

            'my_details(cart.ShoppingCartItems.Length - 1) = New WebMethods.Services.NewOrderRequestDetail
            Dim my_details(cart.ShoppingCartItems.Length - 1) As WebMethods.Services.NewOrderRequestDetail
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                order_detail = New WebMethods.Services.NewOrderRequestDetail

                order_detail.PartNumber = item.Product.PartNumber
                order_detail.PartQuantity = item.Quantity
                'SpecialTaxFlag to is removed as a part of web service update for SAP
                'If item.Product.SpecialTax > 0.0 Then
                '    order_detail.SpecialTaxFlag = "Y"
                'Else
                '    order_detail.SpecialTaxFlag = "N"
                'End If
                my_details.SetValue(order_detail, i)
                i = i + 1
            Next

            order_request.Details = my_details

            'Console.WriteLine("bill to zip: " + order_request.BillTo.Zip)
            'Console.WriteLine("ship to zip: " + order_request.ShipTo.Zip)

            '' modification as a part of enhancement of web service for SAP
            'Response = services.CreateCreditCardOrder(order_request, billing_only, tax_exempt)
            Response = services.CreateCreditCardOrder(order_request, cart.PurchaseOrderNumber, billing_only, tax_exempt)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "CreateCreditCardOrder Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If Response.ErrorNumber <> 0 Then
            Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit CreateCreditCardOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)

        Return Response.NewOrder
    End Function

    Public Function CreateCreditCardOrder(ByRef phone As String, ByRef cart As ShoppingCart, ByRef bill_to As Sony.US.ServicesPLUS.Core.Address, ByRef ship_to As Sony.US.ServicesPLUS.Core.Address, ByRef ship_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean, ByVal tax_exempt As String, ByVal billing_only As Boolean) As Object 'Sony.US.ServicesPLUS.Process.WebMethods.Services.NewOrder

        'Logger.RaiseTrace(ClassName, "Enter CreateCreditCardOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)

        Dim Response As New WebMethods.Services.NewReturnOrder
        Dim order_request As New WebMethods.Services.NewOrderRequest

        Dim intI As Integer = 0

        Dim customerName As String

        'path.action = "urn:bts-Action/Sony Electronics/SISServices/CreateCreditCardOrder"
        'services.SecurityValue = _web_x_credentials
        'services.pathValue = path

        Try
            order_request.LegacyAccountNumber = ""
            order_request.SAPAccountNumber = ""

            order_request.ShipTo = New WebMethods.Services.NewAddress
            order_request.ShipTo.Name = ship_to.Name

            customerName = ship_to.Attn
            If customerName.Length > 30 Then
                customerName = customerName.Substring(0, 30)
            End If

            order_request.ShipTo.Attention = customerName 'ship_to.Attn.Substring(0, 30)
            order_request.ShipTo.Address1 = ship_to.Line1
            order_request.ShipTo.Address2 = ship_to.Line2
            order_request.ShipTo.Address3 = ship_to.Line3
            order_request.ShipTo.City = ship_to.City
            order_request.ShipTo.State = ship_to.State
            order_request.ShipTo.Zip = ship_to.PostalCode
            order_request.ShipTo.Phone = phone

            If ship_backorders_same_way Then
                order_request.CarrierCode = ship_method.SISBackOrderCarrierCode
                order_request.GroupNumber = ship_method.SISBackOrderPickGroupCode
            Else
                order_request.CarrierCode = ship_method.SISCarrierCode
                order_request.GroupNumber = ship_method.SISPickGroupCode
            End If

            ' modification as a part of enhancement of web service for SAP
            ' the po reference is removed from the new order request class
            'order_request.POReference = cart.PurchaseOrderNumber

            Dim i As Integer = 0

            Dim order_detail As Object '  WebMethods.Services.NewOrderRequestDetail
            'my_details(cart.ShoppingCartItems.Length - 1) = New WebMethods.Services.NewOrderRequestDetail
            Dim my_details(cart.ShoppingCartItems.Length - 1) As WebMethods.Services.NewOrderRequestDetail
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                order_detail = New WebMethods.Services.NewOrderRequestDetail

                order_detail.PartNumber = item.Product.PartNumber
                order_detail.PartQuantity = item.Quantity
                'SpecialTaxFlag to is removed as a part of web service update for SAP
                'If item.Product.SpecialTax > 0.0 Then
                '    order_detail.SpecialTaxFlag = "Y"
                'Else
                '    order_detail.SpecialTaxFlag = "N"
                'End If
                my_details.SetValue(order_detail, i)
                i = i + 1
            Next
            order_request.Details = my_details
            ' modification as a part of enhancement of web service for SAP
            'Response = services.CreateCreditCardOrder(order_request, billing_only, tax_exempt)
            Response = services.CreateCreditCardOrder(order_request, cart.PurchaseOrderNumber, billing_only, tax_exempt)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "CreateCreditCardOrder Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try
        If Response.ErrorNumber <> 0 Then
            Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit CreateCreditCardOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)

        Return Response.NewOrder
    End Function

    Public Function CreateAccountOrder(ByRef cart As ShoppingCart, ByRef bill_to As Sony.US.ServicesPLUS.Core.Account, ByVal bill_to_override As Boolean, ByRef ship_to As Sony.US.ServicesPLUS.Core.SISAccount, ByVal ship_to_override As Boolean, ByRef ship_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean, ByVal billing_only As Boolean, ByVal TaxExempt As String) As Object   ' Sony.US.ServicesPLUS.Process.WebMethods.Services.NewOrder

        'Logger.RaiseTrace(ClassName, "Enter CreateAccountOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)

        Dim Response As New WebMethods.Services.NewReturnOrder
        Dim order_request As New WebMethods.Services.NewOrderRequest

        Dim intI As Integer = 0
        Dim customerName As String
        Dim sis_legacy_account As SISAccount
        Dim sis_account As Boolean = False
        Try
            sis_legacy_account = CType(bill_to, SISAccount)
            sis_account = True
        Catch ex As Exception
            sis_account = False
        End Try

        Try
            order_request.LegacyAccountNumber = ship_to.AccountNumber

            If sis_account Then
                order_request.SAPAccountNumber = ""
            Else
                order_request.SAPAccountNumber = bill_to.AccountNumber
            End If


            If ship_to_override Then
                order_request.ShipTo = New WebMethods.Services.NewAddress

                order_request.ShipTo.Name = ship_to.Address.Name
                'order_request.Shipto.Attention = ship_to.ShipTo.Name
                customerName = ship_to.Address.Attn
                If customerName.Length > 30 Then
                    customerName = customerName.Substring(0, 30)
                End If
                order_request.ShipTo.Attention = customerName '(ship_to.Customer.FirstName + " " + ship_to.Customer.LastName).Substring(0, 30)
                order_request.ShipTo.Address1 = ship_to.ShipTo.Line1
                order_request.ShipTo.Address2 = ship_to.ShipTo.Line2
                order_request.ShipTo.Address3 = ship_to.ShipTo.Line3
                order_request.ShipTo.City = ship_to.ShipTo.City
                order_request.ShipTo.State = ship_to.ShipTo.State
                order_request.ShipTo.Zip = ship_to.ShipTo.PostalCode
                order_request.ShipTo.Phone = ship_to.Customer.PhoneNumber + ship_to.Customer.PhoneExtension
                'order_request.Shipto.Attention = ship_to.ShipTo.Name
            End If

            If ship_backorders_same_way Then
                order_request.CarrierCode = ship_method.SISBackOrderCarrierCode
                order_request.GroupNumber = ship_method.SISBackOrderPickGroupCode
            Else
                order_request.CarrierCode = ship_method.SISCarrierCode
                order_request.GroupNumber = ship_method.SISPickGroupCode
            End If

            ' order_request.POReference = cart.PurchaseOrderNumber

            Dim i As Integer = 0
            'Dim my_details(cart.ShoppingCartItems.Length - 1) As NewOrderRequestDetail
            'Dim my_details(cart.ShoppingCartItems.Length - 1) As WebMethods.Services.NewOrderRequestDetail
            Dim my_details(cart.ShoppingCartItems.Length - 1) As WebMethods.Services.NewOrderRequestDetail
            Dim order_detail As Object ' Sony.US.ServicesPLUS.Process.WebMethods.Services.NewOrderRequestDetail
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                order_detail = New WebMethods.Services.NewOrderRequestDetail
                order_detail.PartNumber = item.Product.PartNumber
                order_detail.PartQuantity = item.Quantity
                'spl tax is no more available as a part of web service update for SAP
                'order_detail.SpecialTaxFlag = item.Product.SpecialTax
                my_details.SetValue(order_detail, i)
                i = i + 1
            Next
            order_request.Details = my_details
            Response = services.CreateAccountOrder(order_request, cart.PurchaseOrderNumber, billing_only, TaxExempt)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "CreateAccountOrder Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try
        If Response.ErrorNumber <> 0 Then
            Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
        End If
        'Logger.RaiseTrace(ClassName, "Exit CreateAccountOrder (" + cart.Customer.SIAMIdentity + ")", TraceLevel.Verbose)
        Return Response.NewOrder
    End Function

    Public Function ConfirmCreditCardOrder(ByVal order_number As String, ByVal tax_exempt As String, ByVal card As CreditCard, ByVal auth_code As String)
        'Logger.RaiseTrace(ClassName, "Enter ConfirmCreditCardOrder (" + order_number + ")", TraceLevel.Verbose)
        Dim Response As New WebMethods.Services.NewReturnOrder
        Dim intI As Integer = 0
        Dim sis_expiration As String
        Try
            Dim slash_idx As Integer
            slash_idx = card.ExpirationDate.IndexOf("/")
            Dim month As String = card.ExpirationDate.Substring(0, slash_idx)

            If month.Length = 1 Then
                month = month.Insert(0, "0")
            End If

            Dim next_slash_idx = card.ExpirationDate.IndexOf("/", slash_idx + 1)
            Dim year As String = card.ExpirationDate.Substring(next_slash_idx + 1, 4)

            year = year.Remove(0, 2)

            sis_expiration = month + year
        Catch ex As Exception
            sis_expiration = "0109"
        End Try

        Try
            'Changed by chandra on 22-06-07 for implementation of Enahancement :E684
            Response = services.ConfirmCreditCardOrder(order_number, tax_exempt, card.CreditCardNumberMasked, sis_expiration, auth_code)
            'Response = services.ConfirmCreditCardOrder(order_number, tax_exempt, card.CreditCardNumber, sis_expiration, auth_code)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "ConfirmCreditCardOrder Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If Response.ErrorNumber <> 0 Then
            Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit ConfirmCreditCardOrder (" + order_number + ")", TraceLevel.Verbose)

        Return Response.NewOrder
    End Function

    Public Function ConfirmAccountOrder(ByVal order_number As String, ByVal TaxExempt As String)

        'Logger.RaiseTrace(ClassName, "Enter ConfirmAccountOrder (" + order_number + ")", TraceLevel.Verbose)

        Dim Response As New WebMethods.Services.NewReturnOrder
        Dim intI As Integer = 0
        Try
            Response = services.ConfirmAccountOrder(order_number, TaxExempt)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "ConfirmAccountOrder Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If Response.ErrorNumber <> 0 Then
            If Response.ErrorNumber = 3014 Then
                Throw New Exception("Your order " + order_number + " was placed on hold.  Please contact Customer Service at 1-800-538-7550 to complete your order.")
            Else
                Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
            End If
        End If

        'Logger.RaiseTrace(ClassName, "Exit ConfirmAccountOrder (" + order_number + ")", TraceLevel.Verbose)

        Return Response.NewOrder
    End Function

    ' 2018-10-04 ASleight - No references to this method
    'Public Function CancelOrder(ByVal order_number As String)
    '    'Logger.RaiseTrace(ClassName, "Enter CancelOrder (" + order_number + ")", TraceLevel.Verbose)
    '    Dim Response As New WebMethods.Services.UpdateReturnOrder
    '    Dim intI As Integer = 0

    '    Try
    '        Response = services.CancelOrder(order_number)
    '    Catch ex As Exception
    '        'Logger.RaiseTrace(ClassName, "CancelOrder Exception: " + ex.Message, TraceLevel.Error)
    '        Throw ex
    '    End Try

    '    If Response.ErrorNumber <> 0 Then
    '        Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
    '    End If
    '    'Logger.RaiseTrace(ClassName, "Exit CancelOrder (" + order_number + ")", TraceLevel.Verbose)
    'End Function

    Public Function OrderInquiry1(ByVal order_number As String, ByVal get_details As Boolean, ByVal get_invoices As Boolean) As Object 'Sony.US.ServicesPLUS.Process.WebMethods.Services.Order

        'Logger.RaiseTrace(ClassName, "Enter OrderInquiry (" + order_number + ")", TraceLevel.Verbose)

        Dim intI As Integer = 0

        'path.action = "urn:bts-Action/Sony Electronics/SISServices/OrderInquiry"
        'services.SecurityValue = _web_x_credentials
        'services.pathValue = path

        Try
            irOrder = services.OrderInquiry(order_number, True, get_details, get_invoices)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "OrderInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If irOrder.ErrorNumber <> 0 Then
            Throw New Exception(irOrder.ErrorNumber.ToString() + ":" + irOrder.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit OrderInquiry (" + order_number + ")", TraceLevel.Verbose)

        Return irOrder.Order
    End Function

    ' 2018-10-04 ASleight - No references to this function
    'Public Function CancelOrderLineItem(ByVal order_number As String, ByVal lines() As String)
    '    'Logger.RaiseTrace(ClassName, "Enter CancelOrderLineItem (" + order_number + ")", TraceLevel.Verbose)
    '    Dim Response As New WebMethods.Services.UpdateReturnOrder
    '    Dim intI As Integer = 0

    '    Try
    '        Response = services.CancelBackOrderItems(order_number, lines)
    '    Catch ex As Exception
    '        'Logger.RaiseTrace(ClassName, "CancelBackOrderItems Exception: " + ex.Message, TraceLevel.Error)
    '        Throw ex
    '    End Try

    '    If Response.ErrorNumber <> 0 Then
    '        Throw New Exception(Response.ErrorNumber.ToString() + ":" + Response.ErrorMessage)
    '    End If
    '    'Logger.RaiseTrace(ClassName, "Exit CancelOrderLineItem (" + order_number + ")", TraceLevel.Verbose)
    'End Function

    Public Function SAPNameInquiry(ByVal customer_name As String) As Object()

        'Logger.RaiseTrace(ClassName, "Enter SAPNameInquiry (" + customer_name + ")", TraceLevel.Verbose)

        'Dim svc As New SISServices
        'Dim Response As InquiryReturnSAPName
        Dim intI As Integer = 0


        Dim accountArray As New ArrayList


        'path.action = "urn:bts-Action/Sony Electronics/SISServices/SAPNameInquiry"
        'services.SecurityValue = _web_x_credentials
        'services.pathValue = path

        Try
            irSAPName = services.SAPNameInquiry(customer_name)
            If irSAPName.ErrorNumber <> 0 Then
                Throw New Exception(irSAPName.ErrorNumber.ToString() + ":" + irSAPName.ErrorMessage)
            Else
                Dim loopCnt As Integer
                If irSAPName.SAPPayers().length > 0 Then

                    For loopCnt = 0 To irSAPName.SAPPayers().length - 1
                        Dim sap_payer_account As Sony.US.ServicesPLUS.Process.SAPPayer
                        sap_payer_account = New Sony.US.ServicesPLUS.Process.SAPPayer

                        If Not irSAPName.SAPPayers(loopCnt).SAPAccountNumber Is Nothing Then
                            sap_payer_account.SAPAccountNumber = irSAPName.SAPPayers(loopCnt).SAPAccountNumber.ToString()
                        Else
                            sap_payer_account.SAPAccountNumber = ""
                        End If

                        sap_payer_account.BillTo = New Sony.US.ServicesPLUS.Process.Address

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Name Is Nothing Then
                            sap_payer_account.BillTo.Name = irSAPName.SAPPayers(loopCnt).BillTo.Name.ToString()
                        Else
                            sap_payer_account.BillTo.Name = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Address1 Is Nothing Then
                            sap_payer_account.BillTo.Address1 = irSAPName.SAPPayers(loopCnt).BillTo.Address1.ToString()
                        Else
                            sap_payer_account.BillTo.Address1 = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Address2 Is Nothing Then '
                            sap_payer_account.BillTo.Address2 = irSAPName.SAPPayers(loopCnt).BillTo.Address2.ToString()
                        Else
                            sap_payer_account.BillTo.Address2 = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Address3 Is Nothing Then
                            sap_payer_account.BillTo.Address3 = irSAPName.SAPPayers(loopCnt).BillTo.Address3.ToString()
                        Else
                            sap_payer_account.BillTo.Address3 = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Address4 Is Nothing Then
                            sap_payer_account.BillTo.Address4 = irSAPName.SAPPayers(loopCnt).BillTo.Address4.ToString()
                        Else
                            sap_payer_account.BillTo.Address4 = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.State Is Nothing Then
                            sap_payer_account.BillTo.State = irSAPName.SAPPayers(loopCnt).BillTo.State.ToString()
                        Else
                            sap_payer_account.BillTo.State = ""
                        End If

                        If Not irSAPName.SAPPayers(loopCnt).BillTo.Zip Is Nothing Then
                            sap_payer_account.BillTo.Zip = irSAPName.SAPPayers(loopCnt).BillTo.Zip.ToString()
                        Else
                            sap_payer_account.BillTo.Zip = ""
                        End If



                        'these two fields are not passed from Web service
                        'sap_payer_account.BillTo.Attention = irSAPName.SAPPayers(loopCnt).BillTo.Attention.ToString()
                        'sap_payer_account.BillTo.Phone = irSAPName.SAPPayers(loopCnt).BillTo.Phone.ToString()

                        sap_payer_account.BillTo.Attention = ""
                        sap_payer_account.BillTo.Phone = ""

                        accountArray.Add(sap_payer_account)
                    Next
                    Dim sap_payer_accounts(accountArray.Count - 1) As Sony.US.ServicesPLUS.Process.SAPPayer
                    accountArray.CopyTo(sap_payer_accounts)
                    Return sap_payer_accounts
                End If
            End If

        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "SAPNameInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Exit SAPNameInquiry (" + customer_name + ")", TraceLevel.Verbose)
        Return Nothing
        'Return irSAPName.SAPPayers()

    End Function

    Public Function LegacyAccountInquiry(ByRef account As SISAccount) As Object ' WebMethods.Services.LegacyAccount

        'Logger.RaiseTrace(ClassName, "Enter LegacyAccountInquiry (" + account.AccountNumber + ")", TraceLevel.Verbose)

        Dim intI As Integer = 0

        'path.action = "urn:bts-Action/Sony Electronics/SISServices/LegacyAccountInquiry"
        'services.SecurityValue = _web_x_credentials
        'services.pathValue = path

        Try
            irLegacyAccount = services.LegacyAccountInquiry(account.AccountNumber)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "LegacyAccountInquiry Exception: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        If irLegacyAccount.ErrorNumber <> 0 Then
            Throw New Exception(irLegacyAccount.ErrorNumber.ToString() + ":" + irLegacyAccount.ErrorMessage)
        End If

        'Logger.RaiseTrace(ClassName, "Exit LegacyAccountInquiry (" + account.AccountNumber + ")", TraceLevel.Verbose)

        Return irLegacyAccount.LegacyAccount
    End Function




End Class


