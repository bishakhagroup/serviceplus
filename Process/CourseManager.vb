Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities

Imports System.Text
Imports System.Xml
Imports System.Xml.Schema
Imports System.Collections.Specialized
'Imports System.Web.Mail    ' This namespace is obsolete. Replaced with System.Net.Mail
Imports System.Net.Mail

Public Class CourseManager
    Private Const ClassName As String = "CuourseManager"
    'Private Logger As New SonyLogger
    Protected Const NullDateTime = "01/01/0001"

    Public Sub AddInstructor(ByRef courseInstructor As CourseInstructor)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreInstructor(courseInstructor, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub DeleteCourseInstructor(ByRef courseInstructor As CourseInstructor)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreInstructor(courseInstructor, txn_context, CoreObject.ActionType.DELETE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseInstructor(ByRef courseInstructor As CourseInstructor)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreInstructor(courseInstructor, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddLocation(ByRef courseLocation As CourseLocation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreLocation(courseLocation, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseSchedule(ByRef courseSchedule As CourseSchedule)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreClass(courseSchedule, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub ActivateCourseSchedule(ByRef courseSchedule As CourseSchedule)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreClass(courseSchedule, txn_context, CoreObject.ActionType.RESTORE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    'Public Function IsTBDClassAvailable(ByVal CourseNumber As String, ByVal location As Int16) As Boolean
    '    Dim data_store As New CourseDataManager
    '    Return data_store.IsTBDClassAvailable(CourseNumber, location)
    'End Function

    Public Function IsTrainingCourse(ByVal CourseNumber As String) As Boolean
        Dim data_store As New CourseDataManager
        Return data_store.IsTrainingCourse(CourseNumber)
    End Function

    Public Sub DeleteCourseLocation(ByRef courseLocation As CourseLocation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreLocation(courseLocation, txn_context, CoreObject.ActionType.DELETE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseLocation(ByRef courseLocation As CourseLocation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreLocation(courseLocation, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseLocation(ByRef courseLocation As CourseLocation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreLocation(courseLocation, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub CancelCourseSchedule(ByRef courseSchedule As CourseSchedule)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreClass(courseSchedule, txn_context, CoreObject.ActionType.DELETE)
            data_store.CancelClassReservation(courseSchedule.PartNumber, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub CancelCourse(ByRef course As Course)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            Dim classes As CourseSchedule() = data_store.GetCourseClassesByCNumber(course.Number)
            txn_context = data_store.BeginTransaction
            data_store.StoreCourse(course, txn_context, CoreObject.ActionType.DELETE)
            'delete the course's classes
            For Each cs As CourseSchedule In classes
                data_store.StoreClass(cs, txn_context, CoreObject.ActionType.DELETE)
                data_store.CancelClassReservation(cs.PartNumber, txn_context)
            Next
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseSchedule(ByRef courseSchedule As CourseSchedule)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreClass(courseSchedule, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        If courseSchedule.Vacancy <= 0 Then
            'Need to check the availiability
            GetCourseVacancy(courseSchedule, False)
            If courseSchedule.Vacancy > 0 Then 'now available, send out e-mail
                SendAdminCourseAvailableEMail(courseSchedule, "smtp_server_cs")
            End If
        End If
    End Sub

    Public Sub UpdateCourse(ByRef course As Course)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreCourse(course, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub ActivateCourse(ByRef course As Course)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreCourse(course, txn_context, CoreObject.ActionType.RESTORE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    'Public Function UpdateParticipantsRerservation(ByVal PartNumber As String)

    '    'Logger.RaiseTrace(ClassName, "Enter UpdateCourse", TraceLevel.Verbose)

    '    Dim data_store As New CourseDataManager
    '    Dim txn_context As TransactionContext = Nothing

    '    Try
    '        txn_context = data_store.BeginTransaction
    '        data_store.UpdateParticipantsRerservation(PartNumber, txn_context)
    '        data_store.CommitTransaction(txn_context)
    '    Catch ex As Exception
    '        data_store.RollbackTransaction(txn_context)
    '        'Logger.RaiseTrace(ClassName, "UpdateCourse: " + ex.Message, TraceLevel.Error)
    '        Throw ex
    '    End Try

    '    'Logger.RaiseTrace(ClassName, "Exit UpdateCourse", TraceLevel.Verbose)

    'End Function

    'Public Function UpdateDefaultFlag(ByVal CourseNumber As String, ByVal LocID As Integer, ByVal IsDefaultFlag As Char)

    '    'Logger.RaiseTrace(ClassName, "Enter UpdateCourse", TraceLevel.Verbose)

    '    Dim data_store As New CourseDataManager
    '    Dim txn_context As TransactionContext = Nothing

    '    Try
    '        txn_context = data_store.BeginTransaction
    '        data_store.UpdateDefaultFlag(CourseNumber, LocID, IsDefaultFlag, txn_context)
    '        data_store.CommitTransaction(txn_context)
    '    Catch ex As Exception
    '        data_store.RollbackTransaction(txn_context)
    '        'Logger.RaiseTrace(ClassName, "UpdateCourse: " + ex.Message, TraceLevel.Error)
    '        Throw ex
    '    End Try

    '    'Logger.RaiseTrace(ClassName, "Exit UpdateCourse", TraceLevel.Verbose)

    'End Function

    Public Sub AddCourse(ByRef course As Course)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreCourse(course, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseMinorCategory(ByRef category As CourseMinorCategory)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreMinorCategory(category, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseModel(ByRef model As CourseModel)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreModel("", model, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseModel(ByRef OldModel As String, ByRef model As CourseModel)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreModel(OldModel, model, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub DeleteCourseModel(ByRef OldModel As String, ByVal model As CourseModel)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreModel(OldModel, model, txn_context, CoreObject.ActionType.DELETE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseMajorCategory(ByRef category As CourseMajorCategory)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreMajorCategory(category, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseMajorCategory(ByRef category As CourseMajorCategory)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreMajorCategory(category, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseType(ByRef type As CourseType)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreType(type, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCourseType(ByRef type As CourseType)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreType(type, txn_context, CoreObject.ActionType.UPDATE)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCourseMinorCategory(ByRef category As CourseMinorCategory)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreMinorCategory(category, txn_context, CoreObject.ActionType.ADD)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetReservation(ByRef reservationNumber As String) As CourseReservation
        Dim data_store As CourseDataManager = New CourseDataManager
        Dim cr As CourseReservation
        Dim cs As CourseSchedule = Nothing

        Try
            cr = data_store.GetCourseReservationByNumber(reservationNumber)
            data_store.GetCourseClassByNumber(cr.CourseSchedule.PartNumber, cs, cr.CourseSchedule.Course.Number)
            cr.Participants = data_store.GetCourseParticipant(reservationNumber, "*")
            cr.CourseSchedule = cs
            data_store.CloseConnection()
            Return cr
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Sub PromoConfigValidationHandler(ByVal sender As Object, ByVal args As ValidationEventArgs)
        Throw New Exception("Error search criteria format:" + args.Message)
    End Sub

    Public Function CourseSearch(ByRef scollection As NameValueCollection) As ArrayList
        Try
            If Not scollection("PartNumber") Is Nothing Then
                Dim strPartNumber = scollection("PartNumber").ToString
                Return CourseSearchList(strPartNumber)
            Else 'nned to search on other criteria
                Dim sc As New SearchCriteria
                sc.FillIn(scollection("MC"), scollection("M"), scollection("CN"), scollection("T"), scollection("CT"), scollection("ST"), scollection("L"), scollection("TY"), scollection("MN"))
                Return CourseSearchList(sc.majorCategory, sc.model, sc.courseNumber, sc.courseTitle, sc.city, sc.state, "", sc.month)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function GetCourseSearchCriteria(ByRef criteriaXMLFile As String, ByVal nIndex As Integer) As SearchCriteria
        Dim reader As XmlTextReader = Nothing
        Try
            reader = New XmlTextReader(criteriaXMLFile)
            Dim vr As New XmlValidatingReader(reader)
            vr.ValidationType = ValidationType.Schema
            AddHandler vr.ValidationEventHandler, AddressOf PromoConfigValidationHandler
            Dim sc As New SearchCriteria()
            Dim scList As ArrayList = New ArrayList

            Do While (reader.Read())
                If reader.NodeType = XmlNodeType.Element Then ' The node is an Element
                    Select Case reader.Name()
                        Case "ClassSearchCriteria"
                            sc = New SearchCriteria
                            scList.Add(sc)
                            reader.MoveToAttribute("PartNumber")
                            If (reader.ReadAttributeValue()) Then
                                sc.partNumber = reader.Value
                            End If
                        Case "ModelNumber"
                            sc.model = reader.ReadString
                        Case "CourseTitle"
                            sc.courseTitle = reader.ReadString
                        Case "MajorCategory"
                            sc.majorCategory = reader.ReadString
                        Case "City"
                            sc.city = reader.ReadString
                        Case "State"
                            sc.state = reader.ReadString
                        Case "CourseNumber"
                            sc.courseNumber = reader.ReadString
                        Case "Month"
                            Select Case reader.ReadString
                                Case "January"
                                    sc.month = 1
                                Case "February"
                                    sc.month = 2
                                Case "March"
                                    sc.month = 3
                                Case "April"
                                    sc.month = 4
                                Case "May"
                                    sc.month = 5
                                Case "June"
                                    sc.month = 6
                                Case "July"
                                    sc.month = 7
                                Case "August"
                                    sc.month = 8
                                Case "September"
                                    sc.month = 9
                                Case "October"
                                    sc.month = 10
                                Case "November"
                                    sc.month = 11
                                Case "December"
                                    sc.month = 12
                            End Select
                    End Select
                End If
            Loop
            Return CType(scList(nIndex), SearchCriteria)
        Catch ex As Exception
            Throw ex
        Finally
            If reader IsNot Nothing Then
                reader.Close()
            End If
        End Try
    End Function

    Public Function CourseSearch(ByRef majorCategory As String, ByRef model As String, ByRef number As String, ByRef title As String, ByRef city As String, ByRef state As String, ByRef type As String, ByRef month As Integer, Optional ByRef year As Integer = -1, Optional ByVal blnSIAM As Boolean = False, Optional ByVal ShowFutureClass As Boolean = False, Optional ByVal StatusCode As Short = -1, Optional ByVal Hide As Short = -1) As CourseSchedule()
        'Dim trainingclass_list As ArrayList
        'If year <> -1 Then
        '    trainingclass_list = CourseSearchListYear(majorCategory, model, number, title, city, state, type, month, year, blnSIAM, ShowFutureClass, StatusCode, Hide)
        'Else
        '    trainingclass_list = CourseSearchList(majorCategory, model, number, title, city, state, type, month, blnSIAM, ShowFutureClass, StatusCode, Hide)
        'End If
        'Dim css(trainingclass_list.Count - 1) As CourseSchedule
        'trainingclass_list.CopyTo(css)
        'Return css
        Dim trainingclass_list As ArrayList
        trainingclass_list = CourseSearchList(majorCategory, model, number, title, city, state, type, month, blnSIAM, ShowFutureClass, StatusCode, Hide)
        Dim css(trainingclass_list.Count - 1) As CourseSchedule
        trainingclass_list.CopyTo(css)

        Return css
    End Function

    Public Function CourseSearchTrainingClass(ByRef majorCategory As String, ByRef model As String, ByRef number As String, ByRef title As String, ByRef city As String, ByRef state As String, ByRef type As String, ByRef month As Integer, Optional ByVal year As Integer = -1, Optional ByVal blnSIAM As Boolean = False, Optional ByVal ShowFutureClass As Boolean = False, Optional ByVal StatusCode As Short = -1, Optional ByVal Hide As Short = -1, Optional ByVal ShowReservationOnly As Boolean = False) As CourseSchedule()
        Dim trainingclass_list As ArrayList
        trainingclass_list = CourseSearchListTrainingClass(majorCategory, model, number, title, city, state, type, month, year, blnSIAM, ShowFutureClass, StatusCode, Hide, ShowReservationOnly)
        Dim css(trainingclass_list.Count - 1) As CourseSchedule
        trainingclass_list.CopyTo(css)

        Return css
    End Function

    Public Function CourseSearchList(ByRef partNumber As String) As ArrayList
        Try
            Dim cdm As CourseDataManager = New CourseDataManager
            Return cdm.SearchTrainingClass(partNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CourseSearchListTrainingClass(ByRef majorCategory As String, ByRef model As String, ByRef number As String, ByRef title As String, ByRef city As String, ByRef state As String, ByRef type As String, ByRef month As Integer, ByVal searchyear As Integer, Optional ByVal blnSIAM As Boolean = False, Optional ByVal ShowFutureClass As Boolean = False, Optional ByVal StatusCode As Short = -1, Optional ByVal Hide As Short = -1, Optional ByVal ShowReservationOnly As Boolean = False) As ArrayList
        Try
            Dim cdm As New CourseDataManager
            Dim currentDay As Date = Date.Now
            Dim startDate As String = $"{currentDay.Month}/{currentDay.Day}/{currentDay.Year}"
            Dim endDate As String
            Dim year As Integer

            If searchyear = -1 Then
                If month = -1 Then 'no month specified, search from today to one year later
                    'According to bug 942 comment #13 we need to show all available class for All years
                    'Dim oneYearLater As DateTime = currentDay.AddMonths(12)
                    'startDate = currentDay.Month.ToString() + "/" + currentDay.Day.ToString() + "/" + currentDay.Year.ToString()
                    'endDate = oneYearLater.Month.ToString() + "/" + oneYearLater.Day.ToString() + "/" + oneYearLater.Year.ToString()
                    startDate = $"01/01/{(currentDay.Year - 3)}"
                    endDate = $"12/31/{(currentDay.Year + 1)}"
                Else 'only for that month
                    year = currentDay.Year
                    If month < currentDay.Month Then 'searched month is before current month, search the month in next year
                        year = year + 1
                    End If
                    Dim monthFirstDay As New DateTime(year, month, 1)
                    startDate = $"{monthFirstDay.Month}/{monthFirstDay.Day}/{monthFirstDay.Year}"
                    Dim monthLastDay As New DateTime(year, month, Date.DaysInMonth(year, month))
                    endDate = $"{monthLastDay.Month}/{monthLastDay.Day}/{monthLastDay.Year}"
                End If
            Else
                If month = -1 Then 'no month specified, search from today to one year later
                    startDate = $"01/01/{searchyear}"
                    endDate = $"12/31/{searchyear}"
                Else
                    year = searchyear
                    'If month < currentDay.Month Then 'searched month is before current month, search the month in next year
                    '    year = year + 1
                    'End If
                    Dim monthFirstDay As New DateTime(year, month, 1)
                    startDate = $"{monthFirstDay.Month}/{monthFirstDay.Day}/{monthFirstDay.Year}"
                    Dim monthLastDay As New DateTime(year, month, Date.DaysInMonth(year, month))
                    endDate = $"{monthLastDay.Month}/{monthLastDay.Day}/{monthLastDay.Year}"
                End If
            End If
            Return cdm.SearchTrainingClass(model, number, title, startDate, endDate, city, state, type, majorCategory, blnSIAM, ShowFutureClass, StatusCode, Hide, ShowReservationOnly)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CourseSearchList(ByRef majorCategory As String, ByRef model As String, ByRef number As String, ByRef title As String, ByRef city As String, ByRef state As String, ByRef type As String, ByRef month As Integer, Optional ByVal blnSIAM As Boolean = False, Optional ByVal ShowFutureClass As Boolean = False, Optional ByVal StatusCode As Short = -1, Optional ByVal Hide As Short = -1) As ArrayList
        Dim cdm As New CourseDataManager()
        Dim classList As ArrayList
        Dim currentDay As Date = Date.Now
        Dim startDate As String
        Dim endDate As String
        Dim year As Integer

        Try
            startDate = currentDay.ToString("MM/dd/yyyy") ' currentDay.Month.ToString() + "/" + currentDay.Day.ToString() + "/" + currentDay.Year.ToString()

            If month = -1 Then 'no month specified, search from today to one year later
                Dim oneYearLater As Date = currentDay.AddMonths(12)
                startDate = currentDay.ToString("MM/dd/yyyy") ' currentDay.Month.ToString() + "/" + currentDay.Day.ToString() + "/" + currentDay.Year.ToString()
                endDate = oneYearLater.ToString("MM/dd/yyyy") ' oneYearLater.Month.ToString() + "/" + oneYearLater.Day.ToString() + "/" + oneYearLater.Year.ToString()
            Else 'only for that month
                year = currentDay.Year
                If month < currentDay.Month Then 'searched month is before current month, search the month in next year
                    year += 1
                End If
                Dim monthFirstDay As New DateTime(year, month, 1)
                startDate = monthFirstDay.ToString("MM/dd/yyyy") ' .Month.ToString() + "/" + monthFirstDay.Day.ToString() + "/" + monthFirstDay.Year.ToString()
                Dim monthLastDay As New DateTime(year, month, Date.DaysInMonth(year, month))
                endDate = monthLastDay.ToString("MM/dd/yyyy") ' .Month.ToString() + "/" + monthLastDay.Day.ToString() + "/" + monthLastDay.Year.ToString()
            End If
            classList = cdm.SearchTrainingClass(model, number, title, startDate, endDate, city, state, type, majorCategory, blnSIAM, ShowFutureClass, StatusCode, Hide)

            Return classList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GetCourseVacancy(ByRef courseSchedule As CourseSchedule, ByVal bIncludingWait As Boolean)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.GetCourseVacancy(courseSchedule, bIncludingWait, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetClassCoursesAndLocation() As DataSet
        Dim data_store As New CourseDataManager
        Try
            Return data_store.GetClassCoursesAndLocation()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCertificateCourses() As DataSet
        Dim data_store As New CourseDataManager
        Try
            Return data_store.GetCertificateCourses()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SearchNonClassRoomTrainingForATC(ByVal CourseNumber As String) As Training
        Dim data_store As New CourseDataManager
        Try
            Return data_store.SearchNonClassRoomTrainingForATC(CourseNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCertificateCodeForCourse(ByVal CourseNumber As String) As DataSet
        Dim data_store As New CourseDataManager
        Try
            Return data_store.GetCertificateCodeForCourse(CourseNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetTempReservationAndClass(ByVal CourseNumber As String, ByVal Location As Short) As DataSet
        Dim data_store As New CourseDataManager
        Try
            Return data_store.GetTempReservationAndClass(CourseNumber, Location)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function SaveCertificateForCourse(ByVal CourseNumber As String, ByVal Certificate As String) As String
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim returnValue As String
        Try
            txn_context = data_store.BeginTransaction
            returnValue = data_store.SaveCertificateForCourse(CourseNumber, Certificate, txn_context)
            data_store.CommitTransaction(txn_context)
            Return returnValue
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Function

    Public Function UpdateActivationCodeForOrderLine(ByVal OrderNumber As String, ByVal OrderLineNumner As Short, ByVal ActivationCode As String) As String
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim returnValue As String
        Try
            txn_context = data_store.BeginTransaction
            returnValue = data_store.UpdateActivationCodeForOrderLine(OrderNumber, OrderLineNumner, ActivationCode, txn_context)
            data_store.CommitTransaction(txn_context)
            Return returnValue
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Function

    Public Function UpdateTempTrainingReservation(ByVal PartNumber As String, ByVal CourseNumber As String, ByVal Location As Short, ByVal ReservationNumber As String) As Integer
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim returnValue As Integer
        Try
            txn_context = data_store.BeginTransaction
            returnValue = data_store.UpdateTempTrainingReservation(PartNumber, CourseNumber, Location, ReservationNumber, txn_context)
            data_store.CommitTransaction(txn_context)
            Return returnValue
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Function

    Public Function DeleteTempTrainingReservation(ByVal CourseNumber As String, ByVal Location As Short, ByVal ReservationNumber As String) As Integer
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim returnValue As Integer
        Try
            txn_context = data_store.BeginTransaction
            returnValue = data_store.DeleteTempTrainingReservation(CourseNumber, Location, ReservationNumber, txn_context)
            data_store.CommitTransaction(txn_context)
            Return returnValue
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Function

    ' added by Deepa V 13 Feb,2006
    ' fetch course type
    Public Sub GetCourseType(ByRef courseType As CourseType)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.GetCourseType(courseType, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub SendCourseReminderEMail(ByRef smtpServer As String)
        Dim data_store As New CourseDataManager
        Dim currentDate As Date = Date.Now
        Dim reservation_list As Hashtable = data_store.GetPendingParticipants(currentDate.ToString("MM/dd/yyyy"))
        Dim cm As CustomerManager = New CustomerManager
        Dim customer As Customer
        For Each cr As CourseReservation In reservation_list.Values
            '      If ((cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") <> NullDateTime) And (currentDate.AddDays(cr.CourseSchedule.ReminderDaysBefore) = cr.CourseSchedule.StartDate)) Then
            If ((cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") <> NullDateTime)) Then
                cr.Participants = data_store.GetCourseParticipant(cr.ReservationNumber, "R")
                For Each cp As CourseParticipant In cr.Participants
                    SendCourseParticipantReminderEMail(cr, cp, smtpServer)
                Next
                If cr.Participants.Length > 0 Then
                    If Not cr.CustomerSIAMID Is Nothing Then
                        customer = cm.GetCustomer(cr.CustomerSIAMID)
                    Else
                        customer = cm.GetCustomer(cr.AdminSIAMID)
                    End If
                    SendCourseReserverReminderEMail(cr, customer, smtpServer)
                End If
            End If
        Next
    End Sub

    Private Sub SendCourseParticipantReminderEMail(ByRef cr As CourseReservation, ByRef participant As CourseParticipant, ByRef smtpServer As String)
        Dim EmailBody As New StringBuilder(1500)
        Dim mailObj As MailMessage
        'Dim emailBodyString As String

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link

        EmailBody.Append($"Dear {participant.FirstName} {participant.LastName},{vbCrLf}{vbCrLf}")
        EmailBody.Append($"Your registered class {cr.CourseSchedule.Course.Title} ({cr.CourseSchedule.Course.Number}) will start from {cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy")} to {cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy")}, {cr.CourseSchedule.Time} at following address{vbCrLf}")
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append(cr.CourseSchedule.Location.City + "," + cr.CourseSchedule.Location.State + cr.CourseSchedule.Location.Zip + vbCrLf)
        EmailBody.Append(cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf)
        EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("Your reservation number is:" + cr.ReservationNumber + vbCrLf)

        EmailBody.Append($"Copyright {Date.Now.Year} Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, participant.EMail) With {
            .Subject = $"ServicesPLUS(sm) Training Course Reminder ({cr.CourseSchedule.Course.Number})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Private Sub SendCourseReserverReminderEMail(ByRef cr As CourseReservation, ByRef customer As Customer, ByRef smtpServer As String)
        Dim EmailBody As New StringBuilder(1500)

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link

        Dim mailObj As New MailMessage(ConfigurationData.Environment.Email.General.From, customer.EmailAddress) With {
            .Subject = $"ServicesPLUS(sm) Training Course Reminder ({cr.CourseSchedule.Course.Number})",
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        EmailBody.Append("Dear " + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
        EmailBody.Append($"The class {cr.CourseSchedule.Course.Title}({cr.CourseSchedule.Course.Number}) registered by you will start from {cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy")} to {cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy")}, {cr.CourseSchedule.Time} at following address{vbCrLf}")
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append($"{cr.CourseSchedule.Location.City}, {cr.CourseSchedule.Location.State} {cr.CourseSchedule.Location.Zip}{vbCrLf}")
        EmailBody.Append(cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf)
        EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("Your reservation number is:" + cr.ReservationNumber + vbCrLf)

        EmailBody.Append("Copyright " + Date.Now.Year.ToString() + " Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj.Body = EmailBody.ToString()
        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port

            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendCourseReservationEMail(ByRef order As Order, ByRef cr As CourseReservation, ByRef customer As Customer, ByRef smtpServer As String)
        Dim data_store As New CourseDataManager
        Dim currentDate As Date = Date.Now
        Dim reservation_list As Hashtable = data_store.GetPendingParticipants(currentDate.ToString("MM/dd/yyyy"))
        Dim cm As CustomerManager = New CustomerManager
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)

        Dim mailToCC As String = ""
        For Each cp As CourseParticipant In cr.Participants
            If (cp.EMail <> customer.EmailAddress) Then ' Customer is the main recipient, don't need to CC as well.
                If Not String.IsNullOrWhiteSpace(mailToCC) Then
                    mailToCC = mailToCC + ";"
                End If
                mailToCC = mailToCC + cp.EMail
            End If
        Next

        Dim training_link As String = ConfigurationData.GeneralSettings.URL.Training_Link
        Dim adminEMail = ConfigurationData.Environment.Email.Training.Training_Admin


        EmailBody.Append("Dear " + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
        Dim emailBodyString As String = "Thank for your interest in the Sony Training Institute. You have registered " + vbCrLf + "for the following course:" + vbCrLf + vbCrLf
        EmailBody.Append(emailBodyString)

        emailBodyString = "Class No.:" + vbTab + "  " + cr.CourseSchedule.PartNumber + vbCrLf
        EmailBody.Append(emailBodyString)
        emailBodyString = "Course Title:" + " " + cr.CourseSchedule.Course.Title + vbCrLf
        EmailBody.Append(emailBodyString)
        emailBodyString = "Start Date:" + "   "
        Dim noDateSetYet As Boolean = False
        If (cr.CourseSchedule.StartDate <> NullDateTime) Then
            emailBodyString = emailBodyString + cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") + vbCrLf
        Else
            noDateSetYet = True
            emailBodyString = emailBodyString + "Not Specified" + vbCrLf
        End If
        EmailBody.Append(emailBodyString)
        emailBodyString = "End Date:" + vbTab + "  "
        If (cr.CourseSchedule.EndDate <> NullDateTime) Then
            emailBodyString = emailBodyString + cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") + vbCrLf
        Else
            noDateSetYet = True
            emailBodyString = emailBodyString + "Not Specified" + vbCrLf
        End If
        EmailBody.Append(emailBodyString)
        emailBodyString = "Location:" + vbTab + "  "
        EmailBody.Append(emailBodyString)
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + vbTab + vbTab + "  " + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(vbTab + vbTab + "  " + cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append(vbTab + vbTab + "  " + cr.CourseSchedule.Location.City + ", " + cr.CourseSchedule.Location.State + " " + cr.CourseSchedule.Location.Zip + vbCrLf)
        EmailBody.Append(vbTab + vbTab + "  " + cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf + vbCrLf)
        'EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("Order No.:" + vbTab + "  " + cr.ReservationNumber + vbCrLf)
        EmailBody.Append("P.O. Number:" + "  " + order.POReference + vbCrLf)
        EmailBody.Append("Date:" + vbTab + vbTab + "  " + Date.Now.ToString("MM/dd/yyyy") + vbCrLf + vbCrLf)
        'If noDateSetYet Then
        'EmailBody.Append("The time is not set yet. You will be notified by email and/or phone when the time is set." + vbCrLf)
        'End If
        EmailBody.Append("Price per student: " + String.Format("{0:c}", cr.CourseSchedule.Course.ListPrice) + vbCrLf + vbCrLf)
        EmailBody.Append("Student: ")
        emailBodyString = ""
        For Each cp As CourseParticipant In cr.Participants
            If Not String.IsNullOrWhiteSpace(emailBodyString) Then
                emailBodyString = emailBodyString + "         "
            End If
            emailBodyString = emailBodyString + cp.FirstName + " " + cp.LastName + vbCrLf
        Next
        EmailBody.Append(emailBodyString + vbCrLf)
        EmailBody.Append("We are currently processing your registration and will send you an email" + vbCrLf)
        EmailBody.Append("confirmation. If you do not receive your confirmation within one business day, please ") '+ vbCrLf)
        EmailBody.Append("contact " + adminEMail + vbCrLf + vbCrLf)
        EmailBody.Append("We thank you for your business." + vbCrLf + vbCrLf)
        EmailBody.Append("Sincerely," + vbCrLf)
        EmailBody.Append("Sony Training Institute" + vbCrLf)
        EmailBody.Append(training_link + vbCrLf)

        ServicesPlusException.Utilities.LogDebug($"CourseManager.SendCourseReservationEMail - Mail parameters: To: {customer?.EmailAddress}, From: {adminEMail}, Reservation #: {cr.ReservationNumber}")
        mailObj = New MailMessage(adminEMail, customer.EmailAddress) With {
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal,
            .Subject = "Your Sony ServicesPLUS Training Registration, Order Number " + cr.ReservationNumber
        }
        'mailObj.To.Add(customer.EmailAddress)
        mailObj.CC.Add(mailToCC)
        mailObj.Bcc.Add(ConfigurationData.Environment.Email.Training.Training_Admin)    ' Training@am.sony.com

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port

            mailClient.Send(mailObj)
        End Using
    End Sub

    'Email for non class room training
    ' added by Deepa V feb 14,2006
    Public Sub SendNonClassroomReservationEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef customer As Customer, ByRef smtpServer As String)
        Dim data_store As New CourseDataManager
        Dim currentDate As Date = Date.Now
        Dim reservation_list As Hashtable = data_store.GetPendingParticipants(currentDate.ToString("MM/dd/yyyy"))
        Dim cm As CustomerManager = New CustomerManager
        Dim mailObj As MailMessage
        Dim mailToCC As String = ""
        Dim EmailBody As New StringBuilder(1500)
        Dim training_link As String = ConfigurationData.GeneralSettings.URL.Training_Link
        Dim adminEMail = ConfigurationData.Environment.Email.Training.NonClassRoomTraining_Admin
        'Dim emailBodyString As String

        For Each cp As CourseParticipant In cr.Participants
            If (cp.EMail <> customer.EmailAddress) Then 'don't send repeating email
                If Not String.IsNullOrWhiteSpace(mailToCC) Then
                    mailToCC = mailToCC + ";"
                End If
                mailToCC = mailToCC + cp.EMail
            End If
        Next

        EmailBody.Append($"Dear {customer.FirstName} {customer.LastName}," + vbCrLf + vbCrLf)
        EmailBody.Append("Thank for your order from the Sony Training Institute. You have ordered the following training item:" + vbCrLf)
        EmailBody.Append("Part No.:" + vbTab + "  " + cr.CourseSchedule.PartNumber + vbCrLf)
        EmailBody.Append("Course Title:" + " " + cr.CourseSchedule.Course.Title + vbCrLf)
        EmailBody.Append("Order No.:" + vbTab + "  " + cr.ReservationNumber + vbCrLf)
        EmailBody.Append("P.O. Number:" + "  " + order.POReference + vbCrLf)
        EmailBody.Append("Date:" + vbTab + vbTab + "  " + Date.Now.ToString("MM/dd/yyyy") + vbCrLf + vbCrLf)
        EmailBody.Append("Price: " + String.Format("{0:c}", cr.CourseSchedule.Course.ListPrice) + " + applicable tax" + vbCrLf + vbCrLf)

        EmailBody.Append("We are currently processing your training order and will send you an email" + vbCrLf)
        EmailBody.Append("confirmation. If you do not receive your confirmation within 24 hours, please ")
        EmailBody.Append("contact " + adminEMail + vbCrLf + vbCrLf)
        EmailBody.Append("We thank you for your business." + vbCrLf + vbCrLf)
        EmailBody.Append("Sincerely," + vbCrLf)
        EmailBody.Append("Sony Training Institute" + vbCrLf)
        EmailBody.Append(training_link + vbCrLf)

        mailObj = New MailMessage(adminEMail, customer.EmailAddress) With {
            .Subject = "Your Sony ServicesPLUS Training Order, Order Number " + cr.ReservationNumber,
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }
        mailObj.Bcc.Add(ConfigurationData.Environment.Email.Training.Training_Admin)

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Private Sub SendCourseParticipantReservationEMail(ByRef cr As CourseReservation, ByRef participant As CourseParticipant, ByRef customer As Customer, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)
        Dim emailBodyString As String
        Dim noDateSetYet As Boolean = False

        EmailBody.Append($"Dear {participant.FirstName} {participant.LastName}," + vbCrLf + vbCrLf)
        If (cr.CourseSchedule.StartDate <> NullDateTime) Then
            emailBodyString = $"{customer.Name} has registered class {cr.CourseSchedule.Course.Title}({cr.CourseSchedule.Course.Number}) for you.
                It will start from {cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy")} to {cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy")}
                , {cr.CourseSchedule.Time} at following address:" & vbCrLf
        Else
            noDateSetYet = True
            emailBodyString = $"{customer.Name} has registered class {cr.CourseSchedule.Course.Title}({cr.CourseSchedule.Course.Number}) for you.
                It will be given at following address:" + vbCrLf
        End If

        EmailBody.Append(emailBodyString)
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append(cr.CourseSchedule.Location.City + "," + cr.CourseSchedule.Location.State + cr.CourseSchedule.Location.Zip + vbCrLf)
        EmailBody.Append(cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf)
        EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("Your reservation number Is:" + cr.ReservationNumber + vbCrLf)
        EmailBody.Append("Your reservation status Is:" + participant.StatusDisplay + vbCrLf)
        If noDateSetYet Then
            EmailBody.Append("The time Is Not set yet. You will be notified by email And/Or phone when the time Is set." + vbCrLf)
        End If

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link

        EmailBody.Append(vbCrLf + vbCrLf + "Copyright " + Date.Now.Year.ToString() + " Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms And Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, participant.EMail) With {
            .Subject = $"ServicesPLUS(sm) Training Course Reservation ({cr.CourseSchedule.Course.Number})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port

            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendCourseReserverInvoiceEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef customer As Customer, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(2500)
        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim emailBodyString As String
        Dim noDateSetYet As Boolean = False
        Dim mailToCC As String = ""
        Dim nIndex As Integer = 0

        For Each cp As CourseParticipant In cr.Participants
            If (cp.EMail <> customer.EmailAddress) Then 'don't send repeating email
                If Not String.IsNullOrWhiteSpace(mailToCC) Then
                    mailToCC = mailToCC + ";"
                End If
                mailToCC = mailToCC + cp.EMail
            End If
        Next

        emailBodyString = "Here below Is the invoice for training you requested on the ServicesPLUS web site" + vbCrLf + " at " + servicesplus_link + "." + vbCrLf + vbCrLf + "INVOICE" + vbCrLf + vbCrLf
        EmailBody.Append(emailBodyString)
        'EmailBody.Append("Sony Training Institute" + vbCrLf + "3300 Zanker Road, SJ 2A6" + vbCrLf + "San Jose, CA 95134-1901" + vbCrLf + "Phone 408-955-5000" + vbCrLf + "Fax 408-955-5340" + vbCrLf + "Training@am.sony.com" + vbCrLf + vbCrLf) 'Modified for fixing Bug# 443
        EmailBody.Append("Sony Training Institute" + vbCrLf + "1730 N. First Street, MS 1SE" + vbCrLf + "San Jose, CA 95112-4508" + vbCrLf + "Phone 408-352-4500" + vbCrLf + "Fax 408-352-4161" + vbCrLf + "Training@am.sony.com" + vbCrLf + vbCrLf)
        EmailBody.Append("ServicesPLUS Training Order No.: " + cr.ReservationNumber + vbCrLf)
        EmailBody.Append("Date: " + Date.Now.ToString("MM/dd/yyyy") + vbCrLf + vbCrLf)
        EmailBody.Append("Bill to: " + vbCrLf + order.BillTo.Name + vbCrLf + order.BillTo.Line1 + vbCrLf)
        If ((Not order.BillTo.Line2 Is Nothing) And (order.BillTo.Line2 <> "")) Then
            EmailBody.Append(order.BillTo.Line2 + vbCrLf)
        End If
        EmailBody.Append(order.BillTo.City + vbCrLf + order.BillTo.State + vbCrLf + order.BillTo.PostalCode + vbCrLf + vbCrLf)
        EmailBody.Append("P.O. Number:   " + order.POReference + vbCrLf)
        EmailBody.Append("Class No.:     " + cr.CourseSchedule.Course.Number + vbCrLf)
        EmailBody.Append("Course Title:  " + cr.CourseSchedule.Course.Title + vbCrLf)
        EmailBody.Append("Start Date:    " + cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") + vbCrLf)
        EmailBody.Append("End Date:      " + cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") + vbCrLf)
        EmailBody.Append("Training Time: " + cr.CourseSchedule.Time.ToString() + vbCrLf)
        EmailBody.Append("Location:      " + cr.CourseSchedule.Location.Name + vbCrLf + vbCrLf)
        EmailBody.Append("Price per student: " + String.Format("{0:c}", cr.CourseSchedule.Course.ListPrice) + vbCrLf + vbCrLf)
        EmailBody.Append("Student: ")
        For Each cp As CourseParticipant In cr.Participants
            If (nIndex > 0) Then
                EmailBody.Append(vbCrLf + cp.FirstName + " " + cp.LastName)
            Else
                EmailBody.Append(cp.FirstName + " " + cp.LastName)
            End If
            nIndex = nIndex + 1
        Next
        EmailBody.Append(vbCrLf + vbCrLf + "We accept payment by check, by credit card, or on Sony account. Payment is " + vbCrLf)
        EmailBody.Append("due upon receipt of this invoice, and must be received before the first day" + vbCrLf)
        EmailBody.Append("of class." + vbCrLf + vbCrLf)
        'EmailBody.Append("For payment by credit card or on Sony account, please call 408-955-5000." + vbCrLf + vbCrLf) 'Modified for fixing Bug# 443
        EmailBody.Append("For payment by credit card or on Sony account, please call 408-352-4500." + vbCrLf + vbCrLf)
        EmailBody.Append("For payment by check, please send:" + vbCrLf)
        EmailBody.Append("1) A printed copy of this invoice" + vbCrLf)
        EmailBody.Append("2) A check made payable to Sony Training Institute." + vbCrLf + vbCrLf)
        EmailBody.Append("Send the check and the invoice copy to:" + vbCrLf)
        'EmailBody.Append("Sony Training Institute" + vbCrLf + "3300 Zanker Road, SJ 2A6" + vbCrLf + "San Jose, CA 95134-1901" + vbCrLf + vbCrLf) 'Modified for fixing Bug# 443
        EmailBody.Append("Sony Training Institute" + vbCrLf + "1730 N. First Street, MS 1SE" + vbCrLf + "San Jose, CA 95112-4508" + vbCrLf + vbCrLf)
        EmailBody.Append("Terms and Conditions:" + vbCrLf + vbCrLf)
        EmailBody.Append("HOTEL/GROUND TRANSPORTATION" + vbCrLf)
        EmailBody.Append("Students are responsible for their own travel and hotel arrangements.  With " + vbCrLf)
        EmailBody.Append("your registration confirmation you�ll receive a lodging list. It is strongly " + vbCrLf)
        EmailBody.Append("recommended that hotel reservations be made at least two weeks prior to your " + vbCrLf)
        EmailBody.Append("training class.  Please note that lodging and meals are not included in the " + vbCrLf)
        EmailBody.Append("class registration fee." + vbCrLf + vbCrLf)
        EmailBody.Append("TRANSFERS/CANCELLATION POLICY" + vbCrLf + vbCrLf)
        EmailBody.Append("If it is necessary to cancel your attendance at a course for which you are " + vbCrLf)
        EmailBody.Append("registered, you must cancel your enrollment, in writing, at least three (3) " + vbCrLf)
        EmailBody.Append("weeks prior to the start of the course.  This will entitle you to a full " + vbCrLf)
        EmailBody.Append("reimbursement of the tuition fee.  A 25% cancellation fee will be charged for " + vbCrLf)
        EmailBody.Append("registration cancelled within three (3) weeks of the course, or you may " + vbCrLf)
        EmailBody.Append("transfer the tuition to another course offered within six (6) months and " + vbCrLf)
        EmailBody.Append("there will be no penalty.  This transfer may be made only once to avoid the " + vbCrLf)
        EmailBody.Append("25% cancellation fee.  ""No Shows"" or cancellations received on or after the " + vbCrLf)
        EmailBody.Append("first day of class are billed at 50% of the course�s published fee." + vbCrLf + vbCrLf)
        EmailBody.Append("Dates and locations are subject to change without notice. Please contact us " + vbCrLf)
        EmailBody.Append("to confirm scheduling before making final travel arrangements.  Non-" + vbCrLf)
        EmailBody.Append("refundable airfares are not recommended." + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, customer.EmailAddress) With {
            .Subject = $"Your requested invoice for Sony ServicesPLUS Training Order({cr.ReservationNumber})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }
        mailObj.CC.Add(mailToCC)
        mailObj.Bcc.Add(ConfigurationData.Environment.Email.Training.Training_Admin)

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port

            mailClient.Send(mailObj)
        End Using
    End Sub

    ' modification of email for non classroom training
    ' Added by Deepa V feb 14,2006 
    Public Sub SendNonClassroomBudgetCodeEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef customer As Customer, ByRef smtpServer As String, ByVal budgetCode As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(2500)
        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim emailBodyString As String
        Dim noDateSetYet As Boolean = False

        emailBodyString = "Below is the order placed with budget code entered manually" + vbCrLf + " at " + servicesplus_link + "." + vbCrLf + vbCrLf + "INVOICE" + vbCrLf + vbCrLf
        EmailBody.Append(emailBodyString)
        EmailBody.Append("ServicesPLUS Training Order No.: " + order.OrderNumber + vbCrLf)
        EmailBody.Append("Date: " + Date.Now.ToString("MM/dd/yyyy") + vbCrLf + vbCrLf)

        EmailBody.Append("Budget Code: " + budgetCode + vbCrLf + vbCrLf)

        EmailBody.Append("Requested by: " + vbCrLf + order.Customer.FirstName + vbCrLf + order.Customer.LastName + vbCrLf)

        EmailBody.Append("Bill to: " + vbCrLf + order.BillTo.Name + vbCrLf + order.BillTo.Line1 + vbCrLf)
        If ((Not order.BillTo.Line2 Is Nothing) And (order.BillTo.Line2 <> "")) Then
            EmailBody.Append(order.BillTo.Line2 + vbCrLf)
        End If
        EmailBody.Append(order.BillTo.City + vbCrLf + order.BillTo.State + vbCrLf + order.BillTo.PostalCode + vbCrLf + vbCrLf)
        EmailBody.Append("P.O. Number:  " + order.POReference + vbCrLf)
        EmailBody.Append("Class No.:    " + cr.CourseSchedule.Course.Number + vbCrLf)
        EmailBody.Append("Course Title: " + cr.CourseSchedule.Course.Title + vbCrLf)
        EmailBody.Append("Price:" + String.Format("{0:c}", cr.CourseSchedule.Course.ListPrice) + vbCrLf + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.NonClassRoomTraining_Admin, ConfigurationData.Environment.Email.Training.NonClassRoomTraining_Admin) With {
            .Subject = $"Sony ServicesPLUS Training Order ({order.OrderNumber}) requested with budget code",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port

            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendCourseReserverQuoteEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef customer As Customer, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(2500)
        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim emailBodyString As String
        Dim noDateSetYet As Boolean = False
        Dim mailToCC As String = ""
        Dim nIndex As Integer = 0

        For Each cp As CourseParticipant In cr.Participants
            If (cp.EMail <> customer.EmailAddress) Then 'don't send repeating email
                If Not String.IsNullOrWhiteSpace(mailToCC) Then
                    mailToCC = mailToCC + ";"
                End If
                mailToCC = mailToCC + cp.EMail
            End If
        Next

        emailBodyString = "Here below is the quote for training you requested on the ServicesPLUS web site" + vbCrLf + " at " + servicesplus_link + "." + vbCrLf + vbCrLf + "QUOTE" + vbCrLf + vbCrLf
        EmailBody.Append(emailBodyString)
        'EmailBody.Append("Sony Training Institute" + vbCrLf + "3300 Zanker Road, SJ 2A6" + vbCrLf + "San Jose, CA 95134-1901" + vbCrLf + "Phone 408-955-5000" + vbCrLf + "Fax 408-955-5340" + vbCrLf + "Training@am.sony.com" + vbCrLf + vbCrLf) 'Modified for fixing Bug# 430
        EmailBody.Append("Sony Training Institute" + vbCrLf + "1730 N. First Street, MS 1SE" + vbCrLf + "San Jose, CA 95112-4508" + vbCrLf + "Phone 408-352-4500" + vbCrLf + "Fax 408-352-4161" + vbCrLf + "Training@am.sony.com" + vbCrLf + vbCrLf)
        EmailBody.Append("ServicesPLUS Training Quote No.: " + cr.ReservationNumber + vbCrLf)
        EmailBody.Append("Date: " + Date.Now.ToString("MM/dd/yyyy") + vbCrLf + vbCrLf)
        EmailBody.Append("Bill to: " + vbCrLf + order.BillTo.Name + vbCrLf + order.BillTo.Line1 + vbCrLf)
        If ((Not order.BillTo.Line2 Is Nothing) And (order.BillTo.Line2 <> "")) Then
            EmailBody.Append(order.BillTo.Line2 + vbCrLf)
        End If
        If ((Not order.BillTo.Line3 Is Nothing) And (order.BillTo.Line3 <> "")) Then
            EmailBody.Append(order.BillTo.Line3 + vbCrLf)
        End If
        EmailBody.Append(order.BillTo.City + vbCrLf + order.BillTo.State + vbCrLf + order.BillTo.PostalCode + vbCrLf + vbCrLf)
        EmailBody.Append("Class No.:     " + cr.CourseSchedule.Course.Number + vbCrLf)
        EmailBody.Append("Course Title:  " + cr.CourseSchedule.Course.Title + vbCrLf)

        'modified to send start and end date as TBD if no valid date is set to the course
        ' modified by Deepa V, Mar 22,2006
        If cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") <> NullDateTime Then
            EmailBody.Append("Start Date:    " + cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") + vbCrLf)
            EmailBody.Append("Training Time: " + cr.CourseSchedule.Time.ToString() + vbCrLf)
        Else
            EmailBody.Append("Start Date:    " + "TBD" + vbCrLf)
            EmailBody.Append("Training Time: TBD  " + vbCrLf)
        End If

        If cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") <> NullDateTime Then
            EmailBody.Append("End Date:      " + cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") + vbCrLf)
        Else
            EmailBody.Append("End Date:      " + "TBD" + vbCrLf)
        End If

        EmailBody.Append("Location:     " + cr.CourseSchedule.Location.Name + vbCrLf + vbCrLf)
        EmailBody.Append("Price per student: " + String.Format("{0:c}", cr.CourseSchedule.Course.ListPrice) + vbCrLf + vbCrLf)
        EmailBody.Append("Student: ")
        For Each cp As CourseParticipant In cr.Participants
            If (nIndex > 0) Then
                EmailBody.Append(vbCrLf + cp.FirstName + " " + cp.LastName)
            Else
                EmailBody.Append(cp.FirstName + " " + cp.LastName)
            End If
            nIndex = nIndex + 1
        Next
        EmailBody.Append(vbCrLf + vbCrLf + "We accept payment by check, by credit card, or on Sony account. Payment " + vbCrLf)
        'commented by Deepa V on Mar 22,2006 because the quote mail should not have this sentence
        'EmailBody.Append("is due upon receipt of this invoice, and ")
        EmailBody.Append("must be received before the first day ")
        EmailBody.Append("of class." + vbCrLf + vbCrLf)
        'EmailBody.Append("For payment by credit card or on Sony account, please call 408-955-5000." + vbCrLf + vbCrLf) 'Modified for fixing Bug# 442
        EmailBody.Append("For payment by credit card or on Sony account, please call 408-352-4500." + vbCrLf + vbCrLf)
        EmailBody.Append("For payment by check, please send:" + vbCrLf)
        EmailBody.Append("1) A printed copy of this quote" + vbCrLf)
        EmailBody.Append("2) A check made payable to Sony Training Institute." + vbCrLf + vbCrLf)
        EmailBody.Append("Send the check and the quote copy to:" + vbCrLf)
        'EmailBody.Append("Sony Training Institute" + vbCrLf + "3300 Zanker Road, SJ 2A6" + vbCrLf + "San Jose, CA 95134-1901" + vbCrLf + vbCrLf) 'Modified for fixing Bug# 430
        EmailBody.Append("Sony Training Institute" + vbCrLf + "1730 N. First Street, MS 1SE" + vbCrLf + "San Jose, CA 95112-4508" + vbCrLf + vbCrLf)
        EmailBody.Append("Terms and Conditions:" + vbCrLf + vbCrLf)
        EmailBody.Append("HOTEL/GROUND TRANSPORTATION" + vbCrLf)
        EmailBody.Append("Students are responsible for their own travel and hotel arrangements.  With " + vbCrLf)
        EmailBody.Append("your registration confirmation you�ll receive a lodging list. It is strongly " + vbCrLf)
        EmailBody.Append("recommended that hotel reservations be made at least two weeks prior to your " + vbCrLf)
        EmailBody.Append("training class.  Please note that lodging and meals are not included in the " + vbCrLf)
        EmailBody.Append("class registration fee." + vbCrLf + vbCrLf)
        EmailBody.Append("TRANSFERS/CANCELLATION POLICY" + vbCrLf + vbCrLf)
        EmailBody.Append("If it is necessary to cancel your attendance at a course for which you are " + vbCrLf)
        EmailBody.Append("registered, you must cancel your enrollment, in writing, at least three (3) " + vbCrLf)
        EmailBody.Append("weeks prior to the start of the course.  This will entitle you to a full " + vbCrLf)
        EmailBody.Append("reimbursement of the tuition fee.  A 25% cancellation fee will be charged for " + vbCrLf)
        EmailBody.Append("registration cancelled within three (3) weeks of the course, or you may " + vbCrLf)
        EmailBody.Append("transfer the tuition to another course offered within six (6) months and " + vbCrLf)
        EmailBody.Append("there will be no penalty.  This transfer may be made only once to avoid the " + vbCrLf)
        EmailBody.Append("25% cancellation fee.  ""No Shows"" or cancellations received on or after the " + vbCrLf)
        EmailBody.Append("first day of class are billed at 50% of the course�s published fee." + vbCrLf + vbCrLf)
        EmailBody.Append("Dates and locations are subject to change without notice. Please contact us " + vbCrLf)
        EmailBody.Append("to confirm scheduling before making final travel arrangements.  Non-" + vbCrLf)
        EmailBody.Append("refundable airfares are not recommended." + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, customer.EmailAddress) With {
            .Subject = $"Your requested quote for Sony ServicesPLUS Training Order ({cr.ReservationNumber})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }
        mailObj.CC.Add(mailToCC)
        mailObj.Bcc.Add(ConfigurationData.Environment.Email.Training.Training_Admin)

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Private Sub SendCourseReserverReservationEMail(ByRef cr As CourseReservation, ByRef customer As Customer, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)
        Dim emailBodyString As String
        Dim noDateSetYet As Boolean = False

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link

        EmailBody.Append("Dear " + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
        If (cr.CourseSchedule.StartDate <> NullDateTime) Then
            emailBodyString = "You have registered the class " + cr.CourseSchedule.Course.Title + "(" + cr.CourseSchedule.Course.Number + ")." + "It will start from " + cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") _
         + " to " + cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") + " ," + cr.CourseSchedule.Time + " at following address:" + vbCrLf
        Else
            noDateSetYet = True
            emailBodyString = "You have registered the class " + cr.CourseSchedule.Course.Title + "(" + cr.CourseSchedule.Course.Number + ") to be held at following address:" + vbCrLf
        End If

        EmailBody.Append(emailBodyString)
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append(cr.CourseSchedule.Location.City + "," + cr.CourseSchedule.Location.State + cr.CourseSchedule.Location.Zip + vbCrLf)
        EmailBody.Append(cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf)
        EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("Your reservation number is:" + cr.ReservationNumber + vbCrLf)
        If noDateSetYet Then
            EmailBody.Append("The time is not set yet. You will be notified by email and/or phone when the time is set." + vbCrLf)
        End If

        EmailBody.Append(vbCrLf + vbCrLf + "Copyright " + Date.Now.Year.ToString() + " Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, customer.EmailAddress) With {
            .Subject = $"ServicesPLUS(sm) Training Course Reservation ({cr.CourseSchedule.Course.Number})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendAdminCourseReservationEMail(ByRef cr As CourseReservation, ByRef customer As Customer, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link
        Dim trainingadmin_link As String = ConfigurationData.Environment.URL.Training_AdminLink

        Dim emailBodyString As String = customer.Name + " has registered class " + cr.CourseSchedule.Course.Title + "(" + cr.CourseSchedule.Course.Number + ")." + vbCrLf
        EmailBody.Append(emailBodyString)
        emailBodyString = "From " + cr.CourseSchedule.StartDate.ToString("MM/dd/yyyy") + " to " + cr.CourseSchedule.EndDate.ToString("MM/dd/yyyy") + " ," + cr.CourseSchedule.Time + " at following address" + vbCrLf
        EmailBody.Append(emailBodyString)
        EmailBody.Append(cr.CourseSchedule.Location.Name + vbCrLf + cr.CourseSchedule.Location.Address1 + vbCrLf)
        If Not String.IsNullOrWhiteSpace(cr.CourseSchedule.Location.Address2) Then
            EmailBody.Append(cr.CourseSchedule.Location.Address2 + vbCrLf)
        End If
        EmailBody.Append(cr.CourseSchedule.Location.City + "," + cr.CourseSchedule.Location.State + cr.CourseSchedule.Location.Zip + vbCrLf)
        EmailBody.Append(cr.CourseSchedule.Location.Country.TerritoryName + vbCrLf)
        EmailBody.Append("Phone:" + cr.CourseSchedule.Location.Phone + vbCrLf)
        EmailBody.Append("The reservation number is:" + cr.ReservationNumber + vbCrLf)
        EmailBody.Append(trainingadmin_link + "?OrderNumber=" + cr.ReservationNumber + vbCrLf)

        EmailBody.Append(vbCrLf + vbCrLf + "Copyright " + Date.Now.Year.ToString() + " Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, ConfigurationData.Environment.Email.Training.Training_Admin) With {
            .Subject = $"ServicesPLUS(sm) Training Course Reservation ({cr.CourseSchedule.Course.Number})",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendAdminCourseAvailableEMail(ByRef cs As CourseSchedule, ByRef smtpServer As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link
        Dim trainingadmin_link As String = ConfigurationData.Environment.URL.Training_AdminLink

        Dim emailBodyString As String = " Course " + cs.Course.Title + "(" + cs.Course.Number + ")." + vbCrLf
        EmailBody.Append(emailBodyString)
        emailBodyString = "From " + cs.StartDate.ToString("MM/dd/yyyy") + " to " + cs.EndDate.ToString("MM/dd/yyyy") + " ," + cs.Time + " at following address" + vbCrLf
        EmailBody.Append(emailBodyString)
        EmailBody.Append("is available now. Please check the waiting list to reserve the students in the list." + vbCrLf)

        EmailBody.Append(vbCrLf + vbCrLf + "Copyright " + Date.Now.Year.ToString() + " Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.Training.Training_Admin, ConfigurationData.Environment.Email.Training.Training_Admin) With {
            .Subject = $"ServicesPLUS(sm) Training Class ({cs.PartNumber}) is available now",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            If smtpServer.ToLower() = "smtp_server" Or smtpServer.ToLower() = "emailserver" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            ElseIf smtpServer.ToLower() = "smtp_server_cs" Or smtpServer.ToLower() = "emailserver_cs" Then
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer_CS
            End If
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendNotificationEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef smtp_server As String)
        'Modified by prasad for 45
        If cr.Sentinvoiceorquote = 1 Then 'send invoice without notification e-mail
            Dim status As Byte = cr.ParticipantsStatus()
            Select Case status
                Case 1 'All Reserved
                    SendCourseReserverInvoiceEMail(cr, order, order.Customer, smtp_server)
                Case 2 'All WaitListed
                    SendCourseReserverQuoteEMail(cr, order, order.Customer, smtp_server)
                Case 4 'All cancelled
            End Select
        Else
            SendCourseReservationEMail(order, cr, order.Customer, smtp_server)
        End If

        'If smtp_server <> "smtp_server_cs" Then 'from ServicesPlus
        '    SendAdminCourseReservationEMail(cr, order.Customer, smtp_server)
        'End If
    End Sub

    Public Sub SendNonClassroomNotificationEMail(ByRef cr As CourseReservation, ByRef order As Order, ByRef smtp_server As String, ByVal budgetCode As String)
        ' new function to send mails for non class room training
        ' added by Deepa V feb 14,2006
        If Not String.IsNullOrWhiteSpace(Trim(budgetCode) <> "-" And Trim(budgetCode)) Then
            ' send mail to training admin about the user's budget code.
            SendNonClassroomBudgetCodeEMail(cr, order, order.Customer, smtp_server, budgetCode)
        End If
        SendNonClassroomReservationEMail(cr, order, order.Customer, smtp_server)
        'If smtp_server <> "smtp_server_cs" Then 'from ServicesPlus
        '    SendAdminCourseReservationEMail(cr, order.Customer, smtp_server)
        'End If
    End Sub

    'bill me later by sending invoice
    Public Function Checkout(ByVal customer As Customer, ByVal bill_to As Core.Address) As Order
        Try
            Dim order As New Order With {
                .Customer = customer,
                .OrderDate = Date.Now,
                .SAPBillToAccountNumber = "",
                .BillTo = bill_to
            }

            Return order
        Catch ex As Exception
            Throw New Exception("Can not create invoice training order:" + ex.Message)
        End Try
    End Function

    'using credit card
    Public Function Checkout(ByRef customer As Customer, ByRef credit_card As CreditCard, ByRef bill_to As Core.Address) As Order
        Try
            Dim order As New Order With {
                .Customer = customer,
                .OrderDate = Date.Now,
                .CreditCard = credit_card,
                .SAPBillToAccountNumber = "",
                .BillTo = bill_to
            }

            Return order
        Catch ex As Exception
            Throw New Exception("Can not create credit card training order:" + ex.Message)
        End Try
    End Function

    'on account
    Public Function Checkout(ByRef customer As Customer, ByRef bill_to As Account) As Order
        Try
            Dim order As New Order With {
                .Customer = customer,
                .OrderDate = Date.Now,
                .SAPBillToAccountNumber = bill_to.AccountNumber,
                .BillTo = bill_to.Address 'save the bill to address selected
            }

            Return order
        Catch ex As Exception
            Throw New Exception("Can not create on account training order:" + ex.Message)
        End Try
    End Function

    Public Function CheckClassValidity(ByVal PartNumber As String) As String
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Try
            txn_context = data_store.BeginTransaction
            PartNumber = data_store.CheckClassValidity(PartNumber, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw New Exception("Can not validate class: " + ex.Message)
        End Try

        Return PartNumber
    End Function

    Public Function CompleteOrder(ByRef order As Order, ByRef reservation As CourseReservation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim debugString = $"CourseManager.CompleteOrder - START at {Date.Now.ToShortTimeString()}{Environment.NewLine}"

        order.AllocatedPartSubTotal = 0.0 'reservation.Participants.Length * reservation.CourseSchedule.Course.ListPrice
        order.AllocatedGrandTotal = 0.0 'order.AllocatedPartSubTotal
        'debugString += $"- Grand Total: {order.AllocatedGrandTotal}{Environment.NewLine}"
        txn_context = data_store.BeginTransaction
        Try
            ' 2018-07-18 Removed purchasing from training orders
            data_store.StoreTrainingOrder(order, txn_context)
            reservation.ReservationNumber = order.OrderNumber
            debugString += $"- StoreTrainingOrder complete. Order Number: {reservation.ReservationNumber}{Environment.NewLine}"
            data_store.StoreReservation(reservation, txn_context)
            debugString += $"- StoreReservation complete.{Environment.NewLine}"
            For Each participant As CourseParticipant In reservation.Participants
                participant.ReservationNumber = reservation.ReservationNumber
                participant.UpdatedSIAMID = reservation.CustomerSIAMID
                debugString += $"- Participant Reservation: {participant.ReservationNumber}, SIAM: {participant.UpdatedSIAMID}. Calling StoreParticipant.{Environment.NewLine}"
                data_store.StoreParticipant(participant, txn_context)
            Next
            data_store.CommitTransaction(txn_context)
            Return order
        Catch ex As Exception
            ServicesPlusException.Utilities.LogDebug(debugString)
            data_store.RollbackTransaction(txn_context)
            Throw New Exception("Can not complete training order: " + ex.Message, ex)
        End Try
    End Function

    Public Function CompleteNonClassroomOrder(ByRef order As Order, ByRef cr As CourseReservation)
        Dim data_store As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing

        ' non class room training orders have no reservation or participants
        order.AllocatedPartSubTotal = cr.CourseSchedule.Course.ListPrice
        order.AllocatedGrandTotal = order.AllocatedPartSubTotal
        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreTrainingOrder(order, txn_context)
            data_store.CommitTransaction(txn_context)
            Return order
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw New Exception("Cannot complete Nonclass room training order: " + ex.Message)
        End Try
    End Function

    Public Sub ProcessTrainingOrder(ByRef order As Order, ByRef cr As CourseReservation, ByRef invoiceNumber As String)
        Dim cdm As New CourseDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim someCancelled = False

        If Not String.IsNullOrWhiteSpace(invoiceNumber) Then
            Dim invoice As New Invoice(order) With {
                .InvoiceNumber = invoiceNumber,
                .Total = order.AllocatedGrandTotal
            }

            Dim line_item As New InvoiceLineItem(invoice) With {
                .OrderLineNumber = "1", 'dummy value, no use here
                .InvoiceLineNumber = "1", 'dummy value, no use here
                .PartNumberShipped = cr.CourseSchedule.PartNumber,
                .PartDescription = cr.CourseSchedule.Course.Description,
                .Quantity = 1, 'now always 1 for each order
                .ItemPrice = cr.CourseSchedule.Course.ListPrice
            }
            invoice.AddLineItem(line_item)
            order.AddInvoice(invoice)
            order.Dirty = True
        End If

        txn_context = cdm.BeginTransaction
        Try
            If order.Dirty Then cdm.StoreTrainingOrder(order, txn_context, CoreObject.ActionType.UPDATE)
            cdm.StoreReservation(cr, txn_context, CoreObject.ActionType.UPDATE)
            For Each cp As CourseParticipant In cr.Participants
                cdm.StoreParticipant(cp, txn_context, CoreObject.ActionType.UPDATE)
                If cp.Status = "C" Then 'Need to check the availiability
                    someCancelled = True
                End If
            Next
            If someCancelled Then 'Need to check the availiability
                GetCourseVacancy(cr.CourseSchedule, False)
                If cr.CourseSchedule.Vacancy > 0 Then 'now available, send out e-mail
                    SendAdminCourseAvailableEMail(cr.CourseSchedule, "smtp_server_cs")
                End If
            End If
        Catch ex As Exception
            cdm.RollbackTransaction(txn_context)
            Throw New Exception("Can not processa and update training order:" + ex.Message)
        End Try
        cdm.CommitTransaction(txn_context)
    End Sub

    Public Sub ProcessTrainingStudent(ByRef cp As CourseParticipant, ByRef cs As CourseSchedule, ByVal bToReserve As Boolean)
        Dim cdm As New CourseDataManager
        Dim txn_context_cdm As TransactionContext = Nothing

        Try
            If bToReserve Then              'need to check the vacancy
                GetCourseVacancy(cs, True)
                If cs.Vacancy < 1 Then     'not available, don't proceed
                    Throw New Exception("No vacancy")
                End If
            End If

            txn_context_cdm = cdm.BeginTransaction
            cdm.StoreParticipant(cp, txn_context_cdm, CoreObject.ActionType.UPDATE)
            cdm.CommitTransaction(txn_context_cdm)
        Catch ex As Exception
            cdm.RollbackTransaction(txn_context_cdm)
            Throw New Exception("Can not processa and update training student:" + ex.Message)
        End Try

        If cp.Status = "C" Then 'Need to check the availiability
            GetCourseVacancy(cs, False)
            If cs.Vacancy > 0 Then 'now available, send out e-mail
                SendAdminCourseAvailableEMail(cs, "smtp_server_cs")
            End If
        End If
    End Sub

    Public Function CheckIsPreviousDateNull(ByVal PartNumber As String) As Boolean
        Dim data_store As New CourseDataManager
        Dim IsPrevDateNull As Boolean
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            IsPrevDateNull = data_store.CheckIsPreviousDateNull(txn_context, PartNumber)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return IsPrevDateNull
    End Function
End Class

Public Class SearchCriteria
    Public partNumber As String = ""
    Public majorCategory As String = ""
    Public model As String = ""
    Public courseNumber As String = ""
    Public courseTitle As String = ""
    Public city As String = ""
    Public state As String = ""
    Public location As String = ""
    Public type As String = ""
    Public month As Integer = -1

    Public Sub New()

    End Sub

    Public Sub FillIn(ByVal MC As String, ByVal M As String, ByVal CN As String, ByVal T As String, ByVal CT As String, ByVal ST As String, ByVal L As String, ByVal TY As String, ByVal MN As String)
        If Not MC Is Nothing Then majorCategory = MC
        If Not M Is Nothing Then model = M
        If Not CN Is Nothing Then courseNumber = CN
        If Not T Is Nothing Then courseTitle = T
        If Not CT Is Nothing Then city = CT
        If Not ST Is Nothing Then state = ST
        If Not L Is Nothing Then location = L
        If Not TY Is Nothing Then type = TY
        If Not MN Is Nothing Then month = Integer.Parse(MN)
    End Sub

    Public Function GetQueryString() As String
        Dim queryStr As New StringBuilder

        If Not String.IsNullOrWhiteSpace(partNumber) Then
            queryStr.Append("PartNumber=" + partNumber)
            Return queryStr.ToString() 'don't need other criteria
        End If
        If Not String.IsNullOrWhiteSpace(majorCategory) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("MC=" + majorCategory)
        End If
        If Not String.IsNullOrWhiteSpace(model) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("M=" + model)
        End If
        If Not String.IsNullOrWhiteSpace(courseNumber) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("CN=" + courseNumber)
        End If
        If Not String.IsNullOrWhiteSpace(courseTitle) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("T=" + courseTitle)
        End If
        If Not String.IsNullOrWhiteSpace(city) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("CT=" + city)
        End If
        If Not String.IsNullOrWhiteSpace(state) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("ST=" + state)
        End If
        If Not String.IsNullOrWhiteSpace(location) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("L=" + location)
        End If
        If Not String.IsNullOrWhiteSpace(type) Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("TY=" + type)
        End If
        If month <> -1 Then
            If queryStr.Length > 0 Then queryStr.Append("&")
            queryStr.Append("MN=" + month.ToString())
        End If
        Return queryStr.ToString()
    End Function
End Class
