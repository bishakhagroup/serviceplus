Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web
Imports System.Web.SessionState
Imports System.Xml.Serialization
Imports ServicesPlusException
Imports Sony.US.AuditLog
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process.WebMethods
Imports Sony.US.SIAMUtilities

Public Class SAPHelper
    Private strUname As String = String.Empty
    Private strPwd As String = String.Empty
    Private strUrl As String = String.Empty
    Private strCertificatePath As String = String.Empty
    Private strsalesorderCertificatePath As String = String.Empty
    Private _sourcePart As String
    Private usCulture As New CultureInfo("en-US")

    Public Sub New()
        Try
            ' Set up the WebMethods connection values
            strUname = ConfigurationData.Environment.SAPWebMethods.UserName
            strPwd = Encryption.DeCrypt(ConfigurationData.Environment.SAPWebMethods.password, Encryption.PWDKey)
            strCertificatePath = ConfigurationData.Environment.SAPWebMethods.CertificatePath
            strsalesorderCertificatePath = ConfigurationData.Environment.SAPWebMethods.SalesOrderCertificatePath
        Catch ex As Exception
            Throw New ServicesPlusException.ServicesPlusBusinessException(ERRORCODE.SEP0001, ex)
        End Try
    End Sub
    Public Property sourcePart() As String
        Get
            Return _sourcePart
        End Get
        Set(ByVal value As String)
            _sourcePart = value
        End Set

    End Property

    Public Function GetSAPCredential() As System.Net.ICredentials
        Try
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            Return New NetworkCredential(strUname, strPwd)
        Catch ex As Exception
            Throw New ServicesPLUSWebMethodException(ex.Message)
        End Try
    End Function

#Region "Part Inquiry"
    Public Function PartInquiry(ByRef customer As Customer, ByRef PartNumber As String, ByRef PartNumbers As String(), Optional ByRef sLogmessage As String = "", Optional ByVal InventoryInfo As String = "X", Optional ByVal PriceInfo As String = "X", Optional ByVal Location As String = "", Optional ByVal isFromQuickorder As Boolean = False, Optional ByRef objGlobalData As GlobalData = Nothing, Optional ByRef AccntNo As String = Nothing) As SAPProxyPart() 'Sasikumar WP
        Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
        Dim objPCILoggerForCustomer As New PCILogger() '6524
        Dim SAP_Account_Number As String = ""
        Dim PartNumberDetails As String()
        Dim objSAPPartNumber As PartInquiry.PartNumber
        Dim objPartNumberCollection() As PartInquiry.PartNumber
        Dim objSAPPartInquiryMapping As SAPProxyPart
        Dim objSAPPartInquiryArrayList As New ArrayList()
        Dim objarrySAPKit As New ArrayList()
        Dim objpartInquiry As New PartInquiry.partInquiry()
        Dim objPartInquiryRequest As New PartInquiry.PartInquiryRequest()
        Dim objPartInquiryResponse As New PartInquiry.PartInquiryResponse()
        Dim Cert As X509Certificate
        'Dim objUtilities As ServicesPlusException.Utilities

        Try
            'Utilities.LogMessages(DateTime.Now.ToShortTimeString + " SAPHelper.PartInquiry ---START---")
            'Utilities.LogMessages("SAPHelper.PartInquiry - PartNumber = " & PartNumber)

            If Not String.IsNullOrEmpty(PartNumber) Then
                PartNumberDetails = PartNumber.Split(",")
            ElseIf (PartNumbers IsNot Nothing) AndAlso (PartNumbers.Length > 0) Then
                PartNumberDetails = PartNumbers
            Else
                Utilities.LogDebug("SAPHelper.PartInquiry - PartNumber and PartNumbers are empty.")
                Throw New NullReferenceException("PartNumber and PartNumbers are both null or empty.")
            End If
            'Utilities.LogDebug(String.Format("PartInquiry - PartNumberDetails Length = {0}, Customer Is Null? = {1}", PartNumberDetails.Length, (customer Is Nothing).ToString()))
            If customer Is Nothing Then Utilities.LogDebug("SAPHelper.PartInquiry - Customer is null.")

            If customer.UserType = "A" And customer.IsActive = True Then
                If AccntNo IsNot Nothing Then
                    SAP_Account_Number = AccntNo
                Else
                    If (customer.SAPBillToAccounts Is Nothing) OrElse (customer.SAPBillToAccounts.Length = 0) Then
                        SAP_Account_Number = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
                    ElseIf customer.SAPBillToAccounts.Length = 1 Then
                        SAP_Account_Number = customer.SAPBillToAccounts(0).SoldToAccount
                    ElseIf customer.SAPBillToAccounts.Length > 1 Then
                        ''Send any one sold to account
                        For Each account As Account In customer.SAPBillToAccounts
                            If account.SoldToAccount IsNot Nothing Then
                                SAP_Account_Number = account.SoldToAccount
                                Exit For
                            End If
                        Next
                    End If
                End If
            Else
                SAP_Account_Number = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
            End If
            'Utilities.LogDebug("PartInquiry - SAP Account Number set to " & IIf(SAP_Account_Number Is Nothing, "null", SAP_Account_Number))

            If (String.IsNullOrEmpty(sourcePart)) Then
                sourcePart = PartNumber
            End If
            If (String.IsNullOrEmpty(sLogmessage)) Then
                sLogmessage = "Customer request for " + sourcePart + " led to Part Inquiry webMethod call for  " + PartNumber + " with account " + SAP_Account_Number + "."
            End If
            'Utilities.LogDebug("PartInquiry - SourcePart is " & sourcePart)

            For Each objPart As String In PartNumberDetails
                If Not String.IsNullOrEmpty(objPart.Trim()) Then
                    objSAPPartNumber = New PartInquiry.PartNumber()
                    objSAPPartNumber.PartNumber1 = objPart.ToUpper()
                    objSAPPartInquiryArrayList.Add(objSAPPartNumber)
                End If
            Next
            'Utilities.LogDebug(String.Format("objSAPPartInquiryArrayList is Null = {0}, Length = {1}", (objSAPPartInquiryArrayList Is Nothing), objSAPPartInquiryArrayList.Count))

            objPartNumberCollection = New PartInquiry.PartNumber(objSAPPartInquiryArrayList.Count - 1) {}
            objSAPPartInquiryArrayList.CopyTo(objPartNumberCollection)
            objpartInquiry.Credentials = GetSAPCredential()
            objpartInquiry.Url = ConfigurationData.Environment.SAPWebMethods.PartInquiry
            Cert = X509Certificate.CreateFromCertFile(strCertificatePath)
            'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objpartInquiry.ClientCertificates.Add(Cert)
            'Utilities.LogDebug("PartInquiry - Certificate attached")

            ' GlobalData is nothing assign default value,else get values from object.
            If objGlobalData Is Nothing Then ' Assign US as default value.
                objGlobalData = New GlobalData()
                objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
            End If
            objPartInquiryRequest.SalesOrganization = objGlobalData.SalesOrganization
            objPartInquiryRequest.DistributionChannel = objGlobalData.DistributionChannel
            objPartInquiryRequest.Division = objGlobalData.Division
            If objGlobalData.IsFrench Then
                'Utilities.LogDebug("SAPHelper.PartInquiry - C'est Francais!")
                objPartInquiryRequest.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.CA
            Else
                'Utilities.LogDebug("SAPHelper.PartInquiry - It's English!")
                objPartInquiryRequest.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
            End If
            'Utilities.LogDebug("PartInquiry - GlobalData processed")

            'Check for the caccount used for the
            objPartInquiryRequest.SapAcNumber = SAP_Account_Number
            'objPartInquiryRequest.SapAcNumber = "1078406" Commented the hardcoded value fixed the bugId:8671
            objPartInquiryRequest.PartNumber = objPartNumberCollection
            objPartInquiryRequest.InventoryInfo = InventoryInfo
            ''As per bug 6322
            objPartInquiryRequest.Location = IIf(String.IsNullOrEmpty(Location), ConfigurationData.Environment.SAPWebMethods.PartLocation, Location)
            objPartInquiryRequest.PriceInfo = PriceInfo
            objpartInquiry.SoapVersion = Web.Services.Protocols.SoapProtocolVersion.Default
            ''Set the timeout for webmethod 300000 milli seconds
            objpartInquiry.Timeout = Convert.ToInt64(ConfigurationData.Environment.SAPWebMethods.timeout)
            'Utilities.LogDebug("PartInquiry - WebMethods request built.")
            objPartInquiryResponse = objpartInquiry.ProcessPartInquiryResponse(objPartInquiryRequest)

            'objSAPPartInquiryMapping = New SAPProxyPart() '(objPartInquiryResponse.PartInquiryResponse1.Count - 1) {}
            objSAPPartInquiryArrayList = New ArrayList()
            'TODO: change the hard coding
            For Each inputPartNumber As String In PartNumberDetails
                'Utilities.LogDebug("PartInquiry - Part Number loop.")
                If Not String.IsNullOrEmpty(inputPartNumber) Then
                    For Each objPartInquiryResponse2 As PartInquiry.PartInquiryResponse2 In objPartInquiryResponse.PartInquiryResponse1
                        If String.IsNullOrEmpty(objPartInquiryResponse2.OUTPUT5) And String.IsNullOrEmpty(objPartInquiryResponse2.ErrorNumber) Then
                            Throw New Exception("The operation has timed out")
                        End If

                        ''Part not found Exception
                        If Not String.IsNullOrEmpty(objPartInquiryResponse2.ErrorNumber) Then
                            If objPartInquiryResponse2.ErrorNumber.Equals("024") Or objPartInquiryResponse2.ErrorNumber.Equals("025") Then
                                Dim objEx As New ServicesPlusBusinessException("Item " + inputPartNumber.Trim() + " not found. If you are sure this is the item you need," _
                                 + " please <a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link _
                                 + "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ")
                                objEx.errorMessage = sLogmessage + objPartInquiryResponse2.ErrorNumber + objPartInquiryResponse2.ErrorMessage
                                objEx.setcustomMessage = True
                                objEx.DisplayIncidentNumber = True
                                Throw objEx
                            ElseIf objPartInquiryResponse2.ErrorNumber.Equals("021") Or objPartInquiryResponse2.ErrorNumber.Equals("004") Then
                                Dim objEx As New ServicesPlusBusinessException("Sony open account " + SAP_Account_Number + " is invalid for use with the ServicesPLUS site." _
                                 + " Please <a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link _
                                 + "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ")
                                objEx.errorMessage = sLogmessage + objPartInquiryResponse2.ErrorNumber + objPartInquiryResponse2.ErrorMessage
                                objEx.setcustomMessage = True
                                objEx.DisplayIncidentNumber = True
                                Throw objEx
                            End If
                        End If

                        If isFromQuickorder Then
                            If objPartInquiryResponse2.OUTPUT1.ToUpper.Equals("ZSPS") Then
                                Dim objEx As New ServicesPlusBusinessException("Item " + sourcePart + ": Not available for sale." _
                                 + " Please <a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link _
                                 + "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ")
                                objEx.errorMessage = sLogmessage + "PlayStation item " + inputPartNumber + " is not available for sale."
                                objEx.setcustomMessage = True
                                objEx.DisplayIncidentNumber = True
                                Throw objEx
                            End If
                        End If

                        'Utilities.LogDebug("SAPHelper.PartInquiry - Parse and validate part number.")
                        If inputPartNumber.Trim.ToUpper() = objPartInquiryResponse2.OUTPUT5.Trim.ToUpper() Then
                            objSAPPartInquiryMapping = New SAPProxyPart()
                            ''Kit logic Goes Here
                            If objPartInquiryResponse2.Kitinfo IsNot Nothing Then
                                'Utilities.LogDebug("SAPHelper.PartInquiry - Kit Info")
                                objSAPPartInquiryMapping.hasKit = True
                                objSAPPartInquiryMapping.kit = GetKitArray(objPartInquiryResponse2)
                            End If

                            ''Check if the part start with 991099
                            'If Convert.ToDouble(IIf(objPartInquiryResponse2.NumberAvailable.Trim() = String.Empty, "0", objPartInquiryResponse2.NumberAvailable.ToString().Trim())) > 0 Then
                            'Utilities.LogDebug("SAPHelper.PartInquiry - Validate Part")
                            'Utilities.LogDebug(String.Format("Number Available: {0}, inputPartNumber: {1}, Replacement: {2}", objPartInquiryResponse2.NumberAvailable, inputPartNumber, objPartInquiryResponse2.ReplacementPartNumber))
                            If (Not String.IsNullOrEmpty(objPartInquiryResponse2.NumberAvailable.Trim())) AndAlso (Convert.ToDouble(objPartInquiryResponse2.NumberAvailable.Trim(), usCulture) > 0.0R) Then
                                'Utilities.LogDebug("SAPHelper.PartInquiry - Validate Part 1")
                                ValidatePart(objPartInquiryResponse2, sLogmessage, inputPartNumber, sFeature, isFromQuickorder, sourcePart)
                            ElseIf inputPartNumber.Trim() = objPartInquiryResponse2.ReplacementPartNumber Or String.IsNullOrEmpty(objPartInquiryResponse2.ReplacementPartNumber) Then
                                'Utilities.LogDebug("SAPHelper.PartInquiry - Validate Part 2")
                                ValidatePart(objPartInquiryResponse2, sLogmessage, inputPartNumber, sFeature, isFromQuickorder, sourcePart)
                            Else
                                'Utilities.LogDebug("SAPHelper.PartInquiry - Replacement part?")
                                Dim replacementPart As String = objPartInquiryResponse2.ReplacementPartNumber
                                If Not replacementPart.Trim().ToUpper() = inputPartNumber.Trim().ToUpper() Then 'Input PartNumber=sourcepart as A and ReplacementPartNumber=spart as B as per AddtoCartVisio18
                                    'PartNumber = sPart
                                    'sLogmessage = "Customer request for " + sourcePart + " led to Part Inquiry webMethod call for  " + PartNumber + " with account " + SAP_Account_Number + "."
                                    sLogmessage = "Customer request for " + inputPartNumber + " led to Part Inquiry webMethod call for  " + replacementPart + " with account " + SAP_Account_Number + "."
                                    Return PartInquiry(customer, replacementPart, Nothing, sLogmessage, , , , , objGlobalData)
                                Else
                                    Dim objEx As New ServicesPlusBusinessException("Item " + inputPartNumber.Trim() + ": Error occured," _
                                     + " Please " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link _
                                     + "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ")
                                    'objEx.errorMessage = sLogmessage + "Circular replacement chain: " + sourcePart.Trim() + " led to " + sPart.Trim() + " led to " + sourcePart.Trim() + "."
                                    objEx.errorMessage = String.Format("{0} Circular replacement chain: {1} led to {2} led to {3}.", sLogmessage, sourcePart, replacementPart, sourcePart)
                                    objEx.setcustomMessage = True
                                    objEx.DisplayIncidentNumber = True
                                    Throw objEx
                                End If
                            End If

                            'Utilities.LogDebug("PartInquiry - Part Inquiry Mapping 1.")
                            objSAPPartInquiryMapping.Description = objPartInquiryResponse2.PartDescription
                            objSAPPartInquiryMapping.Availability = Convert.ToDouble(IIf(objPartInquiryResponse2.NumberAvailable.ToString().Trim() = String.Empty, "0", objPartInquiryResponse2.NumberAvailable.Trim()), usCulture)
                            objSAPPartInquiryMapping.ListPrice = Convert.ToDouble(IIf(objPartInquiryResponse2.ListPrice.Trim() = String.Empty, "0", objPartInquiryResponse2.ListPrice.Trim()), usCulture)
                            objSAPPartInquiryMapping.YourPrice = Convert.ToDouble(IIf(objPartInquiryResponse2.YourPrice.Trim() = String.Empty, "0", objPartInquiryResponse2.YourPrice.Trim()), usCulture) ''25

                            'Utilities.LogDebug("PartInquiry - Part Inquiry Mapping 2.")
                            If (Not String.IsNullOrEmpty(objPartInquiryResponse2.CoreCharge)) Then
                                objSAPPartInquiryMapping.CoreCharge = Convert.ToDouble(IIf(objPartInquiryResponse2.CoreCharge.Trim() = String.Empty, "0", objPartInquiryResponse2.CoreCharge.Trim()), usCulture)
                            Else
                                objSAPPartInquiryMapping.CoreCharge = 0
                            End If

                            'Utilities.LogDebug("PartInquiry - Part Inquiry Mapping 3.")
                            objSAPPartInquiryMapping.RecyclingFlag = Convert.ToDouble(IIf(objPartInquiryResponse2.RecyclingFlag.Trim() = String.Empty, "0", objPartInquiryResponse2.RecyclingFlag.Trim()), usCulture)
                            objSAPPartInquiryMapping.ProgramCode = IIf(objPartInquiryResponse2.OUTPUT3 Is Nothing, "", objPartInquiryResponse2.OUTPUT3)
                            objSAPPartInquiryMapping.Replacement = IIf(objPartInquiryResponse2.ReplacementPartNumber Is Nothing, "", objPartInquiryResponse2.ReplacementPartNumber)
                            'Utilities.LogDebug("PartInquiry - Part Inquiry Mapping 4.")
                            objSAPPartInquiryMapping.Number = IIf(objPartInquiryResponse2.OUTPUT5 Is Nothing, "", objPartInquiryResponse2.OUTPUT5)
                            objSAPPartInquiryMapping.BillingOnly = IIf(objPartInquiryResponse2.BillingOnly Is Nothing, "N", objPartInquiryResponse2.BillingOnly)
                            objSAPPartInquiryMapping.MaterialType = objPartInquiryResponse2.OUTPUT1
                            'Utilities.LogDebug("PartInquiry - Part Inquiry Mapping 5.")
                            objSAPPartInquiryArrayList.Add(objSAPPartInquiryMapping)
                            Exit For
                        End If
                    Next
                End If
            Next
            'Utilities.LogDebug(String.Format("PartInquiry - Mapping Collection. ArrayList is null: {0}, ArrayList count: {1}", IIf(objSAPPartInquiryArrayList Is Nothing, "Yes", "No"), IIf(objSAPPartInquiryArrayList Is Nothing, "Null", objSAPPartInquiryArrayList.Count)))
            Dim objSAPPartInquiryMappingCollection() As SAPProxyPart = New SAPProxyPart(objSAPPartInquiryArrayList.Count - 1) {}
            'Utilities.LogDebug("PartInquiry - After Mapping")
            objSAPPartInquiryArrayList.CopyTo(objSAPPartInquiryMappingCollection)

            'Utilities.LogDebug("PartInquiry - Returning Mapping Collection")
            Return objSAPPartInquiryMappingCollection
        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "PartInquiry"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            If (customer IsNot Nothing) Then
                objPCILoggerForCustomer.CustomerID = customer.CustomerID
                objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber
                'objPCILoggerForCustomer.UserName = customer.UserName
                objPCILoggerForCustomer.EmailAddress = customer.EmailAddress
                objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
                objPCILoggerForCustomer.LDAP_ID = customer.LdapID
            End If
            objPCILoggerForCustomer.EventType = EventType.View
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "Part Inquiry Failed. " & ex.Message.ToString()
            objPCILoggerForCustomer.PushLogToMSMQ()

            If (ex.Message.Contains("The operation has timed out")) Then
                Dim objEx As New ServicesPlusBusinessException("Item " + PartNumber + ": Operation to add to cart timed out." _
                 + " Please try again or <a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link _
                 + "',null,'" + sFeature + "');"">contact us</a> for assistance and mention incident number ")
                objEx.errorMessage = sLogmessage + " Operation time out."
                objEx.setcustomMessage = True
                objEx.DisplayIncidentNumber = True
                Throw objEx
            End If

            Throw
        End Try
    End Function

    Public Function GetKitArray(ByVal resp As PartInquiry.PartInquiryResponse2) As SAPProxyKit()
        Try
            Dim objKit As SAPProxyKit
            Dim objkitArrayList As New ArrayList()
            For Each Kit As PartInquiry.Kitinfo In resp.Kitinfo
                objKit = New SAPProxyKit()
                objKit.Availability = IIf(String.IsNullOrEmpty(Kit.Availability), 0, Kit.Availability)
                objKit.Description = IIf(String.IsNullOrEmpty(Kit.Description), "", Kit.Description)
                objKit.ListPrice = IIf(String.IsNullOrEmpty(Kit.ItemListPrice), 0, Convert.ToDouble(Kit.ItemListPrice, usCulture))
                objKit.Quantity = IIf(String.IsNullOrEmpty(Kit.ItemQuantity), 0, Convert.ToDouble(Kit.ItemQuantity, usCulture))
                objKit.YourPrice = IIf(String.IsNullOrEmpty(Kit.ItemYourPrice), 0, Convert.ToDouble(Kit.ItemYourPrice, usCulture))
                objkitArrayList.Add(objKit)
            Next
            Dim objSAPKitInquiryMappingColleciton() As SAPProxyKit = New SAPProxyKit(resp.Kitinfo.Length - 1) {}
            objkitArrayList.CopyTo(objSAPKitInquiryMappingColleciton)
            Return objSAPKitInquiryMappingColleciton
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function GetInvoiceLineItem(ByVal SAPOrder As PoInquiry.poResponse, ByVal SPSOrder As Order, ByVal strInvoiceNumber As String) As Core.InvoiceLineItem
        Dim data_store As New OrderDataManager
        Dim objSAPOrderDetail As New PoInquiry.OrderDetail()
        Dim orderLineCollectionInDB() As OrderLineItem
        Dim item As OrderLineItem
        Dim inv_line As InvoiceLineItem = Nothing
        Dim objSPSProduct As Product = Nothing
        Dim found_it As Boolean = False

        Try
            orderLineCollectionInDB = data_store.GetOrderDetails(SPSOrder.OrderNumber)
        Catch ex As Exception
            Throw ex
        End Try

        'Loop thru the SAP Order deails
        For Each objSAPOrderDetail In SAPOrder.OrderDetail()
            SPSOrder.POReference = IIf(objSAPOrderDetail.OUTPUT1B Is Nothing, "", objSAPOrderDetail.OUTPUT1B)
            item = New OrderLineItem(SPSOrder)
            'inv_line = New InvoiceLineItem(invObject) '5325
            found_it = False
            For Each l_item As OrderLineItem In orderLineCollectionInDB
                If objSAPOrderDetail.PARTNUMBER = l_item.Product.PartNumber And Convert.ToInt16(objSAPOrderDetail.LINENUMBER, usCulture) = 10 * l_item.SequenceNumber Then
                    found_it = True
                    Select Case l_item.Product.Type
                        Case Product.ProductType.Kit
                            objSPSProduct = New Kit
                        Case Product.ProductType.Part
                            objSPSProduct = New Part
                        Case Product.ProductType.ComputerBasedTraining
                            If l_item.Product.SoftwareItem Is Nothing Then
                                objSPSProduct = New Part
                            Else
                                objSPSProduct = New Software
                            End If
                        Case Product.ProductType.Software,
                             Product.ProductType.Certificate,
                             Product.ProductType.OnlineTraining
                            objSPSProduct = New Software
                        Case Product.ProductType.ExtendedWarranty
                            objSPSProduct = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty)
                    End Select
                    'objSPSProduct.ListPrice = ????
                    'Took List Price from database it is not coming from SAP
                    If String.IsNullOrWhiteSpace(objSAPOrderDetail.CUSTOMERPRICE) Then objSAPOrderDetail.CUSTOMERPRICE = "0.00"
                    objSPSProduct.ListPrice = Convert.ToDouble(objSAPOrderDetail.CUSTOMERPRICE.Trim(), usCulture)
                End If
            Next
            If Not found_it Then
                objSPSProduct = New Part
            End If

            objSPSProduct.Description = IIf(objSAPOrderDetail.PARTDESCRIPTION Is Nothing, "", objSAPOrderDetail.PARTDESCRIPTION)
            objSPSProduct.PartNumber = IIf(objSAPOrderDetail.PARTNUMBER Is Nothing, "", objSAPOrderDetail.PARTNUMBER)

            item.Quantity = Convert.ToInt16(Convert.ToDouble(objSAPOrderDetail.PARTQUANTITY.Trim()), usCulture)
            objSPSProduct.ListPrice = objSPSProduct.ListPrice / item.Quantity
            item.InvoiceNumber = objSAPOrderDetail.INVOICENUMBER '5325
            '----------------
            If strInvoiceNumber = item.InvoiceNumber Then
                Dim invObject As Invoice = New Invoice(SPSOrder)
                inv_line = New InvoiceLineItem(invObject)
                inv_line.PartNumberShipped = objSPSProduct.PartNumber
                inv_line.PartDescription = objSPSProduct.Description
                inv_line.ItemPrice = objSPSProduct.ListPrice
                inv_line.Quantity = item.Quantity
                inv_line.InvoiceNumber = item.InvoiceNumber
                invObject.AddLineItem(inv_line)
            End If
        Next
        Return inv_line
    End Function

    Public Sub ValidatePart(ByVal objPartInquiryResponse2 As PartInquiry.PartInquiryResponse2, ByVal sLogmessage As String, ByVal strPart As String, ByVal sFeature As String, ByVal isFromQuickorder As Boolean, ByVal Sourcepart As String)
        'Utilities.LogMessages("ValidatePart - Check Output5")
        If objPartInquiryResponse2.OUTPUT5.Trim.StartsWith("991099") Then
            'Utilities.LogMessages("ValidatePart - Starts with 991099")
            Dim objEx As New ServicesPlusBusinessException("Item " + Me.sourcePart + ": " + objPartInquiryResponse2.PartDescription + ". Call 1-800-538-7550 for more information")

            objEx.errorMessage = sLogmessage + " " + strPart + " start with 991099, so PartDescription value of " + objPartInquiryResponse2.PartDescription + " shown to customer."
            objEx.setcustomMessage = True
            objEx.DisplayIncidentNumber = False  'Added by sathish for 9019 .
            Throw objEx
        End If

        ''Check if the part start with 991099
        'Utilities.LogMessages("ValidatePart - Check Output2")
        If objPartInquiryResponse2.OUTPUT2.StartsWith("HOL") Or objPartInquiryResponse2.OUTPUT2.StartsWith("INT") Then
            'Utilities.LogMessages("ValidatePart - Output2 is invalid")
            Dim objEx As New ServicesPlusBusinessException("Item " + Me.sourcePart + ": For Sony internal sales only" + ", call 1-800-538-7550 for more information")

            objEx.errorMessage = sLogmessage + " " + strPart + " Part Category(OUPUT2) value of " + objPartInquiryResponse2.OUTPUT2 + "is for internal sony sale only."
            objEx.setcustomMessage = True
            objEx.DisplayIncidentNumber = False 'Added by sathish for 9019 .
            Throw objEx
        End If

        ''Check if the listprice is null or less then zero
        'Utilities.LogMessages("ValidatePart - Check ListPrice for null")
        If String.IsNullOrEmpty(objPartInquiryResponse2.ListPrice) Then
            'Utilities.LogMessages("ValidatePart - ListPrice is null")
            Dim objEx As New ServicesPlusBusinessException("Item " + Me.sourcePart + ": Price exception" + ", call 1-800-538-7550 for more information")

            'objEx.errorMessage = sLogmessage + " " + strPart + " ListPrice value of" + objPartInquiryResponse2.ListPrice + "  is less than or equal to 0 or is null or blank." '7420 and 7419
            objEx.errorMessage = sLogmessage + " " + strPart + " ListPrice value of" + objPartInquiryResponse2.ListPrice + "  is less than or equal to 0.01 or is null or blank." '7420 and 7419
            objEx.setcustomMessage = True
            objEx.DisplayIncidentNumber = False 'Added by sathish for 9019 .
            Throw objEx
        End If

        'Utilities.LogMessages("ValidatePart - Check ListPrice for less than zero")
        If Convert.ToDouble(objPartInquiryResponse2.ListPrice, usCulture) <= 0.0R Then
            'Utilities.LogMessages("ValidatePart - ListPrice is less than zero")
            Dim objEx1 As New ServicesPlusBusinessException("Item " + Me.sourcePart + ": Price exception" + ", call 1-800-538-7550 for more information") ' Replaced Line No 490 by sathish for 9019 as per ricks doc ServicesPLUS Add to Cart High Level v29
            objEx1.errorMessage = sLogmessage + " " + strPart + " ListPrice value of" + objPartInquiryResponse2.ListPrice + "  is less than or equal to 0.01 or is null or blank." '7420 and 7419
            objEx1.setcustomMessage = True
            objEx1.DisplayIncidentNumber = False
            Throw objEx1
        End If

        ''Check if the listprice is null or less then zero
        'Utilities.LogMessages("ValidatePart - Check YourPrice for null")
        If String.IsNullOrEmpty(objPartInquiryResponse2.YourPrice) Then
            'Utilities.LogMessages("ValidatePart - YourPrice is null")
            Dim objEx As New ServicesPlusBusinessException("Item " + objPartInquiryResponse2.OUTPUT5.Trim() + ": Price exception, call 1-800-538-7550 for more information")

            objEx.errorMessage = sLogmessage + " " + strPart + " YourPrice value of" + objPartInquiryResponse2.YourPrice + "  is less than or equal to 0.01 or is null or blank." '7420
            objEx.setcustomMessage = True
            objEx.DisplayIncidentNumber = False 'Added by sathish for 9019 .
            Throw objEx
        End If
        'If Convert.ToDouble(objPartInquiryResponse2.YourPrice) <= 0 Then'7420 and 7419
        'Utilities.LogMessages("ValidatePart - Check YourPrice for less than zero")
        If Convert.ToDouble(objPartInquiryResponse2.YourPrice, usCulture) <= 0.0R Then
            'Utilities.LogMessages("ValidatePart - YourPrice is less than zero")
            Dim objEx1 As New ServicesPlusBusinessException("Item " + objPartInquiryResponse2.OUTPUT5.Trim() + ": Price exception" + ", call 1-800-538-7550 for more information") ' Replaced Line No 518 by sathish for 9019 as per ricks doc ServicesPLUS Add to Cart High Level v29
            objEx1.errorMessage = sLogmessage + " " + strPart + " YourPrice value of" + objPartInquiryResponse2.YourPrice + "  is less than or equal to 0.01 or is null or blank." '7420 and 7419
            objEx1.setcustomMessage = True
            objEx1.DisplayIncidentNumber = False 'Added by sathish for 9019 .
            Throw objEx1
        End If

        'Utilities.LogMessages("ValidatePart - Check Quick Order")
        If isFromQuickorder Then
            'Utilities.LogMessages("ValidatePart - Is Quick Order")
            If objPartInquiryResponse2.OUTPUT1.ToUpper = ConfigurationData.Environment.PartNumberMaterialType.MaterialType Then
                Utilities.LogMessages("ValidatePart - Output1 is MaterialType")
                Dim objEx1 As New ServicesPlusBusinessException("Item " + Sourcepart + ": This item is not shippable, and therefore cannot be added from this page.  " _
                            + " If you are ordering:</br>1) Downloadable Software go to the Software tab <br/> 2) Training go to the Training tab. " _
                            + " <br/>Please " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + "contact us" + "</a>" + " for assistance")
                Dim sPagePath As String = System.Web.HttpContext.Current.Request.Url.AbsolutePath

                objEx1.errorMessage = sLogmessage + " " + strPart + " " + "with Material Type  is not allowed from " + sPagePath + "."
                objEx1.setcustomMessage = True
                objEx1.DisplayIncidentNumber = False 'Added by sathish for 9019 .
                Throw objEx1
            End If
        End If

    End Sub
    Public Function PartInquiry1(ByRef customer As Customer, ByVal PartNumber As String, ByVal PartNumbers As String(), Optional ByVal InventoryInfo As String = "Y", Optional ByVal PriceInfo As String = "Y", Optional ByVal Location As String = "N") As SAPProxyPart()
        Dim objPCILoggerForCustomer As PCILogger = New PCILogger() '6524
        Try
            Dim PartNumberDetails As String()
            '6524V11 Starts
            ''6524 start
            'objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            'objPCILoggerForCustomer.EventOriginApplicationLocation = Me.GetType.Name
            'objPCILoggerForCustomer.EventOriginMethod = "PartInquiry1"
            'objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            'objPCILoggerForCustomer.OperationalUser = Environment.UserName
            'objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            'objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            ''objPCILoggerForCustomer.UserName = customer.UserName
            'objPCILoggerForCustomer.EmailAddress = customer.EmailAddress '6524
            'objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            'objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            'objPCILoggerForCustomer.EventType = EventType.Update
            'objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString()
            'objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            'objPCILoggerForCustomer.IndicationSuccessFailure = "Success"
            'objPCILoggerForCustomer.Message = "PartInquiry1 Success."
            ''6524 end
            '6524V11 Ends
            If PartNumber Is Nothing Then

                PartNumberDetails = PartNumbers
            Else
                PartNumberDetails = PartNumber.Split(",")
            End If

            Dim objSAPPartInquiryMapping As SAPProxyPart()
            objSAPPartInquiryMapping = New SAPProxyPart(0) {}
            Dim i As Integer = 0
            ' While i < PartNumberDetails.Count
            objSAPPartInquiryMapping(i) = New SAPProxyPart()
            objSAPPartInquiryMapping(i).Description = "test"
            objSAPPartInquiryMapping(i).Availability = 4
            objSAPPartInquiryMapping(i).ListPrice = 10
            objSAPPartInquiryMapping(i).YourPrice = 5
            objSAPPartInquiryMapping(i).CoreCharge = 4
            objSAPPartInquiryMapping(i).RecyclingFlag = "RF"
            objSAPPartInquiryMapping(i).ProgramCode = "PC"
            objSAPPartInquiryMapping(i).Replacement = PartNumberDetails(i)
            objSAPPartInquiryMapping(i).Number = PartNumberDetails(i)
            'i = PartNumberDetails.Count + 1
            'End While
            Dim objarrySAPKit As ArrayList = New ArrayList()
            Dim objSAPKit As New SAPProxyKit()
            objSAPKit.Description = "test"
            objSAPKit.Availability = 1
            objSAPKit.ListPrice = 1 'Convert.ToDouble(IIf(objKIt.ItemListPrice.Trim() = String.Empty, "0", objKIt.ItemListPrice.Trim()))
            objSAPKit.Number = "testNumber" 'objKIt.ItemNumber
            objSAPKit.YourPrice = "11" 'objKIt.ItemYourPrice
            objSAPKit.Quantity = "" '' objKIt.ItemQuantity
            objSAPKit.StatusMessage = "" '' objKIt.StatusMessage
            objSAPKit.StatusFlag = "" ''objKIt.StatusFlag
            objSAPKit.Category = "" 'objKIt.Category
            objarrySAPKit.Add(objSAPKit)

            Dim objSAkitColleciton() As SAPProxyKit = New SAPProxyKit(objarrySAPKit.Count - 1) {}
            objarrySAPKit.CopyTo(objSAkitColleciton)

            objSAPPartInquiryMapping(0).kit = objSAkitColleciton

            Return objSAPPartInquiryMapping

        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "PartInquiry1"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            'objPCILoggerForCustomer.UserName = customer.UserName
            objPCILoggerForCustomer.EmailAddress = customer.EmailAddress '6524
            objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            objPCILoggerForCustomer.EventType = EventType.Update
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "Part Inquiry Failed. " & ex.Message.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524'6524V11
        End Try
    End Function
    Public Function kitInquiry(ByRef customer As Customer, ByVal KitNumber As String, ByVal KitNumbers As String(), Optional ByVal InventoryInfo As String = "X", Optional ByVal PriceInfo As String = "X", Optional ByVal Location As String = "") As SAPProxyKit()
        Dim objPCILoggerForCustomer As PCILogger = New PCILogger() '6524
        Try
            Dim SAP_Account_Number As String

            Dim kitNumberDetails As String()
            '6524V11 Starts
            ''6524 start
            'objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            'objPCILoggerForCustomer.EventOriginApplicationLocation = Me.GetType.Name
            'objPCILoggerForCustomer.EventOriginMethod = "kitInquiry"
            'objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            'objPCILoggerForCustomer.OperationalUser = Environment.UserName
            'objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            'objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            ''objPCILoggerForCustomer.UserName = customer.UserName
            'objPCILoggerForCustomer.EmailAddress = customer.EmailAddress.ToString() '6524
            'objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            'objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            'objPCILoggerForCustomer.EventType = EventType.View
            'objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString()
            'objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            'objPCILoggerForCustomer.IndicationSuccessFailure = "Success"
            'objPCILoggerForCustomer.Message = "kitInquiry Success."
            ''6524 end
            '6524V11 Ends
            If KitNumber Is Nothing Then

                kitNumberDetails = KitNumbers
            Else
                kitNumberDetails = KitNumber.Split(",")
            End If
            Dim objSAkitColleciton() As SAPProxyKit = New SAPProxyKit(kitNumberDetails.Count - 1) {}
            Dim objSAPPartNumber As PartInquiry.PartNumber
            Dim objPartNumberCollection() As PartInquiry.PartNumber

            'sapPart = New PartInquiry.PartInquiryResponse
            'If customer.UserType = "AccountHolder" And customer.IsActive = True Then'6668
            If (customer.UserType = "A" Or customer.UserType = "P") And customer.IsActive = True Then '6668
                If customer.SAPBillToAccounts.Length > 0 Then
                    SAP_Account_Number = customer.SAPBillToAccounts(0).AccountNumber.ToString()
                Else
                    Throw New ApplicationException("No active account for this user.")
                End If
            Else
                SAP_Account_Number = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
            End If
            Dim iLenth As Integer = kitNumberDetails.Count
            Dim objPart As String = String.Empty
            Dim objSAPPartInquiryArrayList As ArrayList = New ArrayList()
            Dim objarrySAPKit As ArrayList = New ArrayList()

            For Each objPart In kitNumberDetails
                If objPart.Trim() <> String.Empty Then
                    objSAPPartNumber = New PartInquiry.PartNumber()
                    objSAPPartNumber.PartNumber1 = objPart
                    objSAPPartInquiryArrayList.Add(objSAPPartNumber)
                End If
            Next
            objPartNumberCollection = New PartInquiry.PartNumber(objSAPPartInquiryArrayList.Count - 1) {}
            objSAPPartInquiryArrayList.CopyTo(objPartNumberCollection)


            Dim objpartInquiry As New PartInquiry.partInquiry()
            Dim objPartInquiryRequest As New PartInquiry.PartInquiryRequest()
            Dim objPartInquiryResponse As New PartInquiry.PartInquiryResponse()
            Dim Cert As X509Certificate
            objpartInquiry.Credentials = GetSAPCredential()
            objpartInquiry.Url = ConfigurationData.Environment.SAPWebMethods.PartInquiry
            Cert = X509Certificate.CreateFromCertFile(strCertificatePath)
            'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objpartInquiry.ClientCertificates.Add(Cert)


            'Check for the caccount used for the
            objPartInquiryRequest.SapAcNumber = SAP_Account_Number
            'objPartInquiryRequest.SapAcNumber = "1001231"
            objPartInquiryRequest.PartNumber = objPartNumberCollection
            objPartInquiryRequest.InventoryInfo = InventoryInfo
            objPartInquiryRequest.Location = IIf(Location = "", ConfigurationData.Environment.SAPWebMethods.PartLocation, Location)
            objPartInquiryRequest.PriceInfo = PriceInfo

            objPartInquiryResponse = objpartInquiry.ProcessPartInquiryResponse(objPartInquiryRequest)

            'objSAPPartInquiryMapping = New SAPProxyPart() '(objPartInquiryResponse.PartInquiryResponse1.Count - 1) {}
            objSAPPartInquiryArrayList = New ArrayList()
            'TODO: change the hard coding
            For Each strPart As String In kitNumberDetails
                If strPart.Trim() <> String.Empty Then
                    For Each objPartInquiryResponse2 As PartInquiry.PartInquiryResponse2 In objPartInquiryResponse.PartInquiryResponse1
                        If strPart.Trim.ToUpper() = objPartInquiryResponse2.OUTPUT5.Trim.ToUpper() Then

                            If Not objPartInquiryResponse2.Kitinfo Is Nothing Then
                                For Each objKIt As PartInquiry.Kitinfo In objPartInquiryResponse2.Kitinfo
                                    Dim objSAPKit As New SAPProxyKit()
                                    objSAPKit.Description = objKIt.Description
                                    objSAPKit.Availability = objKIt.Availability
                                    objSAPKit.ListPrice = Convert.ToDouble(IIf(objKIt.ItemListPrice.Trim() = String.Empty, "0", objKIt.ItemListPrice.Trim()), usCulture)
                                    objSAPKit.Number = objKIt.ItemNumber
                                    objSAPKit.YourPrice = objKIt.ItemYourPrice
                                    objSAPKit.Quantity = objKIt.ItemQuantity
                                    objSAPKit.StatusMessage = objKIt.StatusMessage
                                    objSAPKit.StatusFlag = objKIt.StatusFlag
                                    objSAPKit.Category = objKIt.Category
                                    objarrySAPKit.Add(objSAPKit)
                                Next
                                objarrySAPKit.CopyTo(objSAkitColleciton)
                            End If
                            Exit For
                        End If
                    Next
                End If
            Next
            'Dim objSAPPartInquiryMappingColleciton() As SAPProxyPart = New SAPProxyPart(objSAPPartInquiryArrayList.Count - 1) {}
            'objSAPPartInquiryArrayList.CopyTo(objSAPPartInquiryMappingColleciton)
            Return objSAkitColleciton
        Catch ex As TimeoutException
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "KitInquiry Failed. " & ex.Message.ToString() '6524
            Throw New Exception()
        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "kitInquiry"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            'objPCILoggerForCustomer.UserName = customer.UserName
            objPCILoggerForCustomer.EmailAddress = customer.EmailAddress.ToString() '6524
            objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            objPCILoggerForCustomer.EventType = EventType.View
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "KitInquiry Failed. " & ex.Message.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try
    End Function

    Public Function kitInquiry1(ByRef customer As Customer, ByVal KitNumber As String, ByVal KitNumbers As String(), Optional ByVal InventoryInfo As String = "X", Optional ByVal PriceInfo As String = "X", Optional ByVal Location As String = "") As SAPProxyKit()
        Dim objPCILoggerForCustomer As PCILogger = New PCILogger() '6524
        Try
            Dim SAP_Account_Number As String

            Dim kitNumberDetails As String()
            '6524V11 Starts
            ''6524 start
            'objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            'objPCILoggerForCustomer.EventOriginApplicationLocation = Me.GetType.Name
            'objPCILoggerForCustomer.EventOriginMethod = "kitInquiry1"
            'objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            'objPCILoggerForCustomer.OperationalUser = Environment.UserName
            'objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            'objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            ''objPCILoggerForCustomer.UserName = customer.UserName
            'objPCILoggerForCustomer.EmailAddress = customer.EmailAddress '6524
            'objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            'objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            'objPCILoggerForCustomer.EventType = EventType.View
            'objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString()
            'objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            'objPCILoggerForCustomer.IndicationSuccessFailure = "Success"
            'objPCILoggerForCustomer.Message = "kitInquiry1 Success."
            ''6524 end
            '6524V11 Ends
            If KitNumber Is Nothing Then

                kitNumberDetails = KitNumbers
            Else
                kitNumberDetails = KitNumber.Split(",")
            End If
            Dim objSAkitColleciton() As SAPProxyKit = New SAPProxyKit(kitNumberDetails.Count - 1) {}
            Dim objSAPPartNumber As PartInquiry.PartNumber
            Dim objPartNumberCollection() As PartInquiry.PartNumber

            'sapPart = New PartInquiry.PartInquiryResponse
            'If customer.UserType = "AccountHolder" And customer.IsActive = True Then'6668
            If (customer.UserType = "A" Or customer.UserType = "P") And customer.IsActive = True Then '6668
                If customer.SAPBillToAccounts.Length > 0 Then
                    SAP_Account_Number = customer.SAPBillToAccounts(0).AccountNumber.ToString()
                Else
                    Throw New ApplicationException("No active account for this user.")
                End If
            Else
                SAP_Account_Number = ConfigurationData.Environment.SAPWebMethods.DefaultAccount
            End If
            Dim iLenth As Integer = kitNumberDetails.Count
            Dim objPart As String = String.Empty
            Dim objSAPPartInquiryArrayList As ArrayList = New ArrayList()
            Dim objarrySAPKit As ArrayList = New ArrayList()

            For Each objPart In kitNumberDetails
                If objPart.Trim() <> String.Empty Then
                    objSAPPartNumber = New PartInquiry.PartNumber()
                    objSAPPartNumber.PartNumber1 = objPart
                    objSAPPartInquiryArrayList.Add(objSAPPartNumber)
                End If
            Next
            objPartNumberCollection = New PartInquiry.PartNumber(objSAPPartInquiryArrayList.Count - 1) {}
            objSAPPartInquiryArrayList.CopyTo(objPartNumberCollection)



            'Check for the caccount used for the





            Dim objSAPKit As New SAPProxyKit()
            objSAPKit.Description = "Test Description"
            objSAPKit.Availability = 10 ''objKIt.Availability
            objSAPKit.ListPrice = 1000 ''Convert.ToDouble(IIf(objKIt.ItemListPrice.Trim() = String.Empty, "0", objKIt.ItemListPrice.Trim()))
            objSAPKit.Number = 1 'objKIt.ItemNumber
            objSAPKit.YourPrice = 111 'objKIt.ItemYourPrice
            objSAPKit.Quantity = 1 'objKIt.ItemQuantity
            objSAPKit.StatusMessage = "test" 'objKIt.StatusMessage
            objSAPKit.StatusFlag = "test" 'objKIt.StatusFlag
            objSAPKit.Category = "Test" 'objKIt.Category
            objarrySAPKit.Add(objSAPKit)

            objarrySAPKit.CopyTo(objSAkitColleciton)



            Return objSAkitColleciton
        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "kitInquiry1"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.CustomerID = customer.CustomerID.ToString()
            objPCILoggerForCustomer.CustomerID_SequenceNumber = customer.SequenceNumber.ToString()
            'objPCILoggerForCustomer.UserName = customer.UserName
            objPCILoggerForCustomer.EmailAddress = customer.EmailAddress '6524
            objPCILoggerForCustomer.SIAM_ID = customer.SIAMIdentity
            objPCILoggerForCustomer.LDAP_ID = customer.LdapID '6524
            objPCILoggerForCustomer.EventType = EventType.View
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "kitInquiry1 Failed. " & ex.Message.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try
    End Function
#End Region

#Region "ORDER Inquiry, Cancel Order, Customer LookUp"

    Public Function CancelOrder(ByVal OrderNumber As String, ByVal LineNumber As String(), ByVal ReasonforRejection As String) As String
        Dim objPCILoggerForCustomer As New PCILogger()
        Try
            Dim URL As String = ConfigurationData.Environment.SAPWebMethods.CancelOrder
            Dim objCancelOrderRequest As New CancelOrder.cancelOrderRequest()
            Dim objCancelOrderResponse As New CancelOrder.cancelOrderResponse()
            Dim objCancelOrder As New CancelOrder.cancelOrder()
            Dim objCancelOrderResult As New CancelOrder.cancelOrderResult()
            Dim objCancelOrdeLine As New CancelOrder.lineNumbers()
            Dim Cert As X509Certificate

            objCancelOrder.Credentials = GetSAPCredential()
            objCancelOrder.Url = ConfigurationData.Environment.SAPWebMethods.CancelOrder
            Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objCancelOrder.ClientCertificates.Add(Cert)
            objCancelOrderRequest.orderNumber = OrderNumber
            objCancelOrderRequest.reasonForRejection = ReasonforRejection
            objCancelOrdeLine.lineNumber = LineNumber
            objCancelOrderRequest.lineNumbers = objCancelOrdeLine

            Dim temStr As String = SerialiseObject(objCancelOrderRequest)
            objCancelOrderResponse = objCancelOrder.CallcancelOrder(objCancelOrderRequest)
            ''objCancelOrderResult = objCancelOrderResponse.cancelOrderResult
            If Not objCancelOrderResponse Is Nothing Then
                For Each objCancel As CancelOrder.cancelOrderResult In objCancelOrderResponse.cancelOrderResult
                    If objCancel.errorNumber = "S" Then
                        Return "S"
                    Else
                        Throw New ServicesPLUSWebMethodException(objCancel.errorNumber)
                    End If
                Next
            End If
        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "CancelOrder"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.EventType = EventType.Others
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "Cancel Order Failed. " & ex.Message.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11
            Throw New ServicesPLUSWebMethodException("")
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try
        Return Nothing
    End Function

    ''Commented for 6219
    'Added the Third parameter
    'Public Function SAPPayorInquiry(ByVal objAccount As Account) As Account()
    '    Try

    '        Dim returnValue As Account() = Nothing
    '        Dim thisArrayList As New ArrayList
    '        Dim objdatabaseOutput As CustomerLookUp.CustomerLookupOutput = New CustomerLookUp.CustomerLookupOutput()
    '        Dim SAPPayerAccountNumber As String = objAccount.AccountNumber
    '        objdatabaseOutput = CustomerLookUp("", "", SAPPayerAccountNumber)
    '        If Not objdatabaseOutput.BillTo Is Nothing Then
    '            objAccount.BillTo.Line1 = objdatabaseOutput.BillTo.Address1
    '            objAccount.BillTo.Line2 = objdatabaseOutput.BillTo.Address2
    '            objAccount.BillTo.Line3 = objdatabaseOutput.BillTo.Address3

    '            objAccount.BillTo.Name = objdatabaseOutput.BillTo.Name
    '            objAccount.BillTo.PhoneExt = objdatabaseOutput.BillTo.PhoneExtension
    '            objAccount.BillTo.PhoneNumber = objdatabaseOutput.BillTo.Phone
    '            objAccount.BillTo.PostalCode = objdatabaseOutput.BillTo.Zip
    '            objAccount.BillTo.State = objdatabaseOutput.BillTo.State
    '            objAccount.BillTo.City = objdatabaseOutput.BillTo.City
    '            objAccount.BillTo.Attn = objdatabaseOutput.BillTo.Attention
    '            objAccount.BillTo.SapAccountNumber = SAPPayerAccountNumber
    '            objAccount.ShipToAccount = New Core.Account(objdatabaseOutput.ShipTo.Length - 1) {}
    '            Dim iCounter As Int16 = 0
    '            For Each objShiptoLocalAccount As CustomerLookUp.ShipTo In objdatabaseOutput.ShipTo
    '                objAccount.ShipToAccount(iCounter) = New Core.Account()
    '                objAccount.ShipToAccount(iCounter).Address.Line1 = IIf(objShiptoLocalAccount.Address1 Is Nothing, "", objShiptoLocalAccount.Address1)
    '                objAccount.ShipToAccount(iCounter).Address.Line2 = IIf(objShiptoLocalAccount.Address2 Is Nothing, "", objShiptoLocalAccount.Address2)
    '                objAccount.ShipToAccount(iCounter).Address.Line3 = IIf(objShiptoLocalAccount.Address3 Is Nothing, "", objShiptoLocalAccount.Address3)

    '                objAccount.ShipToAccount(iCounter).Address.Name = IIf(objShiptoLocalAccount.Name Is Nothing, "", objShiptoLocalAccount.Name)
    '                objAccount.ShipToAccount(iCounter).Address.PhoneExt = IIf(objShiptoLocalAccount.PhoneExtension Is Nothing, "", objShiptoLocalAccount.PhoneExtension)
    '                objAccount.ShipToAccount(iCounter).Address.PhoneNumber = IIf(objShiptoLocalAccount.Phone Is Nothing, "", objShiptoLocalAccount.Phone)
    '                objAccount.ShipToAccount(iCounter).Address.PostalCode = IIf(objShiptoLocalAccount.Zip Is Nothing, "", objShiptoLocalAccount.Zip)
    '                objAccount.ShipToAccount(iCounter).Address.State = IIf(objShiptoLocalAccount.State Is Nothing, "", objShiptoLocalAccount.State)
    '                objAccount.ShipToAccount(iCounter).Address.City = IIf(objShiptoLocalAccount.City Is Nothing, "", objShiptoLocalAccount.City)
    '                objAccount.ShipToAccount(iCounter).Address.Attn = IIf(objShiptoLocalAccount.Attention Is Nothing, "", objShiptoLocalAccount.Attention)
    '                objAccount.ShipToAccount(iCounter).SAPAccount = IIf(objShiptoLocalAccount.SAPShipToAccountNumber Is Nothing, "", objShiptoLocalAccount.SAPShipToAccountNumber)
    '                iCounter = iCounter + 1
    '                thisArrayList.Add(objAccount)
    '            Next
    '        End If
    '        objAccount.AccountNumber = SAPPayerAccountNumber
    '        If thisArrayList.Count > 0 Then returnValue = thisArrayList.ToArray(objAccount.GetType)
    '        Return returnValue

    '    Catch ex As Exception
    '        Throw New ServicesPLUSWebMethodException(ex.Message)
    '    End Try

    'End Function
    'Added the Third parameter
    'Public Function SAPPayorInquiry(ByVal sCustomer As String) As Account()
    '    Try
    '        Dim returnValue As Account() = Nothing
    '        Dim thisArrayList As New ArrayList
    '        Dim objdatabaseOutput As CustomerLookUp.CustomerLookupOutput = New CustomerLookUp.CustomerLookupOutput()
    '        'Dim SAPPayerAccountNumber As String = objAccount.AccountNumber
    '        Dim objAccount As New Account()
    '        objdatabaseOutput = CustomerLookUp("", sCustomer, "")
    '        If Not objdatabaseOutput.BillTo Is Nothing Then

    '            objAccount.BillTo.Line1 = objdatabaseOutput.BillTo.Address1
    '            objAccount.BillTo.Line2 = objdatabaseOutput.BillTo.Address2
    '            objAccount.BillTo.Line3 = objdatabaseOutput.BillTo.Address3

    '            objAccount.BillTo.Name = objdatabaseOutput.BillTo.Name
    '            objAccount.BillTo.PhoneExt = objdatabaseOutput.BillTo.PhoneExtension
    '            objAccount.BillTo.PhoneNumber = objdatabaseOutput.BillTo.Phone
    '            objAccount.BillTo.PostalCode = objdatabaseOutput.BillTo.Zip
    '            objAccount.BillTo.State = objdatabaseOutput.BillTo.State
    '            objAccount.BillTo.City = objdatabaseOutput.BillTo.City
    '            objAccount.BillTo.Attn = objdatabaseOutput.BillTo.Attention
    '            objAccount.BillTo.SapAccountNumber = objdatabaseOutput.SAPPayerAccountNumber ''SAPPayerAccountNumber
    '            objAccount.ShipToAccount = New Core.Account(objdatabaseOutput.ShipTo.Length - 1) {}
    '            Dim iCounter As Int16 = 0
    '            For Each objShiptoLocalAccount As CustomerLookUp.ShipTo In objdatabaseOutput.ShipTo
    '                objAccount.ShipToAccount(iCounter) = New Core.Account()
    '                objAccount.ShipToAccount(iCounter).Address.Line1 = IIf(objShiptoLocalAccount.Address1 Is Nothing, "", objShiptoLocalAccount.Address1)
    '                objAccount.ShipToAccount(iCounter).Address.Line2 = IIf(objShiptoLocalAccount.Address2 Is Nothing, "", objShiptoLocalAccount.Address2)
    '                objAccount.ShipToAccount(iCounter).Address.Line3 = IIf(objShiptoLocalAccount.Address3 Is Nothing, "", objShiptoLocalAccount.Address3)

    '                objAccount.ShipToAccount(iCounter).Address.Name = IIf(objShiptoLocalAccount.Name Is Nothing, "", objShiptoLocalAccount.Name)
    '                objAccount.ShipToAccount(iCounter).Address.PhoneExt = IIf(objShiptoLocalAccount.PhoneExtension Is Nothing, "", objShiptoLocalAccount.PhoneExtension)
    '                objAccount.ShipToAccount(iCounter).Address.PhoneNumber = IIf(objShiptoLocalAccount.Phone Is Nothing, "", objShiptoLocalAccount.Phone)
    '                objAccount.ShipToAccount(iCounter).Address.PostalCode = IIf(objShiptoLocalAccount.Zip Is Nothing, "", objShiptoLocalAccount.Zip)
    '                objAccount.ShipToAccount(iCounter).Address.State = IIf(objShiptoLocalAccount.State Is Nothing, "", objShiptoLocalAccount.State)
    '                objAccount.ShipToAccount(iCounter).Address.City = IIf(objShiptoLocalAccount.City Is Nothing, "", objShiptoLocalAccount.City)
    '                objAccount.ShipToAccount(iCounter).Address.Attn = IIf(objShiptoLocalAccount.Attention Is Nothing, "", objShiptoLocalAccount.Attention)
    '                objAccount.ShipToAccount(iCounter).SAPAccount = IIf(objShiptoLocalAccount.SAPShipToAccountNumber Is Nothing, "", objShiptoLocalAccount.SAPShipToAccountNumber)
    '                iCounter = iCounter + 1
    '                thisArrayList.Add(objAccount)
    '            Next
    '        End If
    '        objAccount.AccountNumber = objdatabaseOutput.SAPPayerAccountNumber
    '        If thisArrayList.Count > 0 Then returnValue = thisArrayList.ToArray(objAccount.GetType)
    '        Return returnValue

    '    Catch ex As Exception
    '        Throw New ServicesPLUSWebMethodException(ex.Message)
    '    End Try

    'End Function
    Public Function GetSAPayerAccounts(ByVal sCompanyName As String, ByVal sSAPPayerAccount As String(), Optional ByVal objGlobalData As GlobalData = Nothing) As Account()
        Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
        Dim sLogmessage As String = String.Empty
        Dim returnValue As Account() = Nothing
        Dim Cert As X509Certificate
        Dim pciLog = New PCILogger()

        Try
            Dim objCustomerLookup As New CustomerLookup.CustomerLookupWSDL()
            Dim objInput As CustomerLookup.cusPayerAccountInput
            Dim objOutput As CustomerLookup.cusPayerAccountOutput()
            Dim thisArrayList As New ArrayList


            ' GlobalData is nothing assign default value,else get values from object.
            If objGlobalData Is Nothing Then ' Assign US as default
                objGlobalData = New GlobalData With {
                    .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                    .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                    .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                    .Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
                }
            End If

            objInput = New CustomerLookup.cusPayerAccountInput With {
                .CompanyName = sCompanyName,
                .SAPPayerAccountNumber = sSAPPayerAccount,
                .SalesOrganization = objGlobalData.SalesOrganization,
                .DistributionChannel = objGlobalData.DistributionChannel,
                .Division = objGlobalData.Division
            }

            ''TODO conf
            objCustomerLookup.Url = ConfigurationData.Environment.SAPWebMethods.CustomerLookUp
            Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
            'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objCustomerLookup.ClientCertificates.Add(Cert)
            objCustomerLookup.Credentials = GetSAPCredential() 'New NetworkCredential("ServicePlusUser", "ServicePlusUser") ''GetSAPCredential()
            objOutput = objCustomerLookup.getPayerDetails(objInput)
            Dim objAccount = Nothing

            For Each objAcc As CustomerLookup.cusPayerAccountOutput In objOutput
                objAccount = New Account
                If Not objAcc.Payer Is Nothing Then
                    objAccount.BillTo.Line1 = IIf(objAcc.Payer.Address2 Is Nothing, "", objAcc.Payer.Address1)
                    objAccount.BillTo.Line2 = IIf(objAcc.Payer.Address2 Is Nothing, "", objAcc.Payer.Address2)
                    objAccount.BillTo.Line3 = IIf(objAcc.Payer.Address3 Is Nothing, "", objAcc.Payer.Address3)
                    objAccount.BillTo.Name = IIf(objAcc.Payer.Name Is Nothing, "", objAcc.Payer.Name)
                    objAccount.BillTo.PhoneExt = IIf(objAcc.Payer.PhoneExtn Is Nothing, "", objAcc.Payer.PhoneExtn)
                    objAccount.BillTo.PhoneNumber = IIf(objAcc.Payer.Phone Is Nothing, "", objAcc.Payer.Phone)
                    objAccount.BillTo.PostalCode = IIf(objAcc.Payer.Zip Is Nothing, "", objAcc.Payer.Zip)
                    objAccount.BillTo.State = IIf(objAcc.Payer.State Is Nothing, "", objAcc.Payer.State)
                    objAccount.BillTo.City = IIf(objAcc.Payer.City Is Nothing, "", objAcc.Payer.City)
                    objAccount.BillTo.Attn = IIf(objAcc.Payer.Attention Is Nothing, "", objAcc.Payer.Attention)
                    objAccount.Validated = True
                Else
                    objAccount.Validated = False
                End If
                objAccount.BillTo.SapAccountNumber = objAcc.SAPPayerAccountNumber
                objAccount.AccountNumber = objAcc.SAPPayerAccountNumber
                objAccount.SAPAccount = objAcc.SAPPayerAccountNumber
                objAccount.SoldToAccount = objAcc.SapSoldToAccountNum

                thisArrayList.Add(objAccount)
            Next
            If thisArrayList.Count > 0 Then returnValue = thisArrayList.ToArray(objAccount.GetType)
            Return returnValue
        Catch ex As Exception
            pciLog.EventOriginApplication = "ServicesPLUS"
            pciLog.EventOriginApplicationLocation = [GetType].Name
            pciLog.EventOriginMethod = "GetSAPayerAccounts"
            pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            pciLog.OperationalUser = Environment.UserName
            pciLog.EventType = EventType.View
            pciLog.EventDateTime = Date.Now.ToLongTimeString()
            pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
            pciLog.IndicationSuccessFailure = "Failure" '6524
            pciLog.Message = "GetSAPayerAccounts Failed. " & ex.Message.ToString() '6524
            pciLog.PushLogToMSMQ() '6524 '6524V11

            Try
                'Dim objUtilties As Utilities = New Utilities
                Utilities.WrapExceptionforUI(ex)
            Catch e As Exception

            End Try

            If (ex.Message.Contains("The operation has timed out")) Then
                Dim objEx As New ServicesPlusBusinessException("Operation timed out." _
                               + " Please click ""Return to Cart"" below to try the process again, or " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + "contact us" + "</a>" + " for assistance and mention incident number ")
                objEx.errorMessage = sLogmessage + " Operation time out."
                objEx.setcustomMessage = True
                objEx.DisplayIncidentNumber = True 'Added by sathish for 9019 .
                Throw objEx
            End If
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try
        Return Nothing
    End Function

    Public Sub GetShiptoAccount(ByRef objAccount As Account, Optional ByRef objGlobalData As GlobalData = Nothing)
        Dim Cert As X509Certificate
        Dim objPCILoggerForCustomer As PCILogger = New PCILogger() '6524
        Try
            Dim sAccount As String = objAccount.AccountNumber
            Dim objAccounts As String() = {objAccount.AccountNumber}

            Dim objAcc As Account() = GetSAPayerAccounts("", objAccounts, objGlobalData)
            objAccount = objAcc(0)
            'objAccount.Validated = objAcc(0).Validated 'commented for 6865
            Dim objCustomerLookup As New CustomerLookup.CustomerLookupWSDL()
            Dim objShipdetails As New CustomerLookup.cusShipToDetails()
            objCustomerLookup.Url = ConfigurationData.Environment.SAPWebMethods.CustomerLookUp
            Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
            'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objCustomerLookup.ClientCertificates.Add(Cert)
            objCustomerLookup.Credentials = GetSAPCredential()
            ' objShipdetails = objCustomerLookup.getShippingInformation(objAccount.AccountNumber, "", "", "")

            If objGlobalData Is Nothing Then ' Assign US as default.
                objGlobalData = New GlobalData()
                objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
            End If

            objShipdetails = objCustomerLookup.getShippingInformation(objAccount.AccountNumber, "", "", "", objGlobalData.SalesOrganization, objGlobalData.DistributionChannel, objGlobalData.Division)

            Dim thisArrayList As New ArrayList
            If objShipdetails IsNot Nothing Then
                Dim tempAccount = Nothing

                For Each objShiptoLocalAccount As CustomerLookup.ShipTo In objShipdetails.ShipTo
                    tempAccount = New Core.Account
                    tempAccount.AccountNumber = IIf(objShiptoLocalAccount.ShipToDetail1 Is Nothing, "", objShiptoLocalAccount.ShipToDetail1)
                    tempAccount.Address.Line1 = IIf(objShiptoLocalAccount.Address1 Is Nothing, "", objShiptoLocalAccount.Address1)
                    tempAccount.Address.Line2 = IIf(objShiptoLocalAccount.Address2 Is Nothing, "", objShiptoLocalAccount.Address2)
                    tempAccount.Address.Line3 = IIf(objShiptoLocalAccount.Address3 Is Nothing, "", objShiptoLocalAccount.Address3)
                    tempAccount.Address.Name = IIf(objShiptoLocalAccount.Name Is Nothing, "", objShiptoLocalAccount.Name)
                    tempAccount.Address.PhoneExt = IIf(objShiptoLocalAccount.PhoneExtn Is Nothing, "", objShiptoLocalAccount.PhoneExtn)
                    tempAccount.Address.PhoneNumber = IIf(objShiptoLocalAccount.Phone Is Nothing, "", objShiptoLocalAccount.Phone)
                    tempAccount.Address.PostalCode = IIf(objShiptoLocalAccount.Zip Is Nothing, "", objShiptoLocalAccount.Zip)
                    tempAccount.Address.State = IIf(objShiptoLocalAccount.State Is Nothing, "", objShiptoLocalAccount.State)
                    tempAccount.Address.City = IIf(objShiptoLocalAccount.City Is Nothing, "", objShiptoLocalAccount.City)
                    tempAccount.Address.Attn = IIf(objShiptoLocalAccount.Attention Is Nothing, "", objShiptoLocalAccount.Attention)
                    tempAccount.ShipToAccountNumber = IIf(objShiptoLocalAccount.ShipToDetail1 Is Nothing, "", objShiptoLocalAccount.ShipToDetail1)

                    thisArrayList.Add(tempAccount)
                Next
                objAccount.ShipToAccount = thisArrayList.ToArray(tempAccount.GetType())
            End If
        Catch ex As Exception
            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "GetShiptoAccount"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.EventType = EventType.View
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "GetShiptoAccount Failed. " & ex.StackTrace.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11 
            Throw ex
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try

    End Sub
    '7980 starts
    Public Sub GetShiptoAccountBySoldtoAccount(ByRef objAccount As Account, ByRef objGlobalData As GlobalData)
        Dim Cert As X509Certificate
        Dim objPCILoggerForCustomer As PCILogger = New PCILogger() '6524
        Try
            Dim sAccount As String = objAccount.AccountNumber
            Dim objAccounts As String() = {objAccount.AccountNumber}

            Dim objAcc As Account() = GetSAPayerAccounts("", objAccounts, objGlobalData)
            'objAccount = objAcc(0) '7980
            Dim thisArrayList As New ArrayList


            If Not objAcc Is Nothing Then
                Dim tempAccount = Nothing
                'For Each objpayerAccount As Sony.US.ServicesPLUS.Core.Account In objAcc '7980
                For Each objpayerAccount As Sony.US.ServicesPLUS.Core.Account In objAcc '7980
                    objAccount = objpayerAccount
                    '6424 V11 Starts
                    ''6524 start
                    'objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
                    'objPCILoggerForCustomer.EventOriginApplicationLocation = Me.GetType.Name
                    'objPCILoggerForCustomer.EventOriginMethod = "GetShiptoAccount"
                    'objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
                    'objPCILoggerForCustomer.OperationalUser = Environment.UserName
                    'objPCILoggerForCustomer.EventType = EventType.View
                    'objPCILoggerForCustomer.EventDateTime = DateTime.Now.ToLongTimeString()
                    'objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
                    'objPCILoggerForCustomer.IndicationSuccessFailure = "Success"
                    'objPCILoggerForCustomer.Message = "GetShiptoAccount Success."
                    ''6524 end
                    '6424 V11 Ends
                    'objAccount.Validated = objAcc(0).Validated 'commented for 6865
                    Dim objCustomerLookup As New CustomerLookup.CustomerLookupWSDL()
                    Dim objShipdetails As New CustomerLookup.cusShipToDetails()
                    ''TODO conf
                    objCustomerLookup.Url = ConfigurationData.Environment.SAPWebMethods.CustomerLookUp ' "https://a2atest.am.sony.com:4444/ws/SelCustomerLookup.WebServices.Provider:CustomerLookupWSDL"
                    Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
                    'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
                    ServicePointManager.Expect100Continue = True
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
                    ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                    objCustomerLookup.ClientCertificates.Add(Cert)
                    objCustomerLookup.Credentials = GetSAPCredential()
                    'objShipdetails = objCustomerLookup.getShippingInformation(objAccount.AccountNumber, "", "", "")
                    'objShipdetails = objCustomerLookup.getShippingInformation(objAccount.SoldToAccount, "", "", "")
                    If Not objAccount.SoldToAccount Is Nothing Then
                        'Sasikumar WP
                        'objShipdetails = objCustomerLookup.getShippingInformation(objpayerAccount.SoldToAccount, "", "", "")
                        ' GlobalData is nothing, assign default value,else get values from object.
                        If objGlobalData Is Nothing Then
                            objGlobalData.SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US
                            objGlobalData.DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value
                            objGlobalData.Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value
                            objGlobalData.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
                        End If

                        objShipdetails = objCustomerLookup.getShippingInformation(objpayerAccount.SoldToAccount, "", "", "", objGlobalData.SalesOrganization, objGlobalData.DistributionChannel, objGlobalData.Division)

                    End If
                    'Dim thisArrayList As New ArrayList
                    If Not objShipdetails Is Nothing Then

                        'Dim tempAccount = Nothing



                        For Each objShiptoLocalAccount As CustomerLookup.ShipTo In objShipdetails.ShipTo

                            tempAccount = New Core.Account
                            tempAccount.AccountNumber = IIf(objShiptoLocalAccount.ShipToDetail1 Is Nothing, "", objShiptoLocalAccount.ShipToDetail1)
                            tempAccount.Address.Line1 = IIf(objShiptoLocalAccount.Address1 Is Nothing, "", objShiptoLocalAccount.Address1)
                            tempAccount.Address.Line2 = IIf(objShiptoLocalAccount.Address2 Is Nothing, "", objShiptoLocalAccount.Address2)
                            tempAccount.Address.Line3 = IIf(objShiptoLocalAccount.Address3 Is Nothing, "", objShiptoLocalAccount.Address3)


                            tempAccount.Address.Name = IIf(objShiptoLocalAccount.Name Is Nothing, "", objShiptoLocalAccount.Name)
                            tempAccount.Address.PhoneExt = IIf(objShiptoLocalAccount.PhoneExtn Is Nothing, "", objShiptoLocalAccount.PhoneExtn)
                            tempAccount.Address.PhoneNumber = IIf(objShiptoLocalAccount.Phone Is Nothing, "", objShiptoLocalAccount.Phone)
                            tempAccount.Address.PostalCode = IIf(objShiptoLocalAccount.Zip Is Nothing, "", objShiptoLocalAccount.Zip)
                            tempAccount.Address.State = IIf(objShiptoLocalAccount.State Is Nothing, "", objShiptoLocalAccount.State)
                            tempAccount.Address.City = IIf(objShiptoLocalAccount.City Is Nothing, "", objShiptoLocalAccount.City)
                            tempAccount.Address.Attn = IIf(objShiptoLocalAccount.Attention Is Nothing, "", objShiptoLocalAccount.Attention)
                            tempAccount.ShipToAccountNumber = IIf(objShiptoLocalAccount.ShipToDetail1 Is Nothing, "", objShiptoLocalAccount.ShipToDetail1)
                            tempAccount.SoldtoAccNumbyShipParty = IIf(objShipdetails.SAPPayerAccountNumber Is Nothing, "", objShipdetails.SAPPayerAccountNumber) '7980 V1
                            thisArrayList.Add(tempAccount)
                            'Try
                            '	Utilities.fnActionLog("Step 9")
                            'Catch e As Exception

                            'End Try
                        Next
                        'objAccount.ShipToAccount = thisArrayList.ToArray(tempAccount.GetType())
                    End If
                Next '7980

                objAccount.ShipToAccount = thisArrayList.ToArray(tempAccount.GetType())
            End If
        Catch ex As Exception

            objPCILoggerForCustomer.EventOriginApplication = "ServicesPLUS"
            objPCILoggerForCustomer.EventOriginApplicationLocation = [GetType].Name
            objPCILoggerForCustomer.EventOriginMethod = "GetShiptoAccount"
            objPCILoggerForCustomer.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILoggerForCustomer.OperationalUser = Environment.UserName
            objPCILoggerForCustomer.EventType = EventType.View
            objPCILoggerForCustomer.EventDateTime = Date.Now.ToLongTimeString()
            objPCILoggerForCustomer.HTTPRequestObjectValues = objPCILoggerForCustomer.GetRequestContextValue()
            objPCILoggerForCustomer.IndicationSuccessFailure = "Failure" '6524
            objPCILoggerForCustomer.Message = "GetShiptoAccount Failed. " & ex.StackTrace.ToString() '6524
            objPCILoggerForCustomer.PushLogToMSMQ() '6524 '6524V11

            Try
                'Dim objUtilties As Utilities = New Utilities
                Utilities.WrapExceptionforUI(ex)
            Catch e As Exception

            End Try


            Throw ex
        Finally
            'objPCILoggerForCustomer.PushLogToMSMQ() '6524
        End Try

    End Sub
    '7980 ends
    ''Commented for 6219
    'Public Function CustomerLookUp(ByVal LegacyAccountNumber As String, ByVal CompanyName As String, ByVal SAPPayerAccountNumber As String) As Sony.US.ServicesPLUS.Process.com.sony.sap.CustomerLookUp.CustomerLookupOutput
    '    Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
    '    Dim sLogmessage As String = String.Empty
    '    Try

    '        Dim objCustomerLookup As New CustomerLookUp.CustomerLookupWSDL()
    '        Dim Cert As X509Certificate

    '        objCustomerLookup.Credentials = GetSAPCredential()
    '        ''Set the timeout for webmethod 300000 milli seconds
    '        objCustomerLookup.Timeout = Convert.ToInt64(ConfigurationData.Environment.SAPWebMethods.timeout)
    '        objCustomerLookup.Url = ConfigurationData.Environment.SAPWebMethods.CustomerLookUp
    '        Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
    '        ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
    '        objCustomerLookup.ClientCertificates.Add(Cert)

    '        Dim objqueryInput As New CustomerLookUp.CustomerLookupInput()
    '        Dim objdatabaseOutput As CustomerLookUp.CustomerLookupOutput = New CustomerLookUp.CustomerLookupOutput()

    '        objqueryInput.LegacyAccountNumber = LegacyAccountNumber
    '        objqueryInput.CompanyName = CompanyName
    '        objqueryInput.SAPPayerAccountNumber = SAPPayerAccountNumber
    '        sLogmessage = "Customer Lookup webMethod call for SAP Payer Account " + SAPPayerAccountNumber
    '        Dim obRequest As String = Me.SerialiseObject(objqueryInput)
    '        objdatabaseOutput = objCustomerLookup.processCustomerLookup(objqueryInput)
    '        Dim obResult As String = Me.SerialiseObject(objdatabaseOutput)
    '        Return objdatabaseOutput

    '    Catch ex As Exception
    '        If (ex.Message.Contains("The operation has timed out")) Then
    '            Dim objEx As New ServicesPlusBusinessException("Operation timed out." _
    '                                                                        + " Please click ""Return to Cart"" below to try the process again, or " + "<a href=""#"" onclick=""javascript:window.open('" + ConfigurationData.GeneralSettings.URL.contact_us_popup_link + "',null,'" + sFeature + "');"">" + "contact us" + "</a>" + " for assistance and mention incident number ")
    '            objEx.errorMessage = sLogmessage + " Operation time out."
    '            objEx.setcustomMessage = True

    '            Throw objEx
    '        End If

    '    End Try

    'End Function


    Public Function OrderInquiry(ByVal OrderNumber As String, ByVal CheckDetail As Boolean,
           ByVal CheckHeader As Boolean, ByVal CheckInvoice As Boolean, ByRef objGlobalData As GlobalData) As Order

        'Return POInquiry("SVC", OrderNumber, CheckDetail, CheckHeader, CheckInvoice) '5325
        '5325 starts
        Try
            Return POInquiry("SVC", OrderNumber, CheckDetail, CheckHeader, CheckInvoice, objGlobalData)
        Catch ex As Exception
            Throw ex '5325
        End Try
        '5325 ends
    End Function

    Private Function POInquiry(ByVal POSender As String, ByVal PONumber As String, ByVal CheckDetail As Boolean,
           ByVal CheckHeader As Boolean, ByVal CheckInvoice As Boolean, ByRef objGlobalData As GlobalData) As Order
        Dim objPCILogger As PCILogger = New PCILogger() '6524

        Try

            Dim objpoRequest As PoInquiry.poRequest = New PoInquiry.poRequest()
            Dim objpoResponse As New PoInquiry.poResponse()
            Dim objpoInquiry As New PoInquiry.poInquiry()
            Dim objOrderDetail As New PoInquiry.OrderDetail()
            Dim objInvoices As New PoInquiry.Invoices()
            Dim Cert As X509Certificate
            '6424 V11 Starts
            ''6524 start
            'objPCILogger.EventOriginApplication = "ServicesPLUS"
            'objPCILogger.EventOriginApplicationLocation = Me.GetType.Name
            'objPCILogger.EventOriginMethod = "POInquiry"
            'objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            'objPCILogger.OperationalUser = Environment.UserName
            'objPCILogger.EventType = EventType.View
            'objPCILogger.EventDateTime = DateTime.Now.ToLongTimeString()
            'objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
            'objPCILogger.IndicationSuccessFailure = "Success"
            'objPCILogger.Message = "POInquiry Success."
            ''6524 end
            '6424 V11 Ends
            objpoInquiry.Credentials = GetSAPCredential()
            objpoInquiry.Url = ConfigurationData.Environment.SAPWebMethods.POInquiry
            Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.CertificatePath)
            'ServicePointManager.CertificatePolicy = New TrustAllCertificatePolicy()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objpoInquiry.ClientCertificates.Add(Cert)

            objpoRequest.P_SENDER = POSender
            objpoRequest.P_SO_NUMBER = PONumber
            objpoRequest.P_DETAIL_FLAG = IIf(CheckDetail, "Y", "N")
            objpoRequest.P_HEADER_FLAG = IIf(CheckHeader, "Y", "N")
            objpoRequest.P_INVOICE_FLAG = IIf(CheckInvoice, "Y", "N")
            ' ASLeight - Fix for new Language options
            objpoRequest.Language = IIf(objGlobalData.IsFrench, ConfigurationData.GeneralSettings.GlobalData.LanguagewM.CA, ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US)

            Dim objSPSOrder As Order
            'Dim instance As HttpSessionState

            objpoResponse = objpoInquiry.processPoInquiry(objpoRequest)
            If objpoResponse.ErrorNumber = "" Then
                objSPSOrder = New Order(PONumber)
                objSPSOrder.OrderNumber = PONumber

                HttpContext.Current.Session("Source") = "SAP"

                CloneOrder(objGlobalData, objpoResponse, objSPSOrder)
            Else
                If objpoResponse.ErrorNumber = "577" Then
                    'Throw New ServicesPLUSWebMethodException(ERRORCODE.wM109)
                    'Throw New ApplicationException("Exception occurred") '5325
                    'bug(8346)
                    HttpContext.Current.Session("Source") = "ServicePlusDatabase"

                    objSPSOrder = New Order(PONumber)
                    objSPSOrder.OrderNumber = PONumber
                    Dim thisorder As Order = getOrder(PONumber, objGlobalData)
                    CloneDBorder(thisorder, objSPSOrder)
                Else

                    HttpContext.Current.Session("Source") = "ServicePlusDatabase with error"

                    objSPSOrder = New Order(PONumber)
                    objSPSOrder.OrderNumber = PONumber
                    Dim thisorder As Order = getOrder(PONumber, objGlobalData)
                    CloneDBorder(thisorder, objSPSOrder)
                    'Return objSPSOrder
                    'Throw New ServicesPLUSWebMethodException("This order is not available in SAP" + ERRORCODE.ora06550)
                End If

            End If
            Return objSPSOrder

        Catch ex As Exception
            objPCILogger.EventOriginApplication = "ServicesPLUS"
            objPCILogger.EventOriginApplicationLocation = [GetType].Name
            objPCILogger.EventOriginMethod = "POInquiry"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventType = EventType.View
            objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
            objPCILogger.IndicationSuccessFailure = "Failure" '6524
            objPCILogger.Message = "POInquiry Failed. " & ex.Message.ToString() '6524
            objPCILogger.PushLogToMSMQ() '6524 '6524V11
            'Throw New ServicesPLUSWebMethodException(ERRORCODE.wM109)'5325
            Throw ex '5325
        Finally
            'objPCILogger.PushLogToMSMQ() '6524
        End Try


    End Function
    Private Function getOrder(ByVal thisOrderNumber As String, ByRef objGlobalData As GlobalData) As Order
        Dim returnValue As Order = Nothing
        Dim getSISOrder As String = ""
        Dim objPCILogger As PCILogger = New PCILogger()

        Try
            Dim thisOrderManager As New OrderManager
            If Not thisOrderManager Is Nothing Then
                Dim tempOrder As Order
                tempOrder = thisOrderManager.GetOrder(thisOrderNumber, False, , objGlobalData)
                If Not tempOrder Is Nothing Then returnValue = tempOrder
            End If
        Catch ex As Exception
            objPCILogger.IndicationSuccessFailure = "Failure" '6524
            objPCILogger.Message = "getOrder Failed. " & ex.Message.ToString() '6524
        Finally
            objPCILogger.PushLogToMSMQ() '6524
        End Try

        Return returnValue
    End Function

    Private Sub CloneDBorder(ByVal thisorder As Order, ByRef SPSOrder As Order)
        Dim objPCILogger As New PCILogger()

        Try
            SPSOrder.AccountNumber = thisorder.SAPBillToAccountNumber
            SPSOrder.OrderDate = thisorder.OrderDate
            SPSOrder.AllocatedGrandTotal = thisorder.AllocatedGrandTotal
            SPSOrder.AllocatedPartSubTotal = thisorder.AllocatedPartSubTotal
            SPSOrder.AllocatedRecyclingFee = thisorder.AllocatedSpecialTax
            SPSOrder.AllocatedShipping = thisorder.AllocatedShipping
            SPSOrder.POReference = thisorder.POReference
            SPSOrder.AllocatedTax = thisorder.AllocatedTax
            Dim objCartType1 As New CreditCardType()
            SPSOrder.CreditCard = New CreditCard(objCartType1)
            If thisorder.CreditCard IsNot Nothing Then
                objCartType1.Description = ""
                SPSOrder.CreditCard.CreditCardNumber = thisorder.CreditCard.CreditCardNumber
                SPSOrder.CreditCard.ExpirationDate = thisorder.CreditCard.ExpirationDate
            Else
                objCartType1.Description = ""
                SPSOrder.CreditCard.CreditCardNumber = ""
                SPSOrder.CreditCard.ExpirationDate = ""
            End If

            SPSOrder.BillTo = New Core.Address() With {
                .Name = thisorder.BillTo.Name,
                .Attn = thisorder.BillTo.Attn,
                .Line1 = thisorder.BillTo.Line1,
                .Line2 = thisorder.BillTo.Line2,
                .Line4 = thisorder.BillTo.Line4,
                .City = thisorder.BillTo.City,
                .State = thisorder.BillTo.State,
                .PostalCode = thisorder.BillTo.PostalCode,
                .Country = thisorder.BillTo.Country,
                .PhoneNumber = thisorder.BillTo.PhoneNumber,
                .PhoneExt = thisorder.BillTo.PhoneExt
            }

            SPSOrder.ShipTo = New Core.Address() With {
                .Name = thisorder.ShipTo.Name,
                .Attn = thisorder.ShipTo.Attn,
                .Line1 = thisorder.ShipTo.Line1,
                .Line2 = thisorder.ShipTo.Line2,
                .City = thisorder.ShipTo.City,
                .State = thisorder.ShipTo.State,
                .PostalCode = thisorder.ShipTo.PostalCode,
                .Country = thisorder.ShipTo.Country,
                .PhoneNumber = thisorder.ShipTo.PhoneNumber,
                .PhoneExt = thisorder.ShipTo.PhoneExt
            }

            If Not String.IsNullOrWhiteSpace(thisorder.Alt_Tax_Classification) Then
                SPSOrder.Alt_Tax_Classification = thisorder.Alt_Tax_Classification
            Else
                SPSOrder.Alt_Tax_Classification = ""
            End If

            If Not String.IsNullOrWhiteSpace(thisorder.EnableDocumentIDNumber) Then
                SPSOrder.EnableDocumentIDNumber = thisorder.EnableDocumentIDNumber
            Else
                SPSOrder.EnableDocumentIDNumber = ""
            End If

            SPSOrder.BillingOnly = thisorder.BillingOnly

            Dim cm As New CustomerDataManager
            Dim customer As Customer = cm.RestoreByID(thisorder.Customer.CustomerID)

            SPSOrder.Customer = New Customer()
            SPSOrder.Customer.SIAMIdentity = IIf(customer.SIAMIdentity Is Nothing, "", customer.SIAMIdentity)

            Dim orderLineCollectionInDB() As OrderLineItem
            Dim data_store As New OrderDataManager
            Try
                orderLineCollectionInDB = data_store.GetOrderDetails(SPSOrder.OrderNumber)
            Catch ex As Exception
                Throw ex
            End Try
            'Dim item As OrderLineItem
            'Dim objSPSProduct As Product

            For Each l_item As OrderLineItem In orderLineCollectionInDB
                l_item.Product = New Part With {
                    .PartNumber = l_item.partnumber,
                    .ListPrice = l_item.Item_Price,
                    .Description = l_item.DESCRIPTION,
                    .TrainingActivationCode = l_item.CERTIFICATIONACTIVATIONCODE
                }
                SPSOrder.AddLineItem(l_item)
            Next
        Catch ex As Exception
            objPCILogger.EventOriginApplication = "ServicesPLUS"
            objPCILogger.EventOriginApplicationLocation = [GetType].Name
            objPCILogger.EventOriginMethod = "ClonedbOrder"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventType = EventType.Others
            objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
            objPCILogger.IndicationSuccessFailure = "Failure" '6524
            objPCILogger.Message = "CloneDBOrder Failed. " & ex.Message.ToString() '6524
            objPCILogger.PushLogToMSMQ() '6524 '6524V11
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            'objPCILogger.PushLogToMSMQ() '6524
        End Try
    End Sub

    Private Sub CloneOrder(ByRef objGlobalData As GlobalData, ByVal SAPOrder As PoInquiry.poResponse, ByRef SPSOrder As Order)
        Dim objPCILogger As PCILogger = New PCILogger() '6524
        Dim item As OrderLineItem
        Dim objSPSProduct As Product
        Dim inv_line As InvoiceLineItem
        Dim data_store As New OrderDataManager
        Dim order As Order

        Try
            'Order level mapping
            SPSOrder.CarrierCode = IIf(String.IsNullOrEmpty(SAPOrder.CARRIERCODE), "", SAPOrder.CARRIERCODE)
            SPSOrder.CreditReason = IIf(String.IsNullOrEmpty(SAPOrder.CREDITREASON), "", SAPOrder.CREDITREASON)
            SPSOrder.CreditSwitch = IIf(SAPOrder.CREDITSWITCH = "Y", True, False)
            SPSOrder.CustomerClass = IIf(String.IsNullOrEmpty(SAPOrder.CUSTOMERCLASS), "", SAPOrder.CUSTOMERCLASS)
            SPSOrder.GroupNumber = IIf(String.IsNullOrEmpty(SAPOrder.GROUPNUMBER), "", SAPOrder.GROUPNUMBER)
            SPSOrder.InternalOrder = IIf(SAPOrder.INTERNALORDER = "Y", True, False)
            SPSOrder.LegacyAccountNumber = IIf(String.IsNullOrEmpty(SAPOrder.LEGACYACCOUNTNUMBER), "", SAPOrder.LEGACYACCOUNTNUMBER)

            'ShipTo mapping
            SPSOrder.ShipTo = New Core.Address With {
                .Line1 = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOADDRESS1), "", SAPOrder.SHIPTOADDRESS1),
                .Line2 = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOADDRESS2), "", SAPOrder.SHIPTOADDRESS2),
                .Line3 = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOADDRESS3), "", SAPOrder.SHIPTOADDRESS3),
                .Line4 = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOADDRESS4), "", SAPOrder.SHIPTOADDRESS4),
                .City = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOADDRESS4), "", SAPOrder.SHIPTOADDRESS4),
                .State = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOSTATE), "", SAPOrder.SHIPTOSTATE),
                .PostalCode = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOZIP), "", SAPOrder.SHIPTOZIP),
                .Name = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTONAME), "", SAPOrder.SHIPTONAME),
                .OfficeNumber = SAPOrder.SHIPTOOFFICENUMBER,
                .PhoneNumber = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOPHONENUMBER), "", SAPOrder.SHIPTOPHONENUMBER),
                .Country = IIf(String.IsNullOrEmpty(SAPOrder.SHIPTOCOUNTRY), "", SAPOrder.SHIPTOCOUNTRY)
            }

            'BillTo mapping
            SPSOrder.BillTo = New Core.Address With {
                .Line1 = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOADDRESS1), "", SAPOrder.BILLTOADDRESS1),
                .Line2 = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOADDRESS2), "", SAPOrder.BILLTOADDRESS2),
                .Line3 = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOADDRESS3), "", SAPOrder.BILLTOADDRESS3),
                .Line4 = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOADDRESS4), "", SAPOrder.BILLTOADDRESS4),
                .City = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOADDRESS4), "", SAPOrder.BILLTOADDRESS4),
                .State = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOSTATE), "", SAPOrder.BILLTOSTATE),
                .PostalCode = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOZIP), "", SAPOrder.BILLTOZIP),
                .Name = IIf(String.IsNullOrEmpty(SAPOrder.BILLTONAME), "", SAPOrder.BILLTONAME),
                .PhoneNumber = IIf(String.IsNullOrEmpty(SAPOrder.BILLTO_PHONE), "", SAPOrder.BILLTO_PHONE),
                .Country = IIf(String.IsNullOrEmpty(SAPOrder.BILLTOCOUNTRY), "", SAPOrder.BILLTOCOUNTRY),
                .Attn = IIf(String.IsNullOrEmpty(SAPOrder.BILLTO_ATTENTION), "", SAPOrder.BILLTO_ATTENTION)
            }

            order = data_store.Restore(SPSOrder.OrderNumber, objGlobalData)

            'Dim creditcard As New CreditCard
            'creditcard.SequenceNumber = order.cred

            If order.CreditCard IsNot Nothing Then
                Dim objCartType As New CreditCardType With {
                    .Description = ""
                }

                SPSOrder.CreditCard = New CreditCard(objCartType) With {
                    .CreditCardNumber = order.CreditCard.CreditCardNumber,
                    .ExpirationDate = order.CreditCard.ExpirationDate,
                    .SequenceNumber = order.CreditCard.SequenceNumber
                }
                SPSOrder.CreditCard.Type.Description = order.CreditCard.Type.Description
                ''SPSOrder.CreditCard.CreditCardNumberMasked = order.CreditCard.CreditCardNumberMasked
            End If

            SPSOrder.Alt_Tax_Classification = order.Alt_Tax_Classification
            SPSOrder.EnableDocumentIDNumber = order.EnableDocumentIDNumber
            SPSOrder.BillingOnly = order.BillingOnly

            Dim cm As New CustomerDataManager
            Dim customer As Customer = cm.RestoreByID(order.Customer.CustomerID)

            SPSOrder.Customer = New Customer With {
                .FirstName = IIf(String.IsNullOrEmpty(customer.FirstName), "", customer.FirstName),
                .LastName = IIf(String.IsNullOrEmpty(customer.LastName), "", customer.LastName),
                .EmailAddress = IIf(String.IsNullOrEmpty(customer.EmailAddress), "", customer.EmailAddress),
                .SIAMIdentity = IIf(String.IsNullOrEmpty(customer.SIAMIdentity), "", customer.SIAMIdentity),
                .LdapID = IIf(String.IsNullOrEmpty(customer.LdapID), "", customer.LdapID)
            }

            'Amount mapping
            If String.IsNullOrEmpty(SAPOrder.AMOUNT) Then SAPOrder.AMOUNT = "0.00"
            Try
                SPSOrder.AllocatedGrandTotal = Convert.ToDouble(SAPOrder.AMOUNT.Replace("-", "").Trim(), usCulture)
            Catch ex As Exception
                Utilities.LogMessages($"SAPHelper.CloneOrder - Failed to parse Allocated Grand Total. String = {SAPOrder.AMOUNT}, Culture = {CultureInfo.CurrentCulture.Name}")
                SPSOrder.AllocatedGrandTotal = Val(SAPOrder.AMOUNT.Replace("-", "").Trim())
            End Try
            'If (Not Double.TryParse(SAPOrder.AMOUNT.Trim, SPSOrder.AllocatedGrandTotal)) Then
            '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Grand Total. String = {0}, Culture = {1}", SAPOrder.AMOUNT, CultureInfo.CurrentCulture.Name))
            '    SPSOrder.AllocatedGrandTotal = Convert.ToDouble(SAPOrder.AMOUNT.Trim(), usCulture)
            'End If

            If String.IsNullOrEmpty(SAPOrder.TOTALTAXAMOUNT) Then SAPOrder.TOTALTAXAMOUNT = "0.0"
            Try
                SPSOrder.AllocatedTax = Convert.ToDouble(SAPOrder.TOTALTAXAMOUNT.Trim(), usCulture)
            Catch ex As Exception
                Utilities.LogMessages($"SAPHelper.CloneOrder - Failed to parse Allocated Tax. String = {SAPOrder.TOTALTAXAMOUNT}, Culture = {CultureInfo.CurrentCulture.Name}")
                SPSOrder.AllocatedTax = Val(SAPOrder.TOTALTAXAMOUNT.Trim())
            End Try
            'If (Not Double.TryParse(SAPOrder.TOTALTAXAMOUNT.Trim, SPSOrder.AllocatedTax)) Then
            '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Tax. String = {0}, Culture = {1}", SAPOrder.TOTALTAXAMOUNT, CultureInfo.CurrentCulture.Name))
            '    SPSOrder.AllocatedTax = Convert.ToDouble(SAPOrder.TOTALTAXAMOUNT.Trim(), usCulture)
            'End If

            If SAPOrder.TOTALRECYCLINGFEES Is Nothing Then SAPOrder.TOTALRECYCLINGFEES = "0.00"
            Try
                SPSOrder.AllocatedSpecialTax = Convert.ToDouble(SAPOrder.TOTALRECYCLINGFEES.Trim(), usCulture)
            Catch ex As Exception
                Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Special Tax. String = {0}, Culture = {1}", SAPOrder.TOTALRECYCLINGFEES, CultureInfo.CurrentCulture.Name))
                SPSOrder.AllocatedSpecialTax = Val(SAPOrder.TOTALRECYCLINGFEES.Trim())
            End Try
            'If (Not Double.TryParse(SAPOrder.TOTALRECYCLINGFEES.Trim, SPSOrder.AllocatedSpecialTax)) Then
            '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Special Tax. String = {0}, Culture = {1}", SAPOrder.TOTALRECYCLINGFEES, CultureInfo.CurrentCulture.Name))
            '    SPSOrder.AllocatedSpecialTax = Convert.ToDouble(SAPOrder.TOTALRECYCLINGFEES.Trim(), usCulture)
            'End If

            If SAPOrder.TOTALSHIPPING Is Nothing Then SAPOrder.TOTALSHIPPING = "0.00"
            If SAPOrder.HANDLINGCHARGE Is Nothing Then SAPOrder.HANDLINGCHARGE = "0.00"
            Try
                SPSOrder.AllocatedShipping = Convert.ToDouble(SAPOrder.SHIPPINGCHARGE.Trim(), usCulture) + Convert.ToDouble(SAPOrder.HANDLINGCHARGE.Trim(), usCulture)
            Catch ex As Exception
                Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Shipping. String = {0} and {1}, Culture = {2}", SAPOrder.SHIPPINGCHARGE, SAPOrder.HANDLINGCHARGE, CultureInfo.CurrentCulture.Name))
                SPSOrder.AllocatedShipping = Val(SAPOrder.SHIPPINGCHARGE.Trim()) + Val(SAPOrder.HANDLINGCHARGE.Trim())
            End Try
            'Dim tShipping, tHandling As Double
            'If (Not Double.TryParse(SAPOrder.SHIPPINGCHARGE.Trim, tShipping)) Then
            '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Shipping. String = {0} and {1}, Culture = {2}", SAPOrder.SHIPPINGCHARGE, SAPOrder.HANDLINGCHARGE, CultureInfo.CurrentCulture.Name))
            '    SPSOrder.AllocatedShipping = Convert.ToDouble(SAPOrder.SHIPPINGCHARGE.Trim(), usCulture)
            'End If

            If SAPOrder.PARTSUBTOTAL Is Nothing Then SAPOrder.PARTSUBTOTAL = "0.00"
            Try
                SPSOrder.AllocatedPartSubTotal = Convert.ToDouble(SAPOrder.PARTSUBTOTAL.Trim(), usCulture)
            Catch ex As Exception
                Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Part Subtotal. String = {0}, Culture = {1}", SAPOrder.PARTSUBTOTAL, CultureInfo.CurrentCulture.Name))
                SPSOrder.AllocatedPartSubTotal = Val(SAPOrder.PARTSUBTOTAL.Trim())
            End Try
            'If (Not Double.TryParse(SAPOrder.PARTSUBTOTAL.Trim, SPSOrder.AllocatedPartSubTotal)) Then
            '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Allocated Part Subtotal. String = {0}, Culture = {1}", SAPOrder.PARTSUBTOTAL, CultureInfo.CurrentCulture.Name))
            '    SPSOrder.AllocatedPartSubTotal = Convert.ToDouble(SAPOrder.PARTSUBTOTAL.Trim(), usCulture)
            'End If

            SPSOrder.AccountNumber = SAPOrder.ACCOUNTNUMBER
            SPSOrder.OrderDate = SAPOrder.RECEIVEDDATE
            If String.IsNullOrEmpty(SAPOrder.CHARGECARD) Then
                SAPOrder.CHARGECARD = ""
                SAPOrder.CHARGEEXP = ""
            End If

            Dim objCartType1 As New CreditCardType()
            objCartType1.Description = ""
            SPSOrder.CreditCard = New CreditCard(objCartType1)
            SPSOrder.CreditCard.CreditCardNumber = SAPOrder.CHARGECARD
            If Not SAPOrder.CHARGEEXP Is Nothing Then
                SPSOrder.CreditCard.ExpirationDate = SAPOrder.CHARGEEXP
            End If

            'Load Invoice from SAPResponse to the Invoice object
            Dim invObject As Invoice = New Invoice(SPSOrder)

            If Not SAPOrder.Invoices Is Nothing Then
                Dim objSAPInvoice As PoInquiry.Invoices ' = New PoInquiry.Invoices
                'Dim inv_line As InvoiceLineItem
                inv_line = New InvoiceLineItem(invObject) '5325
                For Each objSAPInvoice In SAPOrder.Invoices()
                    invObject = New Invoice(SPSOrder)
                    Try
                        invObject.InvoiceNumber = IIf(String.IsNullOrEmpty(objSAPInvoice.DOC_NUMBER), "", objSAPInvoice.DOC_NUMBER)
                    Catch ex As Exception
                        invObject.InvoiceNumber = Val(IIf(objSAPInvoice.DOC_NUMBER Is Nothing, "", objSAPInvoice.DOC_NUMBER))
                    End Try

                    Try
                        invObject.InvoiceDate = IIf(String.IsNullOrEmpty(objSAPInvoice.DATED), Nothing, Convert.ToDateTime(objSAPInvoice.DATED, usCulture))
                    Catch ex As Exception
                        Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Invoice Date. String = {0}, Culture = {1}", objSAPInvoice.DATED, CultureInfo.CurrentCulture.Name))
                    End Try

                    'If (Not DateTime.TryParse(objSAPInvoice.DATED, invObject.InvoiceDate)) Then
                    '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Invoice Date. String = {0}, Culture = {1}", objSAPInvoice.DATED, CultureInfo.CurrentCulture.Name))
                    '    invObject.InvoiceDate = Convert.ToDateTime(objSAPInvoice.DATED, usCulture)
                    'End If
                    invObject.TrackingNumber = IIf(String.IsNullOrEmpty(objSAPInvoice.TRACKINGNUMBER), "", objSAPInvoice.TRACKINGNUMBER) '5325

                    If String.IsNullOrEmpty(objSAPInvoice.GRANDTOTAL) Then objSAPInvoice.GRANDTOTAL = "0.00"
                    Try
                        invObject.Total = Convert.ToDouble(objSAPInvoice.GRANDTOTAL.Trim(), usCulture)
                    Catch ex As Exception
                        Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Grand Total. String = {0}, Culture = {1}", objSAPInvoice.GRANDTOTAL, CultureInfo.CurrentCulture.Name))
                        invObject.Total = Val(objSAPInvoice.GRANDTOTAL.Trim())
                    End Try
                    'If (Not Double.TryParse(objSAPInvoice.GRANDTOTAL.Trim, invObject.Total)) Then
                    '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Grand Total. String = {0}, Culture = {1}", objSAPInvoice.GRANDTOTAL, CultureInfo.CurrentCulture.Name))
                    '    invObject.Total = Convert.ToDouble(objSAPInvoice.GRANDTOTAL.Trim(), usCulture)
                    'End If
                    'Commented By Navdeep kaur on 18th May 2011 fror resolving 6396
                    'If objSAPInvoice.PARTSUBTOTAL Is Nothing Then objSAPInvoice.PARTSUBTOTAL = "0.00"
                    'invObject.PartsSubTotal = Convert.ToDouble(objSAPInvoice.PARTSUBTOTAL.Trim())
                    'If objSAPInvoice.SHIPPINGAMOUNT Is Nothing Then objSAPInvoice.SHIPPINGAMOUNT = "0.00"
                    'invObject.Shipping = Convert.ToDouble(objSAPInvoice.SHIPPINGAMOUNT.Trim())
                    'If objSAPInvoice.TAXAMOUNT Is Nothing Then objSAPInvoice.TAXAMOUNT = "0.00"
                    'invObject.Tax = Convert.ToDouble(objSAPInvoice.TAXAMOUNT.Trim())
                    'End of Comment By Navdeep kaur

                    If Not String.IsNullOrEmpty(objSAPInvoice.CARRIERCODE) Then
                        invObject.ShippingMethod = data_store.GetShippingMethodByCarrierCode(objSAPInvoice.CARRIERCODE)
                    End If

                    If String.IsNullOrEmpty(objSAPInvoice.RECYCLINGFEE) Then objSAPInvoice.RECYCLINGFEE = "0.00"
                    Try
                        invObject.SpecialTax = Convert.ToDouble(objSAPInvoice.RECYCLINGFEE.Trim(), usCulture) 'objSAPInvoice.SpecialTaxAmount
                    Catch ex As Exception
                        Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Recycling Fee. String = {0}, Culture = {1}", objSAPInvoice.RECYCLINGFEE.Trim, CultureInfo.CurrentCulture.Name))
                        invObject.SpecialTax = Val(objSAPInvoice.RECYCLINGFEE.Trim()) 'objSAPInvoice.SpecialTaxAmount
                    End Try
                    'If (Not Double.TryParse(objSAPInvoice.RECYCLINGFEE.Trim, invObject.SpecialTax)) Then
                    '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Recycling Fee. String = {0}, Culture = {1}", objSAPInvoice.RECYCLINGFEE.Trim, CultureInfo.CurrentCulture.Name))
                    '    invObject.SpecialTax = Convert.ToDouble(objSAPInvoice.RECYCLINGFEE.Trim(), usCulture)
                    'End If

                    If (Not String.IsNullOrEmpty(invObject.InvoiceNumber)) Then
                        inv_line = GetInvoiceLineItem(SAPOrder, SPSOrder, invObject.InvoiceNumber)
                        invObject.AddLineItem(inv_line)
                    End If

                    SPSOrder.AddInvoice(invObject)
                Next
            End If

            ''attempt to populate invObject details from master order details
            ''Dim inv As Invoice = SPSOrder.FindInvoice(objSAPOrderDetail.INVOICENUMBER)
            'Dim inv As Invoice '= SPSOrder.FindInvoice(objSAPOrderDetail.INVOICENUMBER)
            'If Not inv Is Nothing Then
            '    Dim inv_line As New InvoiceLineItem(inv)
            '    inv_line.PartNumberShipped = objSAPOrderDetail.PARTNUMBER
            '    inv_line.PartDescription = objSAPOrderDetail.PARTDESCRIPTION
            '    'Need to Change
            '    'inv_line.ItemPrice = objSAPOrderDetail.PartPrice
            '    inv_line.Quantity = objSAPOrderDetail.PARTQUANTITY
            '    inv.AddLineItem(inv_line)
            'End If
            'Get local order line items to obtain type information from ServicesPLUS database
            Dim orderLineCollectionInDB() As OrderLineItem
            'Dim data_store As New OrderDataManager
            Try
                orderLineCollectionInDB = data_store.GetOrderDetails(SPSOrder.OrderNumber)
            Catch ex As Exception
                Utilities.LogDebug("SAPHelper.CloneOrder - Failed while calling GetOrderDetails with Order Number: " & If(SPSOrder?.OrderNumber, "null SPSOrder"))
                Throw ex
            End Try

            'Loop thru the SAP Order deails
            Dim objSAPOrderDetail As PoInquiry.OrderDetail = New PoInquiry.OrderDetail()
            For Each objSAPOrderDetail In SAPOrder.OrderDetail()
                objSPSProduct = New Part()
                SPSOrder.POReference = IIf(objSAPOrderDetail.OUTPUT1B Is Nothing, "", objSAPOrderDetail.OUTPUT1B)
                item = New OrderLineItem(SPSOrder)
                Dim found_it As Boolean = False
                For Each l_item As OrderLineItem In orderLineCollectionInDB
                    If objSAPOrderDetail.PARTNUMBER = l_item.Product.PartNumber And Convert.ToInt16(objSAPOrderDetail.LINENUMBER) = 10 * l_item.SequenceNumber Then
                        found_it = True
                        Select Case l_item.Product.Type
                            Case Product.ProductType.Kit
                                objSPSProduct = New Kit
                            Case Product.ProductType.Part
                                objSPSProduct = New Part
                            Case Product.ProductType.ComputerBasedTraining
                                If l_item.Product.SoftwareItem Is Nothing Then
                                    objSPSProduct = New Part
                                Else
                                    objSPSProduct = New Software With {
                                        .Type = Product.ProductType.ComputerBasedTraining,
                                        .TrainingActivationCode = l_item.Product.TrainingActivationCode,
                                        .TrainingTypeCode = l_item.Product.TrainingTypeCode,
                                        .TrainingTitle = l_item.Product.TrainingTitle,
                                        .TrainingURL = l_item.Product.TrainingURL
                                    }
                                    item.Downloadable = l_item.Downloadable
                                    item.SoftwareDownloadFileName = CType(l_item.Product, Software).DownloadFileName
                                    item.SequenceNumber = l_item.SequenceNumber
                                    item.Deliverymethod = l_item.Deliverymethod
                                End If
                            Case Product.ProductType.Software, Product.ProductType.Certificate, Product.ProductType.OnlineTraining
                                objSPSProduct = New Software
                                Select Case l_item.Product.Type
                                    Case Product.ProductType.Certificate
                                        objSPSProduct.Type = Product.ProductType.Certificate
                                    Case Product.ProductType.OnlineTraining
                                        objSPSProduct.Type = Product.ProductType.OnlineTraining
                                    Case Product.ProductType.Software
                                        objSPSProduct.Type = Product.ProductType.Software
                                End Select
                                'objSPSProduct.Type = l_item.Product.Type
                                item.Downloadable = l_item.Downloadable
                                item.SoftwareDownloadFileName = CType(l_item.Product, Software).DownloadFileName
                                item.SequenceNumber = l_item.SequenceNumber
                                item.Deliverymethod = l_item.Deliverymethod
                                objSPSProduct.TrainingActivationCode = l_item.Product.TrainingActivationCode
                                objSPSProduct.TrainingTypeCode = l_item.Product.TrainingTypeCode
                                objSPSProduct.TrainingTypeCode = l_item.Product.TrainingTypeCode
                                objSPSProduct.TrainingTitle = l_item.Product.TrainingTitle
                                objSPSProduct.TrainingURL = l_item.Product.TrainingURL
                            Case Product.ProductType.ExtendedWarranty
                                objSPSProduct = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty)

                                Dim objExtWarranty As ExtendedWarrantyModel = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty)
                                If Not l_item.Product.SoftwareItem Is Nothing Then
                                    objExtWarranty.EWPurchaseDate = CType(l_item.Product.SoftwareItem, ExtendedWarrantyModel).EWPurchaseDate
                                    objExtWarranty.EWSerialNumber = CType(l_item.Product.SoftwareItem, ExtendedWarrantyModel).EWSerialNumber
                                    objExtWarranty.EWModelNumber = CType(l_item.Product.SoftwareItem, ExtendedWarrantyModel).EWModelNumber
                                Else
                                    objExtWarranty.EWPurchaseDate = CType(l_item.Product, ExtendedWarrantyModel).EWPurchaseDate
                                    objExtWarranty.EWSerialNumber = CType(l_item.Product, ExtendedWarrantyModel).EWSerialNumber
                                    objExtWarranty.EWModelNumber = CType(l_item.Product, ExtendedWarrantyModel).EWModelNumber
                                End If
                                objSPSProduct = objExtWarranty
                                item.Downloadable = True
                                item.SoftwareDownloadFileName = String.Empty
                                item.SequenceNumber = l_item.SequenceNumber
                                item.Deliverymethod = l_item.Deliverymethod
                                objSPSProduct.SoftwareItem = CType(l_item.Product, Software)
                        End Select
                        'objSPSProduct.ListPrice = ????
                        'Took List Price from database it is not coming from SAP
                        If String.IsNullOrEmpty(objSAPOrderDetail.CUSTOMERPRICE) Then objSAPOrderDetail.CUSTOMERPRICE = "0.00"
                        Try
                            objSPSProduct.ListPrice = Convert.ToDouble(objSAPOrderDetail.CUSTOMERPRICE.Trim(), usCulture)
                        Catch ex As Exception
                            Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse List Price. String = {0}, Culture = {1}", objSAPOrderDetail.CUSTOMERPRICE, CultureInfo.CurrentCulture.Name))
                            objSPSProduct.ListPrice = Val(objSAPOrderDetail.CUSTOMERPRICE.Trim())
                        End Try
                        'If (Not Double.TryParse(objSAPOrderDetail.CUSTOMERPRICE.Trim, objSPSProduct.ListPrice)) Then
                        '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse List Price. String = {0}, Culture = {1}", objSAPOrderDetail.CUSTOMERPRICE, CultureInfo.CurrentCulture.Name))
                        '    objSPSProduct.ListPrice = Convert.ToDouble(objSAPOrderDetail.CUSTOMERPRICE.Trim(), usCulture)
                        'End If
                    End If
                Next
                If Not found_it Then
                    objSPSProduct = New Part
                End If

                If String.IsNullOrEmpty(objSAPOrderDetail.CUSTOMERPRICE) Then objSAPOrderDetail.CUSTOMERPRICE = "0.00"
                Try
                    objSPSProduct.ListPrice = Convert.ToDouble(objSAPOrderDetail.CUSTOMERPRICE.Trim(), usCulture)
                Catch ex As Exception
                    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse List Price. String = {0}, Culture = {1}", objSAPOrderDetail.CUSTOMERPRICE, CultureInfo.CurrentCulture.Name))
                    objSPSProduct.ListPrice = Val(objSAPOrderDetail.CUSTOMERPRICE.Trim())
                End Try
                'If (Not Double.TryParse(objSAPOrderDetail.CUSTOMERPRICE.Trim, objSPSProduct.ListPrice)) Then
                '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse List Price. String = {0}, Culture = {1}", objSAPOrderDetail.CUSTOMERPRICE, CultureInfo.CurrentCulture.Name))
                '    objSPSProduct.ListPrice = Convert.ToDouble(objSAPOrderDetail.CUSTOMERPRICE.Trim(), usCulture)
                'End If

                objSPSProduct.Description = IIf(String.IsNullOrEmpty(objSAPOrderDetail.PARTDESCRIPTION), "", objSAPOrderDetail.PARTDESCRIPTION)

                If (String.IsNullOrEmpty(objSAPOrderDetail.BACKORDEREDQUANTITY.Trim)) Then objSAPOrderDetail.BACKORDEREDQUANTITY = "0"
                Try
                    objSPSProduct.BackOrderQuantity = Convert.ToInt16(Convert.ToDouble(objSAPOrderDetail.BACKORDEREDQUANTITY, usCulture))
                Catch ex As Exception
                    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Backorder Quantity. String = {0}, Culture = {1}", objSAPOrderDetail.BACKORDEREDQUANTITY, CultureInfo.CurrentCulture.Name))
                    objSPSProduct.BackOrderQuantity = Val(Convert.ToDouble(objSAPOrderDetail.BACKORDEREDQUANTITY))
                End Try
                'If (Not Short.TryParse(objSAPOrderDetail.BACKORDEREDQUANTITY.Trim, objSPSProduct.BackOrderQuantity)) Then
                '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Backorder Quantity. String = {0}, Culture = {1}", objSAPOrderDetail.BACKORDEREDQUANTITY, CultureInfo.CurrentCulture.Name))
                '    objSPSProduct.BackOrderQuantity = Convert.ToInt16(objSAPOrderDetail.BACKORDEREDQUANTITY.Trim(), usCulture)
                'End If

                objSPSProduct.PartNumber = IIf(objSAPOrderDetail.PARTNUMBER Is Nothing, "", objSAPOrderDetail.PARTNUMBER)

                If (String.IsNullOrEmpty(objSAPOrderDetail.RECYCLINGFEE.Trim)) Then objSAPOrderDetail.RECYCLINGFEE = "0"
                Try
                    objSPSProduct.SpecialTax = Convert.ToDouble(objSAPOrderDetail.RECYCLINGFEE.Trim, usCulture)
                Catch ex As Exception
                    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Recycling Fee. String = {0}, Culture = {1}", objSAPOrderDetail.RECYCLINGFEE, CultureInfo.CurrentCulture.Name))
                    objSPSProduct.SpecialTax = Val(IIf(objSAPOrderDetail.RECYCLINGFEE.Trim() = String.Empty, "0.00", objSAPOrderDetail.RECYCLINGFEE.Trim()))
                End Try
                'If (Not Double.TryParse(objSAPOrderDetail.RECYCLINGFEE.Trim, objSPSProduct.SpecialTax)) Then
                '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Recycling Fee. String = {0}, Culture = {1}", objSAPOrderDetail.RECYCLINGFEE, CultureInfo.CurrentCulture.Name))
                '    objSPSProduct.SpecialTax = Convert.ToDouble(objSAPOrderDetail.RECYCLINGFEE.Trim(), usCulture)
                'End If

                Select Case objSAPOrderDetail.LINESTATUS
                    Case "A"
                        item.LineStatus = OrderLineItem.Status.Allocated
                    Case "B"
                        item.LineStatus = OrderLineItem.Status.Backordered
                    Case "C"
                        item.LineStatus = OrderLineItem.Status.Confirmed
                    Case Else
                        Throw New Exception("Unknown order line item status returned from SIS")
                End Select

                item.LineStatusDate = Date.Now 'Convert.ToDateTime(objSAPOrderDetail.LINESTATUSDATE.Trim())

                Try
                    item.Quantity = Convert.ToInt32(Convert.ToDouble(objSAPOrderDetail.PARTQUANTITY.Trim(), usCulture))
                Catch ex As Exception
                    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Part Quantity. String = {0}, Culture = {1}", objSAPOrderDetail.PARTQUANTITY, CultureInfo.CurrentCulture.Name))
                    item.Quantity = Val(objSAPOrderDetail.PARTQUANTITY.Trim())
                End Try
                'If (Not Integer.TryParse(objSAPOrderDetail.PARTQUANTITY.Trim, item.Quantity)) Then
                '    Utilities.LogMessages(String.Format("SAPHelper.CloneOrder - Failed to parse Part Quantity. String = {0}, Culture = {1}", objSAPOrderDetail.PARTQUANTITY, CultureInfo.CurrentCulture.Name))
                '    item.Quantity = Convert.ToInt32(Convert.ToDouble(objSAPOrderDetail.PARTQUANTITY.Trim(), usCulture))
                '    Utilities.LogMessages("SAPHelper.CloneOrder - Part Quantity = " & item.Quantity)
                'End If

                item.LineNumber = objSAPOrderDetail.LINENUMBER
                objSPSProduct.ListPrice = objSPSProduct.ListPrice / item.Quantity
                item.Product = objSPSProduct
                item.InvoiceNumber = objSAPOrderDetail.INVOICENUMBER '5325
                SPSOrder.AddLineItem(item)
            Next

        Catch ex As Exception
            objPCILogger.EventOriginApplication = "ServicesPLUS"
            objPCILogger.EventOriginApplicationLocation = [GetType].Name
            objPCILogger.EventOriginMethod = "CloneOrder"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventType = EventType.Others
            objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
            objPCILogger.IndicationSuccessFailure = "Failure" '6524
            objPCILogger.Message = "CloneOrder Failed. " & ex.Message.ToString() '6524
            objPCILogger.PushLogToMSMQ() '6524 '6524V11
            'Throw New ServicesPLUSWebMethodException(ex.Message)
            Utilities.WrapExceptionforUI(ex)
        End Try
    End Sub

#End Region

    Private Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal policyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    'on account
    'Public Function Checkout(ByRef customer As Customer, ByRef cart As ShoppingCart, ByRef bill_to As Account, ByRef bill_to_overriden As Boolean, ByRef ship_to As SISAccount, ByRef ship_to_overriden As Boolean, ByRef shipping_method As ShippingMethod, ByRef ship_backorders_same_way As Boolean) As Order
#Region "Order Injection"


    'Public Function CreateCreditCardOrder(ByRef credit_card As CreditCard, ByVal phone As String, ByVal cart As ShoppingCart, ByVal bill_to As Sony.US.ServicesPLUS.Address, ByVal ship_to As Sony.US.ServicesPLUS.Address, ByVal shipping_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean, ByVal taxexempt As String, ByVal billing_only As Boolean) As Order

    '    Dim SAP_order As Object
    '    Dim salesOderResponse As New SalesOrder.SalesOrderResponse()
    '    Dim objSAPhelper As New SAPHelper
    '    Dim objOrderDetails As New SalesOrder.OrderDetails()
    '    Dim objshipinformation As New SalesOrder.ShipInformation()
    '    Dim objBillInformation As New SalesOrder.BillInformation()
    '    Dim objsalesRequest1 As New SalesOrder.SalesOrderRequest1()
    '    Dim objlineDetails As New SalesOrder.LineDetails()
    '    Dim arrLinedetails As SalesOrder.LineDetails()
    '    arrLinedetails = New SalesOrder.LineDetails(cart.ShoppingCartItems.Count - 1) {}

    '    Dim arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
    '    arrOrderLineItem = New Sony.US.ServicesPLUS.Core.OrderLineItem(cart.ShoppingCartItems.Count - 1) {}
    '    Try
    '        SAP_order = New ServicesPLUS.Order
    '        objBillInformation.BillToName1 = bill_to.Attn
    '        objBillInformation.City = bill_to.City

    '        objBillInformation.District = bill_to.Address3
    '        objBillInformation.Email = cart.Customer.EmailAddress

    '        objBillInformation.SoldTo = bill_to.SapAccountNumber
    '        ''TODo:For testing
    '        objBillInformation.SoldTo = "1001510"

    '        objBillInformation.State = bill_to.State
    '        ''HardCoded
    '        objBillInformation.State = "CA"

    '        objBillInformation.Street = bill_to.Address1
    '        objBillInformation.Street2 = bill_to.Address2
    '        objBillInformation.ZipCode = bill_to.Zip
    '        ''HardCode
    '        objBillInformation.ZipCode = "95112"

    '        objOrderDetails.BillInformation = objBillInformation
    '        ''Credit Card Informaion
    '        'objOrderDetails.CardName = bill_to.Name
    '        'objOrderDetails.CardNumber = credit_card.CreditCardNumber
    '        'objOrderDetails.CardType = credit_card.Type.CreditCardTypeCode
    '        'objOrderDetails.CardVerificationValue = credit_card.CSCCode
    '        'objOrderDetails.ValidTo = Convert.ToDateTime(credit_card.ExpirationDate).ToString("MMyy")
    '        'Order Details

    '        objOrderDetails.CustomerPONumber = cart.PurchaseOrderNumber

    '        objOrderDetails.GroupCode = shipping_method.SISPickGroupCode
    '        objOrderDetails.OrderEntryPerson = cart.Customer.SIAMIdentity
    '        ''TODO:DropShip Indicator
    '        objOrderDetails.ReasonCode = "Y"
    '        ''TODO :D
    '        objOrderDetails.TaxExempt = taxexempt
    '        ''TODO :YourReference
    '        objOrderDetails.YourReference = System.Guid.NewGuid().ToString()

    '        ''objlineDetails
    '        Dim i As Integer = 0
    '        For Each item As ShoppingCartItem In cart.ShoppingCartItems
    '            objlineDetails = New SalesOrder.LineDetails()
    '            objlineDetails.OrderQuantity = item.Quantity.ToString
    '            objlineDetails.PartNumber = item.Product.PartNumber
    '            objlineDetails.Carrier = shipping_method.SISCarrierCode
    '            arrLinedetails(i) = objlineDetails
    '            i = i + 1
    '        Next
    '        objOrderDetails.LineDetails = arrLinedetails


    '        'Shiping Information
    '        objshipinformation.BusExt = ship_to.PhoneExt
    '        objshipinformation.City = ship_to.City

    '        objshipinformation.District = ship_to.Address3
    '        objshipinformation.Fax = cart.Customer.FaxNumber
    '        objshipinformation.FirstTelephoneNumber = ship_to.PhoneNumber
    '        ''TODO:SAP ShipTO
    '        objshipinformation.ShipTo = ship_to.SapAccountNumber
    '        objshipinformation.ShipToName1 = ship_to.Name
    '        objshipinformation.ShipToName3 = ship_to.Attn
    '        objshipinformation.State = ship_to.State
    '        ''HardCoded
    '        objshipinformation.State = "CA"

    '        objshipinformation.Street = ship_to.Address1
    '        objshipinformation.Street2 = ship_to.Address2
    '        objshipinformation.ZipCode = ship_to.Zip
    '        ''HardCoded
    '        objshipinformation.ZipCode = "95112"
    '        objOrderDetails.ShipInformation = objshipinformation
    '        ''Simulation X is for simucatin
    '        objOrderDetails.Input3 = "X"
    '        objsalesRequest1.OrderDetails = objOrderDetails
    '        ''TO Hard Coded
    '        objOrderDetails.LineDetails(0).PartNumber = "A1052340A"
    '        objOrderDetails.LineDetails(1).PartNumber = "A1052340A"
    '        salesOderResponse = objSAPhelper.SalesOrderInjection(objOrderDetails)
    '        Me.CloneSAPOrder(salesOderResponse.SalesOrderResponse1, SAP_order, cart, bill_to, ship_to, credit_card, shipping_method)
    '        'Console.WriteLine("Order Created on SIS - order number: " + sis_new_order.OrderNumber)
    '    Catch ex As Exception
    '        'Console.WriteLine("CreateAccountOrder Failed on SIS")
    '        'log
    '        Throw ex
    '    End Try
    '    Return SAP_order
    'End Function

    Private Sub CloneSAPOrder(ByVal so As OrderInjection.SalesOrderResponse1, ByRef lo As Order, ByRef cart As ShoppingCart, ByVal bill_to As Core.Address, ByVal ship_to As Core.Address, ByRef credit_card As CreditCard, ByVal shipping_method As ShippingMethod)
        Dim objPCILogger As PCILogger = New PCILogger() '6524
        Try
            '6524 start
            objPCILogger.EventOriginApplication = "ServicesPLUS"
            objPCILogger.EventOriginApplicationLocation = [GetType].Name
            objPCILogger.EventOriginMethod = "CloneSAPOrder"
            objPCILogger.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            objPCILogger.OperationalUser = Environment.UserName
            objPCILogger.EventType = EventType.Others
            objPCILogger.EventDateTime = Date.Now.ToLongTimeString()
            objPCILogger.HTTPRequestObjectValues = objPCILogger.GetRequestContextValue()
            objPCILogger.IndicationSuccessFailure = "Success"
            objPCILogger.Message = "CloneSAPOrder Success."
            '6524 end

            lo.MessageText = so.MessageText
            lo.MessageType = so.Messagetype

            If so.Messagetype = "S" Then
                Dim arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
                arrOrderLineItem = New Sony.US.ServicesPLUS.Core.OrderLineItem(so.OrderConfirmation.LineConfirmation.Count - 1) {}

                lo.OrderStatus = so.OrderConfirmation.OrderStatus

                lo.OrderNumber = so.OrderConfirmation.SalesDocument

                lo.Paymentcardresulttext = so.OrderConfirmation.PaymentCardResultText
                lo.OrderDate = Date.Now
                lo.ShippingMethod = shipping_method
                lo.VerisgnAuthorizationCode = so.OrderConfirmation.PaymentCardAuthorizationNumber
                lo.AllocatedGrandTotal = so.OrderConfirmation.TotalOrderAmount
                lo.AllocatedShipping = so.OrderConfirmation.TotalShippingHandling
                lo.AllocatedSpecialTax = so.OrderConfirmation.TotalRecyclingFee
                lo.AllocatedPartSubTotal = so.OrderConfirmation.TotalPartSubTotal
                lo.AllocatedTax = so.OrderConfirmation.TotalTaxAmount

                'lo.YourReference = cart.Customer.objOrderDetails.YourReference
                lo.Customer = New Sony.US.ServicesPLUS.Core.Customer
                lo.Customer.SequenceNumber = IIf(Not cart.Customer Is Nothing, cart.Customer.SequenceNumber, 0)
                Dim iLIne As Integer = 0
                For Each i As OrderInjection.LineConfirmation In so.OrderConfirmation.LineConfirmation
                    Dim objLineItem As New OrderLineItem(lo)

                    objLineItem.Quantity = IIf(Not String.IsNullOrEmpty(i.OrderQty), i.OrderQty, 0)
                    objLineItem.ConfirmedQty = IIf(Not String.IsNullOrEmpty(i.ConfirmedQty), i.ConfirmedQty, 0)
                    objLineItem.Product = GetProduct(objLineItem) ''New Sony.US.ServicesPLUS.Core.Part
                    objLineItem.Product.PartNumber = i.PartOrdered
                    objLineItem.Product.Description = i.ShortTextItem
                    objLineItem.Product.ListPrice = i.PartPrice
                    objLineItem.LineNumber = i.LineNumber
                    arrOrderLineItem(iLIne) = objLineItem
                    iLIne = iLIne + 1
                Next


                lo.LineItems1 = arrOrderLineItem
                'Order.SAPBillToAccountNumber = bill_to.AccountNumber
                lo.ContainsDownloadAndShipItem = cart.ContainsDownloadAndShippingItems
                lo.BillTo = bill_to
                'Order.AccountNumber = bill_to.AccountNumber
                lo.ShipTo = ship_to
                lo.POReference = cart.PurchaseOrderNumber
                lo.Customer.CustomerID = cart.Customer.CustomerID
                If (Not credit_card Is Nothing) Then
                    lo.CreditCard = credit_card
                Else
                    lo.CreditCard = Nothing
                End If

            Else
                lo.MessageText = "Exception in connecting to SAP Webmethods"
                lo.MessageType = "E"
            End If

        Catch ex As Exception
            objPCILogger.IndicationSuccessFailure = "Failure" '6524
            objPCILogger.Message = "CloneSAPOrder Failed. " & ex.Message.ToString() '6524
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            objPCILogger.PushLogToMSMQ() '6524
        End Try

    End Sub

    Public Function GetProduct(ByVal l_item As Sony.US.ServicesPLUS.Core.OrderLineItem) As Product
        Dim pciLog As New PCILogger()
        Try
            pciLog.EventOriginApplication = "ServicesPLUS"
            pciLog.EventOriginApplicationLocation = [GetType].Name
            pciLog.EventOriginMethod = "GetProduct"
            pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            pciLog.OperationalUser = Environment.UserName
            pciLog.EventType = EventType.View
            pciLog.EventDateTime = Date.Now.ToLongTimeString()
            pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
            pciLog.IndicationSuccessFailure = "Success"
            pciLog.Message = "GetProduct Success."

            Dim product As Product = Nothing
            If l_item.Product IsNot Nothing Then
                Select Case l_item.Product.Type
                    Case Product.ProductType.Kit
                        product = New Kit
                    Case Product.ProductType.Part
                        product = New Part
                    Case Product.ProductType.ComputerBasedTraining
                        If l_item.Product.SoftwareItem Is Nothing Then
                            product = New Part
                        Else
                            product = New Software With {
                                .Type = Product.ProductType.ComputerBasedTraining,
                                .TrainingActivationCode = l_item.Product.TrainingActivationCode,
                                .TrainingTypeCode = l_item.Product.TrainingTypeCode,
                                .TrainingTitle = l_item.Product.TrainingTitle,
                                .TrainingURL = l_item.Product.TrainingURL
                            }

                        End If
                    Case Product.ProductType.Software, Product.ProductType.Certificate, Product.ProductType.OnlineTraining
                        product = New Software With {
                            .TrainingActivationCode = l_item.Product.TrainingActivationCode,
                            .TrainingTypeCode = l_item.Product.TrainingTypeCode,
                            .TrainingTitle = l_item.Product.TrainingTitle,
                            .TrainingURL = l_item.Product.TrainingURL
                        }
                        Select Case l_item.Product.Type
                            Case Product.ProductType.Certificate
                                product.Type = Product.ProductType.Certificate
                            Case Product.ProductType.OnlineTraining
                                product.Type = Product.ProductType.OnlineTraining
                            Case Product.ProductType.Software
                                product.Type = Product.ProductType.Software
                        End Select
                    Case Product.ProductType.ExtendedWarranty
                        product = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty)

                        Dim objExtWarranty As New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty)
                        If l_item.Product.SoftwareItem IsNot Nothing Then
                            Dim software = CType(l_item.Product.SoftwareItem, ExtendedWarrantyModel)
                            objExtWarranty.EWPurchaseDate = software.EWPurchaseDate
                            objExtWarranty.EWSerialNumber = software.EWSerialNumber
                            objExtWarranty.EWModelNumber = software.EWModelNumber
                        Else
                            Dim software = CType(l_item.Product, ExtendedWarrantyModel)
                            objExtWarranty.EWPurchaseDate = software.EWPurchaseDate
                            objExtWarranty.EWSerialNumber = software.EWSerialNumber
                            objExtWarranty.EWModelNumber = software.EWModelNumber
                        End If

                        product = objExtWarranty 'New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
                End Select
            Else
                product = New Part
            End If

            Return product
        Catch ex As Exception
            pciLog.IndicationSuccessFailure = "Failure"
            pciLog.Message = "GetProduct Failed. " & ex.Message.ToString()
            Throw New ServicesPLUSWebMethodException(ex.Message)
        Finally
            pciLog.PushLogToMSMQ()
        End Try

    End Function

    Public Function SalesOrderInjection(ByVal objOrderDetails As OrderInjection.OrderDetails, ByRef objGlobalData As GlobalData) As OrderInjection.SalesOrderResponse
        Dim sFeature As String = "scrollbars=1,resizable=0,width=744,height=754,left=0,top=0"
        Dim pciLog As New PCILogger()
        Try
            pciLog.EventOriginApplication = "ServicesPLUS"
            pciLog.EventOriginApplicationLocation = [GetType].Name
            pciLog.EventOriginMethod = "SalesOrderInjection"
            pciLog.EventOriginServerMachineName = HttpContext.Current.Server.MachineName
            pciLog.OperationalUser = Environment.UserName
            pciLog.EventType = EventType.Add
            pciLog.EventDateTime = Date.Now.ToLongTimeString()
            pciLog.HTTPRequestObjectValues = pciLog.GetRequestContextValue()
            pciLog.IndicationSuccessFailure = "Success"
            pciLog.Message = "SalesOrderInjection Success."
            pciLog.EmailAddress = objOrderDetails.BillInformation.Email
            'objPCILogger.CustomerID = customer.CustomerID
            'objPCILogger.CustomerID_SequenceNumber = customer.SequenceNumber
            'objPCILogger.SIAM_ID = customer.SIAMIdentity
            'objPCILogger.LDAP_ID = customer.LdapID

            ' Update the provided Order Details
            If objGlobalData.IsCanada Then
                objOrderDetails.ShipInformation.Country = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.CA.Substring(0, 2)
            Else
                objOrderDetails.ShipInformation.Country = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US.Substring(0, 2)
            End If
            objOrderDetails.CustomerPOType = ConfigurationData.Environment.SAPWebMethods.POType

            Dim objsalesRequest1 As New OrderInjection.SalesOrderRequest1 With {
                .OrderDetails = objOrderDetails,
                .SalesOrganization = objGlobalData.SalesOrganization,
                .DistributionChannel = objGlobalData.DistributionChannel,
                .Division = objGlobalData.Division
            }
            If objGlobalData.IsFrench Then
                objsalesRequest1.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.CA
            Else
                objsalesRequest1.Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
            End If

            Dim objSales As New OrderInjection.createSalesOrderInjection With {
                .Credentials = GetSAPCredential(),
                .Url = ConfigurationData.Environment.SAPWebMethods.SalesOrder
            }

            Dim Cert = X509Certificate.CreateFromCertFile(ConfigurationData.Environment.SAPWebMethods.SalesOrderCertificatePath)
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
            objSales.ClientCertificates.Add(Cert)

            objsalesRequest1.Source = ConfigurationData.Environment.SAPWebMethods.Source
            Dim salesOrderRequest As New OrderInjection.SalesOrderRequest With {
                .SalesOrderRequest1 = objsalesRequest1
            }
            Dim objRequest As String = SerialiseObject(salesOrderRequest)
            Dim objResponseSet As New OrderInjection.SalesOrderResponse
            objResponseSet = objSales.receiveSalesOrderNotification(salesOrderRequest)

            Return objResponseSet
        Catch ex As Exception
            pciLog.IndicationSuccessFailure = "Failure"
            pciLog.Message = "SalesOrderInjection Failed. " & ex.Message.ToString()
            Throw ex
        Finally
            pciLog.PushLogToMSMQ()
        End Try
    End Function

    Public Function SerialiseObject(ByVal objToserilize As Object) As String
        Try
            Dim xmlStream As New XmlSerializer(objToserilize.GetType())
            Dim objStream As New MemoryStream()
            xmlStream.Serialize(objStream, objToserilize)
            If objStream Is Nothing Or objStream.Length = 0 Then
                Return String.Empty
            End If

            objStream.Flush()
            objStream.Position = 0
            Dim sr As New StreamReader(objStream)

            Return sr.ReadToEnd()
        Catch ex As Exception
            Throw New ServicesPLUSWebMethodException(ex.Message)
        End Try
    End Function
#End Region

End Class
