'Imports System.Web.Mail    ' This namespace is obsolete. Replaced with System.Net.Mail
Imports System.Net.Mail
Imports System.Text
Imports System.Web.UI.WebControls
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.siam
Imports Sony.US.SIAMUtilities

Public Class CustomerManager

    Private Const ClassName As String = "CustomerManager"
    Private serviceEnvironment As String
    Private Const accountCount As Integer = 10


    Public Sub New()
        serviceEnvironment = ConfigurationData.GeneralSettings.Environment.ToLower()
    End Sub

    ''' <summary> A generally useless function. All it does is "Return New Customer". </summary>
    Public Function CreateCustomer() As Customer
        Return New Customer
    End Function

    Public Function CheckPasswordValidity(ByVal newPassword As String, ByVal emailAddress As String, ByRef errorPasswordNew As Label, ByVal UserType As String) As Boolean '6771
        Dim isValid As Boolean = False
        Dim isNotAlphaNumeric As New RegularExpressions.Regex("((?<=(\w*\d+))(?=([a-zA-Z]+\w*)))|((?<=(\w*[a-zA-Z]+))(?=(\d+\w*)))") '("\w*[a-zA-Z]+\w*\d+\w*")
        Dim isSpecialChar As New RegularExpressions.Regex("\W+")
        Dim email As String = emailAddress.Trim()
        Dim minLen As Integer, maxLen As Integer

        email = email.Substring(0, email.IndexOf("@"))
        Dim objSecurityAd As New SecurityAdministrator
        Dim strErrorMessage As String = ""
        isValid = objSecurityAd.ValidateB2Bpassword(newPassword, emailAddress, strErrorMessage)

        errorPasswordNew.Text = strErrorMessage '6771
        '<Satyam>Read the minimum and maximum length allowed for passwords, from Config file
        'Dim handler As New ConfigHandler("SOFTWARE\SERVICESPLUS")
        'Dim config_settings As Hashtable
        'config_settings = handler.GetConfigSettings("//item[@name='SecurityManagement']")
        'minLen = Integer.Parse("0" + config_settings.Item("MinPwdLength").ToString())
        'maxLen = Integer.Parse("0" + config_settings.Item("MaxPwdLength").ToString())
        minLen = Integer.Parse("0" + ConfigurationData.GeneralSettings.SecurityManagement.MinPwdLength)
        'If UserType = "AccountHolder" Then'6668
        If UserType = "A" Or UserType = "P" Then '6668
            maxLen = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthAccount
        Else
            maxLen = ConfigurationData.GeneralSettings.SecurityManagement.MaxPwdLengthNonAccount
        End If

        '</Satyam>

        If email = newPassword.Trim() Then
            isValid = False
            errorPasswordNew.Text = "Your email address and password cannot be the same."
        ElseIf newPassword.Length < minLen Or newPassword.Length > maxLen Then
            isValid = False
            '<Satyam>Show dynamic password length restriction message
            errorPasswordNew.Text = "Your password must be " + minLen.ToString() + " to " + maxLen.ToString() + " characters long."
            '<Satyam>
        ElseIf Not isNotAlphaNumeric.IsMatch(newPassword) Then
            isValid = False
            errorPasswordNew.Text = "Your password must contain both alphabetic and numeric characters."
        ElseIf isSpecialChar.IsMatch(newPassword) Then
            isValid = False
            errorPasswordNew.Text = "Your password must not contain special characters."
        ElseIf Not objSecurityAd.ValidateB2Bpassword(newPassword, emailAddress, strErrorMessage) Then
            isValid = False
            errorPasswordNew.Text = strErrorMessage
        Else
            isValid = True
        End If

        Return isValid
    End Function

    Public Sub RegisterCustomer(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        CheckRequiredFields(customer)
        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(customer, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            'Logger.RaiseTrace(ClassName, "RegisterCustomer: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit RegisterCustomer", TraceLevel.Verbose)
    End Sub

    Public Function DeleteCustomerRecord(ByVal customerId As String) As Boolean
        Try
            Dim deleteCustomer As New CustomerDataManager
            Dim result As Boolean = deleteCustomer.DeleteCustomerRecord(customerId)

            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetUserList(ByVal userType As String) As DataSet
        Dim data As New CustomerDataManager
        Try
            Return data.GetUserlist(userType)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "RegisterCustomer: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit RegisterCustomer", TraceLevel.Verbose)
    End Function

    Private Sub CheckRequiredFields(ByVal customer As Customer)
        If (String.IsNullOrEmpty(customer.LastName)) Then Throw New Exception("Last Name is required")
        If (String.IsNullOrEmpty(customer.FirstName)) Then Throw New Exception("First Name is required")
        If (String.IsNullOrEmpty(customer.EmailAddress)) Then Throw New Exception("Email Address is required")
        If (String.IsNullOrEmpty(customer.CompanyName)) Then Throw New Exception("Company Name is required")
        If (String.IsNullOrEmpty(customer.Address.Line1)) Then Throw New Exception("Address line 1 is required")
        If (String.IsNullOrEmpty(customer.Address.City)) Then Throw New Exception("City is required")
        If (String.IsNullOrEmpty(customer.Address.State)) Then Throw New Exception("State is required")
        If (String.IsNullOrEmpty(customer.Address.PostalCode)) Then Throw New Exception("Postal Code is required")
    End Sub

    Public Overloads Function GetCustomer(ByVal siam_identity As String) As Customer
        Dim data_store As New CustomerDataManager
        Dim obj As Object

        Try
            obj = data_store.Restore(siam_identity)
        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Overloads Function GetCustomer(ByVal userID As String, ByVal email As String, ByVal firstName As String, ByVal lastName As String) As Object
        Dim data_store As New CustomerDataManager
        Dim obj As Object

        Try
            obj = data_store.Restore(userID, email, firstName, lastName)
        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Function GetCustomerPhoneExtension(ByVal siam_identity As String) As String
        Dim data_store As New CustomerDataManager
        Dim obj As String
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            obj = data_store.GetCustomerPhoneExtension(txn_context, siam_identity)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Function GetSiamIdByLdapId(ByVal xLDAPID As String) As String
        Dim data_store As New CustomerDataManager
        Dim obj As String
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            obj = data_store.GetSiamIdByLdapId(txn_context, xLDAPID)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Function GetCustomerSubscriptionID(ByVal siam_identity As String) As String
        Dim data_store As New CustomerDataManager
        Dim strSubscriptionID As String
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            strSubscriptionID = data_store.GetCustomerSubscriptionID(txn_context, siam_identity)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return strSubscriptionID
    End Function

    Public Function GetINACTIVECUSTOMERDATA(ByVal _EmailAddress As String) As Long
        Dim data_store As New CustomerDataManager
        Dim customerId As Long = 0
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            customerId = data_store.GetInactiveCustomerData(txn_context, _EmailAddress)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
        Return customerId
    End Function

    Public Function ValidateCustomerData(ByVal _EmailAddress As String) As Short
        Dim data_store As New CustomerDataManager
        Dim retVal As Short = 0
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            retVal = data_store.GetValidateCustomerData(txn_context, _EmailAddress)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
        Return retVal
    End Function

    Public Function IsValidSubscriptionID(ByVal custID As String, ByVal strSubscriptionID As String) As Short
        Dim data_store As New CustomerDataManager
        Dim resultValidID As Short = 0
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            resultValidID = data_store.IsValidSubscription(txn_context, custID, strSubscriptionID)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return resultValidID
    End Function

    Public Function IsValidSubscriptionID(ByVal custID As String, ByVal strSubscriptionID As String, ByVal strPassword As String) As Boolean
        Dim data_store As New CustomerDataManager
        Dim IsValidID As Boolean
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            IsValidID = data_store.IsValidSubscription(txn_context, custID, strPassword, strSubscriptionID)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return IsValidID
    End Function

    ''' <summary>
    ''' This overloaded method validates the subscription for users who have a subscription ID stored 
    '''   in their customer profile previously
    ''' </summary>
    Public Function IsValidSubscriptionID(ByVal custID As String) As Boolean
        Dim data_store As New CustomerDataManager
        Dim IsValidID As Boolean
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            IsValidID = data_store.IsValidSubscription(txn_context, custID)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return IsValidID
    End Function

    Public Function GetDateRegistered(ByVal siam_identity As String) As String
        Dim data_store As New CustomerDataManager
        Dim dateReg As String
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            dateReg = data_store.GetCustomerDateReg(txn_context, siam_identity)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return dateReg
    End Function

    Public Sub UpdateCustomerSubscription(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.UpdateCustomerSubscription(customer, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub AddCustomerAccount(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.AddCustomerAccount(customer, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCustomerSubscriptionUserType(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.UpdateCustomerSubscriptionUserType(customer, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateLdapid(ByRef ldapid As String, ByVal customerid As Integer)

        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.UpdateLdapid(customerid, ldapid, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCustomer(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(customer, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCustomerMarketingInterest(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreMarketingInterest(customer, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetAccountData(ByVal userID As String) As ArrayList
        Dim data_store As New CustomerDataManager
        Dim accountList As New ArrayList

        Try
            accountList = data_store.GetAccountData(userID)
        Catch ex As Exception
            Throw ex
        End Try

        Return accountList
    End Function

    Public Sub UpdateCustomerBillToAccounts(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreBillToAccounts(customer, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub UpdateCustomerCreditCard(ByRef customer As Customer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreCreditCard(customer, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub DeleteCustomer(ByRef customer As Customer)
        customer.Action = CoreObject.ActionType.DELETE
        UpdateCustomer(customer)
    End Sub

    Public Function ValidateCustomerSAPAccount(ByRef customer As Customer, ByRef account As Account, ByRef objGlobalData As GlobalData, Optional ByRef blUpdate As Boolean = True, Optional ByRef blSaveValidAcct As Boolean = False) As Boolean
        Dim helper As New SAPHelper()
        Dim legacy_ship_to_accounts(accountCount) As Object
        Dim rc As Boolean = True
        Dim aAccounts As String() = {account.AccountNumber}

        Try
            legacy_ship_to_accounts = helper.GetSAPayerAccounts("", aAccounts, objGlobalData)   ' Fetches accounts, not customer detail.
        Catch ex As Exception
            rc = False
        End Try

        If rc Then
            customer.SAPBillToAccounts = legacy_ship_to_accounts    ' Attaches the accounts, still missing Account.Customer detail.

            'Save only valid accound from admin module 
            For Each objAcct As Account In legacy_ship_to_accounts
                objAcct.Customer = customer     ' ASleight - Fix for missing Customer detail in accounts.
                If Not objAcct.Validated Then customer.RemoveAccount(objAcct)
            Next

            If blUpdate Then
                ' ASleight - This section is just bad. We parse, join, then split and loop twice. Could easily be improved.
                If blSaveValidAcct Then
                    Dim strInvalidAcct As String = String.Empty

                    For Each objAccount As Account In customer.SAPBillToAccounts
                        If Not objAccount.Validated Then
                            If strInvalidAcct.Length > 0 Then
                                strInvalidAcct &= "," & objAccount.AccountNumber
                            Else
                                strInvalidAcct = objAccount.AccountNumber
                            End If
                        End If
                    Next

                    'Remove the invalid account 
                    If strInvalidAcct.Length > 0 Then
                        For Each strData As String In strInvalidAcct.Split(",")
                            For Each objAccount As Account In customer.SAPBillToAccounts
                                If (objAccount.AccountNumber = strData) Then
                                    customer.RemoveAccount(objAccount)
                                    Exit For
                                End If
                            Next
                        Next
                    End If

                End If
                UpdateCustomerBillToAccounts(customer)
            End If
        End If

        Return rc
    End Function

    Public Function GetAllNonValidatedAccountHolders() As Customer()
        Dim data_store As New CustomerDataManager
        Dim customers() As Customer

        Try
            customers = data_store.GetNonValidatedAccountHolders()
        Catch ex As Exception
            Throw ex
        End Try

        Return customers
    End Function

    Public Function SearchCustomerSAPAccount(ByVal companyname As String) As Object() 'SAPPayer()
        Dim helper As New SAPHelper
        Dim objAccounts As String() = {""}

        Try
            Return helper.GetSAPayerAccounts(companyname, objAccounts)
        Catch ex As Exception
            Console.WriteLine(ex.Message())
        End Try

        Return Nothing
    End Function

    Public Sub SendForgotPasswordEmail(ByVal customer As Customer, ByVal userid As String, ByVal one_time_password As String)
        Dim mailObj As MailMessage
        Dim EmailBody As New StringBuilder(1500)

        Dim servicesplus_link As String = ConfigurationData.GeneralSettings.URL.Servicesplus_Link
        Dim promotions_link As String = ConfigurationData.GeneralSettings.URL.Promotions_Link
        Dim find_parts_link As String = ConfigurationData.GeneralSettings.URL.Find_Parts_Link
        Dim find_software_link As String = ConfigurationData.GeneralSettings.URL.Find_Software_Link
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link
        Dim customer_service_link As String = ConfigurationData.GeneralSettings.URL.Customer_Service_Link

        EmailBody.Append("Dear " + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
        EmailBody.Append("Your ServicesPLUS(sm) password has been changed to a single use password." + vbCrLf + vbCrLf)
        EmailBody.Append("Please go to " + servicesplus_link + " and sign in as follows:" + vbCrLf + vbCrLf)
        EmailBody.Append("Login: " + userid + vbCrLf)
        EmailBody.Append("Password: " + one_time_password + vbCrLf + vbCrLf)
        EmailBody.Append("To ensure security of your login, you will need to change your password immediately ")
        EmailBody.Append("after logging in." + vbCrLf + vbCrLf)
        EmailBody.Append("Thank you for using Sony's ServicesPLUS(sm) site.  Please call 800-538-7550 if you  ")
        EmailBody.Append("experience any difficulties." + vbCrLf + vbCrLf)
        EmailBody.Append("Sincerely," + vbCrLf)
        EmailBody.Append("The ServicesPLUS(sm) Team" + vbCrLf + vbCrLf)
        EmailBody.Append("Copyright 2008 Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        EmailBody.Append("")
        EmailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)
        EmailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        EmailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        EmailBody.Append("[Customer Service] " + customer_service_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, customer.EmailAddress) With {
            .Subject = $"ServicesPLUS(sm): Important member information you requested.",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendWelcomeEmail(ByVal customer As Customer)
        Dim mailObj As MailMessage
        Dim emailBody As New StringBuilder(1500)
        Dim privacy_policy_link As String = ConfigurationData.GeneralSettings.URL.Privacy_Policy_Link
        Dim terms_and_conditions_link As String = ConfigurationData.GeneralSettings.URL.Terms_And_Conditions_Link
        Dim contact_us_link As String = ConfigurationData.GeneralSettings.URL.Contact_Us_Link

        emailBody.Append("Dear " + customer.FirstName + " " + customer.LastName + "," + vbCrLf + vbCrLf)
        emailBody.Append("Thank you for registering with Sony's ServicesPLUS(sm) system for support of your ")
        emailBody.Append("Sony professional products. Please bookmark the following link for future ")
        emailBody.Append("access to Sony Professional Services: " + vbCrLf + vbCrLf)
        emailBody.Append("www.sony.com/servicesplus" + vbCrLf + vbCrLf)

        If customer.Tax_Exempt Then
            emailBody.Append("If you would like to establish tax exemption for use with credit card ")
            emailBody.Append("purchases, please call 1-800-538-7550 to apply." + vbCrLf + vbCrLf)
            emailBody.Append("Alternately, you can print and complete the blanket sales tax exemption ")
            emailBody.Append("certificate at https://www.servicesplus.sel.sony.com/pdfs/TaxExemptApp.pdf and fax ")
            emailBody.Append("it to Sony Customer Services at 1-816-880-6854. You will be contacted regarding ")
            emailBody.Append("the status of your application. " + vbCrLf + vbCrLf)
            emailBody.Append("NOTE: You MUST include this Customer ID on the form: " + customer.SIAMIdentity + vbCrLf + vbCrLf)
        Else
            emailBody.Append("If you have any questions, please contact our customer service group at 1-800-538-7550." + vbCrLf + vbCrLf)
        End If

        emailBody.Append("Sincerely," + vbCrLf)
        emailBody.Append("The ServicesPLUS(sm) Team" + vbCrLf + vbCrLf)
        emailBody.Append("Copyright 2017 Sony Electronics Inc. All Rights Reserved" + vbCrLf + vbCrLf)
        emailBody.Append("[Contact Us] " + contact_us_link + vbCrLf)
        emailBody.Append("[Terms and Conditions] " + terms_and_conditions_link + vbCrLf)
        emailBody.Append("[Privacy Policy] " + privacy_policy_link + vbCrLf)

        mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, customer.EmailAddress) With {
            .Subject = "ServicesPLUS(sm) - Thank you for registering",
            .Body = emailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendRegisterWelcomeEmail(ByVal emailAddress As String, ByVal MailBody As String, ByVal Subject As String)
        Try
            Dim mailObj As New MailMessage(ConfigurationData.Environment.Email.General.From, emailAddress) With {
                .Subject = Subject,
                .Body = MailBody.ToString(),
                .IsBodyHtml = False,
                .Priority = MailPriority.Normal
            }

            Using mailClient As New SmtpClient()
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
                mailClient.Port = ConfigurationData.Environment.SMTP.Port
                mailClient.Send(mailObj)
            End Using
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "Error in SendWelcomeEmail:" + ex.Message, TraceLevel.Verbose)
        End Try
    End Sub

    Public Sub SendWelcomeEmailAccountHolder(ByVal customer As Customer)
        If String.IsNullOrWhiteSpace(SystemVariables.RunningApplicationPath) Then Throw New ApplicationException("Template Path Not Set...")

        Try
            Dim filePath = SystemVariables.RunningApplicationPath & "\EmailTemplates\" & ConfigurationData.GeneralSettings.Template.B2BPendingTemplate
            Dim fStream As New IO.FileStream(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim buffer(fStream.Length) As Byte
            Dim sb As New StringBuilder()
            Dim x As Long
            Dim mailObj As MailMessage

            fStream.Read(buffer, 0, buffer.Length)
            fStream.Close()
            For x = 0 To buffer.GetUpperBound(0) - 1
                sb.Append(Convert.ToChar(buffer(x)))
            Next x
            sb.Replace("~FirstName~", customer.FirstName)
            sb.Replace("~LastName~", customer.LastName)

            mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, customer.EmailAddress) With {
                .Subject = "ServicesPLUS(sm) - Thank you for registering",
                .Body = sb.ToString(),
                .IsBodyHtml = True,
                .Priority = MailPriority.Normal
            }
            Using mailClient As New SmtpClient()
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
                mailClient.Port = ConfigurationData.Environment.SMTP.Port
                mailClient.Send(mailObj)
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SendWelcomeEmailCBSAccountHolder(ByVal customer As Customer)
        If String.IsNullOrWhiteSpace(SystemVariables.RunningApplicationPath) Then Throw New ApplicationException("Template Path Not Set...")

        Try
            Dim filePath = SystemVariables.RunningApplicationPath & "\EmailTemplates\" & ConfigurationData.GeneralSettings.Template.CBSRegisterTemplate
            Dim fStream As New IO.FileStream(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            Dim buffer(fStream.Length) As Byte
            Dim sb As New StringBuilder()
            Dim x As Long
            Dim mailObj As MailMessage

            fStream.Read(buffer, 0, buffer.Length)
            fStream.Close()
            For x = 0 To buffer.GetUpperBound(0) - 1
                sb.Append(Convert.ToChar(buffer(x)))
            Next x
            sb.Replace("~FirstName~", customer.FirstName)
            sb.Replace("~LastName~", customer.LastName)

            mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, customer.EmailAddress) With {
                .Subject = $"ServicesPLUS(sm) - Thank you for registering",
                .Body = sb.ToString(),
                .IsBodyHtml = True,
                .Priority = MailPriority.Normal
            }

            Using mailClient As New SmtpClient()
                mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
                mailClient.Port = ConfigurationData.Environment.SMTP.Port
                mailClient.Send(mailObj)
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetAllMarketingInterests(ByRef Language_id As String) As MarketingInterest()
        Dim data_store As New CustomerDataManager
        Dim obj() As MarketingInterest
        Dim temp_language As String = Language_id

        'Try
        If temp_language = "en-CA" Then temp_language = "en-US" ' ASleight - Fix for new en-CA locale. Proc expects en-US or fr-CA only.
        obj = data_store.GetAllMarketingInterests(temp_language)
        'Catch ex As Exception
        'Logger.RaiseTrace(ClassName, "GetAllMarketingInterests: " + ex.Message, TraceLevel.Error)
        'Throw ex
        'End Try
        'Logger.RaiseTrace(ClassName, "Exit GetAllMarketingInterests", TraceLevel.Verbose)
        Return obj
    End Function

    Public Function PopulateCustomerDetailWithSAPAccount(ByVal Account As Account, Optional ByRef objGlobalData As GlobalData = Nothing) As Account
        Dim objSAPHelper As New SAPHelper()

        objSAPHelper.GetShiptoAccount(Account, objGlobalData)
        Return Account
    End Function

    Public Function PopulateCustomerDetailWithSAPAccountBySoldtoAccount(ByVal Account As Account, ByRef objGlobalData As GlobalData) As Account
        Dim objSAPHelper As New SAPHelper()

        objSAPHelper.GetShiptoAccountBySoldtoAccount(Account, objGlobalData)
        Return Account
    End Function

    Public Sub PopulateSAPAccount(ByRef account As Account, ByRef objGlobalData As GlobalData)
        Dim objSAPHelper As New SAPHelper()
        objSAPHelper.GetShiptoAccount(account, objGlobalData)
    End Sub

    Public Sub DeleteSAPAccount(ByVal customerID As Integer, ByRef accountNumber As String)
        Dim am As New AccountDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = am.BeginTransaction
            am.DeleteCustomerAccount(customerID, accountNumber, txn_context)
            am.CommitTransaction(txn_context)
        Catch ex As Exception
            am.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetCustomerId(ByVal siamID As String) As Integer
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim customerID As Integer

        Try
            txn_context = data_store.BeginTransaction
            customerID = data_store.GetCustomerID(siamID, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            'Logger.RaiseTrace(ClassName, "GetCourseType: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit GetCustomerID", TraceLevel.Verbose)
        Return customerID
    End Function

    Public Function GetCustomerIDWithEmailAddress(ByVal emailAddress As String) As Integer
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim customerID As Integer

        Try
            txn_context = data_store.BeginTransaction
            customerID = data_store.GetCustomerIDWithEmailAddress(emailAddress, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            'Logger.RaiseTrace(ClassName, "GetCourseType: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        Return customerID
    End Function

    Public Function GetActiveCustomerEmailAddress(ByVal ldapId As String) As String
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim emailAddress As String

        Try
            txn_context = data_store.BeginTransaction
            emailAddress = data_store.GetActiveCustomerEmailAddress(ldapId, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return emailAddress
    End Function

    Public Function GetCustomersWithLDAPID(ByVal ldapId As String) As List(Of Customer)
        Dim data_store As New CustomerDataManager
        Dim customer_List As List(Of Customer)

        Try
            customer_List = data_store.GetCustomersWithLDAPID(ldapId)
        Catch ex As Exception
            Throw ex
        End Try

        Return customer_List
    End Function

    Public Sub UpdateLoginTime(ByVal customerID As Integer)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.UpdateLoginTime(customerID, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub SaveServiceAgreementData(ByRef ProductRegistrationData As ProductRegistration)
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.SaveServiceAgreementData(ProductRegistrationData, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function SaveServiceRequestData(ByRef ServiceRequestData As ServiceItemInfo) As Long
        Dim data_store As New CustomerDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim iServiceRequestID As Long

        Try
            txn_context = data_store.BeginTransaction
            iServiceRequestID = data_store.SaveServiceRequestData(ServiceRequestData, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return iServiceRequestID
    End Function

    Public Sub SendServiceConfirmationMail(ByVal sMailBody As String, ByVal toMailID As String, ByVal toMailID2 As String, ByVal serviceAddr As String, ByVal ServiceCenterEmail As String, ByVal Subject As String)
        Dim fromAddress As String = ServiceCenterEmail.Trim()   ' Pulled from the DB's depot email address entry. Mostly ProSupport@sony.com (as of 2019-08-01)
        If String.IsNullOrWhiteSpace(fromAddress) Then
            fromAddress = ConfigurationData.Environment.Email.Repair.Service_Request_From_Mail
        End If

        Dim mailObj = New MailMessage(fromAddress, toMailID) With {
                .Subject = Subject,
                .Body = sMailBody,
                .IsBodyHtml = True,
                .Priority = MailPriority.Normal
            }
        If toMailID.ToLower() <> toMailID2.ToLower() Then mailObj.To.Add(toMailID2) ' Only add the second email if it's not the same.
        'mailObj.Bcc.Add(fromAddress)   ' 2019-07-24 Removed at request of ProParts team. Depot email address now triggers automated ticketing.

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendProductRegistrationMail(ByVal sMailBody As String, ByVal toMailID As String, ByVal bccMailId As String, ByVal Subject As String)
        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.ServiceAgreement.Service_Agreement_From_Mail, toMailID) With {
            .Subject = Subject,
            .Body = sMailBody,
            .IsBodyHtml = True,
            .Priority = MailPriority.Normal
        }
        mailObj.Bcc.Add(ConfigurationData.Environment.Email.ServiceAgreement.Service_Agreement_BCC) ' SupportNET@am.sony.com

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Port = ConfigurationData.Environment.SMTP.Port
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub HideCreditCard(ByVal customerCreditcard As CreditCard)
        Dim data_store As New PaymentDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.HideCreditCard(customerCreditcard, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

End Class
