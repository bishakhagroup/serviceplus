Imports System.Net.Mail
'Imports System.Web.Mail    ' This namespace is obsolete. Replaced with System.Net.Mail
Imports Sony.US.SIAMUtilities
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data

Public Class RMAManager

    Private NewLine As String = "<br/>"

    Public Function CreateRMARequest(ByVal customer As Customer) As RMA
        Dim ra As New RMA(customer)
        Return ra
    End Function

    Public Sub SubmitRMARequest(ByVal ra_request As RMA)
        Dim data_store As New RMADataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(ra_request, txn_context)
            data_store.CommitTransaction(txn_context)

        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetAllRACreditReasons() As RMAReason()
        Dim data_store As New RMADataManager
        Dim obj() As RMAReason

        Try
            obj = data_store.GetRACreditReasons()
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Sub SendRARequestEmailToCustomerService(ByVal MailBody As String, ByVal Subject As String)
        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, ConfigurationData.Environment.Email.General.RA_To) With {
            .Subject = Subject,
            .Body = MailBody.ToString(),
            .IsBodyHtml = True,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendRARequestConfirmation(ByVal emailAddress As String, ByVal MailBody As String, ByVal Subject As String)
        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, emailAddress) With {
            .Subject = Subject,
            .Body = MailBody.ToString(),
            .IsBodyHtml = True,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Function GetConflictedPart(ByVal PartNumber As String, ByRef objGlobalData As GlobalData) As String
        Dim data_store As New RMADataManager

        Try
            Return data_store.GetConflictedPart(PartNumber, objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
