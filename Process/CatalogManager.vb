
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Imports ServicesPlusException
Imports System.Collections.Generic


Public Class CatalogManager

    Private Const ClassName As String = "CatalogManager"
    'Private Logger As New SonyLogger

    'including wsdl changes - Deepa
    Private serviceEnvironment As String

    Public Sub New()
        'Dim thisConfigHandler As New ConfigHandler_01
        'If thisConfigHandler.GetCurrentEnvironment.ToString().ToLower() = "production" Then
        '    serviceEnvironment = "production"
        'ElseIf thisConfigHandler.GetCurrentEnvironment.ToString().ToLower() = "staging" Then
        '    serviceEnvironment = "staging"
        'Else
        '    serviceEnvironment = "development"
        'End If
        serviceEnvironment = ConfigurationData.GeneralSettings.Environment.ToLower()
    End Sub

    'search directly against SIS parts master
    Public Function QuickSearch(ByVal model_number As String, ByVal part_number As String, ByVal description As String, ByRef sMaximvalue As String, Optional ByRef objGlobalData As GlobalData = Nothing) As Part()

        'Logger.RaiseTrace(ClassName, "Enter QuickSearch", TraceLevel.Verbose)

        Dim data_store As New SISDataManager
        Dim parts() As Part

        'Sasikumar WP SPLUS_WP007
        If objGlobalData Is Nothing Then ' Assign US as default value.
            objGlobalData = New GlobalData With {
                .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                .Language = ConfigurationData.GeneralSettings.GlobalData.Language.US
            }
        End If

        If model_number.Length = 1 Then
            Dim objEx As New ServicesPlusBusinessException("If using model number, you must specify at least 2 characters.") With {
                .hideContactus = True
            }
            Throw objEx
        End If

        If part_number.Length = 1 Then
            Dim objEx As New ServicesPlusBusinessException("If using part number, you must specify at least 2 characters.") With {
                .hideContactus = True
            }
            Throw objEx
        End If

        If description.Length = 1 Then
            Dim objEx As New ServicesPlusBusinessException("If using description, you must specify at least 2 characters.") With {
                .hideContactus = True
            }
            Throw objEx
        End If

        'strip off embedded slashes and dashes??

        Try
            parts = data_store.SearchSISMaster(model_number, part_number, description, sMaximvalue, objGlobalData) 'Sasikumar WP SPLUS_WP007
        Catch ex As Exception
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Exit QuickSearch", TraceLevel.Verbose)

        Return parts
    End Function
    '6916 Starts
    'search against SIS parts master, the parts browser currently shall search only the sispart table

    'Public Function BrowsePartsSearch(ByVal groupid As Long, ByVal startIndex As Integer, ByVal length As Integer, ByRef totalCount As Integer) As DataTable

    '    'Logger.RaiseTrace(ClassName, "Enter BrowseParts", TraceLevel.Verbose)

    '    Dim data_store As New SISDataManager
    '    '' Dim parts() As Sony.US.ServicesPLUS.Core.Part
    '    Dim pats As DataTable
    '    If groupid = 0 Then
    '        Throw New Exception("range is not valid.")
    '    End If

    '    Try
    '        pats = data_store.getSISPartsInRange(groupid, startIndex, length, totalCount)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    '    'Logger.RaiseTrace(ClassName, "Exit BrowseParts", TraceLevel.Verbose)

    '    Return pats
    'End Function
    Public Function BrowsePartsSearch(ByVal groupid As Long, ByVal startIndex As Integer, ByVal length As Integer, ByRef totalCount As Integer) As Part()

        'Logger.RaiseTrace(ClassName, "Enter BrowseParts", TraceLevel.Verbose)

        Dim data_store As New SISDataManager
        Dim parts() As Part
        'Dim pats As DataTable
        If groupid = 0 Then
            Throw New Exception("range is not valid.")
        End If
        Try
            parts = data_store.getSISPartsInRange(groupid, startIndex, length, totalCount)
        Catch ex As Exception
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Exit BrowseParts", TraceLevel.Verbose)

        Return parts
    End Function
    '6916 Ends

    Public Function Availability(ByVal product As Product, ByVal customer As Customer, ByVal product_type As Product.ProductType, ByRef corePart As Part, ByRef yourprice As Double, ByRef objGlobalData As GlobalData) As String 'Sasikumar WP SPLUS_WP008
        Dim parts As SAPProxyPart()
        Dim sap_services As New SAPHelper()
        Dim availibility As String = String.Empty
        Dim partNumber As String

        Try
            If (TypeOf product Is Software) Then
                partNumber = CType(product, Software).KitPartNumber
            Else
                partNumber = IIf(Not String.IsNullOrEmpty(product.ReplacementPartNumber), product.ReplacementPartNumber, product.PartNumber)
            End If

            parts = sap_services.PartInquiry(customer, partNumber, Nothing, , , , , , objGlobalData) 'Sasikumar WP SPLUS_WP008
            availibility = IIf(objGlobalData.IsFrench, "Non", "No")
            For Each sapPart As SAPProxyPart In parts
                If sapPart.Availability > 0 Then
                    availibility = IIf(objGlobalData.IsFrench, "En inventaire", "In stock")
                Else
                    If objGlobalData.IsFrench Then
                        availibility = "Rupture de stock, <a href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a> pour vérifier la disponibilité."
                    Else
                        availibility = "Backordered, contact <a href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a> to check availability."
                    End If
                End If
                If corePart Is Nothing Then corePart = New Part
                corePart.PartNumber = sapPart.Number
                corePart.ReplacementPartNumber = sapPart.Replacement
                corePart.Description = sapPart.Description
                corePart.ProgramCode = sapPart.ProgramCode

                ' Set up prices, adding the Core Charge if one is present.
                corePart.ListPrice = sapPart.ListPrice
                corePart.CoreCharge = sapPart.CoreCharge
                yourprice = sapPart.YourPrice
                If corePart.IsFRB Or corePart.HasCoreCharge Then
                    corePart.ListPrice += sapPart.CoreCharge
                    yourprice += sapPart.CoreCharge
                End If

                If sapPart.RecyclingFlag > 0 Then
                    corePart.SpecialTax = sapPart.RecyclingFlag ' Don't know the detail number yet as it depends on the shipping address, just set flag here
                End If
            Next
        Catch ex As Exception   ' 2018-10-18 ASleight - Added to prevent the ASM block when SAP is unavailable. Exception wasn't being handled, throwing HTTP 502.
            Throw
        End Try

        Return availibility
    End Function

    Public Function ISRecyclingFlagCheck(ByRef partNumber As String, ByVal customer As Customer, ByVal objGlobalData As GlobalData) As Double
        Dim parts As SAPProxyPart()
        Dim sap_services As New SAPHelper()
        Dim resRecycleflag As Double = 0.0

        parts = sap_services.PartInquiry(customer, partNumber, Nothing, , , , , , objGlobalData)

        For Each Part As SAPProxyPart In parts
            If Part.RecyclingFlag > 0 Then
                resRecycleflag = Part.RecyclingFlag ' Don't know the detail number yet as it depends on the shipping address, just set flag here
            End If
        Next
        Return resRecycleflag
    End Function

    Public Function YourPrice(ByVal product As Product, ByVal customer As Customer, ByVal product_type As Product.ProductType, ByRef availibility As String, ByRef corePart As Part, ByRef your_price As Double, ByRef listprice As Double, ByRef objGlobalData As GlobalData) As Double 'Sasikumar WP SPLUS_WP008
        Dim parts As SAPProxyPart()
        Dim sap_services As New SAPHelper()
        Dim partNumber As String

        Try
            If (TypeOf product Is Software) Then
            partNumber = CType(product, Software).KitPartNumber
        Else
            partNumber = IIf(Not String.IsNullOrEmpty(product.ReplacementPartNumber), product.ReplacementPartNumber, product.PartNumber)
        End If

        parts = sap_services.PartInquiry(customer, partNumber, Nothing, , , , , , objGlobalData) 'Sasikumar WP SPLUS_WP008
        availibility = IIf(objGlobalData.IsFrench, "Non", "No")
            For Each sapPart As SAPProxyPart In parts
                If sapPart.Availability > 0 Then
                    availibility = IIf(objGlobalData.IsFrench, "En inventaire", "In stock")
                Else
                    If objGlobalData.IsFrench Then
                        availibility = "Rupture de stock, <a href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a> pour vérifier la disponibilité."
                    Else
                        availibility = "Backordered, contact <a href=""mailto:ProParts@am.sony.com"">ProParts@am.sony.com</a> to check availability."
                    End If
                End If
                If corePart Is Nothing Then corePart = New Part
                corePart.PartNumber = sapPart.Number
                corePart.ReplacementPartNumber = sapPart.Replacement
                corePart.Description = sapPart.Description
                corePart.ProgramCode = sapPart.ProgramCode

                corePart.ListPrice = sapPart.ListPrice
                corePart.CoreCharge = sapPart.CoreCharge
                your_price = sapPart.YourPrice
                listprice = sapPart.ListPrice
                If corePart.IsFRB Or corePart.HasCoreCharge Then
                    corePart.ListPrice += sapPart.CoreCharge
                    your_price += sapPart.CoreCharge
                    listprice += sapPart.CoreCharge
                End If

                If sapPart.RecyclingFlag > 0 Then
                    corePart.SpecialTax = sapPart.RecyclingFlag ' Don't know the detail number yet as it depends on the shipping address, just set flag here
                End If
            Next
        Catch ex As Exception   ' 2018-10-18 ASleight - Added to prevent the ASM block when SAP is unavailable. Exception wasn't being handled, throwing HTTP 502.
            Throw
        End Try

        Return your_price
    End Function

    'search against SIS kit extract
    Public Function KitSearch(ByRef model_number As String, ByRef part_number As String, ByRef model_description As String) As Kit()
        Dim data_store As New SISDataManager
        Dim kits() As Kit

        If model_number.Length < 2 And part_number.Length < 2 And model_description.Length < 2 Then
            Dim objEx1 As New ServicesPlusBusinessException("You must specify at least 2 characters for the kit name") With {
                .hideContactus = True
            }
            Throw objEx1
        End If

        Try
            kits = data_store.GetKits(model_number, part_number, model_description)

            'have to calculate list price when not from sis
            Dim list_price As Double = 0
            For Each k As Kit In kits
                For Each ki As Sony.US.ServicesPLUS.Core.KitItem In k.Items
                    list_price = list_price + (ki.Product.ListPrice * ki.Quantity)
                Next
                k.ListPrice = list_price
                list_price = 0
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return kits
    End Function

    'parts finder initial search
    Public Function PartSearch(ByVal model_number As String, ByVal globalData As GlobalData, Optional ByRef lstConficts As List(Of String) = Nothing) As Model()
        Dim data_store As New PartsFinderDataManager

        Try
            Return data_store.GetModels(model_number, globalData, lstConficts)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub GetModelCategories(ByRef model As Model)
        Dim data_store As New PartsFinderDataManager

        Try
            data_store.GetModelCategories(model)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GetSubModels(ByRef model As Model)
        Dim data_store As New PartsFinderDataManager

        Try
            data_store.GetSubModels(model)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetModules(ByVal model As Model, ByVal category As Category) As _Module()
        Dim data_store As New PartsFinderDataManager
        Dim modules() As _Module

        Try
            modules = data_store.GetModules(model, category)
        Catch ex As Exception
            Throw ex
        End Try

        Return modules
    End Function
    Public Function GetMaxGroupID() As Long
        Dim data_store As New SISDataManager
        Dim partsgroupid As Long

        Try
            partsgroupid = data_store.GetMaxGroupID()
        Catch ex As Exception
            Throw ex
        End Try

        Return partsgroupid
    End Function

    ' model catalog - Deepa V, Mar 19, 2007
    Public Function GetModelMaxGroupID() As Long
        Dim data_store As New SISDataManager
        Dim modelsgroupid As Long

        Try
            modelsgroupid = data_store.GetModelMaxGroupID()
        Catch ex As Exception
            Throw ex
        End Try

        Return modelsgroupid
    End Function

    Public Function GetPartsGroupByPageNumber(ByVal PageNo As Integer) As PartsCatalogGroup()
        Dim data_store As New SISDataManager
        Dim partsgroup() As PartsCatalogGroup

        Try
            partsgroup = data_store.GetPartsGroupByPageNumber(PageNo)
        Catch ex As Exception
            Throw ex
        End Try

        Return partsgroup
    End Function

    ' model catalog - Deepa V, Mar 19, 2007
    Public Function GetModelsGroupByPageNumber(ByVal PageNo As Integer) As ModelsCatalogGroup()
        Dim data_store As New SISDataManager
        Dim modelsgroup() As ModelsCatalogGroup

        Try
            modelsgroup = data_store.GetModelsGroupByPageNumber(PageNo)
        Catch ex As Exception
            Throw ex
        End Try

        Return modelsgroup
    End Function

    ' function used for search model name from Model catalog page
    Public Function GetSrhModelDetails(ByVal pModelName As String, ByRef objGlobalData As GlobalData, Optional ByRef lstConflicts As List(Of String) = Nothing) As ModelDetails()
        Dim data_store As New SISDataManager
        Dim modelsgroup() As ModelDetails

        Try
            modelsgroup = data_store.GetModelsDetailsBySearch(pModelName, objGlobalData, lstConflicts)
        Catch ex As Exception
            Throw ex
        End Try

        Return modelsgroup
    End Function
    'An additional parameter added by sneha
    Public Function SearchRepairStatus(ByVal xModelNumber As String, ByVal xSerialNumber As String, ByVal xNotificationNumber As String, ByVal xCustomerPhoneNumber As String, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String, ByRef lstConfModels As List(Of String), ByRef objGlobalData As GlobalData) As ArrayList

        'Logger.RaiseTrace(ClassName, "Enter SearchRepairStatus", TraceLevel.Verbose)

        Dim data_store As New CatalogDataManager
        Dim objDSResult As DataSet
        Dim objRepairStatus As RepairStatus
        Dim objRepairStatusCollection As ArrayList = New ArrayList()

        Try
            objDSResult = data_store.SearchRepairStatus(xModelNumber, xSerialNumber, xNotificationNumber, xCustomerPhoneNumber, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT, lstConfModels, objGlobalData)

            If Not objDSResult Is Nothing Then
                If objDSResult.Tables.Count > 0 Then
                    'Tables(0) is changed to Tables(1)-By Sneha
                    For Each dRow As DataRow In objDSResult.Tables(1).Rows
                        With dRow
                            objRepairStatus = New RepairStatus
                            objRepairStatus.MODELNUMBER = IIf(.Item("MODELNUMBER").ToString() Is DBNull.Value, String.Empty, .Item("MODELNUMBER").ToString())
                            'Added by Sneha for bug 9014
                            objRepairStatus.OLDMODELNUMBER = IIf(.Item("old_modelnumber").ToString Is DBNull.Value, String.Empty, .Item("old_modelnumber").ToString())
                            objRepairStatus.SERIALNUMBER = IIf(.Item("SERIALNUMBER").ToString() Is DBNull.Value, String.Empty, .Item("SERIALNUMBER").ToString())
                            objRepairStatus.CUSTOMERPHONE = IIf(.Item("CUSTOMERPHONE").ToString() Is DBNull.Value, String.Empty, .Item("CUSTOMERPHONE").ToString())
                            objRepairStatus.NOTIFICATIONNUMBER = IIf(.Item("NOTIFICATIONNUMBER").ToString() Is DBNull.Value, String.Empty, .Item("NOTIFICATIONNUMBER").ToString())
                            objRepairStatus.STATUS = IIf(.Item("STATUS").ToString() Is DBNull.Value, String.Empty, .Item("STATUS").ToString())
                            objRepairStatus.CARRIERCODE = IIf(.Item("CARRIERCODE").ToString() Is DBNull.Value, String.Empty, .Item("CARRIERCODE").ToString())
                            objRepairStatus.TRACKINGNUMBER = IIf(.Item("TRACKINGNUMBER").ToString() Is DBNull.Value, String.Empty, .Item("TRACKINGNUMBER").ToString())
                            objRepairStatus.CUSTOMERSHIPTO = IIf(.Item("CUSTOMERSHIPTO").ToString() Is DBNull.Value, String.Empty, .Item("CUSTOMERSHIPTO").ToString())
                            objRepairStatus.PLANTNUMBER = IIf(.Item("PLANTNUMBER").ToString() Is DBNull.Value, String.Empty, .Item("PLANTNUMBER").ToString())
                            objRepairStatus.PLANTPHONE = IIf(.Item("PLANTPHONE").ToString() Is DBNull.Value, String.Empty, .Item("PLANTPHONE").ToString())
                            objRepairStatus.PLANTEMAILADDRESS = IIf(.Item("PLANTEMAILADDRESS").ToString() Is DBNull.Value, String.Empty, .Item("PLANTEMAILADDRESS").ToString())
                            objRepairStatus.TRACKINGURL = IIf(.Item("TRACKINGURL").ToString() Is DBNull.Value, String.Empty, .Item("TRACKINGURL").ToString())
                            objRepairStatusCollection.Add(objRepairStatus)
                        End With
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit SearchRepairStatus", TraceLevel.Verbose)
        Return objRepairStatusCollection
    End Function

    Public Function SearchOperationManual(ByVal xModelNumber As String, ByVal xCUSTOMERID As Integer _
            , ByVal xCUSTOMERSEQUENCENUMBER As Integer, ByVal xHTTP_X_FORWARDED_FOR As String, ByVal xREMOTE_ADDR As String, ByVal xHTTP_REFERER As String _
            , ByVal xHTTP_URL As String, ByVal xHTTP_USER_AGENT As String) As DataSet
        Dim data_store As New CatalogDataManager

        Try
            Return data_store.SearchOperationManual(xModelNumber, xCUSTOMERID, xCUSTOMERSEQUENCENUMBER, xHTTP_X_FORWARDED_FOR, xREMOTE_ADDR, xHTTP_REFERER, xHTTP_URL, xHTTP_USER_AGENT)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetModelDetails(ByVal PageNo As Integer) As ModelDetails()
        Dim data_store As New SISDataManager
        Dim modelsgroup() As ModelDetails

        Try
            modelsgroup = data_store.GetModelsDetailsByPgNo(PageNo)
        Catch ex As Exception
            Throw ex
        End Try

        Return modelsgroup
    End Function

    Public Function GetPrePartsRange() As PartsCatalogGroup()
        Dim data_store As New SISDataManager
        Dim partsgroup() As PartsCatalogGroup

        Try
            partsgroup = data_store.GetPrePartsRange()
        Catch ex As Exception
            Throw ex
        End Try

        Return partsgroup
    End Function

    Public Sub getRanges(ByRef rangePerPage As Integer, ByRef partsPerRange As Integer)
        Try
            rangePerPage = CType(ConfigurationData.GeneralSettings.Layout.BrowseParts.RangePerPage, Integer)
            partsPerRange = CType(ConfigurationData.GeneralSettings.Layout.BrowseParts.PartsPerRange, Integer)
        Catch ex As Exception
            ' if any error, set default values
            rangePerPage = 100
            partsPerRange = 100
        End Try
    End Sub

    Public Sub GetModelRanges(ByRef rangePerPage As Integer, ByRef modelsPerRange As Integer)
        Try
            rangePerPage = CType(ConfigurationData.GeneralSettings.Layout.BrowseModels.ModelRangePerPage, Integer)
            modelsPerRange = CType(ConfigurationData.GeneralSettings.Layout.BrowseModels.ModelsPerRange, Integer)
        Catch ex As Exception
            ' if any error, set default values
            rangePerPage = 500
            modelsPerRange = 100
        End Try
    End Sub

    Public Sub getRepairCostConf(ByRef pDepotCharge As String, ByRef pFieldCharge As String, ByRef pDeptMinLbr As String, ByRef pFldMinLbr As String)
        Try
            pDepotCharge = ConfigurationData.GeneralSettings.RepairCosting.DepotServiceCharge
            pFieldCharge = ConfigurationData.GeneralSettings.RepairCosting.FieldServiceCharge
            pDeptMinLbr = ConfigurationData.GeneralSettings.RepairCosting.DepotMinLbrCharge
            pFldMinLbr = ConfigurationData.GeneralSettings.RepairCosting.FieldMinLbrTime
        Catch ex As Exception
            ' if any error, set default values
            pDepotCharge = 0
            pFieldCharge = 0
            pDeptMinLbr = 0
            pFldMinLbr = 0
        End Try
    End Sub

    Public Function GetStates() As StatesDetail()
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetStateDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCountry() As DataSet
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetCountryDetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function StatesBySalesOrg(ByRef objGlobalData As GlobalData) As StatesDetail()
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetStateBySalesOrg(objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CountryBySalesOrg(ByRef objGlobalData As GlobalData, Optional ByRef CountryCode As String = "") As String
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetCountryNameByCode(objGlobalData, CountryCode)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    '--------------------------------------------------------------------------------'
    ' @input: State abbreviation code
    ' @output: String contains Service center name, state and phone, Concatenate by !
    '--------------------------------------------------------------------------------'
    Public Function GetServiceCenter(ByVal psStateAbbre As String) As String
        Dim data_store As New SISDataManager

        Try
            Return data_store.ServiceCenterDetails(psStateAbbre)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '--------------------------------------------------------------------------------'
    ' @input: State abbreviation code
    ' @output: String contains Service center name, state and phone, Concatenate by !
    '--------------------------------------------------------------------------------'
    Public Function GetDepotServiceCenter(ByVal psStateAbbre As String, ByVal pModelName As String) As String
        Dim data_store As New SISDataManager

        Try
            Return data_store.DepotServiceCenterDetails(psStateAbbre, pModelName)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' function to return model status code
    Public Function GetModelStatusCode(ByVal psModelName As String) As String
        Dim data_store As New SISDataManager

        Try
            Return data_store.ModelDiscCode(psModelName)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetPartsGroups() As PartsCatalogGroup()
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetPartsGroups()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetModelsGroups() As ModelsCatalogGroup() ' should add the new modelscataloggroup.vb file into core project
        Dim data_store As New SISDataManager

        Try
            Return data_store.GetModelsGroups()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'parts finder final step
    Public Function PartSearch2(ByVal model As Model, ByVal category As Category, ByVal _module As _Module, ByVal refine_search As String, ByRef objGlobalData As GlobalData, Optional ByVal refine_type As Part.PartsFinderRefineSearchType = Sony.US.ServicesPLUS.Core.Part.PartsFinderRefineSearchType.ByDescription) As Part()
        Dim data_store As New PartsFinderDataManager

        Try
            Return data_store.GetParts(model, category, _module, refine_search, objGlobalData, refine_type)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetExplodedView(ByVal part As Part) As Byte()
        Dim data_store As New PartsFinderDataManager

        If part.HasExplodedView Then
            Try
                Return data_store.GetExplodedView(part)
            Catch ex As Exception
                Throw ex
            End Try
        Else
            Throw New Exception("Part does not have an exploded view")
        End If
    End Function

    Public Function GetExplodedViewPath(ByVal part As Part) As String
        Dim data_store As New PartsFinderDataManager

        If part.HasExplodedView Then
            Try
                Return data_store.GetExplodedViewPath(part)
            Catch ex As Exception
                Throw ex
            End Try
        Else
            Throw New Exception("Part does not have an exploded view")
        End If
    End Function
    Public Function SoftwareSearch(ByVal search_number As String, ByVal salesOrg As String, Optional ByVal SearchByPart As Boolean = False) As Software()
        Dim data_store As New SoftwarePlusDataManager

        Try
            Return data_store.SearchSoftwareModel(search_number, salesOrg, SearchByPart)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ' 2017-01-12 ASleight - I believe this method is orphaned (no longer used)
    'Public Function SoftwareSearch(ByVal model_number As String, ByVal category As String, ByVal description As String) As Software()
    '    Dim data_store As New SoftwarePlusDataManager

    '    Try
    '        Return data_store.SearchSoftware(model_number, description, category)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Function SoftwareModelSearch(ByVal model_number As String, ByVal category As String, ByVal description As String) As Software()
        Dim data_store As New SoftwarePlusDataManager

        Try
            Return data_store.SearchSoftwareModel(model_number, description, category)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetSoftwareCategories() As String()
        Dim data_store As New SoftwarePlusDataManager

        Try
            Return data_store.GetSoftwareCategories()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function MyCatalog(ByVal customer As Customer, ByRef objGlobalData As GlobalData) As CustomerCatalogItem()
        Dim data_store As New CatalogDataManager

        If customer IsNot Nothing Then
            Try
                Return data_store.GetMyCatalog(customer, objGlobalData)
            Catch ex As Exception
                Throw ex
            End Try
        End If

        Return Nothing
    End Function

    Public Function MyReminder(ByVal customer As Customer) As CustomerCatalogItem()
        Dim data_store As New CatalogDataManager

        If customer IsNot Nothing Then
            Try
                Return data_store.GetMyReminder(customer)
            Catch ex As Exception
                Throw ex
            End Try
        End If

        Return Nothing
    End Function

    Public Function MyCatalogReminders(ByVal catalog_items() As CustomerCatalogItem) As CustomerCatalogItem()
        Dim temp As New ArrayList

        For Each item As CustomerCatalogItem In catalog_items
            If item.ReminderToday Then
                temp.Add(item)
            End If
        Next

        Dim sub_set(temp.Count - 1) As CustomerCatalogItem
        temp.CopyTo(sub_set)

        Return sub_set
    End Function

    Public Function AddMyCatalogItem(ByVal customer As Customer, ByVal product As Sony.US.ServicesPLUS.Core.Product) As CustomerCatalogItem
        Dim data_store As New CatalogDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim item As New CustomerCatalogItem(customer) With {
            .Product = product
        }

        If product.Type = Product.ProductType.Part Then
            item.ModelNumber = (CType(product, Part)).Model
        ElseIf product.Type = Product.ProductType.Kit Then
            item.ModelNumber = (CType(product, Kit)).Model
        ElseIf product.Type = Product.ProductType.Software Then
            item.ModelNumber = (CType(product, Software)).ModelNumber
        End If

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(item, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return item
    End Function

    Public Sub UpdateMyCatalogItem(ByRef item As CustomerCatalogItem)
        Dim data_store As New CatalogDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(item, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub DeleteMyCatalogItem(ByRef item As CustomerCatalogItem)
        item.Action = CoreObject.ActionType.DELETE
        UpdateMyCatalogItem(item)
    End Sub

    Public Function getConflictedModels(ByVal xModelNumber As String) As DataSet
        Dim data_store As New CatalogDataManager

        Try
            Return data_store.getConflictedModels(xModelNumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'search against SIS kit extract
    Public Function KitSearch_PartsFindSearch(ByRef model_number As String, ByRef lstConflicts As List(Of String)) As Kit()
        Dim data_store As New SISDataManager
        Dim kits() As Kit
        Dim list_price As Double = 0

        If model_number.Length < 2 Then
            Dim objEx1 As New ServicesPlusBusinessException("You must specify at least 2 characters for the kit name") With {
                .hideContactus = True
            }
            Throw objEx1
        End If

        Try
            kits = data_store.GetKits_PartsFindSearch(model_number, lstConflicts)

            'have to calculate list price when not from sis
            For Each k As Kit In kits
                For Each ki As Sony.US.ServicesPLUS.Core.KitItem In k.Items
                    list_price = list_price + (ki.Product.ListPrice * ki.Quantity)
                Next
                k.ListPrice = list_price
                list_price = 0
            Next
        Catch ex As Exception
            Throw ex
        End Try

        Return kits
    End Function
End Class
