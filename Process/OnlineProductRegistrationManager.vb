﻿
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Public Class OnlineProductRegistrationManager

    Private Const ClassName As String = "ExtendedWarranty"
    'Private Logger As New SonyLogger
    Private ewPRDataSet As DataSet
    Private data_store As OnlineProductRegistrationDataManager
    Private txn_context As TransactionContext


    Public Function GetModelPrefix() As DataSet

        'Logger.RaiseTrace(ClassName, "Enter EWSearch", TraceLevel.Verbose)
        Try
            data_store = New OnlineProductRegistrationDataManager()
            ewPRDataSet = data_store.GetModelPrefix()
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch", TraceLevel.Verbose)
        Return ewPRDataSet

    End Function
    Public Function GetIndustry() As DataSet

        'Logger.RaiseTrace(ClassName, "Enter EWSearch GetIndustry", TraceLevel.Verbose)
        Try
            data_store = New OnlineProductRegistrationDataManager()
            ewPRDataSet = data_store.GetIndustry()
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch GetIndustry", TraceLevel.Verbose)
        Return ewPRDataSet

    End Function
    Public Function GetTitle() As DataSet

        'Logger.RaiseTrace(ClassName, "Enter EWSearch GetTitle", TraceLevel.Verbose)
        Try
            data_store = New OnlineProductRegistrationDataManager()
            ewPRDataSet = data_store.GetTitle()
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch GetTitle", TraceLevel.Verbose)
        Return ewPRDataSet

    End Function
    Public Function GetPRModels(ByVal ModelPrefix As String) As DataSet
        'Logger.RaiseTrace(ClassName, "Enter EWSearch", TraceLevel.Verbose)
        Try
            data_store = New OnlineProductRegistrationDataManager()
            ewPRDataSet = data_store.GetPRModels(ModelPrefix)
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch", TraceLevel.Verbose)
        Return ewPRDataSet
    End Function

    Public Function GetReseller() As DataSet

        'Logger.RaiseTrace(ClassName, "Enter EWSearch", TraceLevel.Verbose)
        Try
            data_store = New OnlineProductRegistrationDataManager()
            ewPRDataSet = data_store.GetReseller()
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch", TraceLevel.Verbose)
        Return ewPRDataSet
    End Function

    ''' <summary>
    ''' Checks if any of the user's SAP Accounts are listed in the PR_Reseller
    ''' table, which is populated by ProServ.
    ''' </summary>
    Public Function IsReseller(userId As String) As Boolean
        Dim retVal As Boolean = False

        Try
            BeginTransaction()
            retVal = data_store.IsReseller(userId, txn_context)
            CommitTransaction()
        Catch ex As Exception
            RollbackTransaction()
            Throw ex
        End Try

        Return retVal
    End Function

    Public Sub BeginTransaction()
        data_store = New OnlineProductRegistrationDataManager()
        txn_context = data_store.BeginTransaction
    End Sub

    Public Sub CommitTransaction()
        data_store.CommitTransaction(txn_context)
    End Sub

    Public Sub RollbackTransaction()
        data_store.RollbackTransaction(txn_context)
    End Sub

    'Added by Prasad for Promotional code check
    Public Function ValidatePromotionalCode(ByVal PROMO_CODE_NUMBER As String, ByVal PurchaseDate As Date, ByVal INDUSTRY As String, ByVal ModelNumber As String, ByVal ResellerName As String) As String
        Dim SUCCESS_MESSAGE As String
        data_store = New OnlineProductRegistrationDataManager()
        Try
            SUCCESS_MESSAGE = data_store.ValidatePromotionalCode(PROMO_CODE_NUMBER, PurchaseDate, INDUSTRY, ModelNumber, ResellerName)
        Catch ex As Exception
            Throw ex
        End Try
        Return SUCCESS_MESSAGE
    End Function
    'Added by Prasad for Admin Promotional code changes
    Public Sub SavePromotionalCode(ByVal strPROMO_CODE_NUMBER As String, ByVal strSUCCESS_MESSAGE As String, ByVal PROMO_START_DATE As Date, ByVal PROMO_END_DATE As Date, ByVal PROD_REG_END_DATE As Date)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.SavePromotionalCode(strPROMO_CODE_NUMBER, strSUCCESS_MESSAGE, PROMO_START_DATE, PROMO_END_DATE, PROD_REG_END_DATE, txn_context)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub ModifyPromotionalCode(ByVal strPROMO_CODE_NUMBER As String, ByVal strSUCCESS_MESSAGE As String, ByVal PROMO_START_DATE As Date, ByVal PROMO_END_DATE As Date, ByVal PROD_REG_END_DATE As Date)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.ModifyPromotionalCode(strPROMO_CODE_NUMBER, strSUCCESS_MESSAGE, PROMO_START_DATE, PROMO_END_DATE, PROD_REG_END_DATE, txn_context)
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Public Function GetModelForPromotionCode(ByVal WhereClause As String) As System.Data.DataSet
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.GetModelForPromotionCode(WhereClause)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetResellerForPromotionCode(ByVal WhereClause As String) As System.Data.DataSet
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.GetResellerForPromotionCode(WhereClause)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetIndustryForPromotionCode(ByVal WhereClause As String) As System.Data.DataSet
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.GetIndustryForPromotionCode(WhereClause)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPromotionalCodeDetails(ByVal WhereClause As String) As System.Data.DataSet
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.GetPromotionalCodeDetails(WhereClause)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetPromotionalCodeNumbers(ByVal ErrorString As String) As System.Data.DataSet
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.GetPromotionalCodeNumbers(ErrorString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Added by Prasad for Admin Promotional code changes
    Public Sub SavePromoModel(ByVal strPROMO_CODE_NUMBER As String, ByVal strModel As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.SavePromoModel(strPROMO_CODE_NUMBER, strModel, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub SavePromoIndustry(ByVal strPROMO_CODE_NUMBER As String, ByVal strInd As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.SavePromoIndustry(strPROMO_CODE_NUMBER, strInd, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub DeletePROReseller(ByVal strPROMO_CODE_NUMBER As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.DeletePROReseller(strPROMO_CODE_NUMBER, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub DeletePROMODEL(ByVal strPROMO_CODE_NUMBER As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.DeletePROMODEL(strPROMO_CODE_NUMBER, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub DeletePROIndustry(ByVal strPROMO_CODE_NUMBER As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.DeletePROIndustry(strPROMO_CODE_NUMBER, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub DeletePROMOTIONCODE(ByVal strPROMO_CODE_NUMBER As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.DeletePROMOTIONCODE(strPROMO_CODE_NUMBER, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Prasad for Admin Promotional code changes
    Public Sub SavePromoReseller(ByVal strPROMO_CODE_NUMBER As String, ByVal strReseller As String)
        data_store = New OnlineProductRegistrationDataManager()
        Try
            data_store.SavePromoReseller(strPROMO_CODE_NUMBER, strReseller, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function DuplicatePromotionalCode(ByVal PROMO_CODE_NUMBER As String) As Boolean
        data_store = New OnlineProductRegistrationDataManager()
        Try
            Return data_store.DuplicatePromotionalCode(PROMO_CODE_NUMBER)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function SaveProductRegistration(ByRef ProductRegistrationData As CustomerProductRegistration) As Long
        'Logger.RaiseTrace(ClassName, "Enter PR SAVE", TraceLevel.Verbose)
        Dim returnVal As Long = 0
        Try
            returnVal = data_store.SaveProductRegistration(ProductRegistrationData, txn_context)
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit  PR SAVE", TraceLevel.Verbose)
        Return returnVal
    End Function

    Public Function GetConflictedModel(ByVal ModelNumber As String, ByRef objGlobalData As GlobalData) As String
        Dim strModelNumber As String = String.Empty
        Try
            data_store = New OnlineProductRegistrationDataManager()

            strModelNumber = data_store.GetConflictedModel(ModelNumber, objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
        Return strModelNumber
    End Function

End Class
