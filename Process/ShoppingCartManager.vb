'Imports System.Web.Mail    ' This namespace is obsolete. Replaced with System.Net.Mail
Imports System.Net.Mail
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.SIAMUtilities

Public Class ShoppingCartManager

    Private serviceEnvironment As String
    Private _sourcePart As String = String.Empty


    Public Sub New()
        serviceEnvironment = ConfigurationData.GeneralSettings.Environment.ToLower()
    End Sub

    Public Function CreateShoppingCart(ByRef customer As Customer, ByVal CartType As ShoppingCart.enumCartType) As ShoppingCart
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim expiration_date As New DateTime
        Dim debugMessage As String = ""

        Dim cart As New ShoppingCart(customer) With {
            .Type = CartType,
            .DateCreated = Date.Now,
            .ExpirationDate = Date.Now.AddDays(30)
        }

        Try
            debugMessage = $"ShoppingCartManager.CreateShoppingCart - Cart type: {CartType}, Customer Email: {IIf(customer Is Nothing, "Null customer", customer.EmailAddress)}"
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            Utilities.LogDebug(debugMessage)
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return cart
    End Function

    Public Sub DeleteShoppingCart(ByRef cart As ShoppingCart)
        'an update method could reset expiration date and change name??
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing

        cart.Action = CoreObject.ActionType.DELETE

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    'Public Function AddItemToCart(ByVal cart As ShoppingCart, ByVal item As CustomerCatalogItem, ByVal SapAcNumber As String, ByVal InventoryInfo As String, ByVal PriceInfo As String, ByVal Location As String) As ShoppingCartItem
    '    Try
    '        Dim objSAPhelper As New SAPHelper()
    '        Dim objShoppingCartItem As ShoppingCartItem
    '        Dim objPartNumer As PartInquiry.PartNumber()
    '        Dim objParrtInquiryResponse As New PartInquiry.PartInquiryResponse
    '        objParrtInquiryResponse = objSAPhelper.PartInquiry(cart.Customer, item, InventoryInfo, PriceInfo, Location)
    '        Return objShoppingCartItem
    '    Catch ex As Exception
    '    End Try
    'End Function

    'use this one from MyCatalog
    Public Function AddItemToCart(ByVal objGlobalData As GlobalData, ByRef cart As ShoppingCart, ByRef item As CustomerCatalogItem) As ShoppingCartItem
        Return AddProductToCart(objGlobalData, cart, item.Product, ShoppingCartItem.ItemOriginationType.My_Catalog)
    End Function

    Public Function AddItemToCart(ByVal objGlobalData As GlobalData, ByRef carts As ShoppingCart(), ByRef item As CustomerCatalogItem) As ShoppingCartItem
        Return AddProductToCart(objGlobalData, carts, item.Product, ShoppingCartItem.ItemOriginationType.My_Catalog)
    End Function

    'use this one after a search
    Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByRef cart As ShoppingCart, ByRef product As Product, ByVal origin As ShoppingCartItem.ItemOriginationType, Optional ByVal delivery_method As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship) As ShoppingCartItem
        Dim model_number As String = ""
        Dim software_download_file_name As String = ""

        If product.Type = Product.ProductType.Kit Then
            model_number = CType(product, Kit).Model
            Return AddProductToCart(objGlobalData, cart, product.PartNumber, model_number, Nothing, True, ShoppingCartItem.ItemOriginationType.Search, Product.ProductType.Kit)
        ElseIf product.Type = Product.ProductType.Software Or product.Type = Core.Product.ProductType.Certificate Or product.Type = Core.Product.ProductType.ClassRoomTraining Or product.Type = Core.Product.ProductType.OnlineTraining Or product.Type = Core.Product.ProductType.ComputerBasedTraining Or product.Type = Core.Product.ProductType.ExtendedWarranty Then
            If TypeOf (product) Is ExtendedWarrantyModel Then
                model_number = CType(product, Software).ModelNumber
                software_download_file_name = "NA"
            ElseIf TypeOf (product) Is Training Then
                model_number = CType(product, Training).ModelNumber
                software_download_file_name = "NA"
            ElseIf TypeOf (product) Is Software Then
                model_number = CType(product, Software).ModelNumber
                software_download_file_name = CType(product, Software).DownloadFileName
            End If

            If delivery_method = ShoppingCartItem.DeliveryMethod.Download And cart.ShoppingCartItems.Length > 0 And Not cart.ContainsDownloadOnlyItems Then
                Throw New Exception("Adding download-only item to a shopping cart containing other non-downloadable items is not supported.")
            ElseIf Not delivery_method = ShoppingCartItem.DeliveryMethod.Download And cart.ContainsDownloadOnlyItems Then
                Throw New Exception("Adding non-downloadable item to a shopping cart containing download-only items is not supported.")
            End If

            Return AddProductToCart(objGlobalData, cart, product.PartNumber, model_number, software_download_file_name, False, ShoppingCartItem.ItemOriginationType.Search, product.Type, 1, delivery_method, product)
        Else
            Dim original_part As Part = CType(product, Part)
            model_number = original_part.Model
            Dim partNumber As String = IIf(Not String.IsNullOrEmpty(original_part.ReplacementPartNumber), original_part.ReplacementPartNumber, original_part.PartNumber)
            Return AddProductToCart(objGlobalData, cart, partNumber, model_number, Nothing, False, ShoppingCartItem.ItemOriginationType.Search, Product.ProductType.Part)
        End If
    End Function

    Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByRef carts As ShoppingCart(), ByRef product As Product, ByVal origin As ShoppingCartItem.ItemOriginationType) As ShoppingCartItem
        Dim model_number As String
        Dim software_download_file_name As String

        If product.Type = Product.ProductType.Kit Then
            model_number = CType(product, Kit).Model
            Return AddProductToCart(objGlobalData, carts, product.PartNumber, model_number, Nothing, True, ShoppingCartItem.ItemOriginationType.Search, Product.ProductType.Kit)
        ElseIf product.Type = Product.ProductType.Software Then
            model_number = CType(product, Software).ModelNumber
            software_download_file_name = CType(product, Software).DownloadFileName
            Return AddProductToCart(objGlobalData, carts, product.PartNumber, model_number, Nothing, False, ShoppingCartItem.ItemOriginationType.Search, Product.ProductType.Part)
        Else
            model_number = CType(product, Part).Model
            Return AddProductToCart(objGlobalData, carts, product.PartNumber, model_number, Nothing, False, ShoppingCartItem.ItemOriginationType.Search, Product.ProductType.Part)
        End If
    End Function

    Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByVal cart As ShoppingCart, ByVal part_number As String, ByVal quantity As Integer) As ShoppingCartItem
        Return AddProductToCart(objGlobalData, cart, part_number, "", Nothing, False, ShoppingCartItem.ItemOriginationType.Manual_Entry, Product.ProductType.Part, quantity)
    End Function

    ' Core method that all others should call
    Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByRef cart As ShoppingCart, ByVal part_number As String, ByVal model_number As String, ByVal software_download_file_name As String, ByVal is_kit As Boolean, ByVal origin As ShoppingCartItem.ItemOriginationType, ByVal product_type As Product.ProductType, Optional ByVal quantity As Integer = 1, Optional ByVal delivery_method As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship, Optional ByRef softwareProduct As Software = Nothing) As ShoppingCartItem
        Dim product As Product = Nothing
        Dim parts_available As Integer = 0
        Dim your_price As Double = 0.0

        Dim sap_services As New SAPHelper()
        Dim hasKit As Boolean

        If quantity < 1 Then Throw New Exception("Quantity must be an integer value greater than zero.")

        Dim parts As SAPProxyPart() ' Sony.US.ServicesPLUS.Process.WebMethods.Services.Part
        parts = sap_services.PartInquiry(cart.Customer, part_number, Nothing, objGlobalData:=objGlobalData)

        For Each sapPart As SAPProxyPart In parts
            Select Case product_type
                Case Product.ProductType.Part
                    hasKit = False
                    product = New Part With {
                        .Model = model_number,
                        .PartNumber = part_number
                    }
                Case Product.ProductType.Kit
                    hasKit = True
                    product = New Part With {
                        .Model = model_number,
                        .PartNumber = sapPart.Number
                    }
                Case Product.ProductType.ExtendedWarranty
                    If softwareProduct Is Nothing Then softwareProduct = New Software()
                    product = New ExtendedWarrantyModel(Product.ProductType.ExtendedWarranty) With {
                        .SoftwareItem = softwareProduct,
                        .PartNumber = part_number,
                        .ModelNumber = model_number,
                        .DownloadFileName = If(software_download_file_name, "")
                    }
                Case Product.ProductType.ComputerBasedTraining, Product.ProductType.Certificate, Product.ProductType.OnlineTraining, Product.ProductType.ClassRoomTraining
                    If softwareProduct Is Nothing Then softwareProduct = New Software()
                    product = New Training(product_type) With {
                        .SoftwareItem = softwareProduct,
                        .PartNumber = part_number,
                        .ModelNumber = model_number,
                        .DownloadFileName = If(software_download_file_name, "")
                    }
                Case Product.ProductType.Software
                    If softwareProduct Is Nothing Then softwareProduct = New Software()
                    product = New Software With {
                        .SoftwareItem = softwareProduct,
                        .PartNumber = part_number,
                        .ModelNumber = model_number,
                        .DownloadFileName = If(software_download_file_name, "")
                    }
                Case Else
                    Throw New Exception("Product type not supported")
            End Select

            'convert part from mainframe to core part
            Dim category As New ProductCategory(sapPart.Category)
            product.Category = category
            UpdatePart(product, sapPart, your_price)

            If softwareProduct IsNot Nothing Then product.TrainingURL = softwareProduct.TrainingURL
            parts_available = sapPart.Availability

        Next

        Dim item As ShoppingCartItem = UpdateShoppingCart(product, parts_available, your_price, cart, delivery_method, quantity, hasKit)
        UpdateShoppingCartItemToDatabase(cart)
        Return item
    End Function

    ' 2018-10-04 ASleight - Method has no references
    'use this one for quick order - kits not allowed
    'Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByRef carts As ShoppingCart(), ByVal part_number As String, ByVal quantity As Integer) As ShoppingCartItem
    '    If quantity < 1 Then
    '        Throw New Exception("Quantity must be an integer value greater than zero.")
    '    End If

    '    Dim shoppingCartItem As ShoppingCartItem
    '    shoppingCartItem = AddProductToCart(objGlobalData, carts, part_number, "", Nothing, False, ShoppingCartItem.ItemOriginationType.Manual_Entry, Product.ProductType.Part, quantity)
    '    If shoppingCartItem Is Nothing Then 'part not found, try search for Kit
    '        shoppingCartItem = AddProductToCart(objGlobalData, carts, part_number, "", Nothing, True, ShoppingCartItem.ItemOriginationType.Manual_Entry, Product.ProductType.Part, quantity)
    '    End If
    '    Return shoppingCartItem
    'End Function

    ' 2018-10-04 ASleight - Method has no references
    'Call this method from Quick Order to add parts in the Cart
    'Public Function CheckPart(ByVal partNumber As String) As String
    '    Return ""
    'End Function

    Public Function AddProductToCartQuickCart(ByRef carts As ShoppingCart(), ByVal partNumber As String, ByVal squantity As String, ByVal isRequestFromQuickorder As Boolean, ByRef output1 As String, Optional ByRef objGlobalData As GlobalData = Nothing, Optional ByRef AccountNumber As String = Nothing) As ShoppingCart
        Dim sap_services As New SAPHelper()
        Dim cart As ShoppingCart = Nothing
        Dim product As Product
        Dim kit_item As KitItem
        Dim deliveryMethod As ShoppingCartItem.DeliveryMethod
        Dim parts_available As Integer = 0
        Dim your_price As Double = 0.0
        Dim sapPartList As SAPProxyPart()
        Dim blnPartFound As Boolean = False
        'Dim strNotFoundPart As String = String.Empty
        Dim quantity As Integer = -1
        Dim debugMessage As String = $"ShoppingCartManager.AddProductToCartQuickCart - sourcePart = {SourcePart}, partNumber = {partNumber}.{Environment.NewLine}"

        Try
            quantity = Convert.ToInt64(squantity)
            If quantity < 1 Then Throw New FormatException()
        Catch fex As FormatException
            Throw New Exception("Item quantity must be an integer value greater than zero.", fex)
        End Try

        Try
            If Not String.IsNullOrEmpty(partNumber) Then
                If String.IsNullOrEmpty(SourcePart) Then SourcePart = partNumber
                sap_services.sourcePart = SourcePart
                'Utilities.LogDebug("ShoppingCartManager.AddProductToCartQuickCart - sap_services.sourcePart = " & sap_services.sourcePart)

                ' DEBUG STATEMENTS BELOW
                If carts(0) IsNot Nothing Then
                    If carts(0).Customer Is Nothing Then
                        debugMessage &= $"ShoppingCartManager.AddProductToCartQuickCart - Cart.Customer is Null{Environment.NewLine}"
                    Else
                        debugMessage &= $"ShoppingCartManager.AddProductToCartQuickCart - Cart.Customer is: {carts(0)?.Customer?.EmailAddress}{Environment.NewLine}"
                    End If
                Else
                    debugMessage &= "ShoppingCartManager.AddProductToCartQuickCart - Cart is Null{Environment.NewLine}"
                End If

                If Not isRequestFromQuickorder Then
                    sapPartList = sap_services.PartInquiry(carts(1).Customer, partNumber, Nothing, , , , , isRequestFromQuickorder, objGlobalData)
                Else
                    sapPartList = sap_services.PartInquiry(carts(0).Customer, partNumber, Nothing, , , , , isRequestFromQuickorder, objGlobalData)
                End If
                debugMessage &= $"ShoppingCartManager.AddProductToCartQuickCart - sapPartList.Length = {sapPartList.Length}{Environment.NewLine}"

                For Each sapPart As SAPProxyPart In sapPartList
                    blnPartFound = True
                    deliveryMethod = ShoppingCartManager.getDeliveryMethod(sapPart.ProgramCode)

                    If (Not isRequestFromQuickorder And (sapPart.MaterialType = ConfigurationData.Environment.PartNumberMaterialType.MaterialType)) Then
                        cart = GetShoppingCart(carts, ShoppingCartItem.DeliveryMethod.Download)
                    Else
                        cart = GetShoppingCart(carts, deliveryMethod)
                    End If
                    'If cart IsNot Nothing Then
                    '    Utilities.LogDebug("ShoppingCartManager.AddProductToCartQuickCart - Cart # " & cart.SequenceNumber & ", PartNumber = " & sapPart.Number)
                    'Else
                    '    Utilities.LogDebug("ShoppingCartManager.AddProductToCartQuickCart - Cart is null. PartNumber = " & sapPart.Number)
                    'End If

                    'If Part.ListPrice <= 0 Or Part.YourPrice <= 0 Then
                    '    Throw New Exception("Please contact customer service at " + ConfigurationData.GeneralSettings.ContactNumber.OrderDetail + " to obtain pricing and availability for part number " + Part.Number + ".")
                    'End If
                    If sapPart.hasKit Then
                        product = New Kit
                        'product.PartNumber = partNumber
                        For Each sapKit As SAPProxyKit In sapPart.kit
                            kit_item = New KitItem With {
                            .Quantity = sapKit.Quantity,
                            .NumberOfPartsAvailable = IIf(IsNumeric(sapKit.Availability), sapKit.Availability, 0),
                            .YourPrice = sapKit.YourPrice,
                            .Product = New Kit With {
                                .Description = sapKit.Description,
                                .ListPrice = sapKit.ListPrice
                            }
                        }
                            CType(product, Kit).AddKitItem(kit_item)
                        Next
                    Else
                        product = New Part
                        product.PartNumber = SourcePart   ' partNumber
                    End If

                    UpdatePart(product, sapPart, your_price)
                    parts_available = sapPart.Availability
                    If Not isRequestFromQuickorder Then
                        '9025 fix
                        If sapPart.MaterialType = ConfigurationData.Environment.PartNumberMaterialType.MaterialType Then
                            UpdateShoppingCart(product, parts_available, your_price, cart, ShoppingCartItem.DeliveryMethod.Download, quantity, sapPart.hasKit)
                        Else
                            UpdateShoppingCart(product, parts_available, your_price, cart, ShoppingCartItem.DeliveryMethod.Ship, quantity, sapPart.hasKit)
                        End If
                    Else
                        'Utilities.LogDebug("ShoppingCartManager.AddProductToCartQuickCart - Calling UpdateShoppingCart. " & cart.SequenceNumber)
                        UpdateShoppingCart(product, parts_available, your_price, cart, ShoppingCartItem.DeliveryMethod.Ship, quantity, sapPart.hasKit)
                    End If
                Next
                If blnPartFound = False Then
                    debugMessage &= $"ShoppingCartManager.AddProductToCartQuickCart - No parts were found."
                    'If strNotFoundPart = String.Empty Then
                    '    strNotFoundPart = partNumber.Trim()
                    'Else
                    '    strNotFoundPart = strNotFoundPart + ", " + partNumber.Trim()
                    'End If
                End If
            End If

            UpdateShoppingCartItemToDatabase(cart)
            Return cart
        Catch ex As Exception
            Utilities.LogDebug(debugMessage)
            Throw ex
        End Try
    End Function

    Private Sub HasKitorPart(ByVal cart As ShoppingCart, ByVal Part As SAPProxyPart)
        Dim shoppingcartitems() As ShoppingCartItem = cart.ShoppingCartItems

        If (shoppingcartitems.Length > 0) Then
            If (shoppingcartitems(0).haskit And Not Part.hasKit) Then
                Dim objEx As New ServicesPlusBusinessException("Item " + Part.Number + ": Kits cannot be combined with Parts and Accessories in the same cart. Please create a New Cart and add item " + Part.Number + " to cart. When creating a new cart the existing cart will be in Saved Carts")

                objEx.errorMessage = "Item " + Part.Number + ": Non-kit item cannot be added to a cart with kits. Please add non-kit item to a new cart"
                objEx.setcustomMessage = True
                Throw objEx
            End If
            If (Part.hasKit And Not shoppingcartitems(0).haskit) Then
                Dim objEx As New ServicesPlusBusinessException("Item " + Part.Number + ": Kits cannot be combined with Parts and Accessories in the same cart. Please create a New Cart and add item " + Part.Number + " to cart. When creating a new cart the existing cart will be in Saved Carts")
                objEx.errorMessage = "Item " + Part.Number + ": Non-kit item cannot be added to a cart with kits. Please add non-kit item to a new cart"
                objEx.setcustomMessage = True
                Throw objEx
            End If
        End If
    End Sub

    Private Function UpdateShoppingCart(ByVal Product As Product, ByVal PartsAvailable As Integer, ByVal your_price As Double, ByRef cart As ShoppingCart, ByVal delivery_method As ShoppingCartItem.DeliveryMethod, ByVal quantity As Integer, ByVal hasKit As Boolean) As ShoppingCartItem
        If quantity < 1 Then Throw New Exception("Quantity must be an integer value greater than zero.")
        Dim item As New ShoppingCartItem(Product, PartsAvailable, your_price, cart)
        item.DeliveryChannel = delivery_method
        item.Quantity = quantity
        item.haskit = hasKit
        cart.AddShoppingCartItem(item)
        Return item
    End Function

    Private Sub UpdatePart(ByRef product As Product, ByVal sapPart As SAPProxyPart, ByRef your_price As Double)
        product.Description = sapPart.Description
        product.ProgramCode = sapPart.ProgramCode
        product.CoreCharge = sapPart.CoreCharge
        product.ListPrice = sapPart.ListPrice
        your_price = sapPart.YourPrice

        If Not String.IsNullOrEmpty(sapPart.ProgramCode.Trim) And "IMSYZTF".Contains(sapPart.ProgramCode.Trim) Then
            product.ListPrice += sapPart.CoreCharge
            your_price += sapPart.CoreCharge
        End If

        If String.IsNullOrEmpty(product.PartNumber) Then product.PartNumber = sapPart.Number

        If ((Not String.IsNullOrEmpty(sapPart.Replacement)) AndAlso (product.PartNumber <> sapPart.Replacement)) Then
            product.IsReplaced = True
            product.ReplacementPartNumber = sapPart.Replacement
        ElseIf (product.PartNumber <> sapPart.Number) Then   ' This should never happen; sapPart.Number should be the part number that was input
            product.IsReplaced = True
            product.ReplacementPartNumber = sapPart.Number
        Else
            product.IsReplaced = False
            product.ReplacementPartNumber = sapPart.Number
        End If

        If sapPart.RecyclingFlag > 0 Then
            product.SpecialTax = sapPart.RecyclingFlag  ' Don't know the detail number yet as it depends on the shipping address, just set flag here
        End If
    End Sub

    Private Sub UpdateShoppingCartItemToDatabase(ByRef cart As ShoppingCart)
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    'Core Function for multiple Carts
    Public Function AddProductToCart(ByVal objGlobalData As GlobalData, ByRef carts As ShoppingCart(), ByVal part_number As String, ByVal model_number As String, ByVal software_download_file_name As String, ByVal is_kit As Boolean, ByVal origin As ShoppingCartItem.ItemOriginationType, ByVal product_type As Product.ProductType, Optional ByVal quantity As Integer = 1, Optional ByVal delivery_method As ShoppingCartItem.DeliveryMethod = ShoppingCartItem.DeliveryMethod.Ship) As ShoppingCartItem
        Dim product As Product
        Dim parts_available As Integer = 0
        Dim your_price As Double = 0.0
        Dim cart As ShoppingCart = Nothing
        Dim sap_services As New SAPHelper()
        Dim parts As SAPProxyPart()
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing
        Dim kit_item As KitItem

        If quantity < 1 Then Throw New Exception("Quantity must be an integer value greater than zero.")
        Try
            'part = sis_services.PartInquiry(carts(0).Customer, part_number) 'Any cart's customer is the same
            parts = sap_services.PartInquiry(carts(0).Customer, part_number, Nothing, objGlobalData:=objGlobalData)
        Catch ex As Exception
            If ex.Message.Contains("Part not found") Then
                Return Nothing 'part not found
            Else
                Throw ex
            End If
        End Try
        product = New Part()        ' 2018-10-04 ASleight - Added to prevent Null Reference warning.

        For Each sapPart As SAPProxyPart In parts
            Dim deliveryMethod As ShoppingCartItem.DeliveryMethod = getDeliveryMethod(sapPart.ProgramCode)
            cart = GetShoppingCart(carts, deliveryMethod)
            'mzn - added 03/02/2004
            If (sapPart.YourPrice * quantity) > ShoppingCartItem.PurchaseLimit Then
                Throw New Exception(ShoppingCartItem.PurchaseLimit.ToString("{0:c}") + " limit exceeded. Please contact customer service at 1-800-538-7550 to process your order.")
            End If

            If sapPart.hasKit Then
                product = New Kit
                For Each Kit As SAPProxyKit In sapPart.kit
                    kit_item = New KitItem With {
                        .Quantity = Kit.Quantity,
                        .NumberOfPartsAvailable = IIf(IsNumeric(Kit.Availability), Kit.Availability, 0),
                        .YourPrice = Kit.YourPrice,
                        .Product = New Kit With {
                            .Description = Kit.Description,
                            .ListPrice = Kit.ListPrice
                        }
                    }
                    CType(product, Kit).AddKitItem(kit_item)
                Next
            Else
                product = New Part
            End If

            If sapPart.Replacement.StartsWith("9910999") Then
                Throw New Exception(sapPart.Replacement + " " + sapPart.Number + " - " + sapPart.Description)
            ElseIf sapPart.Replacement.StartsWith("9910") Then
                Throw New Exception("Part Number " + sapPart.Number + " has been discontinued and is not available for purchase.")
            End If

            If sapPart.ListPrice <= 0 Or sapPart.YourPrice <= 0 Then
                Throw New Exception("Please contact customer service at 1-800-538-7550 to obtain pricing and availability for part number " + sapPart.Number + ".")
            End If

            Select Case product_type
                Case Product.ProductType.Part
                    product = New Part
                    CType(product, Part).Model = model_number
                    If sapPart.Replacement = "" Then
                        CType(product, Part).PartNumber = sapPart.Number
                    Else
                        CType(product, Part).PartNumber = sapPart.Replacement
                    End If

                    If sapPart.RecyclingFlag > 0 Then
                        product.SpecialTax = sapPart.RecyclingFlag  ' Don't know the detail number yet as it depends on the shipping address, just set flag here
                    End If
                Case Product.ProductType.Kit
                    product = New Kit
                    CType(product, Kit).Model = model_number
                    If sapPart.Replacement = "" Then
                        CType(product, Kit).PartNumber = sapPart.Number
                    Else
                        CType(product, Kit).PartNumber = sapPart.Replacement
                    End If

                    If sapPart.hasKit Then
                        For Each Kit As SAPProxyKit In sapPart.kit
                            kit_item = New KitItem With {
                                .Quantity = Kit.Quantity,
                                .NumberOfPartsAvailable = IIf(IsNumeric(Kit.Availability), Kit.Availability, 0),
                                .YourPrice = Kit.YourPrice,
                                .Product = New Kit With {
                                    .Description = Kit.Description,
                                    .ListPrice = Kit.ListPrice
                                }
                            }
                            CType(product, Kit).AddKitItem(kit_item)
                        Next
                    End If
                    If sapPart.RecyclingFlag > 0 Then
                        product.SpecialTax = sapPart.RecyclingFlag  ' Don't know the detail number yet as it depends on the shipping address, just set flag here
                    End If
                Case Product.ProductType.Software
                    product = New Software With {
                        .PartNumber = part_number,
                        .ModelNumber = model_number,
                        .DownloadFileName = software_download_file_name
                    }
                Case Else
                    Throw New Exception("Product type not supported")
            End Select

            UpdatePart(product, sapPart, your_price)
            parts_available = sapPart.Availability
        Next

        Dim item As New ShoppingCartItem(product, parts_available, your_price, cart) With {
            .DeliveryChannel = delivery_method,
            .Quantity = quantity
        }
        cart.AddShoppingCartItem(item)

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try

        Return item
    End Function

    Private Function GetShoppingCart(ByRef carts As ShoppingCart(), ByVal deliverType As ShoppingCartItem.DeliveryMethod) As ShoppingCart
        Select Case deliverType
            Case ShoppingCartItem.DeliveryMethod.Download
                Return carts(ShoppingCart.enumCartType.Download)
                'WORK!!! Now don't implement POD shopping cart
                'Case ShoppingCartItem.DeliveryMethod.PrintOnDemand 
                '   Return carts(ShoppingCart.enumCartType.PrintOnDemand)
            Case Else
                Return carts(ShoppingCart.enumCartType.Other)
        End Select
    End Function

    Public Sub UpdateShoppingCart(ByRef cart As ShoppingCart)
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub RemoveItemFromCart(ByRef cart As ShoppingCart, ByRef item As ShoppingCartItem)
        Dim data_store As New ShoppingCartDataManager
        Dim txn_context As TransactionContext = Nothing

        item.Action = CoreObject.ActionType.DELETE

        'Dim other_items() As ShoppingCartItem
        'Dim found_downloads As Boolean = False
        'other_items = cart.ShoppingCartItems

        'if removing software from the cart then chance that cart no longer contains downloadable items
        'If item.Product.Type = Product.ProductType.Software Then
        'For Each i As ShoppingCartItem In other_items
        '    Try
        '        'If CType(i.Product, Software).DownloadFileName.Length > 0 Then
        '        If i.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Download Then
        '            found_downloads = True
        '            Exit For
        '        End If
        '    Catch ex As Exception
        '        'move on
        '    End Try
        'Next

        ' ASleight - At some point the below "If Not" was commented out, which is the entire reason for the above loop and boolean flag.
        '            I thought about removing it, but it may be worth fixing the logic one day. I assume they had some reason for
        '            commenting it out; I never tested it myself.
        'If Not found_downloads Then
        '    cart.ContainsDownloadableItems = False
        'End If
        'End If

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(cart, txn_context, True)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetSavedCarts(ByVal customerid As Long) As DataSet
        Dim data_store As New ShoppingCartDataManager
        Dim savedcarts As DataSet
        Try
            savedcarts = data_store.GetCustomerSavedCarts(customerid)
        Catch ex As Exception
            Throw ex
        End Try
        Return savedcarts
    End Function

    Public Function GetShoppingCarts(ByVal objGlobalData As GlobalData, ByVal customer As Customer, Optional ByVal refresh_from_sis As Boolean = True, Optional ByVal Sequencenumber As Integer = 0) As ShoppingCart()
        Dim data_store As New ShoppingCartDataManager
        Dim carts() As ShoppingCart
        Try
            carts = data_store.GetCustomerShoppingCarts(customer, Sequencenumber)
        Catch ex As Exception
            Throw ex
        End Try

        If refresh_from_sis Then 'if this switch is used - still have to restore kits from somewhere...
            Dim sap_services As New SAPHelper()
            Dim parts As SAPProxyPart()

            For Each cart As ShoppingCart In carts
                For Each item As ShoppingCartItem In cart.ShoppingCartItems
                    parts = sap_services.PartInquiry(customer, item.Product.PartNumber, Nothing, objGlobalData:=objGlobalData)
                    Dim kit_item As KitItem

                    For Each Part As SAPProxyPart In parts
                        If Part.hasKit Then
                            item.Product = New Kit With {
                                .Category = New ProductCategory(Part.Category),
                                .PartNumber = Part.Number,
                                .Description = Part.Description
                            }
                            If Part.RecyclingFlag > 0 Then
                                CType(item.Product, Kit).SpecialTax = Part.RecyclingFlag
                            End If

                            For Each Kit As SAPProxyKit In Part.kit
                                kit_item = New KitItem With {
                                    .Quantity = Kit.Quantity,
                                    .NumberOfPartsAvailable = IIf(IsNumeric(Kit.Availability), Kit.Availability, 0),
                                    .YourPrice = Kit.YourPrice,
                                    .Product = New Kit With {
                                        .Description = Kit.Description,
                                        .ListPrice = Kit.ListPrice
                                    }
                                }

                                CType(item.Product, Kit).AddKitItem(kit_item)
                            Next
                        Else
                            item.Product = New Part With {
                                .PartNumber = Part.Number,
                                .ReplacementPartNumber = Part.Number,       ' ASleight - Fix for new Shopping Cart update. DB doesn't store searched/replaced parts for Saved Carts.
                                .Description = Part.Description
                            }

                            If Part.RecyclingFlag > 0 Then
                                CType(item.Product, Part).SpecialTax = Part.RecyclingFlag
                            End If
                        End If
                        item.Product.ListPrice = Part.ListPrice
                        item.YourPrice = Part.YourPrice

                        If Not String.IsNullOrEmpty(Part.ProgramCode) And "IMSYZTF".Contains(Part.ProgramCode) Then ' ASleight - Improved from 7 different "If ProgramCode = "I" Or..."
                            item.Product.ListPrice += Part.CoreCharge
                            item.YourPrice += Part.CoreCharge
                        End If

                        item.Product.ProgramCode = Part.ProgramCode
                        item.Product.CoreCharge = Part.CoreCharge
                        item.NumberOfPartsAvailable = Part.Availability

                        If item.YourPrice <> Part.YourPrice Then
                            item.Comment = "Price changed!"
                            cart.Comment = "There was a price change since you last viewed your cart"
                        End If
                    Next

                    '' End If
                    If item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Ship Then
                        cart.Type = ShoppingCart.enumCartType.Other
                    Else
                        cart.Type = ShoppingCart.enumCartType.Download
                    End If
                Next
            Next
        End If

        Return carts
    End Function

    Public Sub SendShoppingCartInEmail(ByVal emailAddress As String, ByVal MailBody As String, ByVal Subject As String)
        Dim mailObj As New MailMessage(ConfigurationData.Environment.Email.General.From, emailAddress) With {
            .Subject = Subject,
            .Body = MailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Shared Function getDeliveryMethod(ByRef programCode As String) As ShoppingCartItem.DeliveryMethod
        If Not String.IsNullOrEmpty(programCode) AndAlso "WAV".Contains(programCode) Then
            Return ShoppingCartItem.DeliveryMethod.PrintOnDemand
        Else
            Return ShoppingCartItem.DeliveryMethod.Ship
        End If
    End Function

    Public Function FindConflictCard(ByVal sParts As String, Optional ByRef objGlobalData As GlobalData = Nothing) As DataSet
        Dim data_store As New ShoppingCartDataManager
        Dim conf_parts As DataSet
        Try
            ' GlobalData is nothing assign default value,else get values from object.
            If objGlobalData Is Nothing Then ' Assign US as default value.
                objGlobalData = New GlobalData With {
                    .SalesOrganization = ConfigurationData.GeneralSettings.GlobalData.SalesOrganization.US,
                    .DistributionChannel = ConfigurationData.GeneralSettings.GlobalData.DistributionChannel.Value,
                    .Division = ConfigurationData.GeneralSettings.GlobalData.Division.Value,
                    .Language = ConfigurationData.GeneralSettings.GlobalData.LanguagewM.US
                }
            End If

            conf_parts = data_store.GetConflictParts(sParts, objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
        Return conf_parts

    End Function

    Public Property SourcePart() As String
        Get
            Return _sourcePart
        End Get
        Set(ByVal value As String)
            _sourcePart = value
        End Set

    End Property
End Class
