Imports System.Collections.Specialized
Imports System.Globalization
Imports System.Net.Mail
Imports System.Security.Cryptography
Imports System.Text
Imports System.Web
'Imports System.Web.Mail    ' This namespace is obsolete. Replaced with System.Net.Mail
Imports ServicesPlusException
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Process.WebMethods
Imports Sony.US.SIAMUtilities

Public Class OrderManager

    Private Const ClassName As String = "OrderManager"
    Private serviceEnvironment As String
    Private usCulture As New CultureInfo("en-US")
    Private numStyle As NumberStyles = NumberStyles.Integer Or NumberStyles.AllowDecimalPoint

    Public Sub New()
        serviceEnvironment = ConfigurationData.GeneralSettings.Environment.ToLower()
    End Sub

    Public Function GetOrders(ByVal customer As Customer, ByVal SortBy As String, ByVal SortOrder As String, ByRef objGlobalData As GlobalData, Optional ByVal get_local_invoices As Boolean = True) As Order()
        Dim data_store As New OrderDataManager
        Dim orders() As Order
        Dim debugMessage As String = $"OrderManager.GetOrders - START at {Date.Now.ToShortTimeString}{Environment.NewLine}"

        Try
            'orders = data_store.GetCustomerOrders(customer, SortBy, SortOrder)
            debugMessage &= $"- Calling GetCustomerOrders for Customer Email: {customer.EmailAddress}{Environment.NewLine}"
            orders = data_store.GetCustomerOrders(customer, objGlobalData)
            debugMessage &= $"- GetCustomerOrders finished. Order count: {If(orders?.Count, "orders is null")}{Environment.NewLine}"
        Catch ex As Exception
            Utilities.LogMessages(debugMessage)
            Throw ex
        End Try

        If get_local_invoices Then
            debugMessage &= $"- Fetching Invoices.{Environment.NewLine}"
            For Each o As Order In orders
                Try
                    data_store.GetOrderInvoices(o)
                Catch ex As Exception
                    debugMessage &= $"- EXCEPTION during Invoice loop.{Environment.NewLine}"
                    Utilities.LogMessages(debugMessage)
                End Try
            Next
        End If

        Return orders
    End Function

    Public Function GetCustomerOrders(ByVal customerid As Long) As DataSet
        Dim data_store As New OrderDataManager
        Dim orders As DataSet
        Try
            orders = data_store.GetCustomerOrders(customerid)
        Catch ex As Exception
            Throw ex
        End Try
        Return orders
    End Function

    Public Function GetCertificateOrderLinesForOrder(ByVal OrderNumber As String) As DataSet
        Dim data_store As New OrderDataManager
        Dim orders As DataSet
        Try
            orders = data_store.GetCertificateOrderLinesForOrder(OrderNumber)
        Catch ex As Exception
            Throw ex
        End Try
        Return orders
    End Function

    Public Sub GetOrderInvoices(ByRef order As Order)
        Dim data_store As New OrderDataManager

        Try
            data_store.GetOrderInvoices(order)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GetSpecOrderInvoices(ByRef order As Order, ByVal OriginalInvoicenumber As String)
        Dim data_store As New OrderDataManager

        Try
            data_store.GetSpecOrderInvoices(order, OriginalInvoicenumber)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GetInvoiceShippingMethod(ByRef invoice As Sony.US.ServicesPLUS.Core.Invoice)
        Dim data_store As New OrderDataManager
        Dim shipping_method As Sony.US.ServicesPLUS.Core.ShippingMethod

        Try
            'Commented By Navdeep kaur on 18th May for resolving 6396
            '5325 starts
            shipping_method = data_store.GetShippingMethodByCarrierCode(invoice.CarrierCode)
            invoice.ShippingMethod = shipping_method
            '5325 ends
            'end of Comment By Navdeep kaur on 18th May for resolving 6396
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetOrder(ByVal order_number As String, Optional ByVal restore_from_sis As Boolean = True, Optional ByVal Bool_GetCustomer As Boolean = True, Optional ByRef objGlobalData As GlobalData = Nothing) As Order
        Dim data_store As New OrderDataManager
        Dim order As Order
        Dim objSAPHelper As New SAPHelper()

        Try
            If restore_from_sis Then
                order = objSAPHelper.OrderInquiry(order_number, True, True, True, objGlobalData)
            Else
                order = data_store.Restore(order_number, objGlobalData, Bool_GetCustomer)
            End If
        Catch ex As Exception
            'log error
            Throw ex
        End Try

        Return order
    End Function

    Public Function GetOrderLineItems(ByVal order_number As String) As Sony.US.ServicesPLUS.Core.OrderLineItem()
        Dim data_store As New OrderDataManager
        Dim order_lines As Sony.US.ServicesPLUS.Core.OrderLineItem()

        If String.IsNullOrEmpty(order_number) Then Return Nothing
        Try
            order_lines = data_store.GetOrderDetails(order_number)
            Return order_lines
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function FindOrder(ByVal order_number As String, ByVal verisign_auth_code As String, ByRef objGlobalData As GlobalData, Optional ByVal IsInvoiceProcessorCall As Boolean = False) As Order
        Dim data_store As New OrderDataManager
        Dim order As Order

        Try
            order = data_store.FindOrder(order_number, verisign_auth_code, objGlobalData, IsInvoiceProcessorCall)
        Catch ex As Exception
            'log error
            Throw ex
        End Try

        Return order
    End Function

    'Public Function FindOrderFromInvoice(ByVal invoice_number As String) As Sony.US.ServicesPLUS.Core.Order
    '    Dim data_store As New OrderDataManager
    '    Dim order As Sony.US.ServicesPLUS.Core.Order

    '    Try
    '        order = data_store.FindOrderFromInvoice(invoice_number)
    '    Catch ex As Exception
    '        'log error
    '        Throw ex
    '    End Try

    '    Return order
    'End Function

    Public Sub CancelOrderLineItem(ByVal order_line_item As OrderLineItem)
        Dim lines(1) As String
        lines.SetValue(order_line_item.LineNumber, 0)
        If order_line_item.LineStatus <> OrderLineItem.Status.Backordered Then
            Throw New Exception("Line item must be in backordered status to cancel")
        End If
        Try
            'sis.CancelOrderLineItem(order_line_item.Order.OrderNumber, lines)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetAllShippingMethods(ByRef objGlobalData As GlobalData) As ShippingMethod()
        Dim data_store As New OrderDataManager
        Dim obj() As ShippingMethod

        Try
            obj = data_store.GetShippingMethods(objGlobalData)
        Catch ex As Exception
            Throw ex
        End Try
        Return obj
    End Function

    Public Function CheckoutB2BCCOrder(ByRef customer As Customer, ByRef cart As ShoppingCart, ByRef credit_card As CreditCard, ByRef bill_to As Account, ByRef ship_to As Account, ByRef phone As String, ByRef shipping_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean) As Order
        Return Nothing
    End Function

    Public Function Checkout2(ByRef customer As Customer, ByRef cart As ShoppingCart, ByRef credit_card As CreditCard, ByRef bill_to As Sony.US.ServicesPLUS.Core.Address, ByRef ship_to As Sony.US.ServicesPLUS.Core.Address, ByRef phone As String, ByRef shipping_method As ShippingMethod, ByVal ship_backorders_same_way As Boolean) As Order
        Dim sis_order = New Order
        Dim objSAPhelper As New SAPHelper
        '***************Changes done for Bug# 1212 Starts here***************
        '**********Kannapiran S****************

        Dim billing_only As Boolean = cart.ContainsDownloadOnlyItems

        Console.WriteLine("Creating on SIS")
        'sis_order = sis.CreateCreditCardOrder(phone, cart, bill_to, ship_to, shipping_method, ship_backorders_same_way, taxexempt, billing_only)
        'sis_order = objSAPhelper.CreateCreditCardOrder(credit_card, phone, cart, bill_to, ship_to, shipping_method, ship_backorders_same_way, ship_to.CACalifornia, billing_only)

        Return sis_order
    End Function

    Public Function SimulateOrder(ByRef customer As Customer, ByRef cart As ShoppingCart, ByRef bill_to As Account, ByRef bill_to_overriden As Boolean, ByRef ship_to As Account, ByRef ship_to_overriden As Boolean, ByRef shipping_method As ShippingMethod, Optional ByVal IsCreditCardOrder As Boolean = False, Optional ByRef Order_Credit_Card As CreditCard = Nothing, Optional ByRef objGlobalData As GlobalData = Nothing) As Order
        Dim billing_only As Boolean = cart.ContainsDownloadOnlyItems
        Dim SAP_order As Object
        Dim salesOrderResponse As New OrderInjection.SalesOrderResponse()
        Dim objSAPhelper As New SAPHelper
        Dim objOrderDetails As New OrderInjection.OrderDetails()
        Dim objshipinformation As New OrderInjection.ShipInformation()
        Dim objBillInformation As New OrderInjection.BillInformation()
        Dim objsalesRequest1 As New OrderInjection.SalesOrderRequest1()
        Dim objlineDetails As New OrderInjection.LineDetails()
        Dim arrLinedetails As OrderInjection.LineDetails()
        Dim arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
        Dim replacedPartList As New Hashtable   ' For hanging onto original and replaced part numbers; Order Simulation has no original/replaced logic.
        Dim debugMessage As String = ""

        Try
            arrLinedetails = New OrderInjection.LineDetails(cart.ShoppingCartItems.Count - 1) {}
            SAP_order = New WebMethods.Services.Order
            ''Simulation X is for simucatin
            objOrderDetails.Input3 = "X"
            ''For Bug number 6425
            objBillInformation.SoldTo = IIf(Not bill_to.SoldToAccount Is Nothing, bill_to.SoldToAccount, bill_to.AccountNumber.PadLeft(10, "0"))  ''bill_to.AccountNumber.PadLeft(10, "0")
            objOrderDetails.Input4 = bill_to.AccountNumber.PadLeft(10, "0")
            objOrderDetails.BillInformation = objBillInformation
            objOrderDetails.CustomerPONumber = cart.PurchaseOrderNumber
            objOrderDetails.GroupCode = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.GroupCode, shipping_method.SISPickGroupCode)
            'objOrderDetails.GroupCode = cart.ContainsDownloadOnlyItems ? con shipping_method.SISPickGroupCode
            objOrderDetails.OrderEntryPerson = customer.SIAMIdentity
            ''TODO:DropShip Indicator
            objOrderDetails.ReasonCode = "Y"
            ''TODO :D
            objOrderDetails.TaxExempt = ship_to.TaxExempt
            ''TODO :YourReference
            objOrderDetails.YourReference = GetUniqueKey()

            Dim i As Integer = 0
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                'Try
                objlineDetails = New OrderInjection.LineDetails With {
                    .OrderQuantity = item.Quantity.ToString,
                    .PartNumber = item.Product.ReplacementPartNumber   ' ASleight - Due to changes in Cart code, "ReplacementPartNumber" always holds the correct part number
                }
                'objlineDetails.PartNumber = item.Product.PartNumber
                If (item.Product.IsReplaced) Then
                    objlineDetails.ItemComments = "This part replaces " & item.Product.PartNumber & ", which you originally searched for."
                    ' ASleight - Add "replaced" parts to a Hashtable in Session Variables so we remember this information past Order Simulation.
                    If (Not replacedPartList.ContainsKey(item.Product.ReplacementPartNumber.Trim.ToUpper)) Then
                        ' If the user entered the same part number twice, ensure we don't try to add it again, or it will cause an Exception
                        replacedPartList.Add(item.Product.ReplacementPartNumber.Trim.ToUpper, item.Product.PartNumber.Trim.ToUpper)
                    End If
                End If
                'objlineDetails.Carrier = shipping_method.SISCarrierCode
                arrLinedetails(i) = objlineDetails
                i = i + 1
                'Catch ex As Exception
                '    Dim errorMessage = "ORDERMANAGER: Failed during Shopping Cart loop. Cart Items count: " & cart.ShoppingCartItems.Count & ", Current Item: " & i _
                '    & ", PartNumber: " & item.Product.PartNumber & ", Replacement: " & item.Product.ReplacementPartNumber & ", IsReplaced: " & item.Product.IsReplaced
                '    objUtilities.LogMessages(errorMessage)
                'End Try
            Next
            HttpContext.Current.Session("replacedPartList") = replacedPartList   ' Set this to keep track of Original and Replacement parts in future pages
            objOrderDetails.LineDetails = arrLinedetails
            objOrderDetails.Input1 = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.CarrierCode, shipping_method.SISCarrierCode)

            'Shiping Information
            objshipinformation.ShipTo = IIf(Not ship_to.SoldToAccount Is Nothing, ship_to.SoldToAccount, ship_to.AccountNumber.PadLeft(10, "0")) ''ship_to.Address.SapAccountNumber
            objshipinformation.BusExt = ship_to.Address.PhoneExt
            objshipinformation.City = ship_to.Address.City
            objshipinformation.District = ship_to.Address.Address3
            objshipinformation.Fax = cart.Customer.FaxNumber
            objshipinformation.FirstTelephoneNumber = ship_to.Address.PhoneNumber
            objshipinformation.ShipToName1 = ship_to.Address.Name
            objshipinformation.ShipToName3 = ship_to.Address.Attn
            objshipinformation.State = ship_to.Address.State
            objshipinformation.Street = ship_to.Address.Address1
            objshipinformation.Street2 = ship_to.Address.Address2
            objshipinformation.ZipCode = ship_to.Address.Zip
            objOrderDetails.ShipInformation = objshipinformation
            objsalesRequest1.OrderDetails = objOrderDetails
            'Sasikumar WP
            'Utilities.LogDebug("OrderManager.SimulateOrder - Calling SalesOrder Injection. GlobalData = (" & objGlobalData.ToString & "), BillTo Account = " & objOrderDetails.Input4)
            salesOrderResponse = objSAPhelper.SalesOrderInjection(objOrderDetails, objGlobalData)
            ' Canadian Double conversion fails somewhere after here. String value is ""
            Dim objServiceObj As New StringBuilder()
            Dim order As New Order()
            If salesOrderResponse IsNot Nothing Then
                order.MessageText = salesOrderResponse.SalesOrderResponse1.MessageText
                order.MessageType = salesOrderResponse.SalesOrderResponse1.Messagetype
                debugMessage &= $"OrderManager.SimulateOrder - SalesOrderResponse found. MessageType: {order.MessageType}, MessageText: {order.MessageText}.{Environment.NewLine}"
                objServiceObj.Append(order.MessageText)
                If order.MessageType = "S" Then
                    If salesOrderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation Is Nothing Then
                        debugMessage &= $"OrderManager.SimulateOrder - No LineConfirmation. Message Text: {order.MessageText}.{Environment.NewLine}"
                        Dim intcount As Integer = Integer.Parse(HttpContext.Current.Session("ShiptoCount").ToString())
                        If intcount > 1 Then
                            Dim objEx As New ServicesPlusBusinessException("The ship to account you selected is not available, please select another account")
                            objEx.setcustomMessage = False
                            Throw objEx
                        Else
                            Dim objEx As New ServicesPlusBusinessException("The ship to account you selected is not available")
                            objEx.setcustomMessage = False
                            Throw objEx
                        End If
                    End If
                    order.OrderStatus = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.OrderStatus
                    order.Customer = New Customer()
                    order.Customer.EmailAddress = customer.EmailAddress
                    order.ShipTo = ship_to.Address
                    order.BillTo = bill_to.BillTo
                    order.Customer.SIAMIdentity = customer.SIAMIdentity
                    order.OrderNumber = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.SalesDocument
                    order.Paymentcardresulttext = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardResultText
                    order.OrderDate = Date.Now
                    order.ShippingMethod = shipping_method
                    order.VerisgnAuthorizationCode = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardAuthorizationNumber
                    ' OLD CODE
                    'Utilities.LogMessages("OrderManager.SimulateOrder - OrderConfirmation.TotalOrderAmount: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount)
                    Try
                        order.AllocatedGrandTotal = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount
                    Catch ex As Exception
                        order.AllocatedGrandTotal = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount)
                    End Try

                    'Utilities.LogMessages("OrderManager.SimulateOrder - OrderConfirmation.TotalShippingHandling: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling)
                    Try
                        order.AllocatedShipping = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling
                    Catch ex As Exception
                        order.AllocatedShipping = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling)
                    End Try

                    'Utilities.LogMessages("OrderManager.SimulateOrder - OrderConfirmation.TotalRecyclingFee: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee)
                    Try
                        order.AllocatedSpecialTax = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee
                    Catch ex As Exception
                        order.AllocatedSpecialTax = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee)
                    End Try

                    'Utilities.LogMessages("OrderManager.SimulateOrder - OrderConfirmation.TotalPartSubTotal: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal)
                    Try
                        order.AllocatedPartSubTotal = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal
                    Catch ex As Exception
                        order.AllocatedPartSubTotal = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal)
                    End Try

                    'Utilities.LogMessages("OrderManager.SimulateOrder - OrderConfirmation.TotalTaxAmount: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount)
                    Try
                        order.AllocatedTax = salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount
                    Catch ex As Exception
                        order.AllocatedTax = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount)
                    End Try
                    ' NEW CODE
                    'Try
                    '    If String.IsNullOrEmpty(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount) Then
                    '        order.AllocatedGrandTotal = 0.0
                    '    Else
                    '        order.AllocatedGrandTotal = Convert.ToDouble(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount, usCulture)
                    '    End If
                    'Catch ex As Exception
                    '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Sales Order Grand Total: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount)
                    '    order.AllocatedGrandTotal = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount)
                    'End Try

                    'Try
                    '    order.AllocatedShipping = Convert.ToDouble(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling, usCulture)
                    'Catch ex As Exception
                    '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Sales Order Allocated Shipping: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling)
                    '    order.AllocatedShipping = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling)
                    'End Try

                    'Try
                    '    order.AllocatedSpecialTax = Convert.ToDouble(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee, usCulture)
                    'Catch ex As Exception
                    '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Sales Order Recycling Fee: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee)
                    '    order.AllocatedSpecialTax = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee)
                    'End Try

                    'Try
                    '    order.AllocatedPartSubTotal = Convert.ToDouble(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal, usCulture)
                    'Catch ex As Exception
                    '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Sales Order Sub Total: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal)
                    '    order.AllocatedPartSubTotal = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal)
                    'End Try

                    'Try
                    '    order.AllocatedTax = Convert.ToDouble(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount, usCulture)
                    'Catch ex As Exception
                    '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Sales Order Allocated Tax: " & salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount)
                    '    order.AllocatedTax = Val(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount)
                    'End Try

                    order.YourReference = objOrderDetails.YourReference
                    order.Customer = New Customer
                    order.Customer.SequenceNumber = IIf(cart.Customer IsNot Nothing, cart.Customer.SequenceNumber, "0")
                    If salesOrderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation Is Nothing Then Throw New ServicesPlusBusinessException("SAP did not return a Line Confirmation during Order Simulation.")
                    arrOrderLineItem = New OrderLineItem(salesOrderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation.Count - 1) {}

                    Dim iLine As Integer = 0
                    For Each sapLineItem As OrderInjection.LineConfirmation In salesOrderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation
                        Dim orderItem As New OrderLineItem(order)
                        ' OLD CODE
                        'Utilities.LogMessages("OrderManager.SimulateOrder - sapLineItem.OrderQty: " & sapLineItem.OrderQty)
                        Try
                            orderItem.Quantity = IIf(Not String.IsNullOrEmpty(sapLineItem.OrderQty), sapLineItem.OrderQty, 0)
                        Catch ex As Exception
                            orderItem.Quantity = Val(IIf(Not String.IsNullOrEmpty(sapLineItem.OrderQty), sapLineItem.OrderQty, 0))
                        End Try

                        'Utilities.LogMessages("OrderManager.SimulateOrder - sapLineItem.BackOrderQty: " & sapLineItem.BackOrderQty)
                        Try
                            orderItem.OrderQty = IIf(Not String.IsNullOrEmpty(sapLineItem.BackOrderQty), sapLineItem.BackOrderQty, sapLineItem.ConfirmedQty)
                        Catch ex As Exception
                            orderItem.OrderQty = Val(IIf(Not String.IsNullOrEmpty(sapLineItem.BackOrderQty), sapLineItem.BackOrderQty, sapLineItem.ConfirmedQty))
                        End Try

                        'Utilities.LogMessages("OrderManager.SimulateOrder - sapLineItem.ConfirmedQty: " & sapLineItem.ConfirmedQty)
                        Try
                            orderItem.ConfirmedQty = IIf(Not String.IsNullOrEmpty(sapLineItem.ConfirmedQty), sapLineItem.ConfirmedQty, 0)
                        Catch ex As Exception
                            orderItem.ConfirmedQty = Val(IIf(Not String.IsNullOrEmpty(sapLineItem.ConfirmedQty), sapLineItem.ConfirmedQty, 0))
                        End Try
                        ' NEW CODE
                        'If Not Integer.TryParse(sapLineItem.OrderQty, numStyle, usCulture, orderItem.Quantity) Then
                        '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Quantity: " & sapLineItem.OrderQty)
                        '    orderItem.Quantity = 0
                        'End If

                        'If String.IsNullOrEmpty(sapLineItem.BackOrderQty) Then
                        '    ' If Back Order Quantity is null, set Order Quantity to Confirmed Quantity
                        '    If Not Integer.TryParse(sapLineItem.ConfirmedQty, numStyle, usCulture, orderItem.OrderQty) Then
                        '        Utilities.LogMessages("OrderManager.SimulateOrder - BackOrder is null - Failed to parse Order Quantity: " & sapLineItem.OrderQty)
                        '        orderItem.OrderQty = 0
                        '    End If
                        'ElseIf Not Integer.TryParse(sapLineItem.BackOrderQty, numStyle, usCulture, orderItem.OrderQty) Then
                        '    ' Otherwise, try to parse Back Order Quantity.
                        '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Back Order Quantity: " & sapLineItem.BackOrderQty & ", Confirmed Quantity: " & sapLineItem.ConfirmedQty)
                        '    orderItem.OrderQty = 0
                        'End If

                        'If String.IsNullOrEmpty(sapLineItem.ConfirmedQty) Then
                        '    orderItem.ConfirmedQty = 0
                        'ElseIf Not Integer.TryParse(sapLineItem.ConfirmedQty, numStyle, usCulture, orderItem.ConfirmedQty) Then
                        '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Confirmed Quantity: " & sapLineItem.ConfirmedQty)
                        '    orderItem.ConfirmedQty = Integer.Parse(sapLineItem.ConfirmedQty, numStyle, usCulture)
                        'End If

                        orderItem.Product = New Part
                        ' ASleight - Changes to keep track of original and replaced part numbers
                        'For Each partNumberPair As String() In partNumberList
                        '    If partNumberPair(1) = sapLineItem.PartShipped Then
                        '        orderItem.Product.PartNumber = partNumberPair(0)
                        '        Exit For
                        '    End If
                        'Next
                        If (replacedPartList.ContainsKey(sapLineItem.PartShipped)) Then
                            orderItem.Product.PartNumber = replacedPartList.Item(sapLineItem.PartShipped).ToString()   ' Fetch the "original" part number from the HashTable we created earlier
                        Else
                            orderItem.Product.PartNumber = sapLineItem.PartShipped
                        End If
                        orderItem.Product.ReplacementPartNumber = sapLineItem.PartShipped
                        orderItem.Product.IsReplaced = (sapLineItem.PartShipped <> sapLineItem.PartOrdered) ' If the two differ, the part has been replaced.
                        orderItem.Product.Description = sapLineItem.ShortTextItem

                        'If String.IsNullOrEmpty(sapLineItem.PartPrice) Then
                        '    orderItem.Product.ListPrice = 0
                        'ElseIf Not Double.TryParse(sapLineItem.PartPrice, numStyle, usCulture, orderItem.Product.ListPrice) Then
                        '    Utilities.LogMessages("OrderManager.SimulateOrder - Failed to parse Part Price: " & sapLineItem.PartPrice)
                        '    orderItem.Product.ListPrice = Double.Parse(sapLineItem.PartPrice, numStyle, usCulture)
                        'End If
                        'Utilities.LogMessages("OrderManager.SimulateOrder - sapLineItem.PartPrice: " & sapLineItem.PartPrice)
                        Try
                            orderItem.Product.ListPrice = sapLineItem.PartPrice
                        Catch ex As Exception
                            orderItem.Product.ListPrice = Val(sapLineItem.PartPrice)
                        End Try
                        arrOrderLineItem(iLine) = orderItem
                        iLine = iLine + 1
                    Next
                    order.LineItems1 = arrOrderLineItem
                    order.SAPBillToAccountNumber = bill_to.AccountNumber
                    order.ContainsDownloadAndShipItem = cart.ContainsDownloadAndShippingItems
                    order.BillTo = bill_to.BillTo
                    order.AccountNumber = bill_to.AccountNumber
                    order.ShipTo = ship_to.Address
                    order.POReference = cart.PurchaseOrderNumber
                    order.Customer.CustomerID = customer.CustomerID
                    order.soldToAccount = bill_to.SoldToAccount
                End If
            Else
                order.MessageText = "Exception in connecting to SAP Webmethods"
                order.MessageType = "E"
            End If
            Return order
        Catch ex As Exception
            Utilities.LogMessages(debugMessage)
            Throw ex
        End Try
    End Function


    'Public Function Checkout1(ByRef customer As Customer, ByRef cart As ShoppingCart, ByRef bill_to As Account, ByRef bill_to_overriden As Boolean, ByRef ship_to As Account, ByRef ship_to_overriden As Boolean, ByRef shipping_method As ShippingMethod, Optional ByVal IsCreditCardOrder As Boolean = False, Optional ByRef Order_Credit_Card As CreditCard = Nothing) As Sony.US.ServicesPLUS.Core.Order
    '    Dim billing_only As Boolean = cart.ContainsDownloadOnlyItems
    '    Dim SAP_order As Object
    '    Dim salesOderResponse As New SalesOrder.SalesOrderResponse()
    '    Dim objSAPhelper As New SAPHelper
    '    Dim objOrderDetails As New SalesOrder.OrderDetails()
    '    Dim objshipinformation As New SalesOrder.ShipInformation()
    '    Dim objBillInformation As New SalesOrder.BillInformation()
    '    Dim objsalesRequest1 As New SalesOrder.SalesOrderRequest1()
    '    Dim objlineDetails As New SalesOrder.LineDetails()
    '    Dim arrLinedetails As SalesOrder.LineDetails()
    '    arrLinedetails = New SalesOrder.LineDetails(cart.ShoppingCartItems.Count - 1) {}
    '    Dim objPCILogger As PCILogger = New PCILogger()
    '    Dim arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
    '    arrOrderLineItem = New Sony.US.ServicesPLUS.Core.OrderLineItem(cart.ShoppingCartItems.Count - 1) {}
    '    Try
    '        SAP_order = New WebMethods.Services.Order
    '        'Do not send the Billing informaion if it is not the Credit Order
    '        objBillInformation.SoldTo = bill_to.AccountNumber.PadLeft(10, "0")

    '        'Check if It is Credit Order
    '        If IsCreditCardOrder Then
    '            'objOrderDetails.CardName = bill_to.BillTo.Name
    '            'objOrderDetails.CardNumber = Order_Credit_Card.CreditCardNumber
    '            'objOrderDetails.CardType = Order_Credit_Card.Type.Description
    '            'objOrderDetails.CardVerificationValue = Order_Credit_Card.CSCCode
    '            'objOrderDetails.ValidTo = Convert.ToDateTime(Order_Credit_Card.ExpirationDate).ToString("MMyy")

    '            ''In case of Credit Card Account order override the Billing information
    '            'objBillInformation.BillToName1 = SAP_order.BillTo.Attn
    '            'objBillInformation.City = SAP_order.BillTo.City
    '            'objBillInformation.Country = "US"
    '            'objBillInformation.District = SAP_order.BillTo.Address3
    '            'objBillInformation.Email = SAP_order.Customer.EmailAddress
    '            'objBillInformation.State = SAP_order.BillTo.State
    '            'objBillInformation.Street = SAP_order.BillTo.Address1
    '            'objBillInformation.Street2 = SAP_order.BillTo.Address2
    '            'objBillInformation.ZipCode = SAP_order.BillTo.Zip

    '        Else
    '            objOrderDetails.CardName = String.Empty
    '            objOrderDetails.CardNumber = String.Empty
    '            objOrderDetails.CardType = String.Empty
    '            objOrderDetails.CardVerificationValue = String.Empty
    '        End If
    '        objOrderDetails.BillInformation = objBillInformation

    '        objOrderDetails.CustomerPONumber = cart.PurchaseOrderNumber

    '        objOrderDetails.GroupCode = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.GroupCode, shipping_method.SISPickGroupCode)
    '        objOrderDetails.OrderEntryPerson = customer.SIAMIdentity
    '        ''TODO:DropShip Indicator
    '        objOrderDetails.ReasonCode = "Y"
    '        ''TODO :D
    '        objOrderDetails.TaxExempt = ship_to.TaxExempt
    '        ''TODO :YourReference
    '        objOrderDetails.YourReference = GetUniqueKey()

    '        ''objlineDetails
    '        Dim i As Integer = 0
    '        For Each item As ShoppingCartItem In cart.ShoppingCartItems
    '            objlineDetails = New SalesOrder.LineDetails()
    '            objlineDetails.OrderQuantity = item.Quantity.ToString
    '            objlineDetails.PartNumber = item.Product.PartNumber
    '            objlineDetails.Carrier = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.CarrierCode, shipping_method.SISCarrierCode)
    '            arrLinedetails(i) = objlineDetails
    '            i = i + 1
    '        Next
    '        objOrderDetails.LineDetails = arrLinedetails


    '        'Shiping Information
    '        objshipinformation.BusExt = ship_to.Address.PhoneExt
    '        objshipinformation.City = ship_to.Address.City

    '        objshipinformation.District = ship_to.Address.Address3
    '        objshipinformation.Fax = cart.Customer.FaxNumber
    '        objshipinformation.FirstTelephoneNumber = ship_to.Address.PhoneNumber
    '        ''TODO:SAP ShipTO
    '        objshipinformation.ShipTo = ship_to.Address.SapAccountNumber
    '        objshipinformation.ShipToName1 = ship_to.Address.Name
    '        objshipinformation.ShipToName3 = ship_to.Address.Attn
    '        objshipinformation.State = ship_to.Address.State
    '        objshipinformation.Street = ship_to.Address.Address1
    '        objshipinformation.Street2 = ship_to.Address.Address2
    '        objshipinformation.ZipCode = ship_to.Address.Zip
    '        objOrderDetails.ShipInformation = objshipinformation
    '        ''Simulation X is for simucatin
    '        objOrderDetails.Input3 = "X"
    '        objsalesRequest1.OrderDetails = objOrderDetails
    '        salesOderResponse = objSAPhelper.SalesOrderInjection(objOrderDetails)

    '        'Console.WriteLine("Order Created on SIS - order number: " + sis_new_order.OrderNumber)
    '    Catch ex As Exception
    '        'Console.WriteLine("CreateAccountOrder Failed on SIS")
    '        'log
    '        Throw ex
    '    End Try
    '    Dim objServiceObj As New StringBuilder()
    '    Dim order As New Sony.US.ServicesPLUS.Core.Order()
    '    If Not salesOderResponse Is Nothing Then
    '        order.MessageText = salesOderResponse.SalesOrderResponse1.MessageText
    '        order.MessageType = salesOderResponse.SalesOrderResponse1.Messagetype
    '        objServiceObj.Append(order.MessageText)
    '        If salesOderResponse.SalesOrderResponse1.Messagetype = "S" Then
    '            order.OrderStatus = salesOderResponse.SalesOrderResponse1.OrderConfirmation.OrderStatus
    '            order.Customer = New Customer()
    '            order.Customer.EmailAddress = customer.EmailAddress
    '            order.ShipTo = ship_to.Address
    '            order.BillTo = bill_to.BillTo
    '            order.Customer.SIAMIdentity = customer.SIAMIdentity
    '            order.OrderNumber = salesOderResponse.SalesOrderResponse1.OrderConfirmation.SalesDocument

    '            order.Paymentcardresulttext = salesOderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardResultText
    '            order.OrderDate = DateTime.Now
    '            order.ShippingMethod = shipping_method
    '            order.VerisgnAuthorizationCode = salesOderResponse.SalesOrderResponse1.OrderConfirmation.PaymentCardAuthorizationNumber
    '            order.AllocatedGrandTotal = salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalOrderAmount
    '            order.AllocatedShipping = salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalShippingHandling
    '            order.AllocatedSpecialTax = salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalRecyclingFee
    '            order.AllocatedPartSubTotal = salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalPartSubTotal
    '            order.AllocatedTax = salesOderResponse.SalesOrderResponse1.OrderConfirmation.TotalTaxAmount
    '            order.YourReference = objOrderDetails.YourReference
    '            order.Customer = New Sony.US.ServicesPLUS.Core.Customer
    '            order.Customer.SequenceNumber = IIf(Not cart.Customer Is Nothing, cart.Customer.SequenceNumber, 0)
    '            Dim iLIne As Integer = 0
    '            For Each i As SalesOrder.LineConfirmation In salesOderResponse.SalesOrderResponse1.OrderConfirmation.LineConfirmation
    '                Dim objLineItem As New OrderLineItem(order)

    '                objLineItem.Quantity = IIf(Not String.IsNullOrEmpty(i.OrderQty), i.OrderQty, 0)
    '                objLineItem.ConfirmedQty = IIf(Not String.IsNullOrEmpty(i.ConfirmedQty), i.ConfirmedQty, 0)
    '                objLineItem.Product = New Sony.US.ServicesPLUS.Core.Part
    '                objLineItem.Product.PartNumber = i.PartOrdered
    '                objLineItem.Product.Description = i.ShortTextItem
    '                objLineItem.Product.ListPrice = i.PartPrice
    '                arrOrderLineItem(iLIne) = objLineItem
    '                iLIne = iLIne + 1
    '            Next

    '            order.LineItems1 = arrOrderLineItem
    '            order.SAPBillToAccountNumber = bill_to.AccountNumber
    '            order.ContainsDownloadAndShipItem = cart.ContainsDownloadAndShippingItems
    '            order.BillTo = bill_to.BillTo
    '            order.AccountNumber = bill_to.AccountNumber
    '            order.ShipTo = ship_to.Address
    '            order.POReference = cart.PurchaseOrderNumber
    '            order.Customer.CustomerID = customer.CustomerID

    '        End If
    '    Else
    '        order.MessageText = "Exception in connecting to SAP Webmethods"
    '        order.MessageType = "E"
    '    End If
    '    Return order
    'End Function

    Private Sub LogSoftwareItems(ByRef order As Order)
        Dim odm As New OrderDataManager
        Dim txn_context As TransactionContext = Nothing
        Try
            txn_context = odm.BeginTransaction
            For Each detail As OrderLineItem In order.LineItems
                If detail.Product.Type = Product.ProductType.Software Then
                    Dim software = CType(detail.Product, Software)
                    Dim downloadRecord As New DownloadRecord With {
                        .KitPartNumber = software.KitPartNumber,
                        .SIAMID = order.Customer.SIAMIdentity,
                        .Time = Now,
                        .DeliveryMethod = IIf(detail.Downloadable, "Download", "Ship")
                    }
                    odm.StoreDownloadRecord(downloadRecord, txn_context)
                End If
            Next
            odm.CommitTransaction(txn_context)
        Catch ex As Exception
            odm.RollbackTransaction(txn_context)
        End Try
    End Sub

    Public Sub CompleteOrder(ByRef order As Order, ByVal cart As ShoppingCart, Optional ByVal TaxExempt As String = " ", Optional ByRef objGlobalData As GlobalData = Nothing)
        Dim objOrder As Order = CreateSalesOrder(order, cart, TaxExempt, objGlobalData)

        If objOrder Is Nothing Then Throw New ArgumentException("Order not created in SAP")
        If objOrder.MessageType = "S" Then
            StoreOrder(objOrder, False)     ' Add Order to database if the SAP Order is successful
        Else
            Dim objEx As New ServicesPlusBusinessException(objOrder.MessageText)
            objEx.errorMessage = objOrder.MessageText
            Throw objEx     ' Throw an exception with the Order's returned message.
        End If
    End Sub

    Private Function CreateSalesOrder(ByVal SAP_order As Order, ByVal cart As ShoppingCart, Optional ByVal TaxExempt As String = " ", Optional ByRef objGlobalData As GlobalData = Nothing) As Order
        Dim salesOderResponse As New OrderInjection.SalesOrderResponse()
        Dim objOrderDetails As New OrderInjection.OrderDetails()
        Dim objshipinformation As New OrderInjection.ShipInformation()
        Dim objBillInformation As New OrderInjection.BillInformation()
        Dim objsalesRequest1 As New OrderInjection.SalesOrderRequest1()
        Dim sapLineDetails As OrderInjection.LineDetails
        Dim arrLinedetails As OrderInjection.LineDetails()
        Dim objSAPHelper As New SAPHelper()
        Dim arrOrderLineItem As OrderLineItem()
        Dim debugMessage As String = ""

        arrOrderLineItem = New OrderLineItem(SAP_order.LineItems1.Count - 1) {}

        Try
            If SAP_order.Customer.UserType = "A" Or SAP_order.Customer.UserType = "P" Then '6668
                ''objBillInformation.SoldTo = IIf(SAP_order.AccountNumber.Length < 10, SAP_order.AccountNumber.PadLeft(10, "0"), SAP_order.AccountNumber)
                objBillInformation.SoldTo = SAP_order.soldToAccount
                objOrderDetails.Input4 = IIf(SAP_order.AccountNumber.Length < 10, SAP_order.AccountNumber.PadLeft(10, "0"), SAP_order.AccountNumber)
            Else
                objBillInformation.SoldTo = IIf(SAP_order.AccountNumber.Length < 10, SAP_order.AccountNumber.PadLeft(10, "0"), SAP_order.AccountNumber)
                ''objOrderDetails.Input4 = IIf(SAP_order.AccountNumber.Length < 10, SAP_order.AccountNumber.PadLeft(10, "0"), SAP_order.AccountNumber)
            End If

            'Order Details
            If Not SAP_order.CreditCard Is Nothing Then
                objOrderDetails.CardName = SAP_order.BillTo.Attn
                If (SAP_order.CreditCard.CreditCardNumber.StartsWith("-E803")) Then
                    objOrderDetails.CardNumber = SAP_order.CreditCard.CreditCardNumber
                    'If SAP_order.Customer.UserType = "A" To fix bugId 8804 
                    '    objOrderDetails.CardName = SAP_order.CreditCard.NickName
                    'End If
                Else
                    objOrderDetails.CardNumber = "INVALID TOKEN"
                    Utilities.LogMessages("Invalid Token for the email addessss for user " & SAP_order.Customer.EmailAddress)
                End If

                'Select Case SAP_order.CreditCard.Type.Description.ToUpper().Trim()
                '    Case "VISA"
                '        objOrderDetails.CardType = "VISA"
                '    Case "MASTERCARD"
                '        objOrderDetails.CardType = "MC"
                '    Case "DISCOVER"
                '        objOrderDetails.CardType = "DISC"
                '    Case "AMERICAN EXPRESS"
                '        objOrderDetails.CardType = "AMEX"
                'End Select
                objOrderDetails.CardType = SAP_order.CreditCard.Type.CardType.ToUpper()
                objOrderDetails.CardVerificationValue = SAP_order.CreditCard.CSCCode
                debugMessage &= $"OrderManager.CreateSalesOrder - Getting CC Expiry Date. Raw value = {SAP_order.CreditCard.ExpirationDate}{Environment.NewLine}"
                objOrderDetails.ValidTo = Convert.ToDateTime(SAP_order.CreditCard.ExpirationDate, usCulture).ToString("MMyy")

                'In case of Credit Card Account order override the Billing information
                objBillInformation.BillToName1 = SAP_order.BillTo.Attn
                objBillInformation.BillToName2 = SAP_order.BillTo.Attn
                objBillInformation.BillToName3 = SAP_order.BillTo.Attn
                objBillInformation.City = SAP_order.BillTo.City
                If String.IsNullOrEmpty(SAP_order.BillTo.Country) Then
                    objBillInformation.Country = objGlobalData.SalesOrganization.Substring(0, 2)
                Else
                    objBillInformation.Country = SAP_order.BillTo.Country
                End If

                objBillInformation.District = SAP_order.BillTo.Address3
                objBillInformation.Email = SAP_order.Customer.EmailAddress
                objBillInformation.State = SAP_order.BillTo.State
                objBillInformation.Street = SAP_order.BillTo.Address1
                objBillInformation.Street2 = SAP_order.BillTo.Address2
                objBillInformation.ZipCode = SAP_order.BillTo.Zip
                objOrderDetails.Input4 = Nothing
            End If

            objOrderDetails.BillInformation = objBillInformation

            objOrderDetails.CustomerPONumber = cart.PurchaseOrderNumber
            objOrderDetails.GroupCode = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.GroupCode, SAP_order.ShippingMethod.SISPickGroupCode)
            objOrderDetails.OrderEntryPerson = SAP_order.Customer.SIAMIdentity
            ''TODO:DropShip Indicator
            objOrderDetails.ReasonCode = "Y"

            objOrderDetails.TaxExempt = TaxExempt
            ''TODO :YourReference
            objOrderDetails.YourReference = GetUniqueKey()
            objOrderDetails.Input1 = IIf(cart.ContainsDownloadOnlyItems, ConfigurationData.GeneralSettings.BillingOnly.CarrierCode, SAP_order.ShippingMethod.SISCarrierCode) ''SAP_order.ShippingMethod.SISCarrierCode
            ''objlineDetails
            Dim i As Integer = 0
            arrLinedetails = New OrderInjection.LineDetails(cart.ShoppingCartItems.Count - 1) {}
            For Each item As ShoppingCartItem In cart.ShoppingCartItems
                sapLineDetails = New OrderInjection.LineDetails With {
                    .OrderQuantity = item.Quantity.ToString(),
                    .PartNumber = item.Product.ReplacementPartNumber
                }
                'objlineDetails.PartNumber = item.Product.PartNumber   ' ASleight - Product.PartNumber now stores "original" part while Replacement carries the proper part to order.
                If (Not String.IsNullOrEmpty(item.Product.PartNumber)) AndAlso (item.Product.PartNumber <> item.Product.ReplacementPartNumber) Then
                    sapLineDetails.ItemComments = "This part was supplied as a replacement for " & item.Product.PartNumber & ", which you originally searched for."
                End If
                'objlineDetails.Carrier = SAP_order.ShippingMethod.SISCarrierCode
                arrLinedetails(i) = sapLineDetails
                i += 1
            Next
            objOrderDetails.LineDetails = arrLinedetails


            'Shiping Information
            objshipinformation.BusExt = SAP_order.ShipTo.PhoneExt
            objshipinformation.City = SAP_order.ShipTo.City

            objshipinformation.District = SAP_order.ShipTo.Address3
            objshipinformation.Fax = cart.Customer.FaxNumber
            objshipinformation.FirstTelephoneNumber = SAP_order.ShipTo.PhoneNumber
            ''TODO:SAP ShipTO
            objshipinformation.ShipTo = IIf(SAP_order.ShipToAccount.SAPAccount.Length < 10, SAP_order.ShipToAccount.SAPAccount.PadLeft(10, "0"), SAP_order.ShipToAccount.SAPAccount)
            objshipinformation.Email = SAP_order.Customer.EmailAddress
            objshipinformation.ShipToName1 = SAP_order.ShipTo.Name
            objshipinformation.ShipToName3 = SAP_order.ShipTo.Attn
            objshipinformation.State = SAP_order.ShipTo.State
            objshipinformation.Street = SAP_order.ShipTo.Address1
            objshipinformation.Street2 = SAP_order.ShipTo.Address2
            objshipinformation.ZipCode = SAP_order.ShipTo.Zip
            If String.IsNullOrEmpty(SAP_order.ShipTo.Country) Then
                objshipinformation.Country = objGlobalData.SalesOrganization.Substring(0, 2)
            Else
                objshipinformation.Country = SAP_order.ShipTo.Country
            End If
            objOrderDetails.ShipInformation = objshipinformation

            ''Simulation X is for simucatin
            objOrderDetails.Input3 = ""
            objOrderDetails.OrderEntryPerson = SAP_order.Customer.SIAMIdentity

            objsalesRequest1.OrderDetails = objOrderDetails
            salesOderResponse = objSAPHelper.SalesOrderInjection(objOrderDetails, objGlobalData)
            CloneSAPOrder(salesOderResponse.SalesOrderResponse1, SAP_order, cart)
        Catch ex As Exception
            Utilities.LogMessages(debugMessage)
            Throw ex
        End Try

        Return SAP_order
    End Function

    Private Sub CloneSAPOrder(ByVal sapResponse As OrderInjection.SalesOrderResponse1, ByRef newOrder As Order, ByRef cart As ShoppingCart)
        Dim replacedPartList As New Hashtable
        Dim orderItem As OrderLineItem
        Dim iLIne As Integer = 0

        If sapResponse.OrderConfirmation Is Nothing And sapResponse.Messagetype = "S" Then
            Throw New ServicesPlusException.ServicesPLUSWebMethodException("Line Item not populated from the SAP response in Order Injection")
        End If
        If HttpContext.Current.Session("replacedPartList") IsNot Nothing Then
            replacedPartList = HttpContext.Current.Session("replacedPartList")
        End If

        newOrder.MessageText = sapResponse.MessageText
        newOrder.MessageType = sapResponse.Messagetype
        If sapResponse.Messagetype = "S" Then
            newOrder.OrderStatus = sapResponse.OrderConfirmation.OrderStatus
            newOrder.OrderNumber = sapResponse.OrderConfirmation.SalesDocument
            If Not newOrder.OrderStatus.Equals("Restricted Party") Then
                Dim arrOrderLineItem As Sony.US.ServicesPLUS.Core.OrderLineItem()
                arrOrderLineItem = New Sony.US.ServicesPLUS.Core.OrderLineItem(sapResponse.OrderConfirmation.LineConfirmation.Count - 1) {}
                newOrder.OrderNumber = sapResponse.OrderConfirmation.SalesDocument

                newOrder.Paymentcardresulttext = sapResponse.OrderConfirmation.PaymentCardResultText
                newOrder.OrderDate = Date.Now
                'Order.ShippingMethod = shipping_method
                newOrder.VerisgnAuthorizationCode = sapResponse.OrderConfirmation.PaymentCardAuthorizationNumber
                newOrder.AllocatedGrandTotal = Convert.ToDouble(sapResponse.OrderConfirmation.TotalOrderAmount, usCulture)
                newOrder.AllocatedShipping = Convert.ToDouble(sapResponse.OrderConfirmation.TotalShippingHandling, usCulture)
                newOrder.AllocatedSpecialTax = Convert.ToDouble(sapResponse.OrderConfirmation.TotalRecyclingFee, usCulture)
                newOrder.AllocatedPartSubTotal = Convert.ToDouble(sapResponse.OrderConfirmation.TotalPartSubTotal, usCulture)
                newOrder.AllocatedTax = Convert.ToDouble(sapResponse.OrderConfirmation.TotalTaxAmount, usCulture)
                'lo.YourReference = cart.Customer.objOrderDetails.YourReference
                newOrder.Customer = New Customer
                newOrder.Customer.SequenceNumber = IIf(Not cart.Customer Is Nothing, cart.Customer.SequenceNumber, 0)
                For Each sapItem As OrderInjection.LineConfirmation In sapResponse.OrderConfirmation.LineConfirmation
                    orderItem = New OrderLineItem(newOrder) With {
                        .Quantity = IIf(Not String.IsNullOrEmpty(sapItem.OrderQty), sapItem.OrderQty, 0),
                        .ConfirmedQty = IIf(Not String.IsNullOrEmpty(sapItem.ConfirmedQty), sapItem.ConfirmedQty, 0),
                        .LineNumber = sapItem.LineNumber,
                        .Product = New Part With {
                            .ReplacementPartNumber = sapItem.PartOrdered,
                            .Description = sapItem.ShortTextItem,
                            .ListPrice = Convert.ToDouble(sapItem.PartPrice, usCulture)
                        }
                    }
                    'objLineItem.Product.PartNumber = i.PartOrdered
                    If (replacedPartList IsNot Nothing) AndAlso (replacedPartList.Item(sapItem.PartOrdered) IsNot Nothing) Then orderItem.Product.PartNumber = replacedPartList.Item(sapItem.PartOrdered)
                    arrOrderLineItem(iLIne) = orderItem
                    iLIne = iLIne + 1
                Next

                newOrder.LineItems1 = arrOrderLineItem
                'Order.SAPBillToAccountNumber = bill_to.AccountNumber
                newOrder.ContainsDownloadAndShipItem = cart.ContainsDownloadAndShippingItems
                'lo.BillTo = cart.Customer.
                'Order.AccountNumber = bill_to.AccountNumber
                'lo.ShipTo = ship_to.ShipTo
                newOrder.POReference = cart.PurchaseOrderNumber
                newOrder.Customer.CustomerID = cart.Customer.CustomerID
                newOrder.Customer.CountryCode = cart.Customer.CountryCode
            End If
        End If
    End Sub

    Private Function GetUniqueKey() As String
        Dim maxSize As Integer = 12
        Dim chars As Char() = New Char(61) {}
        Dim a As String
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        chars = a.ToCharArray()
        Dim size As Integer = maxSize
        Dim data As Byte() = New Byte(0) {}
        Dim crypto As New RNGCryptoServiceProvider()
        crypto.GetNonZeroBytes(data)
        size = maxSize
        data = New Byte(size - 1) {}
        crypto.GetNonZeroBytes(data)
        Dim result As New StringBuilder(size)
        For Each b As Byte In data
            result.Append(chars(b Mod (chars.Length - 1)))
        Next
        Return result.ToString()
    End Function

    Public Function CompleteBackOrderRelease_AuthorizeCreditCard(ByRef order As Order, ByVal tax_exempt As String, Optional ByVal CreditCardValidation As Boolean = True) As Boolean
        Dim avs_address_result As String = ""
        Dim avs_zip_result As String = ""
        Dim csc_result As String = ""
        Dim verisign_authorization_code As String = ""
        Dim credit_card_success As Boolean = False
        Dim pm As New PaymentManager

        Try
            Dim strAccount As String = ""
            If Not order.SAPBillToAccountNumber Is Nothing Then
                strAccount = order.SAPBillToAccountNumber
            End If
            Try
                'If order.Customer.UserType = "AccountHolder" And order.Customer.IsActive = True Then'6668
                If (order.Customer.UserType = "A" Or order.Customer.UserType = "P") And order.Customer.IsActive = True Then '6668
                    'Commented By Navdeep on 19th may for resolving 6397
                    'verisign_transaction_code = pm.AuthorizeCreditCard(order.CreditCard.CreditCardNumber, order.CreditCard.ExpirationDate, order.AllocatedGrandTotal, order.OrderNumber, "cc order", order.BillTo.Line1, order.BillTo.PostalCode, order.CreditCard.CSCCode, order.BillTo.Attn, "", avs_address_result, avs_zip_result, csc_result, verisign_authorization_code, strAccount)
                    'avs_address_result = "Y"
                    'avs_zip_result = "Y"
                Else
                    Console.WriteLine("order.CreditCard Is Nothing " + (order.CreditCard Is Nothing).ToString())
                    If Not order.CreditCard Is Nothing Then
                        Console.WriteLine("order.CreditCard.CreditCardNumber Is Nothing " + (order.CreditCard.CreditCardNumber Is Nothing).ToString())
                        Console.WriteLine("order.CreditCard.ExpirationDate Is Nothing " + (order.CreditCard.ExpirationDate Is Nothing).ToString())
                        Console.WriteLine("order.CreditCard.CSCCode Is Nothing " + (order.CreditCard.CSCCode Is Nothing).ToString())
                        Console.WriteLine("order.AllocatedGrandTotal Is Zero " + (order.AllocatedGrandTotal).ToString())
                        Console.WriteLine("order.BillTo Is Nothing " + (order.BillTo Is Nothing).ToString())
                    End If
                    'Commented By Navdeep on 19th may for resolving 6397
                    'verisign_transaction_code = pm.AuthorizeCreditCard(order.CreditCard.CreditCardNumber, order.CreditCard.ExpirationDate, order.AllocatedGrandTotal, order.OrderNumber, "cc order", order.BillTo.Line1, order.BillTo.PostalCode, order.CreditCard.CSCCode, order.BillTo.Attn, "", avs_address_result, avs_zip_result, csc_result, verisign_authorization_code, strAccount)
                End If
            Catch ex As Exception
                'Commented By Navdeep on 19th may for resolving 6397
                'If Not verisign_transaction_code Is Nothing Then
                '    If verisign_transaction_code.Trim() <> "" Then
                '        pm.VoidCreditCardTransaction(verisign_transaction_code, order.OrderNumber, "", "")
                '    End If
                'End If
                Throw ex
            End Try

            If CreditCardValidation Then 'In CreditCardException processing no Address validation required
                If avs_address_result.ToUpper() = "N" Or avs_zip_result.ToUpper() = "N" Then
                    'Commented By Navdeep on 19th may for resolving 6397
                    'If verisign_transaction_code.Trim() <> "" Then
                    '    pm.VoidCreditCardTransaction(verisign_transaction_code, order.OrderNumber, "", "")
                    'End If
                    credit_card_success = False
                    Throw New Exception("Invalid bill to address")
                End If
                If csc_result.ToUpper() = "N" Then
                    credit_card_success = False
                    Throw New Exception("Invalid CVV code")
                End If
            End If
            'Commented By Navdeep on 19th may for resolving 6397
            'If verisign_authorization_code.Trim() = "" Or verisign_transaction_code.Trim() = "" Then
            If verisign_authorization_code.Trim() = "" Then
                Throw New Exception("Invalid Card, No authorization code at Verisign")
            Else
                order.VerisgnAuthorizationCode = verisign_authorization_code
                'Commented By Navdeep on 19th may for resolving 6397
                'order.VerisignTransactionCode = verisign_transaction_code
                order.SAPBillToAccountNumber = ""
                credit_card_success = True
            End If

            Return credit_card_success

        Catch ex As Exception
            Console.WriteLine("Credit Card Failed: " + ex.Message)
            Throw ex
        End Try
    End Function

    'Public Function CompleteBackOrderRelease(ByRef order As Sony.US.ServicesPLUS.Core.Order, ByVal tax_exempt As Boolean)
    'Public Function CompleteBackOrderRelease_ConfirmInSIS(ByRef order As Sony.US.ServicesPLUS.Core.Order, ByVal tax_exempt As String)
    'Throw New NotImplementedException("Not implemented and will be of no use with SAP system")
    ''here we need to call system according to order number. If order starts with SB1 then B1 will be called
    ''and if it starts with SO1 then will be called 01. Discussed with KEN
    'Dim objSISParameter As SISParameter = New SISParameter()
    'If order.OrderNumber.Substring(1, 2) = "B1" Then
    '    objSISParameter.UserLocation = 1
    'Else
    '    objSISParameter.UserLocation = 2
    'End If
    'Dim sis As New 'SISHelper(objSISParameter)

    'If order.CreditCard Is Nothing Then
    '    Throw New Exception("CompleteBackOrderRelease method called on Order without a CreditCard")
    'Else
    '    'set the confirmed in SIS flag to false by default
    '    order.ConfirmedInSIS = False
    '    Try
    '        sis.ConfirmCreditCardOrder(order.OrderNumber, tax_exempt, order.CreditCard, order.VerisgnAuthorizationCode)
    '        ' the confirmed in SIS flag should be true only when there is no exception or error thrown by confirm cc order
    '        ' Deepa V. Sep 29, 2006
    '        order.ConfirmedInSIS = True
    '    Catch ex As Exception
    '        Console.WriteLine("voiding credit card transaction")
    '        'Try
    '        '    Dim pm As New PaymentManager()
    '        '    order.VerisignVoidTransactionCode = pm.VoidCreditCardTransaction(order.VerisignTransactionCode, order.OrderNumber, "", "")
    '        '    Throw ex
    '        'Catch exp As Exception

    '        'End Try
    '        Throw ex
    '    End Try
    'End If

    'End Function

    'Public Function CompleteBackOrderRelease_VoidTransaction(ByRef order As Sony.US.ServicesPLUS.Core.Order)
    'Commented By Navdeep kaur for 6397 bug
    'Dim pm As New PaymentManager()
    'Try
    '    order.VerisignVoidTransactionCode = pm.VoidCreditCardTransaction(order.VerisignTransactionCode, order.OrderNumber, "", "")
    'Catch exp As Exception
    '    Throw exp
    'Finally
    '    pm = Nothing
    'End Try
    'End Function

    'Public Function CancelOrder(ByRef order As Sony.US.ServicesPLUS.Core.Order, ByVal cart As ShoppingCart)
    'Public Function CancelOrder(ByRef order As Sony.US.ServicesPLUS.Core.Order, ByVal cart As ShoppingCart)

    '    '***************Changes done for Bug# 1212 Starts here***************
    '    '**********Kannapiran S****************
    '    Dim sis As New 'SISHelper(objSISParameter)
    '    '***************Changes done for Bug# 1212 Ends here***************

    '    Try
    '        Console.WriteLine("cancelling order on sis...")
    '        sis.CancelOrder(order.OrderNumber)
    '        order.Action = CoreObject.ActionType.DELETE
    '        'StoreOrder(order, True)

    '        'Dim scm As New ShoppingCartManager
    '        'scm.DeleteShoppingCart(cart)
    '    Catch ex As Exception
    '        Console.WriteLine("Error Canceling Order on SIS: " + ex.Message)
    '        'log
    '        Throw ex
    '    End Try
    'End Function

    'Private Function CloneOrder(ByVal so As Object, ByRef lo As Sony.US.ServicesPLUS.Core.Order, ByVal shopping_cart As ShoppingCart)
    '    so = CType(so, WebMethods.Services.Order)

    '    lo.CarrierCode = so.CarrierCode
    '    lo.CreditReason = so.CreditReason
    '    lo.CreditSwitch = so.CreditSwitch
    '    lo.CustomerClass = so.CustomerClass
    '    lo.GroupNumber = so.GroupNumber
    '    lo.InternalOrder = so.InternalOrder
    '    lo.LegacyAccountNumber = so.LegacyAccountNumber
    '    lo.POReference = so.POReference

    '    lo.ShipTo.Line1 = so.ShipTo.Address1
    '    lo.ShipTo.Line2 = so.ShipTo.Address2
    '    lo.ShipTo.Line3 = so.ShipTo.Address3
    '    lo.ShipTo.Attn = so.ShipTo.Attention
    '    lo.ShipTo.City = so.ShipTo.Address4
    '    lo.ShipTo.State = so.ShipTo.State
    '    lo.ShipTo.PostalCode = so.ShipTo.Zip
    '    lo.ShipTo.Name = so.ShipTo.Name

    '    lo.AllocatedGrandTotal = so.AllocatedGrandTotal
    '    lo.AllocatedTax = so.AllocatedTaxAmount
    '    ' modification as a part of enhancement of web service for SAP
    '    lo.AllocatedSpecialTax = so.AllocatedRecyclingFee  'so.AllocatedSpecialTaxAmount
    '    lo.AllocatedShipping = so.AllocatedShippingAmount
    '    lo.AllocatedPartSubTotal = so.AllocatedPartSubTotal

    '    'get rid of invoice information loaded from oracle
    '    lo.ClearInvoiceList()

    '    Dim invoice As Invoice

    '    Dim i As Object
    '    'If serviceEnvironment.ToLower() = "production" Then
    '    '    i = New WebMethods.Services.OrderInvoice
    '    'ElseIf serviceEnvironment.ToLower() = "staging" Then
    '    '    i = New WebMethods.Services.OrderInvoice
    '    'Else
    '    'End If
    '    i = New WebMethods.Services.OrderInvoice


    '    For Each i In so.Invoices
    '        invoice = New Invoice(lo)
    '        invoice.InvoiceNumber = i.Number
    '        invoice.InvoiceDate = i.Dated
    '        'Commented By Navdeep kaur on 18th May for resolving 6396
    '        'invoice.TrackingNumber = i.TrackingNumber
    '        invoice.Total = i.GrandTotal
    '        'invoice.PartsSubTotal = i.PartSubTotal
    '        'invoice.Shipping = i.ShippingAmount
    '        'invoice.Tax = i.TaxAmount
    '        'End of Comment By Navdeep kaur on 18th May for resolving 6396

    '        ' modification as a part of enhancement of web service for SAP
    '        invoice.SpecialTax = i.RecyclingFee 'i.SpecialTaxAmount
    '        lo.AddInvoice(invoice)
    '    Next

    '    'get local order line items to obtain type information
    '    Dim local_items() As OrderLineItem
    '    If shopping_cart Is Nothing Then
    '        Dim data_store As New OrderDataManager
    '        Try
    '            local_items = data_store.GetOrderDetails(lo.OrderNumber)
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End If

    '    Dim item As OrderLineItem
    '    Dim product As Product

    '    Dim o As Object
    '    'If serviceEnvironment.ToLower() = "production" Then
    '    '    o = New WebMethods.Services.OrderDetail
    '    'ElseIf serviceEnvironment.ToLower() = "staging" Then
    '    '    o = New WebMethods.Services.OrderDetail
    '    'Else
    '    'End If
    '    o = New WebMethods.Services.OrderDetail

    '    For Each o In so.Details
    '        item = New OrderLineItem(lo)

    '        'get type information from local copies
    '        If shopping_cart Is Nothing Then

    '            Dim found_it As Boolean = False
    '            For Each l_item As OrderLineItem In local_items
    '                If o.PartNumber = l_item.Product.PartNumber And o.LineNumber = 10 * l_item.SequenceNumber Then
    '                    found_it = True
    '                    Select Case l_item.Product.Type
    '                        Case Product.ProductType.Kit
    '                            product = New Sony.US.ServicesPLUS.Core.Kit
    '                        Case Product.ProductType.Part
    '                            product = New Sony.US.ServicesPLUS.Core.Part
    '                        Case Core.Product.ProductType.ComputerBasedTraining
    '                            If l_item.Product.SoftwareItem Is Nothing Then
    '                                product = New Sony.US.ServicesPLUS.Core.Part
    '                            Else
    '                                product = New Sony.US.ServicesPLUS.Core.Software
    '                                product.Type = Core.Product.ProductType.ComputerBasedTraining
    '                                item.Downloadable = l_item.Downloadable
    '                                item.SoftwareDownloadFileName = CType(l_item.Product, Software).DownloadFileName
    '                                item.SequenceNumber = l_item.SequenceNumber
    '                                item.Deliverymethod = l_item.Deliverymethod
    '                                product.TrainingActivationCode = l_item.Product.TrainingActivationCode
    '                                product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '                                product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '                                product.TrainingTitle = l_item.Product.TrainingTitle
    '                                product.TrainingURL = l_item.Product.TrainingURL

    '                            End If
    '                        Case Product.ProductType.Software, Core.Product.ProductType.Certificate, Core.Product.ProductType.OnlineTraining
    '                            product = New Sony.US.ServicesPLUS.Core.Software
    '                            Select Case l_item.Product.Type
    '                                Case Core.Product.ProductType.Certificate
    '                                    product.Type = Core.Product.ProductType.Certificate
    '                                Case Core.Product.ProductType.OnlineTraining
    '                                    product.Type = Core.Product.ProductType.OnlineTraining
    '                                Case Core.Product.ProductType.Software
    '                                    product.Type = Core.Product.ProductType.Software
    '                            End Select
    '                            item.Downloadable = l_item.Downloadable
    '                            item.SoftwareDownloadFileName = CType(l_item.Product, Software).DownloadFileName
    '                            item.SequenceNumber = l_item.SequenceNumber
    '                            item.Deliverymethod = l_item.Deliverymethod
    '                            product.TrainingActivationCode = l_item.Product.TrainingActivationCode
    '                            product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '                            product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '                            product.TrainingTitle = l_item.Product.TrainingTitle
    '                            product.TrainingURL = l_item.Product.TrainingURL
    '                        Case Product.ProductType.ExtendedWarranty
    '                            product = New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)

    '                            Dim objExtWarranty As Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel = New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    '                            If Not l_item.Product.SoftwareItem Is Nothing Then
    '                                objExtWarranty.EWPurchaseDate = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '                                objExtWarranty.EWSerialNumber = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '                                objExtWarranty.EWModelNumber = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '                            Else
    '                                objExtWarranty.EWPurchaseDate = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '                                objExtWarranty.EWSerialNumber = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '                                objExtWarranty.EWModelNumber = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '                            End If

    '                            product = objExtWarranty 'New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    '                            item.Downloadable = True

    '                            item.SoftwareDownloadFileName = String.Empty
    '                            item.SequenceNumber = l_item.SequenceNumber
    '                            item.Deliverymethod = l_item.Deliverymethod
    '                            product.SoftwareItem = CType(l_item.Product, Software) 'cart_item.Product.SoftwareItem

    '                    End Select
    '                End If
    '            Next



    '            If Not found_it Then
    '                product = New Sony.US.ServicesPLUS.Core.Part
    '            End If

    '        Else
    '            'get product type information from cart
    '            Dim cart_item As ShoppingCartItem = shopping_cart.FindItem(o.PartNumber)

    '            If Not cart_item Is Nothing Then
    '                Select Case cart_item.Product.Type
    '                    Case Product.ProductType.Kit
    '                        product = New Sony.US.ServicesPLUS.Core.Kit
    '                    Case Product.ProductType.Part
    '                        product = New Sony.US.ServicesPLUS.Core.Part
    '                    Case Product.ProductType.ExtendedWarranty
    '                        Dim objExtWarranty As Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel = New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    '                        If Not cart_item.Product.SoftwareItem Is Nothing Then
    '                            objExtWarranty.EWPurchaseDate = CType(cart_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '                            objExtWarranty.EWSerialNumber = CType(cart_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '                            objExtWarranty.EWModelNumber = CType(cart_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '                        Else
    '                            objExtWarranty.EWPurchaseDate = CType(cart_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '                            objExtWarranty.EWSerialNumber = CType(cart_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '                            objExtWarranty.EWModelNumber = CType(cart_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '                        End If

    '                        product = objExtWarranty 'New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    '                        item.Downloadable = True

    '                        item.SoftwareDownloadFileName = String.Empty
    '                        product.SoftwareItem = CType(cart_item.Product, Software) 'cart_item.Product.SoftwareItem
    '                    Case Product.ProductType.Software
    '                        product = New Sony.US.ServicesPLUS.Core.Software

    '                        If cart_item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Ship Then
    '                            item.Downloadable = False
    '                        Else
    '                            item.Downloadable = True
    '                        End If

    '                        item.SoftwareDownloadFileName = CType(cart_item.Product, Software).DownloadFileName
    '                        product.SoftwareItem = CType(cart_item.Product, Software) 'cart_item.Product.SoftwareItem
    '                        product.TrainingURL = CType(cart_item.Product, Software).TrainingURL
    '                        'product.TrainingURL = product.SoftwareItem.TrainingURL
    '                    Case Core.Product.ProductType.Certificate, Core.Product.ProductType.ClassRoomTraining, Core.Product.ProductType.ComputerBasedTraining, Core.Product.ProductType.OnlineTraining
    '                        product = New Sony.US.ServicesPLUS.Core.Training(cart_item.Product.Type)

    '                        If cart_item.DeliveryChannel = ShoppingCartItem.DeliveryMethod.Ship Then
    '                            item.Downloadable = False
    '                        Else
    '                            item.Downloadable = True
    '                        End If

    '                        item.SoftwareDownloadFileName = CType(cart_item.Product, Software).DownloadFileName
    '                        product.SoftwareItem = CType(cart_item.Product, Software) 'cart_item.Product.SoftwareItem
    '                        product.TrainingURL = CType(cart_item.Product, Software).TrainingURL
    '                        'product.TrainingURL = product.SoftwareItem.TrainingURL

    '                End Select
    '            Else
    '                product = New Sony.US.ServicesPLUS.Core.Part
    '            End If
    '        End If

    '        product.Description = o.PartDescription
    '        product.ListPrice = o.PartPrice
    '        product.PartNumber = o.PartNumber
    '        ' modification as a part of enhancement of web service for SAP
    '        product.SpecialTax = o.RecyclingFee 'o.SpecialTax

    '        'attempt to populate invoice details from master order details
    '        Dim inv As Invoice = lo.FindInvoice(o.InvoiceNumber)
    '        If Not inv Is Nothing Then
    '            Dim inv_line As New InvoiceLineItem(inv)
    '            inv_line.PartNumberShipped = o.PartNumber
    '            inv_line.PartDescription = o.PartDescription
    '            inv_line.ItemPrice = o.PartPrice
    '            inv_line.Quantity = o.PartQuantity
    '            inv.AddLineItem(inv_line)
    '        End If

    '        Select Case o.LineStatus
    '            Case "A"
    '            Case "R"
    '                item.LineStatus = OrderLineItem.Status.Allocated
    '            Case "B"
    '                item.LineStatus = OrderLineItem.Status.Backordered
    '            Case "F"
    '                item.LineStatus = OrderLineItem.Status.Confirmed
    '            Case "P"
    '                item.LineStatus = OrderLineItem.Status.Pick
    '            Case "I"
    '                item.LineStatus = OrderLineItem.Status.InProcess
    '            Case "U"
    '                item.LineStatus = OrderLineItem.Status.Unallocated
    '            Case "*"
    '                item.LineStatus = OrderLineItem.Status.CommentLine
    '            Case "E"
    '                item.LineStatus = OrderLineItem.Status.ErrorStatus
    '            Case "C"
    '                item.LineStatus = OrderLineItem.Status.Cancelled
    '            Case "D"
    '                item.LineStatus = OrderLineItem.Status.Deleted
    '            Case Else
    '                Throw New Exception("Unknown order line item status returned from SIS")
    '        End Select

    '        item.LineStatusDate = o.LineStatusDate
    '        item.Product = product
    '        item.Quantity = o.PartQuantity
    '        item.LineNumber = o.LineNumber
    '        lo.AddLineItem(item)
    '    Next

    'End Function

    'Public Function GetProduct(ByVal l_item As OrderLineItem) As Product
    'Dim product As Product
    'Select Case l_item.Product.Type
    '    Case product.ProductType.Kit
    '        product = New Sony.US.ServicesPLUS.Core.Kit
    '    Case product.ProductType.Part
    '        product = New Sony.US.ServicesPLUS.Core.Part
    '    Case Core.Product.ProductType.ComputerBasedTraining
    '        If l_item.Product.SoftwareItem Is Nothing Then
    '            product = New Sony.US.ServicesPLUS.Core.Part
    '        Else
    '            product = New Sony.US.ServicesPLUS.Core.Software
    '            product.Type = Core.Product.ProductType.ComputerBasedTraining
    '            product.TrainingActivationCode = l_item.Product.TrainingActivationCode
    '            product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '            product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '            product.TrainingTitle = l_item.Product.TrainingTitle
    '            product.TrainingURL = l_item.Product.TrainingURL

    '        End If
    '    Case product.ProductType.Software, Core.Product.ProductType.Certificate, Core.Product.ProductType.OnlineTraining
    '        product = New Sony.US.ServicesPLUS.Core.Software
    '        Select Case l_item.Product.Type
    '            Case Core.Product.ProductType.Certificate
    '                product.Type = Core.Product.ProductType.Certificate
    '            Case Core.Product.ProductType.OnlineTraining
    '                product.Type = Core.Product.ProductType.OnlineTraining
    '            Case Core.Product.ProductType.Software
    '                product.Type = Core.Product.ProductType.Software
    '        End Select

    '        product.TrainingActivationCode = l_item.Product.TrainingActivationCode
    '        product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '        product.TrainingTypeCode = l_item.Product.TrainingTypeCode
    '        product.TrainingTitle = l_item.Product.TrainingTitle
    '        product.TrainingURL = l_item.Product.TrainingURL
    '    Case product.ProductType.ExtendedWarranty
    '        product = New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)

    '        Dim objExtWarranty As Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel = New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    '        If Not l_item.Product.SoftwareItem Is Nothing Then
    '            objExtWarranty.EWPurchaseDate = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '            objExtWarranty.EWSerialNumber = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '            objExtWarranty.EWModelNumber = CType(l_item.Product.SoftwareItem, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '        Else
    '            objExtWarranty.EWPurchaseDate = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWPurchaseDate
    '            objExtWarranty.EWSerialNumber = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWSerialNumber
    '            objExtWarranty.EWModelNumber = CType(l_item.Product, Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel).EWModelNumber
    '        End If

    '        product = objExtWarranty 'New Sony.US.ServicesPLUS.Core.ExtendedWarrantyModel(Core.Product.ProductType.ExtendedWarranty)
    'End Select
    'Return New Sony.US.ServicesPLUS.Core.Part
    'End Function

    Public Function ReprocessInvoice(ByRef invoice As Sony.US.ServicesPLUS.Core.Invoice, ByVal newCCNumber As String, ByVal newCCExpireDate As String, Optional ByVal CreidCardValidation As Boolean = True)
        Dim txn_type As CreditCard.CreditCardAction = CreditCard.CreditCardAction.DelayedCapture
        Dim pm As New PaymentManager
        Dim om As New OrderManager
        Dim verisign_auth_code As String = ""
        Dim avs_address_result As String = ""
        Dim avs_zip_result As String = ""
        Dim strAccount As String = ""

        Try
            If Not invoice.Order.SAPBillToAccountNumber Is Nothing Then
                strAccount = invoice.Order.SAPBillToAccountNumber
            End If
            'Commented By Navdeep on 19th may for resolving 6397
            'If invoice.Order.Customer.UserType = "AccountHolder" And invoice.Order.Customer.IsActive = True Then
            '    invoice.Order.VerisignTransactionCode = pm.AuthorizeCreditCard(newCCNumber, newCCExpireDate, invoice.Total, invoice.Order.OrderNumber, "cc order", invoice.Order.BillTo.Line1, invoice.Order.BillTo.PostalCode, invoice.Order.CreditCard.CSCCode, invoice.Order.BillTo.Attn, "", avs_address_result, avs_zip_result, csc_result, verisign_auth_code, strAccount)
            '    'avs_address_result = "Y"
            '    'avs_zip_result = "Y"
            'Else
            '    invoice.Order.VerisignTransactionCode = pm.AuthorizeCreditCard(newCCNumber, newCCExpireDate, invoice.Total, invoice.Order.OrderNumber, "cc order", invoice.Order.BillTo.Line1, invoice.Order.BillTo.PostalCode, invoice.Order.CreditCard.CSCCode, invoice.Order.BillTo.Attn, "", avs_address_result, avs_zip_result, csc_result, verisign_auth_code, strAccount)
            'End If
            If CreidCardValidation Then ' Credit Card Exception processing does not require to validate the address and zip code
                If avs_address_result.ToUpper() = "N" Or avs_zip_result.ToUpper() = "N" Then
                    If verisign_auth_code.Trim() <> "" Then
                        'pm.VoidCreditCardTransaction(verisign_auth_code, invoice.Order.OrderNumber, "", "") ' ASleight - This function was commented out. No longer used.
                    End If
                    Throw New Exception("Invalid bill to address")
                End If
            End If

            'If csc_result.ToUpper() = "N" Then
            '    Throw New Exception("Invalid CVV code")
            'End If
            invoice.Order.VerisgnAuthorizationCode = verisign_auth_code
            invoice.Order.Dirty = True
            'om.StoreOrder(invoice.Order, True) 'save to DB for the consistency '' Store only one time and with the new creditcardsequencenumber. this is done in the ViewCreditCardExceptionPage of ServicesPLUS Admin

            'Commented By Navdeep kaur on 18th May for resolving 6396
            'invoice.VerisignCaptureTxnCode = pm.CaptureCreditCardPayment(invoice.Order.VerisignTransactionCode, invoice.Total, invoice.Order.OrderNumber, invoice.InvoiceNumber, verisign_auth_code)
            'invoice.VerisignCaptureAuthCode = verisign_auth_code
            'end of Comment By Navdeep kaur on 18th May for resolving 6396

            'om.StoreOrder(invoice.Order, True) 'save to DB for the consistency
            'SendShippingConfirmationEmail(inv)
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub StoreOrder(ByRef order As Order, ByVal update As Boolean)
        Dim data_store As New OrderDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.Store(order, txn_context, update)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Function GetUserLocation(ByVal ordernumber As String) As Short
        Dim data_store As New OrderDataManager
        Try
            Return data_store.GetUserLocation(ordernumber, Nothing)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub SendOrderConfirmationEmail(ByRef order As Order, ByVal MailBody As String, Optional ByVal Attachements As NameValueCollection = Nothing)
        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, order.Customer.EmailAddress) With {
            .Subject = $"ServicesPLUS(sm) Order Information ({order.OrderNumber})",
            .Body = MailBody,
            .IsBodyHtml = True,
            .Priority = MailPriority.Normal
        }

        For Each iOrderLine As OrderLineItem In order.LineItems
            If iOrderLine.Product?.SoftwareItem IsNot Nothing Then
                Select Case iOrderLine.Product.SoftwareItem.Type
                    Case Product.ProductType.ComputerBasedTraining,
                         Product.ProductType.Certificate,
                         Product.ProductType.ClassRoomTraining,
                         Product.ProductType.OnlineTraining
                        mailObj.Bcc.Add(ConfigurationData.Environment.Email.Training.NonClassRoomTraining_Admin)    ' Training@am.sony.com
                        Exit For
                    Case Else
                        Continue For
                End Select
            End If
        Next

        If Attachements?.Keys?.Count > 0 Then
            For iKeyCounter As Integer = 0 To Attachements.Keys.Count - 1
                Dim filename As String = "PDFs/EW/" + Attachements.Item(0)
                mailObj.Attachments.Add(New Attachment(ConfigurationData.GeneralSettings.EWTempPDFFolder + "\" + Attachements(iKeyCounter)))
            Next
        End If

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub SendCertificateConfirmationEmail(ByVal OrderNumber As String, ByVal CustomerEmailAddress As String, ByVal MailBody As String)
        Dim has_backordered_items As Boolean = False
        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, CustomerEmailAddress) With {
            .Subject = $"ServicesPLUS(sm) Training Certificate Activation Code Confirmation ({OrderNumber})",
            .Body = MailBody.ToString(),
            .IsBodyHtml = True,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Sub LogDownload(ByRef downloadRecord As DownloadRecord)
        Dim om As New OrderDataManager
        Dim data_store As New OrderDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.StoreDownloadRecord(downloadRecord, txn_context)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            'Throw ex'6668
        End Try
    End Sub

    Public Sub DeleteOldOrder(ByVal ordernumber As String, ByRef CCNumber As String, ByRef CCSeq As Integer)
        Dim om As OrderDataManager = New OrderDataManager
        Dim data_store As New OrderDataManager
        Dim txn_context As TransactionContext = Nothing

        Try
            txn_context = data_store.BeginTransaction
            data_store.DeleteOldOrder(ordernumber, txn_context, CCNumber, CCSeq)
            data_store.CommitTransaction(txn_context)
        Catch ex As Exception
            data_store.RollbackTransaction(txn_context)
            Throw ex
        End Try
    End Sub

    Public Sub SendFraudOrderEmail(ByVal order As Order, ByVal FraudReasonFlag As Short)
        Dim EmailBody As New StringBuilder(1500)
        Dim has_backordered_items As Boolean = False

        EmailBody.Append(vbCrLf + "Following user has tried placing a suspicious order with following details..." + vbCrLf)
        Select Case FraudReasonFlag
            Case 1
                EmailBody.Append("Fraud check failure reason: User password or previous password is matching with the test password in FraudCheck table. " + vbCrLf + vbCrLf)
            Case 2
                EmailBody.Append("Fraud check failure reason: User First Name is matching with the test First Name in FraudCheck table. " + vbCrLf + vbCrLf)
            Case 3
                EmailBody.Append("Fraud check failure reason: User Last Name is matching with the test Last Name in FraudCheck table. " + vbCrLf + vbCrLf)
            Case 4
                EmailBody.Append("Fraud check failure reason: User Bill to Attention is matching with the test Bill to Attention in FraudCheck table. " + vbCrLf + vbCrLf)
            Case 5
                EmailBody.Append("Fraud check failure reason: User Ship to Attention is matching with the test Ship to Attention in FraudCheck table. " + vbCrLf + vbCrLf)
        End Select
        EmailBody.Append("User Name: " + order.Customer.UserName + vbCrLf + vbCrLf)
        EmailBody.Append("Order Number: " + order.OrderNumber + vbCrLf + vbCrLf)

        'insert order details.
        For Each item As OrderLineItem In order.LineItems
            EmailBody.Append(item.Product.PartNumber + vbCrLf)
            EmailBody.Append(item.Product.Description + vbCrLf)
            EmailBody.Append("Quantity: " + item.Quantity.ToString() + vbTab + vbTab)
            EmailBody.AppendFormat("Item Price: {0:C}" + vbTab, item.Product.ListPrice)
            EmailBody.AppendFormat("Total Price: {0:C}" + vbCrLf, item.Quantity * item.Product.ListPrice)
        Next

        EmailBody.AppendFormat(vbCrLf + vbCrLf + "Shipping:" + vbTab + "{0:C}" + vbCrLf, order.AllocatedShipping)
        EmailBody.AppendFormat("Tax:" + vbTab + vbTab + "{0:C}" + vbCrLf, order.AllocatedTax)
        If order.AllocatedSpecialTax > 0.0 Then EmailBody.AppendFormat("State Recycling Fee: " + vbTab + vbTab + "{0:C}" + vbCrLf, order.AllocatedSpecialTax)
        EmailBody.AppendFormat("Total:" + vbTab + "{0:C}" + vbCrLf + vbCrLf, order.AllocatedGrandTotal)

        EmailBody.Append(vbCrLf + vbCrLf + "Sincerely," + vbCrLf)
        EmailBody.Append("The ServicesPLUS(sm) Team" + vbCrLf + vbCrLf)

        Dim mailObj = New MailMessage(ConfigurationData.Environment.Email.General.From, ConfigurationData.Environment.Email.General.FraudUserReporting) With {
            .Subject = $"Fraud user placing Order({order.OrderNumber}) in {ConfigurationData.GeneralSettings.Environment}",
            .Body = EmailBody.ToString(),
            .IsBodyHtml = False,
            .Priority = MailPriority.Normal
        }

        Using mailClient As New SmtpClient()
            mailClient.Host = ConfigurationData.Environment.SMTP.EmailServer
            mailClient.Send(mailObj)
        End Using
    End Sub

    Public Function FindConflictedOrder(ByVal OrderNumber As String) As String
        Dim data_store As New OrderDataManager
        Dim strOrderNumber As String = String.Empty

        Try
            strOrderNumber = data_store.GetConflictedOrder(OrderNumber)
        Catch ex As Exception
            Throw ex
        End Try

        Return strOrderNumber
    End Function

End Class
