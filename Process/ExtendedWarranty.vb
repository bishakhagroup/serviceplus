﻿
Imports Sony.US.ServicesPLUS.Data
Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.SIAMUtilities
Public Class ExtendedWarrantyManager

    Private Const ClassName As String = "ExtendedWarranty"
    'Private Logger As New SonyLogger
    Dim ewModels As DataSet
    Dim data_store As New ExtendedWarrantyDataManager


    'Extended Warranty initial search
    Public Function EWModelSearch(ByVal ew_model_number As String) As DataSet
        'Logger.RaiseTrace(ClassName, "Enter EWSearch", TraceLevel.Verbose)
        Try
            ewModels = data_store.GetEWModels(ew_model_number)
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch", TraceLevel.Verbose)
        Return ewModels
    End Function

    Public Function EWCertData(ByVal Cert_Guid As String) As DataSet
        'Logger.RaiseTrace(ClassName, "Enter EWSearch", TraceLevel.Verbose)
        Try
            ewModels = data_store.GetEWCertData(Cert_Guid)
        Catch ex As Exception
            Throw ex
        End Try
        'Logger.RaiseTrace(ClassName, "Exit EWSearch", TraceLevel.Verbose)
        Return ewModels
    End Function

    Public Function getewmodelsbyItemNoOrDesc(ByVal ew_ItemNumber As String, ByVal ew_Desc As String) As DataSet
        'Logger.RaiseTrace(ClassName, "Enter EW Item/Description Search", TraceLevel.Verbose)

        Try
            ewModels = data_store.GetewmodelsbyItemNoOrDesc(ew_ItemNumber, ew_Desc)
        Catch ex As Exception
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Exit EW Item/Description Search", TraceLevel.Verbose)

        Return ewModels
    End Function
    Public Function LoadEWProductDetails(ByVal ProductCode As String, ByVal EWProductCode As String) As DataSet

        'Logger.RaiseTrace(ClassName, "Load EW Product Details", TraceLevel.Verbose)

        'Dim models() As EWModel

        Try
            ewModels = data_store.LoadEWProductDetails(ProductCode, EWProductCode)
        Catch ex As Exception
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Load EW Product Details", TraceLevel.Verbose)

        Return ewModels
    End Function
    Public Function GETEWFORCART(ByVal ChildProductCode As String, ByVal ParentProductCode As String) As ExtendedWarrantyModel
        'Logger.RaiseTrace(ClassName, "Enter GETEWFORCART", TraceLevel.Verbose)

        Dim data_store As New ExtendedWarrantyDataManager
        Try
            Return data_store.GETEWFORCART(ChildProductCode, ParentProductCode)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "ExtendedWarranty: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

        'Logger.RaiseTrace(ClassName, "Exit GETEWFORCART", TraceLevel.Verbose)
    End Function

    Public Function CheckCertificateNumber(ByVal CertificateNumber As String) As Integer

        Dim data_store As New ExtendedWarrantyDataManager
        Try
            Return data_store.CheckCertificateNumber(CertificateNumber)
        Catch ex As Exception
            'Logger.RaiseTrace(ClassName, "ExtendedWarranty: " + ex.Message, TraceLevel.Error)
            Throw ex
        End Try

    End Function
End Class
