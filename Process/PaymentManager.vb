Imports Sony.US.ServicesPLUS.Core
Imports Sony.US.ServicesPLUS.Data

'This class cannot be used as a singleton
Public Class PaymentManager

    Private user As String
    Private vendor As String
    Private partner As String
    Private password As String
    Private host_address As String
    Private host_port As Integer
    Private timeout As Integer
    Private parm_list As String
    Private proxy_address As String
    Private proxy_port As Integer
    Private proxy_logon As String
    Private proxy_password As String
    Private user_auth As String
    Private pCtlx As Integer
    'Private payflow_pro As PayflowNETAPI
    Private result As String
    Private pnref As String
    Private respmsg As String
    Private m_auth_code As String
    Private transaction_state As String
    Private m_avs_address_result As String = String.Empty
    Private m_avs_zip_result As String = String.Empty
    Private m_csc_result As String = String.Empty
    Private cert_path As String

    Public Sub New()
        'Dim connection_string As String
        'Dim config_settings As Hashtable
        'Dim handler As New ConfigHandler("SOFTWARE\SERVICESPLUS")
        ''config_settings = handler.GetConfigSettings("//ServicesPLUSConfiguration").Item("Verisign")
        'config_settings = handler.GetConfigSettings("//item[@name='Verisign']")

        'user = config_settings.Item("user").ToString()
        'vendor = config_settings.Item("vendor").ToString()
        'partner = config_settings.Item("partner").ToString()
        'password = config_settings.Item("password").ToString()
        'host_address = config_settings.Item("host_address").ToString()
        'host_port = CType(config_settings.Item("host_port"), Integer)
        'timeout = CType(config_settings.Item("timeout"), Integer)
        'cert_path = config_settings.Item("cert_path").ToString()


        'user = ConfigurationData.Environment.Verisign.User
        'vendor = ConfigurationData.Environment.Verisign.Vendor
        'partner = ConfigurationData.Environment.Verisign.Partner
        'password = ConfigurationData.Environment.Verisign.Password
        'host_address = ConfigurationData.Environment.Verisign.Host_Address
        'host_port = ConfigurationData.Environment.Verisign.Host_Port
        'timeout = ConfigurationData.Environment.Verisign.Timeout
        'cert_path = ConfigurationData.Environment.Verisign.Cert_Path

        'proxy_address = ConfigurationData.Environment.Verisign.PROXY_ADDRESS
        'If IsNumeric(ConfigurationData.Environment.Verisign.PROXY_PORT) Then
        '    proxy_port = Convert.ToInt16(ConfigurationData.Environment.Verisign.PROXY_PORT)
        'Else
        '    proxy_port = 0
        'End If
        'proxy_logon = ConfigurationData.Environment.Verisign.PROXY_LOGON
        'proxy_password = ConfigurationData.Environment.Verisign.PROXY_PASSWORD

        'initialize verisign authentication
        user_auth = "USER=" + user + "&VENDOR=" + vendor + "&PARTNER=" + partner + "&PWD=" + password
    End Sub

#Region "    Obsolete Code"
    'Public Function AuthorizeCreditCard(ByVal credit_card_number As String, ByVal expiration_date As Date, ByVal amount As Double, ByVal comment_1 As String, ByVal comment_2 As String, ByVal avs_street As String, ByVal avs_zip As String, ByVal csc_code As String, ByVal fname As String, ByVal lname As String, ByRef avs_address_result As String, ByRef avs_zip_result As String, ByRef csc_result As String, ByRef auth_code As String, ByRef cust_code As String) As String
    '    '    Dim month As String
    '    '    'Dim day As String
    '    '    Dim year As String
    '    '    Console.WriteLine("In AuthorizeCreditCard")
    '    '    month = expiration_date.Month.ToString()
    '    '    'day = expiration_date.Day.ToString()
    '    '    year = expiration_date.Year.ToString()

    '    '    If month.Length = 1 Then
    '    '        month = "0" + month
    '    '    End If

    '    '    'If day.Length = 1 Then
    '    '    '    day = "0" + day
    '    '    'End If

    '    '    year = year.Substring(2)

    '    '    Dim nIndex As Integer = avs_zip.IndexOf("-")
    '    '    If nIndex >= 0 Then
    '    '        avs_zip = avs_zip.Remove(nIndex, 1)
    '    '    End If

    '    '    'parm_list = "&TRXTYPE=A&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + day + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&STREET=" + avs_street + "&ZIP=" + avs_zip + "&CVV2=" + csc_code

    '    '    ' the & in the name and the address fields are not recognised in Verisign, added the length to the param list so that
    '    '    ' the Verisign recognises that length of characters are string - as given in the payflow pro guide
    '    '    ' modified by Deepa V, May 22,2006
    '    '    'parm_list = "&TRXTYPE=A&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + year + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&STREET=" + avs_street + "&ZIP=" + avs_zip + "&CVV2=" + csc_code + "&CUSTCODE=" + cust_code

    '    '    Dim streetLength As String = avs_street.Length().ToString()
    '    '    Dim streetStr As String
    '    '    streetStr = "&STREET[" + streetLength + "]="

    '    '    Dim zipLength As String = avs_zip.Length.ToString()
    '    '    Dim zipStr As String
    '    '    zipStr = "&ZIP[" + zipLength + "]="

    '    '    parm_list = "&TRXTYPE=A&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE[" + (month.Length + year.Length).ToString() + "]=" + month + year + "&AMT=" + amount.ToString() + "&COMMENT1[" + comment_1.Length.ToString() + "]=" + comment_1 + "&COMMENT2[" + comment_2.Length.ToString() + "]=" + comment_2 + streetStr + avs_street + zipStr + avs_zip + "&CVV2=" + csc_code + "&CUSTCODE=" + cust_code + "&FIRSTNAME[" + fname.Length.ToString() + "]=" + fname

    '    '    ProcessTransaction()
    '    '    avs_address_result = m_avs_address_result
    '    '    avs_zip_result = m_avs_zip_result
    '    '    csc_result = m_csc_result
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function CaptureCreditCardPayment(ByVal authorization_transaction_id As String, ByVal comment_1 As String, ByVal comment_2 As String, ByRef auth_code As String) As String
    '    '    parm_list = "&TRXTYPE=D&TENDER=C&ORIGID=" + authorization_transaction_id + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&INVNUM=" + comment_2
    '    '    ProcessTransaction()
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function CaptureCreditCardPayment(ByVal authorization_transaction_id As String, ByVal amount As Double, ByVal comment_1 As String, ByVal comment_2 As String, ByRef auth_code As String) As String
    '    '    parm_list = "&TRXTYPE=D&TENDER=C&ORIGID=" + authorization_transaction_id + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&INVNUM=" + comment_2
    '    '    ProcessTransaction()
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function ChargeCreditCard(ByVal credit_card_number As String, ByVal expiration_date As Date, ByVal amount As Double, ByVal comment_1 As String, ByVal comment_2 As String, ByVal avs_street As String, ByVal avs_zip As String, ByVal csc_code As String, ByRef avs_address_result As String, ByRef avs_zip_result As String, ByRef csc_result As String, ByRef auth_code As String) As String
    '    '    Dim month As String
    '    '    'Dim day As String
    '    '    Dim year As String

    '    '    month = expiration_date.Month.ToString()
    '    '    'day = expiration_date.Day.ToString()
    '    '    year = expiration_date.Year.ToString()

    '    '    If month.Length = 1 Then
    '    '        month = "0" + month
    '    '    End If

    '    '    'If day.Length = 1 Then
    '    '    '    day = "0" + day
    '    '    'End If

    '    '    year = year.Substring(2)

    '    '    'parm_list = "&TRXTYPE=S&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + Day() + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&STREET=" + avs_street + "&ZIP=" + avs_zip


    '    '    ' the & in the name and the address fields are not recognised in Verisign, added the length to the param list so that
    '    '    ' the Verisign recognises that length of characters are string - as given in the payflow pro guide
    '    '    ' modified by Deepa V, May 22,2006
    '    '    'parm_list = "&TRXTYPE=S&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + year + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&STREET=" + avs_street + "&ZIP=" + avs_zip
    '    '    Dim streetLength As String = avs_street.Length().ToString()
    '    '    Dim streetStr As String
    '    '    streetStr = "&STREET[" + streetLength + "]="

    '    '    Dim zipLength As String = avs_zip.Length.ToString()
    '    '    Dim zipStr As String
    '    '    zipStr = "&ZIP[" + zipLength + "]="

    '    '    parm_list = "&TRXTYPE=S&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + year + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + streetStr + avs_street + zipStr + avs_zip

    '    '    ProcessTransaction()
    '    '    avs_address_result = m_avs_address_result
    '    '    avs_zip_result = m_avs_zip_result
    '    '    csc_result = m_csc_result
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function CreditCreditCard(ByVal credit_card_number As String, ByVal expiration_date As Date, ByVal amount As Double, ByVal comment_1 As String, ByVal comment_2 As String, ByRef auth_code As String) As String
    '    '    Dim month As String
    '    '    'Dim day As String
    '    '    Dim year As String

    '    '    month = expiration_date.Month.ToString()
    '    '    'day = expiration_date.Day.ToString()
    '    '    year = expiration_date.Year.ToString()

    '    '    If month.Length = 1 Then
    '    '        month = "0" + month
    '    '    End If

    '    '    'If day.Length = 1 Then
    '    '    '    day = "0" + day
    '    '    'End If

    '    '    year = year.Substring(2)

    '    '    'parm_list = "&TRXTYPE=C&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + Day() + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2
    '    '    parm_list = "&TRXTYPE=C&TENDER=C&ACCT=" + credit_card_number + "&EXPDATE=" + month + year + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2

    '    '    ProcessTransaction()
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function Credit(ByVal transaction_id As String, ByVal amount As Double, ByVal comment_1 As String, ByVal comment_2 As String, ByRef auth_code As String) As String
    '    '    parm_list = "&TRXTYPE=C&TENDER=C&ORIGID=" + transaction_id + "&AMT=" + amount.ToString() + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&INVNUM=" + comment_2
    '    '    ProcessTransaction()
    '    '    auth_code = m_auth_code
    '    '    Return pnref
    'End Function

    'Public Function VoidCreditCardTransaction(ByVal transaction_id As String, ByVal comment_1 As String, ByVal comment_2 As String, ByVal invoice_number As String) As String
    '    '    parm_list = "&TRXTYPE=V&TENDER=C&ORIGID=" + transaction_id + "&COMMENT1=" + comment_1 + "&COMMENT2=" + comment_2 + "&INVNUM=" + invoice_number
    '    '    ProcessTransaction()
    '    '    Return pnref
    'End Function

    'Public Function TransactionInquiry(ByVal transaction_id As String) As String
    '    '    parm_list = "&TRXTYPE=I&TENDER=C&ORIGID=" + transaction_id
    '    '    ProcessTransaction()
    '    '    Return transaction_state
    'End Function

    'Private Function ProcessTransaction()
    '    '    'parm_list = parm_list.Substring(2, parm_list.Length - 1) + "&" + user_auth

    '    '    parm_list = user_auth + parm_list

    '    '    Try
    '    '        'pCtlx = payflow_pro.SetParameters(host_address, host_port, timeout, proxy_address, proxy_port, proxy_logon, proxy_password)

    '    '        'create payflowpro object
    '    '        'payflow_pro = New PayflowNETAPI(host_address, host_port, timeout, proxy_address, proxy_port, proxy_logon, proxy_password, cert_path)
    '    '        payflow_pro = New PayflowNETAPI(host_address, host_port, timeout, proxy_address, proxy_port, proxy_logon, proxy_password)
    '    '        Dim response As String = payflow_pro.SubmitTransaction(parm_list, payflow_pro.GenerateRequestId())
    '    '        ParseResponse(response)

    '    '        'destroy object
    '    '        payflow_pro = Nothing
    '    '    Catch ex As Exception
    '    '        Throw ex
    '    '    End Try

    '    '    If result <> "0" Then
    '    '        If result = "12" Then
    '    '            Throw New Exception("We're sorry, this credit card has been declined") 'put customer approved message here.
    '    '        Else
    '    '            Throw New Exception(respmsg)
    '    '        End If
    '    '    End If
    'End Function

    'Private Function ParseResponse(ByVal response As String)
    '    '    Dim i As Integer
    '    '    Dim response_elements() As String
    '    '    response_elements = response.Split(Convert.ToChar("&"))

    '    '    If response_elements.Length > 0 Then

    '    '        For i = 0 To response_elements.Length - 1
    '    '            Dim field As String = response_elements.GetValue(i)
    '    '            Dim temp() As String = field.Split(Convert.ToChar("="))
    '    '            Dim variable As String = temp.GetValue(0)
    '    '            Dim value As String = temp.GetValue(1)

    '    '            Select Case variable
    '    '                Case "RESULT"
    '    '                    result = value
    '    '                Case "PNREF"
    '    '                    pnref = value
    '    '                Case "RESPMSG"
    '    '                    respmsg = value
    '    '                Case "AUTHCODE"
    '    '                    m_auth_code = value
    '    '                Case "TRANSSTATE"
    '    '                    transaction_state = value
    '    '                Case "AVSADDR"
    '    '                    m_avs_address_result = value
    '    '                Case "AVSZIP"
    '    '                    m_avs_zip_result = value
    '    '                Case "CVV2MATCH"
    '    '                    m_csc_result = value
    '    '                Case Else
    '    '            End Select
    '    '        Next
    '    '    End If
    'End Function
#End Region

    Public Function GetCreditCardTypes() As CreditCardType()
        Dim data_store As New PaymentDataManager
        Dim obj() As CreditCardType

        Try
            obj = data_store.GetCreditCardTypes()
        Catch ex As Exception
            Throw ex
        End Try

        Return obj
    End Function

    Public Function IsTaxExempt(ByVal customer As String, ByVal ship_to As String) As Boolean
        Dim rc As Boolean = False
        Dim data_store As New PaymentDataManager

        Try
            rc = data_store.IsTaxExempt(customer, ship_to)
        Catch ex As Exception
            Throw ex
        End Try

        Return rc
    End Function
End Class
