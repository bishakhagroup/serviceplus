﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sony.US.SIAMUtilities
{
    public struct EncryptionKey
    {
        public string Key1;
        public string Key1UserID;
        public string Key2;
        public string Key2UserID;
        public string LockKey;
        public string LockKeyUserID;
        public string NewLockKey;
        public string Key2EKey1;
        public string LockKeyEKey2EKey1;
    }
    public static class RevocationProcessInfo
    {
        public static EncryptionKey RevocationEncryptionKey;
        private static string ErrorMessage = String.Empty;
        private static string MyMessage = String.Empty;
        private static Int32 ProgressMax = 0;
        private static Int32 ProgressMin = 0;
        private static Int16 CurrentActivityToken = 0;
        private static bool m_RevocationInProgress = false;
        private static bool m_StopRequested = false;

        public static Int16 CurrentActivity
        {
            get { return CurrentActivityToken; }
            set { CurrentActivityToken = value; }
        }
        public static bool StopRequested
        {
            get { return m_StopRequested; }
            set { m_StopRequested = value; }
        }

        public static bool RevocationInProgress
        {
            get { return m_RevocationInProgress; }
            set { m_RevocationInProgress = value; }
        }

        public static  string Message
        {
            get { return MyMessage; }
            set { MyMessage =   value; }
        }
        public static string ProcessError
        {
            get { return ErrorMessage; }
            set { ErrorMessage = value; }
        }
        public static  Int32 ProgressMaxValue
        {
            get { return ProgressMax; }
            set { ProgressMax = value; }
        }
        public static Int32 ProgressMinalue
        {
            get { return ProgressMin; }
            set { ProgressMin = value; }
        }
        public static string UserName 
        {
            set;
            get;
        }
        public static string SIAM_ID 
        {
            set;
            get;
        }
        public static string EventOriginServerMachineName
        {
            set;
            get;
        }
        public static string OperationalUser
        {
            set; 
            get;
        }
        public static object HTTPRequestObjectValues
        { 
            set;
            get;
        }
    }

}
