using System;
using Sony.US.SIAMUtilities;
using System.Messaging;
using System.Collections;
namespace Sony.US.SIAMUtilities
{
	public class SonyLogMessage
	{
		protected string m_label;
		protected string m_sentTime;
		protected string m_priority;
		protected string m_body;

		public SonyLogMessage(string label,string sentTime,string priority,string body)
		{
			m_label = label;
			m_sentTime = sentTime;
			m_priority = priority;
			m_body = body;
		}

		public string Label
		{
			get { return m_label; }
		}

		public string SentTime
		{
			get { return m_sentTime; }
		}

		public string Priority
		{
			get { return m_priority; }
		}

		public string Body
		{
			get { return m_body; }
		}
	};

	public class SonyLogger1
	{
		System.Messaging.MessageQueue theQueue;
		int	   lifeDays;

		public SonyLogger1()
		{
			// Get the Queue name from Configuration
			string queueName = "";
			object lifeDaysObj = null;

			try
			{
				queueName = CachingServices.Cache["LoggingQueue"] as string;
				lifeDaysObj = CachingServices.Cache["LoggingQueueMsgLifeDays"];

                if (queueName == null || queueName == "")
                {
                    queueName = ConfigurationData.GeneralSettings.MSMQConfiguration.SonyLoggingQueue;
                    CachingServices.Cache["LoggingQueue"] = queueName;
                }

                if (lifeDaysObj == null)
                {
                    lifeDaysObj = ConfigurationData.GeneralSettings.MSMQConfiguration.SonyLoggingQueueMsgLifeDays;
                    if (lifeDaysObj != null)
                        lifeDays = Convert.ToInt16(lifeDaysObj.ToString());
                    else
                        lifeDays = 60; //default to 60 days
                    CachingServices.Cache["LoggingQueueMsgLifeDays"] = lifeDays;
                }
                else
                    lifeDays = Convert.ToInt16(lifeDaysObj.ToString());
            }
			catch
			{
				queueName = "";
			}

			

			try
			{
				if (! IsQueuePresent(queueName))
				{
					theQueue = CreateQueue(queueName);
				}
				else
				{
					theQueue = new MessageQueue(queueName);
				}
				theQueue.Formatter = new System.Messaging.ActiveXMessageFormatter();
			}
			catch
			{
				theQueue = null;
			}
		}

		public void RaiseTrace(string ClassName,string MessageText,System.Diagnostics.TraceLevel MessageType)
		{
			System.Web.Caching.Cache c = CachingServices.Cache;
			System.Diagnostics.TraceSwitch tswitch = c["TraceSwitch"] as System.Diagnostics.TraceSwitch;
			if (tswitch == null)
			{
				tswitch = TraceSwitchConfigurator.GetSIAMTraceSwitch();
			}
			if (tswitch.Level == System.Diagnostics.TraceLevel.Off)
			{
				return;
			}
			else
			{
				System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
				System.Diagnostics.StackFrame sf = stackTrace.GetFrame(1);

				string Message = ClassName+"."+sf.GetMethod().Name + "---" + MessageText;
				string Label = "";
				switch (MessageType)
				{
					case System.Diagnostics.TraceLevel.Error:
						Label = "Error";
						System.Diagnostics.Trace.WriteLineIf(tswitch.TraceError,Message,Label);
						break;
					case System.Diagnostics.TraceLevel.Info: 
						Label = "Info";
						System.Diagnostics.Trace.WriteLineIf(tswitch.TraceInfo,Message,Label);
						break;
					case System.Diagnostics.TraceLevel.Verbose: 
						Label = "Verbose";
						System.Diagnostics.Trace.WriteLineIf(tswitch.TraceVerbose,Message,Label);
						break;
					case System.Diagnostics.TraceLevel.Warning: 
						Label = "Warning";
						System.Diagnostics.Trace.WriteLineIf(tswitch.TraceWarning,Message,Label);
						break;
				}
				MessagePump(Message,Label);
			}
		}

		public ArrayList ReadTrace(string strLabel, string strFilter, string strBeginTime, string strEndTime)
		{
			theQueue.MessageReadPropertyFilter.SetAll();
			MessageEnumerator myEnumerator = theQueue.GetMessageEnumerator();
			ArrayList messageList = new ArrayList();
			Message message;
			SonyLogMessage logMessage;
			TimeSpan msgLifeDaySpan = new TimeSpan(lifeDays,0,0,0);
			DateTime dayNow = DateTime.Now;

			bool bLabelMatch, bFilterMatch, bBeginTimeMatch, bEndTimeMatch;

			// Move to the next message.
			while(myEnumerator.MoveNext())
			{
				message = myEnumerator.Current;

				if ((dayNow - message.SentTime) > msgLifeDaySpan) //delete old log message
				{
					myEnumerator.RemoveCurrent();
					continue;
				}

				bLabelMatch = bFilterMatch = bBeginTimeMatch = bEndTimeMatch = false;
				if (strLabel != "All") 
				{
					if (message.Label == strLabel)
						bLabelMatch = true;
				}
				else
					bLabelMatch = true;

				if (strFilter != "")
				{
					if (message.Body.ToString().IndexOf(strFilter) >= 0)
						bFilterMatch = true;
				}
				else
					bFilterMatch = true;

				if (strBeginTime != "")
				{
					DateTime beginTime = DateTime.Parse(strBeginTime);
					if (message.SentTime >= beginTime)
						bBeginTimeMatch = true;
				}
				else
					bBeginTimeMatch = true;

				if (strEndTime != "")
				{
					DateTime endTime = DateTime.Parse(strEndTime);
					if (message.SentTime <= endTime)
						bEndTimeMatch = true;
				}
				else
					bEndTimeMatch = true;
				if (bLabelMatch && bFilterMatch && bBeginTimeMatch && bEndTimeMatch)
				{
					logMessage = new SonyLogMessage(message.Label,message.SentTime.ToString("G"),
						message.Priority.ToString(),message.Body.ToString());
					messageList.Add(logMessage);
				}
			}

			return messageList;
		}

		private void MessagePump(string Message,string Label)
		{
			try
			{
				if (theQueue != null)
				{
					theQueue.Send(Message,Label);
				}
			}
			catch
			{
				return;
			}
		}

		private bool IsQueuePresent(string QueueName)
		{
			try
			{
				return System.Messaging.MessageQueue.Exists(QueueName);
			}
			catch
			{
				return false;
			}
		}

		private System.Messaging.MessageQueue CreateQueue(string QueueName)
		{
			try
			{
				return System.Messaging.MessageQueue.Create(QueueName,false);
			}
			catch (Exception  ex)
			{
				return null;
			}
		}
	}
}
