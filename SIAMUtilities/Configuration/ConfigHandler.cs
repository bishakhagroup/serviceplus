using System;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Reflection;
using System.Text;
using System.Web.Caching;
using Microsoft.Win32;
using System.Data.SqlClient;
using Sony.US.SIAMUtilities;



namespace Sony.US.SIAMUtilities {
    /// <summary>
    /// The <b>ConfigHandler</b> class is used to load configuration information
    /// from a single XML file. Configuration information is returned as a <see cref="System.Collections.Hashtable"/>.
    /// </summary>
    /// <remarks>The location of the configuration file is stored as a string value of the registry
    /// key <b>HKEY_LOCAL_MACHINE\SOFTWARE\ServicesPLUS</b> called ConfigFileLocation.</remarks>
    public class ConfigHandler {
        #region Class Variables
        // CONSTANTS
        private const string ClassName = "ConfigHandler";
        private const string CurrentEnvironmentKey = @"CurrentEnvironment";
        private const string EnvironmentConnectionString = @"ConnectionString";
        private const string RegKeyConfigLocation = @"ConfigFileLocation";

        // PRIVATE FIELDS
        private string RegistryRootKey = @"SOFTWARE\ServicesPLUS";
        private string EnvironmentName = string.Empty;
        private string ConnectionString = string.Empty;
        private XmlDocument ConfigDoc;

        private const string ItemsElement = "items";
        private const string ItemElement = "item";
        private const string ItemNameAttribute = "name";
        private const string ItemValueAttribute = "value";
        private const string ItemTypeAttribute = "type";
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <remarks>This is the default constructor for the ConfigHandler class and takes no arguments.</remarks>
        public ConfigHandler(string registryKey) {
            if (string.IsNullOrWhiteSpace(registryKey)) throw new ArgumentNullException("ConfigHandler requires a valid registry key path.");

            RegistryRootKey = registryKey;
            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                    // key now points to the 64-bit key
                    EnvironmentName = subKey.GetValue(CurrentEnvironmentKey).ToString();
                    ConnectionString = subKey.GetValue(EnvironmentConnectionString).ToString();
                }
            }
        }
        
        /// <summary>
        /// Alternate constructor
        /// </summary>
        public ConfigHandler() {
            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                    // key now points to the 64-bit key
                    EnvironmentName = subKey.GetValue(CurrentEnvironmentKey).ToString();
                    ConnectionString = subKey.GetValue(EnvironmentConnectionString).ToString();
                }
            }
        }


        #endregion

        public string CurrentEnvironmentName {
            get {
                return EnvironmentName;
            }
        }

        public string DataBaseConnecitonString {
            get {
                return ConnectionString;
            }
        }

        #region Public Methods

        /// <summary>
        /// This methods reads a node from the configuration XML file using 
        /// an XPath expression to locate the data.
        /// </summary>
        /// <param name="xmlPath">The XPath expression.</param>
        /// <returns>A Hashtable of configuration data.</returns>
        /// <example>The following is an example of how you might use the ConfigHandler class:
        /// <code lang="C#">
        /// CareVu.CoreServices.ConfigHandler config = new CareVu.CoreServices.ConfigHandler();
        /// System.Collections.Hashtable configSettings = config.GetConfigSettings("//configuration/DatabaseTraceListener");
        ///
        /// System.String connectionString = configSettings["ConnectionString"].ToString();
        /// System.String traceSpName = configSettings["TraceSPName"].ToString();
        /// System.Int32 maximumRequests = Convert.ToInt32(configSettings["MaximumRequests"]);
        /// </code>
        /// </example>
        ///	<para lang="VB">No example is available for Visual Basic.</para>
        /// <remarks>The data that is extracted from the config file totally depends on the xpath statement that is passed.</remarks>
        public Hashtable GetConfigSettings(string xmlPath) {

            // Lets grab an instance of the cache object
            System.Web.Caching.Cache theCache = CachingServices.Cache;

            // check to see if we have loaded the configuration document in a prior
            // call to this function. ConfigDoc will come back null if not.
            ConfigDoc = (System.Xml.XmlDocument)theCache[RegistryRootKey];

            // If it's null lets load it up from disk using the value in the registry
            // and then cache it so we don't have to do this again.
            if (ConfigDoc == null) {
                //Debug.WriteLine("Configuration: Getting new config document");
                ConfigDoc = new System.Xml.XmlDocument();
                string m_filename = string.Empty;
                //RegistryKey rk = Registry.LocalMachine.OpenSubKey(BootstrapKey);
                //if (rk == null)
                //{
                //    m_filename = @"C:\Program Files\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                //}
                //else
                //{
                //    m_filename = rk.GetValue(BootstrapFileKey) as string;
                //}
                // ASleight - Below changes are for .NET 4.7
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                    using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                        if (subKey == null) {
                            m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                        } else {
                            m_filename = subKey.GetValue(RegKeyConfigLocation) as string;
                        }
                    }
                }

                ConfigDoc.Load(m_filename);
                theCache[RegistryRootKey] = ConfigDoc;
            }

            // using the xpath string lets locate the element we are looking for
            try {
                // We have the doc loaded in ConfigDoc. We need to determine the environemt first and then get
                // that XMLSection and operate on it instead of the ConfigDoc.
                //RegistryKey rk = Registry.LocalMachine.OpenSubKey(BootstrapKey);
                //string EnvironmentName = rk.GetValue(CurrentEnvironmentKey) as string;

                string EnvironmentXPath = @"//Environment[@name='" + EnvironmentName + "']";

                System.Xml.XmlNode environmentSection = ConfigDoc.SelectSingleNode(@"//Environment[@name='GeneralSettings']");
                Hashtable hstTable = new Hashtable {
                    { "EnvironmentName", EnvironmentName },
                    { "DataBaseConnection", ConnectionString }
                };
                if (environmentSection != null) {
                    hstTable["GeneralSettings"] = CreateProfileFromNode(environmentSection, null);
                }

                environmentSection = ConfigDoc.SelectSingleNode(EnvironmentXPath);

                if (string.IsNullOrWhiteSpace(xmlPath)) {
                    if (hstTable["GeneralSettings"] != null) {
                        hstTable["Environment"] = CreateProfileFromNode(environmentSection, null);
                        return hstTable;
                    } else {
                        return CreateProfileFromNode(environmentSection, null);
                    }
                } else {
                    System.Xml.XmlDocument x = new System.Xml.XmlDocument();
                    x.LoadXml($"<SearchSection>{environmentSection.InnerXml}</SearchSection>");

                    System.Xml.XmlNode n = x.SelectSingleNode(xmlPath);
                    if (n == null) return null;

                    return CreateProfileFromNode(n, null);
                }
            } catch (Exception) {
                return null;
            }
        }

        public string GetCurrentEnvironment() {
            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                    if (subKey != null) { 
                        return subKey.GetValue(CurrentEnvironmentKey) as string;
                    }
                }
            }
            return "Development";
        }

        #endregion

        #region Private Methods


        /// <summary>
        /// This methods takes the node located by the GetConfigSettings method
        /// and actually builds the hashtable
        /// </summary>
        /// <param name="section">The XML node from which the hashtable will be built</param>
        /// <param name="Parent">The parent hashtable to which the new hashtable will belong</param>
        /// <returns>A hashtable</returns>
        private Hashtable CreateProfileFromNode(XmlNode section, Hashtable Parent) {
            // Create the new hashtable if necessary.
            if (Parent == null) {
                Parent = new Hashtable();
            }

            // Select a node list that matches our ItemElement value
            XmlNodeList nodes = section.SelectNodes(ItemElement);

            // Iterate through the list and either set the value or call this function
            // recursively to build a child hashtable.

            foreach (XmlNode node in nodes) {
                try {
                    XmlElement elem = (XmlElement)node;
                    if (node.HasChildNodes == true) {
                        var p = new Hashtable();
                        Parent.Add(node.Attributes[ItemNameAttribute].Value, CreateProfileFromNode(node, p));
                    } else {
                        Parent.Add(node.Attributes[ItemNameAttribute].Value, node.Attributes[ItemValueAttribute].Value);
                    }
                } catch (Exception ex) {
                    throw ex;
                }
            }
            return Parent;
        }

        #endregion
    }
}
