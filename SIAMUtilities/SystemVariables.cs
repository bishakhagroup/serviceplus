using System;
using System.Collections.Generic;
using System.Text;

namespace Sony.US.SIAMUtilities
{
    public class SystemVariables
    {
        private static string strRunningApplicationPath = string.Empty;
        public static string RunningApplicationPath
        {
            get
            {
                return strRunningApplicationPath;
            }
            set
            {
                strRunningApplicationPath = value;
            }
        }
    }
}
