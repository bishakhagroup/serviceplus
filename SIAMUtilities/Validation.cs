﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions; 

namespace Sony.US.SIAMUtilities
{
    public class Validation
    {
        public static bool IsEmail(string Email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(Email))
                return (true);
            else
                return (false);
        }

        public static bool IsUrl(string Url)
        {
            string strRegex = "^(https?://)"
            + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" //user@ 
            + @"(([0-9]{1,3}\.){3}[0-9]{1,3}" // IP- 199.194.52.184 
            + "|" // allows either IP or domain 
            + @"([0-9a-z_!~*'()-]+\.)*" // tertiary domain(s)- www. 
            + @"([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." // second level domain 
            + "[a-z]{2,6})" // first level domain- .com or .museum 
            + "(:[0-9]{1,4})?" // port number- :80 
            + "((/?)|" // a slash isn't required if there is no file name 
            + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            Regex re = new Regex(strRegex);

            if (re.IsMatch(Url))
                return (true);
            else
                return (false);
        } 

        

        private static bool IsNumeric(string strTextEntry)
        {           
            Regex objNotWholePattern = new Regex("[^0-9]");
            return !objNotWholePattern.IsMatch(strTextEntry);            
        }

        /// <summary>
        /// Validates a credit card number using the standard Luhn/mod10
        /// validation algorithm.
        /// </summary>
        /// <param name="cardNumber">Card number, with or without
        ///        punctuation</param>
        /// <returns>True if card number appears valid, false if not
        /// </returns>

        // public static bool IsCreditCardValid(string cardNumber,string CardType, ref string ErrorMessage)
        // {
            // ErrorMessage = "*";
            // cardNumber = cardNumber.Trim().Replace("-", "").Replace(".", "").Replace(" ","");
             
            // if (IsNumeric(cardNumber))
            // {
                // switch (CardType)
                // {
                    // case "A":
                        // if (cardNumber.Length != 15) 
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // else if (cardNumber.StartsWith("34") == false && cardNumber.StartsWith("37") == false)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // break;
                    // case "V":
                        // if (!(cardNumber.Length == 13 || cardNumber.Length == 16))
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // else if (cardNumber.StartsWith("4") == false)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // break;
                    // case "M":
                        // if (cardNumber.Length != 16)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // else if (cardNumber.StartsWith("51") == false && cardNumber.StartsWith("52") == false && cardNumber.StartsWith("53") == false && cardNumber.StartsWith("54") == false && cardNumber.StartsWith("55") == false)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // break;
                    // case "D":
                        // if (cardNumber.Length != 16)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // else if (cardNumber.StartsWith("6011") == false)
                        // {
                            // ErrorMessage = "Please correct the invalid card number.";
                            // return false;
                        // }
                        // break;
                // }
            // }
            // else
            // {
                // ErrorMessage = "Please correct the invalid card number.";
                // return false;
            // }

            // /*
                // a) Check if credit card number is valid using the LUHN10 algorithm.
                // b) Check if the first digit(s) are correct for the card type selected (4 for
                // Visa, 51-55 for Mastercard, 6011 for Discover, 34 or 37 for American Express)
                // c) Check if length is correct for the card type selected (13 or 16 for Visa,
                // 16 for Discover and Mastercard, 15 for American Express). 
             
             // */
            // if (ConfigurationData.GeneralSettings.LUHN10Check == "Y")
            // {
                // bool blnResult = false;
                // const string allowed = "0123456789";
                // int i;

                // StringBuilder cleanNumber = new StringBuilder();
                // for (i = 0; i < cardNumber.Length; i++)
                // {
                    // if (allowed.IndexOf(cardNumber.Substring(i, 1)) >= 0)
                        // cleanNumber.Append(cardNumber.Substring(i, 1));
                // }
                // if (cleanNumber.Length < 13 || cleanNumber.Length > 16)
                // {
                    // ErrorMessage = "Please correct the invalid card number.";
                    // return false;
                // }
                // for (i = cleanNumber.Length + 1; i <= 16; i++)
                    // cleanNumber.Insert(0, "0");

                // int multiplier, digit, sum, total = 0;
                // string number = cleanNumber.ToString();

                // for (i = 1; i <= 16; i++)
                // {
                    // multiplier = 1 + (i % 2);
                    // digit = int.Parse(number.Substring(i - 1, 1));
                    // sum = digit * multiplier;
                    // if (sum > 9)
                        // sum -= 9;
                    // total += sum;
                // }
                // blnResult = (total % 10 == 0);
                // if (!blnResult) ErrorMessage = "Please correct the invalid card number.";
                // return blnResult;
            // }
            // else
            // {
                // return true;
            // }
           
        // }

    }
}
