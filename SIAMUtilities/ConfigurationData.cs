using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Reflection;
using Microsoft.Win32;
using System.Xml.Linq;
using System.Linq;
namespace Sony.US.SIAMUtilities {
    public static class ConfigurationData {
        //public static _userType[] mUserTypes = null;
        static Hashtable hstConfigData = null;
        static string CreditCardDecryptionLockKey = string.Empty;
        static bool m_PCILogWriterRunning = false;
        static string m_EWTempPDFFolder = string.Empty;
        //static string RA_EmailID = "proparts@am.sony.com";
        //static string RA = "proparts@am.sony.com";

        public static void Initialize(string LockKey) {
            hstConfigData = new ConfigHandler(@"SOFTWARE\ServicesPLUS").GetConfigSettings(null);
            CreditCardDecryptionLockKey = LockKey;
            //int i = Environment.UserTypes.Length;
        }

        public static void Initialize() {
            try {
                //Changes to fixHomepage redirection
                hstConfigData = new ConfigHandler(@"SOFTWARE\ServicesPLUS").GetConfigSettings(null);
                // CreditCardDecryptionLockKey = Encryption.DeCrypt(Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ServicesPLUS").GetValue("LockKey").ToString(), Encryption.PWDKey);
                string strMessage = string.Empty;
                if (Encryption.CheckLockKey(CreditCardDecryptionLockKey, out strMessage) == false) {
                    CreditCardDecryptionLockKey = string.Empty;
                }
            } catch (Exception ex) {
                Console.WriteLine("Config  Message: " + ex.Message);
                CreditCardDecryptionLockKey = string.Empty;
            }

            //int i = Environment.UserTypes.Length;
        }

        public static class WebmethodTest {
            public static string UserName {set; get; }
            public static string Password { set; get; }
            public static string CancelOrder { set; get; }
            public static string CustomerLookUp { set; get; }
            public static string PartInquiry { set; get; }
            public static string POInquiry { set; get; }
            public static string SalesOrder { set; get; }
            public static string CertificatePath { set; get; }
            public static string PartLocation { set; get; }
            public static string DefaultAccount { set; get; }

            public static void Load(string sEnviroment) {
                string RegistryRootKey = @"SOFTWARE\ServicesPLUS";
                string RegKeyConfigLocation = @"ConfigFileLocation";
                string m_filename = string.Empty;
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                    using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                        if (subKey == null) {
                            m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                        } else {
                            m_filename = subKey.GetValue(RegKeyConfigLocation) as string;
                        }
                    }
                }

                XDocument objDoc = XDocument.Load(m_filename);
                var query = from p in objDoc.Descendants("Environment")
                            where (string)p.Attribute("name") == sEnviroment
                            select p;

                var query1 = from p1 in query.Descendants("item")
                             where (string)p1.Attribute("name") == "Webmethods"
                             select p1;
                foreach (XElement objElement in query1.Elements()) {
                    switch (objElement.Attribute("name").Value) {
                        case "UserName":
                            UserName = objElement.Attribute("value").Value;
                            break;
                        case "password":
                            Password = objElement.Attribute("value").Value;
                            break;
                        case "CancelOrder":
                            CancelOrder = objElement.Attribute("value").Value;
                            break;
                        case "CustomerLookUp":
                            CustomerLookUp = objElement.Attribute("value").Value;
                            break;
                        case "PartInquiry":
                            PartInquiry = objElement.Attribute("value").Value;
                            break;
                        case "POInquiry":
                            POInquiry = objElement.Attribute("value").Value;
                            break;
                        case "SalesOrder":
                            SalesOrder = objElement.Attribute("value").Value;
                            break;
                        case "CertificatePath":
                            CertificatePath = objElement.Attribute("value").Value;
                            break;
                        case "PartLocation":
                            PartLocation = objElement.Attribute("value").Value;
                            break;
                        case "DefaultAccount":
                            DefaultAccount = objElement.Attribute("value").Value;
                            break;
                    }
                }
            }
        }

        public static class GeneralSettings {
            static Hashtable hstGeneral = (Hashtable)hstConfigData["GeneralSettings"];
            static Hashtable hstPCILogger = (Hashtable)hstGeneral["PCILogger"];
            public static string Environment => hstConfigData["EnvironmentName"].ToString();
            public static string LockKey => CreditCardDecryptionLockKey;
            //public static string SISWEBTESTServer { get { return hstGeneral["SISWEBTESTServer"].ToString(); } }
            //public static string SISWEBTESTSystem { get { return hstGeneral["SISWEBTESTSystem"].ToString(); } }
            public static string NoCachPage => hstGeneral["NoCachPage"].ToString();
            public static string LUHN10Check => hstGeneral["LUHN10Check"].ToString();
            public static string ShippingMessage => hstGeneral["ShippingMessage"].ToString();
            public static string ShippingMessageColor => hstGeneral["ShippingMessageColor"].ToString();
            public static string ClassRoomTrainingTimings => hstGeneral["ClassRoomTrainingTimings"].ToString();
            public static bool PCILogWriterRunning { get { return m_PCILogWriterRunning; } set { m_PCILogWriterRunning = value; } }
            //Added by Sunil on 07-01-10 for bug 2145 (Logging Exception handling)
            public static DateTime PCILoggerLastRunTime;

            public static string EWTempPDFFolder {
                get { return m_EWTempPDFFolder; }
                set {
                    if (value != null) m_EWTempPDFFolder = value;
                }
            }
            public static string UnderMaintenance {
                get { return hstGeneral["UnderMaintenance"].ToString(); }
                set {
                    hstGeneral.Remove("UnderMaintenance");
                    hstGeneral.Add("UnderMaintenance", value);
                }
            }
            public static string IAMTempFolder => hstGeneral["IAMTempFolder"].ToString();
            public static string LoggingStatus {
                get {
                    Hashtable hstPCILogger = (Hashtable)hstGeneral["PCILogger"];
                    return hstPCILogger["LoggingStatus"].ToString();
                }
                set {
                    hstPCILogger.Remove("LoggingStatus");
                    hstPCILogger.Add("LoggingStatus", value);
                }
            }
            public static class Email {
                static Hashtable hstEmail = (Hashtable)hstGeneral["Email"];
                public static class Admin {
                    static Hashtable hstAdmin = (Hashtable)hstEmail["Admin"];
                    public static string SIAMAdminMailId => hstAdmin["SIAMAdminMailId"].ToString();
                    public static string CSAdminMailId => hstAdmin["CSAdminMailId"].ToString();
                    public static string SonysecMailId => hstAdmin["SonysecMailId"].ToString();
                    public static string LDAPMailId => hstAdmin["LDAPMailId"].ToString();
                    public static string ProductRegistrationFromID => hstAdmin["ProductRegistrationFromID"].ToString();
                }
            }
            public static class SISServicesMonitor {
                static Hashtable hstSISServicesMonitor = (Hashtable)hstGeneral["SISServicesMonitor"];
            }
            public static class TraceLevel {
                static Hashtable hstTraceLevel = (Hashtable)hstGeneral["TraceLevel"];
                public static string TraceVerbose => hstTraceLevel["TraceVerbose"].ToString();
                public static string TraceError => hstTraceLevel["TraceError"].ToString();
                public static string TraceOff => hstTraceLevel["TraceOff"].ToString();
                public static string TraceWarning => hstTraceLevel["TraceWarning"].ToString();
                public static string TraceInfo => hstTraceLevel["TraceInfo"].ToString();
                public static string CurrentSetting => hstTraceLevel["CurrentSetting"].ToString();
            }
            public static class Layout {
                static Hashtable hstLayout = (Hashtable)hstGeneral["Layout"];
                public static class NextImage {
                    static Hashtable hstNextImage = (Hashtable)hstLayout["NextImage"];
                    public static string Image1 => hstNextImage["Image1"].ToString();
                    public static string Image2 => hstNextImage["Image2"].ToString();
                    public static string Image3 => hstNextImage["Image3"].ToString();
                }
                public static class BrowseParts {
                    static Hashtable hstBrowseParts = (Hashtable)hstLayout["BrowseParts"];
                    public static string RangePerPage => hstBrowseParts["RangePerPage"].ToString();
                    public static string PartsPerRange => hstBrowseParts["PartsPerRange"].ToString();
                }
                public static class BrowseModels {
                    static Hashtable hstBrowseModels = (Hashtable)hstLayout["BrowseModels"];
                    public static string ModelRangePerPage => hstBrowseModels["ModelRangePerPage"].ToString();
                    public static string ModelsPerRange => hstBrowseModels["ModelsPerRange"].ToString();
                }
            }
            public static class RepairCosting {
                static Hashtable hstRepairCosting = (Hashtable)hstGeneral["RepairCosting"];
                public static string DepotServiceCharge => hstRepairCosting["DepotServiceCharge"].ToString();
                public static string DepotServiceChargeForCanada => hstRepairCosting["DepotServiceChargeForCanada"].ToString();
                public static string FieldServiceCharge => hstRepairCosting["FieldServiceCharge"].ToString();
                public static string DepotMinLbrCharge => hstRepairCosting["DepotMinLbrCharge"].ToString();
                public static string FieldMinLbrTime => hstRepairCosting["FieldMinLbrTime"].ToString();
            }
            public static class SecurityManagement {
                static Hashtable hstSecurityManagement = (Hashtable)hstGeneral["SecurityManagement"];
                public static string MinPwdLength => hstSecurityManagement["MinPwdLength"].ToString();
                public static string MaxPwdLengthAccount => hstSecurityManagement["MaxPwdLengthAccount"].ToString();
                public static string MaxPwdLengthNonAccount => hstSecurityManagement["MaxPwdLengthNonAccount"].ToString();
                public static string PwdChangePeriodDays => hstSecurityManagement["PwdChangePeriodDays"].ToString();
                public static string PwdChangeNotifyDays => hstSecurityManagement["PwdChangeNotifyDays"].ToString();
                public static int NumberOfPasswordAttempt => Convert.ToInt16(hstSecurityManagement["NumberOfPasswordAttempt"].ToString());
                public static string UserNameUnAvailable => hstSecurityManagement["UserNameUnAvailable"].ToString();
            }
            public static class MSMQConfiguration {
                static Hashtable hstMSMQConfiguration = (Hashtable)hstGeneral["MSMQConfiguration"];
                public static string SonyLoggingQueue => hstMSMQConfiguration["SonyLoggingQueue"].ToString();
                public static string SonyLoggingQueueMsgLifeDays => hstMSMQConfiguration["SonyLoggingQueueMsgLifeDays"].ToString();
            }
            public static class Template {
                static Hashtable hstTemplate = (Hashtable)hstGeneral["Template"];
                public static string B2BPendingTemplate => hstTemplate["B2BPendingTemplate"].ToString();
                public static string CBSRegisterTemplate => hstTemplate["CBSRegisterTemplate"].ToString();
                public static string B2BApprovalTemplate => hstTemplate["B2BApprovalTemplate"].ToString();
                public static string B2BRejectedTemplate => hstTemplate["B2BRejectedTemplate"].ToString();
                public static string TemplateDirectory => hstTemplate["TemplateDirectory"].ToString();
            }
            public static class PartsFinder {
                static Hashtable hstPartsFinder = (Hashtable)hstGeneral["PartsFinder"];
                //internal static string  ["explodedview_link"].ToString();
                public static string Explodedview_Link => hstPartsFinder["explodedview_link"].ToString();
            }
            public static class URL {
                static Hashtable hstURL = (Hashtable)hstGeneral["URL"];
                public static string Servicesplus_Link => hstURL["servicesplus_link"].ToString();
                public static string Promotions_Link => hstURL["promotions_link"].ToString();
                public static string Find_Parts_Link => hstURL["find_parts_link"].ToString();
                public static string Find_Software_Link => hstURL["find_software_link"].ToString();
                public static string Privacy_Policy_Link => hstURL["privacy_policy_link"].ToString();
                public static string Terms_And_Conditions_Link => hstURL["terms_and_conditions_link"].ToString();
                public static string Contact_Us_Link => hstURL["contact_us_link"].ToString();
                public static string contact_us_popup_link => hstURL["contact_us_popup_link"].ToString();
                public static string Customer_Service_Link => hstURL["customer_service_link"].ToString();
                public static string Training_Link => hstURL["training_link"].ToString();
                public static string Terms_And_Conditions_Link_ca => hstURL["terms_and_conditions_link_ca"].ToString();
                public static string Contact_Us_Link_ca => hstURL["contact_us_link_ca"].ToString();
                public static string Customer_Service_Link_ca => hstURL["customer_service_link_ca"].ToString();
            }

            public static class General {
                static Hashtable hstGeneral_internal = (Hashtable)hstGeneral["General"];
                public static string EmailServicePollingInterval => hstGeneral_internal["EmailServicePollingInterval"].ToString();
                public static string QuickOrderMaxItems => hstGeneral_internal["QuickOrderMaxItems"].ToString();
                public static string OrderUploadMaxItems => hstGeneral_internal["OrderUploadMaxItems"].ToString();
                public static string Update_Date => hstGeneral_internal["update_date"].ToString();
                public static string AdministrationDelegate => hstGeneral_internal["AdministrationDelegate"].ToString();
                public static bool EnableCheckOut => Convert.ToBoolean(hstGeneral_internal["EnableCheckOut"].ToString());
            }

            public static class ContactNumber {
                static Hashtable hstContactNumber = (Hashtable)hstGeneral["ContactNumber"];
                public static string OrderDetail => hstContactNumber["OrderDetail"].ToString();
            }

            public static class SpecialCharacter {
                static Hashtable hstSpecialCharacter = (Hashtable)hstGeneral["SpecialCharacter"];
                public static string SpecialCharacterDetail => hstSpecialCharacter["SpecialCharacterDetail"].ToString();
            }

            public static class CreditCardLockKey {
                public static string CreditCard_LockKey => CreditCardDecryptionLockKey;
            }

            public static class PCILogger {
                static Hashtable hstPCILogger = (Hashtable)hstGeneral["PCILogger"];
                public static double Interval => Convert.ToDouble(hstPCILogger["Interval"].ToString());
                public static string MSMQName => hstPCILogger["MSMQName"].ToString();
                public static string MSMQLabel => hstPCILogger["MSMQLabel"].ToString();
            }
            public static class PunchOut {
                static Hashtable hstPunchOut = (Hashtable)hstGeneral["PunchOut"];
                public static string UseLocalDTD => hstPunchOut["UseLocalDTD"].ToString();
                public static string ValidateXML => hstPunchOut["ValidateXML"].ToString();
            }

            public static class BillingOnly {
                static Hashtable hstBillingOnly = (Hashtable)hstGeneral["BillingOnly"];
                public static string CarrierCode => hstBillingOnly["CarrierCode"].ToString();
                public static string GroupCode => hstBillingOnly["GroupCode"].ToString();
            }

            public static class StatusCodes {
                static Hashtable hstStatusCodes = (Hashtable)hstGeneral["StatusCodes"];
                public static string Active => hstStatusCodes["Active"].ToString();
                public static string Inactive => hstStatusCodes["Inactive"].ToString();
            }

            public static class SIAMOperations {
                static Hashtable hstBillingOnly = (Hashtable)hstGeneral["SIAMOperations"];
                public static string EnableServAgreementDatabase => hstBillingOnly["EnableServAgreementDatabase"].ToString();
            }

            public static class AdminLocalAccess {
                static Hashtable hstStatusCodes = (Hashtable)hstGeneral["AdminLocalAccess"];
                public static string AuthenticateAdminLocalFlag => hstStatusCodes["AuthenticateAdminLocalFlag"].ToString();
                public static string AuthenticateAdminLocalGlobalID => hstStatusCodes["AuthenticateAdminLocalGlobalID"].ToString();
            }

            //Sasikumar WP
            public static class GlobalData {
                static Hashtable hstGlobalData = (Hashtable)hstGeneral["GlobalData"];

                public static class SalesOrganization {
                    static Hashtable hstSalesOrganization = (Hashtable)hstGlobalData["SalesOrganization"];
                    public static string US => hstSalesOrganization["US"].ToString();
                    public static string CA => hstSalesOrganization["CA"].ToString();
                }
                public static class DistributionChannel {
                    static Hashtable hstDistributionChannel = (Hashtable)hstGlobalData["DistributionChannel"];
                    public static string Value => hstDistributionChannel["code"].ToString();
                }
                public static class Division {
                    static Hashtable hstDivision = (Hashtable)hstGlobalData["Division"];
                    public static string Value => hstDivision["code"].ToString();
                }
                public static class Language {
                    static Hashtable hstLanguage = (Hashtable)hstGlobalData["Language"];
                    public static string US => hstLanguage["US"].ToString();
                    public static string CA => hstLanguage["CA"].ToString();
                    public static string CA_en => hstLanguage["CA_en"].ToString();
                }
                public static class LanguagewM {
                    static Hashtable hstLanguagewM = (Hashtable)hstGlobalData["LanguagewM"];
                    public static string US => hstLanguagewM["US"].ToString();
                    public static string CA => hstLanguagewM["CA"].ToString();
                }

                public static class ZipCode {
                    static Hashtable hstZipCode = (Hashtable)hstGlobalData["ZipCode"];
                    public static string US => hstZipCode["US"].ToString();
                    public static string CA => hstZipCode["CA"].ToString();
                }

                public static class ProductionUrl {
                    static Hashtable hstProductionUrl = (Hashtable)hstGlobalData["URL"];
                    public static string US => hstProductionUrl["US"].ToString();
                    public static string CA => hstProductionUrl["CA"].ToString();
                }
            }
        }

        public static class Environment {
            internal static Hashtable hstEnvironment = (Hashtable)hstConfigData["Environment"];

            public static class DataBase {
                public static string ConnectionString => Encryption.DeCrypt(hstConfigData["DataBaseConnection"].ToString(), Encryption.PWDKey); //sathish 
            }

            public static class Token {
                static Hashtable hsttoken = (Hashtable)hstEnvironment["Token"];
                public static string GUID => hsttoken["GUID"].ToString();
                public static string PresharedKey => hsttoken["PresharedKey"].ToString();
                public static string BaseURL => hsttoken["BaseURL"].ToString();
            }

            public static class Download {
                static Hashtable hstDownload = (Hashtable)hstEnvironment["Download"];
                public static string Url => hstDownload["url"].ToString();
                public static string Physical => hstDownload["physical"].ToString();
                public static string ReleaseNotes => hstDownload["releasenotes"].ToString();
                public static string DownloadFiles => hstDownload["downloadfiles"].ToString();
                public static string TechBulletin => hstDownload["techbulletin"].ToString();
                public static string TechBulletInform => hstDownload["techbulletinform"].ToString();
            }

            public static class Email {
                static Hashtable hstEmail = (Hashtable)hstEnvironment["Email"];
                public static class Repair {
                    static Hashtable hstRepair = (Hashtable)hstEmail["Repair"];
                    public static string Service_Request_From_Mail => hstRepair["service_request_from_mail"].ToString();
                    public static string Service_Request_Bcc_Mail => hstRepair["service_request_bcc_mail"].ToString();
                    public static string Service_Request_Bcc_Sanjose_Mail => hstRepair["service_request_bcc_sanjose_mail"].ToString();
                    public static string Service_Request_Bcc_Teaneck_Mail => hstRepair["service_request_bcc_Teaneck_mail"].ToString();
                    public static string Service_Request_Bcc_LosAngeles_Mail => hstRepair["service_request_bcc_LosAngeles_mail"].ToString();
                }
                public static class Training {
                    static Hashtable hstTraining = (Hashtable)hstEmail["Training"];
                    public static string Training_Admin => hstTraining["training_admin"].ToString();
                    public static string NonClassRoomTraining_Admin => hstTraining["nonclassroomtraining_admin"].ToString();
                }
                public static class ServiceAgreement {
                    static Hashtable hstServiceAgreement = (Hashtable)hstEmail["ServiceAgreement"];
                    public static string Service_Agreement_BCC => hstServiceAgreement["service_agreement_bcc"].ToString();
                    public static string Service_Agreement_From_Mail => hstServiceAgreement["service_agreement_from_mail"].ToString();
                }

                public static class User {
                    static Hashtable hstUser = (Hashtable)hstEmail["User"];
                    public static string Delete_InactiveUser_TO_Mail => hstUser["delete_inactiveuser_to_mail"].ToString();
                }

                public static class General {
                    static Hashtable hstGeneral = (Hashtable)hstEmail["General"];
                    public static string From => hstGeneral["from"].ToString();
                    public static string RA_To => hstGeneral["ra_to"].ToString();
                    public static string ServicesPLUS_Mail => hstGeneral["servicesplus_mail"].ToString();
                    public static string FraudUserReporting => hstGeneral["FraudUserReporting"].ToString();
                    public static string InvoiceProcessorConfirmationMail => hstGeneral["InvoiceProcessorConfirmationMail"].ToString();
                    public static string ForgotPasswordFromEmalId => hstGeneral["ForgotPasswordFromEmalId"].ToString();
                }
            }

            public static class URL {
                static Hashtable hstURL = (Hashtable)hstEnvironment["URL"];
                public static string HomePage => hstURL["homePage"].ToString();
                public static string Training_AdminLink => hstURL["training_adminlink"].ToString();
                public static string Cs_AdminLink => hstURL["cs_adminlink"].ToString();
                public static string US => hstURL["US"].ToString();
                public static string CA => hstURL["CA"].ToString();
            }

            public static class ISSAWebService {
                static Hashtable hstISSAWebService = (Hashtable)hstEnvironment["ISSAWebService"];
                public static string Url => string.Empty;
                public static string RequestingApplication => string.Empty;
                public static string RequestingUserID => string.Empty;
                public static string RequestingPassword => string.Empty;
                public static string ISSAApprovalAppURL => string.Empty;
            }

            public static class LdapWebService {
                static Hashtable hstLdapWebService = (Hashtable)hstEnvironment["LdapWebService"];
                public static string Url => hstLdapWebService["Url"].ToString();
                public static string Adminid => hstLdapWebService["Adminid"].ToString();
                public static string UserName => hstLdapWebService["UserName"].ToString();
                public static string Password => hstLdapWebService["Password"].ToString();
            }

            public static class SMTP {
                static Hashtable hstSMTP = (Hashtable)hstEnvironment["SMTP"];
                public static string EmailServer => hstSMTP["EmailServer"].ToString();
                public static string EmailServer_CS => hstSMTP["EmailServer_CS"].ToString();
                public static string EmailServer_BatchProcessor => hstSMTP["EmailServer_BatchProcessor"].ToString();
                public static Int16 Port => Convert.ToInt16(hstSMTP["Port"].ToString());
            }

            public static class IdentityURLs {
                static Hashtable hstIdentityURLs = (Hashtable)hstEnvironment["IdentityURLs"];
                public static string LoginUS => hstIdentityURLs["LoginUS"].ToString();
                public static string LoginENCA => hstIdentityURLs["LoginENCA"].ToString();
                public static string LoginFRCA => hstIdentityURLs["LoginFRCA"].ToString();
                public static string LogoutUS => hstIdentityURLs["LogoutUS"].ToString();
                public static string LogoutENCA => hstIdentityURLs["LogoutENCA"].ToString();
                public static string LogoutFRCA => hstIdentityURLs["LogoutFRCA"].ToString();
            }

            // 2016-05-19 ASleight Changes for Shibboleth 
            //public static class ServicesPLUSLogout
            //{
            //    static Hashtable hstServicesPLUSLogout = (Hashtable)hstEnvironment["ServicesPLUSLogout"];
            //    //internal static string  ["ServicesPLUSLogoutURL"].ToString();
            //    public static string ServicesPLUSLogoutURL { get { return hstServicesPLUSLogout["ServicesPLUSLogoutURL"].ToString(); } }
            //    public static string ServicesPLUSCALogoutURL { get { return hstServicesPLUSLogout["ServicesPLUSCALogoutURL"].ToString(); } }
            //}

            // 2016-05-19 ASleight This class is not needed. Code will be moved away from this.
            //public static class ServicesPLUSLogoutCA
            //{
            //    static Hashtable hstServicesPLUSLogout = (Hashtable)hstEnvironment["ServicesPLUSLogout"];
            //    //internal static string  ["ServicesPLUSLogoutURL"].ToString();
            //    public static string ServicesPLUSCALogoutURL { get { return hstServicesPLUSLogout["ServicesPLUSCALogoutURL"].ToString(); } }
            //}

            public static class SIAMLogout {
                static Hashtable hstSIAMLogout = (Hashtable)hstEnvironment["SIAMLogout"];
                public static string SIAMLogoutURL => hstSIAMLogout["SIAMLogoutURL"].ToString();
            }
            public static class Contacts {
                static Hashtable hstSIAMLogout = (Hashtable)hstEnvironment["Contacts"];
                public static string TrainingInstitute => hstSIAMLogout["TrainingInstitute"].ToString();
            }

            public static class PunchOut {
                static Hashtable hstPunchOut = (Hashtable)hstEnvironment["PunchOut"];
                public static string PunchOutRequestURL => hstPunchOut["PunchOutRequestURL"].ToString();
            }

            public static class PCILogger {
                static Hashtable hstPCILogger = (Hashtable)hstEnvironment["PCILogger"];
                public static string ApplicationLogsLocation => hstPCILogger["ApplicationLogsLocation"].ToString();
            }

            public static string WebSiteURL => hstEnvironment["WebSiteURL"].ToString();

            public static class SAPWebMethods {
                static Hashtable hstWebmethods = (Hashtable)hstEnvironment["Webmethods"];
                public static string UserName => hstWebmethods["UserName"].ToString();
                public static string password => hstWebmethods["password"].ToString();
                public static string CancelOrder => hstWebmethods["CancelOrder"].ToString();
                public static string CustomerLookUp => hstWebmethods["CustomerLookUp"].ToString();
                public static string PartInquiry => hstWebmethods["PartInquiry"].ToString();
                public static string POInquiry => hstWebmethods["POInquiry"].ToString();
                public static string SalesOrder => hstWebmethods["SalesOrder"].ToString();
                public static string CertificatePath => hstWebmethods["CertificatePath"].ToString();
                public static string Source => hstWebmethods["Source"].ToString();
                public static string POType => hstWebmethods["POType"].ToString();
                public static string PartLocation => hstWebmethods["PartLocation"].ToString();
                public static string DefaultAccount => hstWebmethods["DefaultAccount"].ToString();
                public static string PartNumberMaterialType => hstWebmethods["PartNumberMaterialType"].ToString();
                public static string SalesOrderCertificatePath => hstWebmethods["SalesOrderCertificatePath"].ToString();
                //public static string CancelOrderCertificatePath { get { return hstWebmethods["CancelOrderCertificatePath"].ToString(); } }//7792
                public static string timeout => hstWebmethods["timeout"].ToString();
                public static string SalesOrganization => hstWebmethods["SalesOrganization"].ToString();
                public static string Return_Authorization_Form_Processor => hstWebmethods["SalesOrganization"].ToString();
            }

            public static class ChangePassword {
                static Hashtable hstChangePassword = (Hashtable)hstEnvironment["ChangePassword"];
                public static string ChangePasswordURL => hstChangePassword["ChangePasswordURL"].ToString();
            }

            public static class ForgotPassword {
                static Hashtable hstForgotPassword = (Hashtable)hstEnvironment["ForgotPassword"];
                public static string ForgotPasswordURL => hstForgotPassword["ForgotPasswordURL"].ToString();
            }

            public static class CancelOrder {
                static Hashtable hstRejectionReason = (Hashtable)hstEnvironment["CancelOrder"];
                public static string RejectionReason => hstRejectionReason["RejectionReason"].ToString();
            }

            public static class PartNumberMaterialType {
                static Hashtable hstMaterialType = (Hashtable)hstEnvironment["PartNumberMaterialType"];
                public static string MaterialType => hstMaterialType["MaterialType"].ToString();
            }

            public static class SalesOrganization {
                static Hashtable hstSalesOrganization = (Hashtable)hstEnvironment["SalesOrganization"];
                public static string SalesOrgValue => hstSalesOrganization["Code"].ToString();
            }

            public static class RA_Form_Processor {
                static Hashtable hstRA_Form_Processor = (Hashtable)hstEnvironment["Return_Authorization_Form_Processor"];
                public static string FormId => hstRA_Form_Processor["form_id"].ToString();
                public static string Action => hstRA_Form_Processor["action"].ToString();
            }

            public static class SoftwarePlusConfiguration {
                static Hashtable hstSoftwarePlusConfiguration = (Hashtable)hstEnvironment["SoftwarePlusConfiguration"];
                public static string ErrorLogFile => hstSoftwarePlusConfiguration["ErrorLogFile"].ToString();
                public static string SoftwarePLUSDownloadFilepath => hstSoftwarePlusConfiguration["SoftwarePLUSDownloadFilepath"].ToString();
                public static string SoftwarePLUSUserManualsPath => hstSoftwarePlusConfiguration["SoftwarePLUSUserManualsPath"].ToString();
                public static string SoftwarePLUSReleaseNotes => hstSoftwarePlusConfiguration["SoftwarePLUSReleaseNotes"].ToString();
            }
        }

        public static class TokenizationTest {
            public static string GUID {
                set;
                get;
            }

            public static string PresharedKey {
                set;
                get;
            }

            public static string BaseURL {
                set;
                get;
            }

            public static void Load(string sEnviroment) {
                string RegistryRootKey = @"SOFTWARE\ServicesPLUS";
                string RegKeyConfigLocation = @"ConfigFileLocation";
                string m_filename = string.Empty;

                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                    using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                        if (subKey == null) {
                            m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                        } else {
                            m_filename = subKey.GetValue(RegKeyConfigLocation) as string;
                        }
                    }
                }

                XDocument objDoc = XDocument.Load(m_filename);
                var query = from p in objDoc.Descendants("Environment")
                            where (string)p.Attribute("name") == sEnviroment
                            select p;

                var query1 = from p1 in query.Descendants("item")
                             where (string)p1.Attribute("name") == "Token"
                             select p1;
                foreach (XElement objElement in query1.Elements()) {
                    switch (objElement.Attribute("name").Value) {
                        case "GUID":
                            GUID = objElement.Attribute("value").Value;
                            break;
                        case "PresharedKey":
                            PresharedKey = objElement.Attribute("value").Value;
                            break;
                        case "BaseURL":
                            BaseURL = objElement.Attribute("value").Value;
                            break;
                    }
                }
            }
        }

        public static class IDMinderNewUserTest {
            public static string Url {
                set;
                get;
            }

            public static string Adminid {
                set;
                get;
            }

            public static string UserName {
                set;
                get;
            }
            public static string Password {
                set;
                get;
            }

            public static void Load(string sEnviroment) {
                string RegistryRootKey = @"SOFTWARE\ServicesPLUS";
                string RegKeyConfigLocation = @"ConfigFileLocation";
                string m_filename = string.Empty;
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)) {
                    using (var subKey = hklm.OpenSubKey(RegistryRootKey)) {
                        if (subKey == null) {
                            m_filename = @"C:\Program Files (x86)\ServicesPLUS\Sony BSSC Core Services\Configuration\ServicesPLUSConfig.xml";
                        } else {
                            m_filename = subKey.GetValue(RegKeyConfigLocation) as string;
                        }
                    }
                }
                XDocument objDoc = XDocument.Load(m_filename);
                var query = from p in objDoc.Descendants("Environment")
                            where (string)p.Attribute("name") == sEnviroment
                            select p;

                var query1 = from p1 in query.Descendants("item")
                             where (string)p1.Attribute("name") == "LdapWebService"
                             select p1;
                foreach (XElement objElement in query1.Elements()) {
                    switch (objElement.Attribute("name").Value) {
                        case "Url":
                            Url = objElement.Attribute("value").Value;
                            break;
                        case "Adminid":
                            Adminid = objElement.Attribute("value").Value;
                            break;
                        case "UserName":
                            UserName = objElement.Attribute("value").Value;
                            break;
                        case "Password":
                            Password = objElement.Attribute("value").Value;
                            break;
                    }
                }
            }
        }
        #region [Comented]
        //public static class UserTypes
        //{
        //    public UserTypes()
        //    {

        //    }
        //    static Hashtable hstUserTypes = (Hashtable)hstEnvironment["UserTypes"].ToString();
        //    public static class AccountHolder
        //    {
        //        static Hashtable hstAccountHolder = (Hashtable)hstUserTypes["AccountHolder"].ToString();
        //        public static class LoginPolicy
        //        {
        //             static Hashtable hstLoginPolicy = (Hashtable)hstAccountHolder["LoginPolicy"].ToString();
        //            //internal static string  ["passwordExpiration"].ToString();
        //            //internal static string  ["gracePeriod"].ToString();
        //            //internal static string  ["failedLoginThreshold"].ToString();
        //            //internal static string  ["TimeThreshold"].ToString();
        //            public static string PasswordExpiration { get { return  hstLoginPolicy["passwordExpiration"].ToString(); } }
        //            public static string GracePeriod { get { return  hstLoginPolicy["gracePeriod"].ToString(); } }
        //            public static string FailedLoginThreshold { get { return  hstLoginPolicy["failedLoginThreshold"].ToString(); } }
        //            public static string TimeThreshold { get { return  hstLoginPolicy["TimeThreshold"].ToString(); } }
        //            public static class SecurityQuestions
        //            {
        //                static Hashtable hstSecurityQuestions = (Hashtable)hstLoginPolicy["SecurityQuestions"].ToString();
        //                //internal static string  ["0"].ToString();
        //                //internal static string  ["1"].ToString();
        //                //internal static string  ["2"].ToString();
        //                public static string Zero { get { return  hstSecurityQuestions["0"].ToString(); } }
        //                public static string One { get { return  hstSecurityQuestions["1"].ToString(); } }
        //                public static string Two { get { return  hstSecurityQuestions["2"].ToString(); } }

        //            }
        //        }
        //        public static class AuthorizationDelegate
        //        {
        //            //internal static string  ["TypeDef"].ToString();
        //            //internal static string  ["ConstructorArgs"].ToString();
        //            public static string TypeDef { get { return  ["TypeDef"].ToString(); } }
        //            public static string ConstructorArgs { get { return  ["ConstructorArgs"].ToString(); } }
        //        }
        //        public static class AuthenticationDelegate
        //        {
        //            //internal static string  ["TypeDef"].ToString();
        //            public static string TypeDef { get { return  ["TypeDef"].ToString(); } }
        //            public static class ConstructorArgs
        //            {
        //                //internal static string  ["0"].ToString();
        //                //internal static string  ["1"].ToString();
        //                //internal static string  ["2"].ToString();
        //                //internal static string  ["3"].ToString();
        //                public static string Zero { get { return  ["0"].ToString(); } }
        //                public static string One { get { return  ["1"].ToString(); } }
        //                public static string Two { get { return  ["2"].ToString(); } }
        //                public static string Three { get { return  ["3"].ToString(); } }
        //            }
        //        }
        //        public static class AdministrationDelegates
        //        {
        //            //internal static string  ["0"].ToString();
        //            public static string Zero { get { return  ["0"].ToString(); } }
        //            public static class LDAPAdministrator
        //            {
        //                //internal static string  ["TypeDef"].ToString();
        //                public static string TypeDef { get { return  ["TypeDef"].ToString(); } }
        //                public static class ConstructorArgs
        //                {
        //                    //internal static string  ["0"].ToString();
        //                    //internal static string  ["1"].ToString();
        //                    //internal static string  ["2"].ToString();
        //                    //internal static string  ["3"].ToString();
        //                    //internal static string  ["4"].ToString();
        //                    //internal static string  ["5"].ToString();
        //                    //internal static string  ["6"].ToString();
        //                    public static string Zero { get { return  ["0"].ToString(); } }
        //                    public static string One { get { return  ["1"].ToString(); } }
        //                    public static string Two { get { return  ["2"].ToString(); } }
        //                    public static string Three { get { return  ["3"].ToString(); } }
        //                    public static string Four { get { return  ["4"].ToString(); } }
        //                    public static string Five { get { return  ["5"].ToString(); } }
        //                    public static string Six { get { return  ["6"].ToString(); } }
        //                }
        //            }
        //        }
        //    }
        //}
        //  public static class UserTypes
        //{
        //    private static string mName;
        //    private static _loginPolicy mLoginPolicy;
        //    private static _Delegate mAdministrationDelegates;
        //    private static _Delegate mAuthenticationDelegate;
        //    private static _Delegate mAuthorizationDelegate;

        //    public static string Name { get { return mName; } }
        //    public static _loginPolicy LoginPolicy { get { return mLoginPolicy; } }
        //    public static _Delegate AdministrationDelegates { get { return mAdministrationDelegates; } }
        //    public static _Delegate AuthenticationDelegate { get { return mAuthenticationDelegate; } }
        //    public static _Delegate AuthorizationDelegate { get { return mAuthorizationDelegate; } }
        //}
        #endregion
    }
}
