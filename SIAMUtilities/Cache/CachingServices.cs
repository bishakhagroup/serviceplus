using System;
using System.Web.Caching;
using System.Web;
using Sony.US.SIAMUtilities;
using System.Diagnostics;


namespace Sony.US.SIAMUtilities
{
	/// <summary>
	/// CareVu.CoreServices.CachingServices provides access to the cache object.
	/// </summary>
	/// <remarks>CachingServices provides a thin wrapper around the System.Web cache object.
	/// This class provides static access to the cache object via a get (Read Only) accessor
	/// is thread safe by design.</remarks>
	public class CachingServices
	{
		private const string ClassName = "CachingServices";
		private static HttpContext _httpContext;
		
		/// <summary>
		/// Protected constructor to prevent client creation.
		/// </summary>
		/// <remarks>Provides static read only access. No direct instantiation by the client is
		/// allowed.</remarks>
		protected CachingServices()
		{
		}

		/// <summary>
		/// Accessor to the Cache object.  
		/// </summary>
		/// <remarks>Creates the Cache for the current Application Domain on the first call, returns the existing Cache otherwise.</remarks>
		public static Cache Cache
		{
			get
			{

                //if (_httpContext == null)
                //{
                //    lock (typeof(HttpContext))
                //    {
                //        _httpContext = (HttpContext.Current != null) ? HttpContext.Current : new HttpContext(null);
                //    }
                //    return _httpContext.Cache;
                //}

                if (_httpContext == null)
                {
                    lock (typeof(HttpContext))
                    {
                        try
                        {
                            if (HttpContext.Current == null)
                            {
                                //System.Threading.Thread.Sleep(2000);
                                //_httpContext.Cache  = HttpRuntime.Cache; 
                                return HttpRuntime.Cache; 
                            }
                            else
                            {
                                _httpContext = HttpContext.Current;
                            }
                        }
                        catch (Exception e1)
                        {
                            //_httpContext = new HttpContext(null);
                            _httpContext = null;
                        }

                    }
                    return _httpContext.Cache;
                }
                else
                {
                    return _httpContext.Cache;
                }
			}
		}
	}
}
